Un des apports conséquents des machines de Turing est leur capacité à classer les fonctions qui sont calculables selon les difficultés à calculer ces dernières. La notion de  difficulté est mesurée par la quantité de ressources nécessaires à leur calcul. Si on revient à l'analogie de l'humain effectuant un calcul, plus une fonction est compliquée à calculer plus celui-ci doit utiliser de papier et effectuer des choix pour réussir à le calculer. Dans ce contexte, nous différencions les machines de Turing déterministes et non-déterministes ce qui est une première applications des différentes variantes de machines de Turing.

Nous commençons par définir la notion de langage décidé en fonction du temps ou de l'espace. Cette mesure sur la fonction représentant la ressource utilisées va nous servir d'échelle pour créer nos classes de complexité.
	
\begin{definition}[Arbre de calcul]
	Un \emph{arbre de calcul} d'une machine de Turing $\mathcal{M} = (\mathcal{Q}, \Gamma, \Sigma, \delta, s, \#, F)$ depuis la configuration $(\epsilon, s, w)$ est l'arbre de racine la configuration $(\epsilon, s, w)$ tel que les fils de tout noeud contenant la configuration $c$ sont l'ensemble des configurations suivantes de $c$.
\end{definition}

\begin{definition}[Langage décidé en temps/espace $f$]
	Soient $f : \N \to \N$ et une machine de Turing $\mathcal{M}  = (\mathcal{Q}, \Gamma, \Sigma, \delta, s, \#, F)$. On dit que\emph{ $\mathcal{M}$ décide $L$ en temps $f$} si $\mathcal{M}$ décide $L$ et pour tout $w$, la hauteur de l'arbre depuis $(\epsilon, s, w)$ est inférieure à $f\left(\left|w\right|\right)$.
	
	On dit que \emph{$\mathcal{M}$ décide $L$ en espace $f$} si $\mathcal{M}$ décide $L$ et pour tout $w$, au plus $f\left(\left|w\right|\right)$ cases du ruban ont été utilisées.
\end{definition}

\begin{req}
	Attention, lorsque nous évaluons la quantité d'espace utilisé, il y a une petite subtilité si l'espace consommé par la machine de Turing est défini par une fonction $f$ telle que $f(n) < n$. Dans ce cas, on utilise une variante des machines de Turing : la machine à deux rubans (un  ruban pour l'entrée en lecture seule en une seule passe et un ruban de travail dont l'espace utilisé est majoré par $f$).
\end{req}

Cette notion de langage décidé en fonction d'un temps ou d'un espace étant définie, nous pouvons définir différentes familles de complexités qui vont nous donner les classes de complexité usuelles. 	
	
\begin{definition}[Famille de complexité]
	Soit $\mathcal{M}$ une machine de Turing déterministe et $\mathcal{N}$ une machine de Turing non-déterministe.
	\begin{itemize}
		\item $TIME(f) = \{L~|~L \text{ est décidé par } \mathcal{M} \text{ en temps } O\left(f(n)\right)\}$
		\item $NTIME(f) = \{L~|~L \text{ est décidé par } \mathcal{N} \text{ en temps } O\left(f(n)\right)\}$
		\item $EPACE(f) = \{L~|~L \text{ est décidé par } \mathcal{M} \text{ en espace } O\left(f(n)\right)\}$
		\item $NSPACE(f) = \{L~|~L \text{ est décidé par } \mathcal{N} \text{ en espace } O\left(f(n)\right)\}$
	\end{itemize}
\end{definition}

\begin{definition}[Classes de complexité classiques]
	\textcolor{white}{a}
	\begin{itemize}
		\item $P = \cup_{k} TIME\left(x \mapsto x^k\right)$ 
		\item $NP = \cup_{k} NTIME\left(x \mapsto x^k\right)$
		\item $PSPACE = \cup_{k} SPACE\left(x \mapsto x^k\right)$
		\item $NPSPACE = \cup_{k} NSPACE\left(x \mapsto x^k\right)$
	\end{itemize}
\end{definition}	
	
Dans la théorie de la complexité, l'équivalence entre une machine de Turing déterministe et une machine de Turing non-déterministe est une question importante et souvent difficile. Généralement, lorsque nous donnons deux classes de complexité telle que seule la présence ou du non-déterministe les séparent, nous ne pouvons pas dire si elles sont distinctes ou non ($P$ égal ou non $NP$).	Cependant, pour l'étude des classes de complexité mémoire polynomiale, nous avons le résultat suivant $NPSPACE = PSPACE$. C'est une conséquence du théorème de Savitch dont la preuve repose sur un problème de la théorie des graphes. On utilise le graphe des configurations de la machine de Turing non-déterministe (qui est borné par hypothèse), et de lui appliquer le problème d'accessibilité dans un graphe orienté. Comme celui-ci est dans $P$ (on verra plus loin que c'est même un peu mieux que cela), on va pouvoir exploiter sa complexité spatiale afin d'obtenir le résultat que l'on recherche.

\begin{theo}[Savitch \protect{\cite{Carton}}]
	Soit $f : \N \rightarrow \R^{+}$ telle que $f(n) \geq n$ et $f(n)$ est calculable en espace $O(f)$. Alors, $NSPACE(f) \subseteq SPACE(f^{2})$.
\end{theo}
\begin{proof}
	Soit $\mathcal{M}$ une machine non-déterministe qui décide un langage $L$ en espace $f(n)$ (cela implique que $L \in NSPACE(f)$). Montrons qu'il existe une machine $\mathcal{M}'$ qui simule $\mathcal{M}$ pour décider $L$ en espace $O(f^{2}(n))$.
	
	\paragraph{Hypothèses sur $\mathbf{\mathcal{M}}$} Nous supposons sans perte de généralité, sur la famille de langages décidés par la machine de Turing, que $\mathcal{M}$ possède un unique état initial $q_{init}$ et un unique état final $q_{final}$ qui est atteint une fois que $\mathcal{M}$ a effacé son ruban et que la tête de lecture est revenue se placer à gauche. On notera $c_{accept}$ la configuration finale (liée à l'état final). Soit $w$ l'entrée sur $\mathcal{M}$, on note $c_{init}$ la configuration initiael correspondant à $w$.
	
	Comme on s'intéresse à la mémoire utilisée par la machine de Turing durant son exécution (pas à son temps de calcul ou à sa taille), seule la quantité de ruban que la machine utilise durant son exécution doit être invariant par les changement effectués sur la machine de Turing. Les changements que nous effectuons ne modifient pas la quantité de ruban utilisée puisque la seule opération effectuée sur le ruban par cette nouvelle machine consiste à effacer le reste de son ruban.
	
	
	\paragraph{Définition de $\mathbf{\mathcal{M}'}$} On définit $\mathcal{M}'$ par l'algorithme~\ref{algo:M'}. Elle prend en entrée $w$ l'entrée de $\mathcal{M}$ et elle accepte l'exécution si $w \in L(\mathcal{M})$ (elle la rejette dans le cas contraire). On remarque que cet algorithme est bien déterministe. Pour définir la procédure \textsc{$\mathcal{M}$}, nous avons besoin de définir le graphe de configuration d'une machine de Turing.
	
	\begin{definition}[Graphe des configurations]
		Le graphe des configuration est un graphe dont les sommets sont les configurations de la machine de Turing et les arêtes sont définies telles que la fonction de transition permet de passer d'une configuration à une autre en une étape.
	\end{definition}
	
	\begin{algorithm}
		\begin{algorithmic}[1]
			\Procedure{\textsc{$\mathcal{M}'$}}{$w$} 
			\If {$(c_{init}) \vdash^{*}_{\mathcal{M}} c_{accept}$ dans le graphe des configuration de $\mathcal{M}$}
			\State Accepter
			\Else
			\State Rejeter
			\EndIf
			\EndProcedure
		\end{algorithmic}
		\caption{Définition de la machine de Turing déterministe $\mathcal{M}'$}
		\label{algo:M'}
	\end{algorithm}
	
	
	\paragraph{Complexité de $\mathbf{\mathcal{M}'}$} Nous allons évaluer la complexité de $\mathcal{M}'$ en étudiant la complexité pour le calcul de ce prédicat: $(c_{init}) \vdash^{*}_{\mathcal{M}} c_{accept}$. On remarque dans un premier temps que si on a $(c_{init}) \vdash^{*}_{\mathcal{M}} c_{accept}$ alors il existe un chemin de $c_{init}$ à $c_{accept}$ dans le graphe des configurations de $\mathcal{M}$ que l'on note $(c_{init}) \rightarrow^{*}_{\mathcal{M}} c_{accept}$. 
	
	Pour ce calcul de complexité, nous allons majorer le nombre de configurations accessibles à partir $c_{init}$ et définir une fonction qui calcul le chemin de $c_{init}$ à $c_{accept}$ en majorant le nombre d'étapes. Pour cela, nous allons utiliser deux lemmes.
	
	\begin{lemme}
		Il existe un $d \in \N$ tel que le nombre de configurations accessibles depuis $c_{initial}$ soit majoré par $2^{df(|w|)}$.
	\end{lemme}
	\begin{proof}
		Le nombre de configurations accessibles depuis  $c_{init}$ est majoré par $|\mathcal{Q}| \times |\Gamma|^{f(|w|)} \times f(|w|)$ car $|\mathcal{Q}|$ est le nombre d'états que l'on peut atteindre, $|\Gamma|^{f(|w|)}$ est le nombre de mots que l'on peut écrire sur le ruban sans violer la propriété sur $\mathcal{M}$ et $f(|w|)$ est le nombre de positions possibles pour la tête de lecture.
	\end{proof}
	
	\begin{lemme} 
		$c_{init} \rightarrow^{*} c_{accept}$ si et seulement si $c_{init} \rightarrow^{2df(|w|)} c_{accept}$.
	\end{lemme}
	
	Nous allons alors nous appuyer sur la fonction \textsc{Chemin?} dont on donne la spécification. Pour deux configurations $c_1$ et $c_2$ de $\mathcal{M}$ et ainsi qu'un entier $t$, $\textsc{Chemin?}(c_1, c_2, t)$ renvoie \textsc{vrai} s'il existe un chemin (donc une suite de configurations pour $\mathcal{M}$) de $c_1$ vers $c_2$ dans le graphe des configurations de $\mathcal{M}$ en au plus $t$ étapes; et \textsc{faux} sinon. Dans la suite, on suppose que $t$ est une puissance de $2$ (dans le cas contraire, comme on a toujours l'existence d'une puissance de $2$ qui est plus grande que $t$, on prend la plus petite de ces puissances de $2$ comme nouvelle valeur de $t$).
	
	\begin{proof}
		Le sens indirect est immédiat. Nous allons alors montrer la réciproque.
		
		Nous implémentons le test $c_{init} \rightarrow^{*} c_{final}$ à l'aide de la fonction $\textsc{Chemin?}(c_{initial}, c_{accept}, 2^{df(|w|)})$ où \textsc{Chemin?} est définie comme précédemment et par l'algorithme \ref{algo:chemin}.
		\begin{algorithm}
			\begin{algorithmic}[1]
				\Function{\textsc{Chemin?}}{$c_1, c_2, t$} 
				\If {$t = 1$}
				\State Renvoie $c_1 \rightarrow^{\leq 1} c_2$ 
				\Else
				\For{toute configuration $c$ de taille de ruban d'au plus $f(|w|)$}
				\If{\textsc{Chemin?}($c_1, c, \frac{t}{2}$) et \textsc{Chemin?}($c, c_2, \frac{t}{2}$)}
				\State Renvoie \textsc{vrai}
				\EndIf
				\EndFor
				\State Renvoie \textsc{faux}
				\EndIf
				\EndFunction
			\end{algorithmic}
			\caption{Fonction $\textsc{Chemin?}$}
			\label{algo:chemin}
		\end{algorithm}
		
		La fonction \textsc{Chemin?} suit le paradigme diviser pour régner. Sa complexité spatiale est alors de $Cout(t) = O(f) + Cout(\frac{t}{2})$ où $t$ est le troisième argument de \textsc{Chemin?} (Hypothèse 2: on peut calculer $f(|w|)$ en $O(f)$). On obtient donc $Cout(t) = O(f\log_2 t)$ (par le master théorème sur la complexité du principe diviser pour régner). La complexité spatiale de $\mathcal{M}'$, par l'hypothèse, est de $O(f) + Cout(2^{df(|w|)}) = O(f) + O(f\log_2 (2^{df(|w|)}) = O(f) + O(fdf) = O(f) + O(f^{2}) = O(f^{2})$ car on calcul une fois $f(|w|)$ qui se calcule en $O(f)$, par hypothèse, pour la fonction \textsc{Chemin?}. On exécute alors la fonction \textsc{Chemin?} avec $t = 2^{df(|w|)}$ ce qui donne la complexité annoncée.
	\end{proof}	
\end{proof}

\begin{cor}
	$NPSPAPCE = PSPACE$
\end{cor}

Une version plus générale du théorème de Savitch existe, nous l'énonçons ci-dessous. La preuve de celui-ci se fait de manière analogue. La seule différence vient au moment de la définition de la fonction \textsc{Chemin?} où nous devons être plus pointilleux sur sa définition afin de respecter les complexités annoncées. Pour contrer cela, on appelle la fonction \textsc{Chemin?} pour chaque valeur de $f(i)$ et on incrémente $i$ pas à pas tant qu'on n'a pas trouvé de chemin acceptant. Par ce procédé, nous calculons expérimentalement la borne que l'on a ajoutée dans les hypothèses. De plus, l'hypothèse $f(n) \geq n$ nous permet de lire l'entrée sur le ruban; cependant avec une machine à plusieurs rubans, $f(n) \geq \log n$ suffit.

\begin{theo}[Théorème de Savitch \protect{\cite{Sisper}}]
	Soit $f : \N \rightarrow \R^{+}$ tel que pour $n$ assez grand $f(n) \geq \log n$. Toute machine de Turing non-déterministe qui décide en espace $f(n)$ est équivalente à une machine de Turing déterministe en espace $O(f^{2}(n))$.
\end{theo}



\begin{definition}
	On définit le problème \textsc{Universalité} sur les langages rationnels.
	
	\noindent\begin{tabular}{rl}
	Problème & \textsc{Universalité} \\
	\textbf{entrée}: & Une expression rationnelle $E$  \\
	\textbf{sortie } & Oui si $L(E) = \Sigma^{*}$; non sinon \\
\end{tabular}
\end{definition}

\begin{appli}
	Le problème \textsc{Universalité} est un problème dans $PSPACE$
\end{appli}

Pour les autres classes de complexités, l'équivalence entre les machines de Turing déterministes et les machines de Turing non-déterministes est une question ouverte. Dans le cas des langages décidables en temps polynomiales avec une machine de Turing déterministe ou non, la question est restée ouverte. Nous allons alors définir la notion de la $NP$-complétude qui caractérise les problèmes les plus difficiles de la classe $NP$ et qui ne sont à priori pas dans $P$.

\begin{definition}[$NP$-complétude \protect{\cite{Carton}}]
	Un langage $L$ est $NL$-dur si et seulement si tout problème dans $NP$ se réduit en temps polynomial à $L$. Si, de plus, $L$ est dans $NP$, $L$ est dite $NP$-complet.
\end{definition}

\begin{definition} 
	On définit le problème \textsc{SAT} sur les formules de la logique propositionnelle.
	
	\noindent\begin{tabular}{rl}
	Problème & \textsc{SAT} \\
	\textbf{entrée}: & Une formule $\phi$ de la logique propositionnelle \\
	\textbf{sortie } & Oui si $\phi$ est satisfiable; non sinon \\
	\end{tabular}
\end{definition}
	
\begin{theo}[Cook \protect{\cite{Carton}}]
	Le problème \textsc{SAT} est $NP$-complet.
\end{theo}

Il arrive que la machine de Turing non-déterministe ne soit pas la seule variante que l'on doit utiliser pour définir des classes de complexité : les machines de Turing déterministes ou non avec plusieurs rubans permettent de définir de nouvelles classes de complexité. Pour définir une machine de Turing qui décide en espace logarithmique, il nous faut une machine de Turing à deux rubans : un ruban qui contient l'entrée en lecture seule en une seule passe et le second de travail qui contient la borne logarithmique. 

\begin{definition}[Classes $NL$ et $L$ \protect{\cite{Carton}}]
	\textcolor{white}{a}
	\begin{itemize}
		\item $L = SPACE\left(\log n\right)$
		\item $NL = NSPACE\left(\log n\right)$
	\end{itemize}
\end{definition}
	
\begin{definition}
	On définit le problème \textsc{Accessibilité} sur les graphes orientés.
	
	\noindent\begin{tabular}{rl}
	Problème & \textsc{Accessibilité} \\
	\textbf{entrée}: & Un graphe orienté $G$ est deux sommets $s$ et $t$ \\
	\textbf{sortie } & Oui si il existe un chemin de $s$ à $t$ dans $G$; non sinon \\
	\end{tabular}
\end{definition}

\begin{prop}
	Le problème \textsc{Accessibilité} est dans $NL$.
\end{prop}
\begin{proof}[Idée de la preuve]
	On a une machine (algorithme~\ref{algo:Accessibilite}) en espace logarithmique qui décide en espace logarithmique le problème  de l'\textsc{Accessibilité}.
\end{proof}


\begin{algorithm}
	\begin{algorithmic}[1]
		\Procedure{\textsc{path?}}($G, s, t$)
		\While $s \neq t$
		\State $s \gets \text{ un successeur de } s$
		\EndWhile
		\EndProcedure
	\end{algorithmic}
	\captionof{algorithm}{La procédure \textsc{path?} de la preuve de l'appartenance du problème de l'\textsc{Accessibilité} à la classe $NL$.}
	\label{algo:Accessibilite}
\end{algorithm}

\begin{req}
	Si $G$ est un graphe non-orienté alors le problème arrive dans $L$.
\end{req}