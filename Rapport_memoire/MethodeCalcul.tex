Les machines de Turing sont un modèle de calcul formel introduit pour modéliser le procédé de calcul lorsqu'il est réalisé par un humain. Dans cette section, après avoir défini les modèles de calcul formels, nous allons introduire le vocabulaire nécessaire à la manipulation d'une machine de Turing. De plus, les machines de Turing peuvent calculer des fonctions en les évaluant comme le faisait les modèles de calculs précédents.

\begin{definition}[Modèle de calcul \protect{\cite{Lassaigne-Rougemont}}] Un \emph{modèle de calcul} est un système qui associe à une entrée $x$, une sortie $y$ en un nombre fini d'opérations élémentaires.
\end{definition}

	
\subsection{Vocabulaire autour des machines de Turing}

Intuitivement, une machine de Turing est un automate fini muni d'un ruban, fini à gauche et infini à droite, sur lequel on peut lire et écrire sans aucune restriction. Notre objectif est de définir les langages acceptés et décidés par une machine de Turing qui sont les notions au centre de cette leçon. En effet, elles sont au cœur des théories de la calculabilité et de la complexité et donnent de nombreuses applications aux machines de Turing. 

\begin{definition}[Machine de Turing déterministe \protect{\cite{Wolper}}]
	Une \emph{machine de Turing déterministe} est décrite par un heptuplet $\mathcal{M} = (\mathcal{Q}, \Gamma, \Sigma, \delta, s, \#, F)$ où $\mathcal{Q}$ est l'ensemble fini d'états de la machine; $\Gamma$ est l'alphabet de travail qui est celui qu'on utilise sur le ruban; $\Sigma \subseteq \Gamma$ est l'alphabet d'entrée; $\delta: \mathcal{Q} \times \Gamma \to \mathcal{Q} \times \Gamma \times \{\leftarrow, \rightarrow\}$ est la fonction de transition de la machine; $s \in \mathcal{Q}$ est l'état initial de la machine de Turing; $\# \in \Gamma \setminus \Sigma$ est le symbole blanc qui est un symbole frais et $F \subseteq \mathcal{Q}$ est l'ensemble des états acceptants de la machine.
	\label{def:MTdet}
\end{definition}
	
Les machines de Turing sont un modèle très expressif : elles sont au sommet de la hiérarchie de Chomsky \footnote{La hiérarchie de Chomsky catégorise les automates et leurs familles de langages associés en fonction de leur expressivité. On retrouve à leur sommet les machines de Turing associées aux langages généraux, puis nous avons les automates linéairement bornés associés aux langages contractuel. Ensuite, nous avons les automates à pile non-déterministe associés aux langages algébriques. Enfin, les automates finis associés aux langages rationnels sont à la base de cette hiérarchie.}. Cependant en restreignant les actions que l'on peut réaliser sur le ruban, comme par exemple, la lecture seule ou l'utilisation d'un ruban de taille bornée, on peut définir des modèles moins expressifs bien connus de la théorie des automates. 

\begin{appli}
	Un automate fini \cite{Lassaigne-Rougemont} est une machine de Turing déterministe parcourant une seule fois son ruban sans rien écrire: c'est une machine de Turing déterministe sans mémoire.
\end{appli}
	 
Pour décrire l'exécution d'une machine de Turing déterministe, nous devons décrire son évolution en fonction de son état courant et du mot inscrit sur son ruban. Nous utilisons la notion de configuration et une suite de ces configurations permet de décrire l'exécution d'une machine de Turing déterministe.

\begin{definition}[Configuration \protect{\cite{Wolper}}]
	Une \emph{configuration} d'une machine de Turing déterministe $\mathcal{M} = (\mathcal{Q}, \Gamma, \Sigma, \delta, s, \#, F)$ est un élément de $\left(\Gamma^{*} \times \mathcal{Q} \times \left(\epsilon \cup \Gamma^{*}\left(\Gamma \setminus\{\#\}\right)\right)\right)$.
	\label{def:conf}
\end{definition}	

Cette définition de configuration ne fait pas apparaître dans la configuration l'infinité de symboles blancs complétant le ruban à droite: elle s'arrête au dernier symbole non blanc sur le ruban. Cette suite infinie de symboles blancs existe toujours quelques soit la configuration considérée et sa présence ne caractérise pas la configuration. De plus, dans cette définition, la position de la tête de lecture est donnée implicitement par la longueur de la première composante correspondant au mot inscrit sur le ruban avant la tête de lecture (voir Figure~\ref{fig:conf}). Pour décrire une exécution d'une machine de Turing déterministe, il nous faut définir un moyen de passer d'une configuration à la suivante.

\begin{figure}
	\begin{tikzpicture}[decoration={brace,amplitude=15pt,mirror}]
	\tikzset{tape/.style={minimum size=.7cm, draw}}
	\begin{scope}[start chain=0 going right, node distance=0mm]
	\foreach \x [count=\i] in {,,,,,,,$x$,$\#$,$\#$,$\#$} {
		\ifnum\i=11 % if last node reset outer sep to 0pt
		\node [on chain=0, tape, outer sep=0pt] (n\i) {\x};
		\draw (n\i.north east) -- ++(.1,0) decorate [decoration={zigzag, segment length=.12cm, amplitude=.02cm}] {-- ($(n\i.south east)+(+.1,0)$)} -- (n\i.south east) -- cycle;
		\else
		\node [on chain=0, tape] (n\i) {\x};
		\fi
		\ifnum\i=1 % if first node draw a thick line at the left
		\draw [line width=.1cm] (n\i.north west) -- (n\i.south west);     
		\fi
	}
	\node [right=.25cm of n11] (dots) {$\cdots$};
	\node [right=1.5cm of dots] (conf) {$c = (\alpha, q, \gamma)$ où $x \neq \#$};
	\node [tape, above left=.25cm and 1cm of n1] (q7) {$q$};
	\draw [>=latex, ->] (q7) -| (n5); 
	\draw[decorate,draw=blue] (n1.south west)--(n4.south east)  node [midway,anchor=north,yshift=-17pt]{\textblue{$\alpha$}};
	\draw[decorate,draw=Brown] (n5.south west)--(n8.south east)  node [midway,anchor=north,yshift=-17pt]{\textbrown{$\gamma$}};
	\draw[decorate, decoration={pre length=0.2cm,post length=0.2cm, snake, amplitude=.4mm},thick, ->] (dots) -- (conf);
	\end{scope}  
	\end{tikzpicture}
	\caption{Une configuration $c$ pour une machine de Turing déterministe dans un état $q$ avec le mot $\alpha\gamma$ écrit sur le ruban et telle que sa tête de lecture est sur la première lettre du mot $\gamma$.}
	\label{fig:conf}
\end{figure}

\begin{definition}[Configuration suivante \protect{\cite{Wolper}}]
	Soit $c = (\alpha a, q, b\gamma)$ une configuration dans la machine de Turing déterministe $\mathcal{M} = (\mathcal{Q}, \Gamma, \Sigma, \delta, s, \#, F)$. On note $c'$ la \emph{configuration suivante} de $c$ que l'on définit comme suit.
	\begin{itemize}
		\item Si $\delta(q, b) = \left(q', b', \rightarrow\right)$, alors $c' = \left(\alpha ab', q, \gamma\right)$.
		\item Si $\delta(q, b) = \left(q', b', \leftarrow\right)$, alors $c' = \left(\alpha, q, ab'\gamma\right)$.
	\end{itemize}
	On note alors $c \vdash_{\mathcal{M}} c'$ le passage d'une configuration $c$ à sa configuration suivante $c'$ dans la machine de Turing déterministe $\mathcal{M}$, ou $c \vdash c'$ s'il n'y a pas d'ambiguïté sur la machine de Turing déterministe utilisée. De plus, on note $c \vdash^*_{\mathcal{M}} c'$ ou $c \vdash^* c'$ si on passe d'une configuration $c$ à $c'$ en plusieurs étapes.
\end{definition}	

\begin{definition}[Exécution d'une machine de Turing déterministe \protect{\cite{Wolper}}]
	Une \emph{exécution} d'une machine de Turing déterministe $\mathcal{M} = (\mathcal{Q}, \Gamma, \Sigma, \delta, s, \#, F)$ sur un mot $w$ est la suite de configuration, $(\epsilon, s, w) \vdash_{\mathcal{M}} c_1 \vdash_{\mathcal{M}} \dots$, issue de la configuration initiale $(\epsilon, s, w)$ et maximale telle qu'elle soit infini, ou se terminant sur un état acceptant, ou sur une configuration dont la configuration suivante n'est pas définie.
\end{definition}
	
La notion d'exécution d'une machine de Turing déterministe nous permet de définir les langages acceptés et décidés qui se distinguent en fonction de l'existence ou non d'une exécution infinie de la machine de Turing déterministe.

\begin{definition}[Langage accepté et langage décidé \protect{\cite{Wolper}}]
	Le \emph{langage accepté} par une machine de Turing déterministe $\mathcal{M}= (\mathcal{Q}, \Gamma, \Sigma, \delta, s, \#, F)$, noté $L\left(\mathcal{M}\right)$, est l'ensemble des mots $w$ tels que $(\epsilon, s, w) \vdash^{*} (\alpha, q_f, \gamma)$ où $q_f \in F$.
	
	On dit que le langage $L\left(\mathcal{M}\right)$ est \emph{décidé} si $\mathcal{M}$ n'a pas d'exécution infinie.
\end{definition}

\begin{exemple}
	Le langage $L = a^{n}b^{n}c^{n}$ est décidé par une machine de Turing déterministe.
\end{exemple}	


\subsection{Les machines de Turing calculent}

Les machines de Turing déterministes sont des modèles de calcul considérés comme les modèles abstraits des ordinateurs actuels. Ce sont des procédés mécaniques qui réalisent des calculs très généraux : elles permettent d'aller plus loin dans les calculs que les opérations arithmétiques de bases qu'elles maîtrisent. Elles permettent d'évaluer une fonction en une valeur. Cette notion fait le lien entre la théorie des fonctions calculables et celle des langages décidables. Nous allons alors introduire la notion de fonction calculable.

\begin{definition}[Fonction calculable par une machine de Turing \protect{\cite{Wolper}}]
	Une machine de Turing déterministe \emph{calcule une fonction} $f : \Sigma^{*} \to \Sigma^{*}$ si, pour tout mot d'entrée $w$, elle s'arrête toujours dans une configuration où $f(w)$ se trouve sur le ruban.
\end{definition}

\begin{exemple}[Multiplication par deux \protect{\cite{Stern}}]
	Une machine de Turing déterministe peut effectuer la multiplication par deux d'un nombre binaire. On représente cette machine sur la Figure~\ref{fig:MT-multi2}: sur les arcs de son automate fini, on inscrit la fonction de transition sous la forme: $(a, b, c)$ où $a$ est la lettre lue sur le ruban, $b$ est la lettre écrite à la place de $a$ et $c \in \{\leftarrow; \downarrow; \rightarrow\}$. 
\end{exemple}

\begin{figure}
	\centering
	\begin{minipage}[l]{.46\linewidth}
		\begin{tikzpicture}[font=\sffamily]
		
		% Setup the style for the states
		\tikzset{node style/.style={state, 
				minimum width=1cm,
				line width=0.5mm,
				fill=white!20!white}}
		
		% Draw the states
		\node[node style,initial] at (0, 0) (s0) {$ $};
		\node[node style] at (4, 0) (s2) {$ $};
		\node[node style,accepting] at (2,-2) (s3) {$ $};
		
		% Connect the states with arrows
		\draw[every loop, 
		auto=right,
		>=latex,
		draw=black,
		fill=black]	
		(s0) edge [bend left, auto=left] node {$(1, 0, \rightarrow)$} (s2)
		(s0) edge [loop above] node {$(1, 0, \downarrow)$} (s0)
		(s2) edge [loop above] node {$(1, 1, \downarrow)$} (s2)
		(s2) edge [auto=left] node {$(0, 1, \leftarrow)$} (s0)
		(s0) edge [] node {$(\#, 0, \rightarrow)$} (s3)
		(s2) edge [auto=left] node {$(\#, 1, \rightarrow)$} (s3)
		;
		\end{tikzpicture}
		\caption{Machine de Turing déterministe modélisant une multiplication par deux d'un nombre binaire.}
		\label{fig:MT-multi2}
	\end{minipage} \hfill
	\begin{minipage}[r]{.46\linewidth}
		\begin{tabular}{cc}
		\begin{tikzpicture}[->,level/.style={sibling distance=5em, level distance=3em}]
		\node [circle,draw=white] (a){$w$}
		child {node [circle,draw=white] (2){$\bullet$}
		child {node [circle,draw=white] (b) {$\bullet$}
		child {node [circle,draw=white] (c) {\textgreen{$\checkmark$}}}}};
		\end{tikzpicture} & 
		\begin{tikzpicture}[->,level/.style={sibling distance=5em/#1, level distance=3em}]
		\node [circle,draw=white] (a){$w$}
		child {node [circle,draw=white] (2){$\bullet$}
			child {node [circle,draw=white] (b) {$\bullet$}
				child {node [circle,draw=white] (c) {\textgreen{$\checkmark$}}}
				child {node [circle,draw=white] (c2) {\textred{$\times$}}}}
			child {node [circle,draw=white] (b2) {$\bullet$}
				child {node [circle,draw=white] (c1) {\textred{$\times$}}}
				child {node [circle,draw=white] (c3) {$\vdots$}}}}
		child {node [circle,draw=white] (4) {$\bullet$}
			child {node [circle,draw=white] (e1) {$\bullet$}
				child {node [circle,draw=white] (e2) {$\vdots$}}}}
		child {node [circle,draw=white] (3){$\bullet$}
			child {node [circle,draw=white] (b2) {$\bullet$}
				child {node [circle,draw=white] (c4) {$\vdots$}}
				child {node [circle,draw=white] (c6) {\textgreen{$\checkmark$}}}}
			child {node [circle,draw=white] (b4) {$\bullet$}
				child {node [circle,draw=white] (c5) {\textred{$\times$}}}
				child {node [circle,draw=white] (c7) {$\vdots$}}}};
		\end{tikzpicture}
		\\
		\end{tabular}
		\caption{Différence d'exécution entre une machine de Turing déterministe (à gauche) et d'une machine de Turing non-déterministe (à droite).}
		\label{fig:MT-exec}
	\end{minipage}
\end{figure}



