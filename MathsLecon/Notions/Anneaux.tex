%DEV Berlekamp

Nous allons rappeler les résultats majeurs pour l'études des anneaux commutatifs \cite[p.236]{BeckMalikPeyre} (comme la construction de morphismes d'anneaux, les propriétés de divisibilités, les anneaux euclidiens, $\dots$).
	
\subparagraph{Morphismes d'anneaux} Nous commençons par donner quelques énoncés permettant de construire des morphismes d'anneaux.
	
\begin{prop}[Propriété universelle de $\Z$]
	Soit $A$ un anneau unitaire (non nécessairement commutatif); il existe un unique morphisme d'anneau unitaires de $\Z$ dans $A$. Ce morphisme $\varphi$ est donné par:
	\begin{displaymath}
		\begin{array}{cccl}
		\varphi : & \Z & \to & A \\
		& n & \mapsto & n1_A\\
		\end{array}
	\end{displaymath}
\end{prop}
	
Ce résultat permet de définir la caractéristique d'un anneau unitaire: c'est l'unique entier $n \in \N$ tel que $\ker \phi = n \Z$. Nous allons maintenant étudier les morphismes issues de quotient.
	
\begin{theo}[Anneau quotient]
	Soient $A$ un anneau commutatif unitaire et $I$ un idéal de $A$. Il existe sur $A/I$ une unique structure d'anneau (unitaire) qui fasse de $\pi$ un morphisme d'anneaux (unitaires).
		
	Par ailleurs, pour tout anneaux $B$ et tout morphisme d'anneaux $\varphi: A \to B$ tel que $\phi(I) = \{0\}$ \textblue{($I \subset \ker \phi$)}, il existe un unique morphisme d'anneaux $\tilde{\varphi} : A/I \to B$ tel que $\varphi = \tilde{\varphi} \circ \pi$. Ajoutons que $Im \tilde{\varphi} = Im \varphi$ et $\ker \tilde{\varphi} = (\ker \varphi)/I$. En particulier, si $I = \ker \varphi$ alors $\tilde{\varphi}$ réalise un isomorphisme entre $A/I$ et $Im \varphi$.
\end{theo}
	
\begin{req}
	L'anneau $B$ (contrairement à l'anneau $A$) n'est pas supposé commutatif.
\end{req}
	
Pour construire un morphisme d'anneaux issu d'un anneau quotient $A/I$, il suffit de "faire passer au quotient" un morphisme $\phi$ issue de $A$ vérifiant $\phi(I) = \{0\}$. Définissons maintenant des morphismes d'anneaux sur les anneaux de polynômes.
	
\begin{theo}[Propriété universelle des polynômes]
	Soient $A$ et $B$ deux anneaux commutatifs, $\varphi: A \to B$ un morphisme d'anneaux et $i: A \to A\left[X_1, \dots, X_n\right]$ l'inclusion canonique. Pour tout $\left(b_1, \dots, b_n\right) \in B^n$, il existe un unique morphisme d'anneau $\phi: A\left[X_1, \dots, X_n\right]$ vérifiant les propriétés suivantes:
	\begin{itemize}
		\item $\phi(i(a)) = \phi(a)$ pour tout $a \in A$, c'est-à-dire $\phi \circ i = \varphi$;
		\item $\phi(X_j) = b_j$, pour tout $j \in \llbracket 1, n \rrbracket$.
	\end{itemize}
\end{theo}
	
\begin{req}
	Le cas où $B$ n'est pas commutatif est plus délicat mais le résultat (du moins son esprit) reste vrai.
\end{req}
	
Pour construire un morphisme d'anneaux issu d'anneau de polynômes, il suffit de le définir sur l'anneau sous-jacent et ses indéterminées.
	
\subparagraph{Anneaux euclidien} Nous allons donner la définition des anneaux euclidien ainsi que quelques exemples de tels anneaux.
	
\begin{definition}[Anneau euclidien]
	Un anneau $A$ est dit euclidien si $A$ est intègre, et s'il existe une application $\varphi: A \setminus \{0\} \to \N$ vérifiant
	\begin{displaymath}
		\forall (a, b) \in A \times A \setminus \{0\}, \exists q, r \in A, a = bq + r \text{ et } r=0 \text{ ou } \varphi(r)< \varphi(b)
	\end{displaymath}
	L'application $\phi$ s'appelle un stathme euclidien.
\end{definition}
	
Les anneaux $k[X]$ où $k$ est un corps muni de l'application degré et $\Z$ muni de la valeur absolue sont des anneaux euclidien. Il est également important de noté que les anneaux euclidiens sont des anneaux principaux (mais la réciproque est fausse). Cependant les anneaux euclidien apporte des algorithmes (par exemple pour les coefficients de Bézout), le calcul explicite de ces notions, qui n'existe pas dans les anneaux principaux.
	
\subparagraph{Divisibilité} Soit $A$ un anneau commutatif. Pour $a \in A$, on associe l'idéal principal $I = <\!\!\!a\!\!\!>$. Notons qu'un idéal principal à quant-à-lui plusieurs générateurs, il nous faut alors en choisir un ce qui revient à choisir les représentant des classes d'équivalence pour a relation de divisibilité.
	
\begin{definition}[Éléments premiers et irréductibles]
	Soit $A$ un anneau commutatif; un élément $a \in A\setminus\{0\}$ est dit \emph{premier} s'il vérifié l'une des deux propositions équivalentes suivantes:
	\begin{itemize}
		\item $a \notin A^{\times}$ et si pour $b, c \in A$, on a $a | bc$, alors $a | b$ ou $a | c$;
		\item l'idéal $<\!\!a\!\!>$ est un idéal premier.
	\end{itemize}
	Un élément $a \in A\setminus\{0\}$ est dit \emph{irréductible} s'il vérifié l'une des deux propositions équivalente suivantes:
	\begin{itemize}
		\item $a \notin A^{\times}$ et si pour $b, c \in A$, on a $a = bc$, alors $a \in A^{\times}$ ou $a \in A^{\times}$;
		\item l'idéal $<\!\!a\!\!>$ est maximal parmi les idéaux principaux de $A$ distincts de $A$/.
	\end{itemize}
\end{definition}
	
Les nuances de ces définitions dépendent de l'anneau que l'on considère.
\begin{description}
	\item[Si l'anneau est intègre] Un élément premier est nécessairement irréductible.
	\item[Si l'anneau est factoriel] Un anneau factoriel est intègre, donc par la remarque précédente, on a qu'un élément premier est irréductible. Cette notion donne une caractérisation des anneaux factoriels par ces éléments premiers et irréductibles.
	\begin{definition}[Anneau factoriel]
		Un anneau $A$ est factoriel si et seulement si $A$ est intègre, tout éléments de $A$ se décompose en produit d'éléments irréductibles dans $A$ et tout élément irréductibles dans $A$ est aussi premier dans $A$.
	\end{definition}
	\item[Si l'anneau est principal] Les éléments irréductibles et premiers coïncident. C'est la première étape pour montrer que les anneaux principaux sont factoriels.
\end{description}
	
Lorsque l'anneau que l'on étudie est factoriel, on sait que ces éléments admettent une décomposition en éléments irréductibles. La question naturelle est alors comment calculer cette décomposition: cette action est appelée factorisation.