%DEVs: simplicite
%LECONS: 104
Les sous-groupes distingués sont les sous-groupes qui permettent de définir les groupes quotients \cite[p.18]{Mercier}. En effet, lorsqu'on fait la quotient d'un groupe par un sous-groupe, le quotient est groupe si et seulement si le sous-groupe est distingué.

\begin{definition}[Sous-groupe distingué]
	Un sous-groupe $H$ de $G$ est distingué (ou normal) dans $G$ si $xH = Hx$ pour tout $x \in G$. On note alors $H \lhd G$.
\end{definition}

\begin{req}
	Les sous-groupes distingués peuvent être aussi défini à l'aide des automorphismes intérieurs.
\end{req}

\begin{prop}[Caractérisation des sous-groupes distingués]
	Soit $H$ un sous-groupe distingué de $G$. Les propriétés suivantes sont équivalentes.	
		
\begin{minipage}{.36\textwidth}	
		\begin{enumerate}
			\item $\forall x \in G$, $xH = Hx$
			\item $\forall x \in G$, $xH \subseteq Hx$
			\item $\forall x \in G$, $xHx^{-1} = H$
			\item $\forall x \in G$, $xHx^{-1} \subseteq H$
		\end{enumerate}
\end{minipage} \hfill
\begin{minipage}{.56\textwidth}
	{\footnotesize{\begin{proof}
				On a 1 qui est équivalent à 3 et 2 équivalent à 4. De plus 1 implique 2, montrons la réciproque. Comme $xH \subseteq Hx$ pour n'importe quel $x \in G$, alors $x^{-1}H \subseteq Hx^{-1}$. En particulier $x(x^{-1}H)x \subseteq x(Hx^{-1})x$ d'où l'inclusion manquante.
			\end{proof}}}
\end{minipage}
\end{prop}
		
\begin{theo}
	Soit $f : G \to G'$ un morphisme de groupes. Soient $H$ et $H'$ des sous-groupes respectifs de $G$ et de $G'$. Alors: 
	\begin{enumerate}
		\item $H' \lhd G' \Rightarrow f^{-1}(H') \lhd G$;
		\item $H \lhd G \Rightarrow f(H) \lhd f(G)$;
	\end{enumerate}
\end{theo}
{\footnotesize{\begin{proof}
	Soit $f : G \to G'$ un morphisme de groupes. Soient $H$ et $H'$ des sous-groupes respectifs de $G$ et de $G'$.
	\begin{enumerate}
		\item Soit $g \in G$, $x \in f^{-1}(H')$ et montrons que $gxg^{-1} \in f^{-1}(H')$. Comme $H' \lhd G'$, $f(gxg^{-1}) = f(g)f(x)f(g^{-1}) \in H'$ \textblue{($f$ est un morphisme de groupe)}. Donc, par définition de l'application inverse $gxg^{-1} \in f^{-1}(H')$.
		\item Soit $g \in G$, $h \in H$, montrons que $f(g)f(h)f(g)^{-1} \in f(H)$. Comme $f$ est un morphisme et que $H$ est distingué dans $G$, on obtient le résultat immédiatement.
	\end{enumerate}
\end{proof}}}

\noindent\begin{minipage}{.56\textwidth}
	\begin{lemme}
		Si $H$ et $K$ sont deux sous-groupes de $G$, alors :
		\begin{enumerate}
			\item si $H \lhd G$, alors $HK = KH$ est un sous-groupe de $G$;
			\item si $H \lhd G$ et $K \lhd G$, alors $HK \lhd G$.
		\end{enumerate}
	\end{lemme}
\end{minipage} \hfill
\begin{minipage}{.46\textwidth}
	{\footnotesize{\begin{proof}
				Soient $H$ et $K$ sont deux sous-groupes de $G$
				\begin{enumerate}
					\item Se montre en écrivant la définition de distingué.
					\item $\forall g \in G$, $gHK = HgK = HKg$.
				\end{enumerate}
			\end{proof}}}
\end{minipage}	
		
\begin{theo}[Deuxième théorème d'isomorphisme]
	Soit $H$ un sous-groupe distingué d'un groupe $G$, et $K$ un sous-groupe de $G$. Alors,
	\begin{enumerate}
		\item $K \cap H \lhd K$;
		\item $H \lhd KH$;
		\item $K/K \cap H \simeq KH / H$.
	\end{enumerate}
\end{theo}
{\footnotesize{\begin{proof}
	Soit $H$ un sous-groupe distingué d'un groupe $G$, et $K$ un sous-groupe de $G$.
	\begin{enumerate}
		\item Soit $h \in K \cap H$ et $k \in K$, montrons que $khk^{-1} \in K \cap H$. Dans $K$ par produit d'éléments de $K$ \textblue{($K$ est un groupe)}. Dans $H$ car $H$ est distingué dans $G$.
		\item On a $H \subseteq KH \subseteq G$ et $H$ distingué dans $G$, ce qui implique le résultat en revenant aux différentes définitions.
		\item L'application 
		\begin{tabular}{cccc}
			$f$ : & $K$ & $\to$ & $KH/H$ \\
			& $k$ & $\mapsto$ & $\dot{k}$\\
		\end{tabular}
		est un morphisme. Il est surjectif car un élément de $KH = HK$ est de la forme $hk$ et $\dot{\bar{hk}} = \dot{k}$ \textblue{(car $(hk)k^{-1} \in H$)}. Comme $\ker f = K \cap H$, on a bien le résultat (par décomposition canonique).
	\end{enumerate}
\end{proof}}}
		
\begin{theo}[Troisième théorème d'isomorphisme]
	Soit $H$ un sous-groupe distingué d'un groupe $G$. On note $\pi : G \to G/H$ la projection canonique.
	\begin{enumerate}
		\item Les sous-groupes distingués de $G/H$ sont de la forme $K/H$ où $K$ est un sous-groupe de $G$ tel que $H \subset K \lhd G$. L'application
		\begin{tabular}{cccc}
			$\Psi$ : & $\mathcal{G}$ & $\to$ & $\mathcal{G'}$ \\
			& $K$ & $\mapsto$ & $\pi(k) = K/H$\\
		\end{tabular}
		est une bijection croissante de l'ensemble $\mathcal{G}$ des sous-groupes distingués de $G$ contenant $H$ sur l'ensemble  $\mathcal{G'}$ des sous-groupes distingués de $G/H$.
		\item Si $H \subset K \lhd G$, alors $(G/H)/(K/H) \simeq G/K$.
	\end{enumerate}
\end{theo}
{\footnotesize{\begin{proof}
	Soit $H$ un sous-groupe distingué d'un groupe $G$. On note $\pi : G \to G/H$ la projection canonique.
	\begin{enumerate}
		\item L'application $\Psi$ est bien défini car $\pi(K)$ est un sous-groupe distingué comme image par un morphisme de groupe \textblue{(ici, on parle de $\pi$)} d'un sous-groupe distingué. Elle est surjective par surjectivité de $\pi$ et comme l'image réciproque d'un sous-groupe distinguée est distinguée. L'injectivité s'établie par double inclusion.
		\item  L'application 
		\begin{tabular}{cccc}
			$\psi$ : & $G/H$ & $\to$ & $G/H$ \\
			& $\dot{x}$ & $\mapsto$ & $\bar{x}$\\
		\end{tabular}
		est bien définie et sa décomposition canonique donne l'isomorphisme recherché.
	\end{enumerate}
\end{proof}}}
		
\begin{definition}[Groupe simple]
	Un groupe est dit simple s'il n'admet pas de sous-groupe distingué propre, c'est-à-dire autre de $\{id\}$ et lui-même.
\end{definition}

\begin{appli}[Troisième théorème d'isomorphisme aux groupes simples]
	Étant donné $H$ un sous-groupe distingué de $G$ maximal dans l'ensemble des sous-groupes distingué de $G$ (distinct de $G$), on montre (par le troisième théorème d'isomorphisme) que $G/H$ est simple. \textblue{(En effet, $K/H$ sera distingué dans $G/H$ si et seulement si $H \subseteq K \lhd G$, ce qui équivaut ici à $K = H$ ou $G$ soit $K/H = \{\dot{e}\}$ ou $G/H$.)} On a alors la relation suivante:	$G/H$ simple $\Leftrightarrow$ $H$ sous-groupe distingué maximal de $G$.
\end{appli}

\begin{theo}[Quotient d'un groupe \protect{\cite[p.150]{Calais}}]
	Soit $H$ un sous-groupe distingué de $G$, alors l'ensemble quotient $G \setminus H$ peut être muni d'une loi interne quotient induite par celle de $G$, telle que $\forall \overline{x}, \overline{y} \in G \setminus H$, $\overline{x}\overline{y} = \overline{xy}$. Muni de cette loi, $G \setminus H$ a une structure de groupe. 
\end{theo}

\begin{definition}
	Si $H$ un sous-groupe distingué de $G$, alors le groupe $G \setminus H$ est appelé groupe quotient.
\end{definition}