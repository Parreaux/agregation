%LECONs : 106 (groupe lineaire)
%DEVs : O(p,q)
Le principe diviser pour régner appairait aussi dans les mathématiques: les décompositions de Dunford ou polaire en sont des exemples intéressants \cite[p.347]{Caldero-Germoni}. La décomposition polaire nous permet de régner sur les groupes classiques, du moins topologiquement. 

\paragraph{Matrices définies positives} Soit $n$ un entier naturel. L'ensemble des matrices définies positives de taille $n \times n$ est $S_n^{++}(\R) = \{S \in GL_n(\R): ^tS =S; \forall x \in \R^n \setminus {0}, ^txSx > 0 \} = \{P^tP \in \mathcal{M}_n(\R) : P\in GL_n(\R) \}$. Pour $x \in \C^n$, on note $x^* = ^t\overline{x}$ ke vecteur adjoint de $x$; pour $H \in \mathcal{M}_n(\C)$, on note $M^* = ^t\overline{H}$, la matrice adjointe de $H$. L'ensemble des matrices hermitiennes définies positives $n \times n$ est : $H_n^{++}(\C) = \{H \in GL_n(\C): H^*=H; \forall x \in \C^n \setminus {0}, x^*Hx > 0 \} = \{PP^* \in \mathcal{M}_n(\C) : P\in GL_n(\C) \}$.

\begin{lemme}
	L'action de congruence (réelle) du groupe linéaire $GL_n(\R)$ sur $S_n^{++}(\R)$ permet de réaliser $S_n^{++}(\R)$ comme quotient de $GL_n(\R)$ par le groupe orthogonal $O_n(\R)$.
\end{lemme}
\begin{footnotesize}
	\begin{proof}
		Par le théorème de Sylvester (ou Gram-Schmidt), l'action est transitive. Par homéomorphisme, $S_n^{++}(\R)$ est le quotient de $GL_n(\R)$ par le stabilisateur de $I_n$ (le groupe orthogonal).
	\end{proof}
\end{footnotesize}

\paragraph{La décomposition polaire} Étudions maintenant cette décomposition.

\begin{definition}
	Une application $f$ est un homéomorphisme est une application bijective continue et de réciproque continue.
\end{definition}

\begin{theo}
	La multiplication matricielle induit des homéomorphismes :
	\begin{enumerate}
		\item $\mu : O_n(\R) \times S_n^{++}(\R) \overset{\simeq}{\to} GL_n(\R)$, $(O, S) \mapsto OS$;
		\item $\mu : \mathbb{U}_n(\C) \times H_n^{++}(\C) \overset{\simeq}{\to} GL_n(\C)$, $(U, H) \mapsto UH$.
 	\end{enumerate}
\end{theo}
\begin{footnotesize}
	\begin{proof}
		\begin{enumerate}
			\item \begin{itemize}
				\item $\mu$ est bien définie et continue
				\item $\mu$ est surjective : 
				\begin{itemize}
					\item $^tMM \in S_n^{++}(\R)$ \textblue{(action de congruence)}
					\item $^tMM$ est diagonalisable dans une base orthonormée de matrice diagonale réelle positive \textblue{(théorème spectral)}
					\item On pose $S$ qui est la "racine carré" de $^tMM$ \textblue{(car diagonale réelle positive)} symétrique positive \textblue{(matrice de passage orthogonale, définie positive dont ses coeffiscient diagonaux sont positifs)}.
					\item On pose $O = MS^{-1}$ et $^tOO = I_n$.
				\end{itemize}
				\item $\mu$ est injective : 
				\begin{itemize}
					\item On pose $M =OS = O'S'$: $S^2 = S'^2$.
					\item Soit $Q$ un polynôme tel que $Q(\lambda_i) = \sqrt{\lambda_i}$ \textblue{(polynôme de Lagrange)}: $S = Q(S^2) = Q(S'^2)$
					\item $S'$ commute avec $S'^2$ \textblue{(associativité)} $S'$ commute avec $Q(S'^2) = S$ \textblue{(manipulation de polynômes)}; $S$ et $S'$ sont diagonalisable \textblue{(théorème spectrale)} donc $S'$ et $S$ sont co-diagonalisable \textblue{(théorème de co-diagonalisation)}.
					\item Par le caractère de matrices symétriques: $S =S'$.
				\end{itemize}
				\item $\mu{-1}$ est continue: caractérisation de la continuité par suite et compacité de $O_n$. On conclut par injectivité de $\mu$.
			\end{itemize}
			\item Analogue
		\end{enumerate}
	\end{proof}
\end{footnotesize}

\paragraph{Application de la décomposition polaire}
\begin{cor}
	Pour toute matrice inversible de $\mathcal{M}_n(\R)$, on a $\mathopen{|||}A\mathclose{|||}_2 = \sqrt{\rho(^tAA)}$ où $\rho$ est le théorème spectral donné par $\rho(M)= \max_{1 \leq i \leq n} \{|\lambda_i| : \lambda_i \in \mathrm{Spect}(M)\}$.
\end{cor}
\begin{footnotesize}
	\begin{proof}
		$A = OS$: $\mathopen{|||}A\mathclose{|||}_2 = \mathopen{|||}S\mathclose{|||}_2$ \textblue{($\lVert OS(x) \rVert_2 = \lVert S(x) \rVert_2$} et $\mathopen{|||}S\mathclose{|||}_2 =  \rho(S)$ \textblue{(symétrique et diagonalisable)}.
	\end{proof}
\end{footnotesize}

\begin{cor}[Maximalité du groupe orthogonal]
	Tout sous-groupe compact de $GL_n(\R)$ qui contient le groupe orthogonal $O_n(\R)$ est le groupe $O_n(\R)$ lui-même.
\end{cor}
\begin{footnotesize}
	\begin{proof}
		On prend un élément de ce groupe, on lui applique la décomposition polaire. On conclut grâce à sa compacité et au corollaire précédent.
	\end{proof}
\end{footnotesize}

\begin{prop}[Maximalité du groupe orthogonal]
	Tout sous-groupe fini $G$ de $GL_n(\R)$ est conjugué à un sous-groupe de $O_n(\R)$.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		Il suffit de montrer que $G$ est le stabilisateur d'une forme quadratique définie positive $q$ : la forme quadratique euclidienne.
	\end{proof}
\end{footnotesize}

\paragraph{Une méthode de Newton pour la décomposition polaire}
Un algorithme permettant de calculer efficacement une décomposition polaire est basé sur une relation de récurrence que l'on fait converger rapidement. 

\begin{prop}
	La suite $\left(M_k\right)_{k \in \N}$ de $GL_n(\R)$ donné par $M_0 = M$ et $M_{k+1} = \frac{1}{2}M_k(I_n + (^tM_kM_k)^{-1})$ converge vers $O$, où $M =OS$ est la décomposition polaire de $M \in GL_n(\R)$. La suite $(^tM_kM)_k$ converge vers $S$. 
\end{prop}
\begin{footnotesize}
	\begin{proof}
		\begin{itemize}
			\item Par récurrence, on montre que $M_k \in GL_n(\R)$: congruence des matrices symétriques.
			\item Décomposition polaire pour $M_k = O_kS_k$ : montrons que $M_k$ tend vers $O$
			\item $O_{k+1} = O_k$ et $S_{k+1} = \frac{1}{2}(S_k+S_k^{-1})$: unicité de la décomposition polaire
			\item $S_k \to I_n$ et $O_k = O$ : diagonalisation simultanée entre $S_k$ et $S_k^{-1}$
		\end{itemize}
	\end{proof}
\end{footnotesize}

\paragraph{Complément sur la réduction des endomorphismes autoadjoints \cite[p.240]{Gourdon-algebre}}
\input{./../Notions/Reduction_autoadjoint.tex}

\paragraph{Complément sur la réduction simultanée \cite[p.240]{Gourdon-algebre}}
\input{./../Notions/Reduction_simultanee.tex}