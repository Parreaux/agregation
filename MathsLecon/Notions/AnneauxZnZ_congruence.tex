%LECONs : 120 (Anneaux Z/nZ); 121 (Nombres premier)
La structure naturelle de l'anneau $\Z / n\Z$ est celle déduit par le quotient induit par la relation des congruence \cite[p.60]{ElAmrani}. Nous allons étudier cette relation.

\subparagraph{Relation de congruence dans $\Z$} Nous allons étudier la notion de congruence et ces nombreuses applications à la notion de divisibilité.

\begin{definition}
	Soient $n \in \N$ et $(a, b) \in \Z^2$. On dit que $a$ est congru à $b$ modulo $n$ si $a - b$ est multiple de $n$. On note alors $a \equiv b [n]$.
\end{definition}

\begin{req}
	un entier est congru à $0$ modulo $n$ si et seulement si $a$ est divisible par $n$.
\end{req}

\begin{prop}
	Soit $n \in \N^*$. Deux entiers $a$ et $b$ sont congru modulo $n$ si et seulement si le reste de la division euclidienne de $a$ par $n$ est égal au reste de la division euclidienne de $b$ par $n$.
\end{prop}
{\footnotesize{\begin{proof}
	Par division euclidienne, $a = kn + r$ et $b = k'n + r'$ avec $k, k' \in \Z$, $0 \leq r < n$ et $0 \leq r' < n$. On a donc $a - b = (k - k')n + r - r' $ avec $0 \leq \left|r - r'\right| < n$. On en déduite que $a - b$ est un multiple de $n$ si et seulement si $r - r' = 0$. En d'autres termes, $a$ est congru à $b$ modulo $n$ si et seulement si $r = r'$.
\end{proof}}}

\begin{prop}
	La congruence modulo $n$ est une relation d'équivalence dans l'ensemble $\Z$.
\end{prop}
{\footnotesize{\begin{proof}
	\begin{description}
		\item[Réflexivité] Pour tout $a \in \Z$, $a - a =0$ et $0$ est multiple de $n$, donc $a \equiv a [n]$.
		\item[Symétrie] Si $a - b$ est multiple de $n$, alors $b -a$ l'est aussi (\textblue{on multiplie par $-1$}), donc $b \equiv a [n]$.
		\item[Transitivité] Si $a - b$ et $b - c$ sont multiples de $n$, alors $a - c = (a - b) + (b - c)$ l'est aussi, donc $a \equiv c [n]$.
	\end{description}
\end{proof}}}

\begin{prop}
	Soient $n \in \N^*$, $d \in \Z^*$ et $a, b \in \Z$.
	\begin{enumerate}
		\item Si $d$ divise $a, b, n$ et si $a \equiv b [n]$, alors $\frac{a}{d} \equiv \frac{b}{d} \left[\frac{n}{d}\right]$.
		\item Si $d$ divise $n$ et si $a \equiv b [n]$, alors $a \equiv b \left[d\right]$.
	\end{enumerate}
\end{prop}
{\footnotesize{\begin{proof}
	Soient $n \in \N^*$, $d \in \Z^*$ et $a, b \in \Z$.
	\begin{enumerate}
		\item Si $a \equiv b [n]$, alors $n$ divise $a -b$, donc il existe $k \in \Z$ tel que $a - b = kn$. Comme $d$ divise $a, b, n$, on peut écrire $a = da'$, $b = db'$, $n = dn'$ où $a', b', n'$ sont des entiers. On a alors: $da' - db' =kdn'$, et comme $d \neq 0$, on en déduit que $a' -b' = kn'$. Ceci prouve que $a' -b'$ est divisible par $n'$. 
		\item Si $a \equiv b [n]$, alors $n$ divise $a -b$, donc il existe $k \in \Z$ tel que $a - b = kn$. Comme $d$ divise $n$ on a $n = k'd$ où $k'$ est un entier. Donc $a - d = kk'd$, ce qui prouve que $d$ divise $a -b$.				
	\end{enumerate}
\end{proof}}}

\begin{definition}
	Pour tout $x \in \Z$, on note $\overline{x}$ la classe d'équivalence de $x$ modulo $n$, appelée plus simplement classe de $x$ modulo $n$. En d'autres termes: $\overline{x} = \{y \in \Z, y \equiv x [n]\} = \{x+ kn, k \in \Z\}$.
\end{definition}

\begin{ex}
	Pour $n = 3$, on a $\overline{5} = \{y \in \Z, y \equiv 5 [3]\} = \{5+ 3kn, k \in \Z\}$. Ainsi $-4 \in \overline{5}$ mais $1 \notin \overline{5}$.
\end{ex}

\begin{appli}
	Pour $n \in \N$, on note $\Z /n\Z = \{\overline{x}, x \in \Z \}$ où $\overline{x}$ es la classe de $x$ modulo $n$.
\end{appli}

\begin{prop}
	Soient $n \in \N^*$. L'ensemble $\Z/ n\Z$ des classes de congruence modulo $n$ contient $n$ éléments deux à deux distincts : $\Z/ n\Z = \{\overline{0}, \overline{1}, \dots, \overline{n-1} \}$.
\end{prop}
{\footnotesize{\begin{proof}
	Soit $a \in \Z$. La classe de $a$ est $\{a +nk, k \in \Z\}$. Si $r$ est le reste de la division de $a$ par $n$, alors $a \equiv r [n]$, donc $\overline{a} = \overline{r}$. Les différents restes possibles dans la division de $a$ par $n$ sont $0, 1, \dots, n-1$, donc les classes modulo $n$ sont $\overline{0}, \overline{1}, \dots, \overline{n-1}$. Ces classes sont deux à deux distinctes car des entiers compris entre $0$ et $n-1$ ne peuvent être congru modulo $n$ que s'ils sont égaux.
\end{proof}}}

\begin{ex}
	\begin{itemize}
		\item Les classes de congruences modulo $2$ sont l'ensemble des nombres pairs et l'ensemble des nombres impaires.
		\item Les classes de congruence modulo $3$ sont $3\Z$, $\{3k+1, k \in \Z\}$ et $\{3k+2, k \in \Z\}$.
	\end{itemize}
\end{ex}

\subparagraph{Règles de calcul pour les congruences.} Les opérations de $\Z$ induites sur $\Z/n\Z$ lui donne une structure d'anneaux. En effet, l'ensemble $n\Z$ est un idéal pour $\Z$ \textblue{($n\Z$ est un sous-groupe additif et $\forall(x, a) \in I \times A$, $ax \in I$ et $xa \in I$)}, d'où la structure d'anneaux de $\Z/n\Z$.