%DEVs : matrice diagonalisable
%LECONs : 151 (dimension et rang)
Nous allons maintenant présenter quelques résultats sur les sommes et les sommes directes de sous-espaces vectoriels \cite[p.21]{Grifone}.

\paragraph{Cas de deux sous-espaces vectoriel} Nous nous intéressons à ces notions pour deux sous-espaces. Nous en profiterons pour définir les sous-espaces supplémentaires.

\begin{definition}
	Soient $E_1$, $E_2$ deux sous-espaces vectoriels d'un espace vectoriel $E$. On appelle somme de $E_1$ et $E_2$, le sous-espace de $E$ défini par:
	\begin{displaymath}
	E_1 + E_2 = \{x \in E ~|~ \exists x_1 \in E_1, x_2 \in E_2 : x = x_1 + x_2\}
	\end{displaymath}
\end{definition}

\begin{req}
	Cette décomposition n'est à priori pas unique. Elle ne peut être unique que si $E_1 \cap E_2 = \{0\}$.
\end{req}

\begin{definition}
	Soient $E_1$, $E_2$ deux sous-espaces vectoriels d'un espace vectoriel $E$ et soit $\mathcal{E} = E_1 + E_2$. La décomposition de tout élément de $\mathcal{E}$ en somme d'un élément de $E_1$ et d'un élément de $E_2$ est unique, si et seulement si $E_1 \cap E_2 = \{0\}$. On écrit alors $\mathcal{E} = E_1 \oplus E_2$ : $\mathcal{E}$ est somme directe de $E_1$ et de $E_2$.
\end{definition}

\begin{req}
	\begin{displaymath}
	\mathcal{E} = E_1 \oplus E_2 \Leftrightarrow 
	\left\{ \begin{array}{l}
	\mathcal{E} = E_1 + E_2 \\
	E_1 \cap E_2 = \{0\} \\
	\end{array}\right. \Leftrightarrow 
	\left\{ \begin{array}{l}
	\mathcal{E} = E_1 + E_2 \\
	\text{décomposition unique des éléments de } \mathcal{E} \\
	\end{array}\right.
	\end{displaymath}
\end{req}

\begin{prop}
	Soit $E$ un espace vectoriel et $E_1$, $E_2$ deux sous-espaces vectoriels de $E$. Alors $E = E_1 \oplus E_2$ si et seulement si pour tout base $\mathcal{B}_1$ de $E_1$ et $\mathcal{B}_2$ de $E_2$, $\{\mathcal{B}_1, \mathcal{B}_2\}$ est une base de $E$.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		\begin{description}
			\item[$\Leftarrow$] Décomposition unique d'un vecteur dans une base: $x = x_1 + x_2$, $x_1 \in E_1$ et $x_2 \in E_2$.
			\item[$\Rightarrow$] Décomposition unique d'un vecteur dans une somme directe et décomposition unique dans leur base respective donne une décomposition unique dans la base recherchée.
		\end{description}
	\end{proof}
\end{footnotesize}

\begin{definition}
	Soient $E_1$, $E_2$ deux sous-espaces vectoriels d'un espace vectoriel $E$ et soit $\mathcal{E} = E_1 + E_2$. On dit que $E_1$ et $E_2$ sont supplémentaires si $E = E_1 \oplus E_2$.
\end{definition}

\begin{cor}
	Soit $E$ un espace vectoriel. Pour tout sous-espace vectoriel $E_1$, il existe toujours un supplémentaire. Ce supplémentaire n'est pas unique, mais si $E$ est de dimension finie, tous les supplémentaires de $E_1$ ont même dimension.
\end{cor}
\begin{footnotesize}
	\begin{proof}
		On raisonne en dimension finie : on applique le théorème de la base incomplète et la proposition précédente nous permet d'obtenir le dit supplémentaire. Comme le choix des vecteurs n'est pas unique, l'unicité n'est pas garantie. Cependant le nombre de vecteur choisi est toujours le même (base incomplète).
	\end{proof}
\end{footnotesize}

\begin{theo}
	Soit $E$ un espace vectoriel de dimension finie. Alors
	\begin{displaymath}
	E = E_1 \oplus E_2 \Leftrightarrow \left\{\begin{array}{l}
	E_1 \cup E_2 = \{0\} \\
	\dim E = \dim E_1 + \dim E_2 \\
	\end{array}\right.
	\end{displaymath}
\end{theo}
\begin{footnotesize}
	\begin{proof}
		\begin{description}
			\item[$\Rightarrow$] Propositions précédentes
			\item[$\Leftarrow$] On prend deux bases de $E_1$ et $E_2$ et on montre que la concaténation de ces deux bases forme une base pour $E$. 
		\end{description}
	\end{proof}
\end{footnotesize}

\begin{prop}
	Soit $E$ un espace vectoriel de dimension finie et $E_1$, $E_2$ deux sous-espaces vectoriels de $E$. On a 
	\begin{displaymath}
	\begin{array}{ll}
	\dim (E_1 + E_2) = \dim E_1 + \dim E_2 - \dim (E_1 \cap E_2) & \text{(Formule de Grassmann)} \\
	\dim (E_1 \oplus E_2) = \dim E_1 + \dim E_2 & \\
	\end{array}
	\end{displaymath}
\end{prop}
\begin{footnotesize}
	\begin{proof}
		On prend une base de $E_1 \cap E_2$ que l'on complète dans $E_1$ puis dans $E_2$. On prend un vecteur de $E_1 + E_2$ et on montre qu'on l'écrit avec ces deux bases : on prouve ainsi que c'est une base de $E$.
		
		Pour la somme directe, on applique la formule précédente avec $E_1 \cap E_2 = \{0\}$.
	\end{proof}
\end{footnotesize}

\paragraph{Cas de plusieurs sous-espaces} On généralise maintenant les concepts suivants dans le cas où on a plus de deux sous-espaces vectoriels.

\begin{definition}
	Soient $E_1, \dots, E_p$ des sous-espaces vectoriels d'un espace vectoriel $E$. On appelle somme de $E_1, \dots, E_p$, le sous-espace de $E$ défini par:
	\begin{displaymath}
	E_1 + \dots + E_p = \{x \in E ~|~ \exists x_1 \in E_1, \dots, x_p \in E_p : x = x_1 + \dots + x_p\}
	\end{displaymath}
\end{definition}

\begin{prop}
	Si $\mathcal{G}_1, \dots, \mathcal{G}_p$ sont des familles génératrices respectivement de $E_1, \dots, E_p$, alors $\{\mathcal{G}_1, \dots, \mathcal{G}_p\}$ est une famille génératrice de $E_1 + \dots + E_p$.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		Décomposition de l'élément par la somme puis par la combinaison linéaire des familles génératrices : combinaison linéaire pour l'élément.
	\end{proof}
\end{footnotesize}

\begin{definition}
	Soient $E_1, \dots, E_p$ dessous-espaces vectoriels d'un espace vectoriel $E$. On dit qu'ils sont en somme directe si tout vecteur $\mathcal{E} = E_1 + \dots + E_p$ se décompose d'une manière unique en somme d'un vecteur de $E_1$, $\dots$, d'un vecteur de $E_p$. On écrit alors $\mathcal{E} = E_1 \oplus \dots \oplus E_p$.
\end{definition}

\begin{prop}
	Soit $E$ un espace vectoriel et $E_1, \dots, E_p$ des sous-espaces vectoriels de $E$. Alors $E = E_1 \oplus \dots \oplus E_p$ si et seulement si pour tout base $\mathcal{B}_1, \dots, \mathcal{B}_p$ de $E_1, \dots, E_p$, $\{\mathcal{B}_1, \dots, \mathcal{B}_p\}$ est une libre.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		\begin{description}
			\item[$\Leftarrow$] Généralisation du cas de deux sous-espaces vectoriels.
			\item[$\Rightarrow$] Par unicité de la décomposition des éléments d'une somme directe et par le fait qu'une base est une famille libre.
		\end{description}
	\end{proof}
\end{footnotesize}

\begin{req}
	Bien distinguer : les $E_i$ sont en sommes directes et $E$ est somme directe des $E_i$ \textblue{(dans ce cas, la somme directe des $E_i$ "remplie" $E$ tout entier)}.
\end{req}

\begin{cor}
	Si $E$ est de dimension finie : $\dim (E_1 \oplus \dots \oplus E_p) = \dim E_1 + \dots + \dim E_p$
\end{cor}

\begin{cor}
	Soit $E$ un espace vectoriel de dimension finie. Alors
	\begin{displaymath}
	E = E_1 \oplus \dots \oplus E_p \Leftrightarrow \left\{\begin{array}{l}
	E_1 + \dots +  E_p = E \\
	\dim E = \dim E_1 + \dots + \dim E_p \\
	\end{array}\right.
	\end{displaymath}
\end{cor}

\begin{theo}
	Les sous-espaces $E_1, \dots, E_p$ sont en somme directe, si et seulement si $E_1 \cap E_2 = \{0\}$, $(E_1 + E_2) \cap E_3 = \{0\}$, $\dots$, $(E_1 + \dots + E_{p-1}) \cap E_p = \{0\}$.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		On utilise la décomposition unique des éléments.
	\end{proof}
\end{footnotesize}

\begin{req}
	Cette condition est minimale: des conditions plus faibles ne suffisent pas à montrer la somme directe.
\end{req}