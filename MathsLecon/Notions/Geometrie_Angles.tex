%LECONs : 101 (Actions de groupes); 182 (Angles)
Nous allons définir la notion d'angle. Nous manipulons des angles depuis toujours mais leur définition mathématiques n'est pas aisé \cite[p.312]{Garnier}. On utilise alors la notion d'action de groupe via le groupe orthogonal (constitué de rotations) pour définir cette notion.

\emph{Cadre}: On se place dans un plan vectoriel de dimension 2, $\Pev$, que nous orienterons le cas échéant \textred{(\textbf{Attention}: savoir si on a ou non une orientation est primordiale dans ce cadre)}. On note $O^+(\Pev)$ l'ensemble des applications orthogonales positives de $\Pev$ et muni de la loi de composition, cet ensemble est un groupe.

\paragraph{Angles orientés de demi-droites} Commençons par définir les angles pour des demi-droite. En leçon, on s'arretera ici généralement, mais le reste peut être intéressante en culture général. On note $\mathcal{D}$ l'ensemble des demi-droites de $\Pev$ et $\mathcal{U}$ l'ensemble des vecteurs unitaires.

\noindent\begin{minipage}{.46\textwidth}
	\begin{prop}
		L'ensemble $\mathcal{D}$ et $\mathcal{U}$ sont en bijection.
	\end{prop}
\end{minipage} \hfill
\begin{minipage}{.46\textwidth}
	\begin{footnotesize}
		\begin{proof}
			Bijection : $f : u \mapsto \{\lambda\overrightarrow{u}, \lambda \in \left[0, \infty\right]\}$
		\end{proof}
	\end{footnotesize}
\end{minipage}

\begin{theo}
	Le groupe $O^+(\Pev)$ agit de manière simplement transitive dans $\mathcal{U}$: pour tout couple $(\overrightarrow{u}, \overrightarrow{v})$ de $\mathcal{U}$, il existe une unique rotation vectoriel $l$ de $\Pev$ telle que $l(\overrightarrow{u}) = \overrightarrow{v}$.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		Le système donné par $l(\overrightarrow{u}) = \overrightarrow{v}$ où $l$ est sous forme matricielle admet une unique solution.
	\end{proof}
\end{footnotesize}

\begin{cor}
	Le groupe $O^+(\Pev)$ agit de manière simplement transitive dans $\mathcal{D}$.
\end{cor}
\begin{footnotesize}
	\begin{proof}
		Par la bijection entre $\mathbb{D}$ et $\mathcal{U}$.
	\end{proof}
\end{footnotesize}

\noindent\begin{minipage}{.46\textwidth}
	\begin{prop}
		Dans l'ensemble $\mathcal{D} \times \mathcal{D}$, la relation $\mathcal{R}$ définie par $(D_1, D'_1)\mathcal{R}(D_2, D'_2)$ si $\exists l \in O^+(\Pev)$ telle que $l(D_1) = D'_2$ et $l(D_1) = D'_2$ est une relation d'équivalence.
	\end{prop}
\end{minipage} \hfill
\begin{minipage}{.46\textwidth}
	\begin{footnotesize}
		\begin{proof}
			\begin{description}
				\item[Réflexive] Action simplement transitive
				\item[Symétrique] Équivalence dans la définition
				\item[Transitive] On prend la même transformation dans les deux cas
			\end{description}
		\end{proof}
	\end{footnotesize}
\end{minipage}

\begin{definition}
	L'ensemble quotient par cette relation d'équivalence se note $\mathcal{A}$ et c'st l'ensemble des angles de demi-droites vectorielles $\Pev$. La classe d'un couple est alors son angle. \textblue{(Cette relation d'équivalence caractérise les orbites de l'action.)}
\end{definition}

\begin{definition}
	L'angle de deux vecteurs $\overrightarrow{u}$ et $\overrightarrow{v}$ de $\Pev$ est celui des demi-droites qu'ils déterminent.
\end{definition}

\subparagraph{Quelques propriétés de $\mathcal{A}$} On a l'existence d'une bijection canonique entre $\mathcal{A}$ et $O^+(\Pev)$ ce qui nous permet de donner une structure de groupe à $\mathcal{A}$.

\noindent\begin{minipage}{.46\textwidth}
	\begin{theo}
		L'application $\psi$ de $O^+(\Pev)$ à $\mathcal{A}$ qui à $l \in O^+(\Pev)$ associe $\varphi = \mathrm{angle}(D, l(D))$ où $D \in \mathcal{D}$ est une bijection qui ne dépend pas de $D$.
	\end{theo}
\end{minipage} \hfill
\noindent\begin{minipage}{.46\textwidth}
	\begin{footnotesize}
		\begin{proof}
			La quantité $\mathrm{angle}(D, l(D))$ appartient à $\mathcal{A}$.
			
			L'action est simplement transitive: existence et unicité.
			
			L'angle est une classe d'équivalence : indépendante de $\mathcal{D}$.
		\end{proof}
	\end{footnotesize}
\end{minipage}

\begin{definition}
	Soit $\psi$ la bijection canonique de $O^+(\Pev)$ à $\mathcal{A}$ qui à $l \in O^+(\Pev)$ associe $\varphi = \mathrm{angle}(D, l(D))$, $\varphi$ est appelé angle de la rotation $l$.
\end{definition}

\noindent\begin{minipage}{.75\textwidth}
	\begin{theo}
		Soit $\varphi$ et $\varphi'$ deux éléments de $\mathcal{A}$, et soit $l$ et $l'$ les uniques éléments de $O^+(\Pev)$ vérifiant $\varphi = \psi(l)$ et $\varphi' = \psi(l')$, alors
		\begin{enumerate}
			\item la loi $+$ définie par $\varphi + \varphi' = \psi(l \circ l')$ définie une loi interne sur $\mathcal{A}$.
			\item l'ensemble $\left(\mathcal{A}, +\right)$ a une structure de groupe abélien de neutre $w$.
			\item les groupes $\left(O^+(\Pev), \circ\right)$ et $\left(\mathcal{A}, +\right)$ sont isomorphes.
		\end{enumerate}
	\end{theo}
\end{minipage}\hfil
\begin{minipage}{.2\textwidth}
	\begin{footnotesize}
		\begin{proof}
			\begin{enumerate}
				\item Les opérations sont bien définies.
				\item Vérifications immédiates.
				\item Par la bijection.
			\end{enumerate}
		\end{proof}
	\end{footnotesize}
\end{minipage}

\begin{definition}
	Le groupe $\left(\mathcal{A}, +\right)$ est le groupe des angles de demi-droites vectorielles de $\Pev$. Son élément neutre est l'angle nul.
\end{definition}

\begin{req}
	\textred{\textbf{Attention!}} il n'existe pas de groupe d'angles, mais à chaque plan vectoriel, on lui associe un groupe des angles. Pour deux plans différents, on obtient deux groupes isomorphes mais bien distincts.
\end{req}

\subparagraph{Propriétés des angles orientés de demi-droites} On donne ici quelques propriétés sur ces angles (que nous connaissons bien).

\noindent\begin{minipage}{.46\textwidth}
	\begin{prop}[Relation de Chasles]
		Soit les demi-droites vectorielles $D, D', D''$ de $\mathcal{D}$, alors $\left(\widehat{D, D'}\right) + \left(\widehat{D', D''}\right) = \left(\widehat{D, D''}\right)$.
	\end{prop}
\end{minipage}\hfill
\begin{minipage}{.46\textwidth}
	\begin{footnotesize}
		\begin{proof}
			On passe par la structure de groupe (on traduit tout dans un sens puis dans l'autre).
		\end{proof}
	\end{footnotesize}
\end{minipage}

\noindent\begin{minipage}{.6\textwidth}
	\begin{prop}
		Soient $D, D'\in\mathcal{D}$, $h \in O(\Pev)$.
		\begin{enumerate}
			\item Si $h \in O^+(\Pev)$, $\mathrm{angle}(h(D), h(D')) = \mathrm{angle}(D, D')$.
			\item Si $h \in O^-(\Pev)$, $\mathrm{angle}(h(D), h(D')) = -\mathrm{angle}(D, D')$.
		\end{enumerate}
	\end{prop}
\end{minipage}\hfill
\begin{minipage}{.38\textwidth}
	\begin{footnotesize}
		\begin{proof}
			\begin{enumerate}
				\item Commutativité de $O^+(\Pev)$
				\item On utilise une symétrie pour construire l'application inverse de $l$.
			\end{enumerate}
		\end{proof}
	\end{footnotesize}
\end{minipage}

\noindent\begin{minipage}{.55\textwidth}
	\begin{cor}
		Soit $l$ une rotation et $s$ une réflexion d'axe $\Delta$ d'un espace euclidien de dimension 2, alors $l^{-1} = s \circ l \circ s$.
	\end{cor}
\end{minipage} \hfill
\begin{minipage}{.42\textwidth}
	\begin{footnotesize}
		\begin{proof}
			Provient directement de la preuve du deuxième point de la proposition précédente.
		\end{proof}
	\end{footnotesize}
\end{minipage}

\begin{req}
	On obtient les même résultats avec des vecteurs (on considère les demi-droites qu'ils engendrent).
\end{req}


\noindent\begin{minipage}{.6\textwidth}
	\begin{prop}
		Pour tout $D_1, D_2, D_3, D_4 \in \mathcal{D}$, on a
		\begin{enumerate}
			\item $\left(\widehat{D_1, D_2}\right) = w \Leftrightarrow D_1 =D_2$
			\item $\left(\widehat{D_1, D_2}\right) = - \left(\widehat{D_1, D_2}\right)$
			\item $\left(\widehat{D_1, D_2}\right) = \left(\widehat{D_3, D_4}\right) \Leftrightarrow \left(\widehat{D_1, D_3}\right) = \left(\widehat{D_2, D_4}\right)$
		\end{enumerate}
	\end{prop}
\end{minipage} \hfill
\begin{minipage}{.4\textwidth}
	\begin{footnotesize}
		\begin{proof}
			\begin{enumerate}
				\item Par application de l'isomorphisme entre $\mathcal{A}$ et $O^+(\Pev)$
				\item Par les propriétés de la loi de groupe de $\mathcal{A}$
				\item Par la relation de Chasles
			\end{enumerate}
		\end{proof}
	\end{footnotesize}
\end{minipage}

\subparagraph{Définition du cosinus et du sinus}

\begin{definition}
	Soient $\mathcal{B} = \left(\overrightarrow{i}, \overrightarrow{j}\right)$ une base orthonormée directe de $\overrightarrow{\Pev}$, $\varphi \in \mathcal{A}$ et $l$ la rotation d'angle $\varphi$, on définit le cosinus et le sinus de l'angle $\varphi$ par $\cos \varphi = \cos l = a$ et $\sin \varphi = \sin l = b$ où $a = l\left(\overrightarrow{i}\right) . \overrightarrow{i}$ et $b = l\left(\overrightarrow{j}\right) . \overrightarrow{j}$.
\end{definition}
\begin{req}
	Un angle et une rotation sont parfaitement déterminés par leur cosinus et leur sinus.
\end{req}

\begin{prop}
	Soient $D, D' \in \mathcal{D}$ dans $\overrightarrow{\Pev}$ de vecteurs unitaires respectif $\overrightarrow{u}$ et $\overrightarrow{u'}$ et $\varphi = \left(\widehat{D, D'}\right)$, alors, $\cos \varphi = \overrightarrow{u}.\overrightarrow{u'}$ et $\sin \varphi = \overrightarrow{u}.\overrightarrow{v}$ où $v$ est l'unique vecteur unitaire orthogonal à $u$ tel qu'ils forment une base directe.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		On écrit la matrice de la rotation $l$ d'angle $\varphi$ dans cette base.
	\end{proof}
\end{footnotesize}

\subparagraph{Bissectrices : approche géométrique} On se place dans le plan $\Pev$ euclidien.
\begin{prop}
	L'application $g$ de $\mathcal{A}$ dans $\mathcal{A}$ qui à $\varphi$ associe $2\varphi = \varphi + \varphi$ est un morphisme surjectif de groupe ayant $\{w,\overline{w}\}$ comme noyau.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		\begin{itemize}
			\item Commutativité de $\mathcal{A}$ implique que $g$ est un morphisme.
			\item Surjectivité: résolution d'équation
			\item Noyau: définition des angles plats
		\end{itemize}
	\end{proof}
\end{footnotesize}

\noindent\begin{minipage}{.6\textwidth}
	\begin{cor}
		Les groupes $\left(\frac{\mathcal{A}}{\{w, \overline{w}\}}\right)$ et $\left(\mathcal{A},+\right)$ sont isomorphes.
	\end{cor}
\end{minipage} \hfill
\begin{minipage}{.4\textwidth}
	\begin{footnotesize}
		\begin{proof}
			Premier théorème d'isomorphisme.
		\end{proof}
	\end{footnotesize}
\end{minipage}

\begin{prop}
	Soit $D, D' \in \mathcal{D}$, alors il existe exactement $d_1, d_2 \in \mathcal{D}$ telles que $\mathrm{angle}(D, d_i) = \mathrm{angle}(d_i, D')$ pour $i \in \{i, 2\}$. De plus, $d_1$ et $d_2$ sont opposées.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		Par l'application $g$, il existe deux angles qui découpe en deux l'angle des deux demi-droites. On utilise Chasles pour faire apparaître les angles que l'on souahite. Puis on applique la rotation vectoriel $-Id$.
	\end{proof}
\end{footnotesize}

\begin{definition}
	Soient $D, D' \in \mathcal{D}$ et $d_1, d_2 \in \mathcal{D}$ définies comme dans la proposition précédente, alors la droite $d_1 \cup d_2$ est la bissectrice de l'angle $\left(\widehat{D, D'}\right)$ et de l'angle $\left(\widehat{D', D}\right)$.
\end{definition}

\begin{prop}
	Soient $D, D' \in \mathcal{D}$, alors pour toute $d \in \mathcal{D}$, on a $\mathrm{angle}(D, d) = \mathrm{angle}(d, D')$ si et seulement si $D'$ est l'image de $D$ par la symétrie orthogonale vectorielle de base contenant $d$.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		\begin{description}
			\item[$\Leftarrow$] On utilise la symétrie pour montrer l'égalité des angles.
			\item[$\Rightarrow$] On exhibe cette symétrie étudiant son impact sur les angles.
		\end{description}
	\end{proof}
\end{footnotesize}

\begin{cor}
	La bissectrice de $(D, D') \in \mathcal{D}^2$ est la base de l'unique symétrie orthogonale vectorielle axiale de $\Pev$ échangeant $D$ et $D'$.
\end{cor}

\begin{footnotesize}
	\textred{\textbf{Attention}: }\textblue{La définition de bissectrice est délicate lors de la leçon : on peut utiliser sa définition ou ce dernier corollaire.}
\end{footnotesize}

\paragraph{Mesure des angles} Depuis toujours, nous mesurons les angles (à l'aide d'un rapporteur). Nous allons voir comment définir une mesure sur l'objet d'angle que nous venons de définir.

\begin{theo}
	L'application $x \mapsto e^{ix} = \cos x +i\sin x$ de $\R$ dans $\mathbb{U}$ des nombres complexes de module $1$ \textblue{(fonction d'enroulement)} est un morphisme surjectif du groupe $(\R, +)$ dans le groupe $(\mathbb{U, \times})$. Le noyau de ce morphisme est $2\pi\Z$.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		\begin{itemize}
			\item Morphisme : propriétés de l'exponentielle
			\item Surjectivité : surjectivité de l'exponentielle
			\item Noyau : zéros de l'exponentielle
		\end{itemize}
	\end{proof}
\end{footnotesize}

\begin{theo}[Théorème fondamental de la mesure des angles]
	Les groupes suivants sont isomorphes $\left(\mathcal{A}, +\right)$, $\left(O^+\left(\overrightarrow{\Pev}\right), \circ\right)$, $\left(O^+\left(2, \R\right), \times\right)$, $\left(\mathbb{U}, \times\right)$, $\left(\frac{\R}{2\pi\Z}, \dot{+}\right)$.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		\begin{itemize}
			\item $\left(\mathcal{A}, +\right)$ et $\left(O^+\left(\overrightarrow{\Pev}\right), \circ\right)$: ok par $\varphi$
			\item $\left(O^+\left(\overrightarrow{\Pev}\right), \circ\right)$ et $\left(O^+\left(2, \R\right), \times\right)$: par les réels qui sorte lors de l'utilisation d'une représentation matricielle avec une base orthonormée directe
			\item $\left(O^+\left(2, \R\right), \times\right)$ et $\left(\mathbb{U}, \times\right)$: identifiant les coefficient de la représentation matricielle à $\cos$ et à $\sin$.
			\item $\left(\mathbb{U}, \times\right)$ et $\left(\frac{\R}{2\pi\Z}, \dot{+}\right)$: application définie précédemment.
		\end{itemize}
	\end{proof}
\end{footnotesize}

\begin{definition}
	La classe d'équivalence $\dot{\theta}$ de $(1)$ s'appelle la mesure de l'angle $\varphi$.
\end{definition}

\paragraph{Angles de droites} On définie les angles de la même manière, mais à la place de faire agir $O^+(\Pev)$ sur l'ensemble des demi-droites, on fait agir $\frac{O^+(\Pev)}{\{Id, -Id\}}$ sur l'ensemble des droites. On définit, de manière analogue, une relation d'équivalence sur l'ensemble de couple de droite dont les classes d'équivalences vont données le groupe des angles des droites de $\Pev$. On a également un théorème de mesure des angles.

\begin{theo}[Théorème fondamental de la mesure des angles]
	Les groupes suivants sont isomorphes $\left(\mathcal{A}, +\right)$, $\left(\frac{O^+\left(\overrightarrow{\Pev}\right)}{\{Id, -Id\}}, \circ\right)$, $\left(\frac{\R}{\pi\Z}, \dot{+}\right)$.
\end{theo}

\paragraph{Application aux angles géométriques}
\begin{definition}
	Soient trois points $O, A, B$ distincts deux à deux dans le plan euclidien orienté.
	\begin{enumerate}
		\item Si $O, A, B$ sont alignés, on dit que l'angle géométrique de sommet $O$ et dirigé par $\overrightarrow{OA}$ et $\overrightarrow{OB}$ est nul si $O \notin ]A; B[$ et plat sinon.
		\item Si $O, A, B$ ne sont pas aligné, alors $\alpha \in ]0, 2\pi[$ est la mesure de l'angle $\left(\overrightarrow{OA}, \overrightarrow{OB}\right)$. On appelle angle géométrique de sommet $0$ et dirigé par $\overrightarrow{OA}$ et $\overrightarrow{OB}$ l'angle définit par un couple de demi-droite de $\{\left(\overrightarrow{OA}, \overrightarrow{OB}\right), \left(\overrightarrow{OB}, \overrightarrow{OA}\right)\}$ qui a pour mesure $\min(\alpha, 2\pi- \alpha)+2\pi\Z$.
		\item On appelle mesure de l'angle géométrique de sommet $O$ et dirigé par $\overrightarrow{OA}$ et $\overrightarrow{OB}$ le réel $\theta = \arccos \frac{\overrightarrow{OA}.\overrightarrow{OB}}{||\overrightarrow{OA}|| . ||\overrightarrow{OB}||} \in [0, \pi]$.
	\end{enumerate}
\end{definition}

\begin{prop}
	La somme des angles géométrique d'un triangle du plan affine orienté est de mesure $\pi$.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		On considère les rotations vectorielles des trois angles du triangle, puis on calcul l'angle de celle résultant de la composition des trois.
	\end{proof}
\end{footnotesize}

\begin{prop}
	Soit $ABC$ un triangle rectangle en $A$. Alors $\cos \widehat{B} = \frac{AB}{AC} = \sin \widehat{C}$ et $\cos \widehat{C} = \frac{AC}{BC} = \sin \widehat{B}$.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		On applique Pythagore et on développe les quantités qui vont bien.
	\end{proof}
\end{footnotesize}

