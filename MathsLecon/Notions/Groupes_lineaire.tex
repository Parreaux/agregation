%LECONs : 106 (groupe lineaire); 108 (genrateur)
Soit $K$ est un corps et $E$ un $K$-espace vectoriel de dimension finie $n$. On définit le groupe linéaire et le groupe spécial linéaire, ses générateurs \cite[p.95]{Perrin}. On donne également quelques exemples de sous-groupes.

\begin{definition}
	Le groupe linéaire $GL(E)$ est le groupe des $K$-automorphismes de $E$ (des $K$-applications linéaires bijectives de $E$ dans $E$).
\end{definition}

\begin{req}
	Il existe un isomorphisme de $GL(E)$ dans $GL_n(K)$ non canonique (il dépend de la base choisi). On peut alors utiliser l'outil matriciel pour étudier $GL(E)$.
\end{req}

\begin{prop}[Caractérisation de $GL(E)$ \protect{\cite[p.115]{Gourdon-algebre}}]
	Soit $u$ une application linéaire de $E$ dans $E$. On a les équivalences suivantes:
	
	\begin{minipage}{.36\textwidth}
		\begin{enumerate}
			\item $u \in GL(E)$
			\item $u$ est injective
			\item $u$ est surjective
			\item $\det u \neq 0$
			\item $u$ envoie une base sur une base
		\end{enumerate}
	\end{minipage} \hfill
	\begin{minipage}{.6\textwidth}
		\begin{footnotesize}
			\begin{proof}
				\begin{description}
					\item[$1 \Rightarrow 2$] définition
					\item[$2 \Rightarrow 3$] théorème du rang
					\item[$3 \Rightarrow 4$] pas de base telle que la matrice de $u$ contienne $2$ vecteurs colinéaires
					\item[$4 \Rightarrow 5$] sinon, il existerait une base telle que la matrice de $u$ contienne $2$ vecteurs colinéaires et $\det u = 0$
					\item[$1 \Rightarrow 2$] définition
				\end{description}
			\end{proof}
		\end{footnotesize}
	\end{minipage}
\end{prop}

\begin{prop}[\protect{\cite[p.208]{Nourdin}}]
	Soient $m$ et $n$ deux entiers $\geq 1$. $GL_n(\C)$ et $GL_m(\C)$ sont isomorphes (en tant que groupe) si et seulement si $m = n$.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		On cherche à caractériser $n$ à l'aide des sous-groupes abéliens finis de $GL_n(\C)$. Soit $H$ un sous-groupe abélien de $GL_n(\C)$ d'ordre $r$ et tel que $\forall M \in G$, $M^2 = I_n$. Comme $H$ est abélien et que tous ses éléments sont diagonalisables, ils sont codiagonalisable. On en déduit que $r= 2^n$. Donc si $GL_n(\C)$ et $GL_m(\C)$ sont isomorphes, ils ont le même $r$ et $m = n$.
	\end{proof}
\end{footnotesize}


\begin{prop}
	L'application déterminant est un homomorphisme (multiplicatif) de $GL(E)$ dans $K^*$.
\end{prop}

\begin{definition}
	Le noyau de l'application déterminant est le groupe spécial linéaire, noté $SL(E)$.
\end{definition}

\begin{req}
	Comme pour le groupe $GL(E)$, le groupe $SL(E)$ est isomorphe au groupe $SL_n(K)$ correspondant aux matrices de déterminant $1$.
\end{req}

\begin{req}
	Le sous-groupe $SL(E)$ est distingué dans $GL(E)$. En effet, soit $h \in SL(E)$ et $g \in GL(E)$, on a $\det (ghg^{-1}) = (\det g)(\det h)(\det g^{-1}) = (\det g)(\det h)(\det g)^{-1} = \det h = 1$ \textblue{(propriétés du déterminant)}, donc $ghg^{-1} \in SL(E)$.
\end{req}

\paragraph{Générateurs de ces groupes} Comme toujours, on cherche les générateurs les plus simples possibles. Ici, ce sont des hyperplans.

\begin{prop}[Définition des dilatations]
	Soit $H$ un hyperplan de $E$ et $u \in GL(E)$ tel que $u|_H = Id_H$. Les conditions suivantes sont équivalentes:
	
	\begin{minipage}{.46\textwidth}
		\begin{enumerate}
			\item on a $\det u = \lambda \neq 1$ (donc $u \notin SL(E)$)
			\item $u$ admet une valeur propre $\lambda \neq 1$ (donc une droite propre $D$ pour $\lambda$) et $u$ est diagonalisable
			\item on a $\mathrm{Im}(u-Id) \nsubseteq H$
			\item il existe une base telle que $Mat_u = \mathrm{Diag}(1, \dots, 1, \lambda)$ avec $\lambda \in K^*$ et $\lambda \neq 1$.
		\end{enumerate}
	\end{minipage} \hfill
	\begin{minipage}{.46\textwidth}
		\begin{footnotesize}
			\begin{proof}
				\begin{description}
					\item[$4 \Rightarrow 1$] calcul du déterminant
					\item[$1 \Rightarrow 2$] $u$ admet la valeur propre $1$ d'ordre $n-1$ avec l'hyperplan $H$
					\item[$2 \Rightarrow 3$] définition d'un vecteur propre
					\item[$3 \Rightarrow 4$] construction de la base : $n-1$ vecteurs de la base de $H$ et $1$ vecteur n'appartenant pas à $H$ faisant ressortir la valeur $\lambda$.
				\end{description}
			\end{proof}
		\end{footnotesize}
	\end{minipage}
	
	On dit que $u$ est une dilatation d'hyperplan $H$, de droite $D$, de rapport $\lambda$. On a alors $D= \mathrm{Im}(u - Id)$ et $H = \ker(u - Id)$.
\end{prop}

\begin{req}
	Une dilatation de rapport $\lambda$ sur un corps de caractéristique différente de $2$ est une réflexion.
\end{req}

\begin{prop}
	Deux dilatations sont conjuguées dans $GL(E)$ si et seulement si elles ont le même rapport.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		Elles ont la même matrices dans des bases convenables.
	\end{proof}
\end{footnotesize}


\begin{prop}[Définition des transvections]
	Soit $H$ un hyperplan de $E$ d'équation $f \in E^*$. Soit $u \in Gl(E)$, $u \neq Id$ tel que $u|_H = Id_H$. Les conditions suivantes sont équivalentes:
	
	\begin{minipage}{.56\textwidth}
		\begin{enumerate}
			\item on a $\det u = 1$ (donc $u \in SL(E)$)
			\item $u$ n'est pas diagonalisable
			\item on a $D = \mathrm{Im}(u-Id) \subset H$
			\item l'homomorphisme induit, $\overline{u}: E / H \to E / H$, est l'identité de $E / H$
			\item il existe $a \in H$, $a \neq 0$ tel que l'on ait $\forall x \in E$, $u(x) = x + f(x)a$
			\item il existe une base telle que $Mat_u = \begin{pmatrix}
			1 & 0 & \\
			& \ddots & 1 \\
			& 0 & 1\\
			\end{pmatrix}$.
		\end{enumerate}
	\end{minipage} \hfill
	\begin{minipage}{.37\textwidth}
		\begin{footnotesize}
			\begin{proof}
				\begin{description}
					\item[$6 \Rightarrow 1$] calcul du déterminant
					\item[$1 \Rightarrow 2$] $u$ admet la valeur propre $1$ d'ordre $n-1$ avec l'hyperplan $H$
					\item[$2 \Rightarrow 3$] $x$ un vecteur propre : $u(x) - x = 0$ donc $x \in H$
					\item[$3 \Leftrightarrow 4$] $u(x) - x \in H$ si et seulement si $\overline{u}(\overline{x}) = \overline{x}$
					\item[$3 \Rightarrow 5$] $a \in H$, $x_0 \notin H$ tel que $f(x_0) = 1$ et $a = u(x_0) - x_0$ : deux applications qui coïncide sur $H$ est en $x_0 \notin H$.
					\item[$5 \Rightarrow 6$] on construit une base telle que $e_{n-1} = a$, $\{e_1, \dots, e_{n-1}\}$ forment une base de $H$ que l'on peut compléter avec $e_n$ tel que $f(e_n) = 1$.
				\end{description}
			\end{proof}
		\end{footnotesize}
	\end{minipage}
	
	On dit que $u$ est une transvection d'hyperplan $H$, de droite $D$. On a alors $D= (a)$ et $D \subset H$.
\end{prop}

\begin{req}
	La donnée de $H, D, \lambda$ est équivalente à la donnée de $u$ dans le cadre des dilatations. Cependant pour les transvections, ce n'est pas le cas: $u$ détermine $H$ et $D$ mais la réciproque est fausse. 
\end{req}

\begin{prop}[Caractérisation duale]
	Soit $a \in GL(E)$, $u \neq Id$. Les propriétés suivantes sont équivalentes:
	\begin{enumerate}
		\item $u$ est une transvection de droite $D$
		\item on a $u|_D = Id$ et l'homomorphisme induit $\overline{u}: E/D \to E/D$ est l'identité.
	\end{enumerate}
\end{prop}
\begin{footnotesize}
	\begin{proof}
		\begin{description}
			\item[$1 \Rightarrow 2$] par la caractérisation 5 des transvections
			\item[$2 \Rightarrow 1$] théorème du rang appliqué à $u - Id$ sachant que $\mathrm{Im}(u-Id) = D$.
		\end{description}
	\end{proof}
\end{footnotesize}

\begin{prop}[Comportement par conjugaison]
	Soit $\tau$ une transvection de droite $D$ et d'hyperplan $H$ et soit $u \in GL(E)$. Alors, $a \tau u^{-1}$ est une transvection de droite $u(D)$ est d'hyperplan $u(H)$.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		On applique la caractérisation 5.
	\end{proof}
\end{footnotesize}

\begin{prop}
	Deux transpositions quelconques sont conjugués dans $GL(E)$.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		Elles ont même réduction de Jordan.
	\end{proof}
\end{footnotesize}

\begin{prop}
	\begin{enumerate}
		\item Dans $SL_2(K)$ toute transvection est conjugué d'une matrice $\begin{pmatrix}
		1 & \lambda \\
		0 & 1\\
		\end{pmatrix}$, avec $\lambda \in K^*$.
		\item Soient $\lambda, \mu \in K^*$, alors $\begin{pmatrix}
		1 & \lambda \\
		0 & 1\\
		\end{pmatrix}$ et $\begin{pmatrix}
		1 & \mu \\
		0 & 1\\
		\end{pmatrix}$ sont conjugués dans $SL_2(K)$ si et seulement si $\frac{\lambda}{\mu}$ est un carré dans $K$.
		\item Si $n \geq 3$, toute transvections sont conjugués dans $SL_3(K)$.
	\end{enumerate}
\end{prop}
\begin{footnotesize}
	\begin{proof}
		\begin{enumerate}
			\item Par changement de bases
			\item Calcul matriciel
			\item Choisir $s = \mathrm{Diag}(1, \dots, 1, \lambda, \frac{1}{\lambda}, \frac{1}{\lambda})$ qui n'est possible que si $n \geq 3$
		\end{enumerate}
	\end{proof}
\end{footnotesize}

\begin{theo}
	Les transvections engendrent $SL(E)$.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		Se montre par récurrence sur $n$. Pour $n = 1$ ok.
		\begin{lemme}
			Soient $x, y \in E \setminus \{0\}$. Il existe une transvection $u$ ou un produit de transvections $uv$, tels que $u(x) = y$ ou $uv(x) = y$.
		\end{lemme}
		\begin{proof}
			$x$ et $y$ non colinéaire, on applique la caractérisation 5. Sinon, on choisi $z$ non colinéaire : cas précédent.
		\end{proof}
		On passe au quotient par la droite engendré par $x$ ($u(x) = x$). Puis on applique l'hypothèse de récurrence aux classes.
	\end{proof}
\end{footnotesize}

\begin{cor}
	Les dilatations et les transvections engendrent $GL(E)$.
\end{cor}
\begin{footnotesize}
	\begin{proof}
		Par le théorème: $u \in GL(E)$, alors il existe $v$ une dilatation (de rapport $(\det u)^{-1}$ telle que $vu \in SL(E)$.
	\end{proof}
\end{footnotesize}

\paragraph{Exemples de sous-groupes} On étudie ici quelques exemples de sous-groupes des groupes linéaire et linéaire spécial: le centre de ces groupes et les sous-groupes finis.

\begin{theo}
	Le centre $Z$ de $GL(E)$ est formé des homothéties $x \mapsto \lambda x$. Il est donc isomorphe à $K^*$.
	
	Le centre de $SL(E)$ est $Z \cap SL(E)$, il est isomorphe à $\mu_n(k) = \{\lambda \in K ~|~ \lambda^n = 1\}$.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		Le lemme et le comportement par conjugaison permettent de conclure.
		\begin{lemme}
			Soit $u \in GL(E)$. Supposons que $u$ laisse invariante toutes les droites vectorielles de $E$, alors $u$ est une homothétie.
		\end{lemme}
		\begin{proof}
			En formule, cela correspond à une inversion de quantificateur $\forall\exists \Rightarrow \exists\forall$. Par calcul dans le cas non colinéaire (évident sinon).
		\end{proof}
	\end{proof}
\end{footnotesize}

\begin{appli}
	Pour $n = 1$, $GL_n(K)$ est abélien et $SL_n(E) = \{1\}$. Pour $n \geq 2$, $GL_n(K)$ et $SL_n(K)$ ne sont pas abéliens.
\end{appli}

\begin{appli}
	Les groupes $GL(E)$ et $SL(E)$ ne sont pas simple ($n \geq 2$).
\end{appli}

\begin{req}
	Le quotient de $GL(E)$ par son centre est appelé le groupe projectif linéaire et est noté $PGL(E)$. De même, celui de $SL(E)$ par son centre est appelé le groupe projectif spécial linéaire et est noté $PSL(E)$.
\end{req}

\begin{theo}[Bunside \protect{\cite[p.185]{FrancinouGianellaNicolas-al2}}]
	Tout sous-groupe de $GL_n(\C)$ d'exposant fini est fini.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		Les matrices de $GL_n(\C)$ sont diagonalisable donc l'application $M \mapsto \mathrm{Tr}(AM)$ où $\mathrm{Tr(}A^k) = 0, \forall k \in \N^*$ est surjective.
	\end{proof}
\end{footnotesize}







