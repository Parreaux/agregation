%DEV: Weierstrass Bernstein

Nous définissons le module de continuité \cite[p.114]{ZuilyQueffelec} qui nous permet de quantifier à quel point la fonction est continue. Plus la notion de continuité est forte (comme la continuité uniforme vs la continuité simple), plus le module est faible.

\begin{definition}
	Un module de continuité est une application non nulle $\varphi : \R_+ \to \R_+$ telle que $\varphi(0) = 0$; $\varphi$ est continue croissante et $\varphi$ est sous-additive, i.e. $\varphi(t_1 + t_2) \leq \varphi(t_1) + \varphi(t_2)$, $\forall t_1, t_2$.
\end{definition}
	
\begin{req}
	Par récurrence montre que: $\varphi(rt) \leq r\varphi(t)$ $\forall r \in \N$ et $\forall t \in \R_+$. On obtient alors $\varphi(\lambda t) \leq (\lambda + 1) \varphi(t)$ $\forall \lambda \geq 0$ et $\forall t \in \R_+$ \textblue{(s'obtient de la croissance de $\varphi$)}.
\end{req}

\begin{lemme}
	Le module de continuité uniforme de $f$, $\omega$ vérifie les propriétés suivantes:
	\begin{enumerate}
		\item $\omega$ est croissante.
		\item $\forall h_1, h_2 \in [0, 1]$, $\omega(h_1 + h_2) \leq \omega(h_1) + \omega(h_2)$.
		\item $\forall h \in [0, 1]$, $\lambda \in \R_+$ tels que $h\lambda \in [0, 1]$, $_\omega(\lambda h) \leq (\lambda + 1)\omega(h)$
	\end{enumerate}
\end{lemme}
\begin{proof}
	On rappelle que nous avons définie le module de continuité uniforme de $f$ tel que $\omega(h) = \sup \{|f(u) - f(v)|; |u - v| \leq h\}$, $\forall h \in [0, 1]$.
	\paragraph{Montrons 1:} Soient $h_1, h_2 \in [0, 1]$ tels que $h_1 \leq h_2$ alors, on a : 
	\begin{displaymath}
	\{|f(u) - f(v)|; |u - v| \leq h_1\} \subseteq \{|f(u) - f(v)|; |u - v| \leq h_2\}
	\end{displaymath}
	\textblue{(car si $|u - v| \leq h_1$ alors $|u - v| \leq h_2$ et donc si $|f(u) - f(v)| \in \{|f(u) - f(v)|; |u - v| \leq h_1\}$ alors $|f(u) - f(v)| \in \{|f(u) - f(v)|; |u - v| \leq h_2\}$)}. D'où le résultat de croissance \textblue{(par croissance de la borne supérieure)}.
	
	\paragraph{Montrons 2:} Soient $h_1, h_2, u, v \in [0, 1]$ tels que $\mathopen{|} u - v \mathclose{|}
	\leq h_1 + h_2$. Alors, il existe $w \in [\min(u, v), \max(u, v)]$ tel que $\mathopen{|} u - w \mathclose{|} \leq h_1$ et $\mathopen{|} w - v \mathclose{|} \leq h_2$ \textmagenta{(utilisation de l'hypothèse sur $\mathopen{|} u - v \mathclose{|}$)} \textblue{(écrire la contraposée)}. On alors
	
	\begin{tabular}{ccll}
		$\mathopen{|}f(u) - f(v) \mathclose{|}$ & $\leq$ & $\mathopen{|}f(u) - f(w) \mathclose{|} + \mathopen{|}f(w) - f(v) \mathclose{|}$ & \textblue{(par inégalité triangulaire)} \\
		& $\leq$ & $\omega(h_1) + \omega(h_2)$ & \textblue{(par def de la borne supérieure)} \\
	\end{tabular}
	
	On conclut en passant au sup : $\omega(h_1 + h_2) \leq \omega(h_1) + \omega(h_2)$ \textmagenta{(utilisation de l'hypothèse sur $\mathopen{|} u - v \mathclose{|}$)}.
	
	\paragraph{Montrons 3:} D'après le point précédent \textblue{(par récurrence)}, on a, pour tout $n \in \N$ et $h \in [0, 1]$, $\omega(nh) \leq n\omega(h)$. Soit $\lambda \in \R_+$ tel que $h\lambda \in [0, 1]$. On a alors $\lfloor \lambda \rfloor  \leq \lambda \leq \lfloor \lambda \rfloor + 1$. On a donc,
	
	\begin{tabular}{ccll}
		$\omega(\lambda h)$ & $\leq$ & $\omega((\lfloor \lambda \rfloor + 1)h)$ & \textblue{(par croissance de $\omega$ et $\lambda \leq \lfloor \lambda \rfloor + 1$)} \\
		& $\leq$ & $(\lfloor \lambda \rfloor + 1)\omega(h)$ & \textblue{(par la remarque précédente comme $(\lfloor \lambda \rfloor + 1) \in \N$)} \\
		& $\leq$ & $(\lambda + 1)\omega(h)$ & \textblue{(par $\lfloor \lambda \rfloor  \leq \lambda$)} \\
	\end{tabular}
	D'où le lemme.
\end{proof}
