%LECON: 239

Les résultats de régularité sous le signe de l'intégrale se déduisent du théorème de convergence dominée \cite[p.299]{ZuilyQueffelec}.

\subparagraph{Cadre} Soit $(X, d)$ un espace métrique, on souhaite étudier la régularité de la fonction $F: x \mapsto \int_{E} f(x, t) d\mu(t)$ en fonction de celle de $f$.

\begin{theo}[Continuité sous le signe intégral]
	Si $f$ vérifie:
	\begin{enumerate}
		\item $\forall x \in X$, $t \mapsto f(x, t)$ est mesurable;
		\item $\mu$ pour presque tout $t \in E$, $x \mapsto f(x, t)$ est continue $x_0$;
		\item il existe une fonction $g \in L^{1}_{\R_+}$ et indépendante de $x$ telle que $|f(x, t)| \leq g(t)$, $\forall x \in X$, presque partout en $t$.
	\end{enumerate}
	Alors, la fonction $F$ est continue en $x_0$.
\end{theo}
\begin{proof}
	Caractérisation séquentielle de la continuité et application du théorème de convergence dominée
\end{proof}

\begin{req}
	Si le théorème est vrai en tout $x_0$ \textblue{(c'est-à-dire que $f$ est continue sur tout $X$)}, alors $F$ est continue sur tout $X$.
\end{req}

\begin{ex}
	Exemples de fonctions définies par une intégrale continues
	\begin{itemize}
		\item La fonction gamma d'Euler est bien définie: $\Gamma(x) = \int_{0}^{+ \infty}t^{x-1}e^{-t}dt$. 
		\item La transformée de Fourier d'une fonction $f \in L^{1}(\R)$ est une fonction bien définie et continue: $\hat{f}(x) =\int_{\R}e^{-itx}f(t)dt$. \cite[p.320]{ZuilyQueffelec}
	\end{itemize}
\end{ex}

\begin{cex}
	Les contre-exemples ay théorème \cite[p.224]{Hauchecorne}.
	\begin{itemize}
		\item $f(x, t) = \mathbb{1}_{\Q}(x)$ et $F(x)$ n'est pas continue \textblue{($f$ est majorée mais pas continue)}.
		\item $f(x, t) = xe^{-xt}$ et $F$ n'est pas continue en 0 \textblue{($f$ est continue mais ne peut pas être dominée en valeur absolue)}.
	\end{itemize}
\end{cex}

\begin{theo}[Dérivation sous le signe de l'intégrale]
	On suppose que $X$ est un intervalle ouvert de $\R$ et que $f$ vérifie:
	\begin{enumerate}
		\item $\forall x \in X$, $t \mapsto f(x, t)$ est dans $L^{1}(X)$;
		\item pour presque tout $t \in E$, $x \mapsto f(x,t)$ est dérivable sur $X$;
		\item il existe $g \in L^{1}_{\R_+}$ indépendante de $x$ telle $|\frac{\partial f}{\partial x} f(x, t)| \leq g(t)$.  
	\end{enumerate}
	Alors, $\forall x \in X$, $t \mapsto \frac{\partial f}{\partial x} f(x, t)$ est dans $L^{1}(X)$ et $F$ est dérivable sur $X$ de dérivée $F'(x) = \int \frac{\partial f}{\partial x} f(x, t)d\mu(t)$.
\end{theo}
\begin{proof}
	Caractérisation séquentielle de la limite du taux d'accroissement + théorème des accroissements finis + théorème de la convergence dominée.
\end{proof}

\begin{req}
	On a un résultat analogue en remplaçant dérivable par $\mathcal{C}^{k}$. \textblue{Se montre par récurrence sur le nombre de dérivations.}
\end{req}

\begin{ex}[Utilisation de la dérivation sous le signe de l'intégrale]
	Calcul de l'intégrale de Gauss: $\int_{0}^{+\infty}e^{-u^{2}}du = \frac{\sqrt{\pi}}{2}$.
\end{ex}

\begin{appli}
	$\phi(t) = \frac{1}{\sqrt{2\pi}}$ est la densité de Gauss.
\end{appli}

\begin{cex}
	Un contre-exemple au théorème: $f(x, t) = x^2e^{-t|x|}$ est $\mathcal{C}^{1}$ mais pas $F$.
\end{cex}

\begin{appli}[Transformée de Fourier \protect{\cite[p.142]{BriancePages}}]
	La transformée de Fourier est continûment dérivable et $\hat{f}'(u) = i\hat{xf(x)}(u)$. \textblue{La dérivation se transforme en multiplication par $x$ (utile pour résoudre des équations différentielles). Donc on voit apparaître le lien entre dérivation et régularité pour les transformées de Fourier.}
\end{appli}

\begin{theo}[Holomorphie sous le signe intégral]
	Soit $D$ un ouvert de $\C$ et $f: D \times E \to \C$. Supposons que 
	\begin{enumerate}
		\item $\forall z \in D$, $x \mapsto f(z, t)$ est mesurable;
		\item pour presque tout $t \in E$, $z \mapsto f(z, t)$ est holomorphe dans $D$;
		\item pour tout compact $K$ de $D$, il existe $g \in L^{1}_{R_+}$ indépendante de $z$ telle que $|f(z, t)| \leq g(t)$.
	\end{enumerate}
	Alors, la fonction $F$ est holomorphe dans $D$ et $F'(z) = \int \frac{\partial f}{\partial z} f(z, t)d\mu(t)$.
\end{theo}
\begin{proof}
	On montre que l'hypothèse 3 implique que pour tout compact $K$ de $D$, il existe $h \in L^{1}_{R_+}$ indépendante de $z$ telle que $|\frac{\partial f}{\partial z} f(z, t)| \leq g(t)$ en posant $K_{\delta} = \{z \in \C | d(z, K) \leq \delta \}$ et en utilisant la formule de Cauchy. Ensuite, on se remmène à la preuve du théorème de la dérivabilité sous le signe intégral.
\end{proof}

\begin{ex}
	La fonction gamma d'Euler est bien holomorphe dans $\{z \in \C | \Re z > 0 \}$.
\end{ex}