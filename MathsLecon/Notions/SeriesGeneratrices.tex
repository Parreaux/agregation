%LECON: 243

Les séries génératrices ont été introduite pour résoudre des récurrences. Ce sont des séries génératrices pour laquelle la suite satisfait quelques propriétés. Elles servent alors à résoudre des systèmes d'équations récurrents. Cependant, elles permettent également de compter (dénombrement) où d'exprimer des propriétés en probabilité via la fonction génératrice.

\paragraph{Utilisation en dénombrement} Une des application de l'utilisation d'une série génératrice est pour calculer les nombres de Bell qui sont le nombre de relation d'équivalence dans un ensemble à $n$ éléments (DEV).


\paragraph{Utilisation en probabilité} On considère une série entière vue comme série génératrice vérifiant les propriétés de l'élément que l'on souhaite dénombrer. Il nous suffit alors de l'évaluer en une valeur particulière pour obtenir un nombre (somme d'une fonction numérique). La deuxième application qui vient généraliser la première est la création de fonctions génératrices qui sont des séries génératrices particulières \cite[p.159]{Appel}.  

\begin{definition}[Fonction génératrice]
	Soit $X$ une variable aléatoire à valeur dans $\N$. On définit la fonction génératrice de $X$ comme la somme de la série entière suivante:
	\begin{displaymath}
	G(t) = \sum_{n=0}^{\infty} p_nt^n
	\end{displaymath}
\end{definition}

Lorsque cette série converge absolument, la formule de transfert nous permet d'écrire $G(t) = \mathbb{E} \left(t^X\right)$.

\begin{theo}
	Soit $X$ une variable aléatoire discrète à valeur dans $\N$, notons $G$ sa fonction génératrice.
	\begin{enumerate}
		\item Le rayon de la série entière $\sum p_nt^n$ est au moins égal à $1$.
		\item $G$ est définie sur $[-1, 1]$ (au moins), et de classe $\mathcal{C}^{\infty}$ sur $\left]-1,1\right[$ (au moins). On a de plus, $G(1) = 1$, $0 \leq G(t) \leq 1$ pour tout $t \in [0, 1]$, et $G$ est absolument monotone sur $[0, 1]$ : toutes ses dérivées successives sont positives. En particulier $G$ est croissante et connexe.
		\item On peut récupérer les probabilités $p_n$ au moyen des dérivées successives de $G$ en $0$:
		\begin{displaymath}
		\forall n \in \N, p_n = \mathbb{P}\{X = n\} = \frac{G^{(n)}(0)}{n!}
		\end{displaymath}
		\item La fonction génératrice caractérise la loi discrète de $X$ : deux variables ayant la même génératrice ont même loi.
	\end{enumerate}
\end{theo}
{\small{\begin{proof}
	\begin{enumerate}
		\item En $t = 1$, la série $\sum p_n t^n$ converge et a pour somme $1$ (\textblue{on a une somme de toutes les probabilités d'un univers}), donc le rayon de la série est supérieur ou égal à $1$. De plus, pour tout entier $k$ et tout réel $t \in [0, 1]$,
		\begin{displaymath}
		G^{(k)}(t) = \sum_{n=k}^{\infty} n(n-1)\dots(n-k+1)p_nt^{n-k} \geq 0
		\end{displaymath}
	\end{enumerate}
	Le reste des propriétés découle des théorèmes généraux sur les séries entières.
\end{proof}}}

Nous donnons maintenant une table contenant les fonctions génératrices des lois classiques

\begin{tabular}{|cccc|}
	\hline
	Loi & Désignation & Fonction génératrice & Domaine \\
	\hline
	Loi de Bernoulli & $\mathcal{B}(p)$ & $1-p+pt$ & $\R$ \\
	Loi de binomiale & $\mathcal{B}(n,p)$ & $(1-p+pt)^n$ & $\R$ \\
	Loi de géométrique & $\mathcal{G}_{\N^*}(p)$ & $\frac{pt}{1-(1-p)t}$ & $\left[-\frac{1}{1-p},\frac{1}{1-p} \right[$ \\
	Loi de Poisson & $\mathcal{P}(\lambda)$ & $e^{\lambda(t-1)}$ & $\R$ \\
	\hline
\end{tabular}

\begin{theo}[Récupération des moments]
	\begin{enumerate}
		\item $X$ admet une espérance si et seulement si $G$ est dérivable à gauche en $1$, et dans ce cas $\mathbb{E}[X] = G'(1)$.
		\item $X$ admet une variance si et seulement si $G$ est deux fois dérivable à gauche en $1$, et dans ce cas $Var(X) = G''(1) + G'(1) - G'(1)^2$.
		\item $X$ admet un moment d'ordre $k$ si et seulement si $G$ est $k$ fois dérivable à gauche en $1$, et dans ce cas $G^{(k)}(1) = \mathbb{E}[X(X-1)(X-2)\dots(X-k+1)]$.
	\end{enumerate}
\end{theo}
{\small{\begin{proof}
	Par les propriétés de dérivation sur les séries entières sous-jacentes aux fonctions génératrices.
\end{proof}}}

Par ce résultat, une application de la fonction génératrice apparaît: elle permet d'étudier plus facilement les moments d'une variable aléatoire donnée. En particulier, son espérance: ces applications sont une illustration de l'étude d'une espérance pour une variable donnée grâce aux séries génératrices. 

\begin{appli}[Le collectionneur]
	Le problème de collectionneur de vignette qui consiste à calculer le nombre de paquets qu'on peut espérer acheter pour réunir la collection des vignettes utilise les fonctions génératrices pour calculer cette espérance.
\end{appli}

\begin{appli}[Le processus de Galton--Watson \protect{\cite[p.195]{Appel}}]
	Le processus de Galton--Watson étudie la probabilité d'extinction d'une espèce asexuée en fonction de la probabilité de sa reproduction. Cet étude se fait via l'espérance d'une somme de variables aléatoires discrètes et on utilise la série génératrice pour étudier cette espérance.
\end{appli}

Les fonctions génératrices permettent également d'étudier les convergence en loi de variables discrètes \cite[p.399]{Appel}. Nous allons rappeler les différentes manières de convergé pour des variables aléatoires. Ensuite nous donnerons un résultat important sur la convergence en loi qui met en place les fonctions génératrices.
\input{./../Notions/Proba_Convergence-def.tex}


\begin{theo}[Convergence en loi et fonctions génératrices]
	Soit $\left(X_n\right)_{n \geq 1}$ une suite de variables aléatoires à valeurs dans $\N$, de fonctions génératrices $G_n$ et de lois $\mathbb{P}\{X_n = k\} = p_k^{[n]}$. Soit $X$ une variable aléatoire à valeurs dans $\N$ de fonctions génératrice $G$ et de loi $\mathbb{P} = \{X = k\} = p_k$. Alors il y a équivalence entre les énoncés
	\begin{enumerate}
		\item $X_n \overset{L}{\Rightarrow} X$;
		\item $\underset{n \to \infty}{\lim} p_k^{[n]} = p_k$ pour tout $k \in \N$;
		\item $\left(G_n\right)_{n \geq 1}$ converge simplement vers $G$ sur $\left[0,1\right[$.
	\end{enumerate}
\end{theo}
{\small{\begin{proof}
	\textbf{Montrons $\mathbf{1 \Rightarrow 2}$}. Supposons que $X_n \overset{L}{\Rightarrow} X$. Nous souhaiterons écrire, grâce à la convergence en loi, que si $k$ est un entier fixé, alors $F_n\left(x_k\right) \to F\left(x_k\right)$ quand $n$ tend vers l'infini où $F_n$ est la fonction de répartition de $X_n$ et $F$ est celle de $X$. Malheureusement, $x_k$ est justement un point de discontinuité de $F$. En revanche, comme $x_k$ est isolé, il existe un réel $\epsilon > 0$ tel que $\left]x_k, x_k+\epsilon\right]$ tel qu'il ne continent aucun point de discontinuité de $F$. Ainsi $F$ est constant sur $\left[x_k, x_k+\epsilon\right]$. On peut alors écrire que 
	\begin{displaymath}
	F_n\left(x_k\right) = F_n\left(x_k + \epsilon\right) \underset{n \to \infty}{\rightarrow} F\left(x_k + \epsilon\right) = F(x)
	\end{displaymath}
	Ceci établi, on en déduit que pour tout entier $k$:
	\begin{displaymath}
	p_k^{[n]} = F_n(k) - F_n(k-l) \underset{n \to \infty}{\rightarrow} F(k) - F(k-1) = p_k
	\end{displaymath}
	
	\textbf{Montrons $\mathbf{2 \Rightarrow 1}$}. Supposons que $\underset{n \to \infty}{\lim} p_k^{[n]} = p_k$ pour tout $k \in \N$. Nous allons montrer que $\left(F_n\right)_{n \geq 1}$ converge uniformément vers $F$ sur $\R$ tout entier. En écrivant que, pour tous entiers $k$ et $n$,
	\begin{displaymath}
	\left|p_k^{[n]} - p_k\right| = p_k^{[n]} + p_k - 2\min(p_k^{[n]}, p_k)
	\end{displaymath}
	et en remarquant que $0 \leq \min(p_k^{[n]}, p_k) \leq p_k$, on peut sommer la relation pour $k \in K$ où $K$ contient l'ensemble des indices des points de discontinuités de $F$, on obtient
	\begin{displaymath}
	\sum_{k \in K} \left|p_k^{[n]} - p_k\right| = 2 - 2\sum_{k \in K}\min(p_k^{[n]}, p_k)
	\end{displaymath} 
	A $k$ fixé, la suite de terme général $\min(p_k^{[n]}, p_k)$ converge vers $p_k$ tout en étant majoré par celui-ci; on conclut par la version discrète de la convergence dominée (\textblue{autrement dit, on utilise la convergence dominée avec une mesure discrète}):
	\begin{displaymath}
	\underset{n \to \infty}{\lim} \sum_{k \in K}\min(p_k^{[n]}, p_k) = \sum_{k \in K} p_k =1
	\end{displaymath}
	et donc 
	\begin{displaymath}
	\underset{n \to \infty}{\lim} \sum_{k \in K} \left|p_k^{[n]} - p_k\right| = 0
	\end{displaymath}
	Pour conclure, on peut écrire que pour tout $x \in \R$, on a
	\begin{displaymath}
	\begin{array}{ccc}
	F_n(x) = \sum\limits_{\underset{x_k \leq x}{k \in K}} p_k^{[n]} & \text{ et } &  F(x) = \sum\limits_{\underset{x_k \leq x}{k \in K}} p_k\\
	\end{array}
	\end{displaymath}
	et donc majorer
	\begin{displaymath}
	\left|F_n(x) - F(x)\right| = \sum\limits_{\underset{x_k \leq x}{k \in K}} \left|p_k^{[n]} - p_k\right| \leq \sum_{k \in K} \left|p_k^{[n]} - p_k\right| \underset{n \to \infty}{\longrightarrow} 0
	\end{displaymath}
	
	\textbf{Montrons $\mathbf{2 \Rightarrow 3}$}. Supposons que $\underset{n \to \infty}{\lim} p_k^{[n]} = p_k$ pour tout $k \in \N$. Soit $u \in \left[0,1\right[$. Fixons un réel $\epsilon >0$. La majoration $\left|p_k^{[n]} - p_k\right| \leq 1$ permet d'écrire l'inégalité suivante, où le second membre est bien défini
	\begin{displaymath}
	\left|G_n(u) - G(u)\right| \leq \sum_{k=0}^{\infty} \left|p_k^{[n]} - p_k\right|u^k
	\end{displaymath}
	Comme $\left|p_k^{[n]} - p_k\right|$ peut être rendu aussi petit que l'on veut, on peut majorer 
	\begin{displaymath}
	\sum_{k=K+1}^{\infty} \left|p_k^{[n]} - p_k\right|u^k \leq \sum_{k=K+1}^{\infty} u^k = \frac{u^{K+1}}{1- u}
	\end{displaymath}
	On choisit donc $K$ de manière à rendre $\left|p_k^{[n]} - p_k\right|$ aussi petit que l'on veut (plus petit que $\epsilon$). Ce nombre étant fixé, il existe un entier $N$ tel que  $\forall n \geq N$,
	\begin{displaymath}
	\sum_{k=0}^{K} \left|p_k^{[n]} - p_k\right| \leq \epsilon
	\end{displaymath}
	Alors, pour tout $n \geq N$, on a
	\begin{displaymath}
	\left|G_n(u) - G(u)\right| \leq \sum_{k=0}^{K} \left|p_k^{[n]} - p_k\right| + \sum_{k=K+1}^{\infty} \left|p_k^{[n]} - p_k\right|u^k \leq 2\epsilon
	\end{displaymath}
	
	\textbf{Montrons $\mathbf{2 \Rightarrow 3}$}. Supposons que $\left(G_n\right)_{n \geq 1}$ converge simplement vers $G$ sur $\left[0,1\right[$. Commençons par rappeler un résultat qui est conséquence du théorème de Bolzano--Weierstrass:
	\begin{lemme}
		Soit $\left(x_n\right)_{n \geq 1}$ une suite réelle bornée, alors elle admet au moins une valeur d'adhérence $l$. Si elle n'en admet pas d'autre, alors elle converge.
	\end{lemme}
	Nous allons l'appliquer une infinité de fois avant de conclure par un argument diagonal. La suite $\left(p_1^{[n]}\right)_{n \geq 0}$ étant bornée, on peut en extraire une suite $\left(p_1^{[\varphi_1(n)]}\right)_{n \geq 0}$ convergente dont on note la limite $l_1$. 
	La suite $\left(p_2^{[\varphi_1(n)]}\right)_{n \geq 0}$ étant bornée, on peut en extraire une suite $\left(p_2^{[\varphi_2(n)]}\right)_{n \geq 0}$ convergente dont on note la limite $l_2$ (\textblue{où $\varphi_2$ est une extractrice construite à partir de $\varphi_1$ par composition}). On construit ainsi, de proche en proche, des extractrices $\varphi_k$ vérifiant : $\left(p_k^{[\varphi_k(n)]}\right)_{n \geq 0}$ converge dont on note la limite $l_k$. Enfin, on définit $\varphi : \N \to \N$ telle que $\varphi(n) = \varphi_n(n)$. Alors $\left(\varphi(n)\right)_{n \geq m}$ est une sous suite de $\left(\varphi(n)_m\right)_{n \geq 0}$, ce qui prouve que $\left(p_k^{[\varphi(n)]}\right)_{n \geq 0}$ converge et ceci pour tout $k$. On en déduit que le suite de terme général, $G_{\varphi(n)}$ converge simplement vers la fonction définie par:
	\begin{displaymath}
	H(u) = \sum_{k=0}^{\infty} l_ku^k
	\end{displaymath}
	et donc $H = G$; et par unicité des coefficients des séries entières, on en déduit que $l_k = p_k$ pour tout $k$. Nous n'avons pas encore prouvé l'existence d'une sous-suite convergente. Si, pour un certain $q$, la suite $\left(p_q^{[n]}\right)_{n \geq 0}$ ne convergeait pas ver $p_q$, elle admettrait, d'après le lemme, une sous-suite convergeant vers une autre valeur $p_q'$. On pourra alors recommencer le principe d'extraction diagonal précédent afin de montrer (par unicité des coefficients d'une série entière) que $p_k' = p_k$.
\end{proof}}}

