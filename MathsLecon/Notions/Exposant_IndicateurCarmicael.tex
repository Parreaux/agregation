%LECONS: 104
Trouver l'ordre d'un élément dans un groupe cyclique n'est pas toujours simple (même quand on se place dans le cadre $\Z/n\Z$) \cite[p.127]{BaillyMaitre}. Plaçons nous dans le cadre de $\Z/n\Z$ (par le théorème de structure, nous allons ainsi étudier toutes ces caractéristiques pour les groupes abéliens). Déterminer l'ordre d'un de ces éléments n'est pas simple, mais dans tous les cas, nous en connaissons un multiple qui est $\varphi(n)$ où $\varphi$ est l'indicatrice d'Euler (théorème d'Euler). Le but ici est de chercher et de trouver un valeur plus petite que $\varphi(n)$ qui est toujours un multiple de l'ordre d'un élément.

\begin{definition}
	Soit $(G, \times)$ un groupe fini, le plus petit entier $e$ non nul tel que $g^e=1$ pour tout $g \in G$ est l'exposant de $G$. On le note alors $\lambda(G)$.
\end{definition}

\begin{prop}
	Soit $G$ un groupe fini, nous avons les propriétés suivantes:
	\begin{enumerate}
		\item $\lambda(G)$ est un diviseur de $\left|G\right|$.
		\item Si $G$ est cyclique, $\lambda(G) = \left|G\right|$. En particulier $\lambda(\Z/n\Z) =n$.
		\item $\lambda(G) = \mathrm{ppcm}\{\mathrm{ord}(g), g \in G\}$
		\item Soit $e \in \N$, nous avons l'équivalence: $\forall g \in G, g^e =1 \Leftrightarrow e \text{ est un  multiple de } \lambda(G)$.
	\end{enumerate}
\end{prop}
{\footnotesize{\begin{proof}
	\begin{enumerate}
		\item Le théorème de Lagrange nous assure que pour tout $g\in G$, $g^{\left|G\right|} = 1$. En effectuant la division euclidienne de $\left|G\right|$ par $\lambda(G)$, on obtient: $\left|G\right| = q \times \lambda(G) + r$
		avec $r < \lambda(G)$. Et comme $g^{\left|G\right|} \underbrace{=}_{\text{div euclidienne}} g^{q \time \lambda(G) + r} \underbrace{=}_{calcul} g^{\lambda(G)^q}g^r \underbrace{=}_{g^{\lambda(G)}=1} g^r \underbrace{=}_{g^{\left|G\right|} = 1} 1$.
		Donc $g^r = 1$ avec $r < \lambda(G)$. La définition de $\lambda(G)$ nous permet d'affirmer que $r = 0$ et donc que $\lambda(G)$ est un diviseur de $\left|G\right|$.
		
		\item Si $G$ est cyclique l'ordre d'un générateur est le cardinal de $G$, d'où $\lambda(G) \geq \left|G\right|$. Le premier point \textblue{(par la relation de divisibilité)} nous permet d'obtenir l'égalité.
		
		\item Notons $l = \mathrm{ppcm}\{\mathrm{ord}(g), g \in G\}$. Nous avons alors $g^l = 1$ pour tout $g \in G$ \textblue{(car $l$ est un multiple de l'ordre de $g$ par définition de $l$)}. Donc $\lambda(G) \leq l$.
		
		De plus, s'il existe un entier $e$ tel que $g^e=1$, alors l'ordre de $g$ divise $e$, nous avons donc $\mathrm{ord}(g) | \lambda(G)$. Nous avons donc $l | \lambda(G)$ donc $l \leq \lambda(G)$.
		
		D'où l'égalité.
		
		\item Si $g^e=1$, $e$ est un multiple de $\mathrm{ord}(g)$. D'où si $\forall g \in G, g^e =1$, $e$ est multiple de tous les ordres de tous les éléments de $G$, donc il est multiple de leur $\mathrm{ppcm}$ qui est $\lambda(G)$ par le point précédent. L'implication réciproque se déduit de la définition de $\lambda(G)$.
	\end{enumerate}
\end{proof}}}

Nous souhaitons maintenant calculer l'exposant du groupe $\left(\Z/n\Z\right)^{\times}$. 

\begin{definition}
	On appelle indicateur de Carmichaël l'application $\lambda : \N^{*} \to \N$ définie par $\lambda(n) = \lambda\left(\left(\Z/n\Z\right)^{\times}\right)$.
\end{definition}

La proposition suivante nous permet de calculer $\lambda(n)$ pour tout $n$ pour lequel on connaît sa décomposition en facteurs premiers.

\begin{prop}
	\begin{enumerate}
		\item Nous avons $\lambda(2) = 1$, $\lambda(4) = 2$, $\lambda\left(2^r\right) = 2^{r-2}$ pour $r \geq 3$.
		\item Pour $p$ premier impaire et $r \geq 1$, nous avons $\lambda\left(p^r\right)=p^{r-1}(p-1)$.
		\item Si $m \geq 2$ et $n \geq 2$ sont premiers entre eux, nous avons $\lambda(mn) = \mathrm{ppcm}\left(\lambda(m), \lambda(n)\right)$.
	\end{enumerate}
\end{prop}
{\footnotesize{\begin{proof}
	\begin{enumerate}
		\item Pour $r \geq 3$, on a $\left(\Z/2^r\Z\right)^{\times}$ est isomorphe à $\Z/2\Z \times \Z/2^{r-2}\Z$.
		\item Par cyclicité de $\left(\Z/2^r\Z\right)^{\times}$, $\lambda\left(p^r\right)=\varphi\left(p^r\right)$
		\item Conséquence du théorème chinois et application des définitions.
	\end{enumerate}
\end{proof}}}

\begin{appli}[utilisation de la notion d'indicatrice de Carmichael] \textcolor{white}{a}
	\begin{itemize}
	\item Pour tout $x$ impaire et non multiple de $3$, $x^2 - 1$ est multiple de $24$. \textblue{(On a $\lambda(24) = \mathrm{ppcm}\left(\lambda(8), \lambda(3)\right) = \mathrm{ppcm}(2,2)= 2$.)}
	\item Le petit théorème reste vrai dans $\Z/561\Z$ même si $561$ n'est pas un nombre premier. \textblue{(On a $\lambda(561)= \lambda(3 \times 11 \times 17) = \mathrm{ppcm}(2, 10, 16) = 80$. D'ou pour tout $x \in (\Z/261\Z)^{\times}$, nous avons $x^{80} = 1$ donc $x^{560} = 1$.)}
\end{itemize}
\end{appli}

\begin{definition}
	Si $n$ est un nombre impair non premier tel que pour tout $x \in (\Z/n\Z)^{\times}$ nous avons $x^{n-1} = 1$, alors $n$ s'appelle un nombre de Carmichaël.
\end{definition}

\begin{prop}
	Soit $n \in \N^{*}$ non premier, notons $n = p_1^{r_1} \dots p_k^{r_k}$ sa décomposition en facteurs premiers, $n$ est un nombre de Carmichaël si et seulement si 
	\[\left \{ \begin{array}{cc}
	r_i = 1 &  \\
	\left(p_i - 1\right) | \left(n-1\right) &  \\
	\end{array} \forall i \right . \]
\end{prop}
{\footnotesize{\begin{proof}
	On remarque que $n$ est de Carnichael si et seulement si $\lambda(n) |(n-1)$. On suppose que $\lambda\left(p_i^{r_i}\right)$ est multiple de $p_i$ et on obtient qu'il n'est pas de Carnichaël avec la remarque précédente. Par suite, on applique le résultat sur le calcul de $\lambda$ sur la décomposition en facteur premier d'un nombre de Carnichaël.
\end{proof}}}

Voici un tableau des dix premiers nombres de Carnichaël.

\begin{tabular}{|c|c|c|c|}
	\hline
	$i$ & $n$ & décomposition & $\lambda(n)$ \\
	\hline
	$1$ & $561$ & $3 \times 11 \times 17$ & $80$\\
	$2$ & $1105$ & $5 \times 13 \times 17$ & $48$ \\
	$3$ & $1729$ & $7 \times 13 \times 19$ & $36$ \\
	$4$ & $2465$ & $5 \times 17 \times 29$ & $112$ \\
	$5$ & $2821$ & $7 \times 13 \times 31$ & $60$ \\
	$6$ & $6601$ & $7 \times 23 \times 41$ & $1320$ \\
	$7$ & $8911$ & $7 \times 19 \times 67$ & $198$ \\
	$8$ & $10585$ & $5 \times 29 \times 73$ & $504$ \\
	$9$ & $15841$ & $7 \times 31 \times 73$ & $360$ \\
	$10$ & $29341$ & $13 \times 37 \times 61$ & $180$ \\
	\hline
\end{tabular}