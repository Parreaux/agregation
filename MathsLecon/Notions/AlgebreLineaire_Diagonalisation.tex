%DEVs: matrice diagonalisable; suite de polygones
%LECONs : 151 (Dimension et rang); 153 (Polynome et reduction)
On cherche à écrire une matrice sous une forme plus agréable afin de facilité sa manipulation (comme lorsque nous voulons la mettre à la puissance, lors d'un calcul d'exponentielle) \cite[p.956]{Berhuy}. Cette opération revient à diagonaliser la matrice, c'est-à-dire trouver une base dans laquelle la matrice est diagonale.

\paragraph{Critères de diagonalisation} Commençons par définir les matrices diagonalisables puis donnons les différentes caractérisations à la diagonalisation.
\begin{lemme}
	Soient $E$ un $K$-espace vectoriel de dimension finie non nulle $n$, $u \in \mathcal{L}(E)$ et $e$ une base de $E$. Les propriétés suivantes sont équivalentes:
	\begin{enumerate}
		\item la matrice $Mat(u; e)$ est diagonale.
		\item il existe $\lambda_1, \dots, \lambda_n \in K$ tels que $\forall i \in \llbracket 1; n \rrbracket, u(e_i) = \lambda_i$ si $u_i \in e$
	\end{enumerate}
\end{lemme}
\begin{footnotesize}
	\begin{proof}
		Définition d'une matrice représentative. On remarque de plus que les $e_i$ sont non nuls car ils proviennent d'une base.
	\end{proof}
\end{footnotesize}

\begin{definition}
	Soit $u \in \mathcal{L}(E)$. On dit que $u$ est diagonalisable s'il existe une base de $E$ dans laquelle la matrice représentative de $u$ soit diagonale.
\end{definition}

\begin{req}
	Cela revient à dire qu'il existe une base de $E$ formée de vecteur propre de $u$, ou encore de que $E$ se décompose en somme directe de droites stables par $u$.
\end{req}

\begin{theo}
	Soit $u \in \mathcal{L}(E)$. Les propriétés suivantes sont équivalentes:
	\begin{enumerate}
		\item l'endomorphisme $u$ est diagonalisable
		\item on a $E = \underset{\lambda \in Sp_K(u)}{\bigoplus} E_{\lambda}$
		\item le polynôme $\chi_u$ est scindé, et pour tout $\lambda \in Sp_K(u)$, on a $\dim_K(E_{\lambda}) = m_{\lambda}$, où $m_{\lambda}$ est la multiplicité de $\lambda$.
	\end{enumerate}
\end{theo}
\begin{footnotesize}
	\begin{proof}
		\begin{description}
			\item[Montrons $3 \Rightarrow 2$] $\chi_u$ est scindé et les valeurs propres de $u$ sont exactement ses racines : $\chi_u = \prod_{\lambda \in Sp_K(u)}(X- \lambda)^m_{\lambda}$. En comparant les degré, $\sum_{\lambda \in Sp_K(u)} m_{\lambda} = n$, on en déduit que $\dim_K(\underset{\lambda \in Sp_K(u)}{\bigoplus} E_{\lambda}) = \dim_K(E)$. 
			
			\item[Montrons $2 \Rightarrow 1$] Clair en recollant les bases des sous-espaces propres.
			
			\item[Montrons $1 \Rightarrow 3$] On calcul le polynôme caractéristique sur le représentation diagonale de l'endomorphisme.
		\end{description}
	\end{proof}
\end{footnotesize}

\begin{cor}
	Soit $u \in \mathcal{L}(E)$. Si $u$ possède $n = \dim_K(E)$ valeurs propres distinctes, alors $u$ est diagonalisable.
\end{cor}
\begin{footnotesize}
	\begin{proof}
		Application des propositions et théorèmes précédents.
	\end{proof}
\end{footnotesize}

\begin{theo}
	Soit $u \in \mathcal{L}(E)$. Les propriétés suivantes sont équivalentes:
	\begin{enumerate}
		\item l'endomorphisme $u$ est diagonalisable
		\item le polynôme $\prod_{\lambda \in Sp_K(u)} (X-\lambda)$ annule $u$
		\item il existe un polynôme $P$ annulant $u$ scindé sans racines multiples. Dans ce cas $Sp_K(u)$ est contenu dans ces racines
		\item le polynôme minimal $\mu_u$ est scindé sans racines multiples
		\item $\mu_u = \prod_{\lambda \in Sp_K(u)} (X-\lambda)$
	\end{enumerate}
\end{theo}
\begin{footnotesize}
	\begin{proof}
		\begin{description}
			\item[Montrons $1 \Rightarrow 2$] Le théorème précédent et le lemme des noyaux nous donne l'existence de $P = \prod_{\lambda \in Sp_K(u)} (X-\lambda)$ tel que $e = \ker(P(u))$. Dans ce cas $P(u) = 0$.
			
			\item[Montrons $2 \Rightarrow 3$] Clair par propriétés des valeurs propres et des polynômes.
			
			\item[Montrons $3 \Rightarrow 4$] Le polynôme minimal $\mu_u$ divise $P$.
			
			\item[Montrons $4 \Rightarrow 5$] Les polynômes $\mu_u$ et $\chi_u$ ont exactement les même racines qui sont les valeurs propres de $u$.
			
			\item[Montrons $5 \Rightarrow 1$] On applique le lemme de décomposition des noyaux.
		\end{description}
	\end{proof}
\end{footnotesize}

\begin{lemme}
	Soient $u \in \mathcal{L}(E)$ et $F$ un sous-espace stable par $u$. Si $u$ est diagonalisable, alors $u_F \in \mathcal{L}(F)$ est diagonalisable.
\end{lemme}
\begin{footnotesize}
	\begin{proof}
		On applique le théorème précédent à la restriction du polynôme minimal de $u_F$ qui est un restriction de celui de $u$.
	\end{proof}
\end{footnotesize}

\begin{theo}
		Soient $u, u' \in \mathcal{L}(E)$ tels que $u$ et $u'$ commutent. S'ils sont diagonalisables, alors ils le sont dans la même base.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		Si $\lambda$ est une valeur propre de $u$ alors $E_{\lambda}$ est stable par $u'$. Par ce qui précède, $u'_{E_{\lambda}}$ est diagonalisable: il existe donc une base qui diagonalise $u'_{E_{\lambda}}$. En recollant les différentes base, comme $u$ et $u'$ sont diagonalisables, on obtient cette base.
	\end{proof}
\end{footnotesize}

\paragraph{Applications de la diagonalisation} Il y a trois grandes applications à la diagonalisation \cite[p.170]{Grifone}.

\emph{Calcul d'une puissance} Le calcul de $A^m$ où $A$ est une matrice diagonalisable s'effectue en diagonalisant $A$ puis $A^m = P^{-1}D^mP$ où $D^m$ consiste à mettre ses coefficients à la puissance \textblue{(ce qui est facile)}.
	
\emph{Résolution d'un système de suites récurrentes} On se ramène au calcul d'une puissance de la matrice sous-jacente au système.
	
\emph{Système différentiel linéaire à coefficients constants} Si on met le système sous la forme d'une matrice $A$, on souhaite résoudre $\frac{dX}{dt} = AX$. On raisonne comme suit
\begin{enumerate}
	\item on diagonalise $A$ et on trouve $D$ comme matrice diagonale et $P$ comme matrice de passage: $D= P^{-1}DP$
	\item on intègre le système $\frac{dX'}{dt} = DX'$ \textblue{(plus facile car $D$ est diagonale)}
	\item on revient à $X$ par $X = PX'$.
\end{enumerate}
