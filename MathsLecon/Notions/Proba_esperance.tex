%DEV: Weierstrass Bernstein

On donne quelques résultats sur l'espérance d'une variable aléatoire \cite[p.53]{BardeLedoux}.
	
\begin{definition}
	Soit $X$ une variable aléatoire réelle, définie sur $(\Omega, \mathcal{A}, \Pr)$. Si $X$ est intégrable, on appelle espérance de $X$ (sous la probabilité $P$) le nombre réel:
	\begin{displaymath}
		\E[X] = \int_{\Omega}X d\Pr
	\end{displaymath}
	On dit que $X$ est centrée, si elle est intégrable et $\E(X) = 0$.
\end{definition}
	
\begin{theo}[Formule de transfert]
	Soit $X$ un vecteur aléatoire sur $(\Omega, \mathcal{A}, \Pr)$ à valeurs dans $(\R{d}, \mathcal{B}(\R^d))$ et soit $\varphi$ une fonction borélienne de $\R^d$ dans $\R$. Si $\varphi \in L^1(\R^d, \mathcal{B}(\R^d), \Pr^{X})$, alors 
	\begin{displaymath}
		\E[\varphi(X)] = \int_{\Omega} \varphi \circ X(\omega)d\Pr(\omega) = \int_{\R^d} \varphi (\omega)d\Pr^{X}(\omega)
	\end{displaymath}
\end{theo}
	
\begin{theo}
	\begin{enumerate}
		\item (Inégalité de Jensen) Si $\varphi$ est connexe sur $\R$ et si $X$ est une variable aléatoire réelle telle que $X$ et $\varphi(X)$ sont intégrables, alors
		\begin{displaymath}
			\varphi(\E[X]) \leq \E[\varphi(X)]
		\end{displaymath}
			
		\item (Inégalité de Hölder) Si $X \in L^p$, $Y \in L^q$, $p, q \geq 1$ deux exposants conjugués, alors $XY \in L^1$ et
		\begin{displaymath}
			\E[XY] \leq \left(\E\left[\left|X\right|^p\right]\right)^{1/p} \left(\E\left[\left|X\right|^q\right]\right)^{1/q}
		\end{displaymath}
			
		\item L'application $p \mapsto \left(\E\left[\left|X\right|^p\right]\right)^{1/p}$ est croissante.
			
		\item $\mathopen{||}.\mathclose{||}_{p} = \left(\E\left[\left|.\right|^p\right]\right)^{1/p}$ est une norme sur $L^p(\Omega, \mathcal{A}, \Pr)$, $p \geq 1$.
			
		\item On définit $\mathopen{||}X\mathclose{||}_{\infty} = \lim\limits_{p \rightarrow \infty} \mathopen{||}X\mathclose{||}_{p}$. C'est une norme, appelée norme supremum sur $L^{\infty}(\Omega, \mathcal{A}, \Pr)$.
	\end{enumerate}
\end{theo}
	
Donnons maintenant quelques éléments sur la variance d'une variable aléatoire réelle.
	
\begin{definition}
	Soit $X$ une variable aléatoire réelle dont le carré est intégrable. On appelle variance de $X$, et on note $\mathrm{Var}(X)$, la quantité: 
	\begin{displaymath}
		\mathrm{Var}(X) = \E\left[\left(X - \E\left[X\right]\right)^2\right]
	\end{displaymath}
	La racine de la variance est appelée l'écart type, noté $\sigma(X)$. Une variable est dite réduite si elle est d'écart type (et de variance) $1$.
\end{definition}
	
La variance peut aussi s'exprimer sous la forme suivante:
\begin{displaymath}
	\mathrm{Var}(X) = \E[X^2] - \E[X]^2
\end{displaymath}
Par exemple, la variance d'une loi de Bernoulli de paramètre $(n, p)$ est de $np(1-p)$.
	
\begin{theo}[Inégalité de Markov]
	Si $X$ est intégrable et $t > 0$, alors
	\begin{displaymath}
		\Pr\{X \geq t\} \leq \frac{\E[X^+]}{t} \leq \frac{\E\left[\mathopen{|}X\mathclose{|}\right]}{t}
	\end{displaymath}
\end{theo}
	
\begin{cor}[Inégalité de Tchebychev]
	Si $X \in L^2$, pour tout $t > 0$,
	\begin{displaymath}
		\Pr\{\left|X - \E[X]\right| \geq t\} \leq \frac{\mathrm{Var}(X)}{t^2}
	\end{displaymath}
\end{cor}