%LECONS: 120 (Anneau Z/nZ); 121 (Nombre premier); 123 (Corps finis)
%DEV: reciprocite quadratique
Nous allons rappeler quelques notions autours des carrés dans un corps finis et du symbole de Legendre \cite[p.119]{AlFakir}. On est ainsi souvent amené à étudier les carrées de $(\Z/n\Z)^*$.

\begin{definition}[Carré modulo $n$]
	Soit $a$ et $n$ deux entiers premiers entre eux. S'il existe un entier $b$ tel que $a \equiv b^2 [n]$, on dit que $a$ est un résidu quadratique de $n$ ou un carré modulo $n$. Sinon, on dit que $a$ est un résidu non quadratique.
\end{definition}

\begin{definition}[Symbole de Legendre]
	Soit $n$ un nombre premier impair. Le symbole de Legendre pour $a \in \Z$ est:
	\begin{displaymath}
	\left(\frac{a}{p}\right) = \left\{ \begin{array}{c @{~,~} l}
	0 & \text{si } a\equiv 0 [p] \\
	1 & \text{si } a \text{ est un résidu quadratique de } p \\
	-1 & \text{si } a \text{ n'est pas un résidu quadratique de } p \\
	\end{array}\right.
	\end{displaymath}
\end{definition}

\begin{prop}
	Si $p$ est un nombre premier et $\left(\mathbb{F}^*_p\right)^2$ l'ensemble des éléments de $\mathbb{F}^*_p$ qui sont des carrés, on a:
	\begin{enumerate}
		\item si $p = 2$, $\mathbb{F}^*_2 = \left(\mathbb{F}^*_2\right)^2$.
		\item si $p>2$, l'application de $\mathbb{F}^*_p$ dans $\mathbb{F}^*_p$ définie par $x \mapsto x^{(p-1)/2}$ est un morphisme de groupes dont le noyau est $\left(\mathbb{F}^*_p\right)^2$, sous-groupe cyclique d'ordre $\frac{p-1}{2}$ et dont l'image est $\{-1, 1\}$.
	\end{enumerate}
\end{prop}
{\footnotesize{\begin{proof}
	\begin{enumerate}
		\item Trivial car $\mathbb{F}^*_2$ est réduit à l'élément neutre.
		\item L'application est un endomorphisme de groupe multiplicatif par commutativité. D'autre part, le groupe $\mathbb{F}^*_p$ est cyclique d'ordre $p - 1$, on peut choisir un élément générateur $g$.
		
		Soit $x$ un élément du noyau de ce morphisme, il existe $k$ un entier tel que $x = g^k$. Comme $x^{(p-1)/2} =1$, on a $g^{k(p-1)/2} =1$, soit $(p-1) | k(p-1)/2$ et $2|k$. Posons $l = \frac{k}{2}$, on a $x = \left(g^l\right)^2$, donc $x$ est un carré. La réciproque est immédiate.
		
		L'égalité $x = \left(g^l\right)^2$ démontre que le noyau est engendré par $g^2$. Donc comme $g$ est un générateur ses éléments sont d'ordre $\frac{p-1}{2}$. En passant à l'image, les éléments du noyau sont racines de $X^2 -1$ soit sont dans $\{-1, 1\}$.
	\end{enumerate}
\end{proof}}}

\begin{cor}
	Dans $\mathbb{F}^*_p$, un élément $x$ est un carré si et seulement si l'ordre de $x$ divise $\frac{p-1}{2}$ et il y a autant de carré que de non carré.
\end{cor}
{\footnotesize{\begin{proof}
	La condition $^{(p-1)/2} = 1$ caractérise les éléments de l'unique sous-groupe d'ordre $\frac{p-1}{2}$ de $\mathbb{F}^*_p$. L'ordre de $\mathbb{F}^*_p$, il reste $\frac{p-1}{2}$ éléments qui ne sont pas des carrés.
\end{proof}}}

\begin{cor}
	Si $p$ est un nombre premier impair, $a$ et $b$ des entiers, on a:
	
	\noindent\begin{minipage}{.36\textwidth}
		\begin{enumerate}
			\item $\left(\frac{a}{p}\right) \equiv a^{(p-1)/2} [p]$.
			\item $\left(\frac{ab}{p}\right) = \left(\frac{a}{p}\right)\left(\frac{b}{p}\right)$.
			\item Si $a \equiv b [p]$, alors $\left(\frac{a}{p}\right) = \left(\frac{b}{p}\right)$.
		\end{enumerate}
	\end{minipage} \hfill
	\begin{minipage}{.6\textwidth}
		{\footnotesize{\begin{proof}
					\begin{enumerate}
						\item Si $a \equiv 0 [p]$, on est ok. Si $a$ est un résidu quadratique de $p$, il existe $x$ tel que $a \equiv x^2 [p]$, d'où $a^{(p-1)/2} \equiv 1 [p]$. On traite le dernier cas de manière analogue.
						\item On applique le résultat précédent.
						\item Découle de la définition.
					\end{enumerate}
				\end{proof}}}
	\end{minipage}
\end{cor}

\begin{appli}
	Trouver des carrés revient en la résolution d'équations diophantienne du second degré.
\end{appli}

\begin{theo}[Loi de réciprocité quadratique]
	Soient $p$ et $q$ sont deux nombres premiers impaires et distincts, alors
	\begin{displaymath}
	\left(\frac{q}{p}\right)\left(\frac{p}{q}\right) = (-1)^{\left(\frac{p-1}{2}\right)\left(\frac{q-1}{2}\right)}
	\end{displaymath}
\end{theo}

\begin{req}
	La loi de réciprocité quadratique est vrai même si $p$ et $q$ ne sont pas premier. On utilise le symbole de Jacobi.
\end{req}

\begin{definition}[Symbole de Jacobi]
	Si $n$ est un entier naturel impair tel que $n = \prod_{i} p_i^{\alpha_i}$ sa factorisation primaire et $a$ un entier, le symbole de Jacobi $\left(\frac{a}{n}\right)$ est défini par: $\left(\frac{a}{n}\right) = \prod_{i} \left(\frac{a}{p_i}\right)^{\alpha_i}$.
\end{definition}

\begin{theo}[Critère d'Euler]
	Soit $p$ un nombre premier impaire et $a$ un entier premier avec $p$, alors $\left(\frac{a}{p}\right) \equiv a^{\frac{p-1}{2}} [p]$
\end{theo}
\begin{footnotesize}
	\begin{proof}
		Voir corollaire ci-dessus.
	\end{proof}
\end{footnotesize}

\begin{req}
	On peut utiliser ces résidus quadratique pour d'autres tests de primalité : Lucas-Lehmer qui utilise les nombres de Mercènes.
\end{req}