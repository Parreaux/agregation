%DEV: Galton-Watson
%LECON: 243

On s'intéresse à la régularité des limites des suites et des séries de fonctions \cite{Marco-Thieullen-Weil}. On commence par étudier les limites de suites de fonctions.

\subparagraph{Propriétés d'une limite de suite de fonctions} La convergence uniforme permet (contrairement à la convergence simple) de conservé à la limite un bon nombre de propriétés comme la continuité ou la dérivabilité.

\begin{theo}[Continuité d'une limite de suite de fonctions]
	Soit $\left(f_n\right)_{n \in \N}$ une suite de fonctions définies sur $A$ qui converge uniformément vers une fonction $f$ et soit $t_0$ un point de $A$. Si, pour tout $n \in \N$, la fonction $f_n$ est continue en $t_0$, alors $f$ est continue en $t_0$.
\end{theo}
{\small{\begin{proof}
	Par la convergence uniforme on a, pour $\epsilon > 0$, $\exists n \in \N$ tel que pour tout $t \in A$, $\left|f_n(t) - f(t)\right| < \frac{\epsilon}{3}$.
	
	Par la continuité des $f_n$ on a que, pour $\epsilon > 0$, $\exists \eta > 0$ tel que pour tout $t \in A$ vérifiant $\left|t - t_0\right| < \eta$, alors $\left|f_n(t) - f_n(t_0)\right| < \frac{\epsilon}{3}$.
	
	Une inégalité triangulaire nous permet de montrer que $\left|f(t) - f(t_0)\right| \leq \epsilon$.
\end{proof}}}

\begin{cor}
	La limite uniforme d'une suite de fonctions continues est une fonction continue.
\end{cor}

\begin{req}
	La contraposé de ce corollaire (ou du théorème) peut permettre de montrer qu'une suite de fonction ne converge pas uniformément.
\end{req}

\begin{ex}[Montrer la non convergence uniforme d'une suite de fonction avec la continuité \protect{\cite[p.442]{Marco-Thieullen-Weil}}]
	Pour $n \in \N$ et $t \geq 0$, on pose $f_n(t) = \frac{t^n}{1+ t^{2n}}$. La limite simple de la fonction $f$ définie sur $[0, + \infty[$ par $f(t) = 0$ si $0 \leq t \leq 1$, $f(1) = \frac{1}{2}$ et $f(t) = 0$ si $t > 1$. La discontinuité en $1$ prouve qu'il n'y a pas de limite uniforme de la suite sur $[0, +\infty[$.
\end{ex}

Pour l'intégrale d'une limite de suite, aller voir le théorème de continuité sous le signe intégral (leçon 239). Nous allons maintenant nous intéresser à la dérivabilité de ces fonctions.

\begin{theo}[Dérivabilité d'une limite de suite de fonctions]
	Soit $(f_n)_{n \in \N}$ une suite de fonctions de classe $\Cun$ sur le segment $[a, b]$ telle que
	\begin{enumerate}
		\item $(f_n)_{n \in \N}$ converge simplement sur $[a, b]$ vers une fonction $f$;
		\item la suite des dérivées $(f'_n)_{n \in \N}$ converge uniformément vers une fonction $g$ sur $[a, b]$.
	\end{enumerate}
	Nous avons alors les propriétés suivantes:
	\begin{itemize}
		\item $(f_n)_{n \in \N}$ converge uniformément sur $[a, b]$ vers une fonction $f$;
		\item la fonction $f$ est de classe $\Cun$ de dérivée $g$.
	\end{itemize}
\end{theo}
{\small{\begin{proof}
	Application du théorème de continuité sous le signe intégral avec la primitive de la dérivée.
\end{proof}}}

\subparagraph{Propriétés de la somme d'une série de fonctions} Nous continuons avec l'étude des propriétés de la somme d'une série de fonctions : continuité, intégrabilité et dérivabilité \cite[p.459]{Marco-Thieullen-Weil}.

\begin{theo}[Continuité de la somme]
	Soit $\left(f_n\right)_{n \in \N}$ une suite de fonctions définies sur $A$ dont la série est uniformément convergente. Soit $a$ un point de $A$? Si chaque fonction $f_n$ est continue en $a$, alors la somme $\sum_{n =0}^{+\infty} f_n$ est continue en $a$.
\end{theo}
{\small{\begin{proof}
	On applique le résultat de la continuité de la limite d'une suite de fonctions à la suite des sommes partielles de la série.
\end{proof}}}

\begin{theo}[Interversion somme-intégrale \protect{\cite[p.223]{Gourdon-analyse}}]
	Si $\sum g_n$ est une série de fonctions continue qui converge normalement sur $[a, b]$, alors 
	\begin{displaymath}
	\int_{a}^{b} \left(\sum_{n= 0}^{\infty} g_n(t)\right) = \sum_{n= 0}^{\infty} \left(\int_{a}^{b} g_n(t)\right)
	\end{displaymath}
\end{theo}
{\small{\begin{proof}
	Comme la convergence normale implique la convergence uniforme, on applique le théorème de convergence sous le signe intégrale pour les suites de fonctions à la suite des sommes partielles. 
\end{proof}}}

\begin{req}
	On peut alléger les hypothèses de  ce théorème et se contenter de la convergence uniforme. Mais dans ce cas, il nous faut aussi l'intégrabilité des fonctions $f_n$.
\end{req}

\begin{req}
	Un tel théorème permet de calculer des intégrales.
\end{req}

\begin{ex}[Calcul d'une intégrale à l'aide d'une série]
	Pour $n \in \N^*$, soit $f_n$ la suite de fonctions définie telle que $f_n(0) = 0$ et $f_n(t) = \frac{t^n \ln t}{n}$ pour tout $t \in ]0, 1]$. Comme la série $\sum f_n$ est normalement convergente (\textblue{par étude de fonctions}), on applique le théorème:
	\begin{displaymath}
	\begin{array}{ccll}
	\int_{0}^{1} \ln t \ln (1-t) dt & = & - \sum_{n=1}^{+\infty} \frac{t^n \ln t}{n} & \text{\textblue{application du théorème}} \\ 
	& = &  \sum_{n=1}^{+\infty} \frac{1}{n(n+1)^2} & \text{\textblue{par IPP}} \\ 
	& = & 2 - \sum_{n=1}^{+\infty} \frac{1}{n^2} & \textblue{\frac{1}{n(n+1)^2} = \frac{1}{n} - \frac{1}{n+1} - \frac{1}{(n+1)^2}} \\ 
	& = & 2 - \frac{\pi^2}{6} & \textblue{ \sum_{n=1}^{+\infty} \frac{1}{n^2} = \frac{\pi^2}{6}} \\ 
	\end{array}
	\end{displaymath}
\end{ex}

\begin{theo}[Dérivation terme à terme]
	Soit $\left(f_n\right)_{n \in \N}$ une suite de fonction de classe $\Cun$ sur le segment $[a, b]$ telle que
	\begin{enumerate}
		\item la série $\sum_{k \geq 0}f_k$ converge simplement sur $[a, b]$;
		\item la série des dérivées $\sum_{k \geq 0}f'_k$ converge uniformément sur $[a, b]$.
	\end{enumerate}
	Alors la série $\sum_{k \geq 0}f_k$ est uniformément convergente sur $[a, b]$ et sa somme est de classe $\Cun$, de dérivée $\sum_{k \geq 0}f'_k$.
\end{theo}
{\small{\begin{proof}
	On applique le théorème de la dérivation d'une limite de suite de fonctions à la suite des sommes partielles de la série.
\end{proof}}}

\begin{ex}
	La fonction exponentielle complexe est de classe $\Cun$ (définie via les séries entières).
\end{ex}