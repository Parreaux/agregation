%LECONs : 182 (Groupes et geometrie)
La géométrie affine est une des première géométrie. La géométrie affine est l'action du groupe des transformations affines (translations, homothéties, ...) sur un espace vectoriel.

\paragraph{Espace et groupe affine} Nous commençons par définir la géométrie par son action de groupe: il nous faut donc le groupe et l'espace sur lequel il agit.

\begin{definition}[\protect{\cite[p.2]{Boyer}}]
	Un \emph{espace affine} $\E$ est un ensemble muni d'une action libre et transitive du groupe-additif sous-jacent à un espace vectoriel $\overrightarrow{\E}$. \textblue{La dimension de $\E$ est celle de $\overrightarrow{\E}$.}
\end{definition}

\emph{Interprétation} \cite[p.9]{Audin}: un espace affine $\E$ est la donnée d'un ensemble $\E$, d'un espace vectoriel $E$ et d'une application $\Theta:$
\begin{tabular}{ccc}
	$\E \times \E$ & $\rightarrow$ & $E$ \\
	$(A, B)$ & $\mapsto$ & $\overrightarrow{AB}$
\end{tabular} 
telle que:
\begin{itemize}
	\item $\forall a \in \E$, l'application partielle $\Theta_{a}$ qui à $b \in \E$ associe $\overrightarrow{ab}$ est une bijection de $\E$ sur $E$;
	\item $\Theta$ vérifie la relation de Chasles.
\end{itemize}

\begin{req}
	On peut aussi voir un espace affine comme un espace vectoriel sans origine (ni aucun point particulier).
\end{req}

\begin{ex}
	\begin{itemize}
		\item $\emptyset$ est un espace affine. \textblue{Il n'a pas de dimension.}
		\item Tout espace vectoriel a une structure (canonique) d'espace affine.
	\end{itemize}
\end{ex}

\begin{definition}[\protect{\cite[p.3]{Boyer}}]
	Un sous-ensemble $\F$ de $\E$ est un \emph{sous-espace affine} s'il est vide ou s'il contient un point $a \in \F$ tel que $\overrightarrow{\F} = \Theta_{a}(\F)$ est un sous-espace vectoriel de $\overrightarrow{\E}$.
\end{definition}

\begin{definition}[\protect{\cite[p.16]{Audin}}]
	Une application $\phi : \E \rightarrow \F$ est dite \emph{affine} s'il existe un point $O$ dans $\E$ et une application linéaire $\overrightarrow{f} : \overrightarrow{\E} \rightarrow \overrightarrow{\F}$ tels que $\forall M \in \E, \overrightarrow{f}(\overrightarrow{OM}) = \overrightarrow{\phi(O)\phi(M)}$.
\end{definition}

\begin{req}
	L'application linéaire $f$ ne dépend pas du point $O$ choisi.
\end{req}

\begin{ex}
	\begin{itemize}
		\item Une homothétie est une application affine. \textblue{Homothétie: $\overrightarrow{O\phi(M)} = \lambda \overrightarrow{OM}$ soit $f = k~id$}
		\item Les translations sont des applications affines \textblue{Translation: $f = id$}.
	\end{itemize}
\end{ex}

\begin{definition}[\protect{\cite[p.9]{Boyer}}]
	Le \emph{groupe affine} $GA(\E)$ est l'ensemble des bijections affines de $\E$ (pour la loi de composition). \textblue{Ceci est vrai car la composition de deux applications affines et la réciproque d'une application affine restent affines.}
\end{definition}

\begin{req}
	Il est engendré par les translations, les transvections et les homothéties. Il opère sur $\overrightarrow{\E}$ fidèlement et transitivement \cite[p.22]{Laville}. 
\end{req}

\paragraph{Stabilisateurs du groupe affine} Les invariants de ce groupes sont biens connus \cite[p.331]{Caldero-Germoni}. Il préserve: l'alignement (les applications affines préservent l'alignement); le parallélisme (les bijections affines préservent la directions. Est-ce vrai pour les applications?) et le barycentre (les applications affines préservent les barycentres).
	
\begin{prop}[\protect{\cite[p.18]{Audin}}]
	L'image (ou l'image réciproque) d'un sous-espace affine par une application affine est un sous-espace affine. 
\end{prop}
\begin{footnotesize}
	\begin{proof}
		Distinction du cas vide (trivial) et non-vide (application linéaire sur le sous-espace vectoriel sous-jacent).
	\end{proof}
\end{footnotesize}
		
\begin{req}
	Interprétation géométrique: les applications affines préservent les sous-espaces affines. En particulier, elles préservent les alignements.
\end{req}

\begin{definition}[\protect{\cite[p.17]{Boyer}}]
	Si $(a_1, \alpha_1), ..., (a_n, \alpha_n)$ est un système de point pondérés tels que $\sum_{i=1}^{n} \alpha_i \neq 0$, alors il existe un unique point $g$ tels que $\sum_{i=1}^{n} \alpha_i \overrightarrow{ga_i} \neq \overrightarrow{0}$. Ce point s'appelle le \emph{barycentre} du système.
\end{definition}
\begin{footnotesize}
	\begin{proof}
		On définit $g$ comme suit $(\sum_{i=1}^{n} \alpha_i) \overrightarrow{og} = \sum_{i=1}^{n} \alpha_i \overrightarrow{oa_i}$ et on applique Chasles.
	\end{proof}
\end{footnotesize}
		
\begin{prop}
	Une application $f : \E \rightarrow \F$ est affine si et seulement si elle conserve les barycentre.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		\begin{description}
			\item[$\Rightarrow$] $f$ affine: application de la définition et de la linéarité de $f$.  
			\item[$\Leftarrow$] $\overrightarrow{f}(\overrightarrow{u}) = f(a + \overrightarrow{u}) - f(a)$ (uniquement déterminée). On remarque que $a + \lambda\overrightarrow{u} + \mu\overrightarrow{u}$ est le barycentre $(a, 1-\lambda-\mu), (a+\overrightarrow{u}, \lambda), (a+\overrightarrow{u}, \mu)$ et on applique les définitions à $\overrightarrow{f}$ pris au barycentre.
		\end{description}
	\end{proof}
\end{footnotesize}
		
\begin{req}
	Interprétation géométrique: les barycentres sont préservé par le groupe affine.
\end{req}

\begin{prop}
	L'application du groupe affine dans le groupe linéaire qui envoie une application affine sur son application linéaire associée, est un homomorphisme \textblue{(un morphisme)} surjectif de groupes, dont le noyau est le groupe des translations de $\E$, est isomorphe au groupe additif de l'espace vectoriel de $E$.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		Les applications affines associée à $id_{E}$ sont les translations et $t_{u+v} = t_u \circ t_v$.
	\end{proof}
\end{footnotesize}

\begin{req}
	Interprétation géométrique \cite[p.9]{Boyer} : le groupe des translations est distingué dans le groupe affine. Une transformation affine $f$ commute avec une transvection du vecteur $u$ si et seulement si $f$ laisse stable $u$.
\end{req}
		
\paragraph{Classification des figures affines} Les actions de groupes (et en particulier dans cette géométrie), nous permettent de classer les figures selon leurs orbites. Généralement ce sont de bons outils pour faire des preuves sur des objets géométriques: avec une action bien choisie on se ramène à un cas "facile". Cette action doit préserver les propriétés de notre théorème.

\subparagraph{Action sur les triplets de points dans $\R^{2}$ \cite[p.331]{Caldero-Germoni}} 

\begin{prop}
	Le groupe affine de dimension 2 sur $\R$ agit simplement et transitivement sur les triplets de $\R^{2}$.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		On passe par les repères pris comme base de $\R^{2}$ et on identifie l'action canoniquement sur la base de $\R^{2}$.
	\end{proof}
\end{footnotesize}

\begin{appli}
	L'ellipse de Steiner. Soit $ABC$ un triangle du plan (non plat) et $A'$, $B'$, $C'$ les milieux des côtés $[BC], [AC], [AB]$. Alors, il existe une ellipse tri-tangente aux points $A', B', C'$.
\end{appli}
\begin{footnotesize}
	\begin{proof}[Idée de la preuve]
		Idée de la preuve: utiliser une action (définit par la proposition précédente) pour se ramené au cas équilatéral (qui est évident) et conclure.
	\end{proof}
	\begin{proof}
		Preuve: $A_0B_0C_0$ un triangle équilatéral pour lequel le problème est clair. $g$ l'action dont l'existence est assurée précédemment telle que $g(A_{0}) = A$, $g(B_0) = B$ et $g(C_0) = C$. Alors
		\begin{itemize}
			\item les milieux sont conservés (conservation du barycentre);
			\item  l'image du cercle est une conique (changement de variable linéaire préserve le degré), compacte ($g$ continue) donc c'est une ellipse;
			\item $g$ est différentiable donc la tangente est préservée.
		\end{itemize}
	\end{proof}
\end{footnotesize}
			
\subparagraph{Action sur les sous-espaces \cite[p.14]{Audin}} 

\begin{prop}[\protect{\cite[p.49]{Laville}}]
	Soit $\E$ un espace affine de dimension $2$, $\mathcal{D}$ l'ensemble des droites de $\E$. Le groupe affine définit sur $\E$ opère sur $\mathcal{D} \times \mathcal{D}$, non transitivement et il définit trois orbites dans $\mathcal{D} \times \mathcal{D}$:
	\begin{itemize}
		\item l'orbite des couples de droites d'intersections vides;
		\item l'orbite des couples de droites confondues;
		\item l'orbites des couples de droites d'intersection réduite à un point.
	\end{itemize}
\end{prop}
\begin{footnotesize}
	\begin{proof}
		Une application est complètement déterminée par l'image d'un point et l'image d'une base.
	\end{proof}
\end{footnotesize}
			
\begin{req}
	On peut généraliser au cas $d \geq 2$.
\end{req}

\begin{definition}
	On dit que deux sous-espaces affines $\F$ et $\mathcal{G}$ de $\E$ sont parallèles s'ils ont la même direction.
\end{definition}
			
\begin{req}
	\emph{Interprétation} : Le groupe affine préserve le parallélisme. \textblue{C'est un invariant du groupe.}
\end{req}
			
\subparagraph{Action du groupe affine de dimension $d$ sur lui même \cite[p.57]{Laville}} 

\begin{prop}
	Soit $\E$ un espace affine de dimension supérieur ou égale à $2$. Le groupe affine de $\E$ agit sur $\E^{3}$, et il y a plusieurs types d'orbites:
	\begin{enumerate}
		\item une orbite pour tous les triplets de points non alignés;
		\item les orbites de points alignés, deux à deux distincts, qui peuvent être paramétrés par un élément de $K \setminus \{0\}$;
		\item l'orbite des triplets de points confondus;
		\item l'orbite de triplets de points dont deux sont confondus.
	\end{enumerate}
\end{prop}
\begin{footnotesize}
	\begin{proof}
		\begin{enumerate}
			\item Soient $(a, b, c), (a', b', c')$ deux triplets de points non-alignés. Soit $\overrightarrow{f} \in \textsc{GL}(\overrightarrow{\E})$ telle que $\overrightarrow{f}(\overrightarrow{ab}) = \overrightarrow{a'b'}$ et $\overrightarrow{f}(\overrightarrow{ac}) = \overrightarrow{a'c'}$ (car $\dim \overrightarrow{\E} \geq 2$). On pose $f \in \textsc{GA}(\E)$ telle que $f(a) = a'$ et $\forall x \in \E$, $f(x) = a' + \overrightarrow{f}(\overrightarrow{ax})$. On a bien $f(b) = b'$  et $f(c) = c'$. Donc ils sont dans la même orbite.
			\item Si $(a, b, c)$ sont aligné et $(a', b', c')$ sont dans la même orbites, alors $(a', b', c') = (f(a), f(b), f(c))$ pour $f \in \textsc{GA}(\E)$ d'où l'alignement de $(a', b', c')$. De plus, il existe $\lambda \in \R \setminus \{0\}$ tel que $\lambda \overrightarrow{ab} + \overrightarrow{ac} =\overrightarrow{0}$ et  il existe $\lambda' \in \R \setminus \{0\}$ tel que $\lambda' \overrightarrow{a'b'} + \overrightarrow{a'c'} =\overrightarrow{0}$. Soit $\overrightarrow{f}$ l'application linéaire associée à $f$: $\overrightarrow{f}(\overrightarrow{ab}) = \overrightarrow{a'b'}$ et $\overrightarrow{f}(\overrightarrow{ac}) = \overrightarrow{a'c'}$. Comme elle est linéaire, $\lambda' \in \R \setminus \{0\}$ tel que $\lambda \overrightarrow{a'b'} + \overrightarrow{a'c'} =\overrightarrow{0}$ et $\lambda = \lambda'$. Réciproquement, si $(a, b, c)$ sont alignés de paramètre $\lambda$ et $(a', b', c')$ sont alignés de paramètre $\lambda'$, on peut trouver $f \in \textsc{GA}(\E)$ telles que $f(a) = a'$ et $f(b) =b'$. Ainsi les triplets sont dans la même orbite.
			\item Par translation.
			\item Par existence de $f \in \textsc{GA}(\E)$ telles que $f(a) = a'$ et $f(b) =b'$. 
		\end{enumerate}
	\end{proof}
\end{footnotesize}
			
\begin{req}
	On peut généraliser le résultat à $\E{d+1}$ si $\E$ est de dimension $d$ à l'aide des repères affines. Interprétation géométrique: rapport affine, c'est l'invariant fondamental de cette géométrie.
\end{req}

\begin{definition}
	On appelle rapport de trois points alignés $a, b, c$ ($a \neq b$), l'élément $k \in \R$ tel que $\overrightarrow{ac} = k \overrightarrow{ab}$ et on le note $k = \frac{\overline{ac}}{\overline{ab}}$.
\end{definition}
				
\begin{appli}[Théorème de Thalès. \protect{\cite[p.13]{Boyer}}]
	Soient $\mathcal{H}_1$, $\mathcal{H}_2$, $\mathcal{H}_3$ trois hyperplans parallèles distincts et une droite $\mathcal{D}$ non parallèle à ces hyperplans. Pour $i = 1, 2, 3$, on note $a_i = \mathcal{D} \cap \mathcal{H}_{i}$ Le rapport de $a_1, a_2, a_3$ ne dépend pas de la droite $\mathcal{D}$.
\end{appli}
\begin{footnotesize}
	\begin{proof}
		Projection affine ou écriture de la notion d'hyperplan et calcul des coefficients de proportionnalité à la main.
	\end{proof}
\end{footnotesize}
				
\begin{appli}
	Théorème de Pappus affine et Desargues affine ?
\end{appli}


