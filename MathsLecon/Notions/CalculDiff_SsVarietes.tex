%LECONs: 214 (TIL, TFI); 219 (Extremums)
Les sous-variétés sont une notion permettant d'uniformiser la théorie autours des objets appelés surface (parabole, ellipse, cylindre, tore, $\dots$) \cite[p.197]{Rouviere}. Elle se concentre sur l'aspect local des sous-ensembles de $\R^n$: ce sont des sous-espaces affines propres qui ont été transformés.

\paragraph{Sous-variétés}

\begin{definition}
	Soient $V$ un sous-ensemble de $\R^n$, $a \in V$, et $d \in \N$. On dit que $V$ est lisse en $a$, de dimension $d$, s'il existe un difféomorphisme $F$ de classe $\Ck^1$ d'un voisinage ouvert $U$ de $a$ dans $\R^n$ sur le voisinage ouvert $F(U)$ de $0$ dans $\R^n$, qui transforme $V$ en un sous-espace vectoriel de dimension $d$ : $F(V \cap U) = V' \cap F(U)$ où $V' = \R^d \times \{0\} \subset \R^n$.
	
	On dit que $V$ est une sous-variété de dimension $d$ de $\R^n$ si $V$ est lisse (de dimension $d$) en chacun de ces points.
\end{definition}

\begin{req}
	Une sous-variété se ramène localement à un sous-espace vectoriel par simple changement de coordonnées. Par définition, ces notions (sous-variété, lisse) sont invariantes par difféomorphisme. On peut étendre la définition pour un $\Ck^k$-difféomorphisme.
\end{req}

\begin{theo}[Théorème des sous-variétés]
	Soient $V$ un sous-ensemble de $\R^n$, $a \in V$ et $d$ un entier naturel. Les quatre propriétés suivantes sont équivalentes:
	\begin{enumerate}
		\item (carte locale) $V$ est lisse en $a$, de dimension $d$ \textblue{(il existe un $\Ck^p$-difféomorphisme local $\varphi : U \to \R^n$ tel que $\varphi(V \cap U) = \varphi(U) \cap \left[\R^k \times \{0\}^{n- k}\right]$)}.
		\item (équation) Il existe un voisinage ouvert $U$ de $a$ dans $\R^n$ et $n - d$ fonctions $f_i : U \to \R$ de classe $\Ck^1$, telles que $(x \in V \cap U) \Leftrightarrow (x \in U \text{ et } f_1(x_1, \dots, x_n) = 0, \dots, f_n(x_1, \dots, x_n) = 0)$ et les différentielles $Df_1(a), \dots, Df_{n-d}(a)$ sont indépendantes \textblue{(existence d'une fonction surjective telle que son image réciproque en $0$ soit $U \cap V$)}.
		\item (graphe) Il existe un voisinage ouvert $U$ de $a$ dans $\R^n$, un voisinage ouvert $U'$ de $(a_1, \dots, a_d)$ dans $\R^d$ et $n - d$ fonctions $g_i : U' \to \R$ de classe $\Ck^1$, telles que (après permutation éventuelle des coordonnées) $(x \in V \cap U) \Leftrightarrow \left\{ \begin{array}{l}
		(x_1, \dots, x_d) \in U' \text{ et} \\
		x_{d+1} = g_1(x_1, \dots, x_d), \dots, x_n = g_{n-d}(x_1, \dots, x_d) \\
		\end{array}  \right.$ \textblue{(consiste en un changement de coordonnées donné par les $g_i$)}.
		\item (nappe paramétrée) Il existe un voisinage ouvert $U$ de $a$ dans $\R^n$, un voisinage ouvert $\Omega$ de $(0$ dans $\R^d$ et $n$ fonctions $\varphi_i : \Omega \to \R$ de classe $\Ck^1$, telles que l'application $\varphi : u =(u_1, \dots, u_d) \mapsto x = (\varphi_1(u), \dots, \varphi_n(u))$ soit un homéomorphisme de de $\Omega$ sur $V \cap U$, avec $a = \varphi(0)$ et que la matrice jacobienne $D\varphi(0)$ soit injective (de rang $r$) \textblue{($\varphi$ est bijective et bi-continue dont la différentielle en $0$ est injective)}.
	\end{enumerate}
\end{theo}
\begin{footnotesize}
	\begin{proof}[Idée de la preuve]
		\begin{description}
			\item[Implication $1 \Rightarrow 2$] On pose $f_i = x_{d+1}(\varphi(x))$. On montre l'indépendance linéaire via le calcul d'une composée de différentielle.
			\item[Implication $2 \Rightarrow 3$] Application du théorème des fonctions implicites pour résoudre le système d'équations $f_1(x) = \dots = f_{n-d}(x) = 0$ par rapport à $x_{d+1}, \dots, x_n$.
			\item[Implication $3 \Rightarrow 1$] On pose $\varphi : x = (x_1, x_2) \mapsto (x_1, x_2 - u(x_1))$ où $u$ est le vecteur composé des $g_i$. On conclut en appliquant le théorème de la différentielle des fonctions composées.
			\item[Implication $3 \Rightarrow 4$] On pose $\varphi : \tilde{z} \mapsto (z_0 + \tilde{z}, u(z_0 + \tilde{z}))$ où $u$ est le vecteur composé des $g_i$.
			\item[Implication $4 \Rightarrow 3$] Application du théorème d'inversion locale pour inverser $\left(\varphi_1, \dots, \varphi_d\right)$ ce qui nous donne $u_1, \dots, u_d$ fonctions de $x_1, \dots, x_d$. Ensuite on les reporte dans $\left(\varphi_{d + 1}, \dots, \varphi_n\right)$ pour avoir $x_{d + 1}, \dots, x_n$ fonction de $x_1, \dots, x_d$.
		\end{description}
	\end{proof}
\end{footnotesize}


\paragraph{Espace tangent}

\begin{definition}
	Soient $V$ un sous-ensemble de $\R^n$ et $a \in V$. Un vecteur $v \in \R^n$ est dit tangent en $a$ à $V$ s'il existe une fonction dérivable $\gamma : I \to \R^n$ où $I$ est un intervalle ouvert autours de $0$ telle que $\gamma(I) \subset V$, $\gamma(0) = a$ et $\gamma'(0) = v$.
\end{definition}

\begin{theo}
	Si $V$ est lisse en $a$, de dimension $d$, ses vecteurs tangents en $a$ forment un sous-espace vectoriel de dimension $d$, appelé espace vectoriel tangent en $a$ à $V$, noté $T_a V$.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		Ce résultat n'a rien d'évident avec cette définition. Si on utilise la caractérisation par équation, on obtient le résultat par noyau d'une application linéaire. Le point difficile de la preuve "classique" de ce résultat est la stabilité par addition \cite[p.749]{Marco-Thieullen-Weil}. La fonction ainsi définie nous permet également de donnée une base.
	\end{proof}
\end{footnotesize}

\begin{req}
	On utilise souvent le plan tangent affine, parallèle au précédent mais passant par $a$.
\end{req}

\begin{theo}[Caractérisation du plan tangent]
	Soient $V$ un sous-ensemble de $\R^n$, $a \in V$ et $d$ un entier naturel. En utilisant les notations du théorème des sous variétés, les quatre propriétés suivantes sont équivalentes:
	
	\begin{minipage}{.5\textwidth}
		\begin{enumerate}
			\item (carte locale) $T_a V = d\varphi(x_0)^{-1}\left(\R^k \times \{0\}^{n-k}\right)$
			\item (équation) $T_a V = \ker(Df(a))$
			\item (graphe) $T_a V = \{(h, Dg(a_1, \dots, a_d)), h \in \R^n\}$
			\item (nappe paramétrée) $T_a V = D\varphi(O)(\R^n)$
		\end{enumerate}
	\end{minipage} \hfill
	\begin{minipage}{.46\textwidth}
		\begin{footnotesize}
			\begin{proof}
				\begin{description}
					\item[Carte locale] On construit des chemins dans $\R^k \times \{0\}^{n - k}$
					\item[Nappe paramétrée]$\beta : t \mapsto j^{-1} \circ \gamma(t)$ est un chemin. $\gamma : t \mapsto \varphi(tw)$ où $v = D\varphi(0)(w)$.
					\item[Graphe et équation] On utilise la preuve du théorème des sous-variétés.
				\end{description}
			\end{proof}
		\end{footnotesize}
	\end{minipage}
	
\end{theo}



