%LECON : 121
Nous allons donner quelques résultats de base sur les nombres premiers \cite[p.95]{ElAmrani}. Ceux-ci sont essentiellement centré sur l'arithmétique qui en découle.

\begin{definition}[Nombre premier]
	On appelle nombre premier tout entier naturel différent de $1$ n'admettant pour diviseurs positifs que $1$ et lui-même.
\end{definition}

\begin{req}
	Dans le cas d'un entier relatif, on peut dire qu'il est entier si sa valeur absolue l'est. 
\end{req}

\begin{definition}[Nombre composé]
	Un entier $n \geq 2$ est dit composé s'il n'est pas premier.
\end{definition}

Voici quelques résultats de divisibilité sur les nombres premiers. Ils n'ont pas nécessairement leur place dans la leçon 121 mais il est important de s'en souvenir car ils permettent de montrer d'autres résultats importants.

\begin{prop}
	Si $p$ est un nombre premier, alors $p$ est premier avec tous les entiers qu'il ne divise pas. En particulier, on a $\forall k \in \llbracket 1, p-1 \rrbracket$, $k \wedge p = 1$.
\end{prop}
{\footnotesize{\begin{proof}
	Soit $k \in \N$ tel que $k$ est non multiple de $p$. Soit $d$ un diviseur commun à $p$ et à $k$. Alors, comme $p$ est premier, $d = 1$ ou $d = p$. Or par hypothèse, $k$ n'est pas multiple de $k$ donc $d =1$. On en déduit que $k \wedge d =1$.
\end{proof}}}

\begin{cor}
	Deux nombres premiers distincts sont premiers entre eux.
	\label{cor:divisibiliteNbPremier}
\end{cor}
{\footnotesize{\begin{proof}
	On peut supposer $p < q$. Comme $q$ est premier distinct de $p$, $p$ ne divise pas $q$ soit $q$ n'est pas multiple de $p$. Comme $p$ est premier, la proposition précédente nous permet de conclure.
\end{proof}}}

Nous présentons maintenant les lemmes de Gauss et d'Euclide qui nous donne des résultats de divisibilités plus conséquent.

\begin{theo}[Lemme de Gauss \protect{\cite[p.14]{ElAmrani}}]
	Soient $a$, $b$, $c$ dans $\Z$. Si $a$ divise le produit $bc$ et si $a$ est premier avec $b$, alors $a$ divise $c$.
	\label{theo:lemmeGauss}
\end{theo}
{\footnotesize{\begin{proof}
	Puisque $a \wedge b = 1$, alors d'après le théorème de Bézout, il existe $(u, v) \in \Z^2$ tels que $au+bv =1$. En multipliant par $c$, on obtient alors $acu + bvc = c$. Comme, par hypothèse, $a$ divise $bc$ alors $a$ divise $bcv$. En particulier $a$ divise $acu + bcv$. Donc $a$ divise $c$.
\end{proof}}}

\begin{prop}[Lemme d'Euclide]
	Un nombre premier $p$ divise un produit $ab$ de nombres entiers si, et seulement si, $p$ divise $a$ ou $p$ divise $b$.
\end{prop}
{\footnotesize{\begin{proof}
	Si $p$ ne divise ni $a$ ni $b$ alors, d'après le corollaire~\ref{cor:divisibiliteNbPremier}, $p$ est premier avec $a$ et avec $b$. Donc d'après la contraposée du lemme de Gauss (théorème~\ref{theo:lemmeGauss}), $p$ est premier avec $ab$. Donc $p$ ne divise pas ce produit. Contradiction.
	
	Réciproquement, si $a = kp$ ou $b = k'p$ avec $k, k' \in \Z$, alors $ab = kpb$ ou $ab = ak'p$ et $p$ divise le produit.
\end{proof}}}

Nous introduisons maintenant un résultat important sur les nombres premiers: la décomposition en facteurs premier de tout entier naturel. Nous allons énoncer les résultats sur cette décomposition et leur application au pgcd et ppcm.

\begin{prop}
	Tout entier supérieur ou égal à $2$ admet un diviseur premier.
	\label{prop:tousDivPremier}
\end{prop}
{\footnotesize{\begin{proof}
	Soit un entier $n \geq 2$. L'ensemble des diviseurs de $n$ strictement supérieur à $1$ est une partie non vide de $\N$ \textblue{(elle contient au moins $n$)}, elle admet donc un plus petit élément que l'on note $p$.
	
	Montrons que $p$ est premier. Raisonnons par l'absurde et supposons que $p$ admet un diviseur. Comme $p$ est un diviseur de $n$, son diviseur divise également $n$. On obtient une contradiction sur la minimalité de $p$ en tant que diviseur de $n$. Donc $p$ est premier.
\end{proof}}}

\begin{theo}[Théorème fondamental de l'arithmétique]
	Soit $n$ un entier supérieur ou égal à $2$. Alors, $n$ admet une décomposition en produit de facteurs premiers $n = q_1\dots q_k$ où $q_1$, \dots, $q_n$ sont des nombres premiers. On peut également écrire cette décomposition sous la forme
	\begin{displaymath}
	n = p_1^{\alpha_1}\dots p_k^{\alpha_k}
	\end{displaymath}
	où les $\left(p_i\right)_{1 \leq i \leq k}$ sont des entiers premiers distincts et les $\left(\alpha_i\right)_{1 \leq i \leq k}$ sont des entiers naturels non nuls.
	
	De plus, cette décomposition est unique à l'ordre des facteurs près.
	\label{theo:fondaArithmetique}
\end{theo}
{\footnotesize{\begin{proof}
	Soit $n$ un entier supérieur ou égal à $2$.
	
	\subparagraph{Existence} On raisonne par récurrence sur $n$. On montre la propriété $H_n$ : "tout entier compris entre $2$ et $n$ admet une décomposition en produit de facteurs premiers".
	\begin{itemize}
		\item $H_2$ est vrai car $2$ est un nombre premier.
		\item Soit $n \geq 2$ tel que $H_{n-1}$ est vérifier. Montrons que $n$ admet une décomposition en facteurs premier ce qui prouvera $H_n$ (\textblue{pour le reste des nombres strictement inférieur à $n$, on applique l'hypothèse de récurrence $H_{n-1}$}).
		
		Si $n$ est premier, alors on a gagné puisque c'est une décomposition en un seul facteur.
		
		Sinon, d'après la proposition~\ref{prop:tousDivPremier}, $n$ admet un diviseur premier $p$. On peut alors écrire $n = pq$ avec $2 \leq q < n$. En appliquant l'hypothèse de récurrence à $q$, on peut le décomposer en produit de facteurs premiers $q = q_1 \dots q_k$. On en déduit que $n = p q_1 \dots q_k$ qui est une décomposition en facteurs premiers.
	\end{itemize}
	D'où le résultat.
	
	\subparagraph{Unicité} Soit $n = q_1 \dots q_k$ une décomposition en facteur premier. Alors chaque $q_i$ divise $n$ et réciproquement si un nombre premier $p$ divise $n$ alors il divise un $q_i$ et ils sont donc égaux (\textblue{ce sont des nombres premiers}). Les facteurs premiers figurant dans une telle décomposition sont alors tous des diviseurs premiers de $n$ (\textblue{et il n'en existe pas d'autres}).
	
	Considérons alors deux décomposition en facteurs premiers de $n$: 
	\begin{displaymath}
		\begin{array}{ccc}
			n = p_1^{\alpha_1} \dots p_r^{\alpha_r} & \text{ et } &  n = p_1^{\beta_1} \dots p_r^{\beta_r}\\
		\end{array}
	\end{displaymath}
	où les $p_i$ sont premiers distincts.
	
	Supposons qu'il existe $i \in \llbracket 1,r \rrbracket$ tel que $\alpha_i \neq \beta_i$ (par exemple $\alpha_i < \beta_i$), alors
	\begin{displaymath}
	\prod^{r}_{\underset{j \neq i}{j= 1}} p_j^{\alpha_j} = p_i^{\beta_i - \alpha_i} \prod^{r}_{\underset{j \neq i}{j= 1}} p_j^{\beta_j}
	\end{displaymath}
	On en déduit que $p_i$ divise $\prod^{r}_{\underset{j \neq i}{j= 1}} p_j^{\alpha_j}$, ce qui est impossible car si $i \neq j$, les $p_i$ et les $p_j$ sont premiers entre eux (\textblue{puisqu'ils sont premiers}). On en déduit que $\forall i \in \llbracket 1,r \rrbracket$, $\alpha_i = \beta_i$. On obtient alors l'unicité de la décomposition.	
\end{proof}}}

\begin{ex}
	On a $280 = 2^3 \times 5 \times 7$ et $8325 = 3^2 \times 5^2 \times 37$.
\end{ex}
