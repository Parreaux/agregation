%LECONs: 120 (Anneaux ZnZ); 121 (Nombres premiers); 123 (Corps finis)
Un problème intéressant lorsqu'on étudie les nombres premiers est de savoir si lorsque l'on se donne un nombre entier quelconque, on a en réalité un nombre premier ou non \cite[p.189]{Lamoitier} \textblue{(cette référence contient tous les tests que l'on souhaite, on laisse les autres au cas où)}. Ce problème loin d'être évident à été déterminer dans $P$, il y a seulement quelques années. Nous avons de nombreux test plus ou moins efficaces qui sont discriminatoire (ils ne donnent jamais une réponse certaine lorsqu'on passe le test) \cite[p.40]{Hindry}, \cite[p.888]{Cormen}. Ces tests cherchent les entiers qui ne sont pas premiers avec plus ou moins de précision. Nous allons en étudier quelques uns en donnant les théorèmes caractérisant les nombres premiers (qui n'ont pas d'équivalence), puis les algorithmes et enfin les familles de nombres qui les fond échoué. Les tests permettant de dire si un nombre est premier ou non peuvent se classer en cinq catégories:
\begin{enumerate}
	\item les tests classiques : crible d'Erathosthènes, méthode naïve, ...
	\item les tests spécifiques à une catégorie de nombres : Mersenne ou Fermat
	\item les tests déterministes rapide
	\item les tests probabilistes
	\item les autres ...
\end{enumerate}

\paragraph{Autour du théorème de Wilson} Le mathématicien John Wilson énonce une condition nécessaire et suffisant pour savoir si un nombre est premier ou non. Cette condition bien que plus pratique théoriquement ne donne pas de bon résultat en pratique. En effet, cette caractérisation nous ramène à calculer $(p-1)!$ ce qui est très grands. Cependant ce test ne possède aucun faux positif (on ne l'utilise pas car il est de complexité exponentielle). Ce résultat est souvent utilisé pour montrer d'autres théorèmes (et caractérisations partielles) pour les nombres premiers

\begin{theo}[Théorème de Wilson]
	Un entier $p \geq 2$ est un nombre premier si et seulement si $(p-1)! + 1 \equiv 0 [p]$
\end{theo}
{\small{\begin{proof}
	\emph{Condition nécessaire}. Si $p = 2$ ou $p = 3$, c'est évident. Pour traiter le cas $p > 3$, on cherche les éléments du groupe $\left(\Z/p\Z\right)^*$ égaux à leur inverse. Ces éléments vérifient $x^2 = \overline{1}$, soit $\left(x - \overline{1}\right)\left(x + \overline{1}\right) = \overline{0}$. Les seuls éléments de $\left(\Z/p\Z\right)^*$ égaux à leur inverse sont donc $x = \overline{1}$ et $x = \overline{-1}$. On range les autres $\overline{2}, \overline{3}, \dots, \overline{p-2}$ en $\frac{p-3}{2}$ paires d'éléments $\{x_i, y_i\}$ telle que$x_iy_i = \overline{1}$. En posant $k = \frac{p-3}{2}$, on obtient
	\begin{displaymath}
	 \overline{2} \times \overline{3} \times \dots \times \overline{p-2} = \prod_{i=1}^{k} \left(x_iy_i\right) = \overline{1}
	\end{displaymath}
	Donc $(p-1)! \equiv p-1 \equiv -1 [p]$.
	
	\emph{Condition suffisante}. Supposons $p$ non premier, et notons $a$ un diviseur de $p$ vérifiant $1 < a < p$. Alors $a$ divise $(p -1)! + 1$ par hypothèse et divise $(p-1)!$ puisque $1 < a < p$. Donc $a$ divise $1$. Contradiction.
\end{proof}}}

\paragraph{Autour du théorème de Fermat} La première caractérisation des nombres premiers a été donnée par Fermat suite à son théorème: le petit théorème de Fermat \cite[p.101]{ElAmrani}. Il en découler un test qui est mis en échec via les nombres de Carmichaël.

\begin{lemme}
	Soit $p$ un nombre premier. Alors, pour tout $n \in \llbracket 1, p-1 \rrbracket$, $p$ divise $\binom{p}{n}$. En particulier, pour tous $a, b$ dans $\Z$, on a $(a+b)^{p} \equiv a^p + b^p [p]$.
\end{lemme}
{\footnotesize{\begin{proof}
	On a 
	\begin{displaymath}
	\binom{p}{n} = \frac{p!}{n!(p-n)!} = \frac{p}{n} \frac{(p-1)!}{(n-1)!(p-n)!} = \frac{p}{n}\binom{p-1}{n-1}
	\end{displaymath}
	d'où $n\binom{p}{n} = p\binom{p-1}{n-1}$. On a alors $p$ qui divise $n\binom{p}{n}$. Comme $p$ est premier avec $n$, par le théorème de Gauss, $p$ divise $\binom{p}{n}$.
	
	D'après la formule du binôme de Newton, on a pour tous $a$, $b$ dans $\Z$:
	\begin{displaymath}
	(a+b)^p = \sum_{n=0}^{p} \binom{p}{n}a^nb^{p-n} = a^p + b^p + \sum_{n=0}^{p-1} \binom{p}{n}a^nb^{p-n}
	\end{displaymath}
	et d'après ce qui précède, on en déduit que $(a+b)^{p} \equiv a^p + b^p [p]$.
\end{proof}}}

\begin{theo}[Petit théorème de Fermat]
	Soit $p$ un nombre premier. Alors $\forall a \in \Z,$ $a^p \equiv a [p]$.
\end{theo}
{\footnotesize{\begin{proof}
	On raisonne par récurrence sur $a \in \N$.
	\begin{description}
		\item[Cas $a = 0$] La formule est vraie.
		\item[Cas $a \in \N^*$] On suppose que $a$ vérifie $a^p \equiv a [p]$. Par la relation précédente avec $b = 1$, on a $(a+1)^p \equiv a^p +1 [p]$. Par l'hypothèse de récurrence, on a $(a+1)^p \equiv a +1 [p]$. D'où le résultat dans $\N$.
	\end{description}
	Soit maintenant $a < 0$, alors $-a > 0$. D'où d'après ce qui précède, on a $(-1)^pa^p = (-a)^p \equiv -a [p]$. On distingue deux cas:
	\begin{itemize}
		\item Si $p > 2$, comme $p$ est premier, $p$ est nécessairement impaire. D'où $-a^p \equiv -a [p]$, ce qui conclut.
		\item Si $p = 2$, alors $a^2 \equiv -a [2]$ et comme $1 \equiv -1 [2]$, on en déduit que $a^2 \equiv a [2]$, ce qui conclut.
	\end{itemize}
\end{proof}}}

\begin{req}
	La réciproque est fausse dans le cas général. Prenons par exemple $2^{341} \equiv 2 [341]$ mais $341 = 11 \times 31$ n'est pas premier.
\end{req}

\begin{cor}[Petit théorème de Fermat]
	Soit $p$ un nombre premier. Alors $\forall a \in \Z$ tel que $a$ soit premier avec $p$, on a $a^{p-1} \equiv 1 [p]$.
\end{cor}
{\footnotesize{\begin{proof}
	Soit $a$ premier avec $p$. Alors par le théorème précédent, $a^p \equiv a [p]$. Or comme $a$ est premier avec $p$, $a$ est inversible et on obtient $a^{p-1} \equiv 1 [p]$ en appliquant l'inversion.
\end{proof}}}

\begin{ex}
	Si $p$ est un nombre premier, alors $\sum\limits_{k=1}^{p-1} k^{p-1} \equiv -1 [p]$. En effet, puisque $p$ est premier et que $k \in \llbracket 1, p-1 \rrbracket$, alors $p$ ne divise pas $k$ et le petit théorème de Fermat entraîne que $k^{p-1} \equiv 1 [p]$. Ainsi, $\sum\limits_{k=1}^{p-1} k^{p-1} \equiv p-1 \equiv -1 [p]$.
\end{ex}

Le résultat que nous allons énoncé est à la base du système de cryptographie RSA.
\begin{theo}[Petit théorème de Fermat étendu]
	Soient $p$ et $q$ deux nombres premiers distincts. Alors, pour tout entier $a$, $(a \wedge (pq) = 1) \Rightarrow \left(a^{(p-1)(q-1)} \equiv 1 [pq]\right)$.
\end{theo}
{\footnotesize{\begin{proof}
	Puisque $p$ et $q$ sont premiers et que $a$ est premier avec le produit $pq$, alors $a$ est premier avec $p$ et avec $q$. Le petit théorème de Fermat nous donne alors, $a^{p-1} \equiv 1 [p]$ et $a^{q-1} \equiv 1 [q]$. On en déduit alors que $a^{(p-1)(q-1)} \equiv 1 [p]$ et $a^{(p-1)(q-1)} \equiv 1 [q]$.
	
	Notons $m = a^{(p-1)(q-1)}$. On a $m \equiv 1 [p]$ et $m \equiv 1 [q]$. Il existe donc $k$ et $k'$ deux entiers tels que $m -1 = kp$ et $m - 1 = k'q$. On en déduit que $kp = k'q$ ce qui montre que $p$ divise $k'q$ et par le théorème de Gauss ($p \wedge q =1$), $p$ divise $k'$. Il existe alors un entier $k''$ tel que $k' = pk''$ et par conséquent, $m -1 = k'k''q$ soit $m \equiv 1 [pq]$ d'où le résultat.
\end{proof}}}

Le théorème est à l'origine d'un premier test de primalité : le test de Fermat (algorithme~\ref{algo:Fermat}). La complexité de cet algorithme est bonne grâce à l'exponentiation binaire: elle est en $O(k(\log n))$. Sa correction provient du petit théorème de Fermat.

\begin{algorithm}
	\begin{algorithmic}[1]
		\Function \textsc{Fermat}($n$, $k$) avec $n \in \N$ le nombre à tester et $k \in \N$ le nombre de tests
		\For{$i = 1$ à $k$}
		\State $a \gets \textsc{Random}(1, n-1)$ 
		\LineComment{Choisi aléatoirement entre $2$ et $n-1$}
		\If{$a^{n-1} \not\equiv -1 [n]$}
		\State Renvoie \textsc{Composé}
		\EndIf
		\EndFor
		\State Renvoie \textsc{Probablement Premier}
		\EndFunction			
	\end{algorithmic}
	\caption{Test de primalité de Fermat \cite[p.888]{Cormen}.}
	\label{algo:Fermat}
\end{algorithm}

Ce test échoue à coup sur alors pour tous les nombres de Carmichael (et il y en a une infinité). Nous allons cependant donner quelques résultats intermédiaires.

\begin{definition}[Nombre pseudo-premier \protect{\cite[p.184]{AlFakir}}]
	Un entier $n$ est un nombre pseudo-premier en base $b$ s'il est composé et que $b^a \equiv b [a]$.
\end{definition}

\begin{definition}[Nombre de Carmichaël \protect{\cite[p.184]{AlFakir}}]
	Un entier $n$ est un nombre de Carmichaël s'il est un nombre pseudo-premier en toute base $b$ telle que $b$ soit premier avec $n$.
\end{definition}

\begin{ex}
	Le plus petit des nombres de Carmichaël est $561 = 3 \times 11 \times 17$.
\end{ex}

\begin{theo}[Probabilité d'échec du test de Fermat]
	Soit $n$ un nombre entier qui n'est pas de Carmichaël. Alors, la probabilité d'échec du test de Fermat est d'au plus $2^{-k}$.
\end{theo}

\paragraph{Autours du symbole de Jacobi} Le symbole de Jacobi permet entre autres choses de caractérisé les carrés d'un corps fini. Il permet d'exprimer le critère d'Euler qui est un critère pour savoir si un nombre est premier ou non. Ce critère est à l'origine du test de Solovay--Stassen. 

Le lemme d'Euler nous permet de mettre en place un deuxième test, le test de Solovay--Strassen (algorithme~\ref{algo:SolovayStrassen}) qui est meilleur que le test de Fermat puisqu'il n'existe pas une classe de nombre qui le fait échouer à chaque fois. La complexité de cet algorithme est en $O\left(k \log^3 n\right)$ et sa correction provient du lemme d'Euler.

\begin{algorithm}
	\begin{algorithmic}[1]
		\Function \textsc{Solovay--Strassen}($n$, $k$) avec $n \in \N$ le nombre à tester et $k \in \N$ le nombre de tests
		\For{$i = 1$ à $k$}
		\State $a \gets \textsc{Random}(2, n-1)$ 
		\State $x \gets \left(\frac{a}{n}\right)$
		\If{$x = 0$ ou $x \not\equiv a^{\frac{n-1}{2}} [n]$}
		\State Renvoie \textsc{Composé}
		\EndIf
		\EndFor
		\State Renvoie \textsc{Probablement Premier}
		\EndFunction			
	\end{algorithmic}
	\caption{Test de primalité de Solovay--Strassen \cite[p.888]{Cormen}.}
	\label{algo:SolovayStrassen}
\end{algorithm}

Nous allons maintenant étudier les cas pour lequel le test échoue \cite[p.41]{Hindry}. De ce point de vue, il est meilleur que le test de Fermat.

\begin{definition}[Pseudo-premier d'Euler--Jacobi \protect{\cite[p.888]{Cormen}}]
	Un entier impair $n$ est dit un nombre pseud-premier d'Euler--Jacobi en base $a$ si $a$ est premier avec $n$ et $a^{\frac{n-1}{2}} \equiv \left(\frac{a}{n}\right) [n]$.
\end{definition}

\begin{lemme}[Caractérisation de l'ensemble qui échoue au test \protect{\cite[p.41]{Hindry}}]
	Soit $H = \left \{ a \in (\Z/N\Z)^* \mid a^{\frac{N-1}{2}} \equiv \left(\frac{a}{N} [N] \right)\right \}$, alors $H = (\Z/N\Z)^*$ si et seulement si $N$ est premier.
\end{lemme}
{\footnotesize{\begin{proof}
	On a vu que si $N$ est premier, alors $H = (\Z/N\Z)^*$.
	
	Si $p^2$ divise $N$, il existe $a$ d'ordre $p(p-1)$ or $p$ ne divise pas $N-1$ donc $a^{N-1} \neq 1$. Si $N = p_1 \dots p_r$ avec $r \geq 2$, choisissons, par le lemme chinois, $a \equiv 1 [p_i]$ pour $2 \leq i \leq r$ et non carré modulo $p$. Alors $\left(\frac{a}{n}\right) =-1$, mais $a^{\frac{N-1}{2}} \equiv 1 [p_2 \dots p_r]$, donc $a^{\frac{N-1}{2}} \not\equiv -1 [N]$.
\end{proof}}}

\begin{cor}[Probabilité d'échec au test Solovay--Strassen \protect{\cite[p.42]{Hindry}}]
	Pour tout entier $n$ impair supérieur à 2, la probabilité d'échec du test de Solovay--Strassen est d'au plus $2^{-k}$.
\end{cor}
{\footnotesize{\begin{proof}
	Si $N$ est composé, on a $\left((\Z/N\Z)^* : H \right) \geq 2$ \textblue{(on a autant de carré que de non carré?)} donc en prenant $a$ aléatoirement, on a au moins une chance sur deux d'avoir $a \notin H$. Ainsi, si $N$ passe successivement $k$ tests, on peut dire qu'il premier avec une probabilité supérieure à $1 - 2^{-k}$.
\end{proof}}}


\paragraph{Autour du test de Miller--Rabin} Le test de Miller--Rabin (algorithme~\ref{algo:MillerRabin}) vient encore améliorer le test de Solovay--Strassen. Il se base sur la décomposition d'un nombre par ses puissances de deux. En effet, un nombre paire peut s'écrire comme un produit d'une certaine puissance de $2$ et d'un nombre impair. Sa probabilité d'échec est très basse pour une complexité non déraisonnable en $O(k\log n)$.

\begin{algorithm}
	\begin{algorithmic}[1]
		\Function \textsc{Témoin}($a$, $n$) avec $n \in \N$ le nombre à tester et $a \in \N$
		\State Soit $n - 1 = 2^t u$ avec $t \geq 1$ et $u$ impair
		\State $x_0 \gets a^u \mod n$
		\For{$i = 1$ à $t$}
		\State $x_i \gets x_{i-1}^2 \mod n$ 
		\If{$x_i = 1$ et $x_{i-1} \neq 1$ et $x_{i-1} \neq -1$}
		\State Renvoie \textsc{Vrai}
		\EndIf
		\EndFor
		\State Renvoie $x_t = 1$
		\EndFunction			
	\end{algorithmic}
	\caption{Sous-routine témoins qui cherche un témoin de la compositionnalité de $n$. \cite[p.888]{Cormen}.}
	\label{algo:Temoin}
\end{algorithm}
\begin{algorithm}
	\begin{algorithmic}[1]
		\Function \textsc{Miller--Rabin}($n$, $k$) avec $n \in \N$ le nombre à tester et $k \in \N$ le nombre de tests
		\For{$j = 1$ à $k$}
		\State $a \gets \textsc{Random}(1, n-1)$ 
		\If{$\textsc{Témoins}(a,n)$} \Comment{Voir algorithme~\ref{algo:Temoin}}
		\State Renvoie \textsc{Composé}
		\EndIf
		\EndFor
		\State Renvoie \textsc{Probablement Premier}
		\EndFunction			
	\end{algorithmic}
	\caption{Test de primalité de Miller--Rabin \cite[p.888]{Cormen}.}
	\label{algo:MillerRabin}
\end{algorithm}

\begin{lemme}[Correction du test \protect{\cite[p.42]{Hindry}}]
	Soit $N$ impair. Posons $N - 1 = 2^sM$ avec $M$ impair. Si $N$ est premier et que $a$ est premier avec $N$ alors $a^M \equiv 1 [N]$ ou il existe $0 \leq r \leq s-1$ tel que $a^{2^rM} \equiv -1 [N]$.
\end{lemme}
{\footnotesize{\begin{proof}
	L'ordre de $a$ modulo $N$ est $2^tM'$, avec $0 \leq t \leq s$ et $M'$ impaire divisant $M$ \textblue{(soit son ordre est pair et il peut s'écrire sous cette forme avec $t> 0$, sinon on a $t=0$)}. Si $t = 0$, alors $a^{M'} \equiv 1 [N]$, d'où par la relation de divisibilité, $a^{M} \equiv 1 [N]$. Si $t \geq 1$, alors comme $N$ est premier $a^{2^{t-1}M'} \equiv -1 [N]$, d'où par la relation de divisibilité, $a^{2^{t-1}M} \equiv -1 [N]$ \textblue{(car $k$ tel que $M= kM'$ est impair (sinon on peut factoriser par $2$))}.
\end{proof}}}

\begin{req}[Équivalence entre les tests \protect{\cite[p.43]{Hindry}}]
	On introduit les ensembles suivants (correspondant aux ensembles contenant les différents échec des tests):
	\begin{displaymath}
	\begin{array}{ccl}
	G_0 & = & \left(\Z/n\Z\right)^* \\
	G_1 & = & \left\{a \in \left(\Z/n\Z\right)^* \mid a^{N-1} \equiv 1 [N] \right\} \\
	G_2 & = & \left\{a \in \left(\Z/n\Z\right)^* \mid a^{N-1} \equiv \{\pm 1\} [N] \right\} \\
	G_3 & = & \left\{a \in \left(\Z/n\Z\right)^* \mid a^{N-1} \equiv \left(\frac{a}{N}\right) [N] \right\} \\
	S & = & \left\{a \in \left(\Z/n\Z\right)^* \mid a^{M} \equiv 1 [N] \vee \exists r \in \left[0, s-1\right], a^{2^rM} \equiv -1 [N]\right\} \\
	\end{array}
	\end{displaymath}
	On a toujours $S \subseteq G_3 \subseteq G_2 \subseteq G_1 \subseteq G_0$ avec égalité si et seulement si $N$ est premier (soit $G_3 = G_0$). Par ailleurs $G_1$, $G_2$ et $G_3$ sont des sous-groupes de $G_0$ ce qui n'est pas nécessairement le cas de $S$, même si dans le cas $N \equiv 3 [4]$, on $G_2 = G_3 = S$. Sauf dans le cas où $N \equiv 3 [4]$ où les trois tests sont équivalents, le test de Miller--Rabin est meilleur que le test de Solovay--Strassen qui lui-même est meilleur que le test de Fermat.
\end{req}

\begin{prop}[Probabilité d'échec au test de Miller-Rabin \protect{\cite[p.44]{Hindry}}]
	Soit $N$ impair composé. Si $N \neq 9$, alors $\frac{\left|S\right|}{\left|G_0\right|} \leq \frac{1}{4}$.
\end{prop}
{\footnotesize{\begin{proof}
	Nous allons commencer par énoncé un lemme qui nous servira dans la suite de la preuve.
	\begin{definition}
		Soit $A, B$ des entiers. On pose
		\begin{displaymath}
		\phi(A; B) = \# \{a \in \left(\Z/A\Z\right)^* \mid a^{B} \equiv 1 [A]\}
		\end{displaymath}
	\end{definition}
	
	\begin{lemme}
		Soit $t \geq 0$, $N = 1 + 2^sM = p_1^{\alpha_1} \dots p_k^{\alpha_k}$ (avec $M$ impair). Posons $p_i - 1 = 2^{s_i}M_i$, $s_i' = \min(t, s_i)$ et $t_i = \gcd(M, M_i)$, alors $\phi(N, 2^tM) = 2^{s_1' + \dots + s_k'}t_1 \dots t_k$. Par ailleurs le cardinal de l'ensemble $\{a \in (\Z/n\Z)^* \mid a^{2^tM} \equiv -1 [N]\}$ est nul si $t  \leq \min_i s_i$ et égal à $\phi(N, 2^tM)$ si $t < \min_i s_i$.
	\end{lemme}
	{\footnotesize{\begin{proof}
		On a $2^{2^tM} \equiv 1 [N]$ si et seulement si $a^{2^tM} \equiv 1 [p_j^{\alpha_j}]$ pour $j \in \llbracket 1, k \rrbracket$. Le groupe $\left(\Z/p_j^{\alpha_j}Z\right)^*$ est cyclique de cardinal $(p_j - 1)p_j^{\alpha_j -1}$, donc le nombre de solution est 
		\begin{displaymath}
		\gcd\left(2^tM, (p_j - 1)p_j^{\alpha_j -1}\right) = \gcd\left(2^tM, 2^{s_j}M_j\right) = 2^{\min(t, s_j)}t_j
		\end{displaymath}
		Par le lemme chinois, le nombre de solutions modulo $N$ est donc le produit de ces nombres, d'où le résultat annoncé.
		
		Pour la deuxième assertion, soit on a aucune solution, soit il en existe une, et alors l'ensemble des solution est en bijection avec les solutions de la congruence précédente. La congruence $a^{2^tM} \equiv -1 [p_j^{\alpha_j}]$ est résoluble si et seulement si $2^{t+1}$ divise $(p_j - 1)p_j^{\alpha_j -1}$, c'est-à-dire si et seulement si $t+1 \leq s_j$, d'où le résultat.
	\end{proof}}}
	
	Supposons que $s_1 \leq s_2 \leq \dots \leq s_k$. En décomposant l'ensemble $S$ en $S_0 = \{a \in \left(\Z/n\Z\right)^* \mid a^M \equiv 1 [N]\}$ et $T_j = \{a \in \left(\Z/n\Z\right)^* \mid a^{2^jM} \equiv -1 [N]\}$ pour $0 \leq j \leq s_1 -1$, on obtient en appliquant le lemme précédent à chacun de ces ensembles:
	\begin{displaymath}
	\# S = t_1 \dots t_k\left(2+2^k+\dots +2^{k(s_1 -1)}\right) = t_1 \dots t_k \left(\frac{2^{ks_1} + 2^k - 2}{2^k -1}\right)
	\end{displaymath}
	La proportion de $a \in G_0$ qui passe le test de Miller--Rabin est alors:
	\begin{displaymath}
	\frac{\# S}{\#G_0} = \frac{t_1 \dots t_k}{M_1 \dots M_k} \frac{2^{-(s_1+ \dots +s_k)}}{p_1^{\alpha_1 - 1} \dots p_k^{\alpha_k - 1}} \left(\frac{2^{ks_1} + 2^k - 2}{2^k -1}\right)
	\end{displaymath}
	Si $k = 1$, la proportion est égale à $\frac{t_1}{M_1p_1^{\alpha_1 -1}} \leq \frac{1}{p_1^{\alpha_1 -1}} \leq \frac{1}{5}$ (sauf pour $N = 3^2$ où on trouve $\frac{\# S}{\#G_0} = \frac{1}{3}$). 
	
	Si $k \leq 2$, on peut supposer $\alpha_1 = \dots = \alpha_k = 1$, sinon la proportion est $\leq \frac{1}{p_i}$ qu'on peut supposer dans la pratique aussi petit que l'on veut. Si l'un des $M_i \leq t_i$, alors $\frac{t_1 \dots t_k}{M_1 \dots M_k} \leq \frac{1}{3}$. Ensuite,
	\begin{displaymath}
	2^{-(s_1+ \dots +s_k)} \left(\frac{2^{ks_1} + 2^k - 2}{2^k -1}\right) \leq 2^{-ks_1}\frac{2^k - 2}{2^k - 1} + \frac{1}{2^k - 1} \leq 2{1-k}
	\end{displaymath}
	donc la proportion est $\leq \frac{1}{8}$ si $k \geq 4$ et $\leq \frac{1}{4}$ si $k = 3$. Si $k=2$ et si l'un des $M_i$ est distinct des $t_i$, alors la proportion est $\leq \frac{1}{6}$. Si $k = 2$ et $M_1 = t_1$ et $M_2 = t_2$, on voit que $M_1 = M_2$ donc $s_1 < s_2$. La proportion est alors $\leq \frac{1}{4}$.
\end{proof}}}

