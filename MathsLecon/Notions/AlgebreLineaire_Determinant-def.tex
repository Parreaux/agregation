%LECONs : 152 (Determinant)
%DEVs: Suite de polygones
Nous commençons par définir le déterminant \cite[p.134]{Gourdon-algebre}. Pour ce faire nous devons introduire la notion de forme $n$-linéaire alternée et anti-symétrique. On note $K$ un corps commutatif (la théorie reste vrai pour quelques anneaux)

\paragraph{Formes $n$-linéaires alternées et antisymétrique}

\begin{definition}
	Soient $E_1, \dots, E_n$ et $F$ des $K$-espaces vectoriel. Une application $f : E_1 \times \dots \times E_n \to F$ qui à $(x_1, \dots, x_n)$ associe $f(x_1, \dots, x_n)$ est dite $n$-linéaire si en tout point les $n$ applications partielles sont linéaire. Si $F = K$, on parle de forme $n$-linéaire.
\end{definition}

\noindent\begin{minipage}{.56\textwidth}
	\begin{req}
		Si une application est $2$-linéaire, on dit qu'elle est bilinéaire.
	\end{req}
\end{minipage} \hfill
\begin{minipage}{.36\textwidth}
	\begin{req}
		On a $\dim \mathcal{L}_n(E, K) = (\dim E)^n$.
	\end{req}
\end{minipage}

\begin{definition}
	Soit $f \in \mathcal{L}_n(E, K)$.
	\begin{itemize}
		\item $f$ est dite alternée si $f(x_1, \dots, x_n) = 0$ lorsque deux vecteurs sont égaux.
		\item $f$ est dite antisymétrique si l'échange de deux vecteurs donne une valeur opposée. 
	\end{itemize}
\end{definition}

\begin{theo}
	Soient $K$ un corps commutatif de caractéristique différente de $2$, $E$ un $K$-espace vectoriel et $f \in \mathcal{L}_n(E, K)$. Alors $f$ est antisymétrique si et seulement si $f$ est alternée.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		On raisonne en dimension $2$.
		\begin{description}
			\item[$\Rightarrow$] $f$ antisymétrique : $f(x_1, x_1) = - f(x_1, x_1)$, donc $2f(x_1, x_1) = 0$ et $\mathrm{car} K \neq2$ : $f(x_1, x_1) = 0$.
			\item[$\Leftarrow$] $f$ alternée : $f(x_1, x_2) + f(x_1, x_2) = f(x_1 + x_2, x_2) + f(x_1 + x_2, x_1) = f(x_1 + x_2, x_1 + x_2) = 0$, donc $f(x_1, x_2) = - f(x_2, x_1)$.
		\end{description}
	\end{proof}
\end{footnotesize}

\begin{req}
	Dans le cas d'un corps de caractéristique $2$, on a toujours $f$ alternée implique $f$ antisymétrique.
\end{req}

\paragraph{Déterminant sur une famille de vecteurs}

\begin{theo}
	L'ensemble des formes $n$-linéaires alternées sur un $K$-espace vectoriel $E$ de dimension $n$ est un $K$-espace vectoriel $E$ de dimension $1$. De plus, il existe une et une seule forme $n$-linéaire alternée prenant la valeur $1$ sur une base donnée de $E$.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		Soit $B = (e_1, \dots, e_n)$ une base de $E$.
		\begin{itemize}
			\item On définie $d(x_1, \dots, x_n) = e_1^*(x_1)\dots e_n^*(x_n) = x_{1,1} \dots x_{n, n}$. On note $d^{\natural}(x_1, \dots, x_n) = \sum_{\sigma \in \Sn_n} \epsilon(\sigma)x_{\sigma(1),1} \dots x_{\sigma(n),n}$. On a $d^{\natural}(e_1, \dots, e_n) = 1$, d'ou $d^{\natural} \neq 0$.
			
			\item $f$ une forme $n$-linéaire alternée : $f(x_1, \dots, x_n) = \sum_{i_1, \dots,i_n} x_{1, i_1}\dots x_{n, i_n}f(e_{i_1}, \dots, e_{i_n}) = f(e_1, \dots, e_n)d^{\natural}(x_1, \dots, x_n)$.
			
			\item $f \in \mathrm{Vect}(d^{\natural})$ avec $d^{\natural} \neq 0$: existence de l'espace vectoriel de dimension $1$.
			
			\item Si $f(e_1, \dots, e_n)= 1$ , alors $f = d^{\natural}$. D'où l'unicité.
		\end{itemize}
	\end{proof}
\end{footnotesize}

\begin{definition}
	Soit $B$ une base de $E$, alors il existe une unique forme $n$-linéaire alternée sur $E$ prenant la valeur $1$ sur la base $B$. On l'appelle déterminant et on la note $\det_B$.
\end{definition}

\begin{req}
	On a également une formule explicite dans ce cas : $\det_B(x_1, \dots, x_n) = \sum_{\sigma \in \Sn_n} \epsilon(\sigma) x_{1, \sigma(1)} \dots x_{n, \sigma(n)}$.
\end{req}

\begin{prop}
	Soit $f$ une forme $n$-linéaire alternée, alors $f(x_1, \dots, x_n) = f(e_1, \dots, e_n) \det_B(x_1, \dots, x_n)$.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		On applique le théorème précédent : $f \in \mathrm{Vect}(\det_B)$ et son coefficient est donné par l'application de $f$ à $B$.
	\end{proof}
\end{footnotesize}

\begin{prop}[Changement de base]
	Soient $B$, $B'$ deux bases de $E$, alors $\det_{B'}(x_1, \dots, x_n) = \det_{B'}B \det_{B}(x_1, \dots, x_n)$. On en déduit que $\det_{B'} B \det_BB' = 1$.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		On applique la proposition précédente avec $f = \det_{B'}$.
	\end{proof}
\end{footnotesize}

\begin{theo}
	Soient $x_1, \dots, x_n \in E$, les propositions suivantes sont équivalentes:
	\begin{enumerate}
		\item Les vecteurs $x_1, \dots, x_n$ sont liés.
		\item Pour toute bases $B$ de $E$, $\det_B(x_1, \dots, x_n) = 0$.
		\item Il existe une base $B$ de $E$, $\det_B(x_1, \dots, x_n) = 0$.
	\end{enumerate}
\end{theo}
\begin{footnotesize}
	\begin{proof}
		\begin{description}
			\item[Montrons $1 \Rightarrow 2$] Soit $B$ une base de $E$, $\det_B$ est une forme $n$-linéaire alterné. On a $\det_B(x_1, \dots, x_n) = \det_B(\sum_{i=2}^{n} \lambda_ix_i, \dots, x_n)$  \textblue{($\lambda_i \neq 0$ car famille liée)}, par $n$-linéarité $\det_B(x_1, \dots, x_n) = \sum_{i=2}^{n} \lambda_i\det_B(x_i, \dots, x_n)$. Par alternée, $\det_B(x_1, \dots, x_n) = 0$.
			\item[Montrons $2 \Rightarrow 3$] Évident
			\item[Montrons $3 \Rightarrow 1$] $\det_B$ alternée donc il existe $x_j = \sum_{\underset{i\neq j}{i=1}}^{n} \lambda_ixi$ avec $\lambda_i$ non tous nuls: la famille est liée.
		\end{description}
	\end{proof}
\end{footnotesize}



\paragraph{Déterminant sur un endomorphisme}

\paragraph{Déterminant sur une matrice carrée}