%LECONs : 182 (Groupe et geometrie)
La géométrie euclidienne (en particulier l'affine) consiste à l'étude du groupe des isométries opérant sur $\E$ (en tant qu'espace affine euclidien)

\paragraph{Espace affine euclidien et groupe des isométries}


\begin{definition}[\protect{\cite[p.108]{Boyer}}]
	On appelle \emph{espace affine euclidien} tout espace affine réel $\E$ dont la direction $\overrightarrow E$ est muni du produit scalaire canonique. On note $d$ la distance sous-jacente.
\end{definition}
	
\begin{definition}[\protect{\cite[p.52]{Audin}}]
	On appelle \emph{isométrie} de $\E$ toute application linéaire $f: \E \rightarrow \E$ qui conserve les distances, c'est-à-dire que pour tout $A,B \in \E$, $d(f(A),f(B))=d(A,B)$.
\end{definition}

\begin{ex}
	translations, rotations, réflexions, symétries
\end{ex}
	

\begin{definition}[\protect{\cite[p.26]{Laville}}]
	Soit $\E$ un espace affine euclidien. L'ensemble des isométries, noté $Is(\E)$ est un groupe, sous-groupe de $GA(\E)$.
\end{definition}

\begin{prop}[\protect{\cite[p.25]{Laville} }]
	$f$ est une isométrie si et seulement si $f$ est affine et $\overrightarrow{f} \in O(\E)$ où $O(\E)$ est le groupe orthogonal de $\E$.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		Si $f$ affine et $\overrightarrow{f} \in O(\E)$, alors $||\overrightarrow{f(x)f(y)}|| = ||\overrightarrow{xy}||$.Réciproquement, soit $a \in \E$ et posons $\overrightarrow{f}_a : \overrightarrow{\E} \rightarrow \overrightarrow{\E}$ définie telle que $\overrightarrow{f}_a(\overrightarrow{v}) = \overrightarrow{f(a)f(a + \overrightarrow{v})}$. On a, $||\overrightarrow{f}_a(\overrightarrow{v})|| = ||\overrightarrow{v}||$. Donc $\overrightarrow{f}_a$ est une application linéaire bijective orthogonale et $f(a + \overrightarrow{v})= f(a) + \overrightarrow{f}_a(\overrightarrow{v})$.
	\end{proof}
\end{footnotesize}


\paragraph{Générateurs du groupe des isométries \cite[p.54]{Audin}}
 
\begin{definition}
	Une réflexion est une symétrie orthogonale par rapport à un hyperplan. C'est bien une isométrie.
\end{definition}

\begin{theo}
	Soit $\E$ un espace affine de dimension $n$. Toute isométrie de $\E$ peut s'écrire comme composée de $p$ réflexions pour un entier $p \leq n+1$.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		On utilise le résultat analogue dans un espace vectoriel que l'on montre par récurrence sur sa dimension $n$. En effet, si $n = 1$, on fait la liste des isométries $id; -id$ qui sont des réflexion. Pour $n +1$, $x_0 \neq 0$, on a deux cas: $f(x_0) = x_0$ et on considère $S = \{x_0\}^{\bot}$ pour appliquer l'hypothèse de récurrence; sinon on cherche à se ramener au cas précédent en se rendant $x_0$ fixe. Donc si $\phi$ est une isométrie affine, on la vectorialise pour ce ramener au cas précédent.
	\end{proof}
	\begin{proof}[Une autre preuve]
		Soit $A$ un point de $\E$ et on considère $A' = \phi(A) \neq A$. On considère $\mathcal{H}$ l'hyperplan médian de $AA'$, de sorte que $\sigma_\mathcal{H} \circ \phi$ est une isométrie qui fixe $A$ et on applique par récurrence ce résultat ce qui donne $\phi = \sigma_\mathcal{H} \circ \sigma_{\mathcal{H}_1}  \circ \dots \circ \sigma_{\mathcal{H}_{p}}$.
	\end{proof}
\end{footnotesize}

\begin{cor}
	Les générateurs du groupe des isométries sont les réflexions.
\end{cor}
	
\begin{appli}[\protect{\cite[p.86]{Audin}}]
	Les isométries du plan sont exactement les translations, les rotations, les réflexions et les symétries glissées.
\end{appli}
\begin{footnotesize}
	\begin{proof}
		Par le théorème précédent, elles sont composées d'une, de deux ou de trois réflexions. Pour l'isométrie vectorielle, on a trois possibilités: l'identité, une réflexion ou une rotation vectorielle. Si l'isométrie vectorielle est l'identité, on obtient une translation; si l'isométrie vectorielle est la réflexion on obtient une réflexion (si $\phi$ a un point fixe) ou une symétrie glissé (sinon); si l'isométrie vectorielle est une rotation on obtient une rotation.
	\end{proof}
\end{footnotesize}
	
\paragraph{Invariants par le groupe des isométries (en dimension 2) \cite[p.116]{Boyer}} Les angles orientés sont les premiers invariants non-triviaux que l'on trouve en géométrie: ils sont bien plus intéressant que les invariants affines.

\begin{definition}[\protect{\cite[p.58]{Audin}}]
	On appelle isométrie positive une isométrie telle que sont isométrie vectorielle est un déterminant positive.
\end{definition}
	
\begin{definition}
	Un angle orienté de droites du plan est une classe d'équivalence de l'ensemble des couples des droites sous l'action du groupe des isométries positives du plan.
\end{definition}
 
\begin{req}
	\emph{Interprétation} : Les angles orientés sont les invariant du groupe des isométries.
\end{req}
	
\begin{appli}
	Dans la base canonique  de $\R^{2}$ la matrice de rotation de $r \in O_{2}^{+}(\R)$ est donnée par un élément $\theta \in \R/2\pi\Z$ par la formule $(\cos \theta, -\sin \theta), (\sin \theta \cos \theta)$. Le paramètre $\theta$ est l'angle de rotation.
\end{appli}
	 
\begin{req}
	Les angles orientés entre deux demi-droites se définissent de la même manière que les angles orientés entre deux droites.
\end{req}
	 
\begin{definition}
	La mesure d'un angle orienté de deux demi-droites de même origine du plan affine orienté est la paramètre $\theta \in \R/2\pi\Z$ de l'unique rotation $r$ qui envoie la première demi-droite sur la deuxième.
\end{definition}
	
\begin{appli}
	La somme des angles d'un triangle du plan affine euclidien est égale à $\pi$.
\end{appli}
\begin{footnotesize}
	\begin{proof}
		On considère le parallélogramme définie par le triangle (les trois sommets en sont trois dans le parallélogramme).
	\end{proof}
\end{footnotesize}



\paragraph{Stabilisateurs par le groupe des isométries (en dimension 2 et 3) \cite[p.358]{Caldero-Germoni}}
 
\begin{definition}[\protect{\cite[p.156]{Audin}}]
	Un polygone convexe est dit régulier si tout ses côtés et tous ses angles sont égaux. 
\end{definition} 

\begin{definition}
	Le groupe diédral est engendré par les rotations et les réflexions.
\end{definition}

\begin{prop}[\protect{\cite[p.165]{Audin}}]
	Soit $P$ un polygone régulier à $n$ côté est préservé par le groupe des isométries du plan isomorphe au groupe diédral $D_{2n}$.
\end{prop}
	
\begin{definition}
	Dans $\R^{3}$ un solide platonicien est un polyèdre de dimension $3$ (d'intérieur non vide) qui est régulier (faces identiques et régulières) et convexe.
\end{definition}

\begin{theo}[\textred{ADMIS}]
	Il existe exactement cinq solides platoniciens.
\end{theo}

\begin{ex}
	Tétraèdre; cube; octaèdre; dodécaèdre; isocaèdre.
\end{ex}
	
\begin{definition}
	Le groupe des isométries d'une partie $X \subseteq \R^{3}$ est le sous-groupe des isométries de l'espace affine euclidien $\R^{3}$ qui stabilisent $X$.
\end{definition}

\begin{theo}
	Le groupe des isométries d'un tétraède régulier $\Delta_{4}$ est isomorphe à $S_{4}$.
\end{theo}

\begin{theo}
	Le groupe des isométries positives d'un cube $C_{6}$ est isomorphe à $S_{4}$ (Remarque: les isométries le sont $S_{4} \times \Z/2\Z$).
\end{theo} 

\begin{appli}
	Il y a 57 façons de colorier un cube avec 3 couleurs.
\end{appli}
