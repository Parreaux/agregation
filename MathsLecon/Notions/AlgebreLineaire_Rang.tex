%LECONs : 151 (Dimension et rang)
Nous allons étudier la notion de rang: sa définition, ses caractérisations et son calcul.

\paragraph{Définition de la notion de rang} La notion de rang peut être définit sur deux objets de l'algèbre linéaire: les applications linéaires et les matrices. Selon les applications, nous utilisons l'une ou l'autre de ces définitions \cite[p.61]{Grifone}.

\begin{definition}
	Soit $f \in \mathcal{L}(E, F)$ avec $E$ et $F$ deux espaces vectoriels de dimension finie. La dimension de l'image de $f$ est appelé le rang de $f$ : $\mathrm{rg } f = \dim (\mathrm{Im } f)$.
\end{definition}

\begin{theo}[Théorème du rang]
	Soient $E$ et $E'$ deux espaces vectoriels de dimension finie et $f : E \to E'$ une application linéaire. On a alors $\dim E = \mathrm{rg } f + \dim(\ker f)$.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		Soit $n = \dim E$ et $\dim(\ker f) = r$. Montrons que $\dim(\mathrm{Im } f) = n - r$. Soit $\{w_1, \dots, w_r\}$ une base de $\ker f$ et $\{w_1, \dots, w_r, v_{r+1}, \dots, v_n\}$ une base de $E$. Montrons que $B = \{f(v_{r+1}), \dots, f(v_n)\}$ est une base de $\mathrm{Im } f$. 
		\begin{itemize}
			\item $B$ engendre $E'$. Soit $x \in E$, donc $x$ se décompose sur la base et par linéarité de $f$, on a une décomposition de $f(x)$ sur $B$. Par les propriétés du noyaux, on trouve une décomposition de $f(x)$ sur l'image de $f$ par la famille $\{v_{r+1}, \dots, v_n\}$.
			\item $B$ est libre. Si une combinaison est nulle, alors par linéarité de $f$, elle appartient au noyau. On peut donc la décomposée sur b$E$ et comme sur $E$, on  a  une base, on obtient l'annulation des coefficients de la décomposition.
		\end{itemize}
	\end{proof}
\end{footnotesize}

\begin{cor}
	Soit $f \in \mathcal{L}(E, E')$ avec $E$ et $E'$ deux espaces vectoriels de même dimension finie. Alors les propriétés suivantes sont équivalente:
	
	\begin{minipage}{.2\textwidth}
		\begin{enumerate}
			\item $f$ est injective;
			\item $f$ est surjective;
			\item $f$ est bijective.
		\end{enumerate}
	\end{minipage} \hfill
	\begin{minipage}{.71\textwidth}
		\begin{footnotesize}
			\begin{proof}
				Montrons que $1 \Leftrightarrow 2$ (la dernière équivalence en découle) : $f$ est injective si et seulement si $\ker f = \{0\}$. Donc, par le théorème du rang, si et seulement si $\dim E = \mathrm{rg } f = \dim(\mathrm{Im } f)$. Par hypothèse $\dim E = \dim E'$, donc si et seulement si $\mathrm{Im } f =E'$. Donc si et seulement si $f$ est injective.
			\end{proof}
		\end{footnotesize}
	\end{minipage}
\end{cor}

\begin{req}
	La réciproque est fausse. On se place sur $\R[X]$ : $P \mapsto P'$ est surjective et non injective et $P \mapsto XP$ est injective et non surjective.
\end{req}

\begin{cor}
	Soient $E$ et $F$ deux espaces vectoriel de dimension finie et $f : E \to F$.
	\begin{enumerate}
		\item Si $f$ est injective alors $\dim E \leq \dim F$.
		\item Si $f$ est surjective alors $\dim F \leq \dim E$.
	\end{enumerate}
\end{cor}
\begin{footnotesize}
	\begin{proof}
		\begin{enumerate}
			\item Théorème du rang: $\dim E = \mathrm{rg } f + \dim(\ker f)$. Injectivité de $f$: $\dim E = \mathrm{rg } f$. Par $\mathrm{Im } f \subseteq F$, $\mathrm{rg } f \leq \dim F$.
			\item Théorème du rang: $\dim E = \mathrm{rg } f + \dim(\ker f)$. Surjectivité de $f$ : $\dim E = \dim F + \dim(\ker f)$. Comme $\dim(\ker f) \geq 0$, on a $\dim E \geq \dim F$.
		\end{enumerate}
	\end{proof}
\end{footnotesize}

\begin{cor}
	Soit $E$ un espace vectoriel de dimension finie $n$, et $f_1, \dots, f_r$ des formes linéaires sur $E$. Alors, $\dim \left(\bigcap_{i=1}^{r} \ker f_i\right) \geq n -r$ avec égalité si et seulement si la famille $\{f_1, \dots, f_r\}$ est libre.
\end{cor}
\begin{footnotesize}
	\begin{proof}
		On atteint l'égalité que si $\forall i \neq j$ $\mathrm{Im } f_j \subset \ker f_i$.
	\end{proof}
\end{footnotesize}

\begin{appli}
	Résolution de système linéaire
\end{appli}

\begin{cor}[\protect{\cite[p.113]{Gourdon-algebre}}]
	Soit $E$ un espace vectoriel contenant le sous-espace vectoriel $F$. Alors $\dim F/E = \dim E - \dim F$;
\end{cor}
\begin{footnotesize}
	\begin{proof}
		Utilisation des supplémentaires.
	\end{proof}
\end{footnotesize}

\begin{definition}
	Soit $A \subset E$ une famille de vecteur. On appelle rang de $A$ la dimension de l'espace engendré par $A$: $\mathrm{rg } A = \dim(\mathrm{Vect}(A))$.
\end{definition}

\begin{definition}
	Soit $M \in \mathcal{M}_{p, n}(K)$. On appelle rang de $M$ le rang de la famille de ces vecteurs.
\end{definition}

\begin{prop}
	Soient $E$ et $F$ deux espaces vectoriel de base respective $B_1$ et $B_2$. Soit $f : E \to F$ une application linéaire. Alors, $\mathrm{rg}(\mathrm{Mat}_{B_1, B_2}(f)) = \mathrm{rg}(f)$.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		La représentation matricielle engendre l'image de l'application par la famille des colonnes de la matrice.
	\end{proof}
\end{footnotesize}

\begin{req}
	Deux matrices qui représentent la même application linéaire ont même rang. En particulier, deux applications semblables ont même rang.
\end{req}

\begin{req}
	Si $A \in \mathcal{M}_{p, q}(K)$, alors $\mathrm{rg } A \leq \min(p, q)$. Si $A \in \mathcal{M}_{p, q}(K)$, $A$ est inversible si et seulement si $\mathrm{rg } A = n$.
\end{req}

\begin{req}
	Soit $f g$ deux application linéaire $\mathrm{rg } (f \circ g) = \min(\mathrm{rg }f, \mathrm{rg }g)$.
\end{req}

\paragraph{Caractérisation du rang} On va chercher quelques caractérisation du rang en fonction des propriétés sur la matrice \cite[p.121]{Gourdon-algebre}.

\begin{definition}
	Soient $A, B \in \mathcal{M}_{p, q}(K)$. On dit que $A$ et $B$ sont équivalentes s'il existe $P \in GL_q(K)$ et $Q \in GL_p(K)$ telles que $B =QAP$.
\end{definition}

\begin{theo}
	Soit $A \in \mathcal{M}_{p, q}(K)$. Si $r= \mathrm{rg } A \geq 1$, $A$ est équivalente à la matrice $J_r = \begin{pmatrix}
	I_r & 0 \\ 
	0 & 0 \\
	\end{pmatrix}$.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		On construit $P$ et $Q$ tels que $AP = \begin{pmatrix} M & 0 \\ * & 0 \\  \end{pmatrix}$ où $M$ est une matrice de taille $r$ et $Q$ donnant la forme souhaitée.
	\end{proof}
\end{footnotesize}

\begin{cor}
	Deux matrices $A$ et $B$ sont équivalentes si et seulement si elles ont même rang.
\end{cor}
\begin{footnotesize}
	\begin{proof}
		\begin{description}
			\item[$\Rightarrow$] On peut écrire $A = PBQ$, alors $\mathrm{rg } A = \mathrm{rg }(PBQ) = \min(\mathrm{rg } P, \mathrm{rg } B, \mathrm{rg } Q) = \min(p, q, \mathrm{rg } B) = \mathrm{rg } B$ car $\mathrm{rg } B \leq \min(p, q)$.
			\item[$\Leftarrow$] Si $r = \mathrm{rg } A = \mathrm{rg } B$, par le théorème $A$ est équivalente à $J_r$ et $J_r$ est équivalente à $B$. Par transitivité de cette relation d'équivalence, $A$ est équivalente à $B$.
		\end{description}
	\end{proof}
\end{footnotesize}

\begin{req}
	On a un résultat analogue pour les matrices semblable car on pose $Q = P^{-1}$.
\end{req}

\begin{definition}
	Soit $A \in \mathcal{M}_{p,q}$. On pose $I \subset \{1, \dots, p\}$ et $J \subset \{1, \dots, q\}$ tels qu'on définisse $B$ la matrice extraite contenant les coefficients $a_{i,j}$ où $i \in I$ et $j \in J$.
\end{definition}

\begin{theo}
	Soit  $A \in \mathcal{M}_{p,q}$. Le rang de $A$ est le plus grand des ordres des matrices carrées inversibles extraites de $A$.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		On applique le théorème précédent.
	\end{proof}
\end{footnotesize}

\begin{prop}
	Pour toute matrice $A$, on a $\mathrm{rg }(A) = \mathrm{rg }(^tA)$
\end{prop}
\begin{footnotesize}
	\begin{proof}
		Conséquence de la caractérisation pour les matrices extraites.
	\end{proof}
\end{footnotesize}














