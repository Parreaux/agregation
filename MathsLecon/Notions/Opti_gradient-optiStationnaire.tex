%DEVs: Methode de gradient
Nous voulons démontrer la convergence de cette méthode. On va alors commencer par étudier quelques propriétés sur les suites stationnarisante et minimisante d'une fonction $f$ \cite[p.494]{Yger-Weil}.

\begin{definition}
	Une suite $\left(x_k\right)_k$ de $\R^n$ est dite:
	\begin{itemize}
		\item stationnarisante pour $f$ si $\lim_{k \to \infty} \triangledown f\left(x_k\right) =0$
		\item minimisante pour $f$ si $\lim_{k \to \infty} f\left(x_k\right) = \alpha = \inf_{x \in \R^n} f(x)$.
	\end{itemize}
\end{definition}
\textred{\textbf{Attention}: il ne faut pas confondre suite minimisante et suite convergeant vers une fonction optimale: prendre par exemple $f(x) = e^{x}$.} Dans le cas de la méthode du gradient à pas optimal, si la fonction que l'on souhaite minimiser n'est pas convexe, alors la suite $\left(x_k\right)_k$ définie par la méthode reste stationnarisante et ne sera pas minimisante.

\begin{pnum}
	Si $f$ est convexe et différentiable sur $\R^n$, alors toute suite bornée stationnarisante pour $f$ est minimisante pour $f$.
	\label{prop:suites}
\end{pnum}
\begin{proof}
	Soit $\left(x_k\right)_k$ une suite stationnarisante pour $f$. Comme $f$ est supposé convexe, on a $\forall x \in \R^n, f(x) \geq f(x_k) \left< \triangledown f\left(x_k\right), x -x _k \right>$. On obtient:
	
	\begin{tabular}{ccll}
		$\inf_{\R^n} f$ & $\leq$ & $\sup_{K \in \N} \inf_{k \geq K} f\left(x_k\right)= \liminf_{k \to \infty} f\left(x_k\right)$ & \textblue{(définitions de $\inf$ et $\sup$)} \\
		& $\leq$ & $\limsup_{k \to \infty} f\left(x_k\right)$ & \textblue{($\liminf \leq \limsup$)} \\
		& $\leq$ & $\limsup_{k \to \infty} f\left(x_k\right) +\lim_{k \in \infty} \left<\triangledown f\left(x_k\right), x -x_k \right>$ & \textblue{($\lim_{k \in \infty \left<\triangledown f\left(x_k\right), x -x_k \right>} =0$)} \\
		& $\leq$ & $\limsup_{k \to \infty} \left[f\left(x_k\right) + \left<\triangledown f\left(x_k\right), x -x_k \right>\right]$ & \textblue{($\lim < \limsup$ et linéarité de $\limsup$)} \\
		& $\leq$ & $f(x)$ & \textblue{($f(x) \geq f(x_k) \left< \triangledown f\left(x_k\right), x -x _k \right>$)} \\
	\end{tabular}
	
	Comme l'inégalité précédente reste vraie pour tout $x \in \R^n$, on a 
	\begin{displaymath}
	\inf_{\R^n} f \underbrace{\leq}_{\sup} \liminf_{k \to \infty} f\left(x_k\right) \underbrace{\leq}_{\text{limites inf et sup}} \limsup_{k \to \infty} f\left(x_k\right) \underbrace{\leq}_{\text{inégalité précédente}} \inf_{\R^n} f
	\end{displaymath}
	On en déduit que $\liminf_{k \to \infty} f\left(x_k\right) = \limsup_{k \to \infty} f\left(x_k\right)$, ce qui implique que $\left(f\left(x_k\right)\right)_k$ admet une limite. De plus, les inégalités précédentes donnent $\inf_{\R^n} f = \limsup_{k \to \infty} f\left(x_k\right)$, ce qui implique, par unicité de la limite, que $\lim_{k \to \infty} f\left(x_k\right) = \inf_{\R^n} f$. 
\end{proof}

On va maintenant s'intéresser plus précisément à la suite $\left(x_k\right)_k$ définie par la méthode du gradient à pas optimal.

\begin{lnum}
	A chaque étape de la méthode, la direction $d_k = \triangledown f \left(x_k\right)$ est une direction descendante, c'est-à-dire $\min_{[0, b]} g_k(t) < f\left(x_k\right)$.
	\label{lem:directionDescendante}
\end{lnum}
\begin{proof}
	Comme on calcul $d_k = \triangledown f \left(x_k\right)$, on sait que la méthode ne satisfait pas le critère de l'arrêt $\mathopen{||}\triangledown f\left(x_k\right) \mathclose{||} \leq \epsilon$ n'a pas stoppé la méthode. On en déduit:
	
	\begin{tabular}{ccll}
		$\left< d_k, \triangledown f\left(x_k\right) \right>$ & $=$ & $\left< -\triangledown f\left(x_k\right), \triangledown f\left(x_k\right) \right>$ & \textblue{(définition de $d_k$)} \\
		& $=$ & $-\left< \triangledown f\left(x_k\right), \triangledown f\left(x_k\right) \right>$ & \textblue{(linéarité du produit scalaire)} \\
		& $=$ & $-\mathopen{||}\triangledown f\left(x_k\right)\mathclose{||}^2$ & \textblue{(relation norme - produit scalaire)} \\
		& $<$ & $0$ & \textblue{($\epsilon >0$  et $\mathopen{||}\triangledown f\left(x_k\right)\mathclose{||}^2 < \epsilon$)} \\
	\end{tabular}
	
	On raisonne par l'absurde: on suppose que $\min_{t \in [0, b]} g_k(t) \geq f(x_k)$. Alors, 
	
	\begin{tabular}{ccll}
		$\forall t \in [0, b], g_k(t) \geq f\left(x_k\right)$ & $\Leftrightarrow$ & $\forall t \in [0, b], f\left(x_k +td_k\right) \geq f\left(x_k\right)$ & \textblue{(définition de $g_k$)} \\
		& $\Rightarrow$ & $\left< d_k, \triangledown f\left(x_k\right) \right> = \lim_{t \to 0, t >0} \frac{f\left(x_k + td_k\right) - \left(x_k\right)}{t}$ & \textblue{(définition du gradient)} \\
		& $\Rightarrow$ & $\left< d_k, \triangledown f\left(x_k\right) \right> \geq \lim_{t \to 0, t >0} \frac{\overbrace{f\left(x_k\right) - \left(x_k\right)}^{=0}}{\underbrace{t}_{> 0}}$  & \textblue{(première équivalence)} \\
		& $\Rightarrow$ & $\left< d_k, \triangledown f\left(x_k\right) \right> \geq 0$  &  \\
	\end{tabular}
	On obtient donc une contradiction avec le premier item.
\end{proof}

\begin{pnum}
	Si $f$ est continuement différentiable sur $\R$ et coersive alors la suite $\left(x_k\right)_k$ est bornée, admet au moins une sous-suite convergente \textgreen{ (Attention: il y a  une erreur dans l'énoncé dans la référence)} et toute sous-suite convergente est stationnaire.
	\label{prop:SousSuiteConverge}
\end{pnum}
\begin{proof} 
	On suppose $\triangledown f\left(x_k\right) \neq 0$, $\forall k \in \N$. En effet, dans le cas où $\triangledown f\left(x_k\right) = 0$, la méthode s'arrête puisqu'on a trouver un extrema (qui peut être local). De plus, par la définition par récurrence, si $\triangledown f\left(x_k\right) = 0$, alors la suite est constante donc convergente et stationnarisante: elle vérifie les propriétés que l'on veut.
	
	\paragraph{Étape 1: existence d'une sous-suite convergente}
	Le lemme~\ref{lem:directionDescendante} nous assure que $\forall k \in \N$, $g_k(t) < f\left(x_k\right)$ ce qui est équivalent à dire que $f\left(x_{k+1}\right) = f(x_k + td_k)< f\left(x_k\right)$. Donc la suite $\left(f\left(x_k\right)\right)_k$ est décroissante. De plus, la décroissance de la suite $\left(f\left(x_k\right)\right)_k$, implique que $f\left(x_k\right) \leq f\left(x_0\right)$ donc $\left(x_k\right)_k$ est contenu dans le sous-niveau $S_{f(x_0)} = \{x \in \R^n, f(x) \leq f(x_0) \}$.
	
	De plus, la coerisivité de $f$, nous assure que $S_{f\left(x_0\right)}$ est compact. Or, comme la suite $\left(x_k\right)_k$ est contenu dans $S_{f\left(x_0\right)}$ qui est compact, on en déduit que la suite $\left(x_k\right)_k$ est borné. Donc l'ensemble des points adhérents de $\left(x_k\right)_k$, $\mathrm{Adh}\left(\left(x_k\right)_k\right)$, est non nul. D'où l'existence d'une sous-suite convergente.
	
	\paragraph{Étape 2: Montrer que tous les points de $\mathrm{Adh}\left(\left(x_k\right)_k\right)$ sont des points stationnaires de $f$.} Soit $\overline{x} = \lim_{n \to \infty}vx_{k_n}$. 
	
	Le lemme~\ref{lem:directionDescendante} nous assure que $\forall t\in ]0, b]$ et $\forall n \in \N$, on a:
	\begin{displaymath}
	f\left(x_{k_{n +1}}\right) \leq f\left(x_{k_{n} + 1}\right) \leq f\left(x_{k_{n}} - t \triangledown f\left(x_{k_{n}}\right)\right)
	\end{displaymath}
	
	Comme $f$ est continuement dérivable sur $\R^n$, $\forall t \in ]0, b]$:
	\begin{displaymath}
	\frac{f\left(\overline{x}-t\triangledown f\left(\overline{x}\right)\right)-f\left(\overline{x}\right)}{t} \underbrace{=}_{\overline{x} = \lim_{n \to \infty} x_{k_n}} \frac{f\left(x_{k_n}-t\triangledown f\left(x_{k_n}\right)\right)-f\left(x_{k_n}\right)}{t} \leq 0
	\end{displaymath}
	
	On en déduit que:
	\begin{displaymath}
	\left< \triangledown f\left(\overline{x}\right), -\triangledown f\left(\overline{x}\right)\right> \underbrace{=}_{\text{def du gradient}} \lim_{t \to 0} \frac{f\left(\overline{x}-t\triangledown f\left(\overline{x}\right)\right)-f\left(\overline{x}\right)}{t} \underbrace{\geq}_{\text{précédent}} 0
	\end{displaymath}
	
	Soit,
	\begin{displaymath}
	0 \underbrace{\leq}_{\text{précedent}} \left< \triangledown f\left(\overline{x}\right), -\triangledown f\left(\overline{x}\right)\right> \underbrace{=}_{\text{linéarité}} -\left< \triangledown f\left(\overline{x}\right), \triangledown f\left(\overline{x}\right)\right> \underbrace{=}_{\text{norme}} -\mathopen{||} \triangledown f\left(\overline{x}\right)\mathclose{||} \underbrace{\leq}_{\text{positivité norme}} 0
	\end{displaymath}
	
	Donc, $\mathopen{||} \triangledown f\left(\overline{x}\right)\mathclose{||} = 0$, $\overline{x}$ est stationnaire. La suite $\left(x_k\right)_k$ est stationnarisante. 
\end{proof}

\begin{theo}
	Supposons que la fonction $f$ est convexe et continuement différentiable sur $\R^n$.
	\begin{enumerate}
		\item Si $f$ est coersive, alors la suite $\left(x_k\right)_k$ admet au moins une sous-suite convergente et toutes les sous-suites convergentes sont minimisante.
		\item Si $f$ est fortement convexe, alors la suite $\left(x_k\right)_k$ est minimisante et converge vers une solution optimale d'un problème sans contraintes.
	\end{enumerate}
\end{theo}
\begin{proof}
	Supposons que la fonction $f$ est convexe et continuement différentiable sur $\R^n$.
	\begin{enumerate}
		\item C'est une conséquence des propositions~\ref{prop:suites} et~\ref{prop:SousSuiteConverge}. La proposition~\ref{prop:SousSuiteConverge} nous assure que la suite $\left(x_k\right)_k$ est bornée et admet une sous-suite convergente et toute sous-suite convergente est stationnarisante. La proposition~\ref{prop:suites} nous assure que la suite $\left(x_k\right)_k$ admet une sous-suite convergente et toute sous-suite convergente est minimisante. D'où le résultat.
		
		\item Supposons maintenant que $f$ est fortement convexe et différentiable, donc $f$ est coersive. 
		
		Le point 1 de notre théorème, il existe une sous-suite convergente et toute sous-suite convergente est minimisante. Soit $\left(x_n\right)_n$ une de ces sous-suites et $\overline{x}$ sa limite. Comme $f$ est continue et que la suite $\left(x_n\right)_n$ est minimisante : $f\left(\overline{x}\right) = \inf_{\R^n} f$. On en déduit que $\lim f\left(x_k\right) = f\left(\overline{x}\right) = \inf f(x)$.
		
		Comme $f$ est fortement convexe, $f$ est strictement convexe. Donc $\mathrm{Argmin}_{\R^n} f =\{\overline{x}\}$. Donc, pour toute sous-suite convergente de $\left(x_k\right)_k$ converge vers $\overline{x}$ \textblue{(par le lemme~\ref{lem:directionDescendante} et est incluse $S_{f(x_0)}(f)$}. Par la compacité de $S_{f(x_0)}(f)$, $\left(x_k\right)_k$ converge vers $\overline{x}$.
	\end{enumerate}
\end{proof}