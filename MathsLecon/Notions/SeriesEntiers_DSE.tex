%LECONS: 243

Nous avons étudier les propriétés de la fonction série entière et nous avons étudié quelques une de ces applications comme la résolution d'équations linéaires en développant en série entière la solution. Il est maintenant intéressant d'étudier les fonctions qui se développe en séries entière en répondant aux questions: \emph{qu'elles sont-elles?}, \emph{que puis-je faire comme opérations sur elles?}, \emph{admettent-elles des résultats intéressants?}.

\paragraph{Notions de fonctions dérivables en séries entière} Nous avons vu, lors de l'étude de la fonction série entière, qu'une série entière décrit une fonction $\mathcal{C}^{\infty}$. Cependant toutes les fonctions $\mathcal{C}^{\infty}$ n'admettent pas nécessairement un développement en série entière. Nous allons alors définir qu'est-ce qu'une fonction développable en série entière \cite[p.49]{ArnaudiesFraysse-3}.

\begin{definition}[Série de Taylor--Mac Laurin]
	Soit $I$ un intervalle non trivial de $\R$ contenant $0$ à toute fonction $f: I \to \C$ de classe $\mathcal{C}^{\infty}$, on peut associer sa série formelle de Taylor--Mac Laurin
	\begin{displaymath}
	T_f = \sum_{n \geq 0} \frac{1}{n!}f^{(n)}(0)X^n \in \C[X]
	\end{displaymath}
\end{definition}

Deux questions naturelles apparaissent alors: si $f \in \mathcal{C}^{\infty}$,
\begin{itemize}
	\item Quel est le rayon de convergence de sa série de Taylor--Mac Laurin associée à $T_f$?
	\item En supposant que ce rayon de convergence est strictement positif, quel rapport existe-t-il entre les fonctions $f$ et $T_f$?
\end{itemize}
La théorie du développement en série entière vient répondre à ces questions.

\begin{definition}[Développable en série entière]
	Soit $A$ une partie de $\C$ admettant $0$ comme point d'accumulation, et $f: A \to \C$. On dit que $f$ est développable en série entière et on écrit que $f$ est $DSE_0$, si et seulement si il existe une série entière $S$ telle que sa fonction série entière est $f$ pour tout $z$ appartenant à un voisinage de $0$ dans $A$.
\end{definition}

\begin{req}
	On peut étudier les développement en série entière en n'importe quel point d'accumulation de $\C$ en étudiant le développement en série entière en $0$ de sa fonction translaté de $a$ (ce qui nous permet de recentrer la fonction).
\end{req}

\begin{req}
	Si le développement en série entière existe alors la série entière correspondant à $f$ est unique.
\end{req}

\begin{prop}[Caractérisation des fonctions développable en série entière \protect{\cite[p.240]{Gourdon-analyse}}]
	Soit $I$ un intervalle de $\R$ contenant un voisinage de $0$. Une fonction $f$ de classe $\mathcal{C}^{\infty}$ est $DSE_0$ si et seulement s'il existe $\alpha > 0$ tel que la suite de fonctions $\left(R_n\right)$ définie par
	\begin{displaymath}
	R_n(x) = f(x) - \sum_{k=0}^{n} \frac{f^{(k)}(0)}{k!}x^k
	\end{displaymath}
	tende simplement vers $0$ sur $\left]-\alpha, \alpha\right[$. La série entière $\sum \frac{f^{(k)}(0)}{k!}z^k$ a un rayon de convergence supérieur ou égal à $\alpha$ et $f$ est la fonction de cette série entière sur $\left]-\alpha, \alpha\right[$.
\end{prop}
{\small{\begin{proof}
	Pour tout $x \in \left]-\alpha, \alpha\right[$ la série $\sum_{k=0}^{n} \frac{f^{(k)}(0)}{k!}x^k$ converge vers $f(x)$ car $R_n(x) \to 0$. En particulier, la suite $\left(\frac{f^{k}(0)}{k!}x^k\right)_{k \in \N}$ tend vers $0$, elle est donc bornée. Par les critère de convergence d'une série entière, on en déduit que le rayon de convergence de la série entière $\sum \frac{f^{(k)}(0)}{k!}z^k$ est supérieur à $\alpha$.
\end{proof}}}

\begin{req}
	Ce résultat reste encore vrai dans $\C$.
\end{req}

Dans la pratique pour montrer que $R_n$ tend vers $0$ on peut écrire $f$ avec sa formule de Taylor (avec reste de Lagrange ou reste intégral). En effet, on fait apparaître des expressions dont il faut montrer qu'elles tendent vers $0$: $\frac{x^{n+1}}{(n+1)!} f^{(n+1)}(\theta x)$ ou $\int_{0}^{x} \frac{(x-t)^n}{n!} f^{(n+1)}(t)dt$. Cependant, pour montrer que $f$ est dérivable en série entière, il est inutile de montrer que la série  $\sum_{k=0}^{n} \frac{f^{(k)}(0)}{k!}x^k$ à un rayon de convergence positif. En revanche si celui-ci est nul, alors $f$ n'est pas développable en série entière.

\begin{ex}[Fonction exponentielle \protect{\cite[p.241]{Gourdon-analyse}}]
	La fonction $f :\R \to \R$ définie par $x \mapsto e^x$ est de classe $\mathcal{C}^{\infty}$ sur $\R$ et vérifie $f^{(n)}(0) = 1$ pour tout $n$. D'après la formule de Taylor--Lagrange, on a
	\begin{displaymath}
	\begin{array}{cc}
	\forall n \in \N, \forall x \in \R, \exists \theta \in \left]0,1\right[, & R_n(x) = f(x) - \left(1 + x + \dots + \frac{x^n}{n!}\right) = \frac{x^{n+1}}{(n+1)!}e^{\theta x}
	\end{array}
	\end{displaymath}
	donc $\left|R_n(x)\right| \leq \frac{\left|x\right|^{n+1}}{(n+1)!}e^{\left|x\right|}$. D'où $R_n(x) \to 0$ et par la proposition précédente on a que $\forall x \in \R$, $e^x = \sum_{n=0}^{\infty}\frac{x^n}{n!}$.
\end{ex}

\begin{cex}[Fonction $\mathcal{C}^{\infty}$ dont le rayon de convergence de la série de Taylor--Mac Laurin est nul \protect{\cite[p.241]{Gourdon-analyse}}]
	Soit $\varphi : R_+ \to \R$ une fonction de classe $\mathcal{C}^{\infty}$ définie par $x \mapsto \int_{0}^{\infty} \frac{e^{-t}}{1+xt}dt$. Elle vérifie pour tout $n$, $\varphi^{(n)}(x) = (-1)^nn!\int_{0}^{+\infty}\frac{e^{-t}}{1+xt}dt$. En particulier, pour tout $n$, $\varphi^{(n)}(0) = (-1)^nn!\Gamma(n+1) = (-1)^n(n!)^2$. La fonction paire $f(x) = \varphi\left(x^2\right)$ est de classe $\mathcal{C}^{\infty}$ sur $\R$ et $\sum \frac{f^{(n)}(0)}{n!}z^n = \sum \frac{\varphi^{(n)}(0)}{n!}z^{2n}$ a un rayon de convergence nul.
	
	Plus généralement, pour n'importe quelle suite $(a_n)$ (en particulier telle que $\sum a_n z^n$ a un rayon de convergence nul), il existe une fonction $f$ de classe  $\mathcal{C}^{\infty}$ sur $\R$ telle que $\frac{f^{(n)}(0)}{n!} = a_n$ (\textblue{théorème de réalisation de Borel \cite[p.280]{Gourdon-analyse}}).
\end{cex}

\begin{cex}[Fonction $\mathcal{C}^{\infty}$ dont le rayon de convergence de la série de Taylor--Mac Laurin est non nul mais qui n'est pas développable en série entière \protect{\cite[p.241]{Gourdon-analyse}}]
	La fonction $f: \R \to \R$ définie par $x \mapsto e^{-\frac{1}{x^2}}$ et $f(0) =0$ est de classe $\mathcal{C}^{\infty}$ et vérifie $f^{(n)} (0) = 0$ pour tout $n$. La série entière $\sum_{n=0}^{\infty}\frac{x^n}{n!} = 0$ a un rayon de convergence infini mais ne coïncide pas avec $f$ car $f$ est strictement positive sauf en $0$. Donc $f$ n'admet pas de développement en série entière.
\end{cex}

Comme nous avons vu cette caractérisation est compliquée à calculer. De plus, toutes les fonctions de classe $\mathcal{C}^{\infty}$ ne sont développable en série entière. Voici un autre résultat qui nous permet de raffiner un peu cette caractérisation.
\begin{theo}[Développement de Taylor contrôlé \protect{\cite[p.57]{ArnaudiesFraysse-3}}]
	Soit $I$ un intervalle de $\R$ contenant un voisinage de $0$ et $f : I \to \R$ une fonction de classe $\mathcal{C}^{\infty}$. Pour que $f$ soit $DSE_0$, il faut et il suffit qu'il existe $C > 0$, $A > 0$ et $r > 0$ tels que
	\begin{displaymath}
	\begin{array}{cc}
	\forall n \in \N, \forall x \in \left]-r, r\right[ \cap I, & \left|f^{(n)}(x)\right| \leq CA^nn! \\
	\end{array}
	\end{displaymath} 
\end{theo}
{\small{\begin{proof}
	Supposons que la condition est exacte. On en déduit que $\forall n \in \N$, $\forall x \in \left]-r, r\right[ \cap I$, 
	\begin{displaymath}
	\left|R_n(x)\right| \leq \frac{\left|x\right|^{n+1}}{(n+1)!} \times CA^nn! \leq Cr\left(A\left|x\right|\right)^n
	\end{displaymath}
	En particulier, si $x \in \left]-\alpha,\alpha\right[ \cap I$, où $\alpha = \min \left(r, \frac{1}{A}\right)$, on voit que $R_n(x) \underset{n \to \infty}{\rightarrow} 0$. Donc $f$ est $DSE_0$ par la caractérisation précédente.
	
	Réciproquement, supposons que $f$ est $DSE_0$ et posons $T_f(x) = \sum_{n \geq 0}a_nx^n$ avec $a_n = \frac{f^{(n)}(0)}{n!}$ pour tout $n$. Par hypothèse, il existe $\alpha > 0$ tel que $\alpha < R$ où $R$ est le rayon de convergence de $T_f$ et que , pour $x \in \left[-\alpha, \alpha\right]\cap I$: $f(x) = \sum_{n=0}^{\infty} a_nx^n$. Comme $\alpha < R$, la série $\sum a_n \alpha^n$ converge absolument, ce qui permet de poser $M = \sum_{n=0}^{\infty} \left|a_n\right|\alpha^n$, d'où $\forall n$, $\left|a_n\right| \leq \frac{M}{\alpha^n}$. De plus, en dérivant successivement, on a
	\begin{displaymath}
	\begin{array}{cc}
	\forall p \in \N, \forall x \in \left]-r, r\right[ \cap I, & f^{(p)}(x) = \sum_{n=p}^{\infty} n(n-1) \dots (n-p+1)a_nx^{n-p} \\
	\end{array}
	\end{displaymath} 
	d'où la majoration 
	\begin{displaymath}
	\begin{array}{cc}
	\forall p \in \N, \forall x \in \left]-r, r\right[ \cap I, & \left|f^{(p)}(x)\right| \leq \sum_{n=p}^{\infty} n(n-1) \dots (n-p+1)\frac{M}{\alpha^n}\left|x\right|^{n-p} \\
	\end{array}
	\end{displaymath} 
	Soit $r \in \left]0, \alpha\right[$. Pour $x \in I \cap [-r, r]$, on en déduit:
	\begin{displaymath}
	\begin{array}{ccll}
	\left|f^{(p)}(x)\right| & \leq & \sum_{n=p}^{\infty} n(n-1) \dots (n-p+1)\frac{M}{\alpha^p}\left(\frac{r}{\alpha}\right)^{n-p} & \textblue{\text{intro du } r \text{ et séparation des puissances}} \\
	 & = & \frac{M}{\alpha^p}p!\sum_{n=p}^{\infty} \binom{n}{p}\left(\frac{r}{\alpha}\right)^{n-p} & \textblue{\text{application de la formule du binome}} \\
	 & = & \frac{M}{\alpha^p}p!\sum_{q=0}^{\infty} \binom{p+q}{p}\left(\frac{r}{\alpha}\right)^{q} & \textblue{\text{changement d'indice}} \\
	 & = & \frac{M}{\alpha^p}p!\left(1-\frac{r}{\alpha}\right)^{-p-1} & \textblue{\text{évaluation de la somme}} \\
	 & = & \frac{M}{1-\frac{r}{\alpha}}\left(\frac{1}{\alpha - r}\right)^{p}p! & \\
	\end{array}
	\end{displaymath}
\end{proof}}}

\paragraph{Calcul d'un développement en série entière} \emph{Comment développer une fonction en série entière?} Pour cela, nous avons essentiellement deux méthodes: la première est d'exprimer notre fonction à l'aide d'opérations sur les fonctions usuelles car comme on va le voir le développement en série entière protège certaines opérations. Si ce n'est pas possible, on peut utiliser les équations différentielles et leur méthode de résolution pour déterminer les coefficients de notre série par identifications aux coefficient de l'équation différentielle (équation de Bessel \cite[p.101]{FrancinouGianellaNicolas-an4}). Ici nous ne donnons que la méthode à l'aide des fonctions usuelles. La méthode des équations différentielles consistant en la résolution d'une équation différentielle bien choisie avec la méthode des séries entières, elle est traité dans le paragraphe consacré à la dite résolution des équations différentielles à l'aide de séries entières.

\begin{prop}[Opérations sur un développement en série entière]
	\begin{enumerate}
		\item Si $f : A \to \C$ est $DSE_0$ alors si $0 \in A$, elle est continue en $0$. Sinon, si $0 \notin A$, elle se prolonge par continuité en $0$.
		\item Si $f : A \to \C$ et $g : A \to \C$ sont $DSE_0$, alors pour tout $(\lambda, \mu) \in \C^2$, les fonctions $\lambda f + \mu g$ et $fg$ sont $DSE_0$.
		\item Supposons que $f :A \to \C$ soit $DSE_0$ et que $\underset{t \to 0}{\lim} f(z) \neq 0$, alors la fonction $\frac{1}{f}$ définie sur un voisinage de $0$ dans $A$ est $DSE_0$.
		\item Si $f : A \to \C$ est $DSE_0$ et $\underset{t \to 0}{\lim} f(z) = 0$, alors la fonction définie sur $A \setminus \{0\}$ par $z \mapsto \frac{f(z)}{z}$ est $DSE_0$.
		\item La dérivation et l'intégration passent au développement en série entière.
	\end{enumerate}
\end{prop}
{\small{\begin{proof}
	Provient des propriétés sur les séries entières.
\end{proof}}}

On donne ici un tableau des développement en série entière des principales fonctions usuelles \cite[p.493]{Marco-Thieullen-Weil}.

\begin{tabular}{|c|c|c|}
	\hline
	Fonction & Développement en série entière & Validité \\
	\hline
	$e^x$ & $\sum_{n=0}^{\infty} \frac{x^n}{n!}$ & $\R$ \\
	$\cosh x$ & $\sum_{n=0}^{\infty} \frac{x^{2n}}{(2n)!}$ & $\R$ \\
	$\sinh x$ & $\sum_{n=0}^{\infty} \frac{x^{2n+1}}{(2n +1)!}$ & $\R$ \\
	$\cos x$ & $\sum_{n=0}^{\infty} \frac{(-1)^nx^{2n}}{(2n)!}$ & $\R$  \\
	$\sin x$ & $\sum_{n=0}^{\infty} \frac{(-1)^nx^{2n+1}}{(2n +1)!}$ & $\R$ \\
	$(1+x)^{\alpha}$ & $1+\sum_{n=1}^{\infty} \frac{\prod_{k=1}^{n}(\alpha - k + 1)}{n!}x^n$ & $\left]-1,1\right[$ et $\alpha \in \R \setminus \N$ \\
	$\ln(1+x)$ & $\sum_{n=1}^{\infty} \frac{(-1)^n}{n}x^n$ & $\left]-1, 1\right]$ \\
	$\arctan x$ & $\sum_{n=0}^{\infty} \frac{(-1)^n}{2n +1}x^{2n+1}$ & $\left[-1,1\right]$ \\
	$\arcsin x$ & $x+\sum_{n=1}^{\infty} \frac{(2n)!}{2^{2n}(n!)^2(2n +1)}x^{2n+1}$ &$\left[-1,1\right]$\\
	\hline
\end{tabular}

\begin{ex}[Utilisation des fonctions usuelles et des opérations sur le développement \protect{\cite[p.43, ex8, q.b]{ArnaudiesFraysse-3-exresolu}}]
	Développons en série entière en $0$ la fonction $x \mapsto \ln(1+x) \times \ln(1-x)$. Remarquons qu'elle est bien développable en série entière au voisinage de $0$. On trouve que pour $x \in \left]-1,1\right[$:
	\begin{displaymath}
	f'(x) = \frac{\ln(1-x)}{1+x} - \frac{\ln(1+x)}{1-x}
	\end{displaymath}
	Comme pour tout $x \in \left]-1,1\right[$ (\textblue{en utilisant les fonctions usuelles et les fractions rationnelles}):
	\begin{displaymath}
	\begin{array}{ccc}
	\ln(1-x) = - \sum_{n=0}^{\infty} \frac{c^{n+1}}{n+1} & \text{ et } & \frac{1}{1+x} = \sum_{n=0}^{\infty} (-1)^nx^n
	\end{array}
	\end{displaymath}
	par la formule des produits de séries entières, on a:
	\begin{displaymath}
	\frac{\ln(1-x)}{1+x} = \sum_{n=0}^{\infty} b_nx^n
	\end{displaymath}
	où pour tout $n \in \N$
	\begin{displaymath}
	b_n = - \sum_{k+1+h=n} \frac{(-1)^k}{h+1} = (-1)^n\left(1-\frac{1}{2}+ \dots + (-1)^{n-1}\frac{1}{n}\right)
	\end{displaymath}
	Nous en déduisons pour tout $x \in \left]-1,1\right[$
	\begin{displaymath}
	f'(x) = \sum_{m=0}^{\infty} b_{2m+1}x^{2m+1}
	\end{displaymath}
	Par intégration (\textblue{en notant que $f(0) = 0$}), on voit que pour tout $x \in \left]-1,1\right[$:
	\begin{displaymath}
	f(x) = 2\sum_{m=0}^{\infty} \frac{b_{2m+1}}{2n+2}x^{2m+2} = \sum_{n=0}^{\infty} a_{2n}x^{2n}
	\end{displaymath}
	où pour tout $n \in \N^*$:
	\begin{displaymath}
	a_{2n} = \frac{2b_{2n-1}}{2n} = \frac{1}{n} \sum_{k=1}^{2n-1}\frac{(-1)^k}{k}
	\end{displaymath}
	Comme 
	\begin{displaymath}
	\sum_{k=1}^{\infty} \frac{(-1)^{k-1}}{k} = \ln 2
	\end{displaymath}
	nous en déduisons que $a_{2n} \underset{n \to \infty}{\sim} -\frac{\ln 2}{n}$.
\end{ex}

\begin{ex}[Utilisation d'une équation différentielle \protect{\cite[p.46, ex9, q.a]{ArnaudiesFraysse-3-exresolu}}]
	Développons en série entière en $0$ la fonction définie par $x \mapsto \exp (i\lambda \arcsin x)$ où $\lambda \in \R$.  Remarquons qu'elle est bien développable en série entière au voisinage de $0$. Posons pour tout $\varphi \in \left]-\frac{\pi}{2},\frac{\pi}{2}\right[$:
	\begin{displaymath}
	g(\varphi) = f(\sin \varphi) = e^{i\lambda\varphi}
	\end{displaymath}
	Par dérivation, on obtient pour tout $\varphi \in \left]-\frac{\pi}{2},\frac{\pi}{2}\right[$:
	\begin{displaymath}
	\cos \varphi f'(\sin \varphi) = i\lambda e^{i\lambda\varphi}
	\end{displaymath}
	puis:
	\begin{displaymath}
	-\sin \varphi \varphi f'(\sin \varphi) + \cos^2 \varphi f''(\sin \varphi) = -\lambda^2 e^{i\lambda\varphi} = -\lambda^2f(\sin \varphi)
	\end{displaymath}
	Nous en déduisons que pour tout  $x \in \left]-1,1\right[$:
	\begin{displaymath}
	f''(x) = x^2 f''(x) + x f'(x) - \lambda^2f(x)
	\end{displaymath}
	On a donc l'égalité formelle:
	\begin{displaymath}
	\sum_{n=0}^{\infty} (n+2)(n+1)a_{n+2}X^n = \sum_{n=0}^{\infty} \left(n(n-1)+n - \lambda^2\right)a_nX^n
	\end{displaymath}
	d'où nous déduisons que pour tout $n \in \N$:
	\begin{displaymath}
	a_{n+2} = \frac{n^2 - \lambda^2}{(n+2)(n+1)}a_n
	\end{displaymath}
	D'autre part on trouve facilement $f(0) = a_0 = 1$ et $f'(0) = a_1 = i\lambda$. On voit donc que pour tout $p \in \N^*$
	\begin{displaymath}
	\begin{array}{ccc}
	a_{2p} = \frac{\prod_{k=0}^{p-1} \left((2k)^2 - \lambda^2\right)}{(2p)!} & \text{ et } & a_{2p+1} = i\lambda\frac{\prod_{k=0}^{p-1} \left((2k+1)^2 - \lambda^2\right)}{(2p+1)!} 
	\end{array}
	\end{displaymath}
\end{ex}

\paragraph{Fonctions analytiques} Comme dit précédemment les fonctions dérivable en série entière sont aussi appelée fonction analytique. Nous allons les définir et donner quelques propriétés intéressante \cite[p.72]{ArnaudiesFraysse-3}.

\begin{definition}[Fonction analytique]
	Soit $\Omega$ un ouvert non vide de $\C$, et $f: \Omega \to \C$. On dit que $f$ est analytique sur $\Omega$ si et seulement si pour tout $a \in \Omega$, $f$ est $DSE_a$.
	
	On note $\mathcal{H}(U)$ l'ensemble des fonctions analytiques sur $U$.
\end{definition}

\begin{prop}[Propriétés sur les fonctions analytiques]
	\begin{enumerate}
		\item Soit $f \in \mathcal{H}(U)$. Alors $f$ est indéfiniment $\C$-dérivable sur $U$ et pour tout $n$, $f^{(n)} \in \mathcal{H}(U)$.
		\item La série entière associé à $f$ est sa série de Taylor--Mac Laurin.
		\item $\mathcal{H}(U)$ est une sous-$\C$-algèbre de la $\C$-algèbre $\mathcal{F}(U, \C)$.
		\item Si $f \in \mathcal{H}(U)$ et si $f(z) \neq 0$ pour tout $z \in U$, alors $\frac{1}{f} \in \mathcal{H}(U)$.
		\item $\mathcal{H}(U)$ est stable par composition.
	\end{enumerate}
\end{prop}
{\small{\begin{proof}
	Par les propriétés du développement en série entière.
\end{proof}}}

\begin{ex}
	La fonction $\exp : \C \to \C$ définie telle que $z \mapsto e^z$ est analytique sur $\C$. En effet, soit $a \in \C$, alors pour tout $z \in \C$, $\exp(z+a) = \exp(z)\exp(a) = e^a\sum_{n=0}^{\infty}\frac{z^n}{n!}$, ce qui montre que $\exp$ est $DSE_a$.
\end{ex}

Par la proposition précédente et grâce à cet exemple, on montre que la majorité des fonctions usuelles sont analytiques.