%LECONs : 105
On montre que toute permutation de $\Sn(E)$ se décompose en produit de cycles à supports disjoints \cite[p.205]{Berhuy}. L'idée est que pour bien connaître une permutation, il suffit de connaître son action sur chaque orbite non réduite à un élément. La première étape est d'associer à chaque orbite $w$ un cycle de support $w$. Soit $\sigma \in \Sn(E)$. Soit $\Omega^*$ l'ensemble des $\sigma$-orbites non réduites à un élément. Si $w \in \Omega^*$, on pose $\sigma_{w}(a) = \sigma(a)$ si $a \in w$ et $\sigma_{w}(a) = a$ si $a \notin w$.

\begin{lemme}
	Soit $\sigma \in \Sn(E)$, et soit $w \in \Omega^*$ une orbite non réduite à un élément. Alors, $\sigma_{w}$ est un cycle de support $w$, et pour tout $a \in w$, on a $\sigma_w = \left(a ~ \sigma(a) ~ \dots ~ \sigma^{p-1}(a)\right)$ où $p$ est le nombre d'éléments de $w$. De plus, tout $p$-cycle de $\Sn(E)$ est de cette forme.
\end{lemme}
\begin{footnotesize}
	\begin{proof}
		Soit $a \in w \in \Omega^*$, $p$ est le plus petit entier tel que $\sigma^p(a) = a$, et l'on a $w = \{a, \sigma(a), \dots, \sigma^{p-1}(a)\}$. Si $a' \notin w$, $\sigma(a') = a'$. De plus, $\sigma_w\left(\sigma^i(a)\right) = \sigma^{i+1}(a)$.
		
		Si $\sigma$ est un $p$-cycle, posons $w = \mathrm{Supp}(\sigma)$. Si $a \in w$, on a $\sigma_w(a) = \sigma(a)$. Et si $a \notin w$, $\sigma_w(a) = a = \sigma(a)$.
	\end{proof}
\end{footnotesize}


\begin{theo}
	Soit $\sigma \in \Sn(E)$. Alors $\sigma$ se décompose en produit de cycles à supports disjoints, et cette décomposition est unique à l'ordre des facteurs près. Cette décomposition est donnée par $\sigma = \prod_{w \in \Omega^*} \sigma_w$ où $\Omega^*$ l'ensemble des $\sigma$-orbites non réduites à un élément.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		Montrons que $\sigma_w$ sont des cycles à supports disjoints deux à deux: les cycles commutent. Soit $a \in \mathrm{Supp}(\sigma)$ et $w_0$ tel que $\mathrm{Orb}_{\sigma}(a)$, on a $\prod_{w \in \Omega^*} \sigma_w(a) = \sigma_{w_0}\left(\left(\prod_{w \in \Omega^*\setminus \{w_0\}} \sigma_w\right)(a)\right) = \sigma_{w_0}(a) = \sigma(a)$. Montrons l'unicité de la décomposition. Comme les cycles commutent, le support de $\sigma$ est la réunion des $E_i = \{a, \sigma(a), \dots, \sigma^{p_i-1}(a)\}$.
	\end{proof}
\end{footnotesize}

\begin{appli}
	La méthode pratique consiste à prendre un élément qui n'est pas encore dans un cycle et de calculer l'orbite d'un élément. Pour ce faire, on applique $\sigma$ itérativement jusqu'à obtenir le cycle.
\end{appli}


\paragraph{Application : calcul de l'ordre d'un élément} La décomposition en cycle à support disjoint nous permet de calculer l'ordre d'une permutation.

\begin{lemme}
	Soit $\sigma_1, \dots, \sigma_r \in \Sn(E)$ des permutations à supports deux à deux disjoints. Alors, on a $o(\sigma_1 ~ \dots ~ \sigma_r) = \mathrm{ppcm}(o(\sigma_1), \dots, o(\sigma_r))$.
\end{lemme}
\begin{footnotesize}
	\begin{proof}
		Par récurrence sur $r \in \N^*$. Si $r = 1$, ok. Si $r = 2$, par la commutativité des cycles et par les relations sur les supports. Pour $r \in \N^*$, on applique les même arguments et l'hypothèse de récurrence.
	\end{proof}
\end{footnotesize}

\begin{theo}
	L'ordre d'une permutation est le $\mathrm{ppcm}$ des longueurs des cycles à supports disjoints qui la composent.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		Par le lemme précédent et l'ordre des cycles, on conclut.
	\end{proof}
\end{footnotesize}

\paragraph{Application: classes de conjugaison de $\Sn(E)$} On caractérise les classes de conjugaison de $\Sn(E)$ grâce à la décomposition des cycles à supports disjoints.

\begin{definition}
	Deux éléments $\gamma$, $\gamma'$ de $\Sn(E)$ sont dits conjugués s'il existe $\sigma \in \Sn(E)$ tel que $\sigma\gamma\sigma^{-1} = \gamma'$.
\end{definition}

\begin{lemme}
	Soit $\sigma_\in \Sn(E)$. Pour tout $p$-cycle $(a_1 ~ \dots ~ a_p)$, on a $\sigma(a_1 ~ \dots ~ a_p)\sigma^{-1} = (\sigma(a_1) ~ \dots ~ \sigma(a_p)$. En particulier, le conjugué d'un cycle est encore un cycle de même longueur.
\end{lemme}
\begin{footnotesize}
	\begin{proof}
		Soit $\sigma' = \sigma(a_1 ~ \dots ~ a_p)\sigma^{-1}$. Alors, si $i \in \llbracket 1, p-1 \rrbracket$, on a $\sigma'(\sigma(a_i)) = \sigma((a_1 ~ \dots ~ a_p)(a_i)) = \sigma(a_{i+1})$ et si $i = p$, $\sigma'(\sigma(a_p)) = \sigma((a_1 ~ \dots ~ a_p)(a_p)) = \sigma(a_{1})$.
	\end{proof}
\end{footnotesize}

\begin{theo}
	Deux permutations sont conjugués dans $\Sn(E)$ si et seulement si les listes des longueurs des cycles à supports disjoints qui les composent sont les même à l'ordre près.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		Soient $\sigma$, $\sigma'$ conjugués: on applique le lemme. Réciproquement, on les décompose en cycle disjoints puis on définie une bijection entre ces cycles.
	\end{proof}
\end{footnotesize}

\begin{definition}
	Soit $n \geq 1$ un entier. Une partition de $n$ est une suite d'entiers décroissantes $(p_k)_{k\geq 1}$, nulle à partir d'un certain rang, et telle que $\sum_{k \geq 1} p_k = n$.
\end{definition}

\begin{definition}
	Si $\sigma \in \Sn(E)$, le type de $\sigma$ est la partition de $n$ dont les éléments non nuls sont les cardinaux des divers $\sigma$-orbites, rangés par ordre décroissant.
\end{definition}

\begin{cor}
	Soit $E$ un ensemble à $n$ éléments, et soit $\mathcal{C}_E$ l'ensemble des classes de conjugaison de $\Sn(E)$. Alors l'application $\psi : \mathcal{P}(n) \to \mathcal{C}_E$ qui à $p$ associe $\mathrm{Conj}_{\Sn(E)}(\sigma_p)$ est une bijection. Autrement dit, toute permutation de $\Sn(E)$ est conjuguée à une unique permutation de la forme $\sigma_p$ où $p$ est une permutation de $n$.
\end{cor}
\begin{footnotesize}
	\begin{proof}
		Deux permutations conjuguées sont de même type.
	\end{proof}
\end{footnotesize}
