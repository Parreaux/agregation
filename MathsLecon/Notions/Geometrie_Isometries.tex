%LECONs: 101 (Actions de groupes); 182 (Nombre complexe et geometrie)
%DEVs : Isometries
Nous allons rappeler quelques résultats sur les isométries ainsi que leurs applications dans le cadre de solide platoniciens. Les isométries sont des applications qui conservent les distances. On distinguera ensuite celle qui conserve l'orientation et celle qui ne les conservent pas.

\emph{Cadre}: On se place dans $E_n$ un espace affine euclidien orienté de dimension $n$ et $\overrightarrow{E_n}$ l'espace vectoriel euclidien orienté associé à $E_n$. On pose $\left(O, \overrightarrow{e_1}, \dots, \overrightarrow{e_n}\right)$. Dans le cas d'un espace orienté, on considère la base orthonormée directe $\left(\overrightarrow{e_1}, \dots, \overrightarrow{e_n}\right)$.

\paragraph{Généralité sur les isométries} Donnons la définition des isométries avec leur transformation orthogonale associée. Nous allons également à donner des symétries orthogonales et des réflexions.

\begin{definition}
	On appelle une isométrie de $E_n$, toute application $f$ de $E_n$ dans $E_n$ vérifiant $\forall M, N \in E_n$, $\lVert \overrightarrow{f(M)f(N)} \rVert = \lVert \overrightarrow{MN} \rVert$.
\end{definition}

\begin{prop}
	Soit une application orthogonale $l$ de $O\left(\overrightarrow{E_n}\right)$, alors toute application affine $f$ de partie linéaire $l$ est une isométrie de $E_n$.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		Soit $f$ une application affine de partie linéaire $l$ orthogonale: $\forall M, N \in E_n$, $\lVert \overrightarrow{f(M)f(N)} \rVert = \lVert l\left(\overrightarrow{MN}\right) \rVert = \lVert \overrightarrow{MN} \rVert$.
	\end{proof}
\end{footnotesize}

\begin{theo}
	Soit $f$ une isométrie de $E_n$ et soit $A$ un point fixé de $E_n$.
	\begin{enumerate}
		\item Soit $\overrightarrow{u} \in \overrightarrow{E_n}$. La relation $l$ de $\overrightarrow{E_n}$ dans $\overrightarrow{E_n}$ qui à $\overrightarrow{u} = \overrightarrow{AM}$ associe $l\left(\overrightarrow{u}\right) = \overrightarrow{f(A)f(M)}$, est une application orthogonale de $O\left(\overrightarrow{E_n}\right)$.
		\item $f$ est une application affine de partie linéaire une application orthogonale de $O\left(\overrightarrow{E_n}\right)$.
		\item Soit $O \in E_n$. Pour toute application orthogonale $l$ de $O\left(\overrightarrow{E_n}\right)$, alors, il existe une unique isométrie $f$ de partie linéaire $l$ et telle que $f(O) = O$.
	\end{enumerate}
\end{theo}
\begin{footnotesize}
	\begin{proof}
		\begin{enumerate}
			\item On utilise les propriétés d'isométrie pour montrer que $l$ est une application orthogonale.
			\item Cas particulier du premier item.
			\item Par l'orthogonalité de $l$, on montrer que $f$ conserve les distance. Comme une application affine est complètement déterminée par l'image d'un point et de l'application linéaire associée, $f$ est unique.
		\end{enumerate}
	\end{proof}
\end{footnotesize}

\noindent\begin{minipage}{.62\textwidth}
	\begin{cor}
		Soit $f$ une isométrie de $E_n$, alors $f$ est une application affine bijective et donc $Is(E_n) \subset GA(E_n)$ où $Is(E_n)$ est l'ensemble des isométries de $E_n$.
	\end{cor}
\end{minipage} \hfill
\begin{minipage}{.35\textwidth}
	\begin{footnotesize}
		\begin{proof}
			Affine : théorème précédent. 
			
			Bijection : application orthogonale.
		\end{proof}
	\end{footnotesize}
\end{minipage}

\subparagraph{Symétries orthogonales} Soit l'espace vectoriel euclidien $(E, \varphi)$ et $F$ un sous-espace vectoriel de $E$. Une symétrie vectorielle de base $F$ et de direction $F^{\bot}$ s'appelle la symétrie orthogonale vectorielle de base $F$.

\begin{prop}
	Soit $\overrightarrow{E_n}$ un espace vectoriel euclidien, alors $s$ est une transformation orthogonale involutive de $\overrightarrow{E_n}$ si et seulement si $s$ est une symétrie orthogonale.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		\begin{description}
			\item[$\Rightarrow$] $s$ est une isométrie vectorielle : transformation orthogonale involutive. $s$ est orthogonale: $\forall x \in F$, $\forall y \in G$, $s(x).s(y) = x.y$.
			\item[$\Leftarrow$] $s$ est une application linéaire : $s$ est une symétrie orthogonale vectorielle. $s$ est involutive : $s$ conserve la norme.
		\end{description}
	\end{proof}
\end{footnotesize}

\begin{definition}
	Soit l'espace affine euclidien $E_n$, d'espace vectoriel associé $\overrightarrow{E_n}$ et $\mathcal{F}$ une variété linéaire affine de direction $F$, la symétrie de base $\mathcal{F}$ et de direction $F^{\bot}$ s'appelle symétrie orthogonale de base $\mathcal{F}$.
\end{definition}

\begin{prop}
	Soit $\overrightarrow{E_n}$ un espace vectoriel euclidien, alors une isométrie $f$ de $E_n$ est une involutive si et seulement si $f$ est une symétrie orthogonale.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		\begin{description}
			\item[$\Rightarrow$] $l$ est une symétrie vectorielle orthogonale : $l$ est une transformation orthogonale involutive \textblue{($f$ est une isométrie involutive)}. $f$ est isométrie orthogonale: $f^2 = Id_{E_n}$, $l$ est la symétrie orthogonale de base $F$ et de direction $F^{\bot}$.
			\item[$\Leftarrow$] $f$ est involutive : $f$ est une symétrie. $f$ est une isométrie : partie linéaire de $f$ est une transformation orthogonale.
		\end{description}
	\end{proof}
\end{footnotesize}

\subparagraph{Réflexion}

\begin{definition}
	Soit l'espace affine euclidien $E_n$ et $\mathcal{F}$ un hyperplan affine, la symétrie orthogonale de base $\mathcal{F}$ s'appelle réflexion de $E_n$ ou symétrie orthogonale hyperplane de base $\mathcal{F}$.
\end{definition}

\begin{prop}
	Soit $A$ et $B$ deux points distincts de l'espace affine euclidien $E_n$, il existe une unique réflexion $s$ de $E_n$ telle que $s(A) = B$.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		Si $s$ existe, sa base est l'hyperplan de $E_n$ passant par le milieu de $[A,B]$ et de direction $\mathrm{Vect}\{\overrightarrow{AB}\}^{\bot}$. Une telle application convient et $s(A) = I$ où $I$ est le milieu de $[A, B]$.
	\end{proof}
\end{footnotesize}

\begin{definition}
	Soit $A$ et $B$ deux points distincts de l'espace affine euclidien $E_n$, la base de l'unique réflexion échangeant $A$ et $B$ s'appelle hyperplan médiateur de $A$ et $B$.
\end{definition}

\begin{req}
	Dans le cas $n = 2$, l'hyperplan médiateur est appelé médiatrice du segment.
\end{req}

\begin{prop}
	Soit $A$ et $B$ deux points distincts de l'espace affine euclidien $E_n$, l'hyperplan médiateur $\mathcal{H}$ de $A$ et $B$ est l'ensemble des points de $E_n$ équidistants de $A$ et $B$: $\mathcal{H} = \{M, M \in E_n, MA = MB\}$.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		Décomposition des normes au carré, puis on montre que le produit scalaire $\overrightarrow{IM}. \overrightarrow{AB} = 0$.
	\end{proof}
\end{footnotesize}

\begin{prop}
	Soit $\mathcal{H}$ et $\mathcal{H}'$ deux hyperplans de $E_n$ de même direction $H$. Alors,
	\begin{enumerate}
		\item Il existe une translation de vecteur $\overrightarrow{u}$ appartenant à $H^{\bot}$ transformant $\mathcal{H}$ en $\mathcal{H}'$.
		\item Soit $s$ la réflexion de base $\mathcal{H}$ et soit $s'$ la réflexion de base $\mathcal{H}'$, alors $s' \circ s = t_{2\overrightarrow{u}}$ où $t_{2\overrightarrow{u}}$ est une translation de vecteur $2\overrightarrow{u}$.
		\item Toute translation de vecteur $2\overrightarrow{u}$ se décompose en produit de deux réflexion, l'une de ces réflexions étant arbitraire.
	\end{enumerate}
\end{prop}
\begin{footnotesize}
	\begin{proof}
		\begin{enumerate}
			\item On pose $\overrightarrow{u} = \overrightarrow{AA'}$.
			\item $M \in E_n$ la droite $D_M$ coupe $\mathcal{H}$ en $I$ et $\mathcal{H}'$ en $I'$. En notant $M'' = s(s(M))$ et $\overrightarrow{MM''} = 2\overrightarrow{II'} = 2 \overrightarrow{u}$.
			\item $H = \mathrm{Vect}\{\overrightarrow{u}\}^{\bot}$ et $\mathcal{H}' = t_{\overrightarrow{u}}(\mathcal{H})$, alors $s \circ s' = t_{2\overrightarrow{u}}$.
		\end{enumerate}
	\end{proof}
\end{footnotesize}


\paragraph{Groupe des isométrie sur un espace affine de dimension $n$} L'application $L$ de $GA(E_n)$ dans $GL\left(\overrightarrow{E_n}\right)$ qui à une application affine $f$ fait correspondre sa partie linéaire est un morphisme, surjectif de noyau $T\left(E_n\right)$, du groupe $\left(GA(E_n), \circ\right)$ dans le groupe $\left(GL\left(\overrightarrow{E_n}\right), \circ\right)$.

\begin{theo}
	Soit $Is(E_n)$ l'ensemble des isométries de l'espace affine euclidien $E_n$, alors
	\begin{enumerate}
		\item $(Is(E_n), \circ)$ est un groupe non commutatif, sous groupe de $\left(GA(E_n), \circ\right)$.
		\item L'application $L$ de $GA(E_n)$ dans $GL\left(\overrightarrow{E_n}\right)$ est un morphisme surjectif de noyau $T\left(E_n\right)$
	\end{enumerate}
\end{theo}
\begin{footnotesize}
	\begin{proof}
		\begin{enumerate}
			\item On utilise le morphisme $L$ est son application inverse \textblue{(on peut le montrer sans utiliser le morphisme)}.
			\item La surjectivité est ok, son noyau est calculer à l'aide d'une isométrie.
		\end{enumerate}
	\end{proof}
\end{footnotesize}

\begin{cor}
	$T(E_n)$ est un sous-groupe distingué du groupe $(Is(E_n), \circ)$ et le groupe $Is(E_n) \setminus T(E_n)$ et le groupe $O\left(\overrightarrow{E_n}\right)$ sont isomorphes.
\end{cor}
\begin{footnotesize}
	\begin{proof}
		Comme $T(E_n)$ est le noyau d'un morphisme, il est distingué dans $(Is(E_n), \circ)$. On applique le premier théorème d'isomorphisme des groupes.
	\end{proof}
\end{footnotesize}

\begin{theo}
	Soit $A$ un point fixe de $E_n$, alors toute isométrie $f$ de $E_n$ se décompose de manière unique, sous la forme $f = t \circ g$ où $g$ est une isométrie de $E_n$ vérifiant $g(A) = A$.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		Existence : $t = \overrightarrow{AA'}$ et $g = t^{-1} \circ f$ et $g(A) = A$.
		
		Unicité : raisonnement par l'absurde.
	\end{proof}
\end{footnotesize}

\begin{definition}
	$Is^+(E_n)$ l'ensemble des déplacements, ou isométrie positive : les isométries admettant pour partie linéaire une application orthogonale positive. $Is^-(E_n)$ l'ensemble des antidéplacements, ou isométries négatives : les isométries admettant pour partie linéaire une application orthogonale négative.
\end{definition}

\begin{theo}
	$Is^+(E_n)$ est un sous-groupe de $(Is(E_n), \circ)$ appelé groupe des déplacements.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		Par le morphisme $L$ appliqué au groupe $O^+(E_n)$.
	\end{proof}
\end{footnotesize}

\begin{theo}
	Soit $f$ une isométrie de $Is(E_n)$, alors il existe $p \leq n +1$ tel que
	\begin{enumerate}
		\item $f$ est la composé de $p$ réflexions
		\item si $f$ admet au moins un point invariant, alors $p \leq n$ et la variété linéaire affine des points invariants de $f$ est de dimension $n - p$.
	\end{enumerate}
\end{theo}

\begin{cor}
	L'ensemble des réflexions de $Is(E_n)$ est un système de générateur de $Is(E_n)$.
\end{cor}

\paragraph{Isométrie en dimension 1} Les applications orthogonales $\overrightarrow{E_1}$ sont $Id$ et $-Id$. On a alors $Is^+(E_1)$ qui est l'ensemble des translations et $Is^-(E_1)$ qui est l'ensemble des symétries centrales par rapport à un point de $E_1$.

\paragraph{Isométrie en dimension 2} Donnons quelques caractéristiques de nos isométries de dimension 2.

\begin{tabular}{|c|c|c|c|}
	\hline
	Isométrie $f$ & $\{M, f(M) = M\}$ & $Is(E_2)$ & Réflexions \\
	\hline
	Identité & $\dim = 2$ & $+$ & $0$ \\
	Réflexion & $\dim = 1$ & $-$ & $1$ \\
	Rotation & $\dim = 0$ & $+$ & $2$ \\
	Translation & $\emptyset$ & $+$ & $2$ \\
	Réflexion-Translation & $\emptyset$ & $-$ & $3$ \\
	\hline
\end{tabular}

\subparagraph{Utilisation des nombres complexes} On utilise le plan vectoriel euclidien orienté $E_2$ et le plan complexe euclidien $\C$, muni du produit scalaire $\varphi$ défini par $\varphi(z, z') = \Re(z\overline{z'})$ et orienté par la base $(1, i)$.

\begin{theo}
	Soit les plans euclidiens orientés $E_2$ et $\C$ et soit $\phi$ la bijection de $E_2$ dans $\C$, qui à $M$ associe son affixe $z$. Soit $T$ un élément du groupe symétrique $S(E_2)$ de $E_2$, et soit $f = \phi \circ T \circ \phi^{-1}$.
	\begin{enumerate}
		\item $f$ est une bijection de $\C$, c'est-à-dire un élément du groupe symétrique $S(\C)$.
		\item L'application $\psi$ de $S(E_2)$ dans $S(\C)$ définie par $\psi(T) = f$ est un isomorphisme du groupe $S(E_2)$ dans le groupe $S(\C)$.
	\end{enumerate}
\end{theo}
\begin{footnotesize}
	\begin{proof}
		\begin{enumerate}
			\item $f = \psi(T)$ \textblue{(Montre comment les nombres complexes aide à l'étude des transformations.)}
			\item $T = \phi^{-1} \circ T \circ \phi$: bijection entre les deux groupe. Par calcul on montre le morphisme.
		\end{enumerate}
	\end{proof}
\end{footnotesize}

\noindent\begin{minipage}{.66\textwidth}
	\begin{prop}
		Soit $f$ la bijection de $\C$ définie par $z' = f(z) = e^{i\alpha}z +a$.
		\begin{enumerate}
			\item si $e^{i\alpha} = 1$, la translation de vecteur $\overrightarrow{u}$ d'affixe $a$.
			\item sinon, la rotation de centre $A$ d'affixe $\frac{a}{1-e^{i\alpha}}$ et d'angle $\alpha$.
		\end{enumerate}
	\end{prop}
\end{minipage} \hfill
\begin{minipage}{.34\textwidth}
	\begin{footnotesize}
		\begin{proof}
			\begin{enumerate}
				\item Si $e^{i\alpha} = 1$ et $a \neq 0$ pas de point fixe pour $f$.
				\item Sinon, $A$ d'affixe $w$ est le point fixe de $f$.
			\end{enumerate}
		\end{proof}
	\end{footnotesize}
\end{minipage}

\begin{prop}
	Soit $T$ la réflexion de base une droite $D$ passant par un point $A$ et de direction $\overrightarrow{D}$ vérifiant $\left(\mathrm{Vect}\{\overrightarrow{i}\}, \overrightarrow{D}\right) = \frac{1}{2} \alpha \mod \pi$. On suppose que l'affixe $w$ de $A$ vérifie $w = e^{i\alpha}\overline{w} + a + ib$. Alors, $f = \psi(T)$ si et seulement si $f(z) = e^{i\alpha}\overline{z} + a + ib$
\end{prop}
\begin{footnotesize}
	\begin{proof}
		Résolution d'un système donnant les propriétés que l'on souhaite.
	\end{proof}
\end{footnotesize}

\begin{prop}
	Soit $T$ une isométrie négative de $E_2$ sans point invariant, alors $T$ est la composée commutative d'une réflexion de base une droite $D$ de direction $\overrightarrow{D}$ vérifiant $\left(\mathrm{Vect}\{\overrightarrow{i}\}, \overrightarrow{D}\right) = \frac{1}{2} \alpha \mod \pi$ et d'une translation de vecteur $\overrightarrow{w}$ non nul de $\overrightarrow{D}$. De plus, $f = \psi(T)$ si et seulement si $f(z) = e^{i\alpha}\overline{z} + a + ib$ avec $f$ admettant aucun point fixe.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		Par calcul direct.
	\end{proof}
\end{footnotesize}

\paragraph{Isométrie en dimension 3} Donnons quelques caractéristiques de nos isométries de dimension 2.

\begin{tabular}{|c|c|c|c|}
	\hline
	Isométrie $f$ & $\{M, f(M) = M\}$ & $Is(E_2)$ & Réflexions \\
	\hline
	Identité & $\dim = 3$ & $+$ & $0$ \\
	Réflexion & $\dim = 2$ & $-$ & $1$ \\
	Rotation & $\dim = 1$ & $+$ & $2$ \\
	Symétrie centrale & $\dim = 0$ & $-$ & $3$ \\
	Translation & $\emptyset$ & $+$ & $2$ \\
	Réflexion-Translation & $\emptyset$ & $-$ & $3$ \\
	\hline
\end{tabular}

\paragraph{Applications des isométries aux solides platoniciens \cite[p.228]{Caldero-Germoni-N2}}
Nous allons donner quelques outils afin de traduire les isométries dans le monde des permutations. \textblue{Ces isométries permettent de calculer la table de caractère des groupes concernés.}

\subparagraph{Cas du tétraèdre}

\begin{theo}
	Les groupes d'isométries d'un tétraèdre régulier $\Delta_4$ sont : $Is(\Delta_4) \simeq \mathfrak{S}_4$ et $Is^+(\Delta_4) \simeq \mathfrak{A}_4$.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		On fait agir $Is(\Delta_4)$ sur l'ensemble des sommets du tétraèdre: $Is(\Delta_4) \simeq \mathfrak{S}_4$. Comme $Is^2(\Delta_4)$ est d'indice $2$ dans $Is(\Delta_4)$: $Is^+(\Delta_4) \simeq \mathfrak{A}_4$.
	\end{proof}
\end{footnotesize}

\begin{tabular}{|c|c|l|l|}
	\hline
	Nombre & Ordre & Isométrie de $\Delta_4$ & Permutation de $\mathfrak{S}_4$ \\
	\hline
	1 & 1 & $Id$ & $Id$ \\
	8 & 3 & rotation d'axe sommet-centre de face opposée & $3$-cycle \\ 
	3 & 2 & rotation d'angle par milieu de $2$ arrêtes opposées & double transposition \\
	6 & 2 & symétrie par rapport au plan médiateur & transposition \\
	6 & 4 & symétrie $\circ$ rotation & $4$-cycle \\ 
	\hline 
\end{tabular}

\subparagraph{Cas du cube} On traite le cas du cube. Le cas de l'octaèdre est analogue lorsqu'on remplace \emph{sommet} par \emph{centre des faces} dans les isométries \textblue{(dualité des deux solide : l'un définit l'autre)}.

\begin{theo}
	Le groupe d'isométries d'un cube régulier $C_6$ est : $Is^+(C_6) \simeq \mathfrak{S}_4$.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		On fait agir $Is(\Delta_4)$ sur l'ensemble de ces grandes diagonales: $Is^+(C_6) \simeq \mathfrak{S}_4$. 
	\end{proof}
\end{footnotesize}

\noindent\begin{tabular}{|c|c|l|l|}
	\hline
	Nombre & Ordre & $Is^+(C_6)$ & Permutation de $\mathfrak{S}_4$ \\
	\hline
	1 & 1 & $Id$ & $Id$ \\
	8 & 3 & rotation d'axe sommet-sommet opposée & $3$-cycle \\ 
	3 & 2 & rotation d'angle $\pi$ d'axe le centre de deux faces opposées & double transposition \\
	6 & 2 & rotation d'angle $\pi$ d'axe le centre de deux arrêtes opposée & transposition \\
	6 & 4 & rotation d'angle $^+_-\frac{\pi}{2}$ d'axe le centre de deux faces opposées & $4$-cycle \\ 
	\hline 
\end{tabular}

\begin{theo}
	Le groupe d'isométries d'un octaèdre régulier est : $Is^+(O_8) \simeq \mathfrak{S}_4$.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		On admet que l'on peut inscrire un cube dans l'octaèdre ce qui nous donne le résultat. 
	\end{proof}
\end{footnotesize}

\subparagraph{Cas du dodécaèdre} Comme dans le cadre du cube et de l'octaèdre, le dodécaèdre et de l'icosaèdre sont duaux. On va alors étudier l'isomorphisme entre l'icosaèdre et $\mathfrak{A}_5$.  

\begin{theo}
	Le groupe d'isométries d'un dodécaèdre régulier $P_{12}$ est : $Is^+(P_{12}) \simeq \mathfrak{U}_5$.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		On admet que l'on peut inscrire cinq cubes distincts dans le dodécaèdre. On fait donc agir le groupe des isométries sur les cinq cubes. Cette action fixe le grandes diagonales du dodécaèdre: on conclut comme pour les autres. 
	\end{proof}
\end{footnotesize}

\begin{theo}
	Le groupe d'isométries d'un icosaèdre régulier $\Delta_{20}$ est : $Is^+(\Delta_{20}) \simeq \mathfrak{U}_5$.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		On peut inscrire un dodécaèdre dans un icosaèdre.
	\end{proof}
\end{footnotesize}

\noindent\begin{tabular}{|c|c|l|l|}
	\hline
	Nombre & Ordre & $Is^+(\Delta_20)$ & Permutation de $\mathfrak{A}_5$ \\
	\hline
	1 & 1 & $Id$ & $Id$ \\
	24 & 3 & rotation d'ordre $5$ d'axe sommet-sommet opposée & $5$-cycle \\ 
	20 & 2 & rotation d'ordre $3$ d'axe le centre de deux faces opposées & $3$-cycle \\
	15 & 2 & rotation d'angle $\pi$ d'axe le centre de deux arrêtes opposée & double transposition \\
	\hline 
\end{tabular}