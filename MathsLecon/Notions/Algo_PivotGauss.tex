%DEVs: Berlekamp
%LECONs : 106 (groupe lineaire); 108 (Parties génératrices); 151 (Dimension et rang); 152 (Déterminant); 162 (Systemes lineaires)
La méthode du pivot de Gauss permet d'obtenir des résultats sur des matrices en répétant plusieurs fois une opération simple (on voit apparaître le côté algorithmique) \cite[p.205]{Cognet}. Cette méthode permet de répondre à quelques questions ou problèmes sur les matrices comme:
\begin{enumerate}
	\item Un élément de $\mathcal{M}_n(K)$ est-il dans $GL_n(K)$? \textblue{(Trouver une matrice triangulaire équivalente à la matrice de départ et vérifier la présence de coefficients nuls sur la diagonale.)}
	\item Quel est le rang d'une matrice de $\mathcal{M}_n(K)$? \textblue{(Trouver une matrice diagonale équivalente à la matrice de départ dont on compte le nombre de coefficients non nuls.)}
	\item Calcul d'une matrice inversible de $GL_n(K)$. \textblue{(Appliquer les transformations pour trouver l'identité de la matrice de départ à l'identité.)}
	\item Résoudre un système linéaire. \textblue{(Mettre le système sous forme triangulaire.)}
	\item Trouver les générateurs de $GL_n(K)$.
\end{enumerate}

\paragraph{Définitions et notations} Soit $M \in \mathcal{M}_n(K)$, on note $L_i$ sa $i^{\text{ième}}$ ligne et $C_j$ sa $j^{\text{ième}}$ colonne. On note $E_{i,j}(n)$ la matrice élémentaire dont le seul coefficient non nul, $1$, est à la $i^{\text{ième}}$ ligne et $j^{\text{ième}}$ colonne.

\begin{definition}
	On appelle matrice de transvection de $\mathcal{M}_n(K)$ toute matrice de la forme $I_n + \lambda E_{i, j}(n)$ où $\lambda \in K$ et $i \neq j$. On la notera $T_{i, j}(\lambda, n)$.
\end{definition}

\begin{req}
	La matrice $T_{i, j}(\lambda, n)$ est inversible d'inverse $T_{i, j}(-\lambda, n)$.
\end{req}

\begin{definition}
	On appelle matrice de dilation de $\mathcal{M}_n(K)$ toute matrice de la forme $I_n + (\lambda -1)E_{j, j}(n)$ où $\lambda \in K^{\times}$. On la notera $D_{j}(\lambda, n)$.
\end{definition}

\begin{req}
	La matrice $D_{j}(\lambda, n)$ est inversible d'inverse $D_{j}(\frac{1}{\lambda}, n)$.
\end{req}

\begin{definition}
	On appelle matrice de permutation de $\mathcal{M}_n(K)$ toute matrice $P =(p_{i, j})_{1 \leq i, j \leq n}$ tel qu'il existe un élément $\sigma \in \Sn_n$ tel que $\forall 1 \leq i, j \leq n$, $p_{i,j} = \delta_{i, \sigma(j)}$ où $\delta_{i, j}$ est nul si $i \neq j$ et vaut $1$ sinon.
\end{definition}

\begin{req}
	La matrice $P(\sigma, n)$ est inversible d'inverse $P(\sigma^{-1}, n) = ^tP(\sigma, n)$.
\end{req}


\paragraph{La méthode de Gauss pour les coefficients dans un corps} Soit $M \in \mathcal{M}_{p, n}(K)$. Commençons par donner une intuition de l'action de ces matrices sur une matrice par la multiplication à gauche ou à droite.
\begin{itemize}
	\item Multiplier à gauche (respectivement à droite) la matrice $M$ par $T_{i, i'}(\lambda, p)$ (respectivement par $T_{j, j}(\lambda, n)$) revient à ajouter à la ligne $L_i(M)$ (respectivement à la colonne $C_{j'}(M)$) $\lambda$ fois la ligne $L_{i'}(M)$ (respectivement à la colonne $C_{j}(M)$).
	\item Multiplier à gauche (respectivement à droite) la matrice $M$ par $D_i(\mu, p)$ (respectivement par $D_j(\mu, n)$) revient à multiplier la ligne $L_i(M)$ (respectivement à la colonne $C_{j'}(M)$) par $\mu$.
	\item Multiplier à gauche (respectivement à droite) la matrice $M$ par $P(\sigma, p)$ (respectivement par $P(\sigma, n)$) revient à échanger les lignes (respectivement les colonnes) des $M$.
\end{itemize}

Comme on peut toujours se ramener à une matrice carrée en rajoutant des lignes (ou des colonnes) qui manquent, on considère $M \in \mathcal{M}_n(K)$. Énonçons le principe de cet algorithme.
\begin{enumerate}
	\item Mettre $M$ sous forme triangulaire supérieure: on utilise les permutations et le transvections des lignes. Cette étape consiste à nettoyer tous les coefficients inférieurs au coefficient diagonal (la permutation permet d'obtenir le triangle).
	\item Mettre une matrice triangulaire supérieure sous forme diagonale: permutation et transvections des colonnes. Cette étape nous permet alors de nettoyer les coefficients à droite des coefficients diagonaux. 
	\item Obtenir l'identité d'une matrice diagonale (si c'est possible: $M \in GL_n(K)$ : dilatation des coefficients.
\end{enumerate}
L'algorithme~\ref{algo:PivotGauss} met en place cet algorithme: il cherche les forme échelonnée réduite de matrice. Pour chacun des coefficients, il traite les trois étapes en même temps. Sa complexité est en $O(n^3)$.

\begin{prop}
	\begin{enumerate}
		\item Le groupe $SL_n(K)$ est engendré par les matrices de transvections. Le groupe $GL_n(K)$ est engendré par les transvections et les dilatations.
		\item Si $A \in GL_n(K)$, il existe un unique élément $M \in SL_n(K)$ et un unique élément $\mu \in K^{\times}$ tel que $A = MD_n(\mu)$. L'élément $\mu$ est le déterminant de la matrice $A$.
	\end{enumerate}
\end{prop}
\begin{footnotesize}
	\begin{proof}
		\begin{enumerate}
			\item D'après la décomposition obtenu par l'algorithme de Gauss, si $\det A = 1$, alors la matrice obtenue sans les dilatations est déjà $I_n$. Comme on l'obtient uniquement en multipliant des matrices de transvection ou leur inverse (qui est aussi une matrice de transvection), on en déduit le système de générateurs.
			
			La troisième étape de la décomposition consiste à rajouter des dilatations: d'où le résultat.
			\item Reformulation classique de la méthode de Gauss dans la troisième étape.
		\end{enumerate}
	\end{proof}
\end{footnotesize}

\begin{algorithm}
	\begin{algorithmic}[1]
		\Function \textsc{Gauss-Jordan}
		\State $r \gets 0$ \Comment ($r$ est l'indice de ligne du dernier pivot trouvé)
		\For{$j = 1$ à $m$} \Comment ($j$ décrit tous les indices de colonnes)
		\State Rechercher $k = max\left(|A[i,j]|, r+1 \leq i \leq n\right)$ \Comment ($A[k,j]$ est le pivot)
		\If{$A[k,j] \neq 0$} \Comment ($A[k,j]$
		désigne la valeur de la ligne $k$ et de la colonne $j$)
		\State $r \gets r+1$ \Comment ($r$ désigne l'indice de la future ligne servant de pivot)
		\State Diviser la ligne $k$ par $A[k,j]$ \Comment (On normalise la ligne de pivot de façon que le pivot prenne la valeur $1$)
		\State Échanger les lignes $k$ et $r$ \Comment (On place la ligne du pivot en position $r$)
		\For{$i = 1$ à $n$} \Comment (On simplifie les autres lignes)
		\If{$i \neq r$}
		\State Soustraire à la ligne $i$ la ligne $r$ multipliée par $A[i,j]$ \Comment (de façon à annuler $A[i,j]$)
		\EndIf
		\EndFor
		\EndIf
		\EndFor
		\EndFunction			
	\end{algorithmic}
	\caption{Algorithme de Gauss--Jordan (ou pivot de Gauss).}
	\label{algo:PivotGauss}
\end{algorithm}

\paragraph{Pivot de Gauss pour les matrices à coefficients dans $\Z$} Le pivot de Gauss peut s'étendre aux matrices de $\mathcal{M}_{m,n}(\Z)$ et nous permet de connaître les générateurs de $GL_n(\Z)$ (qui est l'ensemble des matrices de coefficients dans $\Z$ dont le déterminant vaut $-1$ ou $1$) et de classifier (à équivalence près) les matrices de $\mathcal{M}_{m,n}(\Z)$. Comme nous ne sommes plus dans un corps, il nous faut faire attention à la division...

Nous allons considérés les même matrices élémentaires que pour un corps, mais elles seront définies de manière à rester dans $GL_n(\Z)$. Ainsi, on utilisera $T_{i, j}(\epsilon,n)$ avec $\epsilon \in \{1, -1\}$ (la multiplication à gauche d'une telle matrice revient à ajouter ou retrancher une ligne $i$ à la ligne $j$). De même, on considère les matrices de permutation qui appartiennent à $GL_n(\Z)$ qui permettent d'échanger deux lignes ou deux colonnes. On considère enfin les matrices de dilatation $D_i(-1,n)$ telle que multiplier à gauche par cette matrice revient à prendre l'opposée de la ligne $i$.

On peut alors définir une action de $GL_n(\Z)$ sur $\mathcal{M}_{n, 1}(\Z)$ en associant à tout couple $(P, X)$ le vecteur colonne $PX$. La méthode de Gauss permet de paramétrer les orbites de cette action par le pgcd des coefficients constituant la colonne de $X$, on peut alors en trouver un représentant.

\begin{prop}
	Soit $X$ un élément de $\mathcal{M}_{n, 1}(\Z)$.
	\begin{enumerate}
		\item L'élément $X'$ de $\mathcal{M}_{n, 1}(\Z)$ appartient à l'orbite $\omega_X$ si et seulement si le pgcd des coefficients de $X'$ est égal au pgcd des coefficients de $X$.
		\item Si $a_X$ est le pgcd des coefficients de $X$, $\omega_X = \omega_{C_{a_X}}$. La famille $(C_a)_{a \in \N}$ est un système de représentant de la relation d'équivalence induite par l'action.
	\end{enumerate}
\end{prop}
\begin{footnotesize}
	\begin{proof}
		\begin{enumerate}
			\item Si $X = 0$ , ok. Supposons $X \neq 0$, donc $X' \neq 0$. Si $X' \in \omega_X$, il existe $P \in GL_n(\Z)$ tel que $X' = PX$ et en écrivant les coefficients de $X'$ en fonction de $P$ et de $X$, on obtient le résultat. Réciproquement, il faut montrer que pour toute colonne $X'$, il existe $P \in GL_n(\Z)$ tel que $PX'$ est la colonne $C_{a_X'}$ (se montre par récurrence).
			\item Application de ce qui précède.
		\end{enumerate}
	\end{proof}
\end{footnotesize}

\begin{cor}
	Le groupe $GL_n{\Z}$ est engendré par les matrices élémentaire ($T_{i, j}(\epsilon, n)$, $D_i(-1,n)$ et $P(\sigma, n)$ où $\epsilon \in \{-1, 1\}$).
\end{cor}
\begin{footnotesize}
	\begin{proof}
		On raisonne par récurrence.
	\end{proof}
\end{footnotesize}

