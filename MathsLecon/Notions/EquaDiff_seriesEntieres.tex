%LECON : 243

Les propriétés de la régularité des fonctions séries entière (elles sont de classe $\mathcal{C}^{\infty}$) en font de très bonne candidates pour être solution d'une équation différentielle. En supposant que la solution est développable en série entière, nous allons pouvoir identifier la fonction solution grâce à l'unicité des coefficient. Nous allons en donner une méthode générale ainsi qu'un cas où on sait que la solution peut s'exprimer comme une série entière. 

Nous rappelons que si une fonction est fonction série entière, on dit également qu'elle est développable en série entière. En effet, elle est assez régulière pour faire une série de terme général une fonction de ses dérivées successives. De plus, cette séries coïncide avec la fonction sur un voisinage autours du point qu'on a développé. \textred{\textbf{Attention}}, toutes les fonctions ne sont pas dérivable en série entière, même si elles sont ultra régulière (prendre par exemple la fonction $x \mapsto \exp \frac{-1}{x^2}$ \cite[p.57]{ArnaudiesFraysse-3}).

Nous donnons une méthode générale permettant de résoudre les équations différentielles. Je n'ai toujours pas trouvé une référence la mentionnant aussi explicitement mais quelques unes en parle et il faut l'extraire... C'est une méthode d'analyse-synthèse.
\begin{description}
	\item[Analyse] \begin{enumerate}
		\item Supposer qu'il existe une solution $S(x) = \sum a_nx^n$ développable en série entière;
		\item Introduire cette solution dans l'équation, en dérivant terme à terme pour exprimer $S'(x)$ et ses dérivées successives;
		\item Par des changements d'indice, on se ramène à une écriture du type $\sum b_nx_n=0$, où la suite $(b_n)$ s'écrit en fonction de la suite $(a_n)$;
		\item Par unicité des coefficients d'une série entière, on sait que $b_n=0$;
		\item Trouver la suite $(a_n)$ en fonction éventuellement de certains paramètres;
	\end{enumerate}
	\item[Synthèse] Réciproquement, on vérifie que la série entière $\sum a_nx^n$ a un rayon de convergence non nul et qu'elle est solution de l'équation différentielle. 
\end{description}

Nous allons maintenant donner un cas où cette méthode s'applique à coup sûr \cite[p.409]{ZuilyQueffelec2}. 
\begin{prop}
	Soient $I$ un intervalle de $\R$ et $p$ et $q$ deux fonctions continue sur $I$. On s'intéresse alors à l'équation différentielle $y''+py'+qy = 0$ que l'on souhaite résoudre. On suppose que $p(x) = \sum p_nx^n$ et $p(x) = \sum q_nx^n$, les séries convergeant pour $\left|x\right| < R$. Alors pour tout $(a_0, a_1) \in \R^2$, l'équation différentielle a une solution unique $y$ telle que $y(0) = a_0$ et $y'(0) = a_1$, $y$ étant développable en série entière convergente sur $\left]-R, R\right[$.
\end{prop}

\begin{req}
	On remarque le caractère global de cette proposition.
\end{req}

{\small{\begin{proof}
	Nous allons appliquer la méthode que nous avons donnée précédemment. Supposons que le problème est résolu. Soit $y(x) = \sum_{n=0}^{\infty} a_nx^n$ ($a_0$ et $a_1$ sont imposés), la série convergeant pour $\left|x\right| < R$. Alors
	\begin{displaymath}
	\left \{\begin{array}{l}
	y'(x) = \sum_{n=0}^{\infty} (n+1)a_{n+1}x^n \\
	y''(x) = \sum_{n=0}^{\infty} (n+2)(n+1)a_{n+2}x^n \\
	p(x)y'(x) = \sum_{n=0}^{\infty} \left(\sum_{j=0}^{n}(n - j +1)a_{n-j+1}p_j\right)x^n \\
	q(x)y(x) = \sum_{n=0}^{\infty} \left(\sum_{j=0}^{n}a_{n-j}q_j\right)x^n \\
	\end{array}
	\right.
	\end{displaymath} 
	$y$ sera solution de l'équation différentielle si et seulement si tous les coefficients de la série entière associée à $y'' + py' + qy$ sont nuls, c'est-à-dire si et seulement si (\textblue{par unicité des coefficients d'une fonction série entière et donc du développement en série entière}) pour tout $n \geq 0$ 
	\begin{displaymath}
	(n+2)(n+1)a_{n+2} = - \sum_{j=0}^{n} (n-j+1)a_{n-j+1}p_j - \sum_{j=0}^{\infty} a_{n-j}q_j
	\end{displaymath}
	Cette relation montre que la donnée de $a_0$ et $a_1$ impose les valeurs des coefficients de la suite $(a_n)$.
	
	Il ne nous reste plus qu'à montrer que la série formelle $\sum_{n=0}^{\infty} a_nx^n$ ainsi déterminée a un rayon de convergence $\geq R$, les calculs seront donc justifiés et la proposition sera prouvée. Posons, quand $0< r < R$ les séries 
	\begin{displaymath}
	\begin{array}{ccc}
	P_r = \sum_{j=0}^{\infty} \left|p_j\right|r^j & ; & Q_r = \sum_{j=0}^{\infty} \left|q_j\right|r^j \\
	\end{array}
	\end{displaymath}
	Cherchons un $M_r > 0$ tel que l'inégalité suivante puisse se prouver par récurrence
	\begin{displaymath}
	\begin{array}{cc}
	\left|a_n\right| \leq M_rr^{-n}, & \forall n \geq 0 \\
	\end{array}
	\end{displaymath}
	Supposons que cette relation de récurrence est vérifiée. Alors par les remarques précédentes on a:
	\begin{displaymath}
	\begin{array}{ccll}
	(n+2)(n+1)\left|a_{n+2}\right| & \leq & \sum_{j=0}^{n} (n+1-j)\frac{M_r}{r^{n+1-j}}\left|p_j\right| +\sum_{j=0}^{n} \frac{M_r}{r^{n-j}}\left|q_j\right| & \text{\textblue{solution + inégalité}} \\
	& = & \frac{(n+1)M_r}{r^{n+1}}\sum_{j=0}^{n} \left|p_j\right|r^j - \underbrace{\frac{M_r}{r^{n+1}}\sum_{j=0}^{n} j\left|p_j\right|r^j}_{\geq 0} + \frac{M_r}{r^{n}}\sum_{j=0}^{n} \left|q_j\right|r^j & \textblue{\text{introduction } r^j} \\
	& \leq & \frac{(n+1)M_r}{r^{n+1}}\sum_{j=0}^{n} \left|p_j\right|r^j + \frac{M_r}{r^{n}}\sum_{j=0}^{n} \left|q_j\right|r^j & \textblue{\text{soustraire terme } \geq 0} \\
	& \leq & \frac{(n+1)M_r}{r^{n+1}}P_r + \frac{M_r}{r^{n}}Q_r & \textblue{\text{definition}} \\
	& \leq & \frac{(n+1)M_r}{r^{n+2}}\left(rP_r + r^2Q_r\right) & \\
	\end{array}
	\end{displaymath}
	On en déduit que $\left|a_{n+2}\right| \leq \frac{(n+1)M_r}{r^{n+2}}\left(\frac{rP_r + r^2Q_r}{n+2}\right) \leq \frac{(n+1)M_r}{r^{n+2}}$ si $\frac{rP_r + r^2Q_r}{n+2} \leq 1$. La relation de récurrence nous permet de montrer que la série  $\sum_{n=0}^{\infty} a_nx^n$ a bien un rayon de convergence $\geq R$.
\end{proof}}}

\begin{req}
	ce résultat reste vrai si on change la corps de base : $(a_0, a_1) \in K^2$ où $K = \C$ ou $\R$. De plus, il reste vrai pour une équation différentielle de degrés quelconque (si toutes les fonctions sont développable en séries entières) et si l'équation n'est pas homogène et que le reste est développable en série entière lui aussi. 
\end{req}

\begin{appli}[Utilisation des séries entière pour résoudre une équation différentielle \protect{\cite[p.528]{Kieffer}}]
	Considérons l'équation différentielle $(E_0)$ sur $\R$ définie telle que : $x''-tx =0$. La proposition précédent nous assure que nous pouvons appliquer la méthode de résolution avec série entière sur cette équation.
	
	Supposons qu'il existe $r \in \R_+^*$ tel que $(E_0)$ admette une solution $x$ développable en série entière de rayon $R \geq r$ : $x(t) = \sum_{n=0}^{\infty}a_nt^n$. Alors $x$ est $\mathcal{C}^{\infty}\left(]-r, r[\right)$, et pour tout $t\in ]-r, r[$, on a:
	\begin{displaymath}
	\left\{ \begin{array}{c @{ = } l}
	x'(t) & \sum_{n=1}^{\infty}na_nt^{n-1} \\	
	x''(t) & \sum_{n=2}^{\infty}n(n-1)a_nt^{n-2} \\
	\end{array}
	\right.
	\end{displaymath}
	D'où, pour tout $t \in ]-r, r[$:
	\begin{displaymath}
	\begin{array}{ccl}
	x''-tx & = & \sum_{n=2}^{\infty}n(n-1)a_nt^{n-2} + \sum_{n=0}^{\infty}a_nt^{n+1} \\
	& = & 2a_2 + \sum_{n=1}^{\infty}\left((n+2)(n+1)a_{n+2} - a_{n-1}\right)t^{n} \\
	\end{array}
	\end{displaymath}
	On en déduit que $x$ est solution de $(E_0)$ sur $]-r, r[$ si et seulement si:
	\begin{displaymath}
	\left\{
	\begin{array}{c@{=}cc}
	a_2 & 0 & \\
	a_{n+2} & \frac{a_{n-1}}{(n+2)(n+1)} & \forall n \in \N^* \\
	\end{array}
	\right.
	\end{displaymath}
	
	Soient $(a_0, a_1) \in \R^2$, $a_2 = 0$ et $\left(a_n\right)_{n\geq 3}$ est définie par la relation : $\forall n \geq 3$, $a_n = \frac{a_{n-3}}{n(n-1)}$. En particulier, pour tout $p \in \N$, $a_{3p+2} = 0$. D'après la règle de d'Alembert, pour tout $t \in \R$, les séries $\sum_{p\geq 0} a_{3p}t^{3p}$ et $\sum_{p\geq 0} a_{3p+1}t^{3p+1}$ convergent. Il s'ensuit que la série entière $\sum_{n \geq 0} a_nt^n$ est de rayon infini. D'après l'analyse, la fonction $x$ associée à la série entière $\sum_{n \geq 0} a_nt^n$ est solution de $(E_0)$. Prenons $a_0 = 1$ et $a_1 = 0$, on obtient:
	\begin{displaymath}
	\begin{array}{cc}
	\forall t \in \R, & x_1(t) = 1 + \sum_{p=1}^{\infty} \frac{t^{3p}}{(2 \times 3)(5 \times 6) \dots ((3p-1)3p)}
	\end{array}
	\end{displaymath}
	Prenons $a_0 = 0$ et $a_1 = 1$, on obtient:
	\begin{displaymath}
	\begin{array}{cc}
	\forall t \in \R, & x_2(t) = 1 + \sum_{p=1}^{\infty} \frac{t^{3p}}{(3 \times 4)(6 \times 7) \dots (3p(3p+1))}
	\end{array}
	\end{displaymath}
	D'après l'étude précédente, $x_1$ et $x_2$ sont solutions de $(E_0)$ et forment une base de l'espace des solutions de $(E_0)$. 
\end{appli}

\begin{appli}[Équation de Bessel \protect{\cite[p.101]{FrancinouGianellaNicolas-an4}}]
	On montre que 
	\begin{displaymath}
	\frac{1}{\pi} \int_{0}^{\pi} \cos\left(x\sin \theta\right) d\theta = \sum_{n= 0}^{\infty} \frac{(-1)^n}{4^n(n!)^2}x^{2n}
	\end{displaymath}
	en utilisant une équation différentielle. On montre que $\frac{1}{\pi} \int_{0}^{\pi} \cos\left(x\sin \theta\right) d\theta$ et $\sum_{n= 0}^{\infty} \frac{(-1)^n}{4^n(n!)^2}x^{2n}$ sont solutions de la même équation différentielle: $xy''+y'+xy =0$ en vérifiant que $\frac{1}{\pi} \int_{0}^{\pi} \cos\left(x\sin \theta\right) d\theta$ est bien une solution et en résolvant $xy''+y'+xy =0$ à l'aide de séries entières.
\end{appli}

