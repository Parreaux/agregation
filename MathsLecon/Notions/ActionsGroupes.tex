%DEVs: isometrie du cube; reciprocite quadratique; matrice diagonalisable sur Fq; Generateur de SL2(Z)
%LECONS: 101 (action de groupe); 104 (groupe fini); 105 (groupe symetrique); 106 (groupe lineaire); 182 (Groupe et geometrie); 190 (denombrement)
Les actions de groupes \cite[p.195]{Calais} sont des notions importantes car elles permettent d'agir sur un ensemble. Elles ont plusieurs applications notamment en géométrie (c'est la base de la géométrie) ou en dénombrement si le groupe est fini (via les formules sur les cardinaux).

\begin{req}
	Il y a deux monde: celui des groupes et des objets sur lesquels ils agissent. La translation dans le monde des objets (par application de l'action) se traduit par la conjugaison dans les groupes.
\end{req}

\begin{definition}[Action de groupes]
	Une action de groupe d'un groupe $G$ sur un ensemble $X$ est une application $f: G \times X \to X$ telle que $\forall x \in X, f(e,x)=x$ et $\forall g_1, g_2 \in G, \forall x \in X, f(g_1.g_2, x) = f(g_1, f(g_2, x))$.
		
	Une action de groupe de $G$ sur $X$ peut être également la donné d'un morphisme de $G$ dans $\mathfrak{S}_{\left|X\right|}$.
\end{definition}

\begin{definition}[Action fidèle \protect{\cite[p.174]{Berhuy}}]
	Soit $G$ un groupe agissant sur $E$. On dit que $G$ agit fidèlement sur $E$ si pour tout $g \in G$, $gx = x$ pour tout $x \in E$ implique $g= 1_G$ \textblue{(le neutre)}. Autrement dit si le morphisme de l'action est injectif.
\end{definition}

\begin{ex}[Actions de groupes]
	Donnons quelques exemples classiques d'actions de groupes.
	\begin{itemize}
		\item $G$ opère sur $G$ par translation à gauche.
		\item $G$ opère sur $\mathcal{P}(G)$ par translation à gauche.
		\item $G$ opère sur $G$ par conjugaison.
		\item $G$ opère sur $\mathcal{P}(G)$ par conjugaison.
	\end{itemize}
\end{ex}

\begin{definition}[Stabilisateur]
	On définit le stabilisateur de l'action, pour tout élément $x \in X$, comme $\mathrm{Stab}(x) = \{g \in G, f(g, x) = x\}$.
\end{definition}

\begin{req}
	Le stabilisateur d'un élément $x$ de $X$ est vu comme l'ensemble des éléments (de $G$) qui stabilise, "laisse fixe" $x$ lors de l'application de $f$. On vérifie facilement que $\mathrm{Stab}(x)$ est un sous-groupe de $G$.
\end{req}

\begin{definition}[Action libre \protect{\cite[p.16]{Caldero-Germoni}}]
	Une action de $G$ sur $X$ est libre si tous les stabilisateurs des points de $X$ sont triviaux.
\end{definition}

\begin{prop}[\protect{\cite[p.15]{Caldero-Germoni}}]
	Soit $G$ un groupe opérant sur un ensemble $X$ et $x \in X$. Alors, $\mathrm{Stab}(g.x) = g\mathrm{Stab}(x)g^{-1}$. 
\end{prop}
\begin{footnotesize}
	\begin{proof}
		$\left(g\mathrm{Stab}(x)g^{-1}\right) . (g . x) \textblue{\text{(Action du groupe } \left(g\mathrm{Stab}(x)g^{-1}\right) \text{ sur l'image)}} = \left(g\mathrm{Stab}(x)\right) . x \textblue{\text{ (calcul des actions)}} = g . x \textblue{\text{ (stabilisateur)}}$. On en déduit l'inclusion $\subseteq$. En remplaçant $x$ par $g.x$ et $g$ par $g^{-1}$, on obtient par le même procédé, l'inclusion inverse.
	\end{proof}
\end{footnotesize}

\begin{prop}[\protect{\cite[p.31]{Ulmer}}]
	Soit $\varphi : G \to \mathfrak{S}(X)$ une action de $G$ sur l'ensemble $X$. Alors, $\ker(\varphi) = \bigcap_{x \in X} \mathrm{Stab}(x)$.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		Si $g \in \ker(\varphi)$, $g.x = \varphi(g)(x) = e.x = x$ et $g \in \bigcap_{x \in X} \mathrm{Stab}(x)$. Inversement, $g.x = \varphi(g)(x) = x$ et $g \in \ker(\varphi)$.
	\end{proof}
\end{footnotesize}

\begin{req}
	On justifie ainsi la définition d'action fidèle et notamment l'injectivité du morphisme.
\end{req}

\begin{prop}[\protect{\cite[p.42]{Ulmer}}]
	Si $m \leq n$, alors si $\Sn_n$ agit sur $\Sn_m$ alors $\Sn_m \simeq \bigcap_{p \in \{m+1, \dots, n\}} \mathrm{Stab}(p)$.
\end{prop}

\noindent\begin{minipage}{.5\textwidth}
	\begin{lemme}[\protect{\cite[p.172]{Berhuy}}]
		Soit $G$ opérant sur un ensemble $E$. La relation sur $E$ définie par $x \sim y$ s'il existe $g \in G$ tel que $y = g.x$ est une relation d'équivalence.
	\end{lemme}
\end{minipage}\hfill
\begin{minipage}{.4\textwidth}
	\begin{footnotesize}
		\begin{proof}
			\begin{description}
				\item[Reflexive] On prend $g = 1_G$
				\item[Symétrique] On multiplie par $g^{-1}$
				\item[Transitive] On utilise les propriétés de calcul
			\end{description}
		\end{proof}
	\end{footnotesize}
\end{minipage}

\begin{definition}[Orbite]
	L'orbite de l'action, pour tout élément $x \in X$, comme: $\mathrm{Orb}(x) = \{y \in X | \exists g \in G, f(g, x) = y\}$.
\end{definition}

\begin{req}
	Une orbite est une classe d'équivalence sur la relation d'équivalence définie précédemment. Une orbite d'un élément $x$ de $X$ comme l'ensemble des éléments (de $X$) atteignable à partir de $x$ en appliquant $f$ à un $g$.
\end{req} 

\begin{ex}[Exemples sur ces notions]
	Quelques exemples sur les notions d'orbites et de stabilisateurs.
	\begin{itemize}
		\item $G$ opère sur $G$ par translation à gauche.
		\begin{itemize}
			\item $\mathrm{Stab}(x) = \{e\}$
			\item $\mathrm{Orb}(x) = G$ \textblue{(ici $X =G$)}
		\end{itemize}
	\end{itemize}
\end{ex}

\begin{definition}[Action transitive \protect{\cite[p.172]{Berhuy}}]
	On dit que $G$ agit transitivement sur $E$ si pour tous $x, x' \in E$, il existe $g \in G$ tel que $x' = gx$.
\end{definition}

\noindent\begin{minipage}{.46\textwidth}
	\begin{lemme}[\protect{\cite[p.172]{Berhuy}}]
		Soit $G$ agissant sur $E$. Alors les propriétés suivantes sont équivalentes:
		\begin{enumerate}
			\item $G$ agit transitivement sur $E$
			\item $\forall x \in E$, $\mathrm{Orb}(x) = E$
			\item $\exists x_0 \in E$ tel que $\mathrm{Orb}(x_0) = E$
			\item $E$ n'admet qu'une seule orbite : $E$
		\end{enumerate}
	\end{lemme}
\end{minipage}\hfill
\noindent\begin{minipage}{.46\textwidth}
	\begin{footnotesize}
		\begin{proof}
			\begin{description}
				\item[Montrons $1 \Rightarrow 2$] $\mathrm{Orb}(x) \subseteq E$: définition. $\mathrm{Orb}(x) \supseteq E$: si $x' \in E$, $\exists g$ tel que $x' = g.x$, alors $x' \in \mathrm{Orb}(x)$.
				\item[Montrons $2 \Rightarrow 3$] Évident
				\item[Montrons $3 \Rightarrow 4$] Par la relation d'équivalence : les classes forment une partition.
				\item[Montrons $4 \Rightarrow 1$] Soient $x, x' \in E$, alors $x, x'$ sont dans la même orbite. Donc, il existe $g$ tel que $x' = gx$.
			\end{description}
		\end{proof}
	\end{footnotesize}
\end{minipage}


\begin{definition}[Points fixes]
	Nous définition les points fixes d'un élément $g$ de $G$ comme l'ensemble des éléments (de $X$) qui sont stabilisés par $g$. De manière plus formelle on définit l'orbite comme $\mathrm{Fix}(g) = \{x \in X, f(g, x) = x\}$.
\end{definition}

\noindent\begin{minipage}{.46\textwidth}
	\begin{lemme}[\protect{\cite[p.172]{Berhuy}}]
		Soit $G$ agissant sur $E$. Alors les propriétés suivantes sont équivalentes:
		\begin{enumerate}
			\item $x$ est un point fixe sous l'action de $G$
			\item $\mathrm{Orb}(x) = \{x\}$
			\item $|\mathrm{Orb}(x)| = 1$
			\item $\mathrm{Stab}(x) = G$
		\end{enumerate}
	\end{lemme}
\end{minipage}\hfill
\noindent\begin{minipage}{.46\textwidth}
	\begin{footnotesize}
		\begin{proof}
			\begin{description}
				\item[Montrons $1 \Rightarrow 2$] Définition orbite et point fixe.
				\item[Montrons $2 \Rightarrow 3$] Évident
				\item[Montrons $3 \Rightarrow 4$] $\mathrm{Stab}(x) \subseteq G$ : ok; $\mathrm{Stab}(x) \supseteq G$: comme $\left|\mathrm{Orb}(x)\right| = 1$, $\forall g$, $g . x = x$. Donc $\forall g$, $g \in \mathrm{Stab}(x)$.
				\item[Montrons $4 \Rightarrow 1$] Définition orbite et point fixe.
			\end{description}
		\end{proof}
	\end{footnotesize}
\end{minipage}
	
\begin{lemme}
	Soit $G$ un groupe et $X$ un ensemble sur lequel $G$ agit. Pour tout élément $x \in X$, on a la relation suivante: $\left|\mathrm{Stab}(x)\right|\left|\mathrm{Orb}(x)\right| = \left|G\right|$
	\label{lem:cardG}
\end{lemme}
{\footnotesize{\begin{proof}
	On prouve cette relation à l'aide d'une bijection entre $\mathrm{Orb}$ et $G/\mathrm{Stab}$.
		
	Soit $x \in X$. On pose 
	\begin{displaymath}
		\begin{array}{ccll}
			\varphi_{x} : & G/\mathrm{Stab} & \to & \mathrm{Orb} \\
			& g & \mapsto & f(g, x)
		\end{array}
	\end{displaymath}
	Cette application est bien définie car $\mathrm{Stab}$ est un sous-groupe de $G$ donc le quotient est bien un groupe. \textred{(Attention: ce n'est pas un morphisme car $\mathrm{Orb}$ est un sous-ensemble de $X$.)} De plus, elle est surjective par définition de l'orbite. 
		
	Elle est injective. En effet, si $g_1, g_2 \in G$ tels que $f(g_1, x) = f(g_2, x)$. On a alors, en composant par $g_1^{-1}$, $f(g_1^{-1}, f(g_1, x)) = f(g_1^{-1},f(g_2, x))$. Par les propriétés sur $f$, on a $f(g_1^{-1}g_1, x) = f(g_1^{-1}g_2, x)$. Or, comme $f(e, x) = x$, on a $f(g_1^{-1}g_2, x) = f(g_1^{-1}g_1, x) = f(e, x) = x$.
		
	On a donc bien une bijection entre ces deux ensembles finis et on conclut en passant aux cardinaux.
\end{proof}}}
	
\begin{lemme}
	Soit $G$ un groupe et $X$ un ensemble sur lequel $G$ agit. On a la relation suivante $\sum_{g \in G} \left|\mathrm{Fix}(g)\right| = \sum_{x \in X} \left|\mathrm{Stab}(x)\right| $
	\label{lem:FixStab}
\end{lemme}
{\footnotesize{\begin{proof}
	On pose un ensemble $S =\{(g, x) \in G \times X | f(g, x) = x\}$. On pose ensuite l'application $S$ définie telle que:
	\begin{displaymath}
		\begin{array}{ccll}
			S: & G \times X & \to & \{0, 1\} \\
			& g, x & \mapsto & \left \{ \begin{array}{cc}
				1 & \text{si } (g, x) \in S \\
				0 & \text{sinon}\\
			\end{array}
		\right . 
		\end{array}
	\end{displaymath}
	Comme $\mathrm{Stab}(x)$ et $\mathrm{Fix}(g)$ forment des partitions respectivement de $X$ et de $G$, on a $S(., x) = \mathbb{1}_{\mathrm{Stab}(x)}$ et $S(g, .) = \mathbb{1}_{\mathrm{Fix}(g)}$. On a alors $\left|S\right| = \sum_{g \in G} \left|\mathrm{Fix}(g)\right| = \sum_{x \in X} \left|\mathrm{Stab}(x)\right|$. D'où le résultat.
\end{proof}}}
	
\begin{theo}[Formule du Burnside]
	Soit $G$ un groupe et $X$ un ensemble sur lequel $G$ agit. Le nombre d'orbite de l'action est $t = \frac{1}{\left|G\right|} \sum_{g \in G} \left|Fix(g)\right|$
\end{theo}
{\footnotesize{\begin{proof}
	On va alors montrer que $t\left|G\right| = \sum_{g \in G} \left|Fix(g)\right|$. On remarque que $X = \sqcup_{i=1}^{t} \mathrm{Orb}(x_i)$ où $x_i$ est un représentant d'un des orbites. Comme on a $\sum_{g \in G} \left|\mathrm{Fix}(g)\right| = \sum_{x \in X} \left|\mathrm{Stab}(x)\right|  \footnotesize{\textblue{\text{ (lemme~\ref{lem:FixStab}) }}}
	 = \sum_{i=1}^{t} \left|\mathrm{Orb}(x_i)\right| \frac{\left|G\right|}{ \left|\mathrm{Orb}(x_i)\right|}  \footnotesize{\textblue{\text{ (lemme~\ref{lem:cardG}) }}}
	= t\left|G\right| \footnotesize{\textblue{\text{ ($G$ indépendant somme)}}}$. D'où le résultat.
\end{proof}}}
	
\begin{req}
	On voit apparaître une version de la formule des classes.
\end{req}

\paragraph{Actions associées à l'action d'un groupe et invariants \cite[p.47]{Laville}}

\begin{prop}
	Soit $G$ un groupe opérant sur un ensemble $E$, $p \in \N{*}$, alors $G$ opère de façon naturelle sur $E^{p} = E _\times \dots \times$ par $g(x_1, \dots, g_p) = (gx_1, \dots gx_p)$.
	
	$G$ opère aussi sur $\mathcal{P}(E)$, l'ensemble des sous-ensembles de $E$ par l'action: si $A \in \mathcal{P}(E)$, $gA = \{ga, a \in A\}$.
\end{prop}

De plus, si $G$ opère sur $E$ et $F$, il opère sur l'ensemble des fonctions de $E$ dans $F$.

\begin{definition}
	Soit $G$ un groupe opérant sur $E$. Un élément $x \in E$ est dit invariant quand: $\forall g \in G$, $gx =x $.
\end{definition}

Si on se donne un espace homogène $(G, E)$, un ensemble $K$ et qu'on fait opérer $G$ sur $K$ de manière triviale, on cherche le plus petit entier $p$ et une (ou plusieurs) fonction invariante $f : E^{p} \rightarrow K$. Cette recherche est la recherche d'invariant pour notre action de groupe et dans ce cas précis pour la géométrie que l'on considère.

Cette recherche d'invariant permet la classification des figures. Si on se donne une géométrie par un espace homogène $(G, E)$. On considère l'espace des orbites $\mathcal{P}(E)/G$ (définit par l'action de $G$ sur $\mathcal{P}(E)$) et on cherche suffisamment d'invariant sur chaque orbite pour les caractériser (puisqu'une figure en géométrie correspond à une orbite de cette action).