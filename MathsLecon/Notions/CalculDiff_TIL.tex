%LECONs: 214 (TIL, TFI)
%DEVs : Expoentielle de matrices
Le théorème d'inversion local consiste en la résolution de l'équation $y = f(x)$ en inversant $f$, soit $x = f^{-1}(y)$ \cite[p.187]{Rouviere}. Avant d'énoncer le théorème, rappelons la définition d'un $\Ck^k$-difféomorphisme.

\paragraph{Application $\Ck^k$-difféomorphisme \cite[p.707]{Marco-Thieullen-Weil}}

\begin{definition}[Difféomorphisme global]
	Soient $E$ et $F$ deux espaces vectoriels normés, $U$ est un ouvert de $E$ et $V$ un ouvert de $F$. Soit $k \in \N^* \cup \{+\infty\}$.
	\begin{enumerate}
		\item Une application $f : U \to V$ est un $\mathcal{C}^k$-difféomorphisme global (ou simplement $\Ck^k$-difféomorphisme) si $f$ est bijective de $U$ dans $V$, et si $f^{-1}$ est $\Ck^k$ sur $V$.
		\item On dit que $U$ et $V$ sont $\Ck^k$-difféomorphe s'il existe un $\Ck^k$-difféomorphisme de $U$ dans $V$.
	\end{enumerate}
\end{definition}

\begin{definition}[Difféomorphisme local]
	Soient $E$ et $F$ deux espaces vectoriels normés, $U$ est un ouvert de $E$ et $k \in \N^* \cup \{+\infty\}$.
	\begin{enumerate}
		\item Une application $f : U \to F$ est un $\mathcal{C}^k$-difféomorphisme local en $x_0 \in U$ s'il existe un voisinage ouvert $V$ de $x_0$ et un voisinage $W$ de $f(x_0)$ tels que $f|_V : V \to W$ est un $\Ck^k$-difféomorphisme.
		\item Une application $f : U \to F$ est un $\mathcal{C}^k$-difféomorphisme local $U$, si $f$ est un $\Ck^k$-difféomorphisme local en tout point de $U$.
	\end{enumerate}
\end{definition}

\paragraph{Les variantes du théorème d'inversion locale}

\begin{theo}[Théorème d'inversion locale]
	Soient $U$ un ouvert de $\R^n$, $a$ un point de $U$, et $f: U \to \R^n$ une application de classe $\Ck^1$. On suppose que la matrice jacobienne $Df(a)$ est inversible (i.e. $\det Df(a) \neq 0$). Il existe alors un ouvert $V$ contenant $a$ (et contenu dans $U$) et un ouvert $W$ contenant $b = f(a)$, tels que $f$ (restreinte à $V$) soit un difféomorphisme de classe $\Ck^1$ de $V$ sur $W = f(V)$.
\end{theo}
\begin{footnotesize}
	\begin{proof}[Idée de la preuve]
		On peut développer $f(x)$ à l'ordre un puis appliqué l'inverse de $Df(a)$.
	\end{proof}
	\begin{proof}
		Utilisation d'un point fixe: on se ramène à un problème de point fixe en posant $F(x) = x - Df(a)^{-1}(f(x) - y)$. La solution sera donc une limite d'une suite récurrente définie par $F$. On conclut en vérifiant la régularité de la solution obtenue.
	\end{proof}
\end{footnotesize}

\begin{req}
	On peut se placer dans des Banach en supposant que $df(a)$ est bijective de $E$ dans $F$.
\end{req}

\emph{Interprétation}: On a ainsi: $(x \in V \text{ et } y = f(x)) \Leftrightarrow (y \in W \text{ et } x = f^{-1}(y))$ en notant $f^{-1} : W \to V$ la réciproque de $f$ restreinte à $V$. L'équation $y = f(x)$ admet alors une unique solution dans $V$, mais elle peut en avoir d'autre en dehors de $V$ (on peut avoir $V \neq U$).

\begin{req}
	On a $D\left(f^{-1}\right)(y) = Df(x) ^{-1}$ pour $x \in V$.
\end{req}

En pratique, l'application $f$ se présente sous la forme $f(x) = \left(f_1(x_1, \dots, x_n), \dots, f_n(x_1, \dots, x_n)\right)$ et l'hypothèse à vérifiée est 
\begin{displaymath}
	\det Df(a) = \det \begin{pmatrix}
	\frac{\partial f_1}{\partial x_1} & \dots & \frac{\partial f_1}{\partial x_n} \\
	\vdots & & \vdots \\
	\frac{\partial f_n}{\partial x_1} & \dots & \frac{\partial f_n}{\partial x_n} \\
	\end{pmatrix} (a_1, \dots, a_n) \neq 0
\end{displaymath}
Notons que cette hypothèse est nécessaire par la dérivée de composition.

\begin{ex}[\protect{\cite[p.202]{Rouviere}}]
	Soit $f : \R^2 \to \R^2$ tel que $f: (x, y) \mapsto (s,p) = (x + y, xy)$. Alors $U = \{x > y\}$ sont les ouverts connexes maximaux tels que $f : \Omega \to f(\Omega)$ soit un difféomorphisme.
\end{ex}

\begin{cex}[\protect{\cite[p.204]{Rouviere}}]
	Soit $f(x) = x + x^2 \sin \frac{\pi}{2}$ si $x \neq 0$ et $f(0) = 0$ est dérivable mais pas inversible localement.
\end{cex}

\begin{req}
	On obtient le même résultat en prenant $\Ck^k$ à la place de $\Ck^1$.
\end{req}

\begin{theo}[Théorème d'inversion globale]
	Soient $U$ un ouvert de $\R^n$ et $f : U \to \R^n$ de classe $\Ck^1$. On suppose que $f$ est injective sur $U$ et que, pour tout $u \in U$ la matrice jacobienne $Df(u)$ est inversible (i.e. $\det Df(u) \neq 0$). Alors, l'ensemble image $f(U)$ est un ouvert de $\R^n$ et $f$ est un $\Ck^1$-difféomorphisme de $U$ sur $f(U)$.
\end{theo}

\begin{req}
	L'hypothèse d'injectivité est nécessaire. En pratique la montrer revient souvent à calculer un inverse. Ce théorème perd alors tout intérêt.
\end{req}

\begin{cex}[\protect{\cite[p.204]{Rouviere}}]
	Soit $f(x, y) = (x^2 - y^2, 2xy)$ est une difféomorphisme local au voisinage de tout point de $U$ mais pas un difféomorphisme global.
\end{cex}

\begin{appli}[Inverse globale \protect{\cite[p.221]{Rouviere}}]
	Soient $k > 0$ une constante et $f : \R^n \to \R^n$ une application de classe $\Ck^1$ supposée $k$-dilatante, i.e. $\lvert f(x) - f(y) \rvert \geq k \lvert x - y\rvert$, pour tout $x, y \in \R^n$. Alors, $f$ est un difféomorphisme global de $\R^n$ sur lui-même.
\end{appli}
\begin{footnotesize}
	\begin{proof}
		\begin{itemize}
			\item $f$ est injective: $f(x) = f(y)$ entraîne (via les normes) que $x = y$.
			\item $f(\R^n)$ est un fermé de $\R^n$: caractérisation séquentielle.
			\item $Df(x)$ inversible pour tout $x$: calcul des normes de la définition.
			\item $f(\R^n)$ est un ouvert de $\R^n$: Inversion locale au voisinage d'un point.
			\item Conclure: connexité de $\R^n$ montre $f$ surjective et on conclut par le théorème d'inversion globale.
		\end{itemize}
	\end{proof}
\end{footnotesize}

\begin{req}
	Ce théorème est un cas particulier du théorème d'Hadamard--Lévy. L'hypothèse $k$-dilatante entraîne que $f$ est propre et que $Df(x)$ est inversible en tout point.
\end{req}

\begin{theo}[Théorème d'inversion version holomorphe]
	Soient $U$ un ouvert connexe de $\C$ et $f : U \to \C$ une fonction holomorphe. On suppose que $f$ est injective sur $U$. Alors, l'image $f(U)$ est un ouvert de $\C$ et $f$ est un difféomorphisme holomorphe de $U$ sur $f(U)$, i.e. $f^{-1}$ est holomorphe sur $f(U)$.
\end{theo}

\begin{req}
	Il est remarquable qu'on est besoin d'aucune hypothèse sur la dérivée de $f$: l'injectivité de celle-ci suffit à montrer que $f'$ ne s'annule pas. Sinon, le développement en série entière de $f$ indiquerait qu'elle se comporte comme $x^k$, $k \geq 2$ qui n'est pas injective.
\end{req}

\begin{appli}[Inversion de la fonction holomorphe \protect{\cite[p.234]{Rouviere}}]
	Soit $f$ une fonction holomorphe sur le disque unité $\left|z\right| < 1$. On suppose $f(0) = 0$, $f'(0) = 1$ et $\left|f'(z)\right| < M$ pour $\left|z\right| < 1$. Alors, $f$ est un difféomorphisme holomorphe du disque $\left|z\right| < R$ sur un ouvert contenant le disque $ \left|w\right| < \frac{R}{2}$.
\end{appli}
\begin{footnotesize}
	\begin{proof}
		\begin{itemize}
			\item Il existe $R \in \left]0, 1\right[$ tel que $\left|f'(z) - 1\right|\leq \frac{\left|z\right|}{R}$ pour $\left|z\right| < 1$ : on applique le principe du maximum à $\frac{f'(z) - 1}{z}$ et on choisi $R = \frac{1}{(M + 1)}$.
			\item $f$ injective sur le disque $\left|z\right| \leq R$: compare les valeurs de $f(z) - z$ en deux points.
			\item $\left|f(z) - z\right| \leq \frac{\left|z\right|^2}{2R}$ pour $\left|z\right| < 1$ : calcul d'intégrale
			\item Conclure: application du théorème de Rouché à la fonction $z \mapsto f(z) - w$ puis le théorème d'inversion locale version holomorphe.
		\end{itemize}
	\end{proof}
\end{footnotesize}


\paragraph{Application en analyse} Nous donnons ici quelques applications de ce théorème. Ces applications viennent de problèmes d'analyses.

\begin{theo}[Théorème de changement de coordonnées]
	Soient $f_1, \dots, f_n$ des fonctions numériques de classes $\Ck^1$ au voisinage d'un point $a$ de $\R^n$. Les relations $u_1 = f_1(x_1, \dots, x_n), \dots, u_n = f_n(x_1, \dots, x_n)$ définissent un changement de coordonnées sur un voisinage de $a$ si et seulement si le déterminant du Jacobien n'est pas nul, c'est-à-dire, si les différentielles $Df_1(a), \dots, Df_p(a)$ sont des formes linéaires indépendantes sur $\R^n$. 
\end{theo}
\begin{footnotesize}
	\begin{proof}
		Reformulation du théorème d'inversion locale.
	\end{proof}
\end{footnotesize}

\begin{ex}[\protect{\cite[p.71]{Rouviere}}]
	Coordonnées polaires
\end{ex}

\begin{cor}
	Soient $f_1, \dots, f_p$ des fonctions numériques de classes $\Ck^1$ au voisinage d'un point $a$ de $\R^n$. On peut les compléter par des fonctions $f_{p+1}, \dots, f_n$ en un changement de coordonnées au voisinage de $a$ sur $\R^n$ si et seulement si les différentielles $Df_1(a), \dots, Df_p(a)$ sont des formes linéaires indépendantes sur $\R^n$. 
\end{cor}

\begin{req}
	Ce résultat est une généralisation du théorème de la base incomplète en algèbre linéaire. Il permet de simplifier un problème en changeant les coordonnées de fonctions qui y jouent un rôle important (résolution d'équations aux dérivées partielles).
\end{req}

\begin{prop}[Perturbation de l'identité \protect{\cite[p.714]{Marco-Thieullen-Weil}}]
	Soient $E$ un espace de Banach et $g : E \to E$ une application $\Ck^1$. S'il existe $M > 0$ tel que, pour tout $x \in E$, $\lvert d_xg \rvert_{\mathcal{L}(E)} \leq M$, alors, pour tout $\epsilon \in \left]0, \frac{1}{M} \right[$, l'application $f_{\epsilon} = I_E + \epsilon g$ est un $\Ck^1$-difféomorphisme de $E$ dans $E$.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		\begin{itemize}
			\item $f_{\epsilon}$ est bijective: on se ramène à un résultat des points fixe et on montre que $z- \epsilon g(z)$ est contractante via le théorème des accroissements finis.
			\item $f_{\epsilon}$ est bijective: on applique le théorème d'inversion locale à $f_{\epsilon}$.
		\end{itemize}
	\end{proof}
\end{footnotesize}

\begin{prop}[Isométrie de $\R^{n*}$ \protect{\cite[p.715]{Marco-Thieullen-Weil}}]
	Soient $n \geq 2$ et $f : \R^{n*} \to \R^{n*}$ une application $\Ck^1$ telle que $\forall x \in \R^{n*}$, $Df(x) \in GL_n(\R)$ et $\lvert f(x)\rvert_2 = \lvert x \rvert$. Alors $f$ est surjective.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		\begin{itemize}
			\item On se restreint au cas où l'isométrie est définie sur la sphère $S_r$ de centre l'origine et de rayon $r$ qui est connexe.
			\item $f(S_r)$ est fermé: image d'un compact.
			\item $f(S_r)$ est ouvert: théorème d'inversion locale.
		\end{itemize}
	\end{proof}
\end{footnotesize}

\begin{theo}[Lemme de Morse à deux variables \protect{\cite[p.330]{Rouviere}}]
	Soit $U$ un ouvert de $\R^2$ tel que $0 \in U$. Soit $f : U \to \R$ de classe $\Ck^3$. On suppose que la forme quadratique $D^2(f)(0,0)$ est non-dégénérée.
	\begin{enumerate}
		\item Si la signature de $D^2(f)(0,0)$ est $(2, 0)$ alors il existe $u$ et $v$ tels que $f(x, y) - f(0, 0) - df(0, 0)(x, y) = u(x, y)^2 + v(x, y)^2$.
		\item Si la signature de $D^2(f)(0,0)$ est $(1, 1)$ alors il existe $u$ et $v$ tels que $f(x, y) - f(0, 0) - df(0, 0)(x, y) = u(x, y)^2 - v(x, y)^2$.
	\end{enumerate}
\end{theo}
\begin{footnotesize}
	\begin{proof}
		\begin{itemize}
			\item Développement de Taylor à l'ordre un avec reste intégrale.
			\item On effectue une réduction de Gauss et par positivité du déterminant (via la signature du cas 1), on obtient les fonction $u$ et $v$. On conclut avec le théorème d'inversion locale.
			\item Si la signature est $(+, -)$ on se ramène au cas précédent, si elle est $(+, +)$ on décompose par Gauss. Sinon (elle est $(-, -)$) et on applique ce qui précède à $-f$.
		\end{itemize}
	\end{proof}
\end{footnotesize}

\begin{appli}[\protect{\cite[p.341]{Rouviere}}]
	Étude affine locale d'une surface via le lemme de Morse: décompose selon la signature.
\end{appli}

\paragraph{Application en algèbre et en géométrie} Nous donnons ici quelques applications de ce théorème. Ces applications viennent de problèmes d'algèbre.

\begin{theo}[Réduction des formes quadratiques \protect{\cite[p.209]{Rouviere}}]
	On note $E$ l'espace des matrices réelles $n \times n$ et $S$ le sous-espace des matrices symétriques. On fixe $A_0 \in S$ inversible. Soit $\varphi : E \to S$ l'application définie par $\varphi(M) = ^tMA_0M$. Alors, il existe un voisinage $V$ de $A_0$ dans $S$ et une application de $A \mapsto M \in V$ de classe $\Ck^1$ telle que $A ) ^tMA_0M$.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		\begin{itemize}
			\item L'application $\varphi$ est de classe $\Ck^1$ car polynomiale de noyau de $D\varphi(I)$ est les matrices $H$ telles que $A_0H$ soit antisymétrique.
			\item Décomposition des matrices en somme d'une matrice symétrique et une matrice antisymétrique : supplémentaire du noyau de $D\varphi(I)$ est dans $F$. On restreint alors $\varphi$ à $F$ et on applique le théorème d'inversion locale.
		\end{itemize}
	\end{proof}
\end{footnotesize}

\begin{req}
	Pour toute forme quadratique suffisamment proche d'une forme quadratique non dégénérée, elle lui est équivalente (on s'y ramène par changement de base). Elles ont en particulier la même signature. Donc les matrice de même signature donnée forment un ouvert de $S$.
	
	De plus, en appliquant ce résultat à $A_0 = I$, on voit que tout matrice $A$ symétrique suffisamment proche de l'identité admet une racine carré symétrique.
\end{req}

\begin{appli}[Lemme de Morse à $n$ variables \protect{\cite[p.354]{Rouviere}}]
	Soit $U$ un ouvert de $\R^2$ tel que $0 \in U$. Soit $f : U \to \R$ de classe $\Ck^3$. On suppose que la forme quadratique $D^2(f)(0,0)$ est non-dégénérée de signature $(p, n-p)$ et que $Df(0) = 0$. Alors, il existe un difféomorphisme $x \mapsto u = \varphi(x)$ entre deux voisinage de l'origine, de classe $\Ck^1$, tel que $\varphi(0) = 0$ et $f(x) - f(0) = u_1^2 + \dots + u_p^2 - u_{p+1}^2 - \dots - u_n^2$.
\end{appli}
\begin{footnotesize}
	\begin{proof}[Idée de la preuve]
		\begin{itemize}
			\item Formule de Taylor à l'ordre un avec reste intégral: existence d'une matrice $Q$ symétrique.
			\item Réduction des formes quadratiques : décomposition de $Q(x) = ^tM(x)Q(0)M(x)$.
			\item Théorème d'inversion local sur la matrice $M$.
		\end{itemize}
	\end{proof}
\end{footnotesize}

Le TIL transforme un système quelconque (de classe $\Ck^1$) de deux équations à deux inconnues se discute comme un système linéaire de rang le rang de la matrice jacobienne.

\begin{appli}[Système de deux équations à deux inconnues \protect{\cite[p.211]{Rouviere}}]
	Soient $U$ un ouvert de $\R^2$ et $\varphi : U \to \R^2$ une application de classe $\Ck^1$ telle que $\varphi(x, y) = \left(f(x, y), g(x, y)\right)$. Étudions le système d'équation $f(x, y) = u$, $g(x, y) = v$ où $u$ et $v$ sont données.
	\begin{itemize}
		\item Si $D\varphi$ est de rang $2$ en un point de $U$, alors il existe un ouvert $V$ (donné par le TIL) tel qu'il existe une unique solution.
		\item Si $D\varphi$ est de rang $1$ en tout point de $U$, alors le système possède soit une infinité de solution, soit aucune.
		\item Si $D\varphi$ est de rang $0$ en tout point de $U$, alors le système possède une solution si et seulement si $u = A$ et $v = B$; tout point $(x, y) \in U$ est alors solution.
	\end{itemize}
\end{appli}

\begin{theo}[Image de l'exponentielle de matrice \protect{\cite[p.48]{Zavidovique}}]
	Soit $A \in \mathcal{M}_n(\C)$, on a $\exp(\C[A]) = \C^{\times}[A]$.
\end{theo}

\begin{cor}
	\begin{enumerate}
		\item $\exp(\mathcal{M}_n(\C))  = GL_n(\C)$
		\item $\exp(\mathcal{M}_n(\R)) = \{A^2, ~A \in GL_n(\R)\}$
		\item $GL_n(\C)$ ne possède pas de sous-groupe arbitrairement petit.
	\end{enumerate}
\end{cor}


