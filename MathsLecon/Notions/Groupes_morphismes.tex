%LECON: 104
Les morphismes de groupes \cite{Calais} sont des applications entre deux groupes qui préservent leur structure: l'image de cette application est alors un groupe (ou un sous-groupe). Ils conservent alors de nombreuses propriétés des groupes: leur étude est très importante.

\begin{definition}[Morphisme de groupe]
	Soient deux groupes $(G, \cdot)$ et $(G', *)$, un morphisme de groupe de $G$ dans $G'$ est une application $f:G \to G'$ telle que, quels que soient $x$ et $y$ dans $G$, on ait: $f(x \cdot y) = f(x) * f(y)$.
\end{definition}

Un morphisme de groupe est aussi appelé homéomorphisme de groupes. D'ailleurs, on note l'ensemble des morphismes de groupes de $G$ dans $G'$ par $\mathrm{Hom}(G, G')$. Un morphisme de groupe $G$ dans lui-même est appelé endomorphisme de groupe. On note l'ensemble des endomorphisme du groupe $G$ par $\mathrm{End}(G)$.

\begin{prop}
	Tout $f \in \mathrm{Hom}(G, G')$ vérifie les propriétés suivantes:
	\begin{enumerate}
		\item $f(e) = e'$.
		\item $\forall x \in G$, $f(x^{-1}) = \left(f(x)\right)^{-1}$.
		\item $\forall x \in G$ et $\forall n \in \N$, $f(x^n) = \left(f(x)\right)^n$.
		\item Si $H$ est un sous-groupe de $G$, alors $f(H)$ est un sous-groupe de $G'$.
		\item  Si $H'$ est un sous-groupe de $G'$, alors $f^{-1}(H') = \{x \in G; f(x)\in H\}$ est un sous-groupe de $G$.
	\end{enumerate}
\end{prop}
{\footnotesize{\begin{proof}
	\begin{enumerate}
		\item Pour tout $x \in G$, $f(x)f(e) = f(xe) = f(x) = f(x)e'$. Par simplification dans $G'$, on a $f(e) = e'$.
		
		\item Soit $x \in G$, $f(e) = f\left(xx^{-1}\right) = f\left(x\right)f\left(x^{-1}\right)$. Or $f(e) = e' = f(x)\left(f(x)\right)^{-1}$ \textblue{(par l'item 1)}. Donc $f(x^{-1}) = \left(f(x)\right)^{-1}$.
		
		\item Se montre par distinction de cas sur $n$.
		\begin{description}
			\item[Cas $n = 0$,] $x^0 = e$ dans ce cas on se ramène au cas 1
			\item[Cas $n > 0$,] $f(x^n) = f(x) \dots f(x)$ \textblue{($n$ fois)}, donc $f(x^n) = \left(f(x)\right)^n$.
			\item[Cas $n < 0$,] $f(x^{-n}) = f\left((x^{-1})^{n'}\right) = f\left(x^{-1}\right)^{n'} = \left(f(x)^{-1}\right)^{n'} = f\left(x\right)^{n}$.
		\end{description}
		\item $f\left(H\right) =\{f(x), x \in H\}$. Soient $y_1$, $y_2$ dans $f\left(H\right)$, il existe $x_1$ et $x_2$ tels que $y_1 = f\left(x_1\right)$ et $y_2 = f\left(x_2\right)$. On a $y_1y_2^{-1} = f\left(x_1\right)\left(f\left(x_2\right)\right)^{-1} = f\left(x_1\right)\left(f\left(x_2^{-1}\right)\right) = f\left(x_1x_2^{-1}\right)$. En appliquant le théorème, on obtient le résultat.
		\item Soient $x_1$, $x_2$ dans $f^{-1}\left(H'\right)$, alors $f\left(x_1\right) \in H'$ et $f\left(x_2\right) \in H'$. Comme $H'$ est un sous-groupe de $G'$, on a $f\left(x_1\right)\left(f\left(x_2\right)\right)^{-1} = f\left(x_1\right)\left(f\left(x_2^{-1}\right)\right) = f\left(x_1x_2^{-1}\right) \in H'$. Donc $x_1x_2^{-1} \in f^{-1}\left(H\right)$, et en appliquant le théorème, on obtient le résultat.
	\end{enumerate}
\end{proof}}}

\noindent\begin{minipage}{.6\textwidth}
	\begin{cor}
		Soit $f \in \mathrm{Hom}(G, G')$, alors:
		\begin{enumerate}
			\item $f\left(G\right)$ est un sous-groupe de $G'$.
			\item $f^{-1}\left(e'\right) =\{x \in G, f(x)=e'\}$ est un sous-groupe de $G$.
		\end{enumerate}
	\end{cor}
\end{minipage}\hfill
\begin{minipage}{.36\textwidth}
	{\footnotesize{\begin{proof}
				Application de la proposition au cas particulier de $H =G$ et $H'=\{e'\}$.
			\end{proof}}}
\end{minipage}



\begin{definition}[Image et noyau]
	Soit $f\in \mathrm{Hom}(G, G')$.
	\begin{itemize}
		\item $f\left(G\right)$ est appelé image de $f$ et est noté $\mathrm{Im}f$.
		\item $f^{-1}\left(e'\right)$ est appelé noyau de $f$ et est noté $\ker f$.
	\end{itemize}  
\end{definition}

\begin{prop}
	Pour $f \in \mathrm{Hom}(G, G')$, on a:
	\begin{enumerate}
		\item $f$ surjectif $\Leftrightarrow \mathrm{Im} f = G'$.
		\item $f$ injectif $\Leftrightarrow \ker f = \{e\}$.
	\end{enumerate}
\end{prop}
{\footnotesize{\begin{proof}
	\begin{enumerate}
		\item Immédiat par définition de la surjectif.
		\item Rappelons que $f$ est injectif si et seulement si $\forall x, x' \in G$, $f(x) = f(x')$ implique $x =x'$.
		\begin{itemize}
			\item Supposons $f$ injectif; soit $x \in \ker f$, alors $f(x) = e' = f(e)$, d'où par la remarque précédente $x = e$ et $\ker f =\{e\}$.
			
			\item Supposons que $\ker f =\{e\}$; soient $x$ et $x'$ dans $G$ tels que $f(x) = f(x')$; on en déduit que $e' =\left(f(x)\right)^{-1}f(x') = f(x^{-1})f(x')$, d'où $e = f(x^{-1}x')$, ce qui implique $x^{-1}x' \in \ker f$; $\ker f =\{e\} \Rightarrow x^{-1}x' =e$, d'où $x' =x$. Donc $f$ est injectif.
		\end{itemize} 
	\end{enumerate}
\end{proof}}}

\begin{prop}
	Soient $G$, $G'$, $G''$ trois groupes, alors $f \in \mathrm{Hom}(G, G')$ et $g \in \mathrm{Hom}(G', G'')$ implique $g \circ f \in \mathrm{Hom}(G, G'')$.
\end{prop}
{\footnotesize{\begin{proof}
	Soient $x$ et $y$ dans $G$;
	
	\begin{tabular}{ccll}
		$g \circ f(xy)$ & $=$ &$g\left(f(xy)\right)$ &  \\
		& $=$ & $g\left(f(x)f(y)\right)$ & car $f \in \mathrm{Hom}(G, G')$ \\
		& $=$ & $g\left(f(x)\right)g\left(f(y)\right)$ & car $f \in \mathrm{Hom}(G', G'')$ \\
		& $=$ & $\left(g \circ f(x)\right)\left(g\circ f(y)\right)$ & \\
	\end{tabular}
\end{proof}}}

\begin{definition}[Isomorphisme]
	Une application $f$ d'un groupe $G$ dans un groupe $G'$ est un isomorphisme de groupes si $f \in \mathrm{Hom}(G, G')$ et s'il existe $g \in \mathrm{Hom}(G', G)$ tel que $g \circ f = id_G$ et $f \circ g =id_{G'}$.
\end{definition}