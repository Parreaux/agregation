%DEVs : matrice diagonalisable; cone nilpotent
%LECONs : 101 (action de groupe); 106 (groupe lineaire); 123 (Corps finis)
Nous donnons ici une batterie de cardinaux \cite[p.57]{Caldero-Germoni-N2} liés au groupe linéaire $GL_n(\F_q)$. On note l'entier $n$ quantique $[n]_q = 1 + \dots + q^{n-1}$ que l'on peut considérer comme une $q$-déformation du nombre $n$ : si $q = 1$, on retrouve $n$. 

On définit de même le factoriel et le coefficient binomial quantique. Pour $m_1, \dots, m_k$ dans $\N$ tels que $m_1 + \dots + m_k = n$, on définit le nombre multinomial quantique:
\begin{displaymath}
\begin{array}{ccccccc}
[n]_q! = [n]_q[n-1]_q \dots [1]_q & & [0]_q! =1 & & \left[\begin{array}{c}
n \\ m
\end{array}\right]_q = \frac{[n_q]!}{[m]_q![n-m]_q!} & & \left[\begin{array}{c}
n \\ m_1 \dots m_k
\end{array}\right]_q = \frac{[n]_q!}{\prod_{i = 1}^{k} [m_i]_q!} \\
\end{array}
\end{displaymath}

\noindent\begin{minipage}{0.46\textwidth}
	\begin{lemme}[Espace et sous-espace]
		Soit $n, q \in \N$.
		\begin{enumerate}
			\item L'espace vectoriel : $|E| = |\F_q^n| = q^n$
			\item L'espace projectif : $|\mathbb{P}(E)| = [n]_q$
			\item Grassmannienne : $|Gr_{m,n}(\F_q)| =  \left[\begin{array}{c}
			n \\ m
			\end{array}\right]_q$
		\end{enumerate}
	\end{lemme}
\end{minipage} \hfill
\begin{minipage}{.5\textwidth}
	\begin{footnotesize}
		\begin{proof}
			\begin{enumerate}
				\item Existence d'une base.
				
				\item On fait agir le groupe multiplicatif $\mathbb{K}^*$ sur $\mathbb{K}^{n+1} \setminus \{0\}$ par homothéties \textblue{($\lambda \in \mathbb{K}^*$ agit sur $v$ par $\lambda . v = \lambda v$)}. L'action est libre: $\forall v \neq 0, Stab_{\mathbb{K}^*}(v) = \{1\}$. On en déduit que :
				$|\mathbb{P}^n(\F_q)| = \frac{|\F{n+1}_q \setminus \{0\}}{|\F^*_q|} = \frac{q^{n+1} -1 }{q-1} = 1 + q + \dots + q^n$.
				
				\item $GL_n(\mathbb{K})$ agit transitivement sur la grassmannienne $Gr_{m,n}(\mathbb{K})$. Le stabilisateur d'un sous-espace $F$ est donné par $Stab(F) = (GL_m(\mathbb{K}) \times GL_{n-m}(\mathbb{K}) \ltimes \mathcal{M}_{m, n-m}(\mathbb{K})$. 
			\end{enumerate}
		\end{proof}
	\end{footnotesize}
\end{minipage}

\begin{lemme}[Groupes et sous-groupes]
	Soit $n, q \in \N$.
	\begin{enumerate}
		\item Groupe linéaire : $|GL_n(\F_q)| = g_n = q^{\frac{n(n-1)}{2}}(q-1)^n[n]_q!$
		\item Groupe spécial linéaire : $|SL_n(\F_q)| = \frac{g_n}{q-1} = q^{\frac{n(n-1)}{2}}(q-1)^{n-1}[n]_q!$
		\item Groupe projectif : $|PGL_n(\F_q)| = \frac{g_n}{q-1} = q^{\frac{n(n-1)}{2}}(q-1)^{n-1}[n]_q!$
		\item Groupe spécial projectif : $|PSL_n(\F_q)| = \frac{g_n}{(q-1)d} = \frac{1}{d}q^{\frac{n(n-1)}{2}}(q-1)^{n-1}[n]_q!$ avec $d = \gcd(q-1, n)$
		\item Groupe orthogonal impair: si $ch \neq 2$, $|O_{2n+1}(\F_q)| = 2q^n\prod_{k=0}^{n-1} \left(q^{2n} - q^{2k}\right) = 2q^{n^2}(q^2 -1)^n[n]_{q^2}!$
		\item Groupe orthogonal pair: 
		
		$|O_{2n}(\F_q)| = 2\left(q^n - (-1)^{\frac{n(q-1)}{2}}\right)\prod_{k=0}^{n-1} \left(q^{2n} - q^{2k}\right) = 2q^{n(n-1)}\left(q^n - (-1)^{\frac{n(q-1)}{2}}\right)(q^2 -1)^{n-1}[n]_{q^2}!$
		\item Groupe symplectique: $|Sp_{2n}(\F_q)| = q^{n^2} \prod_{k=1}^n(q^{2k}-1) = q^{n^2}(q^2 - 1)^n [n]_{q^2}!$
	\end{enumerate}
\end{lemme}
\begin{footnotesize}
	\begin{proof}
		\begin{enumerate}
			\item Deux démonstrations.
			\begin{itemize}
				\item On fait agir transitivement $GL_n(\mathbb{K})$ sur $\mathbb{K}^n \setminus \{0\}$. On a $Stab(e_1) \simeq GL_{n-1}(\mathbb{K}) \ltimes \mathbb{K}^{n-1}$. On en déduit que $|GL_n(\F_q)| = |GL_{n-1}(\F_q)|q^{n-1}(q^n - 1)$. On conclut par récurrence.
				
				\item On compte le nombre de bases de l'espace $\F_q^n$. Pour cela, on fait agir simplement transitivement $Gl_n(\F_q)$ sur l'ensemble des bases. On raisonne ensuite par récurrence.
			\end{itemize}
			
			\item $|SL_n(\F_q)| = |GL_n(\F_q)| \setminus |\F_q^*|$ par la suite exacte suivante : $1 \longrightarrow SL_n(\mathbb{K}) \longrightarrow GL_n(\mathbb{K}) \longrightarrow \mathbb{K}^* \longrightarrow 1$.
			
			\item $PGL_n(\mathbb{K}) \simeq GL_n(\mathbb{K}) \setminus \mathbb{K}^*$ ou on compte le nombre de repère projectif (de la même manière que le précédent).
			
			\item Le groupe spécial projectif est le quotient du groupe spécial par son sous groupe distingué des homothéties. On cherche donc à montrer que le nombre de racine $n^{\text{ième}}$ de l'unité dans $\F_q$ vaut $d = \gcd(n, q-1)$ \textblue{(on applique Lagrange et Bézout)}.
		\end{enumerate}
		\textblue{(Les autres voir le tome 1)} %TODO
	\end{proof}
\end{footnotesize}

\begin{minipage}{.46\textwidth}
	\begin{lemme}[Famille]
		Soient $q, n, m \in \N$. 
		\begin{enumerate}
			\item Famille libre à $m$ éléments ($m \leq n$): $q^{\frac{m(m-1)}{2}}(q-1)^m\frac{[n]_q!}{[n-m]_q!}$
			\item Famille génératrice à $m$ éléments ($m \geq n$): $q^{\frac{n(n-1)}{2}}(q-1)^n\frac{[m]_q!}{[m-n]_q!}$
			\item Bases : $q^{\frac{n(n-1)}{2}}(q-1)^n[n]_q! = g_n$
		\end{enumerate}
	\end{lemme}
\end{minipage} \hfill
\begin{minipage}{.46\textwidth}
	\begin{footnotesize}
		\begin{proof}
			\begin{enumerate}
				\item On fait agir transitivement $GL_n(\F_q)$ sur l'ensemble des systèmes libres de cardinal $m$ \textblue{(théorème de la base incomplète)}. On trouve un premier cardinal que l'on simplifie avec les résultats précédents.
				\item Dualité
				\item Lorsqu'on compte le nombre de base de $\F_q^n$ pour trouver $|GL_n(\F_q)$.
			\end{enumerate}
		\end{proof}
	\end{footnotesize}
\end{minipage}

\begin{lemme}[Application linéaire]
	Soient $q, n, m \in \N$. 
	\begin{enumerate}
		\item Générale: $\left|\mathcal{L}\left(\F_q^m, \F_q^n\right)\right| = q^{mn}$
		\item Injective de $\F_q^m$ vers $\F_q^n$ ($m \leq n$): $q^{\frac{m(m-1)}{2}}(q-1)^m\frac{[n]_q!}{[n-m]_q!}$
		\item Surjective de $\F_q^m$ vers $\F_q^n$ ($n \leq m$): $q^{\frac{n(n-1)}{2}}(q-1)^n\frac{[m]_q!}{[m-n]_q!}$
		\item De rang $r$ de $\F_q^m$ vers $\F_q^n$ ($r \leq m$ et $r \leq n$): $g_r \left[\begin{array}{c}
		m \\ r
		\end{array}\right]_q \left[\begin{array}{c}
		n \\ r
		\end{array}\right]_q = q^{\frac{r(r-1)}{2}}(q-1)^r[r]_q!\left[\begin{array}{c}
		m \\ r
		\end{array}\right]_q \left[\begin{array}{c}
		n \\ r
		\end{array}\right]_q$
	\end{enumerate}
\end{lemme}
\begin{footnotesize}
	\begin{proof}
		\begin{enumerate}
			\item Argument de dimension
			\item Application linéaire entièrement caractérisé par l'image d'une base. Le nombre d'application injective est donc le nombre de système libre.
			\item Dualité via la transposition
			\item On fait agir par multiplication à droite $GL_n(\F_q)$ sur $\mathcal{M}_{m,n}(\F_q)$. L'ensemble des matrices dont l'image est $F$ un sous-espace de dimension $r$ est une orbite qui ne dépend ni de $r$ ni de $F$. On conclut via le stabilisateur.
		\end{enumerate}
	\end{proof}
\end{footnotesize}

\noindent\begin{minipage}{.6\textwidth}
	\begin{lemme}[Endomorphisme]
		Soient $q, n, m \in \N$. 
		\begin{enumerate}
			\item Généraux: $|End(E)| =q^{n^2}$
			\item Diagonalisable: $|\mathcal{D}_n| = \underset{\underset{n_i \geq 0}{n_1 + \dots + n_q = n}}{\sum} \frac{g_n}{\prod_{i=1}^{q} g_{n_i}}$
			\item Niloptents : $|\mathcal{N}_n| = q^{n(n-1)}$
			\item Trigonalisable: $|\mathcal{T}_n| = \underset{\underset{n_i \geq 0}{n_1 + \dots + n_q = n}}{\sum} \frac{g_n}{q^{-n}\prod_{i=1}^{q} g_{n_i}}$
		\end{enumerate}
	\end{lemme}
\end{minipage} \hfill
\begin{minipage}{.4\textwidth}
	\begin{footnotesize}
		\begin{proof}
			\begin{enumerate}
				\item Par dimension
				\item On fait agir $GL_n(\F_q)$ sur l'ensemble des sous-espaces propres.
				\item %TODO
				\item Mêmes arguments que pour la diagonalisation mais le lemme des noyaux est différents donc on n'utilise pas les sous-espaces propres.
			\end{enumerate}
		\end{proof}
	\end{footnotesize}
\end{minipage}