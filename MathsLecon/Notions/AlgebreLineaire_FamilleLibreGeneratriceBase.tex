%LECONs: 151 (Dimension et rang)
Nous allons définir la notion de base en étudiant les familles libres et génératrices \cite[p.10]{Grifone}.

\begin{definition}
	Une famille de vecteur $\{v_1, \dots, v_n\}$ d'un espace vectoriel $E$ est génératrice si $\mathrm{Vect}\{v_1, \dots, v_n\} = E$.
\end{definition}

\begin{req}
	Interprétation: pour tout $x \in E$, $x$ est combinaison linéaire des $v_i$. De plus, une telle famille n'existe pas toujours.
\end{req}

\begin{definition}
	Un espace vectoriel est dit de dimension finie, s'il existe une famille génératrice finie. Dans le cas contraire, on dit qu'il est de dimension infini.
\end{definition}

\begin{ex}
	$\R^n$ est de dimension finie et $\R[X]$ est de dimension infinie.
\end{ex}

\begin{definition}
	Une famille de vecteur $\{v_1, \dots, v_n\}$ d'un espace vectoriel $E$ est libre si $\lambda_1v_1 + \dots \lambda_nv_n = 0 \Rightarrow \lambda_1 = \dots = \lambda_n = 0$.
\end{definition}

\begin{req}
	Si une famille de vecteur n'est pas libre elle est dite liée.
\end{req}

\begin{prop}[Caractérisation de la liberté par la généricité]
	Une famille $\{v_1, \dots, v_n\}$ est liée si et seulement si l'un au moins des vecteurs $v_i$ appartient à l'espace engendré par les autres.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		\begin{description}
			\item[$\Rightarrow$] Existence de $\lambda_i$ qui donne les coefficients de la combinaison linéaire.
			\item[$\Leftarrow$] La combinaison linéaire possède des coefficients non-nuls.
		\end{description}
	\end{proof}
\end{footnotesize}

\begin{prop}
	Soit $\{v_1, \dots, v_n\}$ une famille libre et $x$ un vecteur quelconque de l'espace engendré par les $v_i$. Alors la décomposition de $x$ sur les $v_i$ est unique.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		On considère deux décompositions que nous soustrayons: les coefficients sont donc nuls et donc égaux.
	\end{proof}
\end{footnotesize}

\begin{definition}
	On appelle base une famille libre et génératrice.
\end{definition}

\begin{prop}
	Une famille $\{v_1, \dots, v_n\}$ est une base de $E$ si et seulement si $\forall x \in E$, $x$ se décompose de manière unique sur les $v_i$.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		Immédiat par ce qui précède.
	\end{proof}
\end{footnotesize}

\begin{prop}[Propriétés des familles libres et génératrices] \textcolor{white}{text}
	
\begin{minipage}{.5\textwidth}
	\begin{enumerate}
		\item $\{x\}$ est une famille libre si et seulement si $x \neq 0$.
		\item Toute famille contenant une famille génératrice est génératrice.
		\item Toute sous-famille d'une famille libre est libre.
		\item Toute famille contenant une famille liée est liée.
		\item Toute famille $\{v_1, \dots, v_n\}$ dont l'un des vecteurs $v_i$ est nul, est liée.
	\end{enumerate}
\end{minipage} \hfill
\begin{minipage}{.42\textwidth}
	\begin{footnotesize}
		\begin{proof}
			\begin{enumerate}
				\item $\lambda x= 0 \Rightarrow \lambda = 0$ si et seulement si $x \neq 0$.
				\item On rajoute des composantes de coefficients nuls dans la décomposition.
				\item Par contraposé: si un est lié, la combinaison existerait dans la première famille.
				\item On rajoute des composantes de coefficients nuls dans la combinaison linéaire.
				\item $\{0\}$ est liée et on applique le précédent.
			\end{enumerate}
		\end{proof}
	\end{footnotesize}
\end{minipage}
\end{prop}

Nous allons maintenant montrer l'existence de bases dans tout espace vectoriel de dimension fini.

\begin{theo}
	Soit $E \neq \{0\}$ un espace vectoriel de dimension finie et $G$ une famille génératrice. Considérons une famille libre $L \subset G$. Il existe alors une base $B$ telle que $L \subset B \subset G$.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		Soit $G = \{v_1, \dots, v_p\}$ une famille génératrice et $L = \{v_1, \dots, v_r\}$ une famille libre \textblue{(à renumérotation près)}. Si $L_1$ est génératrice, alors le théorème est prouvé. Supposons que $L_1$ n'est pas génératrice.
		\begin{itemize}
			\item Montrer qu'il existe $v_{i_1} \{v_{r+1}, \dots, v_p\}$ tel que $L_2 = \{v_1, \dots, v_r, v_{i_1}\}$ est libre. Par l'absurde, on a une combinaison linéaire pour tout $v \in v_{i_1} \{v_{r+1}, \dots, v_p\}$ et donc $L_1$ est génératrice.
			\item Si $L_2$ est génératrice, on a fini. Sinon, par récurrence on construit une suite de famille libre dans $G$ jusqu'à ce qu'elle devienne génératrice \textblue{(elle sera un jour génératrice car elle est bornée par $G$)}.
		\end{itemize}
	\end{proof}
\end{footnotesize}

\begin{theo}
	Soit $E \neq \{0\}$ un espace de dimension finie; alors:
	\begin{enumerate}
		\item De toute famille génératrice on peut extraire une base.
		\item Toute famille libre peut être complétée de manière à formée une base (Théorème de la base incomplète).
	\end{enumerate}
\end{theo}
\begin{footnotesize}
	\begin{proof}
		\begin{enumerate}
			\item Théorème précédent.
			\item Soit $L$ une famille libre. Soit $G$ une famille génératrice \textblue{(on peut avoir $L \cap G = \emptyset$)}. On pose $G' = L \cup G$: $G'$ est génératrice et $L \subset G$. On applique le théorème précédent.
		\end{enumerate}
	\end{proof}
\end{footnotesize}

\begin{prop}[\protect{\cite[p.63]{Grifone}}]
	Soit $f \in \mathcal{L}(E, F)$ et $\{v_i\}_{i \in I}$ une famille de vecteur de $E$.
	\begin{enumerate}
		\item Si $f$ est injective et la famille $\{v_i\}_{i \in I}$ est libre, alors la famille $\{f\left(v_i\right)\}_{i \in I}$ est libre dans $E'$.
		\item Si $f$ est surjective et la famille $\{v_i\}_{i \in I}$ est génératrice, alors la famille $\{f\left(v_i\right)\}_{i \in I}$ est génératrice dans $E'$.
	\end{enumerate}
\end{prop}
\begin{footnotesize}
	\begin{proof}
		\begin{enumerate}
			\item Par la linéarité de $f$: $a_1f(v_1) + \dots + a_qf(v_q) = 0$ implique que $a1v_1 + \dots + a_qv_q \in \ker f$. Par l'injectivité de $f$ : $a1v_1 + \dots + a_qv_q = 0$. Par la liberté de $\{v_i\}_{i \in I}$ : $a_i = 0$.
			\item Par la surjectivité de $f$: $\exists x$ tel que $f(x) = y$. Par la généricité de $\{v_i\}_{i \in I}$ : $x = a_1v_1 + \dots + a_rv_r$. Par la linéarité de $f$: $y = a_1f(v_1) + \dots + a_rf(v_r)$.
		\end{enumerate}
	\end{proof}
\end{footnotesize}
