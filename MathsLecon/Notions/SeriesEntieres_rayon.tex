%LECONS : 243

Une question fondamentale dans l'étude des séries entière est de connaître l'ensemble des points où la série converge. Cet ensemble de point forme un disque dont le rayon est appelé rayon de convergence. Nous allons étudier ceux-ci de plus près.

\paragraph{Rappels sur la convergence de séries de fonctions} \input{./../Notions/SeriesFonctions_convergence.tex}


\paragraph{Rayon de convergence}
Les points convergence des séries entières possèdent des propriétés remarquables en grandes parties grâce au lemme d'Abel que nous allons énoncer maintenant.

\begin{lemme}[Lemme d'Abel \pcite{ArnaudiesFraysse-3}]
	Soient $\sum a_n z^n$ une série entière et $z_0 \in \C^{*}$ tel que la suite $\left(a_n z_0^n\right)_{n \in \N}$ est bornée. Alors, pour tout $z \in \C$ tel que $\left|z\right| < \left|z_0\right|$, la série entière converge absolument.
	
	De plus; pour tout réel $r \geq 0$ tel que $0 \leq r \leq \left|z_0\right|$, la série entière converge normalement sur le disque fermé $\mathcal{B}(0, r)$.	
\end{lemme}
{\footnotesize{\begin{proof}
	Soit $M$ un majorant de la suite $\left(a_n z_0^n\right)_{n \in \N}$ \textblue{(existe car la suite est borné par hypothèse)}, on a donc $\forall n \in \N \left|a_n z_0^n\right| \leq M$. Soit $z \in \C$ tel que $\left|z\right| < \left|z_0\right|$, alors pour tout $n \in \N$, on a:
	\begin{displaymath}
	\left|a_nz^n\right| \underbrace{=}_{\text{introduction de }z_0^n}\left|a_nz_0^n\right|\left|\frac{z}{z_0}\right|^n \underbrace{\leq}_{\text{hypothèse sur }M} M\left|\frac{z}{z_0}\right|^n
	\end{displaymath}
	Comme $\left|\frac{z}{z_0}\right| < 1$ \textblue{(par hypothèse on a  $\left|z\right| < \left|z_0\right|$)}, la série géométrique $\sum \left|\frac{z}{z_0}\right|$ converge. Donc la série entière converge absolument.
	
	Soit $r \in \R$ tel que $0 \leq r \leq \left|z_0\right|$. Pour tout $n \in \N$ et pour tout $z \in \mathcal{B}(0, r)$, on a $\left|a_nz^n\right| \leq \left|a_nr^n\right|$ \textblue{(car $z \leq r$ et croissance et positivité des opérations que l'on applique ensuite)}.  Comme $r < z_0$ \textblue{(par hypothèse)}, on a vu que la série $\sum \left|a_nr^n\right|$ converge, d'où la convergence normale de la série entière.
\end{proof}}}

\begin{definition}[Rayon de convergence \pcite{ArnaudiesFraysse-3}]
	Le rayon de convergence d'une série entière est 
	\begin{displaymath}
	R = \sup\{r \in \R_+, \left(a_nr^n\right)_{n \in \N} \text{ est bornée}\}
	\end{displaymath}
\end{definition}
\textred{\textbf{Attention}} dans la référence, la définition n'est pas donnée directement sous cette forme. Il faut combiner la proposition 1.1 et la définition 1.2 pour aboutir à cette forme.

\begin{req}
	Le rayon de convergence $R$ peut être également définit \cite[p.473]{Marco-Thieullen-Weil} comme $R = \sup \{r \in \R_+, \underset{n \to +\infty}{\lim} a_nr^n = 0\}$ ou bien comme  $R = \sup \{r \in \R_+, \sum a_nr^n \text{ est obsolument convergente}\}$. Ce résultat se déduit du lemme d'Abel et des résultat sur la convergence absolue des séries de fonctions.
\end{req}

\begin{ex}[Étude du rayon de convergence de ces séries \pcite{ArnaudiesFraysse-3}] On étudie les rayons de convergences pour certaines séries entières.
	\begin{itemize}
		\item La série $\sum n!z^n$ a un rayon de convergence de $0$ puisque le seul réel $r$ pour lequel la suite $\left(n!r^n\right)_{n \in \N}$ est bornée vaut $0$.
		\item La série $\sum \frac{z^n}{n!}$ a un rayon de convergence de $+ \infty$ car pour tout réel $r$ la suite $\left(\frac{r^n}{n!}\right)_{n \in \N}$ est bornée.
	\end{itemize}
\end{ex}

\begin{req}
	Avec ce qu'on a dit précédemment, on voit que la série entière $\sum a_n z^n$ diverge grossièrement pour tout $z \notin \overline{\mathcal{B}}(0, R)$ où $R$ est le rayon de convergence de la série
\end{req}

\begin{theo}[Caractérisation du rayon de convergence]
	Soit $\sum a_n z^n$ une série entière. Soit $R \in \left]0; +\infty\right[$. Les propriétés suivantes sont équivalentes.
	\begin{enumerate}
		\item $R$ est le rayon de convergence de la série entière.
		\item La série $\sum \left|a_n z^n\right|$ converge pour tout $z \in \C$ tel que $\left|z\right|< R$ et diverge pour tout $z \in \C$ tel que $\left|z\right|> R$.
		\item  La série $\sum a_n z^n$ converge absolument pour tout $z \in \C$ tel que $\left|z\right|< R$ et $\left|z\right|> R$ la suite $\left(a_n z^n\right)_{n \in \N}$ est non bornée.
	\end{enumerate}
\end{theo} 
{\small{\begin{proof} 
	Soit $\sum a_n z^n$ une série entière. Soit $R \in \left]0; +\infty\right[$.
	
	\emph{Montrons 1 implique 3.} Supposons que $R$ est le rayon de convergence de la série entière. Alors par définition d'un rayon de convergence, la suite $\left(a_nz^n\right)_{n \in \N}$ est bornée $\forall \left|z\right| < R$ et non bornée $\forall \left|z\right| > R$ (\textblue{sinon on vient violer la condition du $\sup$}). De plus, par le lemme d'Abel, si $\left|z\right| < R$, la série $\sum a_n z^n$ est absolument convergente. D'où le résultat.
	
	\emph{Montrons 3 implique 2.} Soit $z \in \C$, on va alors distinguer deux cas. Si $\left|z\right| < R$, alors (\textblue{par hypothèse}) la série $\sum a_n z^n$ est absolument convergente, ce qui implique que la série $\sum \left|a_n z^n\right|$ converge. Maintenant si $\left|z\right| > R$, alors \textblue{par hypothèse}) la suite $\left(a_nz^n\right)_{n \in \N}$ est non bornée. En particulier, la suite $\left(\left|a_nz^n\right|\right)_{n \in \N}$ est non bornée ce qui implique que $\left|a_nz^n\right| \underset{n \to \infty}{\nrightarrow} 0$. On en déduit que la série $\sum \left|a_n z^n\right|$ diverge.
	
	\emph{Montrons 2 implique 1.} Soit $z \in \C$, on va alors distinguer deux cas et on montre dans les deux cas que $R$ est un rayon de convergence de cette série. Si $\left|z\right| < R$, alors (\textblue{par hypothèse}) la série $\sum \left|a_n z^n\right|$ converge, ce qui implique que $\left|a_n z^n\right| \underset{n \to \infty}{\rightarrow} 0$. La suite $\left(a_nz^n\right)_{n \in \N}$ est donc bornée à partir d'un certain rang. Maintenant si $\left|z\right| > R$, alors \textblue{par hypothèse}) la série $\sum \left|a_n z^n\right|$ diverge, ce qui implique que $\sum a_n z^n$ n'est pas absolument convergente. Par la contraposée du lemme d'Abel, la suite $\left(a_nz^n\right)_{n \in \N}$ n'est pas bornée. D'où $R$ est bien le rayon de convergence de la série (\textblue{par définition}).
\end{proof}}}

La frontière du disque de convergence est souvent appelé cercle de convergence mais on préférera l'appelation cercle d'incertitude. En effet, l'ensemble des points tels que la série entière converge en ces points sur le cercle d'incertitude est quelconque: il peut être vide ou correspondre au cercle entier ou être un quelconque sous-ensemble du disque. 

\begin{ex}[Étude du cercle d'incertitude pour certaines séries entière \pcite{Hauchecorne}]
	Nous étudions la convergence sur le cercle d'incertitude de certaine séries entières.
	\begin{itemize}
		\item Série entière qui diverge sur tout son cercle d'incertitude : $\sum z^n$ soit $a_n =1 \forall n \in \N$. Son rayon de convergence est $1$ car $r=1$ induit que la suite $a_nr^n = 1$ est bornée. Pour $z \in \C$ tel que $\left|z\right| = 1$, la série diverge \textblue{(c'est une somme infinie de $1$)}. Donc la série entière diverge bien sur tout son cercle d'incertitude.
		\item Série entière qui converge sur tout son cercle d'incertitude: $\sum \frac{1}{n^2}z^n$. Son rayon de convergence est $1$ car $r=1$ induit que la suite $\frac{1}{n^2}r^n = \frac{1}{n^2}$ est bornée \textblue{(La suite $\left(\frac{1}{n^2}\right)_{n \in \N}$ est décroissante (strictement) à partir de $n=1$)}. Pour $z \in \C$ tel que $\left|z\right| = 1$, la série entière vaut $\sum \frac{1}{n^2}$ qui converge \textblue{(critère de Riemman)}. Donc la série entière converge bien sur tout son cercle d'incertitude.
		\item Série entière qui converge sur certain point du cercle d'incertitude et diverge en d'autres: $\sum \frac{1}{n}z^n$. Son rayon de convergence est $1$ car $r=1$ induit que la suite $\frac{1}{n}r^n = \frac{1}{n}$ est bornée \textblue{(La suite $\left(\frac{1}{n}\right)_{n \in \N}$ est décroissante (strictement) à partir de $n=1$)}. Soit $z =1$ alors $\left|z\right| = 1$, la série entière vaut $\sum \frac{1}{n}$ qui diverge \textblue{(série harmonique)}.  Soit $z =-1$ alors $\left|z\right| = 1$, la série entière vaut $\sum \frac{(-1)^n}{n}$ qui converge \textblue{(critère des séries alternées)}. Donc la série entière converge en certains point de son cercle d'incertitude et diverge en d'autres.
	\end{itemize}
\end{ex}

\subparagraph{Opération sur les séries entières} Les séries formelles peuvent s'additionner ou se multiplier sans problème. La difficulté pour les séries entières va être la définition de la nouvelle série entière là ou la convergence veut dire quelque chose: il nous faut donc redéfinir le rayon de convergence de ces séries \cite[p.479]{Marco-Thieullen-Weil}.

\begin{prop}[Somme de deux séries entières]
	Soient $\sum a_n z^n$ et $\sum b_n z^n$ deux séries entières de rayons de convergence respectifs $R_a$ et $R_b$. Si $R$ est le rayon de convergence de la somme des deux séries alors:
	\begin{itemize}
		\item $\min\left(R_a, R_b\right) \leq R$;
		\item si $R_a \neq R_b$, alors $R = \min\left(R_a, R_b\right)$.
	\end{itemize}
\end{prop}
{\small{\begin{proof}
	\begin{enumerate}
		\item Si $\min\left(R_a, R_b\right) = 0$ alors $\min\left(R_a, R_b\right) \leq R$ (\textblue{le rayon de convergence est un réel positif}). Supposons maintenant que $\min\left(R_a, R_b\right) \neq 0$. Soit $r \in \left[0, \min\left(R_a, R_b\right)\right[$ (\textblue{existe car l'intervalle est non vide, par hypothèse}), alors les deux séries $\sum a_n r^n$ et $\sum b_n r^n$ sont absolument convergente (\textblue{lemme d'Abel puisque $r$ est inférieur à leur rayon de convergence respectif}). En particulier, leur somme est absolument convergente. Cela entraîne que $r \leq R$ où $R$ est le rayon de convergence de cette somme (\textblue{lemme d'Abel}). Comme $r$ est quelconque dans $\left[0, \min\left(R_a, R_b\right)\right[$, on en déduit que $\min\left(R_a, R_b\right) \leq R$.
		
		\item Supposons maintenant que $R_a \neq R_b$. Sans perte de généralité, on peut supposer que $R_a < R_b$. Soit $r \in \left]R_a, R_b\right[$, la série $\sum a_n r^n$ diverge et $\sum b_n r^n$ converge (\textblue{caractérisation du rayon de convergence}). Leur somme ne peut alors que diverger, ce qui entraîne que $r \geq R$. Par l'inégalité du point 1, on en conclut que $R = \min\left(R_a, R_b\right)$.
		\end{enumerate}
\end{proof}}}

\Attention, dans le cas où les rayons sont égaux on ne peut pas affirmer que $R = R_a$. Cela justifie le premier point de la proposition que nous ne pouvons pas raffiner.

\begin{cex}
	Soient $\sum 2^nz^n$ et $\sum \left(1-2^n\right)z^n$ deux séries entières de rayons de convergence $\frac{1}{2}$ (\textblue{en effet, si $\left|z\right| < \frac{1}{2}$, la série $\sum \left|\left(1-2^n\right)z^n\right| \leq \sum \left|\left(1-2^n\right)\right|$ converge et la série $\sum \left|2^nz^n\right|$ converge car c'est une série géométrique de raison $< 1$. De plus si $\left|z\right| > \frac{1}{2}$, la série $\sum \left|\left(1-2^n\right)z^n\right| \leq \sum \left|1\right|$ diverge (série harmonique) et la série $\sum \left|2^nz^n\right|$ diverge car c'est une série géométrique de raison $> 1$.}). Mais en les additionnant, on obtient $\sum z^n$ dont le rayon de convergence est $1$ (\textblue{voir exemple plus haut}).
\end{cex}

\begin{definition}[Produit de Cauchy]
	Soient $\sum a_nz^n$ et $\sum b_nz^n$ deux séries entières. Le produit de Cauchy de ces deux séries est la série de terme général $\left(a_0b_n + a_1b_{n-1} + \dots + a_nb_0\right)z^n$ qui est une série entière.
\end{definition}

\begin{prop}
	Soient $\sum a_nz^n$ et $\sum b_nz^n$ deux séries entières de rayon de convergence respectif $R_a$ et $R_b$. Si $R$ est le rayon de convergence du produit de Cauchy de ces deux séries, alors $\min\left(R_a, R_b\right) \leq R$.
\end{prop}
{\small{\begin{proof}
	On raisonne de manière analogue au cas de l'addition. Si $\min\left(R_a, R_b\right) = 0$ alors $\min\left(R_a, R_b\right) \leq R$ (\textblue{le rayon de convergence est un réel positif}). Supposons maintenant que $\min\left(R_a, R_b\right) \neq 0$. Soit $r \in \left[0, \min\left(R_a, R_b\right)\right[$ (\textblue{existe car l'intervalle est non vide, par hypothèse}), alors les deux séries $\sum a_n r^n$ et $\sum b_n r^n$ sont absolument convergente (\textblue{lemme d'Abel puisque $r$ est inférieur à leur rayon de convergence respectif}). En particulier, leur produit de Cauchy est absolument convergente. Cela entraîne que $r \leq R$ où $R$ est le rayon de convergence de cette somme (\textblue{lemme d'Abel}). Comme $r$ est quelconque dans $\left[0, \min\left(R_a, R_b\right)\right[$, on en déduit que $\min\left(R_a, R_b\right) \leq R$.
\end{proof}}}

\Attention, même dans le cas où $R_a$ et $R_b$ sont différents, nous ne pouvons rien dire de plus sur le rayon de convergence de la série produit.

\begin{cex}[Rayon de convergence d'un produit de deux séries entières strictement plus grand que chacun des rayons de convergences des deux séries \pcite{Hauchecorne}]
	Soient les séries entières $\sum a_nz^n$ et $\sum b_nz^n$ où 
	\[a_n = \left \{ \begin{array}{l @{\text{ si }} l} 
	2 & n=0 \\
	2^n & n \geq 1 \\
	\end{array} \right. \text{ et } b_n = \left \{ \begin{array}{c @{\text{ si }} l} 
	-1 & n=0 \\
	1 & n \geq 1\\
	\end{array} \right. \]
	Le produit de Cauchy de ces deux suites donne la série entière $\sum w_nz^n = -2$ car $w_0 = -2$ et $w_n =0$. De plus, le rayon de convergence de  $\sum a_nz^n$ est $\frac{1}{2}$ (\textblue{la suite $\left(2^{n}\left(\frac{1}{2}\right)^{n}\right)_{n \in \N^{*}}$ est bornée car vaut $1$ et $\left(2^{n}r^{n}\right)_{n \in \N^{*}}$ avec $r > \frac{1}{2}$ n'est pas bornée car tend vers $+\infty$}), celui de $\sum b_nz^n$ est $1$ (\textblue{voir exemple précédent}), alors que le rayon de convergence de $\sum w_nz^n$ est l'infini (\textblue{c'est une constante}).
\end{cex}

\subparagraph{Calcul d'un rayon de convergence} La partie technique lors de l'étude d'une série entière est de déterminer le rayon de convergence de la série. Il existe quelques outils (plus ou moins puissant) nous permettant de réaliser cette étude. 

\begin{ex}[Calcul de rayon de convergence grâce à la définition ou par sa caractérisation \pcite{ArnaudiesFraysse-3}]
	On utilise la définition ou sa caractérisation pour exhiber le rayon de convergence de ces trois séries.
	\begin{itemize}
		\item Étude de la série entière $\sum n^nz^n$. Soit $z \neq 0$, alors $n^nz^n \underset{n \to \infty}{\nrightarrow} 0$. La série $\sum n^nz^n$ diverge alors pour tout $z \in \C^{*}$. Donc $R =0$.
		\item Étude de la série entière $\sum \frac{z^n}{n^n}$. Soit $z \in \C$, alors $\left(\frac{\left|z\right|^n}{n^n}\right)^{\frac{1}{n}} = \frac{\left|z\right|}{n} \underset{n \to \infty}{\rightarrow} 0$. La série $\sum \frac{z^n}{n^n}$ converge absolument alors pour tout $z \in \C$. Donc $R = + \infty$.
		\item Étude de la série entière $\sum \left(\cos n\theta\right)z^n$. On devine que le rayon de convergence de cette série est $1$. Soit $z \in \C$ tel que $\left|z\right| < 1$, alors la série $\sum \left(\cos n\theta\right)z^n$ converge absolument (\textblue{en effet, $\forall n \in \N$, $\left|\cos n\theta\right| \leq 1$ d'où $\forall n \in \N$, $\left|z^n\cos n\theta\right| \leq \left|z^n\right|$ est le terme général d'une série convergente (série géométrique)}). On en conclut que $R \geq 1$.
		
		Soit maintenant $z = 1$, alors la suite numérique $\sum \cos n \theta$ diverge car $\cos n\theta \underset{n \to \infty}{\nrightarrow} 0$ (\textblue{en effet, dans le cas contraire, on aurait que $\cos 2n\theta = 2\cos^2 n\theta - 1 \underset{n \to \infty}{\rightarrow} 1$, ce qui donne une contradiction à $\cos 2n\theta \underset{n \to \infty}{\nrightarrow} 0$}). Ainsi la série entière diverge au point $1$, d'où $R \leq 1$. On en conclut que $R = 1$ ce qui était annoncé.
	\end{itemize}
\end{ex}

La définition et ce résultat sont souvent suffisant pour déterminer le rayon de convergence d'une série entière. Lorsque cela n'est plus le cas, on utilise d'autres outils un peu plus puissant.

\begin{prop}[Règle de d'Alembert \protect{\cite[p.475]{Marco-Thieullen-Weil}}]
	Soient $\sum a_n z^n$ une série entière dont les coefficients sont non nuls à partir d'un certain rang. Si la limite du rapport $\frac{\left|a_{n+1}\right|}{\left|a_n\right|}$ existe dans $\R_+ \cup \{+\infty\}$, alors le rayon de convergence de la série $\sum a_nz^n$ est égal à 
	\begin{displaymath}
	\frac{1}{\underset{n \to +\infty}{\lim} \frac{\left|a_{n+1}\right|}{\left|a_n\right|}}
	\end{displaymath}
\end{prop}
{\small{\begin{proof}
	Soient $\sum a_n z^n$ une série entière dont les coefficients sont non nuls à partir d'un certain rang. Supposons que la limite du rapport $\frac{\left|a_{n+1}\right|}{\left|a_n\right|}$ existe dans $\R_+ \cup \{+\infty\}$ quand $n$ tend vers $+ \infty$, nous la notons $L$.
	
	Soient $z \in \C$ non nul,et $N \in \N$ tel que $\forall n > N$, $a_n$ soit non nul. Alors, $\forall n \geq N$, 
	\begin{displaymath}
	\frac{\left|a_{n+1}z^{n+1}\right|}{\left|a_n z^n\right|} =\frac{\left|a_{n+1}\right|}{\left|a_n\right|}z
	\end{displaymath}
	
	Maintenant si on applique la règle de D'Alembert à la série (\textblue{numérique}) $\sum \left|a_nz^n\right|$, on obtient les deux cas suivants:
	\begin{itemize}
		\item $\forall z$ tel que $\left|z\right| < \frac{1}{L}$, la série $\sum a_n z^n$ est absolument convergente;
		\item $\forall z$ tel que $\left|z\right| > \frac{1}{L}$, la série $\sum a_n z^n$ n'est pas absolument convergente.
	\end{itemize}
	Par les propriétés sur le rayon de convergence d'une série entière, on obtient que $\frac{1}{L}$ est le rayon de convergence de la série $\sum a_n z^n$.
\end{proof}}}

\Attention, l'ordre des coefficient lors de la division est essentiel: le rayon de convergence (lorsqu'on peut le définir) vaut $\frac{1}{\underset{n \to +\infty}{\lim} \frac{\left|a_{n+1}\right|}{\left|a_n\right|}}$ ou $\underset{n \to +\infty}{\lim} \frac{\left|a_{n}\right|}{\left|a_{n+1}\right|}$

\begin{ex}[Utilisation de la règle de D'Alembert \protect{\cite[p.475]{Marco-Thieullen-Weil}}]
	Déterminons le rayon de convergence de la série $\sum_{n \geq 1} \frac{z^n}{n^{\alpha}}$ où $\alpha$ est un réel. Pour appliquer le critère de d'Alembert, on calcul:
	\begin{displaymath}
	\lim_{n \to \infty} \frac{n^{\alpha}}{(n+1)^{\alpha}} = \lim_{n \to \infty} \left(\frac{n}{n+1}\right)^{\alpha} = 1
	\end{displaymath}
	Nous en déduisons que $1$ est le rayon de convergence de la série $\sum_{n \geq 1} \frac{z^n}{n^{\alpha}}$.
\end{ex}

Dans le cas où la série entière $\sum a_n z^n$ possède une infinité de coefficients nuls, la règle de d'Alembert tel que présenté dans la proposition précédente ne peut s'appliquer. Cependant, utiliser le critère de d'Alembert d'une série numérique sur la série $\sum \left|a_n z^n\right|$ peut nous permettre de calculer le rayon de convergence de cette dernière.

\begin{ex}[Application de la règle de d'Alembert avec une infinité de coefficients nuls \protect{\cite[p.475]{Marco-Thieullen-Weil}}]
	Déterminons le rayon de convergence de la série entière $\sum_{n \geq 0} \frac{z^{n^2}}{n+1}$. Cette série a pour coefficient la suite $\left(a_n\right)_{n \in \N}$ définie par 
	\begin{displaymath}
	a_n = \left \{ \begin{array}{cl}
	\frac{1}{\sqrt{n} + 1} & \text{si } n \text{ est un carré d'un entier} \\
	0 & \text{sinon} \\
	\end{array}
	\right .
	\end{displaymath}
	Appliquons maintenant la règle d'Alembert à la série numérique $\sum_{n \geq 0} \frac{\left|z\right|^{n^2}}{n+1}$. Pour $z$ non nul, nous avons
	\begin{displaymath}
	\frac{\frac{\left|z\right|^{(n+1)^2}}{n+2}}{\frac{\left|z\right|^{n^2}}{n+1}} = \frac{n+1}{n + 2}\left|z\right|^{2n+1}
	\end{displaymath}
	Si $\left|z\right| < 1$, on obtient que 
	\begin{displaymath}
	\lim_{n \to + \infty} \frac{\frac{\left|z\right|^{(n+1)^2}}{n+2}}{\frac{\left|z\right|^{n^2}}{n+1}} = 0
	\end{displaymath}
	Dans ce cas, nous en déduisons la convergence absolue de la série $\sum_{n \geq 0} \frac{z^{n^2}}{n+1}$.
	
	Si $\left|z\right| > 1$, on obtient que 
	\begin{displaymath}
	\lim_{n \to + \infty} \frac{\frac{\left|z\right|^{(n+1)^2}}{n+2}}{\frac{\left|z\right|^{n^2}}{n+1}} = + \infty
	\end{displaymath}
	Dans ce cas, nous en déduisons la divergence de la série $\sum_{n \geq 0} \frac{z^{n^2}}{n+1}$.
	
	Le rayon de convergence de la série entière $\left|z\right| < 1$, on obtient que 
	\begin{displaymath}
	\lim_{n \to + \infty} \frac{\frac{\left|z\right|^{(n+1)^2}}{n+2}}{\frac{\left|z\right|^{n^2}}{n+1}} = 0
	\end{displaymath}
	Dans ce cas, nous en déduisons la convergence absolue de la série $\sum_{n \geq 0} \frac{z^{n^2}}{n+1}$ est alors $1$.
\end{ex}

\begin{ex}[Utilisation de la règle de d'Alembert dans le cas d'une série quelconque \protect{\cite[p.475]{Marco-Thieullen-Weil}}]
	Soit $\sum_{n \geq 0}$ une série entière telle que la suite $\left(a_n\right)_{n \in \N}$ a une limite finie non nulle. Alors par le critère de d'Alembert, comme on a
	\begin{displaymath}
	\frac{a_{n+1}}{a_n} \to 1
	\end{displaymath}
	le rayon de convergence de cette série est $1$.
\end{ex}

Lorsque le critère de d'Alembert est inefficace (\textblue{le rapport que l'on considère ne possède pas de limite}), on fait appelle au critère de Cauchy.

\begin{prop}[Règle de Cauchy \protect{\cite[p.476]{Marco-Thieullen-Weil}}]
	Soient $\sum a_n z^n$ une série entière. Si la limite de la suite de terme général $\sqrt[n]{\left|a_n\right|}$ existe dans $\R_+ \cup \{+\infty\}$ quand $n$ tend vers $+\infty$, alors le rayon de convergence de la série $\sum a_nz^n$ est égal à 
	\begin{displaymath}
	\frac{1}{\underset{n \to +\infty}{\lim} \sqrt[n]{\left|a_n\right|}}
	\end{displaymath}
\end{prop}
{\small{\begin{proof}
	Soient $\sum a_n z^n$ une série entière dont les coefficients sont non nuls à partir d'un certain rang. Supposons que la limite de la suite de terme général $\sqrt[n]{\left|a_n\right|}$ existe dans $\R_+ \cup \{+\infty\}$ quand $n$ tend vers $+ \infty$, nous la notons $L$.
	
	Pour tout $r \geq 0$ et $n \in \N$, nous avons 
	\begin{displaymath}
	\\sqrt[n]{\left|a_nr^n\right|} = r\sqrt[n]{\left|a_n\right|}
	\end{displaymath}
	
	Maintenant si on applique la règle de Cauchy à la série (\textblue{numérique}) $\sum \left|a_nz^n\right|$, on obtient les deux cas suivants:
	\begin{itemize}
		\item $\forall z$ tel que $\left|z\right| < \frac{1}{L}$, la série $\sum a_n z^n$ est absolument convergente;
		\item $\forall z$ tel que $\left|z\right| > \frac{1}{L}$, la série $\sum a_n z^n$ n'est pas absolument convergente.
	\end{itemize}
	Par les propriétés sur le rayon de convergence d'une série entière, on obtient que $\frac{1}{L}$ est le rayon de convergence de la série $\sum a_n z^n$.
\end{proof}}}

\begin{ex}[Utilisation de la règle de Cauchy \protect{\cite[p.475]{Marco-Thieullen-Weil}}]
	Déterminons le rayon de convergence de la série entière $\sum_{n \geq 1} \frac{z^n}{n^{\ln n}}$. Nous avons
	\begin{displaymath}
	\sqrt[n]{\frac{1}{n^{\ln n}}} = \frac{1}{n^{\frac{\ln n}{n}}}
	\end{displaymath}
	Comme $n^{\frac{\ln n}{n}}$ tend vers $1$, lorsque $n$ tend vers l'infini, on peut en déduire que 
	\begin{displaymath}
	\lim_{n \to +\infty} \sqrt[n]{\frac{1}{n^{\ln n}}} = 1
	\end{displaymath}
	Par la règle de Cauchy, on en déduit que $1$ est le rayon de convergence de la série entière $\sum_{n \geq 1} \frac{z^n}{n^{\ln n}}$.
\end{ex}

\begin{req}
	On peut montrer que, pour une série entière donnée, si la règle de d'Alembert s'applique alors la règle de Cauchy s'applique \cite[p.237]{Gourdon-analyse}. (\textblue{En effet, on a pour tout $a_n > 0$, 
	\begin{displaymath}
	\left(\lim_{n \to +\infty} \frac{a_{n+1}}{a_n} = \lambda\right) \Rightarrow \left(\lim_{n \to +\infty} \sqrt[n]{a_n} = \lambda\right)
	\end{displaymath}
	si $\frac{a_{n+1}}{a_n} \to \lambda > 0$, alors par la continuité de la fonction $\ln$, $\alpha_n = \ln a_{n+1} - a_n$ tend vers $\ln \lambda$, et par le lemme de Cesàro,
	\begin{displaymath}
	\frac{\alpha_0 + \dots + \alpha_{n}}{n} = \frac{\ln a_n - \ln a_0}{n}
	\end{displaymath}
	tend aussi vers $\ln \lambda$, ce qui nous donne que $\sqrt[n]{a_n}$ tend aussi vers $\lambda$. Si maintenant, $\lambda = 0$, la suite $\alpha_n = \ln a_{n+1} - a_n$ tend vers $-\infty$ et le lemme généralisé au cas de la limite infinie, nous donne encore que $\ln a_n /n$ tend vers $-\infty$ ce qui implique que $\sqrt[n]{a_n}$ tend vers $0 = \lambda$ (en passant à l'exponentielle).} \cite[p.395]{Marco-Thieullen-Weil}) \Attention, la réciproque est fausse (\textblue{En effet, on peut déterminer le rayon de convergence de $\sum_{n\geq 0} 2+(-1)^n z^n$ grâce au critère de Cauchy, puisque la série $\sqrt[n]{2+(-1)^n} \underset{n \to +\infty}{\rightarrow} 1$ mais que la série $\frac{2+(-1)^{n+1}}{2+(-1)^n}$ ne converge pas.} \cite[p.206]{Gourdon-analyse}).
\end{req}

L'existence de la limite de la suite est argument contraignant qui empêche l'utilisation généralisé du critère de Cauchy. Nous allons le généralisé de manière à pouvoir appliquer ce nouveau critère à toutes les séries entières: ce critère s'appelle aussi le critère de Cauchy--Hadamard.

\begin{prop}[Critère d'Hadamard \protect{\cite[p.4775]{Marco-Thieullen-Weil}}]
	Le rayon de convergence de toute série entière $\sum_{n \geq 0} a_n z^n$ est égal à
	\begin{displaymath}
	\frac{1}{\limsup_{n \to +\infty} \sqrt[n]{\left|a_n\right|}}
	\end{displaymath}
\end{prop}
{\small{\begin{proof}
	Soit $\sum_{n \geq 0} a_n z^n$ une série entière de rayon de convergence $R$. Supposons que $R$ est nul: alors pour tout $r > 0$, la suite de termes général $\left|a_n\right|r^n$ n'est pas bornée (\textblue{critère sur le rayon de convergence d'une série entière}). On peut donc trouver, pour chaque réel $r > 0$, une infinité d'entiers $n$ tels que $1 < \left|a_n\right|r^n$. D'où, on en déduit que, $\frac{1}{r} < \sqrt[n]{\left|a_n\right|}$. Cela entraîne que $\limsup_{n \to +\infty} \sqrt[n]{\left|a_n\right|} = +\infty = \frac{1}{0} = \frac{1}{R}$. D'où le résultat.
	
	Supposons maintenant que $R$ est non nul, peut être éventuellement infini, et fixons un réel $e$ dans l'intervalle $\left] 0, R \right[$. Comme la suite de terme général $\left|a_n\right|r^n$ tend vers $0$, on peut trouver un entier $N \in \N$ tel que, si $n \geq N$, alors $\left|a_n\right|r^n \leq 1$. En prenant la racine $n$-ième dans l'inégalité précédente, on obtient $\sqrt[n]{\left|a_n\right|}r \leq 1$. Le passage dans cette inégalité à la limite supérieure nous permet d'écrire $\limsup_{n \to +\infty}\sqrt[n]{\left|a_n\right|}r \leq 1$. D'où
	\begin{displaymath}
	\limsup_{n \to +\infty}\sqrt[n]{\left|a_n\right|} \leq \frac{1}{r} \underbrace{\leq}_{\underset{\text{quelconque}}{r \in \left] 0, R \right[}} \frac{1}{R}
	\end{displaymath}
	Montrons maintenant l'inégalité inverse. On raisonne par l'absurde en supposant que $\limsup_{n \to +\infty}\sqrt[n]{\left|a_n\right|} < \frac{1}{R}$. Il existe alors un réel $r > 0$ tel que
	\begin{displaymath}
	\limsup_{n \to +\infty}\sqrt[n]{\left|a_n\right|} < \frac{1}{r} < \frac{1}{R}
	\end{displaymath}
	Par définition de la limite supérieure d'une suite, on peut trouver un entier $N \in \N$ tel que si $n \geq N$, alors $\sqrt[n]{\left|a_n\right|} < \frac{1}{r}$. En élevant à la puissance $n$ les membres de l'inégalité précédente, on obtient $\left|a_n\right| < \frac{1}{r^n}$, c'est-à-dire $\left|a_n\right|r^n < 1$. Nous avons prouvé que la suite $\left(a_nr^n\right)_{n \in \N}$ est borné. Ce qui entraîne $r \leq R$, d'où la contradiction.
\end{proof}}}

\begin{ex}[Utilisation de la règle d'Hadamard]
	Montrons à l'aide de la règle d'Hadamard que $1$ est le rayon de convergence de la série entière $\sum_{n\geq 0}n^nz^{n^2}$. Cette série a pour coefficient la suite $\left(a_n\right)_{n \in \N}$ définie par 
	\begin{displaymath}
	a_n = \left \{ \begin{array}{cl}
	\sqrt{n}^{\sqrt{n}} & \text{si } n \text{ est un carré d'un entier} \\
	0 & \text{sinon} \\
	\end{array}
	\right .
	\end{displaymath}
	Nous en déduisons que 
	\begin{displaymath}
	\sqrt[n]{a_n} = \left \{ \begin{array}{cl}
	\sqrt{n}^{\frac{1}{\sqrt{n}}} & \text{si } n \text{ est un carré d'un entier} \\
	0 & \text{sinon} \\
	\end{array}
	\right .
	\end{displaymath}
	On peut vérifier facilement que $0$ et $1$ sont les seules valeurs d'adhérence de la suite de terme général $\sqrt[n]{a_n}$. On en déduit que $\limsup_{n \to +\infty}\sqrt[n]{a_n} = 1$. D'où le résultat sur le rayon de convergence.
\end{ex}

Les opérations sur les séries entières et leurs répercutions sur les rayons de convergence nous permet d'obtenir des résultats intéressant pour calculer les rayons de convergences de nouvelles séries à partir de séries usuelles. Pour cela, nous avons besoin d'introduire la notion de série dérivée, nous verrons plus loin (dans la prochaine propriété et lors de l'étude de la fonction série entière) que cette série est bien définie et que nous pouvons la définir comme nous venons de le faire.

\begin{definition}[Série entière dérivée]
	Soit $\sum a_n z^n$ une série entière. Nous appelons série entière dérivée la série entière définie telle que $\sum na_n z^n$.
\end{definition}

\begin{prop}[Rayon de convergence et opérations \protect{\cite[p.24]{ArnaudiesFraysse-3}}]
	Soient $\sum_{n \geq 0} a_n z^n$ et $\sum_{n \geq 0} b_n z^n$ deux séries entières de rayons de convergence respectifs $R_a$ et $R_b$. Alors,
	\begin{enumerate}
		\item Si $\forall n$, $\left|a_n\right| \leq \left|b_n\right|$, alors $R_a \geq R_b$;
		\item Si $\sum_{n \geq 0} a_n z^n -\sum_{n \geq 0} b_n z^n$ existe (au sens de la série formelle), alors $R_a = R_b$;
		\item Les séries $\sum_{n \in \N}a_n z^n$ et $\sum_{n \in \N}a_n z^{n-p}$ ont le même rayon de convergence;
		\item Si $a_n = \lambda_n b_n$, avec $\lambda_n \underset{n \to \infty}{\in} O\left(n^{\alpha}\right)$, pour $\alpha \in \R$, alors $R_a \geq R_b$.
		\item La série dérivée de $\sum a_n z^n$ possède le même rayon de convergence que $\sum a_n z^n$.
	\end{enumerate}
\end{prop}
{\small{\begin{proof}(Application du lemme d'Abel)
	Soient $\sum_{n \geq 0} a_n z^n$ et $\sum_{n \geq 0} b_n z^n$ deux séries entières de rayons de convergence respectifs $R_a$ et $R_b$.
	\begin{enumerate}
		\item Si $z \in \C$ vérifie $\left|z\right|< R_b$, alors ma série $\sum \left|b_n\right|\left|z\right|^n$ converge, d'où par comparaison la série $\sum \left|a_n\right|\left|z\right|^n$ converge. Le lemme d'Abel nous permet de conclure.
		\item Pour $z \in \C$, les séries $\sum a_nz^n$ et $\sum b_nz^n$ ont la même nature (converge ou diverge en même temps) car la série $\left(\left(a_n - b_n\right)z^n\right)_{n \in \N}$ est stationnaire. Le lemme d'Abel nous permet de conclure.
		\item Soit $z \in \C^{*}$. Les séries $\sum_{n \in \N}a_n z^n$ et $\sum_{n \in \N}a_n z^{n-p}$ converge ou diverge en même temps puisque les suites $\left(a_n z^n\right)_{n \in \N}$ et $\left(a_n z^{n - p}\right)_{n \in \N}$ sont proportionnelles. On conclut avec le lemme d'Abel.
		\item Soit $z \in \C$ tel que $\left|z\right| < R_b$. Soit $A \in R_+$ tel que $\forall n, \left|\lambda_n\right| \leq An^{\alpha}$. Fixons $r \in \left]\left|z\right|, R_b\right[$, la série $\sum \left|b_n\right|r^n$ converge et par conséquent la suite $\left(\left|b_n\right|r^n\right)_{n \in \N}$ est bornée. On a $\forall n$ que:	
		\begin{displaymath}
		\begin{array}{ccll}
		\left|a_n\right|\left|z\right|^n & = & \left|\lambda_n b_n\right|\left|z\right|^n & \textblue{\text{definition de }a_n} \\
		& = & \left|\lambda_nb_n\right|r^n \left(\frac{\left|z\right|}{r}\right)^n & \textblue{\text{introductoin de }r \neq 0} \\
		& \leq & A\left|b_n\right|r^n n^{\alpha}\left(\frac{\left|z\right|}{r}\right)^n & \textblue{\text{hypothèse sur }\lambda_n} \\
		& \leq & B n^{\alpha}\left(\frac{\left|z\right|}{r}\right)^n & \textblue{\text{où } B \text{ est } A \text{ fois la borne de la suite } \left(\left|b_n\right|r^n\right)_{n \in \N}} \\
		\end{array}
		\end{displaymath}
		En appliquant la règle de d'Alembert pour les séries numérique, on trouve que la série $\sum n^{\alpha}\left(\frac{\left|z\right|}{r}\right)^n$ converge car $\frac{\left|z\right|}{r} < 1$. On en déduit que, (\textblue{par convergence comparée}), la série $\sum \left|a_n\right|\left|z\right|^n$ converge également. Le lemme d'Abel nous permet de conclure.
		\item D'après le points 3 et 4. La dérivation est l'application du point 3 avec $p=1$ et un facteur $n$ qui sort qu'on contrôle grâce au point 4.
	\end{enumerate}
\end{proof}}}

\begin{ex}[Utilisation des opérations sur le rayon de convergence]
	Soit $\theta \in \R$ et $\alpha \in \R$. Donnons le rayon de convergence de la série entière $\sum_{n \in \N} \frac{\cos n\theta}{n^{\alpha}}z^n$. Une double application de la proposition précédente (item 4) montre que cette série a le même rayon de convergence que la série $\sum_{n \in \N} \cos n\theta z^n$ dont nous avons donner son rayon de convergence dans un exemple précédent. Donc le rayon de convergence de $\sum_{n \in \N} \frac{\cos n\theta}{n^{\alpha}}z^n$ est $1$.
\end{ex}
