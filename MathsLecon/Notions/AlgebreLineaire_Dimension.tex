%LECONs : 151 (Dimension et rang)
Nous allons donner les résultats fondamentaux de la théorie de la dimension \cite[p.17]{Grifone}.

\begin{theo}
	Dans un espace vectoriel $E$ sur $K$ de dimension finie, toutes les bases ont le même nombre d'éléments. Ce nombre est appelé dimension de $E$ sur $K$ et est noté $\dim_K E$.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		\begin{lemme}
			Dans un espace vectoriel vectoriel par $n$ éléments, toute famille contenant plus de $n$ éléments est liée.
		\end{lemme}
		\begin{proof}
			Soit $F =\{v_1, \dots, v_n\}$ une famille génératrice et $F' = \{w_1, \dots, w_m\}$ une famille de $m > n$ vecteurs. Montrons que $F'$ est lié.
			\begin{itemize}
				\item Si l'un des $w_i = 0$, $F'$ est liée. Supposons que $F'$ ne contient aucun vecteur nul. Comme $F$ est génératrice : $w_1 = a_1v_1 + \dots + a_nv_n$ avec un $a_i \not 0$ \textblue{($w_1 \not 0$)}. On peut alors, exprimer $v_1$ comme combinaison linéaire de $w_1, v_2, \dots, v_n$ \textblue{(quitte à renuméroter)}. Donc la famille $\{w_1, v_2, \dots, v_n\}$ est génératrice.
				\item Par récurrence, on va alors replacer tous les vecteurs de $F$ par des vecteurs de $F'$ en conservant le caractère générateur de $F$ \textblue{(on applique la même méthode)}.
				\item La famille $\{w_1, \dots, w_n\}$ est génératrice, $w_{n+1} \notin \{w_1, \dots, w_n\}$ s'écrit donc comme combinaison linéaire des $\{w_1, \dots, w_n\}$. La famille $\{w_1, \dots, w_{n+1}\}$ est donc lié et $F'$ contient une famille liée: $F'$ est liée.
			\end{itemize}
		\end{proof}
		Soient $B$ et $B'$ deux bases, si $\mathrm{card}(B) < \mathrm{card}(B')$ alors $B'$ non libre par le lemme (contradiction). De manière symétrique, on ne peut pas avoir $\mathrm{card}(B) > \mathrm{card}(B')$. D'où l'égalité.
	\end{proof}
\end{footnotesize}

\begin{cor}
	\begin{enumerate}
		\item Dans un espace vectoriel de dimension $n$, toute famille ayant plus de $n$ éléments est liée.
		\item Dans un espace vectoriel de dimension $n$, toute famille ayant moins de $n$ éléments ne peut pas être génératrice.
	\end{enumerate}
\end{cor}
\begin{footnotesize}
	\begin{proof}
		\begin{enumerate}
			\item Lemme dans la preuve du théorème.
			\item Par l'absurde: on pourrait extraire une base de taille $< n$ d'une base (qui est famille génératrice de taille $n$).
		\end{enumerate}
	\end{proof}
\end{footnotesize}

\begin{appli}
	Preuve par récurrence sur la dimension: algorithme de Berlekamp; réduction des automorphisme auto-adjoints.
\end{appli}

\begin{prop}
	Soient $E_1, \dots, E_p$ des $K$-espaces vectoriels de dimensions finies. Alors: $\dim(E_1 \times \dots \times E_p) = \dim(E_1) + \dots + \dim(E_p)$.
\end{prop}
\begin{footnotesize}
	\begin{proof}
		La base est formée des vecteurs contenant unique une composante non nul correspondant à un élément de la base de l'espace vectoriel à cet position.
	\end{proof}
\end{footnotesize}

\begin{theo}
	Soit $E$ un espace vectoriel de dimension $n$.
	\begin{enumerate}
		\item Toute famille génératrice ayant $n$ éléments est une base.
		\item Toute famille libre ayant $n$ éléments est une base.
	\end{enumerate}
\end{theo}
\begin{footnotesize}
	\begin{proof}
		\begin{enumerate}
			\item On réalise une extraction de base dans cette famille. Comme il nous faut $n$ élément, la base est cette famille.
			\item On applique le théorème de la base incomplète. Comme on a déjà $n$ éléments, on a pas besoin d'en rajouter.
		\end{enumerate}
	\end{proof}
\end{footnotesize}

\begin{prop}
	Soit $E$ un espace vectoriel de dimension finie et  $F$ un sous-espace vectoriel de $E$. Alors $F$ est de dimension finie, et de plus:
	\begin{enumerate}
		\item $\dim_K F \leq \dim_K E$
		\item $\dim_K F = \dim_K E$ si et seulement si $F = E$.
	\end{enumerate}
\end{prop}
\begin{footnotesize}
	\begin{proof}
		On montre que $F$ est de dimension finie par l'absurde: pas le cas existe une famille libre à $k > n$ éléments. Contradiction car cette famille devrait être libre dans $E$, ce qui est impossible.
		\begin{enumerate}
			\item Raisonnons comme dans le cas du théorème d'existence. On obtient $L_k$ qui si elle est génératrice et $k < n$ par ce qui précède. 
			\item Soit $B$ une base de $F$: elle a donc $n$ éléments. Elle est donc une base de $E$.
		\end{enumerate}
	\end{proof}
\end{footnotesize}

\begin{theo}[\protect{\cite[p.63]{Grifone}}]
	Deux espaces vectoriels de dimension finie sont isomorphes, si et seulement si, ils ont la même dimension.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		\begin{itemize}
			\item Si $f : E \to E'$ est un isomorphisme, l'image de $B$ une base de $E$ est $B'$ une base dans $E'$.
			\item On pose $f : E \to E'$ qui a l'élément $k$ de la base de $E$ l'envoie sur l'élément $k$ de celle de $E'$. Cette application est bien un isomorphisme.
		\end{itemize}
	\end{proof}
\end{footnotesize}


\begin{prop}[\protect{\cite[p.85]{Grifone}}]
	Soit $E$ et $E'$ deux espaces vectoriels.
	
	\begin{minipage}{.36\textwidth}
		\begin{enumerate}
			\item $\dim \mathcal{L}(E, F) = \dim E * \dim F$
			\item $\dim E = \dim E^*$
		\end{enumerate}
	\end{minipage} \hfill
	\begin{minipage}{.56\textwidth}
		\begin{footnotesize}
			\begin{proof}
				\begin{enumerate}
					\item On passe en représentation matricielle.
					\item $\dim_K E^* = \dim_K \mathcal{L}(E, K) = \dim_K E * \dim_K K = \dim_K E$.
				\end{enumerate}
			\end{proof}
		\end{footnotesize}
	\end{minipage}
\end{prop}
