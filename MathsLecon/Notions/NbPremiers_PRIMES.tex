%LECONS : 121 (Nombre premier)
Savoir si un nombre entier quelconque est un nombre premier ou non est un problème de décision qui a première vue n'est pas facile. Nous allons étudier sa complexité.

\begin{definition}[Problème \textsc{PRIMES}]
	On définit le problème de \textsc{PRIMES} qui permet de savoir si un entier est premier ou non.
		
	\noindent\begin{tabular}{rl}
		Problème & \textsc{PRIMES} \\
		\textbf{entrée} : & $n \in \N$  \\
		\textbf{sortie} : & Oui si $n$ est un nombre premier; non sinon \\
	\end{tabular}
\end{definition}

\begin{prop}
	Le problème \textsc{PRIMES} est dans $CO-NP$.
\end{prop}
{\footnotesize{\begin{proof}
	En effet si on nous donne un diviseur potentiel de $n$, on vérifie en temps linéaire que c'est bien un diviseur de $n$. De plus, comme ce diviseur est nécessairement inférieur à $n$ (voir même $\sqrt{n}$), on peut le deviner bits par bits en temps linéaire.
\end{proof}}}

Nous allons maintenant voir que le problème \textsc{PRIMES} est dans $NP$. Pour cela nous allons exhiber un certificat polynomial appelé certificat de Pratt. Ce certificat nous donne un test de primalité appelé le test de Lucas--Lehmer.
%AFINIR

\begin{cor}
	Le problème \textsc{PRIMES} est dans $NP$.
\end{cor}
{\footnotesize{\begin{proof}
	Donner par le test Lucas--Lehmer.
\end{proof}}}

Maintenant, nous allons voir qu'en réalité le problème \textsc{PRIMES} est dans $P$ \cite[p.46]{Hindry}: on va alors exhiber un test de primalité déterministe en temps polynomial qui n'admet aucune erreurs. L'idée principale de ce test est de réaliser ces tests dans $\Z[X]$. Notons tout de même que si \textsc{PRIMES} est dans $P$, cela n'implique pas qu'on puisse le calculer en un temps raisonnable. En effet, l'algorithme qui permet d'exhiber que \textsc{PRIMES} est dans $P$ a une complexité en $O\left(n^18\right)$ où $n$ est l'entier que l'on test. Il nous faut donc d'autres tests de primalité: admettant des cas d'erreurs mais d'une complexité bien plus raisonnable.

\begin{lemme}
	Soit $N$ un nombre premier et $h(X) \in \Z[X]$ un polynôme de degré $r$, alors $(X - a)^N \equiv X^N - a [(N, h(X))]$.
\end{lemme}

Dans un anneaux la notion $a \equiv b [I]$ signifie que $a - b \in I$ où $I$ est un idéal et que l'écriture $\left(a_1, \dots a_m\right)$ représente l'idéal engendré par ces éléments. Il nous faut alors montrer que si $r$ est en $O\left((\log N)^k\right)$, alors ce test reste polynomial. Le problème est de choisir les paires $a$, $h(X)$ de sorte qu'elles détectent la non primalité. La solution de Agrawal, Kayal et Saxena est de choisir $h(X) = X^r -1$ avec $r$ premier très bien choisi : $r = O\left((\log N)^k\right)$ et de montrer qu'il suffit de tester les $a \in \left[1, L\right]$ avec $L = O\left(\sqrt{r}\log N\right)$ pour s'assurer que $N$ est premier, ou éventuellement une puissance d'un nombre premier (ce qui n'est pas gênant).

\begin{lemme}
	Soit $Y > 1$ et $N \geq 2$ entiers. Il existe un nombre premier $r$ vérifiant:
	\begin{enumerate}
		\item l'ordre de $N$ modulo $r$ est au moins $Y$;
		\item $r =O\left(Y^2 \log N\right)$.
	\end{enumerate}
\end{lemme}
{\footnotesize{\begin{proof}
	Posons $A = \prod_{1 \leq y \leq Y} \left(N^y - 1\right)$. Soit $r$ le plus petit nombre premier ne divisant pas $A$, alors pour $y \leq Y$ on a $N^y \not\equiv 1 [r]$, d'où la condition 1. Par ailleurs, chaque $p < r$ divise $A$ alors que $A \leq N^{\frac{Y(Y+1)}{2}}$ donc
	\begin{displaymath}
	c_1r \leq \sum_{p < r} \log p \leq \log A \leq \frac{Y(Y+1)}{2} \log N
	\end{displaymath}
	D'où la condition 2.
\end{proof}}}

\begin{req}
	Comme l'ordre de $N$ modulo $r$ divise $r -1$, on a forcément $r > Y$.
\end{req}

\begin{lemme}
	Le cardinal de l'ensemble des monômes en $L$ variables et de degré $\leq k$ est
	\begin{displaymath}
	\# \{(m_1, \dots, m_L) \mid m_i \geq 0 \wedge \sum_{i=1}^{L} m_i \leq k\} = \binom{L+k}{k} \geq 2^{\min(L,k)}
	\end{displaymath}
\end{lemme}
{\footnotesize{\begin{proof}
	Le cardinal se calcul par récurrence: si $f(L, k)$ est le cardinal à calculer n a $f(L, 0) = 1$, $f(1, k) = k +1$ et $f(L, k) = f(L, k-1) + f(L-1, k)$. Pour la minoration, on observe que si $k \leq L$, on a
	\begin{displaymath}
	\binom{L+k}{k} = \frac{(L+k)(L+k-1)\dots(L+1)}{k(k-1)\dots 2} = \prod_{i =0}^{k-1} \left(\frac{L+k-i}{k-i}\right) \geq 2^k
	\end{displaymath}
	Si $L \geq k$, on renverse le rôle de $L$ et $k$.
\end{proof}}}

\begin{theo}[Agrawal--Kayal--Saxena]
	Soit $N \geq 2$ et soit $r$ un nombre premier tel que :
	\begin{enumerate}
		\item aucun nombre premier $\leq r$ ne divise $N$;
		\item on a $ord(M \mod r) \geq \left(\frac{2\log N}{\log 2}\right)^2 + 1$;
		\item pour $1 \leq a \leq r- 1$, on a $(X - a)^N \equiv X^N -a [(N, X^r -1)]$
	\end{enumerate}
	Alors $N$ est une puissance d'un nombre premier.
\end{theo}



\begin{appli}
	L'algorithme qui test de primalité cen temps polynomial le fait avec une complexité d'au plus $O((\log N)^{18})$.
\end{appli}
