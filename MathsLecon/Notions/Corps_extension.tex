%LECONs : 123 (Corps finis); 141 (Polynomes et corps de rupture); 151 (Dimension et rang)
Nous allons étudier les extensions de corps et analyser comment elles s'appliquent à la théorie de la dimension \cite[p.65]{Perrin}. Dans toute la suite nous ne considérons que des corps commutatifs.

\begin{definition}
	Soient $K$ et $L$ des corps tels que $K \subset L$. On dit que $L$ est une extension de $K$.
\end{definition}

\begin{req}
	Si $K$ est un sous-corps de $L$, alors $L$ est un $K$-espace vectoriel.
\end{req}

\begin{definition}
	Si $\dim_K L$ est finie \textblue{(existe car $L$ est un $K$-espace vectoriel)}, on pose $[L : K] = \dim_K L$ et l'entier $[L : K]$ s'appelle le degré de $L$ sur $K$.
\end{definition}

\begin{req}
	Si $K$ et $L$ sont des corps finis, on a $\left|L\right| = \left|K\right|^n$ avec $n = [L : K]$.
\end{req}

\begin{theo}[Théorème de la base télescopique]
	Soient $K \subset L \subset M$ des corps, $\left(e_i\right)_{i \in I}$ une base de $L$ sur $K$, $\left(f_j\right)_{j \in J}$ une base de $M$ sur $L$. Alors  $\left(e_if_j\right)_{(i, j) \in I \times J}$ une base de $M$ sur $K$.
\end{theo}
\begin{footnotesize}
	\begin{proof}
		\begin{itemize}
			\item La famille $\left(e_if_j\right)_{(i, j) \in I \times J}$ est libre sur $K$. Si on a $\sum_{i, j} \lambda_{i, j}e_if_j = 0$ pour $\lambda_{i,j} \in K$, on a également  $\sum_{j} f_j\left(\sum_i \lambda_{i, j}e_i\right) = 0$. Comme $\left(f_j\right)_{j \in J}$ une base de $M$ sur $L$, on a $\sum_i \lambda_{i, j}e_i = 0$. De même, on a $\lambda_{i, j} = 0$. 
			
			\item La famille $\left(e_if_j\right)_{(i, j) \in I \times J}$ engendre $M$. Soit $x \in M$, on le décompose alors dans la base des $f_j$. Comme les coefficients de la décomposition appartiennent à $L$, on peut les décomposer dans la base des $e_i$, ce qui donne le résultat. 
		\end{itemize}
	\end{proof}
\end{footnotesize}

\begin{cor}[Multiplicativité des degrés]
	Soient $K \subset L \subset M$ des corps tels que les degrés des extensions soient finis, on a $[M: K] = [M : L][L : K]$
\end{cor}
\begin{footnotesize}
	\begin{proof}
		On applique la théorie de la dimension aux bases que nous trouvons dans le théorème précédent.
	\end{proof}
\end{footnotesize}

\begin{definition}
	Soit $K \subset L$ une extension et $\alpha \in L$. Soit $\varphi : K[T] \to L$ l'homéomorphisme défini par $\varphi|_K = Id_K$ et $\varphi(T) = \alpha$.
	\begin{enumerate}
		\item Si $\varphi$ est injectif, on dit que $\alpha$ est transcendant sur $K$.
		\item Sinon, on dit que $\alpha$ est algébrique sur $K$. Il existe alors un polynôme non nul, $P(T)$, tel que $P(\alpha) = 0$. Plus précisément, si $I = \ker \varphi$, $I$ est un idéal principal non nul et $I = (P)$. Comme $P \neq 0$ et $P$ peut être supposé unitaire, on dit que $P$ est le polynôme minimal.
	\end{enumerate}
\end{definition}

\begin{theo}
	Soit $K \subset L$ une extension et $\alpha \in L$. Les propriétés suivantes sont équivalentes:
	\begin{enumerate}
		\item $\alpha$ est algébrique sur $K$;
		\item on a $K[\alpha] = K(\alpha)$;
		\item on a $\dim_K K[\alpha] < + \infty$.
	\end{enumerate}
\end{theo}
\begin{footnotesize}
	\begin{proof}
		\begin{description}
			\item[Montrons $1 \Rightarrow 2$] Soit $\alpha$ algébrique de polynôme minimal $P$: $\overline{\varphi} : K[T]/ (P) \to K[\alpha]$ est un isomorphisme. Comme $K[T]/ (P)$ est un corps \textblue{($(P)$ est un idéal premier ($K[\alpha]$ est intègre) et maximal ($P$ irréductible dans $K[T]$)} $K[\alpha]$ est un corps, d'où l'égalité.
			\item[Montrons $2 \Rightarrow 1$] Si $\alpha$ n'est pas algébrique, alors $K[\alpha]$ n'est pas un corps et $K[\alpha] \neq K(\alpha)$
			\item[Montrons $1 \Rightarrow 3$] Si $\alpha$ est transcendant, $K[\alpha] \simeq K[T]$ qui est de dimension infinie sur  $K$.
			\item[Montrons $3 \Rightarrow 1$] Provient de l'isomorphisme $\overline{\varphi} : K[T]/ (P) \to K[\alpha]$ car si $\deg P = n$, on extrait une base par divisions euclidiennes successives.
		\end{description}
	\end{proof}
\end{footnotesize}

\begin{appli}
	Si $K \subset L$, alors  $M = \{x \in L ~|~ x~algébrique~dans~K\}$ est un sous-corps de $L$. En effet, si $\alpha, \alpha' \in M$ considérons le sous-anneau $K[\alpha, \alpha'] = K[\alpha][\alpha']$. Comme $\alpha'$ est algébrique sur $K$, il l'est également sur $K[\alpha]$. Par le théorème précédent, $K[\alpha]$ et $K[\alpha, \alpha']$ sont des corps. De plus, par le théorème et la multiplicativité des degrés, on a $\left[K[\alpha, \alpha'] : K\right] < +\infty$. Comme $K[\alpha + \alpha']$ et $K[\alpha\alpha']$ sont inclus dans $K[\alpha, \alpha']$, $\alpha + \alpha'$ et $\alpha\alpha'$ sont également algébrique donc dans $M$.
\end{appli}



