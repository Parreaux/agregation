%DEV: Galton-Watson
%LECON: 243

On s'intéresse ici à la convergence des suites et des séries de fonctions. On se place dans le cadre réel, mais si les définitions et les résultats sont encore valide sur un autre corps de base. On commence par rappeler quelques résultats sur les suites de fonctions \cite[p.438]{Marco-Thieullen-Weil}.
	
\begin{definition}[Convergence simple d'une suite de fonctions]
	Soit $\left( f_n \right)_{n \in \N}$ une suite de fonctions définies sur $A$. On dit que $\left( f_n \right)_{n \in \N}$ converge simplement vers la fonction $f$, si, pour tout $t$ de $A$, la suite de nombre $\left( f_n(t) \right)_{n \in \N}$ converge dans $\R$ vers $f(t)$.
\end{definition}
	
\begin{definition}[Convergence uniforme d'une suite de fonctions]
	Soit $\left( f_n \right)_{n \in \N}$ une suite de fonctions définies sur $A$. On dit que $\left( f_n \right)_{n \in \N}$ converge uniformément vers la fonction $f$, si, 
	\begin{displaymath}
		\forall \epsilon \in \R^{*}_{+} ~ \exists N \in \N ~ \forall x \in A \text{ si } n \geq N \text{, alors } \left|f_n(x) - f(x)\right|< \epsilon.
	\end{displaymath}
	La fonction $f$ est dite limite uniforme de la suite $\left( f_n \right)_{n \in \N}$.
\end{definition}

\begin{req}
	La convergence uniforme entraîne la convergence simple. \textred{\textbf{Attention}}, la réciproque est fausse (voir exemple suivant \cite[p.221]{Gourdon-analyse})
\end{req}

\emph{Interprétation géométrique de la convergence uniforme (dans le cas réel)} \cite[p.220]{Gourdon-analyse}: Il arrive un moment (un rang pour lequel) où le graphe de $f_n$ est coincé entre le graphe de $f - \epsilon$ et le graphe de $f + \epsilon$.

\begin{ex}
	La suite de fonctions $f_n : [0, 1[ \to \R$ définie telle que $f_n(x) = x^n$ converge simplement vers $0$ sur $[0, 1[$, mais pas uniformément car pour tout $n \in \N$, il existe $x \in [0, 1]$ tel que $\left|f_n(x) - 0\right| \geq \frac{1}{2}$. Par contre, elle est uniformément convergente vers $0$ sur $\left[0, \frac{1}{2}\right]$ car pour tout $n$ et pour tout $x \in \left[0, \frac{1}{2}\right]$, $\left|f_n(x) - 0\right| \leq 2^{-n}$. Plus généralement, elle converge uniformément vers $0$ sur $[0, a]$ pour tout $a < 1$.
\end{ex}

On donne un critère pour montrer la convergence uniforme d'une suite de fonctions : le critère de Cauchy uniforme \cite[p.221]{Gourdon-analyse}.

\begin{prop}
	Une suite de fonction $\left(f_n\right)_{n \in \N}$ définie sur $A$ converge uniformément vers $f$ si et seulement si 
	\begin{displaymath}
	\begin{array}{cc}
	\forall \epsilon > 0, \exists N \in \N, \forall p \geq N, \forall q \geq N, \forall x \in A, & \left|f_p(x)-f_q(x)\right| < \epsilon \\
	\end{array} 
	\end{displaymath}
\end{prop}
{\small{\begin{proof}
	Condition nécessaire: application d'une inégalité triangulaire. Soir $\epsilon > 0$. Par convergence uniforme de la suite de fonctions $\exists N \in \N$ tel que $\forall x \in A \text{ si } n \geq N \text{, alors } \left|f_n(x) - f(x)\right|< \frac{\epsilon}{2}$. Soient $ p \geq N, q \geq N$ et $x \in A$, on a
	
	\begin{tabular}{ccll}
		$\left|f_p(x)-f_q(x)\right|$ & $= $ & $\left|f_p(x)-f(x) + f(x) - f_q(x)\right|$ & \textblue{introduction de $f$} \\
		& $\leq$ & $\left|f_p(x)-f(x)\right| + \left|f(x) - f_q(x)\right|$ & \textblue{inégalité triangulaire} \\
		& $\leq$ & $\epsilon$ & \textblue{application de l'hypothèse} \\
	\end{tabular}
	
	Condition suffisante: pour tout $x \in A$, la suite $\left(f_n(x)\right)_{n\in \N}$ est de Cauchy donc converge car $\R$ est un espace complet. On note sa limite $f(x)$ et on définit ainsi la fonction $f$. Montrons que $f$ est bien limite uniforme de la suite de fonctions. Soit $\epsilon > 0$. Par le critère de Cauchy uniforme, il existe $N \in \N$ tel que $\forall p \geq N, \forall q \geq N, \forall x \in A$ on a $\left|f_p(x)-f_q(x)\right| < \epsilon$. Soient $p \geq N$ et $x \in A$. En faisant tendre $q$ vers l'infini, on obtient $\left|f_p(x)-f(x)\right| < \epsilon$. Ce qui nous donne la convergence uniforme.
\end{proof}}}

\emph{Comment prouver la convergence uniforme si le critère de Cauchy ne fonctionne pas (ou si on ne souhaite pas l'appliquer)?} La preuve de la convergence uniforme se fait en deux temps. D'abord on cherche la limite simple de la suite et ensuite on prouve que la suite réelle $\sup_{x \in A} \left|f_n(x) - f(x)\right|$ tend vers zéro quand $n$ tend vers l'infini.
	
Maintenant, on va traiter le cas des séries de fonctions \cite[p.457]{Marco-Thieullen-Weil}. Soit $\left(f_n\right)_{n \in \N}$ une suite de fonction définie sur une partie $A$ non vide de $\R$. La série associée est la suite des sommes
\begin{displaymath}
	S_n = \sum_{k=0}^{k=n} f_k
\end{displaymath}
	
\begin{definition}[Convergence simple et uniforme d'une série de fonctions]
	On dit que la série est simplement (respectivement uniformément) convergente si la suite $\left(S_n\right)_{n \in \N}$ est simplement (respectivement uniformément) convergente.
\end{definition}
	
\begin{definition}[Convergence normale d'une série de fonctions]
	Soit $\left(f_n\right)_{n \in \N}$ une suite de fonction définie sur $A$. La série $\sum_{k \geq 0}f_k$ est dite normalement convergente sur $A$ si la série numérique de termes $\sup_{x \in A} \left|f_n(x)\right|$ est convergente.
\end{definition}

\begin{req}
	Il est équivalent de dire que la série de fonction $\sum g_n$ converge normalement s'il existe une série à termes positifs $\sum a_n$ convergente telle que 
	\begin{displaymath}
	\begin{array}{cc}
	\forall n \in \N, \forall x \in X & \|g_n(x)\| \leq a_n \\
	\end{array}
	\end{displaymath}
	Comme $ \|g_n(x)\| \leq 2\|g_n(x)\|$ (\textblue{on a des termes positifs}) et que la série de terme général $2\|g_n(x)\|$ converge si et seulement si la série de terme général $\|g_n(x)\|$ converge (\textblue{on peut faire sortir le scalaire $2$ de la somme}). On obtient l'équivalence.
\end{req}

\begin{ex}
	La série de fonction $\sum g_n$ définie par $g_n : [0, 1] \to \R$ définie par $g_n(x) = \frac{x^n}{n^2}$ converge normalement sur $[0, 1]$ car $\|g_n\|_{\infty} = \frac{1}{n^2}$ et la somme $\sum \|g_n \|$ converge.
\end{ex}

\begin{theo}
	Soit $\left(f_n\right)_{n \in \N}$ une suite de fonction définie sur $A$. Si la série $\sum_{k \geq 0}f_k$ est normalement convergente sur $A$, alors elle y est uniformément convergente.
\end{theo}
{\small{\begin{proof}
	On applique le critère de Cauchy uniforme: pour tout $n, p \in \N$ et pour tout $x \in A$,
	\begin{displaymath}
	\|g_n(x) + \dots + g_{n+p}(x) \| \underbrace{\leq}_{\text{\textsf{\textblue{norme}}}} \|g_n(x) \| + \dots + \| g_{n+p}(x) \| \underbrace{\leq}_{\text{\textsf{\textblue{cv normale}}}} \|g_n\|_{\infty} + \dots + \| g_{n+p}(x) \|_{\infty}
	\end{displaymath}
\end{proof}}}


\begin{cex}[\textred{\textbf{Attention}}, la réciproque est fausse \protect{\cite[p.226]{Gourdon-analyse}}]
	La série de fonction $\sum \left(-1\right)^n\frac{x}{n^2 + x^2}$ converge uniformément sur $\R_+$ (\textblue{par majoration des restes}) mais ne converge pas normalement.
\end{cex}