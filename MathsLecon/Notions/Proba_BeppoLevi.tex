%DEVs : TCL
Le théorème de Beppo Levi est un résultat important pour les théories de l'intégration et des probabilités \cite[p.56]{GaretKurtzmann}. Nous allons l'énoncé, donner sa preuve puis quelques applications de ce dernier.

\begin{theo}[Théorème de Beppo Levi (convergence monotone)]
	Si $\left(f_n\right)_{n \geq 1}$ est une limite croissante de fonctions mesurables positives convergeant presque partout vers $f$, alors la suite $\left(\int f_n d\mu\right)_{n \geq 1}$ converge vers $\int f d\mu$ \textblue{(la limite peut être infinie)}.
\end{theo}
\begin{footnotesize}
	\begin{proof}[Démonstration \protect{\cite[p.91]{GaretKurtzmann}}]
		\begin{enumerate}
			\item On montre d'abord une forme très faible de ce résultat: si $f$ est une fonction simple \textblue{(une fonction numérique dont l'image est constituée d'un nombre fini de valeurs réelles)} positive et $\left(E_n\right)_{n\geq 1}$ une suite croissante d'ensembles mesurables de réunion de $E$, alors $\lim\limits_{n \to + \infty} \int \mathbb{1}_{E_n} f d\mu = \int \mathbb{1}_{E} f d\mu$.
			
			En effet, si $f = \sum_{i = 1}^{n} \alpha_i\mathbb{1}_{A_i}$, avec pour tout $i$, $\alpha_i \geq 0$, on a $f\mathbb{1}_{E_n} = \sum_{i = 1}^{n} \alpha_i \mathbb{1}_{A_i \cap E_n}$ et $\int f\mathbb{1}_{E_n} d\mu = \sum_{i = 1}^{n} \alpha_i \mu(A_i \cap E_n)$. La convergence vers $\sum_{i = 1}^{n} \alpha_i \mu(A_i \cap E) = \int f\mathbb{1}_{E} d\mu$ découle du théorème de continuité séquentielle croissante \textblue{(soit une suite d'événement croissant pour l'inclusion, alors $\mathbb{P}(\bigcup E_n) = \mathbb{P}(\lim E_n)$)}.
			
			\item Passons au cas général: on considère une suite $\left(f_n\right)_{n \in \N}$ croissante de fonctions mesurables tendant vers $f$ et on veut montrer que $\int f_n d\mu$ tend vers $\int f d\mu$. La suite $\left(\int f_n d\mu\right)_{n \in \N}$ est croissante \textblue{(par croissante de l'intégrale et croissance de $f_n$)}, majorée par $\int f d\mu$ \textblue{($f$ majore les $f_n$ par croissante de la suite et on applique ensuite la croissance de l'intégrale)}, donc cette limite existe et est majorée par $\int f d\mu$. Montrons alors que $\lim \int f_n d\mu \geq \int f d\mu$. Par définition de l'intégrale, montrons que pour toutes partitions finies $\left(\Omega_i\right)_{i \in I}$, on a $\lim \int f_n d\mu \geq I\left(\left(\Omega_i\right)_{i \in I}, f\right)$ où $I\left(\left(\Omega_i\right)_{i \in I}, f\right) = \sum_i \inf \{f(\omega); \omega \in \Omega_i\} \mu(\Omega_i)$. Posons $g = \sum_{i \in I} \inf \{f(\omega); \omega \in \Omega_i\} \mathbb{1}_{\Omega_i}$, alors $g$ est une fonction simple avec $0 \leq g \leq f$.
			
			Posons $\alpha \in \left]0, 1\right[$ et posons $E_n = \{f_n \geq \alpha g\}$. Comme la suite $(f_n)_{n \in \N}$ est croissante, la suite $\left(E_n\right)_{n \in \N}$ l'est également. Comme les fonctions $f_n$ et $g$ sont mesurables, $E_n \in \mathcal{F}$. Si $g(\omega) = 0$, on a $\omega \in E_n$ pour tout $n$, sinon, comme $\lim f_n(\omega) = g(\omega) > \alpha g(\omega)$, on a $\omega \in E_n$ pour $n$ assez grand. Finalement, la réunion des $E_n$ est $\Omega$ tout entier. Ainsi d'après l'item 1 de la preuve \textblue{(l'hypothèse de mesurailité est donc importante pour pouvoir l'appliquer)}, on a $\lim\limits_{n \to \infty} \int \mathbb{1}_{E_n} \alpha g d\mu = \int \alpha g d\mu$.
			
			On a $f_n \geq \mathbb{1}_{E_n}f_n \geq \mathbb{1}_{E_n}\alpha g$, d'où $\lim \int f_n d\mu \geq \lim \int \mathbb{1}_{E_n}f_n d\mu \geq \int\alpha g d\mu$, soit $\lim \int f_n d\mu \geq \alpha I\left(\left(\Omega_i\right)_{i \in I}, f\right)$. En faisant tendre $\alpha$ vers $1$, on peut conclure.
		\end{enumerate}
	\end{proof}
\end{footnotesize}

\begin{appli}[Linéarité de l'intégrale]
	Si $f$ et $g$ sont intégrables, $\alpha$ un réels=, alors $\int f + \alpha g d\mu = \int f d\mu + \alpha \int g d\mu$.
\end{appli}
\begin{footnotesize}
	\begin{proof}[Démonstration \protect{\cite[p.93]{GaretKurtzmann}}]
		\begin{enumerate}
			\item Soient $f$ et $g$ deux fonctions mesurables positives, $\alpha > 0$. Soient $\left(f_n\right)$, $\left(g_n\right)$ des suites croissantes de fonctions simples positives convergeant respectivement vers $f$ et $g$. On a pour tout $n$, $\int \left(f_n + \alpha g_n\right) d\mu = \int f_n d\mu + \alpha \int g_n d\mu$. En appliquant trois fois le théorème de Beppo Levi, on obtient à la limite $\int \left(f + \alpha g\right) d\mu = \int f d\mu + \alpha \int g d\mu$.
			
			\item Soient $f$ et $g$ deux fonctions intégrables de signe quelconque. On a $\int \left|\alpha g\right| d\mu \leq \int \left|\alpha\right|g^+ + \left|\alpha\right|g^- d\mu < \infty$ \textblue{(linéarité des fonctions positives)}. Par une décomposition par cas (en fonction du signe de $\alpha$), on obtient donc que $\int \alpha g d\mu = \alpha \int g d\mu$.
			
			On s'est ramener au cas $\alpha = 1$. Comme $\left| f+ g\right| \leq \left|f\right| + \left|g\right|$, $f + g$ est intégrable. On pose $h = f + g$ et on étudie l'intégrale de $h$ en la décomposant de sa partie positive et sa partie négative. On utilise ensuite le point 1 pour conclure.
		\end{enumerate}
	\end{proof}
\end{footnotesize}

\begin{appli}[Lemme de Fatou]
	Pour toute suit $\left(f_n\right)_{n \geq 1}$ de fonctions mesurables positives, on a $\int \liminf_{n \to \infty} f_n d\mu \leq \liminf_{n \to \infty} \int f_n d\mu$.
\end{appli}
\begin{footnotesize}
	\begin{proof}
		Posons $g_n = \inf_{k \geq n} f_k$. La suite $\left(g_n\right)_{n \geq 1}$ est une suite croissante, dont la limite est, par définition, $\lim_{n \to + \infty} g_n = \liminf_{n \to \infty} f_n$. On a pour tout $n$; $f_n \geq g_n$ et donc $\int f_n d\mu \geq \int g_n d\mu$. D'où, $\liminf_{n \to \infty} \int f_n d\mu \geq \int g_n d\mu$. Mais d'après le théorème de Beppo Levi, $\int g_n d\mu$ converge vers $\int g d\mu$, ce qui est le résultat souhaité.
	\end{proof}
\end{footnotesize}

\begin{req}
	Du lemme de Fatou, on en déduit le théorème de convergence dominée.
\end{req}