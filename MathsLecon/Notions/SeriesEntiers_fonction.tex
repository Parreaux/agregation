%LECON : 243

Si on considère une série entière définie telle que $\sum a_ z^n$ qui converge sur un disque de convergence que l'on note $\mathcal{D}_{S}$ (on considère le disque ouvert), on peut alors définir une fonction de ce disque de convergence dans $\C$ correspondant à la somme de la série entière. Nous allons donc chercher à étudier sa régularité \cite[p.29]{ArnaudiesFraysse-3}. Sauf indication du contraire on considère la fonction définie sur $\C$ (le seul moment ou on va faire une différence c'est lors de l'étude de la dérivabilité de notre fonction entière).

On définit la fonction série entière pour une série formelle $S = \sum_{n = 0}^{\infty} a_nX^n \in \C[X]$ par:
\begin{displaymath}
\begin{array}{cccl}
	\widetilde{S}: & \mathcal{D}_{S} & \to & \C \\
	& z & \mapsto & \sum_{n = 0}^{\infty} a_nz^n \\
\end{array} 
\end{displaymath}
où $\mathcal{D}_{S}$ est le disque ouvert de convergence de la série formelle $S$ vue comme série entière. Même si sur le cercle d'incertitude, en certain point, la série entière converge, nous n'allons pas étudier la fonction (ni un de ces prolongement) en ses points.

\begin{prop}[Premières propriétés]
	Soit $S = \sum_{n = 0}^{\infty} a_nX^n$ et $T =\sum_{n = 0}^{\infty} b_nX^n$ deux séries formelles. On a les résultats suivants:
	\begin{enumerate}
		\item Pour $\lambda \in \C^{*}$, $\widetilde{\lambda S} = \lambda \widetilde{S}$;
		\item Pour $z \in \mathcal{D}_{S} \cap \mathcal{D}_{T}$, on a
		\begin{displaymath}
		\begin{array}{ccc}
		\widetilde{(S+T)}(z) = \widetilde{S}(z) + \widetilde{T}(z) & \text{ et } & \widetilde{(ST)}(z) = \widetilde{S}(z)\widetilde{T}(z)\\
		\end{array}
		\end{displaymath}
		\item Si $S$ est de valuation non nul, alors pour tout $z \in \mathcal{D}_{S} \cap \mathcal{D}_{\frac{1}{S}}$, on a
		\begin{displaymath}
		\widetilde{\left(\frac{1}{S}\right)}(z) = \frac{1}{\widetilde{S}(z)}
		\end{displaymath}
	\end{enumerate}
\end{prop}
{\small{\begin{proof}
	\begin{enumerate}
		\item Montrons que le rayon de convergence de la série entière $\sum a_n z^n$ est le même que celui de la série entière $\sum \lambda a_n z^n = \lambda\sum a_n z^n$. Considérons la suite $\left(\lambda a_n z^n\right)_{n\in \N}$. Cette suite est proportionnelle à $\left(a_n z^n\right)_{n\in \N}$, en particulier elle est bornée si et seulement si cette dernière est bornée. Par la définition du rayon de convergence, on obtient le résultat.
		\item Soit $z \in \mathcal{D}_{S} \cap \mathcal{D}_{T}$, alors les séries entières $\sum a_n z^n$ et $\sum b_n z^n$ sont absolument convergente. Donc en appliquant les résultats sur les opérations due les rayons de convergence, on conclut.
		\item On applique le résultat précédent à la série produit $S \frac{1}{S} = 1$.
	\end{enumerate}
\end{proof}}}

\paragraph{Rappel sur la régularité de suites et de séries de fonctions} Pour étudier la régularité d'une fonction série entière nous allons vouloir l'étudier comme une série de fonctions particulières. Nous rappelons donc ici quelques résultats fondamentaux sur la régularité des fonctions définies comme limite ou somme d'une suite ou d'une série de fonctions.
\input{./../Notions/SeriesFonctions_regularite.tex}

\paragraph{Continuité} Nous allons étudier la continuité de la fonction série entière \cite[p.30]{ArnaudiesFraysse-3} et ses conséquences qui est toujours continue. On voit apparaître ici une première caractérisation des fonctions développables en séries entières. En effet, une fonction n'est pas développable en série entière sur un point de discontinuité, sinon la fonction qui lui est associée ne serait pas continue. 

\begin{prop}[Continuité]
	Soit $S = \sum a_n z^n$ une série formelle dans $\C$. Alors:
	\begin{enumerate}
		\item La fonction $\widetilde{S}$ est continue sur le disque de convergence de $S$ vue comme série entière.
		\item Soient $a$ et $b$ réels, avec $-R < a < b < R$ où $R$ est le rayon de convergence de la série $S$. Alors,
		\begin{displaymath}
		\int_{a}^{b} \widetilde{S}(t) dt = \sum_{n=0}^{\infty} a_n t^ndt = \sum_{n=0}^{\infty} a_n \frac{b^{(n+1)} - a^{(n+1)}}{n+1}
		\end{displaymath}
		\item La série entière $\sum a_n z^n$ converge normalement, donc uniformément, sur tout compact du disque de convergence.
	\end{enumerate}
\end{prop}
{\small{\begin{proof}
	Soit $S = \sum a_n z^n$ une série formelle dans $\C$ dont la série entière associée à un disque de convergence que l'on note $\mathcal{D}$.
	\begin{enumerate}
		\item Les sommes partielles $\sum_{k=0}^{n} a_k z^k = S_n(z)$ sont polynomiales (\textblue{en $z$}) donc continues sur $\C$. D'autre part, la série entière $\sum a_n z^n$ converge normalement sur tout disque fermé $\overline{D}_r$ tel que $0 \geq r < R$ où $R$ est le rayon de convergence de la série entière $\sum a_n z^n$ (\textblue{lemme d'Abel}). 
		
		Soit $z_0 \in \mathcal{D}$. Soit $r \in \R+$ tel que $\left|z_0\right| < r < R$ (\textblue{existe car $z_0 \in \mathcal{D}$ et que le cercle d'incertitude (donc la frontière de $\mathcal{D}$) n'est pas comprise dans $\mathcal{D}$}). D'après les remarque précédente et par le théorème de continuité par limite uniforme d'une suite de fonction, $\widetilde{S}|_{\overline{D}_{r}}$ est continue. En particulier, $\widetilde{S}|_{\overline{D}_{r}}$ est continue en $z_0$. Comme $\overline{D}_{r}$ est un voisinage de $z_0$, $\widetilde{S}$ est continue en $z_0$, d'où le résultat.
		
		\item Soit $r = \max \left(\left|a\right|, \left|b\right|\right)$. D'après ce qui précède, la série de fonctions continue de $t$, $\sum a_n t^n$ converge normalement, donc uniformément sur $[-r, r]$ (\textblue{car $[-r, r] \subseteq \overline{D}_{r}$}), ce qui autorise l'intégration terme à terme. On obtient alors la formule recherchée.
		
		\item Soit $L$ une partie compacte non vide du disque de convergence $\mathcal{D}$. Notons $F = \C \setminus \mathcal{D}$, $F$ est fermé dans $\C$ (\textblue{complémentaire d'une partie ouverte: le disque de convergence}) et $F \cap L = \emptyset$. 
		
		La fonction $f: L \to \R_+^*$ définie telle que $z \mapsto d(z, F) = \underset{t \in F}{\inf} \left|z - t\right|$ est continue (\textblue{on montre qu'elle est uniformément continue en montrant qu'elle est lipschitzienne}), donc admet un minimum $\alpha$ (\textblue{$L$ est compact et par le théorème de Heine}). Comme $\alpha > 0$ (\textblue{$F \cap L = \emptyset$}), le nombre $r = R - \alpha$ est inférieur (strict) à $R$. De plus, $L \subset \overline{D}_{r}$. 
		
		Comme la convergence de la série entière $\sum a_n z^n$ est normale sur $\overline{D}_{r}$, elle l'est sur $L$.
	\end{enumerate}
\end{proof}}}

Une des applications importantes de la continuité est le théorème des zéros isolé qui montre que le terme général d'une série entière n'admet pas de points d'accumulation. Ce théorème nous permet également de montre l'unicité des coefficients d'une série entière (on remarque également que la dérivabilité des séries entières nous permet de le montrer).

\begin{theo}[Principe des zéros isolés \protect{\cite[p.239]{Gourdon-analyse}}]
	Soit $f$ la fonction d'une série entière $\sum a_n z^n$ sur son disque de convergence. S'il existe une suite $\left(z_p\right)$ de nombres complexes tendant vers $0$ tels que $f\left(z_p\right) = 0$ pour tout $p$ alors $a_n = 0$ pour tout $n$.
\end{theo}
{\small{\begin{proof}
	Supposons que l'un des $a_n$ ne soit pas nul, et notons $q$ le plus petit entier naturel tel que $a_q \neq 0$. On peut écrire sur $\mathcal{D}$,
	\begin{displaymath}
	\begin{array}{ccc}
	f(z) = z^pg(z) & \text{ avec } & g(z) = \sum_{n=0}^{\infty} a_{n+q}z^n
	\end{array}
	\end{displaymath}
	Comme, pour tout $p$, $g\left(z_p\right) = 0$ et que $g$ est continue (\textblue{fonction série entière de rayon de convergence positif (strictement)}), on a 
	\begin{displaymath}
	a_p = g(0) = \underset{p \to \infty}{\lim} g(z_p) =0
	\end{displaymath}
	Contradiction et $a_n =0$ pour tout $n$.
\end{proof}}}

\begin{cor}
	Soit $f$ et $g$ deux fonctions séries entières de séries entières $\sum a_n z^n$ et $\sum b_n z^n$ vérifiant $f(z_p) = g(z_p)$ pour une suite de nombres complexes non nuls tendant vers $0$, alors pour tout $n$, $a_n = b_n$. En particulier, deux séries entières dont les fonctions coïncident sur un voisinage de $0$ dans $\R$ sont égales.
\end{cor}
{\small{\begin{proof}
	On applique le théorème précédent à la fonction $f- g$ qui est également une fonction série entière. 
\end{proof}}}

\paragraph{Dérivabilité dans $\R$} Nous nous intéressons maintenant aux propriétés de dérivabilité de la fonction série entière définie dans $\R$ \cite[p.238]{Gourdon-analyse}. On rappelle la notion de série dérivée que nous avons définie lorsque nous avons étudier le rayon de convergence d'une série entière.

\begin{definition}[Série entière dérivée]
	Soit $\sum a_n z^n$ une série entière. Nous appelons série entière dérivée la série entière définie telle que $\sum na_n z^n$.
\end{definition}

\begin{prop}[Dérivabilité dans $\R$]
	La fonction $f$ série entière de la série $\sum a_n x^n$ définie dans $\R$ de rayon de convergence $\R$ est de classe $\Cun$. La série entière dérivée à le même rayon de convergence que $\sum a_n x^n$ et $\forall x \in ]-R, R[$, on a
	\begin{displaymath}
		f'(x) = \sum_{n=1}^{+\infty}na_nx^{n-1}
	\end{displaymath}
\end{prop}
{\small{\begin{proof}
	Montrons que la série entière dérivée à le même rayon de convergence que $\sum a_n x^n$. (\textblue{Notons qu'une preuve à déjà été donnée plus, celle-ci est une autre manière de voir la preuve donc nous la redonnons}).
	
	Notons $R$ le rayon de convergence de la série entière $\sum a_n x^n$ et $R'$ le rayon de convergence de sa série entière dérivée. Soit $r$ tel que $0 \leq r > R'$. La suite $\left(na_nr^n\right)_{n \in \N}$ est bornée donc $\left(a_nr^n\right)_{n \in \N}$ est bornée. Soit $r \leq R$. Soit maintenant $r \leq R$. Soit $r_0$ tel que $r < r_0 < R$, la suite $\left(a_nr^n\right)_{n \in \N}$ est bornée, donc la suite $\left(na_nr^n\right)_{n \in \N}$ est bornée tend vers $0$ car $na_nr^n = n\left(a_nr_0^n\right)\left(\frac{r}{r_0}\right)^n$ avec $\left(\frac{r}{r_0}\right) <1$, on en conclut que $r < R'$ donc $R < R'$. Donc $R = R'$.
	
	La dérivabilité de $f$ et la valeur de $f'$ sont une conséquence du théorème de dérivabilité des suites de fonctions. De plus, la continuité de $f'$ est assurer par la continuité d'une fonction série entière (\textblue{puisque la fonction dérivée est une fonction série entière}).
\end{proof}}}

\begin{ex}[Dérivée de la fonction $x \mapsto \frac{1}{1 -x}$ \protect{\cite[p483]{Marco-Thieullen-Weil}}]
	Pour tout réel $x \neq 1$, nous avons l'identité
	\begin{displaymath}
	\frac{1}{1-x} = 1 + x + x^2 + \dots + \frac{x^{n+1}}{1-x}
	\end{displaymath}
	(\textblue{que l'on montre par récurrence sur $n$ : le cas $n = 0$ donne $1 + \frac{x^{1}}{1-x}= \frac{1- x + x}{1-x} = \frac{1}{1-x}$; sinon pour $n \neq 0$, $1 + x + x^2 + \dots + \frac{x^{n+1}}{1-x} = 1 + x\left(1 + x + x^2 + \dots + \frac{x^{n}}{1-x}\right) = 1 + x\frac{1}{1-x} = \frac{1- x + x}{1-x} = \frac{1}{1-x}$})  qui montre que la fonction, pour $x \in ]-1; 1[$, de la série entière $\sum_{n \geq 0} x^n$ est la fonction $x \mapsto \frac{1}{1 - x}$. Par le théorème sur la dérivation, de la série entière $\sum_{n \geq 0} (n+1)x^n$ a pour somme, sur l'intervalle $]-1, 1[$, la fonction $x \mapsto \frac{1}{(1 - x)^2}$. C'est-à-dire que, pour tout $x \in ]-1, 1[$, 
	\begin{displaymath}
	\frac{1}{(1 - x)^2} = \sum_{n \geq 0} (n+1)x^n
	\end{displaymath}
\end{ex}

\begin{cor}
	La fonction série entière de la série $\sum a_n x^n$ est de classe $\mathcal{C}^{\infty}$ sur $]-R, R[$ où $R$ est le rayon de convergence de $\sum a_n x^n$. De plus, on définie $f^{(p)}$ comme la fonction d'une série entière de rayon de convergence $R > 0$ et \begin{displaymath}
	f^{(p)}(x) = \sum_{n=p}^{+\infty}\frac{n!}{(n-p)!}a_nx^{n-p}
	\end{displaymath}
\end{cor}
{\small{\begin{proof}
	Par récurrence en utilisant la proposition précédente.
\end{proof}}}

Une application importante de cette notion de dérivabilité est l'unicité des coefficient pour une fonction série entière. Cela implique qu'une fonction définie par une série entière est entièrement caractérisé par ces coefficients. Même si ce résultat n'est vrai que dans $\R$ (pour l'instant), en étudiant la $\C$-dérivabilité des fonctions séries entières, on pourra l'étendre à toutes fonctions.
\begin{cor}
	Si $f$ et $g$ sont deux fonctions de séries entières définies tels que $\sum a_n x^n$ et $\sum b_n x^n$ coïncident sur un voisinage alors $a_n = b_n$.
\end{cor}
{\small{\begin{proof}
	En effet, si $f$ et $g$ coïncide sur un voisinage de $0$ on peut alors écrire que pour tout $n \in \N$, $b_n = \frac{g^{(n)}(0)}{n!}= \frac{f^{(n)}(0)}{n!} = a_n$. On raisonne de même pour un autre voisinage.
\end{proof}}}

\subparagraph{Dérivabilité dans $\C$} Nous allons maintenant généralisé les concepts de dérivabilité sur $\C$ afin d'en tirer les conséquences nécessaires, notamment en terme d'analycité et d'holomorphie. Nous commençons par quelques rappels sur la notion de $\C$-dérivabilité. Comme on s'y attend la notion de $\C$-dérivabilité correspond à la dérivabilité sur $\R$ (sur la bonne restriction de fonction). Cependant, il faut faire attention, car la notion de $\C$ dérivabilité est une notion très forte. Il ne suffit pas qu'une fonction soit différentiable pour qu'elle soit $\C$-dérivable, mais la réciproque est vrai \cite[p.239]{Gourdon-analyse}.  

\begin{definition}[Fonction $\C$-dérivée \protect{\cite[p.38]{ArnaudiesFraysse-3}}]
	Soit $U$ un ouvert de $\C$ et $f : U \to \C$ une fonction. On dit que $f$ est $\C$-dérivable en un point $z_0 \in U$ si et seulement si l'élément $\underset{\underset{z \neq z_0}{z \to z_0}}{\lim} \frac{f(z) - f\left(z_0\right)}{z -z_0}$ existe dans $\C$. Si c'est le cas, cet élément est la $\C$-dérivée de $f$ en $z_0$ et se note $f'\left(z_0\right)$. On dit que $f$ est $\C$-dérivable sur $U$ si et seulement si elle est dérivable en chaque point $z_0 \in U$.  Si c'est le cas, la fonction, notée $f'$, $U \to \C$ définie par $z_0 \mapsto f'\left(z_0\right)$ s'appelle $\C$-dérivée de $f$.
\end{definition}

\begin{ex}
	Pour $n \in \N^*$, la fonction $f : \C \to \C$ telle que $z \mapsto z^n$ est $\C$-dérivable, et sa $\C$-dérivée est la fonction $f' : z \mapsto nz^{n-1}$.
\end{ex}

\begin{ex}
	La fonction exponentielle complexe $\begin{array}{ccll}
	\exp : & \C & \to & \C \\
	& z & \mapsto & \sum_{n= 0}^{+\infty} \frac{z^n}{n!}
	\end{array}$ est $\C$-dérivable en tout point $z_0 \in \C$. C'est évident en $0$ car 
	\begin{displaymath}
	\frac{\exp z - \exp 0}{z} = \sum_{n= 0}^{+\infty} \frac{z^{n-1}}{n!} \underset{z\to 0}{\rightarrow} 1
	\end{displaymath}
	et on en déduit que pour tout $z_0 \in \C$:
	\begin{displaymath}
	\frac{\exp z - \exp z_0}{z- z_0} = \exp(z_0)\frac{\exp (z - z_0) - 1}{z- z_0} \underset{z\to 0}{\rightarrow} \exp z_0
	\end{displaymath}
	On en déduit une propriété fondamentale de l'exponentielle: sa dérivée est elle-même.
\end{ex}

\begin{prop}[Propriété d'une $\C$-dérivée]
	\begin{enumerate}
		\item Une fonction constante est $\C$-dérivable, de $\C$-dérivée nulle.
		\item La $\C$-dérivabilité de $f$ en $z_0 \in U$ entraîne sa continuité en $z_0$.
		\item Soit $f$ et $g : u \to \C$ $\C$-dérivable en $z_0$; alors $f +g$ et $fg$ le sont aussi et on a: $(f +g)'(z_0) = f'(z_0) + g'(z_0)$ et $(fg)'(z_0) = f'(z_0)g(z_0) + f(z_0)g'(z_0)$.
		\item Soit $f : U \to \C$ telle que $f$ ne s'annule jamais et soit $\C$-dérivable en $z_0$ alors $\frac{1}{z}$ l'est aussi et on a :
		$\left(\frac{1}{z}\right)'(z_0) = -\frac{f'(z_0)}{(f(z_0))^2}$.
		\item Soit $V$ un ouvert tel que $\Im \left(f\right) \subset V$, et soir $\phi : V \to \C$. Si $f$ est $\C$-dérivable en $z_0 \in U$ et si $\phi$ est $\C$-dérivable en $u_0 = f(z_0)$, alors $\phi \circ f$ est $\C$-dérivable en $z_0$, et $\left(\phi \circ f\right)'(z_0) = \phi'(u_0)f'(z_0)$.
	\end{enumerate}
\end{prop}
{\small{\begin{proof}
	\begin{enumerate}
		\item Soit $f = a$ une fonction constante. On a $\lim \frac{f(z) - f\left(z_0\right)}{z -z_0} = \lim \frac{a -a}{z -z_0} = \lim 0 = 0$. Donc $f$ est $\C$-dérivable puisque la limite existe et de $\C$-dérivée nulle.
		\item Soit $\epsilon > 0$, par $\C$-dérivabilité de $f$ en $z_0$, on a:
		\begin{displaymath}
			\exists \eta, \left|z - z_0\right| < \eta \Rightarrow \left|\frac{f(z) - f\left(z_0\right)}{z -z_0}\right| < \epsilon
		\end{displaymath} 
		Soit par les propriétés du module sur $\C$ et par hypothèse sur $\left|z -z_0\right|$: 
		\begin{displaymath}
		\exists \eta, \left|z - z_0\right| < \eta \Rightarrow \left|f(z) - f\left(z_0\right)\right| < \epsilon \left|z -z_0\right| < \epsilon\eta
		\end{displaymath} 
		Comme $\epsilon$ est arbitraire, en posant $\epsilon' = \frac{\epsilon}{\eta}$, on peut conclure.
		\item Cas de $f+g$: $\lim \frac{(f+g)(z) - (f+g)\left(z_0\right)}{z -z_0} = \lim \left(\frac{f(z) -f\left(z_0\right)}{z -z_0} + \frac{g(z) -g\left(z_0\right)}{z -z_0}\right) = \lim \frac{f(z) -f\left(z_0\right)}{z -z_0} + \lim \frac{g(z) -g\left(z_0\right)}{z -z_0} = f'(z_0) + g'(z_0)$.
		
		Cas de $fg$ (on factorise comme il faut): $\lim \frac{(fg)(z) - (fg)\left(z_0\right)}{z -z_0} = \lim \left(\frac{f(z) -f\left(z_0\right)}{z -z_0}g(z) + \frac{g(z) -g\left(z_0\right)}{z -z_0}f(z)\right) = \lim \frac{f(z) -f\left(z_0\right)}{z -z_0}g(z_0) + \lim \frac{g(z) -g\left(z_0\right)}{z -z_0}f(z_0) = f'(z_0)g(z_0) + g'(z_0)f(z_0)$.
		
		\item On pose $g =\frac{1}{f}$, on a $\lim \frac{g(z) - g\left(z_0\right)}{z -z_0} = \lim \frac{f(z_0)-f(z)}{(z -z_0)\left(f(z)f(z_0)\right)} = \lim -\frac{f(z)-f(z_0)}{(z -z_0)\left(f(z)f(z_0)\right)} = -\frac{f'(z_0)}{(f(z_0))^2}$.
		
		\item On a : $\lim \frac{(\phi \circ f)(z) - (\phi \circ f)\left(z_0\right)}{z -z_0} =\lim \frac{\phi(u) - \phi(u_0)}{u -u_0} \frac{u -u_0}{z - z_0}$ avec $u_0 = f(z_0)$ et $u = f(z)$. Comme $\phi$ et $f$ sont $\C$-dérivable, on a : $\lim \frac{(\phi \circ f)(z) - (\phi \circ f)\left(z_0\right)}{z -z_0} = \phi'(u_0) \lim \frac{f(z) -f(z_0)}{z - z_0} = \phi'(u_0) f'(z_0)$.
	\end{enumerate}
\end{proof}}}

\begin{prop}[Cas des fonctions séries entières]
	Soit $\sum a_n z^n$ une série entière. La fonction série entière $\sum a_n z^n$ est $\C$-dérivable dans son disque de convergence et sa $\C$-dérivée est la fonction de la série entière dérivée. De plus, cette fonction est $\mathcal{C}^{\infty}$ dans $\C$ et sa dérivée $p^{ieme}$ est la même que dans le cas réel. 
\end{prop}
{\small{\begin{proof}
	Soit $z_0 \in \C$ tel que $\left|z_0\right| < R$ où $R$ est le rayon de convergence de la série entière. Fixons un réel $r \in \left]\left|z_0\right|, R\right[$. Pour tout $n \in \N^*$, notons $u_n$ le polynôme $a_n\left(X^{n-1} + X^{n-2}z_0 + \dots + z_0^{n-1}\right)$. Soit $z \in \mathcal{D} \setminus \{z_0\}$, on a $\frac{\sum a_n z^n - \sum a_n z_0^n}{z - z_0} = \sum_{n\geq 1} u_n(z)$. Mais pour $n \in \N^*$ et $\left|z\right| \leq r$, $\left|u_n(z)\right| \leq n\left|a_n\right|r^{n-1}$. Or la série numérique $\sum n\left|a_n\right|r^{n-1}$ converge, d'où la série de fonction en $z$ converge normalement, donc uniformément sur $\{z \in \C \left|z\right|\leq r\}$ vers une somme $\sum (n+1)a_{n+1}z_0^n$ qui est continue par le caractère polynomiale de $u_n$. Ce qui prouve le résultat.
	
	Pour montrer le caractère $\mathcal{C}^{\infty}$ de la série de fonction on raisonne par récurrence en appliquant ce que l'on vient de faire.
\end{proof}}}

Par ce dernier résultat, on a alors qu'une série entière est $\mathcal{C}^{\infty}$ sur $\C$. On verra plus tard que la réciproque est fausse: une fonction $\mathcal{C}^{\infty}$ sur $\C$ ne sont pas toujours développable en série entière. Celles qui le sont seront appelée fonctions analytiques.

\subparagraph{Intégration et séries entières} Lorsqu'on étudie des séries entières ont souhaite pouvoir étudier l'intégrale des fonctions définies par celles-ci. Nous allons donner quelques résultats d'intégrations autours des séries entières \cite[p.239]{Gourdon-analyse}.

\begin{prop}
	Soit $\sum a_n x^n$ une série entière de fonction $f$. Alors la primitive de $f$ est la fonction de la série entière $\sum \frac{a_n}{n+1}x^{n+1}$, de même rayon que $\sum a_n x^n$.
\end{prop}
{\small{\begin{proof}
	On applique les résultats de dérivation la fonction définie par la série entière $\sum \frac{a_n}{n+1}x^{n+1}$.
\end{proof}}}

\begin{theo}[Formule de Cauchy]
	Soit $\sum a_n z^n$ une série entière de rayon de convergence $R > 0$, et $f$ la fonction série entière sur son disque de convergence. Alors,
	\begin{displaymath}
	\begin{array}{cc}
	\forall r \in \left]0, R\right[, \forall n \in \N, & 2\pi r^n a_n = \int_{0}^{2\pi}f\left(re^{i \theta}\right)e^{-ni\theta} d\theta
	\end{array}
	\end{displaymath}
\end{theo}
{\small{\begin{proof}
	Soient $r \in \left]0, R\right[$ et $n \in \N$. On peut écrire
	\begin{displaymath}
	\int_{0}^{2\pi}f\left(re^{i \theta}\right)e^{-ni\theta} d\theta = \int_{0}^{2\pi}\left(\sum_{p=0}^{\infty} a_pr^pe^{i(p-n) \theta}\right) d\theta = \sum_{p=0}^{\infty} a_pr^p \int_{0}^{2\pi}\left(e^{i(p-n) \theta}\right) d\theta
	\end{displaymath}
	(\textblue{on a le droit de faire l'inversion car la série de fonction converge normalement sur $[0, 2\pi]$ car $\sum \left|a_p\right|r^p$ converge pour $0 \leq r < R$}). Comme 
	\begin{displaymath}
	\int_{0}^{2\pi}\left(e^{i(p-n) \theta}\right) d\theta = \left \{ \begin{array}{cc}
	0 & \text{si } p \neq n \\
	2\pi & \text{si } p = n \\
	\end{array}\right.
	\end{displaymath}
	on conclut.
\end{proof}}}

\begin{appli}[Théorème de Liouville \protect{\cite[p.248]{Gourdon-analyse}}]
	Soit $\sum a_n z^n$ une série entière dont le rayon de convergence est infinie. Soit $f : \C \to \C$ sa fonction associée. Si $f$ est bornée alors $f$ est constante.
\end{appli}
{\footnotesize{\begin{proof}
	Soit $M$ un majorant de $\left|f\right|$ sur $\C$. On a:
	\begin{displaymath}
	\begin{array}{ccccc}
	\forall n \in \N, \forall r > 0 & ~ & a_nr^n = \frac{1}{2\pi}\int_{0}^{2\pi}f\left(re^{i \theta}\right)e^{-ni\theta} d\theta & \text{ donc } & \left|a_n\right|r^n < M
	\end{array}
	\end{displaymath}
	Si $n \in \N^*$, la majoration $\left|a_n\right| < \frac{M}{r^n}$ reste vrai pour tout $r > 0$. Si on fait tendre $r$ vers $0$ (\textblue{ce qui est licite par la remarque précédente}), alors $a_n = 0$ pour tout $n \in \N^*$. D'où le résultat.
\end{proof}}}

\begin{req}
	Une généralisation étonnante de ce théorème est le théorème de Picard qui dit que toute fonction série entière qui évite deux valeurs est constante. Autrement dit, s'il existe deux valeurs distinctes $a$ et $b$ dans $\C$ telle que $\forall z \in \C$, $f(z) \neq a$ et $f(z) \neq b$, alors $f$ est constante.
\end{req}

\begin{theo}[Égalité de Parseval]
	Soit $\sum a_nz^n$ une série entière de rayon de convergence $R > 0$, et $f$ sa fonction sur son disque de convergence. Alors, pour tout $r \in \left]0, R\right[$, la série $\sum \left|a_n\right|^2r^{2n}$ converge et on a 
	\begin{displaymath}
	\sum_{n = 0}^{\infty} \left|a_n\right|^2r^{2n} = \frac{1}{2\pi}\int_{0}^{2\pi} \left|f\left(re^{i\theta}\right)\right|^2 d\theta
	\end{displaymath}
\end{theo}
{\small{\begin{proof}
	Application de l'égalité de Parseval dans la cas des séries de Fourier à la fonction $f(re^{i\theta})$
\end{proof}}}
