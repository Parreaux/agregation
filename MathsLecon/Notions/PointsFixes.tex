%DEV: Newton; Cauchy-Lipschitz
Le théorème de point fixe joue un rôle central en calcul différentiel, comme clef des théorèmes des fonctions inverses, des fonctions implicites et du théorème de Cauchy-Lipschitz \cite[p.137]{Rouviere}. Ce théorème est puissant (avec une preuve relativement simple) puisqu'il donne existence, unicité et une bonne méthode d'approximation de la solution.

\begin{definition}
	Soient $X$ un espace métrique complet (non vide), $d$ la distance de $X$, et $F$ une application de $X$ dans lui-même. On dit que $F$ est contractante si et seulement si, il existe une constante positive $k < 1$ telle que pour tout $x, y \in X$, $d(F(x), F(y)) \leq kd(x, y)$.
\end{definition}

\begin{theo}
	Soient $X$ un espace métrique complet (non vide), $d$ la distance de $X$, et $F$ une application de $X$ dans lui-même. On suppose $F$ contractante. 
	
	Alors, il existe un unique point fixe $a \in X$ tel que $F(a) = a$ (point fixe de $F$). De plus, ce point peut s'obtenir comme limite de la suite $\left(x_n\right)_{n\in \N}$ des itérés, définie par récurrence à partir d'un point quelconque $x_0 \in X$ selon $x_{n+1} = F(x_n)$. On a plus précisément pour $n \geq 1$, $d(x_n, a) \leq \frac{k^n}{1 - k}d(x_0, x_1)$.
	\label{thm:pointFixe}
\end{theo}
\begin{footnotesize}
	\begin{proof}[Démonstration \protect{\cite[p.159]{Rouviere}}]
		Pour $n \geq 1$ on a $d(x_n, x_{n+1}) = d(F(x_{n-1}), F(x_n)) \leq kd(x_{n-1}, x_n)$ \textblue{(car $F$ est contractante)}. Par récurrence, on en déduit que $d(x_n, x_{n+1}) \leq k^nd(x_0, x_1)$. Par inégalité triangulaire, on a alors, pour $n \geq 0$ et $n \geq 0$, $d(x_n, x_{n+p}) \leq \left(k^n + k^{n + 1} + \dots + k^{n+p}\right)d(x_0, x_1) \leq \frac{k^n}{1 - k}d(x_0, x_1)$ \textblue{(sommation d'une série géométrique de raison $k < 1$)}.
	
		On a donc $d(x_n, x_{n+p}) \leq \epsilon$ pour $n$ assez grand et $p \geq 0$ \textblue{(par l'inégalité précédente et $k < 1$)}. Les $x_n$ forment une suite de Cauchy de $X$ qui converge vers un point $a$. En faisant tendre $p$ vers $+ \infty$ dans l'inégalité ci-dessous, on obtient $d(x_n, a) \leq \frac{k^n}{1 - k}d(x_0, x_1)$ \textblue{(caractère Cauchy de la suite)}.
	
		Enfin, $x_{n + 1} = F(x_n)$ tend vers $a$ \textblue{(par ce qui précède)} et vers $F(a)$ \textblue{($F$ est continue car contractante)}. D'où $F(a) = a$ et $a$ est un point fixe de $F$.
	
		Pour l'unicité, s'il existait un deuxième point fixe $b$, alors $d(a, b) = d(F(a), F(b)) \leq kd(a, b)$ d'où $d(a, b) = 0$ et par les propriétés de la distance $a = b$.
	\end{proof}
\end{footnotesize}

\begin{cex}
	Soient $X = \left]0, 1 \right[$ et $F(x) = \frac{x}{2}$ une application contractante de $X$ dans lui-même. Cependant $F$ est sans points fixes car $X$ n'est pas complet.
\end{cex}

\begin{cex}
	Soient $X = \left[0, 1\right]$ un espace complet et $F(x) = \sqrt{x^2 + 1}$ une application contractante. Cependant $F$ est sans points fixes car $F$ ne s'applique pas de $X$ dans lui-même puisque $F(X) = \left[1, \sqrt{2}\right]$.
\end{cex}

\begin{cex}
	Soient $X = \R$ un espace complet et $F(x) = \sqrt{x^2 + 1}$ de $X$ dans lui-même. Cependant $F$ est sans points fixes car $F$ n'est pas contractante dans $X$ même si $\left|F(x) - F(y)\right| < \left|x - y\right|$ pour $x \neq y$.
\end{cex}

\begin{cex}
	Soient $X = \left[0, \frac{\pi}{2}\right]$ un espace complet et $F(x) = \sin x$ de $X$ dans lui-même mais non-contractante. Cependant $F$ possède un unique point fixe dont la suite des itérés converge très lentement.
\end{cex}

\begin{cex}
	Soient $X$ un espace complet quelconque et $F(x) = x$ de $X$ dans lui-même mais non-contractante. Cependant tout point de $X$ est point fixe de $F$.
\end{cex}

\begin{cor}[\protect{\cite[p.159]{Rouviere}}]
	Soient $X$ un espace métrique complet et $F$ une application de $X$ dans lui-même. On suppose qu'une certaine itéré $F^p$ est contractante, où $p \geq 1$. 
	
	Alors, $F$ a un unique point fixe qui est limite de la suite $\left(F^n(x_0)\right)_{n \in \N}$ avec $x_0 \in X$ quelconque. De plus, ka vitesse de convergence de cette suite est géométrique selon les puissances de $k_p^{\frac{1/p}{den}}$ où $k_p$ est la constante de  Lipschitz de $F^p$.
\end{cor}
\begin{footnotesize}
	\begin{proof}
		Le théorème~\ref{thm:pointFixe} donne $a$, unique point fixe de $F^p$ et limite de la suite des $\left(F^{np}(x_0)\right)_{n \geq 0}$ pour tout $x_0 \in X$. Montrons alors que $a$ est l'unique point fixe de $F$. Comme $F(a) = F(F^p(a)) = F^{p + 1}(a) = F^p(F(a))$, le point $F(a)$ est aussi point fixe de $F^p$. D'où par l'unicité de ce point fixe, $F(a) = a$, soit $a$ est un point fixe de $F$. Inversement, tout point fixe de $F$ est point fixe de $F^p$. D'où par l'unicité sur $F^p$, $a$ est l'unique point fixe de $F$.
		
		D'après le théorème~\ref{thm:pointFixe}, on a $d\left(F^{np}(x_0), a\right) \leq \frac{k_p^n}{1- k_p}d(x_0, F^p(x_0))$. En remplaçant le point initial $x_0$ par $F^q(x_0)$ on en déduit $d\left(F^{np+q}(x_0), a\right) \leq \frac{k_p^n}{1 - k_p}d\left(F^q(x_0), F^{p+q}(x_0)\right)$ pour $q \in \{0, \dots, p-1\}$ et $n \geq 0$. Par division euclidienne, tout entier $m$ s'écrit $m = np + q$ avec $0 \leq q \leq p - 1$. Alors, $n > \left(\frac{m}{p}\right)-1$ et on obtient facilement $d(F^m(x_0), a) \leq Ck_p^{\frac{m}{p}}$, pour $m \in \N$ où $C$ est une constante indépendante de $m$. La suite des itérés $F^m(x_0)$ à partir d'un $x_0$ quelconque, converge donc vers le point fixe. La vitesse de convergence est donc géométrique selon les puissances de $k_p^{\frac{1/p}{den}}$.
	\end{proof}
\end{footnotesize}

\begin{req}
	Ce raffinement du résultat possède quelques applications que nous allons détailler ici (dans une preuve de Cauchy--Lipschitz par exemple).
\end{req}

\begin{appli}[Point fixe et équations intégrales \protect{\cite[p.175]{Rouviere}}]
	Soient $I = \left[a, b\right]$ un intervalle compact, $K: I \times I \to \R$ une fonction continue et $E$ l'espace des fonctions réelles continues sur $I$, muni de la norme de la convergence uniforme. On se donne $\varphi \in E$. Pour tout $t \in I$,
	\begin{enumerate}
		\item l'équation de Fredholm $x(t) = \varphi(t) + \int_{a}^{b} K(s, t)x(s)ds$ admet une unique solution $x \in E$ si $(b - a)\max_{s, t \in I}\left|K(s, t)\right| < 1$.
		\item l'équation $x(t) = \varphi(t) + \int_{0}^{1} \lambda x(s)ds$ admet une solution unique si $\lambda \neq 1$ et admet une solution si $\lambda = 1$ et $\int_{0}^{1} \varphi(t)dt = 0$.
		\item l'équation de Volterra $x(t) = \varphi(t) + \int_{a}^{t} K(s, t)x(s)ds$ admet toujours une unique solution $x \in E$.
	\end{enumerate}
\end{appli}
\begin{footnotesize}
	\begin{proof}
		\begin{enumerate}
			\item Soit $F(x)(t) = \varphi(t) + \int_{a}^{b} K(s, t)x(s)ds$ pour $t \in I$. A chaque $x \in E$, l'opérateur $F$ associe la fonction $F(x) \in E$ et le problème s'écrit donc $F(x) = x$. On a 
			\begin{displaymath}
			\left|F(x)(t) - F(y)(t)\right| = \left|\int^b_a K(s, t)(x(s) - y(s))ds\right| \leq (a-b) \lVert K\rVert_{\infty}\lVert x-y\rVert_{\infty}
			\end{displaymath}
			D'où $\lVert F(x) - F(y)\rVert_{\infty}  \leq (a-b) \lVert K\rVert_{\infty}\lVert x-y\rVert_{\infty}$. On en déduit que $F$ est contractante sur $E$ si $(b - a)\lVert K\rVert_{\infty} = (b - a)\max_{s, t \in I}\left|K(s, t)\right| < 1$. Par complétude de $E$, lorsque $F$ est contractante, elle admet un unique point fixe.
			
			\item L'équation proposée entraîne que $x(t) = \varphi(t) + c$ où $c$ est une constante. En intégrant de $0$ à $1$ l'intégrale donnée, il vient que $\int_0^1 x(t)dt = \int_{0}^1 \varphi(t)dt + \lambda \int_{0}^{1} x(s)ds$, d'où en reportant $x = \varphi + c$, $(1-\lambda)c = \lambda \int_{0}^{1} \varphi(t)dt$.
			\begin{itemize}
				\item Si $\lambda \neq 1$, il y a une unique solution quelle que soit $\varphi \in E$: $x(t) = \varphi(t) + \frac{\lambda}{1 - \lambda} \int_{0}^{1} \varphi(s)ds$.
				\item Si $\lambda = 1$, il y a une solution si et seulement si $\int_{0}^{1}\varphi(t) dt= 0$: $x(t) = \varphi(t) + c$ où $c$ est une constante arbitraire.
			\end{itemize}
			
			\item Soit $F(x)(t) = \varphi(t) + \int_{a}^{t} K(s, t)x(s)ds$, l'équation intégrale devient donc $F(x) = x$. On a pour $x, y \in E$ et $a \leq t \leq b$, 
			\begin{displaymath}
			\left|F(x)(t) - F(y)(t)\right| = \left|\int^b_a K(s, t)(x(s) - y(s))ds\right| \leq \lVert K\rVert_{\infty}\int^b_a\left|x(s) - y(s)\right|ds \leq (t-a) \lVert K\rVert_{\infty}\lVert x-y\rVert_{\infty}
			\end{displaymath}
			On en déduit par récurrence sur $p \geq 1$, $\left|F^p(x)(t) - F^p(y)(t)\right| \leq \frac{\left((t-a) \lVert K\rVert_{\infty}\right)^p}{p!}\lVert x-y\rVert_{\infty}$ et finalement $\lVert F^p(x)(t) - F^p(y)(t)\rVert_{\infty} \leq \frac{\left((b-a) \lVert K\rVert_{\infty}\right)^p}{p!}\lVert x-y\rVert_{\infty}$.
			
			Si $p$ est choisi suffisamment grand, $F^p$ est contractante sur $E$ donc admet un unique point fixe $x$, qui est aussi l'unique point fixe de $F$.
		\end{enumerate}
	\end{proof}
\end{footnotesize}
