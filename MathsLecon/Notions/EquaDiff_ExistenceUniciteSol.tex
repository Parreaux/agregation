%DEVs: Cauchy-Lipschitz
%LECONs: 220 (Equation differentiel)
Avant de se mettre à la recherche des solutions, nous voulons nous assurer qu'elles existent. C'est Cauchy qui est le premier à avoir formaliser l'existence de solution (la différence entre le local et le global apparaît à ce moment dans les mathématiques).

\begin{lemme}
	Une fonction $y : I \to \R^m$ est une solution du problème de Cauchy de données initiales $(t_0, y_0)$ si et seulement si
	\begin{enumerate}
		\item $y$ est continue et $\forall t \in I$, $(t, y(t)) \in U$.
		\item $\forall t \in I$, $y(t) = y_0 + \int_{t_0}^{t} f(u, y(u)) du$.
	\end{enumerate}
\end{lemme}
\begin{footnotesize}
	\begin{proof}
		\begin{description}
			\item[$\Rightarrow$] Si $y$ vérifie 1 et 2 alors $y$ est différentiable et $y(t_0) = y_0$, $y'(t) = f(t, y(t))$.
			\item[$\Leftarrow$] Si $y$ est solution d'un problème de Cauchy, $y$ vérifie automatiquement 1 et on déduit 2 en intégrant.
		\end{description}
	\end{proof}
\end{footnotesize}

Pour montrer l'existence d'une solution, il faut étudier notre intégrale. On commence par montrer qu'une solution ne peut pas trop s'éloigner de son point initial.

\begin{definition}
	On dit que $C$ est un cylindre de sécurité pour l'équation $(E)$ si toute solution $y : I \to \R^m$ du problème de Cauchy $y(t_0) = y_0$ avec $I \subset \left[t_0 - T, t_0 + T\right]$ reste contenue dans $\overline{B}(y_0, r_0)$.
\end{definition}

\begin{theo}[Cauchy--Peano--Arzela]
	Soit $C = \left[t_0 - T, t_0 + T\right] \times \overline{B}(y_0, r_0)$ \textblue{(avec $ T \leq \min\left(T_0, \frac{r_0}{M}\right)$)} un cylindre de sécurité pour l'équation $(E)$ : $y' = f(t, y)$. Alors, il existe une solution $y : \left[t_0 - T, t_0 + T\right] \to \overline{B}(y_0, r_0)$ de $(E)$ avec condition initiale $y'(t_0) = y_0$.
\end{theo}
\begin{footnotesize}
	\begin{proof}[Idée de la preuve]
		Utilisation du théorème d'Ascoli pour montrer l'existence d'une sous-suite uniformément convergente de solutions approchées.
	\end{proof}
\end{footnotesize}

\begin{ex}
	$y' = 3 \left|y\right|^{\frac{2}{3}}$  admet deux solutions maximales pour le problème de Cauchy: $y_{(1)}(t) = 0$ et $y_{(2)}(t) = t^3$ pour $t \in \R$.
\end{ex}

\begin{cor}
	Pour tout point $(t_0, y_0) \in U$, il passe au moins une solution maximale $y : I \to \R^m$ de $(E)$. De plus, l'intervalle de définition $I$ de toute solution maximale est ouvert (on a pas d'unicité en général).
\end{cor}
\begin{footnotesize}
	\begin{proof}
		On utilise la théorème qui nous assure de pouvoir prolonger la solution donnée par le théorème de Cauchy--Peano.
	\end{proof}
\end{footnotesize}

\begin{definition}
	Une fonction $f: U \to \R^m$ où $U$ est un ouvert de $\R \times \R^m$ est dite localement lipschitzienne en $y$ si pour tout point $(t_0, x_0) \in U$ il existe un cylindre de $C_0 = \left[t_0 - T_0, t_0 + T_0\right] \times \overline{B}(y_0, r_0)$ et une constante $j \geq 0$ tel que $f$ soit $k$-lipschitzienne en $y$ sur $C_0$: $\forall (t, y_1), (t, y_2) \in C_0$, $\lVert f(t, y_1) - f(t, y_2) \rVert \leq k\lVert y_1 - y_2 \rVert$.
\end{definition}

\begin{req}
	Pour qu'une fonction soit localement lipschitzienne en $y$ sur $U$, il suffit que ces dérivées partielles en $y$ soient continues (théorème des accroissements finis).
\end{req}

Nous donnons maintenant le théorème de Cauchy--Lipschitz local. Elle permet d'étudier beaucoup plus d'équations différentielles que le théorème local mais ne garantie plus l'existence et l'unicité sur le domaine de définition complet (on se limite à du local). 
	
\begin{theo}[Théorème de Cauchy--Lipschitz local]
	Si $f : U \to \R^m$ est localement lipschitzienne en $y$, alors pour tout cylindre de sécurité $C = \left[t_0 - T, t_0 + T\right] \times \overline{B}(y_0, r_0)$, le problème de Cauchy avec condition initiale $(t_0, y_0)$ admet une unique solution exacte $y : \left[t_0 - T, t_0 + T\right] \to U$. De plus, toute suite $y_{(p)}$ de solutions $\epsilon_p$-approchées avec $\epsilon_p$ tendant vers $0$ converge uniformément vers la solution exacte $y$ sur $\left[t_0 - T, t_0 + T\right]$.
\end{theo}
\begin{footnotesize}
	\begin{proof}[Idée de la preuve]
		Cette version est une adaptation de la démonstration que nous présentons avec l'utilisation de cylindre de sécurité qui permettent de garantir que les hypothèses (précédentes) sont bien vérifiée.
	\end{proof}
\end{footnotesize}

\begin{cor}
	Soient $U$ un ouvert de $\R \times \R^m$ et $f: U \to \R^m$ de classe $\mathcal{C}^1$. Alors pour toute données initiale $(t_0, x) \in U$, le système différentielle 
	\[ \left  \{
	\begin{array}{c @{ = } l}
		y'& f(t,y) \\
		y(t_0) & x\\
	\end{array}
	\right.
	\]
	admet une solution maximale unique.
\end{cor}
\begin{footnotesize}
	\begin{proof}
		On remarque maintenant que par hypothèse, $f$ n'est plus que localement lipschitzienne en $y$ \textblue{(on majore la norme de la différentielle sur le bon compact et on utilise l'inégalité de moyenne)}.
	\end{proof}
\end{footnotesize}
	
\begin{req}
	Comme pour le théorème de Cauchy--Peano, cette solution est dite maximale car elle ne peut être prolongée sur un intervalle de temps strictement plus grand. On notera que l'énoncé ne précise pas l'intervalle, ni sa taille, (il dépend de la fonction $f$ et des données initiales).
\end{req}

\begin{theo}[Théorème de Cauchy--Lipschitz global \protect{\cite[p.170]{Rouviere}}]
	Soient $\R^m$ muni de sa norme $\mathopen{||} . \mathclose{||}$, $I$ un intervalle de $\R$ et $f : I \times \R^m \to \R^m$ une application continue, supposée globalement lipschitzienne en $y$ au sens suivant: pour tout compact $K \subset I$, il existe $k > 0$ tel que pour tous $t \in K$, $y, z \in \R^m$,
	\begin{displaymath}
	\mathopen{||} f(t, y) - f(t, z) \mathclose{||} \leq k \mathopen{||} y- z \mathclose{||}
	\end{displaymath}
	Alors, pour tous $t_0 \in I$ et $x \in \R^m$, le problème de Cauchy
	\[ \left  \{
	\begin{array}{c @{ = } l}
	y'& f(t,y) \\
	y(t_0) & x\\
	\end{array}
	\right.
	\]
	admet une unique solution $t \mapsto y(t)$ qui est globale (définie sur $I$ tout entier).
\end{theo}

\begin{ex}[Équation du pendule \protect{\cite[p.170]{Rouviere}}]
	Étudions l'équation du pendule 
	$ \left  \{
	\begin{array}{c @{ = } l}
	u''& -\sin u \\
	u(0) & a\\
	u'(0) & b\\
	\end{array}
	\right.
	$. On se ramène à un système différentiel d'ordre 1: $y' = f(y)$, $y(0) = x$ où $y =\left(\begin{array}{c} u \\ u' \\ 	\end{array}\right)$, $x =\left(\begin{array}{c} a \\ b \\ \end{array}\right)$ et $f(y) =\left(\begin{array}{c} u' \\ - sin u \\ \end{array}\right)$. Pour la norme 1, on vérifie (grâce à l'inégalité de la moyenne) que $f$ est globalement lipschitzienne. On applique donc le théorème qui nous donne l'existence et l'unicité d'une solution sur tout $\R$.
\end{ex}