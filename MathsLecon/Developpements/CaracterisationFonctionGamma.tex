\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
		};
		%\draw[color=blue] (box.north west)--(box.north east);
		\node[title] at (box.north) {#2};
		\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\Cun}{\mathcal{C}^1}
	\newcommand{\Cdeux}{\mathcal{C}^2}
	\newcommand{\Cinf}{\mathcal{C}^{\infty}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{OliveGreen}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	\newcommand{\textmagenta}[1]{\textcolor{DarkOrchid}{#1}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\newcommand{\pcite}[1]{\protect{\cite{#1}}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{remarque}{Remarque}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	
	\title{Caractérisation de la fonction $\Gamma$ sur $\R$}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Rudin \cite[p.179]{Rudin}.
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 229 (Fonctions monotones, fonctions convexes); 239 (Fonctions définie par une intégrale); 265 (Fonctions usuelles et spéciales).
		}}
	
	\section{Introduction}
	
	La fonction $\Gamma$ est une fonction spéciale permettant de prolonger la fonction factorielle sur $\R$ puis $\C$ (sauf sur les entiers négatifs). C'est une fonction très importante car elle possède de nombreuses propriétés qui ont des applications dans l'ensemble des mathématiques (lien avec la fonction $\zeta$ de Riemman). L'objet de ce développement est d'en donner une caractérisation sur les réels.
	
	\section{Caractérisation de la fonction $\Gamma$}
	
	\begin{theo}
		Soit $f$ une fonction définie, positive sur $\left]0, +\infty\right[$ et vérifiant
		\begin{enumerate}
			\item $f(x + 1) = xf(x)$;
			\item $f(1) = 1$;
			\item La fonction $\ln f$ est convexe sur $\left]0, +\infty\right[$.
		\end{enumerate}
		Alors, $f = \Gamma$.
	\end{theo}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement}}}{
		\begin{enumerate}
			\item La fonction $\Gamma$ vérifie les trois propriétés.
			\item Il existe une unique fonction vérifiant ces trois propriétés pour $x > 0$.
		\end{enumerate}
	}

	\begin{proof}
		Soit $f$ une telle fonction vérifiant les trois propriétés.
		
		\paragraph{Étape 1 : la fonction $\Gamma$ vérifie les trois propriétés}
		\begin{enumerate}
			\item Montrons que $\Gamma(x + 1) = x\Gamma(x)$.
			\begin{displaymath}
			\begin{array}{ccll}
			\Gamma(x + 1) & = & \int_{0}^{+\infty} t^xe^{-t} dt & \textblue{\text{(Définition de $\Gamma$)}} \\
			& = & \lim\limits_{A \to +\infty}\int_{0}^{A} t^xe^{-t} dt & \textblue{\text{(Définition de la limite)}} \\
			& = & \lim\limits_{A \to +\infty}\left(\left[-t^xe^{-t}\right]_0^A - \int_{0}^{A} -xt^{x - 1}e^{-t} dt\right) & \textblue{\text{(Par intégration par partie $u = t^x$ et $v = -e^{-t}$)}} \\
			& = & \lim\limits_{A \to +\infty}\left(t^Ae^{-A} +x \int_{0}^{A} t^{x - 1}e^{-t} dt\right) & \textblue{\text{(Calcul et manipulation de l'intégrale)}} \\
			& = & x \int_{0}^{+\infty} t^{x - 1}e^{-t} dt & \textblue{\text{(Calcul de la limite)}} \\
			& = & x\Gamma(x) & \textblue{\text{(Définition de la fonction $\Gamma$)}}
			\end{array}
			\end{displaymath} 
			
			\item Montrons que $\Gamma(1) = 1$.
			\begin{displaymath}
			\begin{array}{ccll}
			\Gamma(1) & = & \int_{0}^{+\infty} e^{-t} dt & \textblue{\text{(Définition de $\Gamma$ évaluer en $1$)}} \\
			& = & \left[-e^{-t}\right]_0^{+\infty} & \textblue{\text{(Primitive)}} \\
			& = & e^{0} & \textblue{\text{(Calcul)}} \\
			& = & 1 & \textblue{} \\
			\end{array}
			\end{displaymath}
			
			\item Montrons que $\ln \Gamma$ est convexe sur $\left]0, +\infty\right[$. Soient $p > 1$ et $q$ tels que $\frac{1}{p} + \frac{1}{q} = 1$.
			\begin{displaymath}
			\begin{array}{ccll}
			\Gamma(\frac{x}{p} + \frac{y}{q}) & = & \int_{0}^{+\infty} t^{\frac{x}{p} + \frac{y}{q}-1}e^{-t} dt & \textblue{\text{(Définition de $\Gamma$ évaluer en $1$)}} \\
			& = & \int_{0}^{+\infty} t^{\frac{x}{p} + \frac{y}{q} - \frac{1}{p} - \frac{1}{q}}e^{- \frac{t}{p} - \frac{t}{p}} dt & \textblue{\text{(Car $1 = \frac{1}{p} + \frac{1}{q}$)}} \\
			& = & \int_{0}^{+\infty} \underbrace{\left(t^{\frac{x}{p} - \frac{1}{p}}e^{- \frac{t}{p}}\right)}_{f} \underbrace{\left(t^{\frac{y}{q} - \frac{1}{q}}e^{-\frac{t}{q}}\right)}_g dt & \textblue{\text{(Manipulation des puissances)}} \\
			& \leq & \left(\int_{0}^{+\infty}\left(t^{\frac{x}{p} - \frac{1}{p}}e^{- \frac{t}{p}}\right)^p dt\right)^{\frac{1}{p}} \left(\int_{0}^{+\infty}\left(t^{\frac{y}{q} - \frac{1}{q}}e^{-\frac{t}{q}}\right)^qdt\right)^{\frac{1}{q}} & \textblue{\text{(Par Holder)}} \\
			& = & \left(\int_{0}^{+\infty}t^{x - 1}e^{-t} dt\right)^{\frac{1}{p}} \left(\int_{0}^{+\infty}t^{y - 1}e^{-t}dt\right)^{\frac{1}{q}} & \textblue{\text{(Utilisation de la puissa,ce)}} \\
			& = & \Gamma(x)^{\frac{1}{p}} + \Gamma(x)^{\frac{1}{q}} & \textblue{} \\
			\end{array}
			\end{displaymath}
			Appliquons la définition de la connexité à la fonction $\ln \Gamma$ avec $\lambda = \frac{1}{p}$.
			\begin{displaymath}
			\begin{array}{ccll}
			\ln\left(\Gamma(\frac{x}{p} + \frac{y}{q})\right) & \leq & \ln\left(\Gamma(\frac{x}{p})^{\frac{1}{p}})\Gamma(\frac{y}{q})^{\frac{1}{q}})\right) & \textblue{\text{(Croissance de $\ln$ et précédent)}} \\
			& = & \frac{1}{p}\ln\left(\Gamma(\frac{x}{p}))\right) + \frac{1}{q}\ln\left(\Gamma(\frac{y}{q}))\right) & \textblue{\text{Propriété de la fonction $\ln$}} \\
			\end{array}
			\end{displaymath}
		\end{enumerate}
		
		\paragraph{Étape 2 : il existe une unique fonction vérifiant ces trois propriétés pour $x > 0$}
		Il existe une unique fonction vérifiant les trois propriétés pour $x > 0$.
		\begin{itemize}
			\item Par l'étape 1, il suffit de l'établir pour $x \in \left]0, 1\right]$ \textblue{(on translate ensuite)}.
			\item Soit $f$ une fonction vérifiant ces trois propriétés. On pose $\varphi = \ln f$.
			\begin{itemize}
				\item On a $\varphi(x + 1) = \ln x + \varphi(x)$
				\begin{displaymath}
				\begin{array}{ccll}
				\varphi(x + 1) & = & \ln(f(x + 1)) & \textblue{\text{(Définition de $\varphi$)}} \\
				& = & \ln(x + f(x)) & \textblue{\text{(Propriété $1$ de $f$)}} \\ 
				& = & \ln(x) + \ln(f(x)) & \textblue{\text{(Propriété de $\ln$)}} \\ 
				& = & \ln(x) + \varphi(x) & \textblue{\text{Définition de $\varphi$}} \\
				\end{array}
				\end{displaymath}
				\item On a $\varphi(1) = 1$
				\begin{displaymath}
				\begin{array}{ccll}
				\varphi(1) & = & \ln(f(1)) & \textblue{\text{(Définition de $\varphi$)}} \\
				& = & \ln(0) & \textblue{\text{(Propriété $2$ de $f$)}} \\ 
				& = & 1 & \textblue{\text{(Propriété de $\ln$)}} \\ 
				\end{array}
				\end{displaymath}
				\item $\varphi$ est connexe \textblue{(par la propriété $3$ de $f$)}.
			\end{itemize}
			\item Soit $n \in \N^*$ et $x \in \left]0, 1\right]$. Par connexité de $\varphi$, on applique l'inégalité des trois pentes.
			\begin{itemize}
				\item Par l'inégalité des trois pentes, appliquée en $n \leq n + 1 + x \leq n + 2$ \textblue{(car $x < 1$)}.
				\begin{displaymath}
				\begin{array}{cccccl}
				\frac{\varphi(n + 1) - \varphi(n)}{n + 1 - n} & \leq & \frac{\varphi(n + 1 + x) - \varphi(n)}{n + 1 + x - n} & \leq & \frac{\varphi(n + 1 + x) - \varphi(n)}{n + 1 + x - n} & \textblue{\text{(Inégalité des trois pentes)}} \\
				& & & & & \\
				\varphi(n + 1) - \varphi(n) & \leq & \frac{\varphi(n + 1 + x) - \varphi(n)}{1 + x} & \leq & \frac{\varphi(n + 1 + x) - \varphi(n)}{x} & \textblue{\text{(Calcul)}} \\
				& & & & & \\
				\varphi(n + 1) - \varphi(n) & \leq & \frac{\varphi(n + 1 + x) - \varphi(n)}{x} & \multicolumn{3}{l}{\textblue{\text{(Inégalité)}}} \\
				\end{array}
				\end{displaymath}
				\item Par l'inégalité des trois pentes, appliquée en $n + 1 \leq n + 1 + x \leq n + 2$ \textblue{(car $x < 1$)}.
				\begin{displaymath}
				\begin{array}{cccl}
				\frac{\varphi(n + 1 + x) - \varphi(n + 1)}{n + 1 + x - n - 1} & \leq & \frac{\varphi(n + 2) - \varphi(n + 1)}{n - 1 - n + 2} & \textblue{\text{(Inégalité des trois pentes)}} \\
				& & & \\
				\frac{\varphi(n + 1 + x) - \varphi(n + 1)}{x} & \leq & \varphi(n + 2) - \varphi(n + 1) & \textblue{\text{(Calcul)}} \\
				\end{array}
				\end{displaymath}
			\end{itemize}
			\item On obtient alors la double inégalité suivante:
			\begin{displaymath}
			\begin{array}{cccccl}
			\varphi(n + 1) - \varphi(n) & \leq & \frac{\varphi(n + 1 + x) - \varphi(n + 1)}{x} & \leq & \varphi(n + 2) - \varphi(n + 1) & \textblue{\text{(Double inégalité)}} \\
			& & & & & \\
			\varphi(n) + \ln(n) - \varphi(n) & \leq & \frac{\varphi(n + 1 + x) - \varphi(n)}{x} & \leq & \varphi(n + 1) + \ln(n + 1) - \varphi(n + 1) & \textblue{(\varphi(x + 1) = \varphi(x) + \ln x)} \\
			& & & & & \\
			\ln(n) & \leq & \frac{\varphi(n + 1 + x) - \varphi(n)}{x} & \leq & \ln(n + 1) & \textblue{\text{(Simplication)}} \\
			\end{array}
			\end{displaymath}
			\item Étudions de plus près cette inégalité.
			\begin{itemize}
				\item $\varphi(n + 1 + x) = \varphi(x) + \ln(x(x + 1) \dots (x + n))$. Montrons le par récurrence sur $n \in \N$.
				\begin{description}
					\item[Initialisation: $n = 0$] Par une des propriétés de $\varphi$.
					\item[Hérédité: $n \in \N$] Soit $n$ tel que $\varphi(n + 1 + x) = \varphi(x) + \ln(x(x + 1) \dots (x + n))$.
					\begin{displaymath}
					\begin{array}{ccll}
					\varphi(n + 2 + x) & = & \varphi(n + 1 + x) + \ln(n + 1 + x) & \textblue{\text{(Propriété de $\varphi$)}} \\
					& = & \varphi(x) + \ln(x(x + 1) \dots (x + n)) + \ln(n + 1 + x) & \textblue{\text{(Hypothèse de récurrence)}} \\
					& = & \varphi(x) + \ln(x(x + 1) \dots (x + n)(n + 1 + x)) & \textblue{\text{(Propriété de $\ln$)}} \\
					\end{array}
					\end{displaymath}
				\end{description}
				\item $\varphi(n + 1) = \ln(n!)$. Montrons le par récurrence sur $n \in \N$.
				\begin{description}
					\item[Initialisation: $n = 0$] Par une des propriétés de $\varphi$ et de $\ln$.
					\item[Hérédité: $n \in \N$] Soit $n$ tel que $\varphi(n + 1) = \ln(n!)$.
					\begin{displaymath}
					\begin{array}{ccll}
					\varphi(n + 2) & = & \varphi(n + 1) + \ln(n + 1) & \textblue{\text{(Propriété de $\varphi$)}} \\
					& = & \ln(n!) + \ln(n + 1) & \textblue{\text{(Hypothèse de récurrence)}} \\
					& = & \ln((n + 1)!) & \textblue{\text{(Propriété de $\ln$)}} \\
					\end{array}
					\end{displaymath}
				\end{description}
			\end{itemize}
			\item On en déduit que 
			\begin{displaymath}
			\begin{array}{cccccl}
			0 & \leq & \frac{\varphi(n + 1 + x) - \varphi(n)}{x} - \ln n & \leq & \ln(n + 1) - \ln n & \textblue{\text{(Soustraction de $\ln n$)}} \\
			& & & & & \\
			0 & \leq & \frac{\varphi(x) + \ln(x(x + 1) \dots (x + n)) - \ln(n!)}{x} - \ln n & \leq & \ln(n + 1) - \ln n & \textblue{\text{(Propriétés)}} \\
			& & & & & \\
			0 & \leq & \varphi(x) - (\ln(x \dots (x + n)) - \ln(n!) + x\ln n) & \leq & x\left(\ln(n + 1) - \ln n \right)& \textblue{\text{(Mutiplication par $x$)}} \\
			& & & & & \\
			0 & \leq & \varphi(x) - \ln\left(\frac{n! n^x}{x(x + 1) \dots (x + n)}\right) & \leq & x\left(\ln\left(\frac{n + 1}{n}\right) \right)& \textblue{\text{(Propriété de $\ln$)}} \\
			& & & & & \\
			\end{array}
			\end{displaymath}
			Comme $x\left(\ln\left(\frac{n + 1}{n}\right) \right)$ tend vers $0$ lorsque $n$ tend vers $\infty$, on a que $\varphi(x) - \ln\left(\frac{n! n^x}{x(x + 1) \dots (x + n)}\right)$ tend vers $0$ lorsque $n$ tend vers $\infty$.
		\end{itemize}
		
		On en déduit que si $f$ vérifie les trois propriétés, $f = \lim\limits_{n \to \infty} \frac{n! n^x}{x(x + 1) \dots (x + n)}$. Par unicité de la limite, $f = \Gamma$ \textblue{($\Gamma$ vérifie les propriétés et l'unicité nous donne une seule fonction)}.		
	\end{proof}
	
	\begin{remarque}
		On remarque qu'on a démontrer que $\Gamma = \lim\limits_{n \to \infty} \frac{n! n^x}{x(x + 1) \dots (x + n)}$. Cette formule est la formule d'Euler.
	\end{remarque}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}