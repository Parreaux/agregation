\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
		};
		%\draw[color=blue] (box.north west)--(box.north east);
		\node[title] at (box.north) {#2};
		\end{tikzpicture}\bigskip%
		} 

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\D}{\mathcal{D}}
	\newcommand{\A}{\mathfrak{A}}
	\newcommand{\Sn}{\mathfrak{S}}
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{OliveGreen}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	\newcommand{\textmagenta}[1]{\textcolor{DarkOrchid}{#1}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\theoremstyle{plain}
	\newtheorem{theo}{Théorème}
	\newtheorem{prop}{Proposition}
	\newtheorem{cor}{Corollaire}
	\newtheorem{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem{definition}{Définition}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	
	\title{Simplicité de $\A_n$ pour $n \geq 5$}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Ulmer \cite[p.53]{Ulmer} 
		
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 104 (Groupes finis); 105 (Groupe symétrique); 108 (générateurs d'un groupe).
		}}

	\section{Introduction}
	
	L'objet de ce développement est de montrer la simplicité de $\A_n$ dans le cas $n \geq 5$. Cependant, il est intéressant de noté que $\A_n$ est simple pour $n \neq 4$, comme on va le voir ci-dessous. 
	
	\begin{theo}
		Le groupe alterné $\A_n$ est simple pour $n \neq 4$.
	\end{theo}
	\begin{proof}
		Pour $n \leq 4$, on a naturellement :
		\begin{itemize}
			\item $\A_1 = \mathfrak{S}_1 = \{Id\}$;
			\item $\A_2 = \{Id\}$;
			\item $\A_3$ est de cardinal $3$ donc isomorphe à $\Z/_{3\Z}$ qui est simple;
			\item $\A_4$ n’est pas simple, car contient $V_4 = \{Id, (12)(34), (13)(24), (14)(23)\} \simeq \Z/_{2\Z} \times \Z/_{2\Z}$ comme sous-groupe distingué non trivial (\textblue{c'est le sous-groupe engendré par les doubles transpositions}).
		\end{itemize}
		Le cas $n \geq 5$ est plus subtile et est donc l'objet de ce développement.
	\end{proof} 
	
	\section{Simplicité de $\A_n$ pour $n \geq 5$}
	
	Le théorème que l'on souhaite montrer dans le développement est le suivant \textblue{(on remarque que c'est la partie la plus technique de la preuve du théorème introduit précédemment)}:
	\begin{theo}
		Le groupe $\A_n$ est alterné quand $n \geq 5$.
	\end{theo}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement}}}{\begin{enumerate}
			\item Les $3$-cycles engendrent le groupe alterné $\A_n$, quand $n \geq 5$.
			\begin{enumerate}
				\item Les trois cycles sont dans $\A_n$.
				\item Tout élément de $\A_n$ s'écrit comme un produit pair de transposition.
				\item Un produit de deux transpositions s'écrit comme un produit de 3-cycles.
			\end{enumerate}
			\item Les $3$-cycles sont conjugués entre eux dans $\A_n$, quand $n \geq 3$: étude de la bonne permutation avec des commutateurs.
			\item Preuve du théorème.
			\begin{enumerate}
				\item Si $H$ contient un $3$-cycle alors $H = \A_n$.
				\item Si $H$ contient un $p$-cycle $\gamma$ où $p \geq 4$, alors $H = \A_n$.
				\item Si $H$ ne contient que des $2$-cycles, alors on a une contradiction.
			\end{enumerate}
		\end{enumerate}
	}
	
	\begin{lemme}
		Les $3$-cycles engendrent le groupe alterné $\A_n$ (quand $n \geq 3$).
	\end{lemme}
	\begin{proof}
		\textmagenta{L'hypothèse $n \geq 3$ permet d'assurer l'existence des $3$-cycles.}
		
		La signature d'un trois cycle est $(-1)^{n-(n-2)} = 1$ \textblue{(car il y a $n-3$ orbites réduites à un singletons et une orbite de taille $3$, par définition d'un $3$-cycle)}. Donc l'ensemble des $3$-cycles est inclus dans $\A_n$.
		
		Tout élément de $\A_n$ est le produit d'un nombre paire de transpositions. \textblue{(En effet,les transpositions engendrent $\mathfrak{S}_n$ et comme la signature d'une transposition vaut $-1$ (car on a $(-1)^{n-(n-1)} = -1$), tous les éléments de $\A_n$ s'écrivent comme produit d'un nombre de transposition qui doit être paire pour obtenir la bonne signature.}
		
		Un produit de deux transpositions est soit un $3$-cycle, soit un produit de $3$-cycles:
		
		\[ \left  \{
		\begin{array}{c @{=} l}
		(i,j) (k,l) & (i,l,k)(i,j,k) \\
		(i,j) (i,k) & (i,k,j)\\
		\end{array}
		\right.
		\]
		Ce qui conclut la preuve.
	\end{proof}
	
	\begin{definition}[Éléments conjugués]
		Deux éléments $\gamma$, $\gamma'$ de $\A_n$ sont dits conjugués s'il existe $\sigma \in \mathfrak{S}_n$ tel que $\sigma\gamma\sigma^{-1} = \gamma'$.
	\end{definition}
	
	\begin{lemme}
		Les $3$-cycles sont conjugués entre eux dans $\A_n$, quand $n \geq 5$.
	\end{lemme}
	\begin{proof}
		Soit $(i, j, k)$ un $3$-cycle quelconque de $\mathcal{S}_n$. On introduit la permutation 
		\[ \sigma = \left  (
		\begin{array}{c c c c c c c c}
		1 & 2 & 3 & 4 & 5 & \dots & n  \\
		i & j & k & l & s & \dots & \\
		\end{array}
		\right )
		\]
		où $l,s \in \{1, \dots, n\} \setminus \{i, j, k\}$ \textmagenta{(on utilise ici l'hypothèse $n \geq 5$ pour trouver $l$ et $s$ tels qu'on les souhaite)}.
		
		L'une des deux transpositions $\sigma$ ou $\sigma' = \sigma \circ (l, s)$ est paire donc appartient à $\A_n$. \textblue{(En effet, si $\sigma$ n'est pas paire, alors sa signature vaut $-1$ et comme la signature d'une transposition vaut $-1$ et que la signature est un morphisme de groupe, alors la signature de $\sigma'$ vaut $1$: elle est donc paire. Sinon, $\sigma$ est déjà paire.)}
		
		De plus, $\sigma (1, 2, 3) \sigma^{-1} = \sigma' (1, 2, 3) \sigma'^{-1} = (i, j, k)$ (\textblue{car $ls$ n'agit dans le support de $\sigma$}). Donc $(i, j, k)$ et $(1, 2, 3)$ sont conjugués. Par transitivité de la relation de conjugaison \textblue{(en effet, soit $\alpha$, $\beta$, $\gamma$, trois transpositions de $\mathfrak{S}_n$ telles que $\alpha$ et $\beta$ soient conjuguées ainsi que $\beta$ et $\gamma$. Alors, il existe $\sigma, \sigma' \in \mathfrak{S}_n$ telles que $\alpha= \sigma\beta\sigma^{-1}$ et $\beta= \sigma'\gamma\sigma'^{-1}$, soit $\alpha= \sigma\sigma'\gamma\sigma'^{-1}\sigma^{-1}$ d'où le résultat)}, on en déduit que deux $3$-cycles sont toujours conjugués dans $\A_n$ \textblue{(en effet, soit deux $3$-cycles, par ce qui précèdent, on peut toujours les conjuguer à $(1,2,3)$ et conclure par transposition)}.
	\end{proof}

	\begin{theo}
		Le groupe $\A_n$ est alterné quand $n \geq 5$.
	\end{theo}
	\begin{proof}
		Soit $H$ un sous-groupe distingué de $\A_n$ distinct de $\{Id\}$. On distingue tous les cas possibles.
		
		\paragraph{Si $H$ contient un $3$-cycle}: donc, d'après le lemme 2 \textmagenta{(on utilise ici l'hypothèse $n \geq 5$ puisque nous utilisons le lemme 2)}, il les contient tous. Comme les $3$-cycles engendrent $\A_n$ (lemme 1), on en déduit que $H = \A_n$.
		
		\paragraph{Si $H$ contient un $p$-cycle $\gamma$ où $p \geq 4$}: supposons, sans perte de généralité que $\gamma = (1, 2, \dots, p)$. Soit $\nu = (1, 2, 3)$. Alors $\nu^{-1} = (1, 3, 2)$ et $\gamma^{-1} = (1, p, p-1, \dots, 2)$ \textblue{(se vérifie par un calcul rapide)} et on vérifie que $\gamma^{-1}\nu^{-1}\gamma\nu= (2,3,p)$ \textblue{(nécessite de faire proprement le calcul)}. 
		
		Comme $H \lhd A_n$, $\gamma \in H \Rightarrow (\gamma^{-1} \in H$ et $\nu^{-1}\gamma\nu \in H) \Rightarrow (2, 3, p) \in H$ \textblue{(par les propriétés de groupe pour $H$ et car $H$ est distingué dans $\A_n$)}. Donc $H$ contient un $3$-cycle et le premier point donne $H = \mathfrak{S}_n$ \textmagenta{(on utilise ici l'hypothèse $n \geq 5$ puisque nous utilisons le point précédent)}.
		
		\paragraph{Si $H$ ne contient que des $2$-cycles}: soit $\sigma \in H \setminus \{Id\}$, $\sigma$ est le produit d'un nombre pair de transposition: $\sigma = (i, j)(k,l) \dots$. Cette décomposition est en fait la décomposition de $\sigma$ en produit de cycles à supports disjoints \textblue{(les cycles à supports disjoints engendrent $\mathfrak{S}_n$)}, de sorte que toutes les transpositions qui interviennent dans le produit $\sigma = (i, j)(k,l) \dots$ après les deux premières ont des supports qui n'intersectent pas $\{i, j, k, l\}$. Si on note, $\sigma = (i,j)(k,l)\phi$, on a $Supp(\phi) \cap \{i, j, k, l\} = \emptyset$. Si $\nu = (i,j, k)$ alors $\nu^{-1}\sigma\nu\sigma^{-1} = (i, l)(j,k)$.
		
		Mais $H \lhd A_n$, donc $\nu^{-1}\sigma\nu \in H$ \textblue{($\sigma \in H$)} et comme $\sigma^{-1} \in H$ \textblue{($H$ est un groupe)}, alors $\nu^{-1}\sigma\nu\sigma^{-1} \in H$, soit $(i, l)(j,k) \in H$. $H$ contient alors un produit de deux transpositions.
		
		On peut donc supposer que $H$ contient le produit $\sigma =(i, j)(k,l)$. Soit $s \notin \{i, j, k, l\}$ et $\beta = (k, l, s)$, alors $\beta^{-1}\sigma\beta\sigma^{-1} = \beta$ \textmagenta{(on utilise ici l'hypothèse $n \geq 5$ pour trouver le $s$ qui convient)}. Mais $H$ étant distingué dans $\A_n$, $\beta^{-1}\sigma\beta \in H$ et comme précédemment, $\beta \in H$.
		
		Absurde car $H$ ne contient pas de trois cycles par hypothèse. D'où le théorème.
	\end{proof}
	
	\begin{req}
		Les produit comme celui-ci: $\beta^{-1}\sigma\beta\sigma^{-1}$, s'appelle des commutateurs.
	\end{req}
	
	\section{Compléments autours de la simplicité d'un groupe}
		
	\subsection*{Quelques applications} 
	
	Nous donnons ici quelques applications de la simplicité de $\A_n$ quand $n \neq 4$.
	
	\begin{prop}[Application 1.]
		Pour $n \neq 4$, les seuls sous-groupes distingués de $\mathfrak{S}_n$ sont $\{Id\}$, $\A_n$ et $\mathfrak{S}_n$.
	\end{prop}
	\begin{proof}[Idée de la preuve]
		On se donne un sous-groupe distingué et on fait son intersection avec $\A_n$ qui donne soit $\{Id\}$ soit $\A_n$ et on conclut par une distinction de cas.
	\end{proof}
	
	\begin{prop}[Application 2.]
		Il n'y a pas de surjection de $\mathfrak{S}_n$ dans $\mathfrak{S}_{n+1}$ pour $n \geq  5$.
	\end{prop}
	\begin{proof}[Idée de la preuve]
		On raisonne par l'absurde.
	\end{proof}
		
	\subsection*{Sous-groupe distingué}
	\input{./../Notions/Groupes_distingues.tex}	
		
	\subsection*{Orbites, transpositions et cycles}
	\input{./../Notions/GroupeSymetrique_orbiteTranspositionsCycles.tex}
		
	\subsection*{Le groupe alterné}
	\input{./../Notions/GroupesAlterne.tex}	
	
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}