\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
} 


\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\Ck}{\mathcal{C}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{OliveGreen}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	\newcommand{\textmagenta}[1]{\textcolor{DarkOrchid}{#1}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\newcommand{\pcite}[1]{\protect{\cite{#1}}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\newtheorem*{application}{Application}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	\newtheorem*{cex}{Contre-exemple}
	
	\title{Théorème des extremums liées}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Références du développement:} Avez \cite[p.103]{Avez}; Objectif agrégation \cite[p.20]{BeckMalikPeyre}; X-ENS Algèbre 1 \cite[p.295]{FrancinouGianellaNicolas-al1}; Rouvière \cite[p.380]{Rouviere} (les références dépendent de l'orientation de la leçon)
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 151 (Dimension et rang); 152 (Déterminant); 159 (Dualité); 214 (TIL, TFI); 219 (Extremum).
		}}
		
	\section{Introduction}
	
	Le problème de recherche des extremums liés consiste à maximiser ou minimiser une fonction de plusieurs variables  $f(x_1, \dots,x_n)$ lorsque ces variables sont liées par certaines relations. La recherche d'extremums dans un massif montagneux consiste à trouver les sommets, la recherche des extremums liés se rapporteraient à la recherche des cols (de la route) qui passent rarement au sommet de la montagne. Le théorème des extremums liés permet bien souvent de résoudre le problème de la recherche des extremums liés à l'aide des multiplicateurs de Lagrange.
	
	\section{Théorème des extremums liés}
	
	En fonction des leçons, ce développement n'est pas à réaliser de la même manière. Selon la version du développement présentée, le théorème des extremums liés est plus ou moins démontré.
	
	\titlebox{red}{\textred{\textbf{Schéma du développement (leçons 151 et 214)}}}{
		Ce qui est important dans ces leçons est de faire apparaître les utilisation de la dimension (et ses calculs) ou la notion de sous-variété. On démontre donc le lemme et le théorème (de manière précise car on manipule la dimension finie et la notion de rang). On se base alors sur la démonstration du théorème donnée par \cite[p.103]{Avez} même si l'énoncé est pris soit dans \cite[p.20]{BeckMalikPeyre} ou dans \cite[p.343]{Rouviere}.
		\begin{itemize}
			\item Preuve du lemme
			\item Preuve du théorème
			\begin{enumerate}
				\item Montrer que $M$ est une sous-variété.
				\item Montrer que $T_a(M) = \cap_{i=1}^{k} \ker (Dg_i(a))$. On pose $T = \cap_{i=1}^{k} \ker (Dg_i(a))$.
				\begin{enumerate}
					\item $T_a(M)$ est un espace vectoriel de même dimension que $M$.
					\item Montrons que la dimension de $T$ est $n - k$.
					\item Montrons que $T_a(M) \subseteq T$.
					\item On conclut grâce à la dimension.
				\end{enumerate}
				\item Montrer que $T_a(M) \subseteq \ker(Df(a))$.
				\item Appliquer le lemme pour conclure.
			\end{enumerate}
		\end{itemize}
	}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement (leçons 152, 159 et 219)}}}{
		Pour ces leçons, on va traiter le lemme (qui justifie sa présence dans la leçon 159), le théorème et l'application (qui justifie sa présence dans la leçon 152) donnant un exemple d'application du théorème (intéressant dans le cadre de la leçon 219). La preuve du théorème est alors un peu moins poussé, on se base plutôt sur la preuve dans \cite[p.20]{BeckMalikPeyre}.
		\begin{itemize}
			\item Preuve du lemme
			\item Preuve du théorème
			\begin{enumerate}
				\item Dire que $M$ est une sous-variété.
				\item Dire que $T_a(M) = T =  \cap_{i=1}^{k} \ker (Dg_i(a))$.
				\item Montrer que $T_a(M) \subseteq \ker(Df(a))$.
				\item Appliquer le lemme pour conclure.
			\end{enumerate}
			\item Preuve de l'application
			\begin{enumerate}
				\item Montrer que le maximum de $\det$ sur l'ensemble $X$ est atteint et est positif.
				\item Montrer s que si le maximum de $\det$ est atteint en $(v_1, \dots, v_n)$ les $(v_i)_{i}$ forment une base orthogonale de $E$.
				\item Réciproquement, montrer que si les $(v_i)_{i}$ forment une base orthogonale de $E$, $\det(v_1, \dots, v_n) \in \{-1, 1\}$ selon l'orientation de la base
			\end{enumerate}
		\end{itemize}
	}

	\begin{lemme}
		Soient $\varphi$, $\varphi_1, \dots, \varphi_n$ des formes linéaires sur un espace vectoriel $E$ de dimension $n$.
		\begin{displaymath}
		\varphi \in \mathrm{Vect}(\varphi_1, \dots, \varphi_n) \Longleftrightarrow \cap_{i = 1}^{n} \ker \varphi_i \subseteq \ker \varphi
		\end{displaymath}
	\end{lemme}
	\begin{proof}
		\begin{description}
			\item[$\Rightarrow$] Supposons que $\varphi \in \mathrm{Vect}(\varphi_1, \dots, \varphi_n)$, on en déduit qu'il existe des $\left(\lambda_i\right)_{i \in \llbracket 1, n \rrbracket} \in E$ tels que $\varphi = \sum_{i=1}^{n} \lambda_i \varphi_i$. Soit $x \in \cap_{i = 1}^{n} \ker \varphi_i$, alors pour tout $1 \leq i \leq n$, $\varphi_i(x)$. On a alors $\varphi(x) = 0$ \textblue{(car $\varphi(x) = \sum_{i=1}^{n} \lambda_i \varphi_i(x)$)} et donc $x \in \ker \varphi$. Donc $\cap_{i = 1}^{n} \ker \varphi_i \subseteq \ker \varphi$.
			\item[$\Leftarrow$ \protect{\cite[p.295]{FrancinouGianellaNicolas-al1}}] Supposons que $\cap_{i = 1}^{n} \ker \varphi_i \subseteq \ker \varphi$. On peut également supposer que $\varphi$ est non nulle, sans quoi le résultat est évident. Notons $F$ le sous-espace de $E^{*}$ engendré par les $\left(\varphi_1, \dots, \varphi_n\right)$ et $D$ la droite de $E^*$ engendrée par $g$. On a alors:
			\begin{displaymath}
			\begin{array}{ccll}
				F^{\circ} & = & \{x \in E ~|~ \forall i \in \rrbracket 1, k \rrbracket, \varphi_i(x) = 0 \} & \textblue{\text{(Définition de l'othogonalité au sens de la dualité)}} \\
				& = &  \cap_{i = 1}^{n} \ker \varphi_i & \textblue{\text{(Définition du noyaux et de l'intersection)}} \\
				& \subseteq & \ker \varphi & \textblue{\text{Hypothèse}} \\
				& = & \{x \in E ~|~ \varphi(x) = 0 \} & \textblue{\text{(Définition du noyaux)}} \\
				& = & D^{\circ} & \textblue{\text{(Définition de l'othogonalité au sens de la dualité)}} \\
			\end{array}
			\end{displaymath}
			En prenant l'orthogonal de $F^{\circ}$ et $D^{\circ}$ \textblue{(qui appartiennent à $E$)} dans $E$, $\left(D^{\circ}\right)^{\bot} \subseteq \left(F^{\circ}\right)^{\bot}$ \textblue{(car $F^{\circ} \subseteq D^{\circ}$ et que la dualité échange l'inclusion)}.
			
			Comme $\dim E < \infty$, pour tout sous-espace vectoriel de $E^{*}$, $\left(G^{\circ}\right)^{\bot} = G$, donc $D \subseteq F$. On en déduit que $\varphi \in \mathrm{Vect}(\varphi_1, \dots, \varphi_n)$.
		\end{description}
	\end{proof}
	
	\begin{theo}[Théorème des extremums liés]
		Soient $U$ un ouvert $\R^n$ et $f, g_1, \dots, g_k : U \to \R$ de classe $\Ck^1$. Posons $M = \{w \in U, g_1(w) = \dots = g_k(w) = 0 \}$. Si la restriction de $f$ à $M$ admet un extremum local en $a \in U$, et si la famille $\left(Dg_i(a)\right)_{i \in \llbracket 1, k\rrbracket}$ est linéairement indépendante, alors il existe $\left(\lambda_1, \dots, \lambda_k\right) \in \R^k$ tels que $\sum_{i = 1}^{k} Dg_i(a)\lambda_i = Df(a)$. Les $\left(\lambda_i\right)_{i \in \llbracket 1, k \rrbracket}$ sont appelés les multiplicateurs de Lagrange.
	\end{theo}
	\begin{proof}
		Conservons les notations du théorème
		\paragraph{Étape 1: montrons que $M$ est une sous-variété \cite[p.93,~definition~3]{Avez}}
		\begin{itemize}
			\item Posons $g = \left(g_1, \dots, g_k\right)$, on a alors $U \cap M = g^{-1}\left(\{O_{\R^k}\}\right)$ \textblue{(comme image réciproque de $0$)}.
			\item $M$ est une sous-variété si et seulement si $\forall x \in M$, $Dg(x)$ est surjective \textblue{(dans la troisième définition des sous-variétés, l'hypothèse sur $U \cap M$ est déjà vérifié (remarque précédente))}.
			\item $\forall x \in M$, $Dg(x) = \begin{pmatrix} Dg_1(x) \\ \vdots \\ Dg_k(x) \\ \end{pmatrix}$. La famille $\left(Dg_i(a)\right)_{i \in I}$ est libre, elle forme alors une base de $\R{n - k}$ et donc $\forall x \in M$, $Dg(x)$ est surjective car elle est de rang $ - k$.
		\end{itemize}
		Donc $M$ est une sous-variété de dimension $n-k$.
		
		\paragraph{Étape 2: montrons que $T_a(M) = \cap_{i=1}^{k} \ker (Dg_i(a))$ \cite[p.98,~théorème~2.3]{Avez}} Notons $T = \cap_{i=1}^{k} \ker (Dg_i(a))$. Montrons alors que $T_a(M) = T$.
		
		\subparagraph{Étape a: $T_a(M)$ est un espace vectoriel de même dimension que $M$ \cite[p.98,~théorème~2.2]{Avez}} D'après la première définition des plans tangents \textblue{(carte locale)}, $D\varphi(a)(T_a(M)) = \R^d \times \{0\}$ \textblue{(composition par $D\varphi(a)$)}. Donc $T_a(M)$ est un espace vectoriel de dimension $d = \dim M = n - k$.
		
		\subparagraph{Étape b: montrons que la dimension de $T$ est $n - k$} Posons $g = \left(g_1, \dots, g_k\right)$. On a $T = \ker g$ et $\R^k = \mathrm{Im} g$ car $g$ est surjective. Donc par le théorème du rang appliqué à $g$: $\dim \R^n = \dim (\mathrm{Im} g) + \dim (\ker g)$. On en déduit que $n = \dim R^k + \dim T$, soit $\dim T = n - k$.
		
		\subparagraph{Étape c: montrons que $T_a(M) \subseteq T$} Soit $v \in T_a(M)$.
		\begin{itemize}
			\item Il existe un intervalle ouvert $I$ contenant $0$ et une application différentielle $\gamma : I \to M$ telle que $\gamma(0) = a$ et $\gamma'(0) = v$ \textblue{(définition du plan tangent)}. 
			\item Pour tout $i \in \llbracket 1, k\rrbracket$ et tout $t \in I$, on a $g_i(\gamma(t)) = 0$ \textblue{(car $\forall t \in I$ $\gamma(t) \in M$ et par définition de $\gamma$)}. 
			\item Pour tout $i \in \llbracket 1, k\rrbracket$ et tout $t \in I$, $Dg_i(\gamma(t))(\gamma'(t)) = 0$ \textblue{(car $Dg_i(\gamma(t)) = 0$ et on applique le théorème de la différentielle composée)}.
			\item On évalue en $0$ et on obtient que pour tout $i \in \llbracket 1, k\rrbracket$, $Dg_i(\gamma(0))(\gamma'(0)) = 0 = Dg_i(a)(v)$. Donc $v \in T$.
		\end{itemize}
		
		Donc, comme $\dim T_a(M) = \dim T$ et que $T_a(M) \subseteq T$, on en conclut que $T_a(M) = T$.
		
		\paragraph{Étape 3: montrons que $T_a(M) \subseteq \ker(Df(a))$ \cite[p.98,~théorème~2.4]{Avez}} Soit $v \in T_a(M)$. Alors, il existe un intervalle ouvert $I$ contenant $0$ et une application différentielle $\gamma : I \to M$ telle que $\gamma(0) = a$ et $\gamma'(0) = v$ \textblue{(définition du plan tangent)}. 
		\begin{itemize}
			\item Pour tout $t \in I$, $f|_M \circ \gamma(t) = f \circ \gamma(t)$ \textblue{(car pour tout $t \in I$, $\gamma(t) \in M$)}.
			\item Cette fonction admet un extremum pour $t = 0$ \textblue{(car $\gamma(0) = a$ et $a$ est un extremum de $f$ par hypothèse, si $t \neq 0$, on obtient une contradiction)}.
			\item On en déduit que $t \mapsto Df(\gamma(t))(\gamma'(t))$ s'annule en $0$ car $f(\gamma(t))$ admet un extremum en $0$. Donc $Df(a)(v) = 0$ \textblue{(évaluation en $0$)} et $v \in \ker Df(a)$.
		\end{itemize}
		
		En particulier $\cap_{i=1}^{k} \ker (Dg_i(a)) \subseteq \ker D(f)$. On peut alors appliquer le lemme, ce qui nous permet de conclure.
	\end{proof}
	
	\begin{application}[Inégalité de Hadamard \protect{\cite[p.380]{Rouviere}}]
		Soit $E = \R^n$ muni du produit scalaire usuel noté $< . >$. Pour tout $x_1, \dots, x_n \in E$, on a l'inégalité suivante:
		\begin{displaymath}
		\left|\det(x_1, \dots, x_n)\right| \leq \lVert x_1 \rVert \dots \lVert x_n\rVert
		\end{displaymath}
		avec égalité si et seulement si les $\left(x_i\right)_{1\leq i \leq n}$ forment une base orthogonale de $E$.
	\end{application}
	\begin{proof}
		Notons $X = \{(v_1, \dots, v_n) \in E^n, \lVert v_1 \rVert = \dots = \lVert v_1 \rVert = 1 \}$.
		
		\paragraph{Étape 1: montrons que le maximum de $\det$ sur l'ensemble $X$ est atteint et est positif}
		\begin{itemize}
			\item $X$, produit des sphère unité de $E$, est un compacte de $E^n$. De plus, $\det$ est continue sur $E^n$ \textblue{(et même de classe $\Ck^{\infty}$ car c'est une fonction polynomiale)}. Donc, la fonction $\det$ atteint son maximum sur $X$.
			\item Notons $e_i$ les vecteurs de la base canonique de $\R^n$. Comme $(e_1, \dots, e_n) \in X$ \textblue{(car leur norme vaut $1$ par définition)} et que $\det(e_1, \dots, e_n) = 1$ \textblue{(définition du déterminant de la base canonique)}, le maximum de $\det$ sur $X$ vaut au moins $1$.
		\end{itemize}
		
		\paragraph{Étape 2: montrons que si le maximum de $\det$ est atteint en $(v_1, \dots, v_n)$ les $(v_i)_{i}$ forment une base orthogonale de $E$}
		\begin{itemize}
			\item Ce maximum est un extremum de $\det$ lié par les conditions 
			\begin{displaymath}
			\left\{\begin{array}{c}
			g_1(v_1, \dots, v_n) \\
			\vdots \\
			g_n(v_1, \dots, v_n) \\
			\end{array}\right. \text{ où } g_i(v_1, \dots, v_n) = \lVert v_i \rVert^2 - 1
			\end{displaymath}
			 
			\item $\forall (v_1, \dots, v_n) \in X$ et $\forall (h_1, \dots, h_n) \in E^n$, $Dg_i(v_1, \dots, v_n)(h_1, \dots, h_n) = 2\left< v_i, h_i \right>$. Elle sont donc indépendantes comme formes linéaires.
			
			\item $X$ est une sous-variété de $E^n = \R{n^2}$ de dimension $n^2 - n$.
			
			\item Le théorème des extremums liés nous assure l'existence de $\lambda_i$ tels que $D\det(v_1, \dots, v_n)(h_1, \dots, h_n) = \sum_{i=1}^{n}\lambda_i\left< v_i, h_i \right>$, pour tout $h_i \in E$.
			
			\item Par la linéarité de $\det$, pour tout $v_i$, on a $D\det(v_1, \dots, v_n)(0, \dots, 0, h_i, 0, \dots, 0) =  \det(v_i, \dots, v_{i-1}, h_i, v_{i+1}, \dots, v_n)$. Donc $D\det(v_1, \dots, v_n)(h_1, \dots, h_n) = \sum_{i=1}^{n}\lambda_i\left< v_i, h_i \right>$ est équivalent à $\det(v_i, \dots, v_{i-1}, h_i, v_{i+1}, \dots, v_n) = \lambda_i \left< v_i, h_i \right>$ \textblue{(on somme les différentielles partielles qui sont continues)}.
			
			\item On en déduit que si $h_i = v_i$, $\lambda_i = f(v_1, \dots, v_n) > 0$ et $v_i . v_j = 0$.
		\end{itemize}
		
		\paragraph{Étape 3: réciproquement, montrons que si les $(v_i)_{i}$ forment une base orthogonale de $E$, $\det(v_1, \dots, v_n) \in \{-1, 1\}$ selon l'orientation de la base}
		\begin{itemize}
			\item Par les étapes $1$ et $2$, le maximum de $\det$ sur $X$ vaut $1$ et est atteint en $(v_1, \dots, v_n)$ si et seulement si les $\left(x_i\right)_{1\leq i \leq n}$ forment une base orthogonale de $E$.
			
			\item Par homogénéité en chacun des $v_i$, $\det(v_1, \dots, v_n) \leq \lVert v_1 \rVert \dots \lVert v_n \rVert$. On en déduit le résultat si les $v_i$ sont tous nuls ou s'ils forment une base orthogonale normalisée.
		\end{itemize}
	\end{proof}
	
	\section{Compléments autours des extremums liés}
	
	\subsection*{Le théorème d'inversion locale}
	\input{./../Notions/CalculDiff_TIL.tex}
	
	\subsection*{Le théorème des fonctions implicites}
	\input{./../Notions/CalculDiff_TFI.tex}
	
	\subsection*{Les sous-variétés}
	\input{./../Notions/CalculDiff_SsVarietes.tex}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}

\end{document}