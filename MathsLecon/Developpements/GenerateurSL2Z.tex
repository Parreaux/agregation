\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
} 

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\Cun}{\mathcal{C}^1}
	\newcommand{\Cdeux}{\mathcal{C}^2}
	\newcommand{\Cinf}{\mathcal{C}^{\infty}}
	\newcommand{\Sn}{\mathfrak{S}}
	
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{OliveGreen}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	\newcommand{\textmagenta}[1]{\textcolor{DarkOrchid}{#1}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\newcommand{\pcite}[1]{\protect{\cite{#1}}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	
	\title{Générateurs de $\mathrm{SL}_2(\Z)$}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Alessandri, \emph{Thèmes en géométrie} \cite[p.81]{Alessandri} 
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
		\textblue{\emph{Leçons où on présente le développement:}} 182 (Nombres complexes en géométrie); 183 (Groupe et géométrie).
			
		\textblue{\emph{Leçons où on peut en parler}:} 101 (Actions de groupe); 108 (Générateurs d'un groupe).
	}}

	\section{Introduction}
	
	Connaître un ensemble de générateurs pour un groupe est rarement simple. La géométrie est une méthode permettant de déterminer les générateurs; on fait agir le groupe dont on souhaite connaître les générateurs sur un ensemble bien choisi. Pour le groupe $SL_2(\Z)$ : on fait agir ce groupe par homographie sur le plan complexe. Cet étude fait émerger une étude du demi-plan de Poincaré.
	
	\section{Étude des générateurs de $\mathrm{SL}_2(\Z)$}
	
	\begin{theo}
		Le groupe $\mathrm{SL}_2(\Z)$ est engendré par les matrices $S = \begin{pmatrix}
		0 & -1 \\
		1 & 0 \\
		\end{pmatrix}$ et $T = \begin{pmatrix}
		1 & 1 \\
		0 & 1 \\
		\end{pmatrix}$.
	\end{theo}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement}}}{Ce développement est très long, il est difficile (voire impossible) de tout faire: il faut faire des choix (le plus pertinent, le plus technique, les endroit où on est le plus à l'aise, ...).
		\begin{enumerate}
			\item Le groupe $\mathrm{Sl}_2(\Z)$ agit sur $P$ par homographie \textblue{(lemme~\ref{lemme1})}.
			\item $G = \left< S, T \right>$ agit sur $P$: toutes les orbites rencontrent $D = \{z \in P ~|~ \left|z\right| \geq 1, \left|\Re(z)\right|\leq\frac{1}{2}\}$ \textblue{(lemme~\ref{lemme2})}.
			\begin{enumerate}
				\item Dire qu'il existe un nombre fini de couple d'entier $(c, d) \in \Z^2$ tel que $\left|cz+d\right| \leq 1$.
				\item Dire que $I_z = \{\Im(u) ~|~ u \in O_z \} = \{\Im(A * z) \geq \Im(z) \}$ admet un maximum: $M_z$ donné par $A_1$.
				\item On pose $z_1 = A_1 *z$, $n = \lfloor \Re(z_1) + \frac{1}{2} \rfloor$ et $z_2 = T^{-n} * z_1$. D'où $z_2 \in D$ et $\Im(z_2) = M_z$.
				\begin{enumerate}
					\item Dire que si $u \in P$ alors $T * u = u + 1$.
					\item Montrer que $\left|\Re(z_2)\right| \leq \frac{1}{2}$.
					\item Montrer que $\left|z_2\right| \geq 1$.
					\item Montrer que $S * z_2 \in O_z$.
				\end{enumerate}
			\end{enumerate}
			\item $A \in \mathrm{SL}_2(\Z)$ tel que $A * z \in D$ est une matrice de $G$ \textblue{(lemme~\ref{lemme3})}.
			\begin{enumerate}
				\item Dire que $c \in \{0, -1, 1\}$.
				\item Étude du cas $c = 0$.
			\end{enumerate}
			\item Conclure en appliquant les deux lemmes.
		\end{enumerate}
	}
	
	\begin{proof}
		Soit $P = \{z \in \C, \mathrm{Im} > 0\}$ le demi-plan de Poincaré.
		
		\begin{lemme}
			Le groupe $\mathrm{SL}_2(\Z)$ agit sur $P$ par homographie : si $A = \begin{pmatrix}
			a & b \\ c & d  \\
			\end{pmatrix} \in \mathrm{SL}_2(\Z)$, $A * z = \frac{az+b}{cz+d}$.
			\label{lemme1}
		\end{lemme}
		\begin{proof}
			Soit $A$ et $B \in \mathrm{SL}_2(\Z)$ et $z \in P$:
			\begin{displaymath}
				\begin{array}{ccll}
				\Im(A * z) & = & \frac{(ab - dc)}{\left|cz + d\right|^2} \Im(z) & \textblue{(A* z = \frac{ac\left|z\right|^2 + bd + adz + bc\overline{z}}{\left|cz + d\right|^2})} \\
				& = & \frac{\Im(z)}{\left|cz + d\right|^2} & \textblue{(ab-dc = \det A = 1)} \\
				& > & 0 & \textblue{(\text{hypothèse sur } z)}  \\
				\end{array}
			\end{displaymath} 
			Donc $\mathrm{SL}_2(\Z)$ opère par homographie sur $P$.
		\end{proof}
		
		Notons $G$ le sous-groupe engendré par $S$ et $T$ et $D = \{z \in P ~|~ \left|z\right| \geq 1, \left|\Re(z)\right|\leq\frac{1}{2}\}$.
		
		\begin{lemme}
			$G$ agit via l'action de $\mathrm{SL}_2(\Z)$ sur $P$. Alors, pour cette action de $G$, toutes les orbites rencontre $D$.
			\label{lemme2}
		\end{lemme}
		\begin{proof}
			\emph{Idée}: Construire un point "d'altitude maximale" sur l'orbite $O_z$, $z$ fixé dans $P$ puis de le translater à "altitude constante" (grâce à $T$) pour rentrer dans $D$. \textblue{Les étapes de cette preuves doivent être effectuer dans le bon ordre pour être correcte: il faut commencer par trouver le point maximum avant de le translater. Enfin il faut vérifier si on appartient à $D$. La translation doit être effectuer avant la vérification sinon on peut la vérifier avant mais plus après.}
			
			\subparagraph{Étape a: montrons qu'il existe un nombre fini de couple d'entier $(c, d) \in \Z^2$ tel que $\left|cz+d\right| \leq 1$}. On a 
			\begin{displaymath}
			\begin{array}{ccll}
			\left|c\right| \Im(z) & = & \left|\Im(cz +d)\right| & \textblue{(d \in \Z)} \\
			& \leq & \left|cz+d\right| & \textblue{\left|w\right| = \sqrt{\Re(w)^2 + \Im(w)^2} \geq \left|\Im(w)\right|} \\
			& \leq & 1 & \text{\textblue{(hypothèse)}} \\
			\end{array}
			\end{displaymath}
			Donc $\left|c\right| \leq \frac{1}{\Im(z)}$ \textblue{(car $\Im(z) > 0$} et $\left|d\right| \leq 1 + \left|c\right|\left|z\right|$ \textblue{(par les propriétés du module)}. On en déduit le nombre fini de couple.
			
			On pose $I_z = \{\Im(u) ~|~ u \in O_z \} = \{\Im(A * z) \geq \Im(z) \}$.
			\subparagraph{Étape b : montrons que $I_z$ admet un maximum.}
			\begin{itemize}
				\item On a un nombre fini de $(c, d)$ tel que $\Im(A*z) \geq \Im(z)$ \textblue{(on applique l'égalité précédente à celle de $\Im(A*z)$)}.
				\item Il existe $A_1$ tel que $M_z = \Im(A*z)$ soit le maximum de $I_z$ \textblue{(on inclut dans la sphère unité qui est compacte)}.
			\end{itemize}
			
			On pose $z_1 = A_1 *z$, $n = \lfloor \Re(z_1) + \frac{1}{2} \rfloor$ et $Z_2 = T^{-n} * z_1$. 
			\subparagraph{Étape c: montrons que $z_2$ appartient à la bande d'équation $\left|\Re(u)\right| \leq \frac{1}{2}$ et, de plus, on a $\Im(z_2) = M_z$.}
			\begin{itemize}
				\item Soit $u \in P$, montrons que $T *u = u + 1$.
				\begin{displaymath}
					T * u = \begin{pmatrix} 1 & 1 \\ 0 & 1 \\ \end{pmatrix} *u = \frac{u + 1}{1} = u + 1 
				\end{displaymath}
				\item Montrons que $\left|\Re(z_2)\right| \leq \frac{1}{2}$.
				\begin{itemize}
					\item Montrons que $\Re(z_2) = \Re(z_1) - n$.
					\begin{displaymath}
						\begin{array}{ccll}
						\Re(z_2) & = & \Re(T^{-n} * z_1) & \textblue{(\text{définition de } z_2)} \\
						& = & \Re(\frac{z_1 - n}{1}) & \textblue{(\text{définition de } A * z \text{ et } T^n = \begin{pmatrix}
							1 & -n \\
							0 & 1 \\
							\end{pmatrix})} \\
						& = & \Re(z_1 - n) & \textblue{\text{(calcul)}} \\
						& = & \Re(z_1) - n & \textblue{(n \in \Z)} \\
						\end{array}
					\end{displaymath}
					\item Montrons que $- \frac{1}{2} \leq \Re(z_2)$ : on montre que $- \frac{1}{2} \leq \Re(z_1) - n$.
					\begin{displaymath}
						\begin{array}{ccll}
						\Re(z_1) - n & = & \Re(Z) - \lfloor\Re(z_1) + \frac{1}{2}\rfloor & \textblue{(\text{par définition de } n)} \\
						& \leq & \Re(z_1) - \Re(z_1) - \frac{1}{2} & \textblue{\text{(définition de la borne inférieure)}}\\
						& \leq & - \frac{1}{2} & \textblue{\text{(par calcul)}}\\
						\end{array}
					\end{displaymath}
					\item Montrons que $\Re(z_2) \leq \frac{1}{2}$ : on montre que $\Re(T^{-n}*z_1) \leq \frac{1}{2}$. Or comme $T^{-n} * u = u - n$, et que $z_1 = z + n$ où $\Re(z) \leq \frac{1}{2}$, alors, $\Re(T^{-n}*z_1) \leq \frac{1}{2}$.
				\end{itemize}
				\item Montrons que $\left|z_2\right| \geq 1$ \textblue{(on le montre bien pour $z_2$ et non pour $z_1$ car sans la translation on peut être de module supérieur à un mais pas après la translation)}. On raisonne par l'absurde: on suppose que $\left|z_2\right| < 1$. Dans ce cas,
				\begin{displaymath}
					\begin{array}{ccll}
						\Im(S*z_2) & = & \frac{\Im(z_2)}{\left|z_2\right|^2} & \textblue{(\text{propriété de l'action de groupe évaluer en les valeurs de } S)} \\
						& > & M_z & \textblue{(\left|z_2\right| \geq 1 \text{ (par hypothèse)})} \\
					\end{array}
				\end{displaymath} 
				Or, 
				\begin{displaymath}
					\begin{array}{ccll}
					\Im(z_2) & = & \Im(T^{- n} * z_1) & \textblue{(\text{définition de } z_2)} \\
					& = & \Im(z_1) & \textblue{(T^{-n} *z_1 = z_1 - n \text{ avec } n \in \Z)} \\
					& = & M_z & \textblue{(\text{par définition de } M_z)}
					\end{array}
				\end{displaymath}
				\item Montrons que $S* z_2 \in O_z$.
				\begin{displaymath}
				\begin{array}{ccll}
				S * z_2 & = & S * T^{n}A_1 * z_1 & \textblue{(\text{par définition de } z_2)} \\
				& = & ST^{n}A_1 * z_1 & \textblue{(\text{par propriété de l'action})} \\
				& \in & O_z & \textblue{(\text{définition de l'orbite})} \\
				\end{array}
				\end{displaymath}
			\end{itemize}
		\end{proof}
		
		On cherche maintenant à caractériser les matrices de $\mathrm{SL}_2(\Z)$ tel que $A * z \in D$ pour $z$ fixé. Posons $A = \begin{pmatrix}
		a & b \\
		c & d \\
		\end{pmatrix} \in \mathrm{SL}_2(\Z)$ tel que $A * z \in D$.
		
		\begin{lemme}
			$A$ est une matrice de $G$.
			\label{lemme3}
		\end{lemme}
		\begin{proof}
			\begin{description}
				\item[Cas $\Im(A * z) \geq \Im(z)$] : $\left|cz + d\right| \leq 1$. Montrons que $c \in \{0, -1, 1\}$ et examinons chacun de ces cas.
				\begin{displaymath}
				\begin{array}{ccll}
				\left|c\right| & \leq & \frac{1}{\Im(z)} & \textblue{(\text{début du lemme~\ref{lemme2}})} \\
				& \leq & \frac{2}{\sqrt{3}} & \textblue{(\text{égalité dans le cas } j \text{ ou } -\frac{1}{j})} \\
				& < & 2 & \textblue{(\sqrt{3} > 1)} \\
				\end{array}
				\end{displaymath}
				Comme $c \in \Z$, on en déduit que $c \in \{0, -1, 1\}$.
				\begin{description}
					\item[Cas $c = 0$] On a $ad = 1$ \textblue{(car $\det A = 0$ et $c = 0$)}. Quitte à changer $A$ en $-A$ \textblue{(invariant pour le déterminant et l'action de groupe)}, on suppose que $a = d = 1$ et $A * z = z+ b$ \textblue{(calcul de l'action)}.
					\begin{itemize}
						\item Si $\left|\Re(z)\right| < \frac{1}{2}$ alors $b = 0$ et $A \in \{Id, -Id\}$ \textblue{($A * z = z + b \in D$ et $b \in \Z$, $0$ est la seule valeur qui convient)}.
						\item Si $\left|\Re(z)\right| = - \frac{1}{2}$ alors $b \in \{0, 1\}$ et $A \in \{Id, -Id, T\}$ \textblue{\text{(même argument)}}.
						\item Si $\left|\Re(z)\right| = \frac{1}{2}$ alors $b \in \{0, -1\}$ et $A \in \{Id, -Id, T^{-1}\}$ \textblue{\text{(même argument)}}.
					\end{itemize}
					\item[Cas $c = 1$] On a alors $\left|z + d\right| \leq 1$. On a déduit que $d = 0$ ou $z = j$ et $d = 1$ \textblue{(car $j = -\frac{1}{2} + i \frac{\sqrt{3}}{2}$ donc $\left|j + 1\right| = \frac{1}{2} + i \frac{\sqrt{3}}{2} = 1$)} ou $z = -\frac{1}{j}$ et $d = -1$ \textblue{(car $\frac{1}{j} = \frac{1}{2} + i \frac{\sqrt{3}}{2}$ donc $\left|j - 1\right| = -\frac{1}{2} + i \frac{\sqrt{3}}{2} = 1$)}.
					\begin{itemize}
						\item Si $d = 0$, alors $1 = \det A = -b$ \textblue{($\det A = ad - bc$ avec $d = 0$ et $c = 1$)} et $A * z = a - \frac{1}{z}$ \textblue{(car $A * z = \frac{az + b}{cz + d}$ avec $d = 0$ et $c = 1$}. Comme $\left|z\right| \leq 1$ \textblue{(par hypothèse)} et $\left|z\right| \geq 1$ \textblue{(car $z \in D$)}, on a $\left|z\right| = 1$. 
						\begin{itemize}
							\item Dans ce cas $\frac{1}{z}$ est le symétrique de $z$ par rapport à l'axe des imaginaires purs.
							\item $\left(a - \frac{1}{z}\right)$ ne peut être dans $D$ que si $a = 0$ ou si $z = j$ et $a = -1$ ou si $z = \frac{1}{j}$ et $a = 1$ \textblue{($z \in D$ si et seulement si $\left|z\right| \geq 1$ et $\left|\Re(z)\right| \leq \frac{1}{2}$)}.
						\end{itemize}
						Dans ce cas, $A \in \{S, (ST)^2, TS\}$.
						\item Si $z = j$ et $d = 1$. Comme $\det A = 1 = a - b$ et $A * j = \frac{aj + (a - 1)}{j + 1} = a - \frac{1}{j} = a + j$. Comme $a + j \in D$ alors $a = 0$ ou $a = 1$. Dans ce cas, $A \in \{ST, TST\}$
						
						\item Si $z = -\frac{1}{j}$ et $d = -1$. De même, on trouve que $A \in \{(TS)^2, T^{-1}ST^{-1}\}$.
					\end{itemize}
					\item[Cas $c = -1$] On se ramène au cas précédent en considérant $A$ par $-A$ \textblue{(ce qui ne modifie pas $A * z$)}.
				\end{description}
				
				
				\item[Cas $\Im(A * z) \geq \Im(z)$] Comme $\Im(A * z) < \Im(A^{-1}(A*z))$, on applique le cas précédent à $A^{-1}$.
			\end{description}
		\end{proof}
		
		Soit $z \in \mathring{D}$. Soit $A \in G$, $A * z$ est sur l'orbite de $O_z$ de $z$.
		\begin{itemize}
			\item Le lemme~\ref{lemme2} assure qu'il existe $B \in G$ tel que $B * (A * z) = BA * z \in D$.
			\item Le lemme~\ref{lemme3} assure que $BA \in \{Id, -Id\}$ et donc $A \in \{B, B^{-1}\}$ \textblue{(on est nécessairement dans le cas $c = 0$ et $\left|\Re(z)\right| < \frac{1}{2}$ car sinon $z \notin \mathring{D}$)} \textblue{(car c'est le seul cas qui considère un élément dans l'intérieur de $D$)}.
			\item Comme $S^{2} = -Id$, $G = SL_2(\Z)$.
		\end{itemize}
	\end{proof}
	
	\section{Compléments sur les actions de groupes}
	
	\input{./../Notions/ActionsGroupes.tex}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}