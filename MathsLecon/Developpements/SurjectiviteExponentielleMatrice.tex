\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
		};
		%\draw[color=blue] (box.north west)--(box.north east);
		\node[title] at (box.north) {#2};
		\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\Ck}{\mathcal{C}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{OliveGreen}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	\newcommand{\textmagenta}[1]{\textcolor{DarkOrchid}{#1}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\newcommand{\pcite}[1]{\protect{\cite{#1}}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	\newtheorem*{cex}{Contre-exemple}
	
	\title{Image de l'exponentielle de matrice}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Un max de maths \cite[p.40]{Zavidovique}.
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 153 (Polynômes endomorphismes et réduction); 156 (Exponentielle de matrice); 214 (TIL, TFI).
		}}

	\section{Introduction}
	
	L'exponentielle de matrice est à priori non injective et non surjective si on la définie de $\mathcal{M}_n(\C)$ dans lui-même. Cependant, en restreignant les domaines d'entrés ou d'arrivés nous arrivons à obtenir une application injective, surjective ou même bijective \textblue{(pas bête la bête)}: on se ramène alors à une étude d'image. Lorsqu'on étudie les polynômes d'endomorphismes sur le corps $\C$, l'exponentielle est surjective.
	
	Le calcul de l'image de l'application exponentielle sur les polynômes d'endomorphismes à coefficients dans $\C$ fait appelle à des raisonnement classique sur les groupes topologiques et via l'utilisation de la connexité. Ces groupes possèdent des lois internes continues et $\C[A]$ en ait un (comme l'image de l'exponentielle). Utiliser la connexité pour montrer l'égalité de deux ensembles est un raisonnement commun. De plus, pour montrer le caractère ouvert ou fermé de $\exp(\C[A])$ on utilise des arguments classiques de cette théorie (ce qui est toujours bon à se souvenir).
	
	\section{Exponentielles de matrices surjectives}
	
	\begin{theo}
		Pour tout $A \in \mathcal{M}_n(\C)$, $\exp(\C[A]) = \C[A]^{\times}$.
	\end{theo}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement}}}{
		\begin{enumerate}
			\item Montrer que $\C[A]^{\times} = \C[A] \cap GL_n(\C)$.
			\item Montrer que $\exp\left(\C[A]\right) \subseteq \C[A]^{\times}$.
			\item Montrer que $\C[A]^{\times}$ est connexe dans $\C[A]$.
			\begin{enumerate}
				\item Montrer que $\C[A]^{\times}$ est ouvert dans $\C[A]$.
				\item Montrer que $\C[A]^{\times}$ est connexe par arc dans $\C[A]$
			\end{enumerate}
			\item Montrer que $\exp\left(\C[A]\right)$ est ouvert dans $\C[A]^{\times}$.
			\item Montrer que $\exp\left(\C[A]\right)$ est fermé dans $\C[A]^{\times}$.
			\item Conclure.
		\end{enumerate}
	}
	
	
	\begin{proof} Soit $A \in \mathcal{M}_n(\C)$.
		\paragraph{Étape 1: Montrons que $\C[A]^{\times} = \C[A] \cap GL_n(\C)$}
		
		\begin{description}
			\item[$\subseteq$] Soit $M \in \C[A]^{\times}$. Alors $A \in \C[A]  \cap GL_n(\C)$ \textblue{(car $\C[A]^{\times} \subset \C[A]$ et comme $M \in \C[A]^{\times}$, $\exists N \in \C[A]$ tel que $MN = I_n$)}.
			\item[$\supseteq$] Soit $M \in \C[A] \cap GL_n(\C)$. Le polynôme caractéristique de $M$ est $\chi_M = \sum_{i = 0}^{n} a_iX^i$ ou $a_0 \neq 0$ \textblue{(sinon $\chi_M = X\left(\sum_{i = 1}^{n} a_i X^i\right)$ et en l'évaluant en $M$, on obtient que $\chi_M(M) = M\left(\sum_{i = 1}^{n} a_i M^i\right) = 0$ et $M \notin GL_n(\C)$)}. Par le théorème de Cayley--Hamilton, $\chi_M(M) = 0$. Donc $M\underbrace{\left(- \frac{1}{a_0} \sum_{i = 1}^{n} a_iM^{i-1}\right)}_{\in \C[A]} = I_n$. D'où $M \in \C[A]^{\times}$.
		\end{description}
		
		\paragraph{Étape 2:  Montrons que $\exp\left(\C[A]\right) \subseteq \C[A]^{\times}$}
		\begin{itemize}
			\item Montrons que $\exp(\C[A]) \subseteq \C[A]$. $\C[A]$ est un sous-espace vectoriel de $\mathcal{M}_n(\C)$. Il est donc fermé de $\mathcal{M}_n(\C)$ \textblue{(vrai en dimension finie, corollaire de l'équivalence des normes)}. La matrice $\exp(A) = \lim\limits_{N \to \infty} \sum_{k = 0}^{N} \frac{1}{k!} A^k$ est une limite de polynôme en $A$ \textblue{(continuité)}. Donc $\exp(A) \in \C[A]$.
			
			\item Montrons que $\exp(\C[A]) \subseteq GL_n(\C)$. On a $\det(\exp(M)) = \exp(\mathrm{Tr})(M) > 0$.
		\end{itemize}
		On a alors, $\exp(\C[A]) \in \C[A] \cap GL_n(\C)$. Par l'étape 1, comme $\C[A]^{\times} = \C[A] \cap GL_n(\C)$, $\exp(\C[A]) \in \C[A]^{\times}$.
		
		
		\paragraph{Étape 3: Montrons que $\C[A]^{\times}$ est connexe dans $\C[A]$}
		\subparagraph{Étape a: $\C[A]^{\times}$ est ouvert dans $\C[A]$} Par l'étape 1, on a $\C[A]^{\times} =\C[A] \cap GL_n(\R)$. 
		
		\subparagraph{Étape b : $\C[A]^{\times}$ est connexe par arc dans $\C[A]$} Soient $M, N \in \C[A]^{\times}$.
		\begin{itemize}
			\item La droite affine complexe $z \mapsto M(z) = zM + (1 - z)N$, $z \in \C$ est entièrement contenue dans $\C[A]$.
			\item On cherche donc un chemin de $M$ à $N$ sous la forme $M(\gamma(t)) = \gamma(t)M + (1- \gamma(t))N$, pour tout $t \in \left[0, 1\right]$ où $\gamma$ est une fonction continue et $\gamma(0) = 0$, $\gamma(1) = 1$. 
			\begin{itemize}
				\item On pose $P(z) = \det (M(z))$ polynôme en $z$ \textblue{(bien polynomial car composition de $\det$ qui est un polynôme avec $M(z)$ également un polynôme)}.
				\item On remarque que $P \neq 0$ car $P(M(0)) = P(N) \neq 0$ \textblue{($N$ est inversible)}. Donc $P$ ne s'annule qu'en un nombre fini de points.
				\item On construit ces chemins en évitant ces points : $\forall t \in \left[0, 1\right]$, $\gamma(t) = t + iat(1 - t)$. Donc $(t, a) \mapsto \gamma_a(t)$ est injective.
			\end{itemize}
			Donc $\C[A]^{\times}$ est connexe par arcs.
		\end{itemize}
		
		Comme $\C[A]^{\times}$ est connexe par arcs et ouvert dans $\C[A]$, il est alors connexe dans $\C[A]$.
		
		\paragraph{Étape 4: Montrons que $\exp\left(\C[A]\right)$ est ouvert dans $\C[A]^{\times}$}
		\begin{itemize}
			\item Étudions la différentielle en $0$ de $\exp : \mathcal{M}_n(\C) \to GL_n(\C)$. On a \textblue{(par un développement en série entière)} que $D \exp (0) = I_n$. On en déduite que la différentielle en $0$ de $\exp: \C[A] \to \C[A]^{\times}$ est bijective \textblue{($D \exp (0) = I_n \in \C[A]$)}.
			
			\item Le théorème d'inversion locale nous assure l'existence de deux ouverts de $\C[A]$, $0_{\mathcal{M}_n(\C)} \in U$ et $Id_n \in V$ tels que l'exponentielle réalise un $\mathcal{C}^1$-difféomorphisme de $\mathcal{U}$ à $\mathcal{V}$ \textblue{($\exp \left(\C[A]\right)$ contient un voisinage de $I_n$)}.
			
			\item On exhibe le voisinage. Soit $\exp(M) \in \exp(\C[A])$. On pose $\mathcal{V}_M = \{V\exp(M), V \in \mathcal{V}\}$. Soit $V\exp(M) \in \mathcal{V}_M$, $V = \exp(U)$ avec $U \in \mathcal{U}$ et $U, V \in \C[A]$ \textblue{($\mathcal{C}^1$-difféomorphisme)}. Comme l'exponentielle commute avec ses voisins, $V \exp(M) = \exp(U + M) \in \C[A]$.
		\end{itemize}
		Donc $\exp(\C[A])$ est ouvert dans $\C[A]^{\times}$ car l'image de l'exponentielle est voisin de tous ces points.
		
		\paragraph{Étape 5: Montrons que $\exp\left(\C[A]\right)$ est fermé dans $\C[A]^{\times}$}
		\begin{itemize}
			\item $\C[A]^{\times} \setminus \exp(\C[A]) = \cup_{M \in \C[A]^{\times} \setminus \exp(\C[A])} M\exp(\C[A])$.
			\begin{description}
				\item[$\subseteq$] 
				\begin{displaymath}
				\begin{array}{ccll}
				\C[A]^{\times} \setminus \exp(\C[A]) & = & \cup_{M \in \C[A]^{\times} \setminus \exp(\C[A])} M\exp(0) & \textblue{(\exp(0) = I_n)} \\
				& \subseteq & cup_{M \in \C[A]^{\times} \setminus \exp(\C[A])} M\exp(\C[A]) & \\
				\end{array}
				\end{displaymath}
				\item[$\supseteq$] Soient $M \in \C[A]^{\times} \setminus \exp(\C[A])$ et $N = M\exp(B)$ avec $B \in \C[A]$, alors $M = N\exp(-B)$. Supposons par l'absurde que $N \in \exp(\C[A])$, alors $M \in \exp(\C[A])$ \textblue{(car $\exp(\C[A])$ est un groupe multiplicatif)}. Contradiction.
			\end{description}
			\item $\C[A]^{\times} \setminus \exp(\C[A])$ est donc ouvert dans $\C[A]^{\times}$ par réunion d'ouverts. On en conclut que $\exp(\C[A])$ est fermé dans $\C[A]^{\times}$.
		\end{itemize}
		
		\paragraph{Étape 6: Conclure} Par connexité de $\C[A]^{\times}$ \textblue{(étape 3)} et comme $\exp(\C[A])$ est un ouvert \textblue{(étape 4)} et un fermé \textblue{(étape 5)} de $\C[A]^{\times}$, on a $\exp(\C[A]) = \C[A]^{\times}$.
		
	\end{proof}
	
	\begin{cor}
		L'image de $\mathcal{M}_n(\C)$ par l'exponentielle est $GL_n(\C)$.
	\end{cor}
	\begin{proof}
		Soit $A \in GL_n(\C)$. Comme $A \in \C[A]^{\times}$ et $\exp(\C[A]) = \C[A]^{\times}$, on a l'existence de $B \in \C[A]$ \textblue{(donc $B \in \mathcal{M}_n(\C)$)} tel que $A = \exp(B)$.
	\end{proof}
	
	\begin{cor}
		On a $\exp\left(\mathcal{M}_n(\R)\right) = GL_n(\R)^2 = \{A^2 ~|~ A \in GL_n(\R)\}$.
	\end{cor}
	\begin{proof}
		\begin{description}
			\item[$\subseteq$] $M \in \mathcal{M}_n(\R)$, alors $\exp(M) = \exp\left(\frac{M}{2}\right)^2$.
			\item[$\supseteq$] Soit $B = A^2$ avec $A \in GL_n(\R)$, on a $A = \exp\left(\overline{P}(A)\right)$. Alors, $B = A^2 = \exp(P + \overline{P}(A))$.
		\end{description}
	\end{proof}
	
	
	\section{Compléments autours de l'exponentielle de matrices}
	
	\subsection*{Exponentielle de matrices}
	\input{./../Notions/ExponentielleMatrice.tex}
	
	\subsection*{Le théorème d'inversion locale}
	\input{./../Notions/CalculDiff_TIL.tex}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
	
\end{document}