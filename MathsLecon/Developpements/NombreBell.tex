\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}


\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\Cun}{\mathcal{C}^1}
	\newcommand{\Cdeux}{\mathcal{C}^2}
	\newcommand{\Cinf}{\mathcal{C}^{\infty}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{OliveGreen}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	\newcommand{\textmagenta}[1]{\textcolor{DarkOrchid}{#1}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\newcommand{\pcite}[1]{\protect{\cite{#1}}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{remarque}{Remarque}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	
	\title{Nombres de Bell}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} X-ENS algèbre 1 \cite[p.14]{FrancinouGianellaNicolas-al1}.
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 190 (Dénombrement); 230 (Séries numériques); 243 (Série entière).
		}}
	
	\section{Introduction}
	
	Les nombres de Bell dénotent le nombre de partitions distinctes de l'ensemble $\llbracket 1, n \rrbracket$ où de manière équivalente, le nombre de relations d'équivalence sur cet ensemble. On dénombre alors l'ensemble des partitions de cet ensemble d'entier. Ce nombre s'exprime à l'aide d'une série numérique (le résultat est sa somme). On utilise une série génératrice, définie par une série entière que nous évaluons en une valeur précise.
	
	\section{Dénombrer le nombre de partitions : nombre de Bell}
	
	\begin{theo}
		Soit $n \geq 0$. On note $B_n$ le nombre de partitions distinctes de  l'ensemble $\llbracket 1, n \rrbracket$ (avec $B_0 = 1$). Alors
		\begin{enumerate}
			\item La série entière $\sum_{n = 0}^{+ \infty} \frac{B_n}{n!} z^n$ a un rayon de convergence $R > 0$ et sa somme $f$ vérifie $\forall z \in \left] -R, R\right[$, $f(z) = e^{e^z - 1}$.
			\item Pour tout $k \in \N$, $B_k = \frac{1}{e} \left(\sum_{n = 1}^{+ \infty} \frac{n^k}{n!}\right)$.
		\end{enumerate}
	\end{theo}
	
	\begin{ex}
		\begin{itemize}
			\item $B_0 = 1$ car la seule partition de $\emptyset$ est $\{\emptyset\}$.
			\item $B_1 = 1$ car la seule partition de $\{1\}$ est $\{1\}$.
			\item $B_2 = 2$ car les partitions de $\{1, 2\}$ sont $\{\{\{1\}, \{2\}\}, \{\{1, 2\}\}\}$
			\item $B_3 = 5$ car les partitions de $\llbracket 1, 3\rrbracket$ sont $\{\{\{1\}, \{2\}, \{3\}\}, \{\{1, 2\}, \{3\}\}, \{\{1, 3\}, \{2\}\}, \{\{2, 3\}, \{1\}\}, \{\{1, 2, 3\}\}\}$.
		\end{itemize}
	\end{ex}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement}}}{
		Si on a le temps, on peut faire un ou deux exemples de calculs de $B_n$ (pour $n$ assez petit) après avoir énoncer le théorème.
		\begin{enumerate}
			\item Établir la relation de récurrence suivante $B_{n+1} = \sum_{k = 0}^{n} \binom{n}{k} B_k$.
			\begin{enumerate}
				\item Calcul de $\left|E_k\right|$.
				\item Exprimer la récurrence.
			\end{enumerate}
			\item Majorer $B_n$: raisonnement par récurrence.
			\item Étude de la série entière.
			\begin{enumerate}
				\item Calcul du rayon de convergence.
				\item Calcul de la somme
			\end{enumerate}
			\item Expression de $B_n$.
		\end{enumerate}
	}
	
	\begin{proof}
		Soit $n \geq 0$.
		\paragraph{Étape 1 : établissons la relation de récurrence suivante $B_{n+1} = \sum_{k = 0}^{n} \binom{n}{k} B_k$} Pour $n = 0$ cette relation est vérifiée: $B_1 = 1$ et $\sum_{k = 0}^{0} \binom{0}{k} B_0 = B_0 = 1$. Établissons cette relation de récurrence pour $n \in \N$. Notons $E_k$ l'ensemble des partitions de $\llbracket 1, n+1 \rrbracket$ pour lesquelles la partie contenant $(n+1)$ est de cardinal $(k+1)$.
		
		\subparagraph{Étape a : calcul de $\left|E_k\right|$} Montrons que $\left|E_k\right| = \binom{n}{k} B_{n-k}$.
		\begin{itemize}
			\item Constituer la partie contenant $(n + 1)$ consiste en choisir $k$ éléments dans $\llbracket 1, n \rrbracket$: $\binom{n}{k}$ choix.
			\item Réaliser les partitions des $(n - k)$ éléments restants : $B_{n - k}$ choix \textblue{(définition de $B_j$)}.
		\end{itemize}
		
		\subparagraph{Étape b: exprimons la récurrence} Soit $\{E_0, \dots, E_n\}$ une partition de $\{\text{partition de } \llbracket 1, n + 1 \rrbracket\}$. En effet:
		\begin{itemize}
			\item $\forall i \neq j$, $E_i \cap E_j = \emptyset$ car les parties contenant $(n + 1)$ sont de cardinal $(i + 1)$ ou $(j + 1)$ (qui sont distincts) : les partitions ne peuvent donc pas être les mêmes.
			\item $E_0, \dots, E_n$ décrivent l'ensemble des partitions de $\llbracket 1, n + 1 \rrbracket$ car ils décrivent l'ensembles des partitions selon la taille de la partie contenant $(n + 1)$.
		\end{itemize}
		On a alors
		\begin{displaymath}
		\begin{array}{ccll}
		B_{n + 1} & = & \left|\cup_{k=0}^{n} E_k\right| & \textblue{\text{(on compte le nombre de partition grâce à la partition des partitions)}} \\
		& = & \sum_{k = 0}^{n} \left|E_k\right| & \textblue{\text{(car ils forment une partition)}} \\
		& = & \sum_{k = 0}^{n} \binom{n}{k} B_{n-k} & \textblue{\text{(par le calcul précédent)}} \\
		& = & \sum_{j = 0}^{n} \binom{n}{n - j} B_{j} & \textblue{(\text{changement d'indice } j = n - k)} \\
		& = & \sum_{j = 0}^{n} \binom{n}{j} B_{j} & \textblue{(\binom{n}{n - j} = \binom{n}{j})} \\
		\end{array}
		\end{displaymath}
		
		\paragraph{Étape 2 : majorons $B_n$} Montrons par récurrence sur $n \in \N$ la propriété $\mathcal{P}_n$ : $"B_n \leq n!$.
		\begin{description}
			\item[Initialisation] Pour $n = 0$, par hypothèse, on a $B_0 = 1 \leq 1 = 0!$.
			\item[Hérédité] Soit $n \in \N$ tel que $\forall k \leq n$ $\mathcal{P}_k$ soit vérifiée. On a alors
			\begin{displaymath}
			\begin{array}{ccll}
			B_{n + 1} & = & \sum_{k = 0}^{n} \binom{n}{k} B_{k} & \textblue{\text{(Étape 1)}} \\
			& \leq & \sum_{j = 0}^{n} \binom{n}{j} k! & \textblue{((\mathcal{P}_k))} \\
			& = & \sum_{k = 0}^{n} \frac{n!}{(n - k)! k!} k! & \textblue{(\text{définition de } \binom{n}{k})} \\
			& = & n! \sum_{j = 0}^{n} \frac{1}{(n - k)! k!} & \textblue{\text{(simplification)}} \\
			& \leq & n! \sum_{j = 0}^{n} 1 & \textblue{(\frac{1}{(n - k)! k!} \leq 1)} \\
			& = & n!(n + 1) & \textblue{\text{(manipulation de la somme)}} \\
			& = & (n + 1)! & \textblue{\text{(manipulation de la factorielle)}} \\
			\end{array}
			\end{displaymath}
			D'où $\mathcal{P}_{n + 1}$.
		\end{description}
		
		\paragraph{Étape 3 : montrons que la série entière $\sum_{n = 0}^{+ \infty} \frac{B_n}{n!} z^n$ a un rayon de convergence $R > 0$ et sa somme $f$ vérifie $\forall z \in \left] -R, R\right[$, $f(z) = e^{e^z - 1}$}
		
		\subparagraph{Étape a : calcul du rayon de convergence} On a alors $\forall n \geq 0$ et $\forall z \in \C$, $\frac{B_n}{n!} z^n \leq \left|z\right|^n$ \textblue{(car $\frac{B_n}{n!} \leq 1$)}. La série a alors un rayon de convergence $R \geq 1$. En particulier, $R \neq 0$.
		
		\subparagraph{Étape b : calcul de la somme} Comme $R \neq 0$, pour tout $z \in \left] -R, R\right[$, on peut écrire
		\begin{displaymath}
		\begin{array}{ccll}
		f(z) & = & \sum_{n = 0}^{+ \infty} \frac{B_n}{n!} z^n & \textblue{\text{(Définition de $f$)}} \\
		& = & 1 + \sum_{n = 0}^{+ \infty} \frac{B_{n + 1}}{(n + 1)!} z^{n + 1} & \textblue{\text{(On sort le premier terme de la somme)}} \\
		\end{array}
		\end{displaymath}
		On peut alors dérivé terme à terme cette série entière:
		\begin{displaymath}
		\begin{array}{ccll}
		f'(z) & = & \sum_{n = 0}^{+ \infty} \frac{1}{n!} \left(\sum_{k = 0}^{n} \binom{n}{k} B_{k}\right) z^n & \textblue{\text{(Dérivation ($B_{n+1}$ est une constante) et expression de $B_{n+1}$)}} \\
		& = &\sum_{n = 0}^{+ \infty} \left(\sum_{k = 0}^{n} \frac{B_k}{k (n - k)!} \right) z^{n} & \textblue{\text{(par définition du binome et en simplifiant l'expression obtenue)}} \\
		\end{array}
		\end{displaymath}
		On reconnaît le produit de Cauchy entre les séries $\sum \frac{B_n}{n!} z^n$ et $\sum \frac{z^n}{n!}$ \textblue{(de rayon de convergence $+\infty$ et de somme $e^z$)} qui ont toutes les deux un rayon de convergence supérieur ou égal à $R$. Donc $\forall z \in \left] -R, R\right[$, $f'(z) = f(z)e^z$. On en déduit qu'il existe $C \in \R$ tel que, pour tout $z \in \left] -R, R\right[$, $f(z) = Ce^{e^z}$.
		
		Comme $f(0) = B_0 = 1$, on en déduit que $C = \frac{1}{e}$ \textblue{(car $Ce^{e^z} = Ce = 1$ si $z = 0$)}. On conclut que pour tout $z \in \left] -R, R\right[$, $f(z) = \frac{1}{e}e^{e^z} = e^{e^z - 1}$. 
		
		\paragraph{Étape 4 : montrons que pour tout $k \in \N$, $B_k = \frac{1}{e} \left(\sum_{n = 1}^{+ \infty} \frac{n^k}{n!}\right)$}
		\begin{itemize}
			\item La série entière définissant la fonction exponentielle ayant un rayon de convergence infini, on a $\forall z \in \C$,
			\begin{displaymath}
			\begin{array}{ccll}
			e^{e^z} & = & \sum_{n = 0}^{+ \infty} \frac{e^{nz}}{n!} & \textblue{(\text{série entière de } e^z)} \\
			& = & \sum_{n = 0}^{+ \infty} \frac{1}{n!} \left(\sum_{k = 0}^{+ \infty} \frac{(nz)^k}{k!}\right) & \textblue{(\text{série entière de } e^{nz})} \\
			\end{array}
			\end{displaymath}
			
			\item Pour tout $k, n \in \N$, on note $u_{n, k} = \frac{(nz)^k}{n!k!}$. Montrons que $\left(u_{n, k}\right)_{n, k}$ est sommable. Pour tout $n \in \N$,
			\begin{displaymath}
			\begin{array}{ccll}
			\sum_{k = 0}^{+ \infty} \left|u_{n, k}\right| & = & \sum_{k = 0}^{+ \infty} \frac{\left|nz\right|^k}{n!k!} & \textblue{\text{(Définition de $u_{n, k}$ et propriété du module)}} \\
			& = & \frac{e^{\left|nz\right|}}{n!} & \textblue{(\text{série entière de } e^{nz})} \\
			\end{array}
			\end{displaymath}
			On obtient le terme général d'une série convergeant vers $e^{e^{\left|z\right|}}$. La double somme est donc bien sommable.
			
			\item On peut donc sommer cette famille dans l'ordre que l'on souhaite \textblue{(cas particulier de Fubini)}. Pour tout $z \in \left] -R, R \right[$, on a:
			\begin{displaymath}
			\begin{array}{ccll}
			f(z) & = & \frac{1}{e} \sum_{n = 0}^{\infty} \left(\sum_{k = 0}^{\infty} u_{n, k}\right) & \textblue{\text{(Définition de $f$)}} \\
			& = & \frac{1}{e} \sum_{k = 0}^{\infty} \left(\sum_{n = 0}^{\infty} u_{n, k}\right) & \textblue{\text{(Interversion des sommes)}} \\
			& = & \frac{1}{e} \sum_{k = 0}^{\infty} \left(\sum_{n = 0}^{\infty} \frac{n^k}{n!}\right) \frac{z^k}{k!} & \textblue{\text{(Définition de $u_{n, k}$ et sortir les termes ne dépendant pas de $n$)}} \\
			\end{array}		
			\end{displaymath}
			
			\item Par unicité du développement en série entière de $f$ sur $\left] -R, R \right[$, on en déduit que pour tout $k \geq 0$, $B_k = \frac{1}{e} \left(\sum_{n = 1}^{+ \infty} \frac{n^k}{n!}\right)$.
		\end{itemize}
		
		\begin{remarque}
			On vient de montrer qu'en réalité la rayon de convergence de $f$ est $\infty$.
		\end{remarque}
	\end{proof}

	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}