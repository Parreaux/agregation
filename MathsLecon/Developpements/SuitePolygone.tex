\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{pgf,tikz,pgfplots}
\pgfplotsset{compat}
\usepackage{mathrsfs}
\usetikzlibrary{arrows}

\usepackage{array}
\usepackage{multirow}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
} 

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\Cun}{\mathcal{C}^1}
	\newcommand{\Cdeux}{\mathcal{C}^2}
	\newcommand{\Cinf}{\mathcal{C}^{\infty}}
	\newcommand{\Sn}{\mathfrak{S}}
	
	\sloppy
	
	\newcommand{\textblue}{\textcolor{blue}}
	\newcommand{\textgreen}{\textcolor{OliveGreen}}
	\newcommand{\textred}{\textcolor{red}}
	\newcommand{\textbrown}{\textcolor{Brown}}
	\newcommand{\textmagenta}{\textcolor{DarkOrchid}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\newcommand{\pcite}[1]{\protect{\cite{#1}}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	
	\title{Suite de polygones}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Gourdon \cite{Gourdon-algebre} (pour le déterminant circulant)
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 152 (Déterminant); 182 (Nombre complexe en géométrie); 226 (Suites récurrentes).
		}}
	
	\section{Introduction}
	
	Le déterminant est un outil mathématique puissant. Dans cette étude, on l'utilise afin de calculer la limite d'une suire récurrente particulière : une suite définie à partir de polygones. En définissant une suite de polygones définie telle que les sommets soient sur les différents côtés, cette suite converge vers l'isobarycentre du polygone. Notons qu'elle peut être définie comme on le souhaite (les nouveaux sommets doivent juste être sur un des côtés) sans que cela impacte sa limite.
	
	\section{Suite de polygones}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement}}}{
		\begin{itemize}
			\item Calcul du déterminant circulant
			\item Application aux calcul de la limite d'une suite de polygones 
			\begin{enumerate}
				\item Réécrire la suite récurrente avec une matrice.
				\item Étude des valeurs propres de $M$.
				\item Existence de la limite.
				\item Identification de la limite.
			\end{enumerate}
		\end{itemize}
	}
	
	\begin{lemme}[Calcul du déterminant d'une matrice circulante]
		Soit $A = \begin{pmatrix}
		a_1 & a_2 & \dots & a_n \\
		a_n & a_1 & \dots & a_{n-1} \\
		\vdots & & & \vdots \\
		a_2 & a_3 & \dots & a_1 \\
		\end{pmatrix} \in \mathcal{M}_n(\C)$ une matrice circulante et $P_A = \sum_{k = 1}^{n} a_kX^k$. Alors, $\det A = \prod_{k=1}^{n} P_A(w^k)$ où $ w = e^{\frac{2i\pi}{n}}$.
	\end{lemme}
	\begin{proof}
		On note $\Omega = \begin{pmatrix}
		1 & 1 & \dots & \dots & 1 \\
		1 & w & w^2 & \dots & w^{n-1} \\
		\vdots & & & & \vdots \\
		1 & w^{n-1} & (w^2)^{n-1} & \dots & (w^{n-1})^{n-1} \\
		\end{pmatrix}$.
		\begin{itemize}
			\item On remarque que $\det \Omega \neq 0$ car c'est un déterminant de Vandermond \textblue{(car les $w^i$ sont deux à deux distincts)}.
			\item On remarque que $\det(A\Omega) = \det \begin{pmatrix}
			P(1) & P(w) & \dots & P(w^{n-1}) \\
			P(1) & P(w)w & \dots & P(w^{n-1})w^{n-1} \\
			\vdots & & & \vdots \\
			P(1) & P(w)w^{n-1} & \dots & P(w^{n-1})(w^{n-1})^{n-1} \\
			\end{pmatrix}$
		\end{itemize}
		Donc $\det(A \Omega)  = \left\{ \begin{array}{ll}
		\det(A)\det(\Omega) & \textblue{\text{(par multiplicativité du déterminant)}} \\
		P(1)P(w) \dots P(w^{n-1})\det(\Omega) & \textblue{\text{(par propriété du déterminant)}} \\
		\end{array}\right.$
		
		Comme $\det \Omega \neq 0$, on obtient le résultat annoncé.
	\end{proof}
	
	\begin{minipage}{.46\textwidth}
		\begin{theo}
			On considère une suite de polygones de $n$ sommets dont les sommets sont pris dans $\C$, soit un $n-$uplet de $\C$, définie telle que pour tout $k \in \N$, le polygone $P_{k+1}$ définit à partir des milieux des côtés du polygone $P_k$. On souhaite montrer que cette suite converge vers l'isobarycentre de $P_0$.
		\end{theo}
		
		On donne, dans la figure~\ref{fig:exSuite}, un exemple d'une telle suite.
	\end{minipage} \hfill
	\begin{minipage}{.46\textwidth}
		\begin{figure}[H]
			\centering
			\includegraphics[scale = 0.5]{./../Figures/Polygone.pdf}
			\caption{Un exemple de suite polygone.}
			\label{fig:exSuite}
		\end{figure}
	\end{minipage}
	
	\begin{proof}
		Soit $\left(P_k\right)_k$ une suite de polygone à $n$ sommets. On note alors $P_0 = \left(z_1^{(0)}, \dots, z_n^{(0)}\right) \in \C^n$ et $P_{k+1} = \left(\frac{z_1^{(k)}+ z_2^{(k)}}{2}, \dots, \frac{z_{n-1}^{(k)}+ z_n^{(k)}}{2}\right)$. Montrons que la suite $P_k$ converge vers $g\left(1, \dots, 1\right)$ où $g$ est l'isobarycentre de $P_0$.
		
		\paragraph{Étape 1 : réécrire la suite récurrente avec une matrice.} On peut réécrire cette suite récurrente $P_{k+1} = MP_{k}$ avec 
		\begin{displaymath}
		M= \begin{bmatrix}
		\frac{1}{2} & \frac{1}{2} & 0 & \cdots  & 0 \\
		0 & \frac{1}{2} & \frac{1}{2} & \ddots & \vdots\\
		\vdots & \ddots & \ddots & \ddots & 0 \\
		0 & \dots & 0 & \frac{1}{2}& \frac{1}{2} \\
		\frac{1}{2} & 0 & \dots & 0 & \frac{1}{2} \\
		\end{bmatrix}
		\end{displaymath}
		Et par récurrence immédiate, on a $P_k = M^kP_0$. Il suffit alors de montrer que $\left(M^k\right)$ converge dans $\mathcal{M}_n(\C)$	muni d’une norme d’algèbre $\mathopen{|||}.\mathclose{|||}$. 
		
		\paragraph{Étape 2: étude les valeurs propres de $M$.} On étudie alors le polynôme caractéristique de $M$.
		\begin{displaymath}
		\chi_M\left(\lambda\right) = \det\left(M - \lambda Id\right) = \begin{bmatrix}
		a_0 & \frac{1}{2} & 0 & \cdots  & 0 \\
		0 & a_0 & \frac{1}{2} & \ddots & \vdots\\
		\vdots & \ddots & \ddots & \ddots & 0 \\
		0 & \dots& 0 & a_0 & \frac{1}{2} \\
		\frac{1}{2} & 0 & \cdots & 0 & a_0 \\
		\end{bmatrix}
		\end{displaymath}
		avec $a_0 = \frac{1}{2}- \lambda$. On reconnaît un déterminant circulant, et par le lemme
		\begin{displaymath}
		\chi_M\left(\lambda\right)  = \prod_{j=1}^{n}P\left(\frac{1}{2} - \lambda + \frac{1}{2}w^j\right) = \prod_{j=1}^{n}P\left(w^{j}\right) = \prod_{j=1}^{n}P\left(\lambda - \frac{1+w^j}{2}\right)
		\end{displaymath}
	
		\paragraph{Étape 3: existence de la limite.} Comme les $w^i$ sont deux à deux distinctes, le polynôme caractéristique de $M$, $\chi_M$ est scindé à racine simple, donc $M$ est diagonalisable de valeurs propres $\{\frac{1+w^j}{2} ~|~ 1\leq j \leq n \}$. Donc $\exists Q \in \mathrm{GL}_n(\C)$ telle que
		$M = Q \mathrm{Diag}\left(\frac{1+w}{2}, \dots, \frac{1+w^n}{2}\right)Q^{-1}$. Or, pour $j \neq n$, on a 
		\begin{displaymath}
		\left|\frac{1 + w^j}{2}\right| \underbrace{=}_{\text{calcul}} \left|\cos\left(\frac{\pi j}{n}\right)\right| \underbrace{<}_{\text{on est dans } \R} 1
		\end{displaymath}
		et donc $\left|\frac{1 + w^j}{2}\right|^k \to 0$.
		On a donc $\lim_{k \to \infty} M^{k} = Q \mathrm{Diag}\left(0, \dots, 0, 1\right)Q^{-1}$
		qui converge bien dans $\mathcal{M}_n(\C)$ et on note cette limite $M^{\infty}$. On en déduit, par la récurrence immédiate, $\lim_{k \to \infty} P_k = M^{\infty}P_0$ que l'on note $X$.
		
		\paragraph{Étape 4: identification de la limite.} Par continuité de la limite, on a $X = M^{\infty}X$. Or, l’espace correspondant à la valeur propre $1$ contient le vecteur $(1, \dots,	1)$, et comme il est de dimension $1$, ce vecteur le génère complètement, donc $X = (a, \dots, a)$, c’est à dire que $\left(P_k\right)$	converge vers le point d’affixe $a$.
		
		Enfin, on remarque que si $g$ est l’isobarycentre de $P_0$, il est aussi celui de $P_k$, car on a
		\begin{displaymath}
		g_{k+1} \underbrace{=}_{\text{def de l'isobarycentre}} \frac{1}{n} \sum_{i=1}^{n} z^{(k+1)}_i \underbrace{=}_{\text{def de la suite}} \frac{1}{n} \sum_{i=1}^{n} \frac{z_i^{(k)}+ z_{i+1}^{(k)}}{2} = \frac{1}{n} \sum_{i=1}^{n} z^{(k)}_i \underbrace{=}_{\text{def de l'isobarycentre}}  g_k
		\end{displaymath}
		Par continuité, $g$ est encore l’isobarycentre de $X$, d’où $a = g$.
	\end{proof}
	
	
	\section{Compléments autours du déterminant et de la diagonalisation}
	
	\subsection*{Déterminant}
	\input{./../Notions/AlgebreLineaire_Determinant-def.tex}
	
	\subsection*{Valeurs propres}
	\input{./../Notions/AlgebreLineaire_ValEspPropre.tex}
	
	\subsection*{Diagonalisation}
	\input{./../Notions/AlgebreLineaire_Diagonalisation.tex}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}

\end{document}