\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}


\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\Cun}{\mathcal{C}^1}
	\newcommand{\Cdeux}{\mathcal{C}^2}
	\newcommand{\Cinf}{\mathcal{C}^{\infty}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{OliveGreen}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	\newcommand{\textmagenta}[1]{\textcolor{DarkOrchid}{#1}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\newcommand{\pcite}[1]{\protect{\cite{#1}}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	
	\title{Calcul de l'intégrale de Fresnel}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Gourdon \cite[p.342]{Gourdon-analyse}.
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 236 (Calcul d'intégrale).
			
			\textblue{\emph{Leçons où il peut être évoqué:}}  239 (Fonction définie par une intégrale).
		}}
	
	\section{Introduction}
	
	Il existe plusieurs méthodes pour calculer une intégrale. Le calcul de l'intégrale de Fresnel permet d'en appliquer quelques unes. En effet, on  utilise, par exemple, la notion de fonction définie par une intégrale, le théorème de Fubbini, le changement de variable en dimension deux.
	
	\section{Calcul de l'intégrale de Fresnel}
	
	\begin{theo}
		La valeur de l'intégrale de Fresnel définie par $\varphi = \int_{0}^{+ \infty} e^{ix^2} dx$, est $e^{\pi/4} \frac{\sqrt{\pi}}{2}$.
	\end{theo}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement}}}{
		\begin{enumerate}
			\item Montrer que $\varphi$ existe.
			\item Montrer que $f(t)^2 = \frac{i\pi}{4}- i \int_{0}^{\frac{\pi}{4}} \exp\left(i\frac{t^2}{\cos^2 \theta}\right) d\theta$ où $f(t) = \int_{0}^{t} e^{ix^2} dx$.
			\begin{enumerate}
				\item Calculer $F$ via Fubini: $F(t) = f(t)^2$.
				\item Calculer $F$ via des coordonnées polaire: $F(t) = \frac{i\pi}{4}- i \int_{0}^{\frac{\pi}{4}} \exp\left(i\frac{t^2}{\cos^2 \theta}\right) d\theta$.
			\end{enumerate}
			\item Montrer que $I(T) = \frac{1}{T}\int_{0}^{T}F(t)dt$ converge lorsque $T$ tend vers $+ \infty$.
			\item Donner la valeur de $\varphi$.
		\end{enumerate}
	}
	
	\begin{proof}
		Notons $\varphi = \int_{0}^{+ \infty} e^{ix^2} dx$ l'intégrale de Fresnel. On pose $f(t) = \int_{0}^{t} e^{ix^2} dx$.
		\paragraph{Étape 1: montrons que $\varphi$ existe \cite[p.146,~rq.6]{Gourdon-analyse}} L'intégrale $\int_{0}^{+ \infty} e^{ix^2} dx$ converge: par le changement de variable $u = x^2$, $f$ est de même nature que l'intégrale $\int_{0}^{+\infty} e^{iu}u^{-1/2}$ qui est une intégrale convergente d'après la règle d'Abel. 
		
		\paragraph{Étape 2: montrons que $f(t)^2 = \frac{i\pi}{4}- i \int_{0}^{\frac{\pi}{4}} \exp\left(i\frac{t^2}{\cos^2 \theta}\right) d\theta$} Posons $F(t) = \iint_{\left[0, t\right]^2} e^{i(x^2 + y^2)} dxdy$, pour tout $t \geq 0$. Calculons $F$ de deux manières différentes.
		
		\subparagraph{Étape a : calculons $F$ via Fubini: $F(t) = f(t)^2$}
		\begin{displaymath}
		\begin{array}{ccll}
		F(t) & = & \iint_{\left[0, t\right]^2} e^{i(x^2 + y^2)} dxdy & \textblue{\text{(Définition de $F$)}} \\
		& = & \iint_{\left[0, t\right]^2} e^{ix^2}e^{iy^2} dxdy & \textblue{\text{(Propriétés de l'exponentielle)}} \\
		& = & \left(\int_{\left[0, t\right]} e^{ix^2}dx\right)\left(\int_{\left[0, t\right]}e^{iy^2}dy\right)  & \textblue{\text{(Par Fubini)}} \\
		& = & f(t)^2 & \textblue{\text{(Définition de $f$)}} \\
		\end{array}
		\end{displaymath}
		
		\subparagraph{Étape b : calculons $F$ via un changement de coordonnées polaire: $F(t) = \frac{i\pi}{4}- i \int_{0}^{\frac{\pi}{4}} \exp\left(i\frac{t^2}{\cos^2 \theta}\right) d\theta$}
		\begin{itemize}
			\item Symétrie du domaine et de l'intégrande par rapport à la droite $x = y$:
			\begin{displaymath}
			F(t) = 2 \iint_{\Delta_t} e^{i(x^2 + y^2)} dxdy \text{ où } \Delta_t = \{(x, y) \in \R^2 ~|~ 0 \leq x \leq t, 0 \leq y \leq x \}
			\end{displaymath}
			
			\item Expression en coordonnées polaires:
			\begin{itemize}
				\item Opérande: $x^2 + y^2 = e^{ir^2}r$.
				\item Compact $\Delta_t$: $K_T = \{(r, \theta) \in \R^{\times} \times \left[0, 2\pi\right] ~|~ 0 \leq r\cos \theta \leq t \text{ et } \theta \in \left[0, \frac{\pi}{4}\right]\}$.
			\end{itemize}
			
			\item Calcul de l'intégrale:
			\begin{displaymath}
			\begin{array}{ccll}
			F(t) & = & 2 \iint_{\Delta_t} e^{i(x^2 + y^2)} dxdy & \textblue{\text{(Précédemment)}} \\
			& = & 2 \iint_{K_T} e^{ir^2}r drd\theta & \textblue{\text{(Changement en coordonnées polaires)}} \\
			& = & 2 \int_{0}^{\frac{\pi}{4}} \left(\int_{0}^{\frac{t}{\cos \theta}}e^{ir^2}r dr\right) d\theta & \textblue{\text{(Théorème de Fubini)}} \\
			& = & \int_{0}^{\frac{\pi}{4}} \frac{1}{i}\left(\exp\left(i \frac{t^2}{\cos^2 \theta}\right)-1 \right)d\theta & \textblue{\text{(Calcul de primitive)}} \\
			& = & \frac{i\pi}{4}- i \int_{0}^{\frac{\pi}{4}} \exp\left(i\frac{t^2}{\cos^2 \theta}\right) d\theta & \textblue{\text{(Calcul)}} \\
			\end{array}
			\end{displaymath}
		\end{itemize}
		
		\paragraph{Étape 3: montrons que $I(T) = \frac{1}{T}\int_{0}^{T}F(t)dt$ converge lorsque $T$ tend vers $+ \infty$} Pour $T > 0$, on pose $I(T) = \frac{1}{T}\int_{0}^{T}F(t)dt$ et montrons que $I(T)$ converge lorsque $T$ tend vers $+ \infty$. On a 
		\begin{displaymath}
		\begin{array}{ccll}
		I(T) & = & \frac{1}{T}\int_{0}^{T}F(t)dt & \textblue{\text{(Définition de $I(T)$)}} \\
		& = & \frac{1}{T}\int_{0}^{T}\left(\frac{i\pi}{4}- i \int_{0}^{\frac{\pi}{4}} \exp\left(i\frac{t^2}{\cos^2 \theta}\right) d\theta\right)dt & \textblue{\text{(Définition de $F(t)$, étape 2)}} \\
		& = & \frac{1}{T}\left(\int_{0}^{T}\frac{i\pi}{4}dt- i \int_{0}^{T}\left(\int_{0}^{\frac{\pi}{4}} \exp\left(i\frac{t^2}{\cos^2 \theta}\right) d\theta\right) dt\right) & \textblue{\text{(Linéarité de l'intégrale)}} \\
		& = & \frac{1}{T}\frac{i\pi}{4}T - \frac{i}{T} \int_{0}^{T}\left(\int_{0}^{\frac{\pi}{4}} \exp\left(i\frac{t^2}{\cos^2 \theta}\right) d\theta\right) dt & \textblue{\text{(Calcul)}} \\
		& = & \frac{i\pi}{4} - \frac{i}{T} \int_{0}^{\frac{\pi}{4}}\left(\int_{0}^{T} \exp\left(i\frac{t^2}{\cos^2 \theta}\right) dt\right) d\theta & \textblue{\text{(Fubini)}} \\
		& = & \frac{i\pi}{4} - \frac{i}{T} \int_{0}^{\frac{\pi}{4}}\left(\int_{0}^{\frac{T}{\cos \theta}} \exp\left(iu^2\right)\cos \theta du\right) d\theta & \textblue{\text{(Changement de variables $u = \frac{t}{\cos \theta}$ avec $\cos \theta$ constante)}} \\
		& = & \frac{i\pi}{4} - \frac{i}{T} \int_{0}^{\frac{\pi}{4}}\cos\theta f\left(\frac{T}{\cos \theta}\right)d\theta & \textblue{\text{(Définition de $f$)}} \\
		\end{array}
		\end{displaymath}
		Par convergence de $\varphi$ \textblue{(étape 1)}, $f$ est bornée. Donc $\frac{1}{T} \int_{0}^{\frac{\pi}{4}}\cos\theta f\left(\frac{T}{\cos \theta}\right)d\theta \underset{T \to \infty}{\longrightarrow} 0$. On en déduit que $I(T) \underset{T \to \infty}{\longrightarrow} \frac{i\pi}{4}$
		
		\paragraph{Étape 4: donnons la valeur de $\varphi$}
		\begin{itemize}
			\item $F(t) = f(t)^2$, donc $ \forall T > 0$, $I(T) = \frac{1}{T} \int_{0}^{T} f(t)^2 dt$.
			\item $t \mapsto f(t)^2$ converge vers $\varphi^2$ dans $t \to \infty$. Par le théorème de la moyenne de Cesàro, $I(T)$ converge vers $\varphi^2$ quand $T \to \infty$.
			\item On en déduit que $\varphi^2 = \frac{i\pi}{4}$. On cherche à déterminer le signe de $\mathrm{Im} \varphi$ pour déterminer $\varphi$.
			\begin{displaymath}
			\begin{array}{ccll}
			\mathrm{Im} \varphi & = & \mathrm{Im} \int_{0}^{+ \infty} e^{ix^2} dx & \textblue{\text{(Définition de $\varphi$)}} \\
			& = & \mathrm{Im} \int_{0}^{+ \infty} e^{iu} \frac{du}{2\sqrt{u}} & \textblue{\text{(Changement de variables $u = x^2$)}} \\
			& = & \int_{0}^{+ \infty} \frac{\sin u}{2\sqrt{u}} du & \textblue{\text{(Formule de Gauss)}} \\
			& = & \sum_{n=0}^{+\infty}\int_{2n\pi}^{2(n+1)\pi} \frac{\sin u}{2\sqrt{u}} du & \textblue{\text{(Décomposition de l'intégrale)}} \\
			& = & \sum_{n=0}^{+\infty}\left(\int_{2n\pi}^{(2n+1)\pi} \frac{\sin u}{2\sqrt{u}} du + \int_{(2n + 1)\pi}^{(2n+2)\pi} \frac{\sin u}{2\sqrt{u}} du\right) & \textblue{\text{(Décomposition de l'intégrale)}} \\
			& = & \sum_{n=0}^{+\infty}\left(\int_{2n\pi}^{(2n+1)\pi} \frac{\sin u}{2\sqrt{u}} du - \int_{2n\pi}^{(2n+1)\pi} \frac{\sin u}{2\sqrt{u+\pi}} du\right) & \textblue{\text{($t = u + \pi$ et $\sin(u + \pi) = - \sin u$)}} \\
			& = & \sum_{n=0}^{+\infty}\int_{2n\pi}^{(2n+1)\pi} \frac{\sin u}{2}\left(\frac{1}{\sqrt{u}} - \frac{1}{\sqrt{u + \pi}}\right) du & \textblue{\text{(Linéarité de l'intégrale)}} \\
			\end{array}
			\end{displaymath}
			Donc $\mathrm{Im} \varphi \geq 0$ car $\forall n \in \N$, $\forall u \in \left[2n\pi, (2n + 1)\pi\right]$ on a $\sin u \left(\frac{1}{\sqrt{u}} - \frac{1}{\sqrt{u + \pi}}\right) \geq 0$ \textblue{(on a $u + \pi > u$ et par croissance de $x \mapsto \sqrt{x}$ et décroissance de $x \mapsto \frac{1}{x}$, $\frac{1}{\sqrt{u}} - \frac{1}{\sqrt{u + \pi}} \geq 0$, de plus $\sin u \geq 0$)} et on conclut grâce à la positivité de l'intégrale.
		\end{itemize}
		On en conclut que $\varphi = \frac{\sqrt{\pi}}{2}e^{\frac{i\pi}{4}}$.
	\end{proof}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}