\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\F}{\mathbb{F}}
	\newcommand{\Cun}{\mathcal{C}^1}
	\newcommand{\Cdeux}{\mathcal{C}^2}
	\newcommand{\Cinf}{\mathcal{C}^{\infty}}
	\newcommand{\Sn}{\mathfrak{S}}
	
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{OliveGreen}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	\newcommand{\textmagenta}[1]{\textcolor{DarkOrchid}{#1}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\newcommand{\pcite}[1]{\protect{\cite{#1}}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\newtheorem{lem}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	
	\title{Nombre d'automorphismes diagonalisables sur un corps fini}
	
	\author{Julie Parreaux}
	\date{2018-2019}
		
	\maketitle
		
	\noindent\emph{Références du développement:} H2G2 \cite[p.66]{Caldero-Germoni-N2}; Gourdon \cite[p.176]{Gourdon-algebre}; X-ENS algèbre 1 \cite[p.17]{FrancinouGianellaNicolas-al1}.
		
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
		\textblue{\emph{Leçons où on présente le développement:}} 101 (Action de groupe); 104 (Groupe fini); 106 (Groupe linéaire); 190 (Dénombrement).
				
		\textblue{\emph{Leçons où on peut en parler:}} 121 (Nombre premier); 123 (Corps fini).
	}}
	
	\section{Introduction}
	
	Le nombre de matrice sur un corps fini est dénombrable (et même fini). On peut alors compter le nombre de matrice vérifiant certaines propriétés comme le fait qu'elle soit ou non diagonalisable. Les actions de groupes sont un bon moyen de dénombrer des ensembles finis. Ici, on utilise la formule des classes pour une action naturelle du groupe linéaire d'un corps fini.
	
	\section{Matrice diagonalisable sur $\F_q$}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement}}}{
		\begin{enumerate}
			\item Énumération d'espaces vectoriels via une action de groupe.
			\begin{enumerate}
				\item Définition de l'action.
				\item Cette action est transitive.
				\item Calcul du cardinal.
			\end{enumerate}
			\item Caractérisation des automorphismes diagonalisables.
			\item Preuve du théorème.
		\end{enumerate}
	}
	
	\begin{theo}
		Soit $n \geq 1$ un entier. Le nombre de matrices diagonalisables dans le groupe linéaire $GL_n(\F_q)$ sur le corps fini $\F_q$ est égal à
		\begin{displaymath}
		\underset{\underset{n_1 + \dots + n_{q-1} = n}{n_1, \dots, n_{q-1}}}{\sum} \frac{\left| GL_n(\F_q) \right|}{\left| GL_{n_1}(\F_q) \right| \cdots \left|GL_{n_{q-1}}(\F_q)\right|}
		\end{displaymath}
	\end{theo}
	\begin{proof}
		Soit $n \geq 1$ un entier. Calculons le nombre de matrices diagonalisables dans le groupe linéaire $GL_n(\F_q)$ sur le corps fini $\F_q$.
		
		\paragraph{Étape 1 : énumération d'espaces vectoriels via une action de groupe.}
		\begin{lem}
			Soit $\mathcal{E}_k$ l'ensemble des $k$-uplets $\left(E_1, \dots E_k\right)$ de sous-espaces de $\F_q^n$ tels que $E_1 \oplus \dots \oplus E_k = \F_q^n$ avec $\dim E_i = n_i$ pour $1 \leq i \leq k$. Alors 
			\begin{displaymath}
			\left|\mathcal{E}_k\right| = \frac{\left|GL_n(\F_q)\right|}{\left|GL_{n_1}(\F_q)\right| \cdots \left|GL_{n_{q-1}}(\F_q)\right|}
			\end{displaymath}
			\label{lem:sousCard}
		\end{lem}
		\begin{proof}
			On fait agir $GL_n(\F_q)$ sur $\mathcal{E}_k$ par l'action naturelle $g.\left(E_1, \dots, E_k\right) = \left(g(E_1), \dots, g(E_k)\right)$.
			
			\subparagraph{Étape a: l'action est bien définie.}
				\begin{itemize}
					\item $\dim (g(E_i)) = \dim(E_i)$ \textblue{($g \in GL_n(\F_q)$ donc $g$ est un isomorphisme)} $= n_i$ \textblue{(hypothèse)} 
					\item Comme les $E_i$ sont en somme directe montrons que les $g(E_i)$ le sont également.
					\begin{itemize}[label=$\bullet$]
						\item Soit $y \in \sum_{i=1}^{k} g(E_i)$. Supposons que $y = \sum_{i=1}^{k} g(x_i) = \sum_{i=1}^{k} g(\tilde{x_i})$.
						
						\item Par linéarité de $g ~\textblue{\left(\in GL_n(\F_q)\right)}$, $y = g\left(\sum_{i=1}^{k} x_i\right) = g\left(\sum_{i=1}^{k} \tilde{x_i}\right)$.
						
						\item Par application de $g^{-1} ~\textblue{\left(\in GL_n(\F_q)\right)}$, $\sum_{i=1}^{k} x_i = \sum_{i=1}^{k} \tilde{x_i}$.
						
						\item Comme les $E_i$ sont en somme directe, $\forall i, x_i = \tilde{x_i}$. Donc les $g(E_i)$ sont en somme directe.
					\end{itemize}
				\end{itemize}
			
			\subparagraph{Étape b: l'action est transitive} : $\forall x, y \in \mathcal{E}_k, \exists ! g \in GL_n(\F_q)$ tel que $g.x = y$.
				\begin{itemize}
					\item Pour tout $k$-uplet $\left(E_1, \dots, E_k\right) \in \mathcal{E}_k$, on construit une base de $\F_q^n$ en compilant les bases de $E_i$, $1 \leq i \leq k$ \textblue{(caractérisation d'une somme directe)}.
					
					\item Pour deux $k$-uplets $\left(E_i\right)_i$ et $\left(E'_i\right)_i$ de $\mathcal{E}_k$, on construit deux bases $e$ et $e'$.
					
					\item $g \in GL_n(\F_q)$ qui envoie $e$ sur $e'$ \textblue{(existe par changement de bases)}, envoie $E_i$ sur $E'_i$ \textblue{(construction des bases)}.
				\end{itemize}
				
			\subparagraph{Étape c: étude du cardinal} Montrons que $\left|\mathcal{E}_k\right| = \frac{\left|GL_n(\F_q)\right|}{\left|GL_{n_1}(\F_q)\right| \cdots \left|GL_{n_{q-1}}(\F_q)\right|}$.
				\begin{itemize}
					\item $Stab_{(E_i)}$ est le sous-groupe de $GL_n(\F_q)$ qui stabilise tous les $E_i$ \textblue{(par définition de $Stab_{(E_i)}$)}.
					\item On applique la formule des classes au sous-groupe $\prod_{i}GL(E_i)$ (diagonal par bloc) = $\prod_{i}GL_{n_i}(\F_q)$.
				\end{itemize}
		\end{proof}
		
		\paragraph{Étape 2: caractérisation des automorphismes diagonalisables.}
		\begin{lem}[\protect{\cite[p.176]{Gourdon-algebre}}]
			Une matrice $A \in \mathcal{M}_n(\F_q)$ est diagonalisable si et seulement si $A^q = A$.
			\label{lem:gourdon}
		\end{lem}
		\begin{proof}
			\begin{description}
				\item[$\Rightarrow$] $A$ diagonalisable : 
				$A = P^{-1} \left[\begin{array}{ccc}
				\lambda_1 & & \\
				& \ddots & \\
				& & \lambda_n \\
				\end{array}\right] P$ avec $P \in GL_n(\F_q)$ et $\left(\lambda_i\right)_{1\leq i \leq n} \in \F_q$.
				
				$A^q = P^{-1} \left[\begin{array}{ccc}
					\lambda_1^q & & \\
					& \ddots & \\
					& & \lambda_n^q  \\
				\end{array}\right] P$ \textblue{(puissance)} $= P^{-1} \left[\begin{array}{ccc}
					\lambda_1 & & \\
					& \ddots & \\
					& & \lambda_n  \\
				\end{array}\right] P$ \textblue{($\F_q^{\times}$ est cyclique: $x^q = x$)} $= A$.
				
				\item[$\Leftarrow$] $X^q - X$ est un polynôme annulateur de $A$ \textblue{(par hypothèse)}. Soit $\xi$ un générateur de $\F_q^{\times}$ \textblue{(qui est cyclique)}, alors $X^q - X = X(X^{q-1} -1) = X\prod_{i=1}^{q-1}\left(X- \xi^i\right)$. Le polynôme annulateur est alors scindé donc $A$ est diagonalisable.
			\end{description}
		\end{proof}
		
		\begin{cor}
			Une matrice $A \in GL_n(\F_q)$ est diagonalisable si et seulement si $A^{q-1} - I_n = 0$.
		\end{cor}
		\begin{proof}
			Soit $A \in GL_n(\F_q)$.
			
			\begin{tabular}{ccl}
				$A$ est diagonalisable & si et seulement si & $A^q - A = 0$ \textblue{(lemme~\ref{lem:gourdon})} \\
				& si et seulement si & $A^{q-1} - I_n = 0$ \textblue{($A \in GL_n(\F_q)$ donc $A$ est inversible)}
			\end{tabular}
		\end{proof}
		
		\paragraph{Étape 3: preuve du théorème.}
		Montrons maintenant le théorème. On ordonne de $1$ à $q$ les éléments de $\F_q = \{\xi^i, 1 \leq i \leq q\}$. Pour toute famille d'entiers positifs $\left(n_i\right)_{1\leq i \leq q}$ telle que $\sum n_i = n$, on note $\mathcal{E}_{q, (n_i)}$ l'ensemble $\mathcal{E}_k$ du lemme~\ref{lem:sousCard}. Construisons une bijection entre l'ensemble des matrices diagonalisables et la réunion sur l'ensemble des $(n_i)$ tel que $\sum n_i = n$ des $\mathcal{E}_{q, (n_i)}$.
		\begin{itemize}[label=$\to$]
			\item Soit $A$ une matrice diagonalisable de $\mathcal{M}_n(\F_q)$.
			\begin{itemize}
				\item On assigne à la matrice diagonalisable $A$ la famille des sous-espaces $\left(E_i\right)_{1 \leq i \leq q} = E_A$ où $E_i$ est le sous-espace propre de $A$ pour la valeur propre $\xi_i \in \F_q$ ou $E_i = 0$ sinon.
				
				\item $A$ est diagonalisable : $A^q - A =0$ \textblue{(par le lemme~\ref{lem:gourdon})}. De plus, $X^{q-1} - 1 = \prod_{i=1}^{q-1} (X - \xi^i)$. On en déduit que $\F_q^n = \bigoplus_{i=1}^{q-1} \left(A - \xi^iI_n\right)$ avec $E_i = \left(A - \xi^iI_n\right)$. Donc $\left(E_i\right)_i \in \mathcal{E}_{q, \dim E_i}$.
			\end{itemize}
			
			\item Soit $(n_i)$ tel que $\sum_{i = 1}^{q} n_i =n$. Pour tout $\left(E_i\right)_i$ de $\mathcal{E}_{q, n_i}$, on fait correspondre $A_{(E_i)}$ de l'endomorphisme $\varphi$ de $\F_q^n$ tel que $\varphi(z_i) = \xi_iz_i$.
		\end{itemize}
		D'où la bijection \textblue{($A \mapsto E_A$ et $(E_i) \mapsto A_{(E_i)}$)}. On en déduit le résultat en appliquant le lemme~\ref{lem:sousCard} à l'union.
	\end{proof}
	
	\section{Quelques notions utiles}
	
	\subsection*{Action de groupe}
	\input{./../Notions/ActionsGroupes.tex}
	
	\subsection*{Dénombrement sur des ensembles de matrices}
	\input{./../Notions/Denombrement_CorpsFini_matrice.tex}
	
	\subsection*{Somme directe}
	\input{./../Notions/AlgebreLineaire_SommeDirecte.tex}
	
	\subsection*{Valeurs propres, espaces propres}
	\input{./../Notions/AlgebreLineaire_ValEspPropre.tex}
	
	\subsection*{Diagonalisation}
	\input{./../Notions/AlgebreLineaire_Diagonalisation.tex}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}