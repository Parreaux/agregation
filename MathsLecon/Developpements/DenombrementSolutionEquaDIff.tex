\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\Cun}{\mathcal{C}^1}
	\newcommand{\D}{\mathcal{D}}
	\newcommand{\A}{\mathfrak{A}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{OliveGreen}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	\newcommand{\textmagenta}[1]{\textcolor{DarkOrchid}{#1}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{lemme}{Lemme}
	\newtheorem{lnum}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	
	\title{Dénombrement du nombre de zéros des solutions d'une équation différentielle.}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Zully--Queffelec \cite[p.404]{ZuilyQueffelec2}.
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 220 (Équations différentielles non-linéaires); 221 (Équations différentielles linéaires); 224 (Développement asymptotique).
			
			\textblue{\emph{Leçons où on peut l'évoquer:}} 190 (Dénombrement).
		}}
	
	\section{Introduction}
	
	Résoudre des équations différentielles même linéaire peut être délicat car nous devons calculer une exponentielle de matrice (ce qui nous complique beaucoup la tâche). Lorsque le calcul de cette solution n'est pas réalisable dans de bonne condition, on est amené à se poser des questions de nature quantitative sur les solutions de l'équation que l'on considère. On peut alors se demander si les solutions admettent des zéros et combien, sont-elle développable en série entière ou encore si celles-ci sont stable. 
	
	L'objectif de ce développement est de dénombrer les zéros des solutions d'une équation différentielle qui n'est en apparence pas très désagréable. Les deux outils essentiels de notre développement est le passage en coordonnées polaires et le principe d'entrelacement de Sturm.
	
	\section{Dénombrer le nombre de solution d'une équation différentielle}
	
	\begin{theo}
		Soient $a \in \R$ et $q \in \Cun\left([a, +\infty]\right)$, $q> 0$ tels que 
		\begin{displaymath}
		\begin{array}{cccc}
		\int_{a}^{+\infty} \sqrt{q(u)}du = + \infty  & \text{ et } & q'(x) = o(q^{3/2}(x)) & \text{ quand } x \to +\infty \\
		\end{array}
		\end{displaymath}
		Soit $y$ une solution réelle non nulle de $y''+qy =0$ sur $[a, +\infty[$ et soit $N(x)$ le nombre de zéros de $y$ sur $[a, x]$. Alors
		\begin{displaymath}
		\begin{array}{ccc}
		N(x) \sim \frac{1}{\pi} \int_{a}^{x} \sqrt{q(u)}du & \text{quand} & x \to +\infty
		\end{array}
		\end{displaymath}
	\end{theo}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement}}}{
		\begin{enumerate}
			\item Changement de temps
			\item Changement en coordonnées polaires via le lemme de relèvement.
			\item Recherche d'un équivalent pour $\theta$ en $+ \infty$.
			\item Estimation du nombre de zéros de $Y$.
			\item Estimation du nombre de zéros de $y$.
		\end{enumerate}
	}
	
	\begin{proof}
		Pour cette preuve nous allons avoir besoin du lemme de relèvement (lemme~\ref{lem:relevement}) que nous allons énoncé et démonter plus loin (dont la démonstration est elle-même à connaître).
		\paragraph{Étape 1 : changement de temps} On effectue un changement du temps dans l'équation différentielle en posant $\tau(x) = \int_{a}^{x} \sqrt{q(u)}du$.
		
		Comme $q$ est de classe $\Cun$ et est strictement positive, $\sqrt{q}$ existe et est strictement positive. Donc $\tau$ est une bijection croissante \textblue{(car $\tau'=\sqrt{(q)} >0$, comme $q >0$) } de classe $\Cun$ \textblue{(car $\tau'=\sqrt{(q)}$ est continue puisque $q$ est continue)} de $[a, \infty[$ sur $[0, \infty[$. 
		Alors $\tau^{-1}$ est également une bijection croissante de classe $\Cun$ de $[0, \infty[$ sur $[a, \infty[$ 
		\textblue{(car $f \circ f^{-1}(x)= x$ implique que $\left(f^{-1}\right)^{'} = \frac{1}{f' \circ f^{-1}}$)}.
		
		On pose $Y = y \circ \tau^{-1}$, soit $y(x)= Y(\tau(x))$. Soit $t = \tau(x)$ la nouvelle variable de notre équation. On a alors
		\begin{displaymath}
		y' \underbrace{=}_{\text{dérivée d'une composition}} \tau'(x)Y'\left(\tau(x)\right) \underbrace{=}_{\text{remplacement de } \tau'} \sqrt{q(x)}Y'\left(\tau(x)\right)
		\end{displaymath}
		\begin{displaymath}
		y'' \underbrace{=}_{\text{dérivée d'un produit}} \frac{q'(x)}{2\sqrt{q(x)}}Y'\left(\tau(x)\right) + q(x)Y''\left(\tau(x)\right)
		\end{displaymath}
		d'où l'équation $y''+qy$ devient $q(x)Y''\left(\tau(x)\right) + \frac{q'(x)}{2\sqrt{q(x)}}Y'\left(\tau(x)\right) + q(x)Y\left(\tau(x)\right)$. En posant, $\varphi(t)= \frac{q'(x)}{2q(x)^{\frac{3}{2}}}$ pour $y = \tau(x)$, on obtient cette nouvelle équation: $Y''(t) + \phi(t)Y'(t)+Y(t) =0$ (\textblue{en divisant par $q(x) > 0$}).
		
		\begin{req}
			Nous avons modifier en profondeur l'équation, il est alors légitime de se demandé si ces modifications sont utiles et quelles sont les désavantages liés à celle-ci. Cette transformation nous est gagnante car $Y$ est maintenant précédée par une constante $1$. Par contre, cette transformation nous amène un terme $\varphi Y'$ qui n'existait pas avant. On remarque que $\lim_{t \to \infty}\varphi(t) =0$, donc tout est comme si l'équation est $Y'' + Y =0$.
		\end{req}
		
		\paragraph{Étape 2: changement en cordonnées polaires via le lemme de relèvement} On souhaite appliquer le lemme de relèvement (lemme~\ref{lem:relevement}) à $Y$ et $Y'$ de classe $\Cun\left(\left[0, +\infty\right], \R\right)$.
		
		Montrons que $Y$ et $Y'$ n'ont pas de zéros en commun. En effet, supposons que $Y(t_0) = Y'(t_0) =0$, on peut alors réécrire le problème de Cauchy est alors
		\begin{displaymath}
		\left \{
		\begin{array}{r @{=} c}
		Y''(t) + \varphi(t)Y'(t)+Y(t) & 0 \\
		Y(t_0) = Y'(t_0) & 0 \\
		\end{array}
		\right .
		\end{displaymath}
		Par le théorème de Cauchy--Lipschitz \textblue{(unicité des solutions)} nous assure que $Y =0$. Cela implique par définition de $Y$ que $y = 0$, ce qui est absurde.
		
		On va alors appliquer le lemme~\ref{lem:relevement}, et on écrit
		\begin{displaymath}
		\left \{
		\begin{array}{c @{=} l}
		Y & r \sin(\theta) \\
		Y'& r \cos(\theta) \\
		\end{array}
		\right .
		\end{displaymath}
		avec $r, \theta \in \Cun\left(\left[a, \infty\right], \R\right)$. 
		
		En dérivant, on obtient:
		\begin{displaymath}
		Y' \underbrace{=}_{\text{dérivée}} r'\sin \theta + r\theta' \cos \theta \underbrace{=}_{\text{lemme~\ref{lem:relevement}}} r\cos \theta
		\end{displaymath}
		\begin{displaymath}
		Y'' \underbrace{=}_{\text{dérivée de } r\cos\theta} r'\cos \theta - r\theta' \sin \theta \underbrace{=}_{\text{équation}} -\varphi r \cos \theta - r\sin \theta
		\end{displaymath} 
		
		\paragraph{Étape 3: étude de $\theta$ (recherche d'un équivalent)} On obtient alors
		
		\begin{tabular}{ccl}
		$\cos \theta Y' - \sin \theta Y''$ &$=$& $\underbrace{r \theta'}_{\begin{array}{c}
			\left(r'\sin \theta + r\theta' \cos \theta\right)\cos \theta - \left(r'\cos \theta - r\theta' \sin \theta\right)\sin \theta \\
			r \theta' \underbrace{\left(\sin^2 \theta + \cos^2 \theta\right)}_{1}\\
			\end{array}}$\\ 
		$\cos \theta Y' - \sin \theta Y''$ & $=$ & $\underbrace{r + \varphi r \sin \theta \cos \theta}_{\begin{array}{c}
			\left(r\cos \theta\right)\cos \theta - \left(-\varphi r \cos \theta - r\sin \theta\right)\sin \theta \\
			r \underbrace{\left(\sin^2 \theta + \cos^2 \theta\right)}_{1} + \varphi r \sin \theta \cos \theta \\
			\end{array}}$ \\
		\end{tabular}
		
		D'où en divisant par $r$ \textblue{($r$ qui est non nul par définition)}, $\theta' = 1 + \varphi \sin \theta \cos\theta$. Comme $\left|\sin \theta\cos \theta\right| \leq \frac{1}{2}$ (\textblue{car $\sin \theta \cos \theta < \frac{1}{2}\sin (2\theta)$}), on a $\left|\theta'(t) - 1\right| \leq \frac{1}{2} \left|\varphi(t)\right|$. Or $\lim_{t \to \infty}\varphi(t) = 0$, on en déduit que $\lim_{t \to \infty} \theta'(t) =1$, soit $\theta(t) \sim_{t \to \infty} t$.
		
		\paragraph{Étape 4: estimation du nombre de zéros de $Y$.} Montrons que $M(t) \sim_{t \to \infty} \frac{t}{\pi}$ où $M(t)$ est le nombre de zéros de $Y$ sur $[0,t]$.
		
		Soit $t_0 > 0$ tel que $\forall t \geq t_0$, $\theta'(t) > 0$ \textblue{(possible car $\lim_{t \to \infty}\theta'(t) = 1$)}.
		
		On a:
		
		\begin{tabular}{ccll}
			$M(t)$ & $=$ & $\# \{u \in [0, t], Y(u) = 0\}$ & \textblue{(définition de $M(t)$)} \\
			& $=$ & $\# \{u \in [0, t], r(u)\sin\left(\theta(u)\right) = 0\}$ & \textblue{(définition de $Y$)} \\
			& $=$ & $\# \{u \in [0, t], \sin\left(\theta(u)\right)  = 0\}$ & \textblue{($Y'(u) > 0 \Rightarrow r(u)\sin\left(\theta(u)\right) > 0 \Rightarrow r(u)>0$)} \\
			& $\sim$ & $\# \{u \in [t_0, t], \sin\left(\theta(u)\right)  = 0\}$ & \textblue{(manque les zéros entre $0$ et $t_0$ (reste négligeable))} \\
			& $=$ & $\# \{v \in [\theta(t_0), \theta(t)], \sin\left(v\right)  = 0\}$ & \textblue{(changement de variables: $v =\theta(u)$)} \\
			& $=$ & $\# \{k \in \Z, \theta(t_0) \leq k\pi \leq \theta(t)\}$ & \textblue{($\sin(k\pi) = 0 \forall k \in \Z$)} \\
			& $\sim$ & $\frac{\theta(t)}{\pi}$ & \textblue{(nombre de $k\pi \leq \theta(t)$ est environ égal à $\frac{\theta(t)}{\pi}$)} \\
			& $\sim$ & $\frac{t}{\pi}$ ~~~~ quand $t \to \infty$ & \textblue{(hypothèse $\theta(t) \sim t$)} \\
		\end{tabular}
		
		\paragraph{Étape 5: estimation du nombre de zéros de $y$.} Montrons maintenant que $N(x) = M(\tau(x))$ où on rappelle que $N(x)$ est le nombre de zéros de $y$ sur $[0, t]$. On a:
		
		\begin{tabular}{ccll}
			$M(\tau(x))$ & $=$ &$\# \{t \in [0, \tau(x)] Y(t) = 0\}$ & \textblue{(définition de $M$)} \\
			& $=$ &$\# \{s \in [a, x] Y(\tau(s)) = 0\}$ & \textblue{(changement de variables $t = \tau(s)$)} \\
			& $=$ &$\# \{s \in [a, \tau(x)] y(s) = 0\}$ & \textblue{($y(s) = Y(\tau(s))$)} \\
			& $=$ &$N(s)$ & \textblue{(définition de $N$)} \\
		\end{tabular}
		
		D'où, par ce qui précède, $N(x) \sim \frac{\tau(x)}{\pi}$ quand $x$ tend vers $+\infty$. Ce qui conclut la preuve avec la définition de $\tau$.
	\end{proof}
	
	\paragraph{Exemples et contre-exemples d'application du théorème}
	
	Ce résultat peut s'appliquer "facilement" pour $Id$ et $t^2$.
	
	Contre-exemple: $q(x) = \frac{1}{4x^2}$, $q'(x) = \frac{1}{2x^3}$ et $y'' = \frac{1}{4x^2}y = 0$.
	
	\section{Compléments autours des équations différentielles linéaires}	
	
	\subsection*{Le lemme de relèvement}
	
	\begin{lnum}[Lemme de relèvement]
		Soit $y_1$, $y_2 \in \Cun\left(\left[0, \infty\right], \R\right)$ sans zéro commun et soit $w = y_1y'_2 -y'_1y_2$. Si $y_1(a)+iy_2(a)=re^{i\theta_0}$, alors on peut écrire
		\begin{displaymath}
		\left \{
		\begin{array}{c @{=} l}
		y_1 & r \cos\left(\theta\right) \\
		y_2 & r \sin\left(t\Theta\right) \\
		\end{array}
		\right .
		\end{displaymath}
		où $r, \theta \in \Cun\left(\left[a, \infty\right], \R\right)$, et sont données par les formules
		\begin{displaymath}
		\left \{
		\begin{array}{c @{=} l}
		r & \sqrt{y_1^2 +y_2^2} \\
		\theta(x) & \theta_0 + \int_{a}^{x} \frac{w(t)}{r^2(t)}dt\\
		\end{array}
		\right .
		\end{displaymath}
		\label{lem:relevement}
	\end{lnum}
	\begin{proof}
		Posons $\phi = y_1 + iy_2 \neq 0$ et $\psi(x) = \int_{a}^{x} \frac{\phi'(t)}{\varphi(t)}dt + \mathrm{Log~} r_0 +i\theta_0$. De plus, $\left(\varphi e^{-\psi}\right)' = \left(\varphi' - \psi'\varphi\right)e^{-\psi} =0$. Donc $\varphi(x)e^{-\psi(x)} = \varphi(a)e^{-\psi(a)} = r_0e^{i\theta_0}r_{0}^{-1}e^{-i\theta_0} =1$. Donc $y_1 + iy_2 =e^{\varphi} =re^{\theta}$, où $r = \sqrt{y_1^2 +y_2^2}$ et $\theta = \Im \psi$. Or,
		
		\begin{tabular}{ccll}
			$\psi(x)$ & $=$ & $\int_{a}^{x} \frac{\phi'(t)}{\varphi(t)}dt + \mathrm{Log~} r_0 +i\theta_0$& \\
			& $=$ & $\int_{a}^{x} \frac{y_1'(t)+iy_2'(t)}{y_1(t)+iy_2(t)}dt + \mathrm{Log~} r_0 +i\theta_0$& \\
			& $=$ & $\int_{a}^{x} \frac{\left(y_1'(t)+iy_2'(t)\right)\left(y_1(t)+iy_2(t)\right)}{r^2}dt + \mathrm{Log~} r_0 +i\theta_0$& \\
		\end{tabular}
		D'où,
		\begin{displaymath}
		\theta(x) = \theta_0 + \int_{a}^{x} \frac{\left(y_1'(t)+iy_2'(t)\right)\left(y_1(t)+iy_2(t)\right)}{r^2}dt = \theta_0 + \int_{a}^{x} \frac{w(t)}{r^2(t)}dt
		\end{displaymath}
	\end{proof}
	
	\subsection*{Théorème de Cauchy--Lipschitz} Il est utilisé pour vérifier qu'on peut appliqué le lemme de relèvement (lemme~\ref{lem:relevement}). On rappelle ici le résultat.
	\input{./../Notions/EquaDiff_CauchyLipschitz.tex}
	
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}