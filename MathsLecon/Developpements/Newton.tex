\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{multirow}
\usepackage{tikz}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}


\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\Cun}{\mathcal{C}^1}
	\newcommand{\Cdeux}{\mathcal{C}^2}
	\newcommand{\Cinf}{\mathcal{C}^{\infty}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{OliveGreen}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	\newcommand{\textmagenta}[1]{\textcolor{DarkOrchid}{#1}}
	
	\newcommand{\pcite}[1]{\protect{\cite{#1}}}
	\newcommand{\Attention}{\textred{\textbf{Attention}}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\theoremstyle{plain}
	\newtheorem{theo}{Théorème}
	\newtheorem{prop}{Proposition}
	\newtheorem{cor}{Corollaire}
	\newtheorem{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem{definition}{Définition}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	\newtheorem*{cex}{Contre-exemple}
	
	\title{Méthode de Newton}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Rouvière \cite[p.140]{Rouviere}.
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 223 (Suite numérique); 224 (Développement asymptotique); 226 (Suite récurrente); 228 (Continuité et dérivabilité).
			
			\textblue{\emph{Leçons où il peut être évoqué:}}  229 (Fonctions monotones, convexes).
		}}

	\section{Introduction}
	
	La méthode de Newton est une méthode numérique itérative qui grâce à une suite récurrente résout l'équation $f(x) =0$ lorsque la fonction $f$ possède de bonnes propriétés. Elle a également l'avantage d'avoir une interprétation géométrique intuitive et intéressante. L'idée principal de cet algorithme (outre d'utilisé une méthode itérative), est de se ramener à une problème équivalent de point fixe $F(x) = x$ pour lesquels on a de jolis théorèmes qui peuvent s'appliquer (théorème de Picard par exemple). Elle permet de bien montrer l'existence d'une telle solution (elle ne se limite pas à montrer que $f$ à un zéro) en exhibant cette solution. Un énoncé plus général que celui que l'on prouve permet de montrer l'existence de cette solution.
	
	\begin{theo}[Méthode de Newton générale \protect{\cite[p.59]{Dieudonne}}]
		Soit $x_0 \in I$, où $I$ est un intervalle de $\R$, supposons qu'il existe deux nombres $c \geq 0$ et $\lambda > 0$ tel que 
		\begin{enumerate}
			\item $\left|f\left(x_0\right)\right| \leq \frac{c}{2\lambda}$;
			\item $\forall x, z \in \left[x_0 - c, x_0 + c\right] \subseteq I$, on a 
			\begin{enumerate}
				\item $\left|f'(x)\right| \geq \frac{1}{\lambda}$;
				\item $\left|f'(x) - f(x)\right| \geq \frac{1}{2\lambda}$.
			\end{enumerate}
		\end{enumerate}
		Dans ces conditions, il existe une unique solution $\alpha$ de l'équation $f(x) = 0$, $\alpha \in \left[x_0 - c, x_0 + c\right]$. En outre, si $\left(z_n\right)_{n \in \N}$ est une suite quelconque de points de $\left[x_0 - c, x_0 + c\right]$, on peut définir une suite $\left(x_n\right)_{n \in \N}$ de points de cet intervalle telle que
		\begin{displaymath}
		x_{n+1} = x_n - \frac{f\left(x_n\right)}{f\left(z_n\right)}
		\end{displaymath}
		et la limite de cette suite est $\alpha$.
	\end{theo}
	
	Cette méthode a de nombreuses applications que ce soit en mathématiques ou en informatique. En effet, à chaque étape elle est d'une complexité raisonnable (si la complexité de $f$ et de sa dérivée l'est aussi) car on ne fait "qu'une division". De plus, à l'aide du Jacobien, nous pouvons étendre facilement la méthode à Newton à $\R^d$ même si on en parle pas ici. Grâce à sa convergence quadratique et sa complexité raisonnable, la méthode de Newton reste une des méthodes les plus utilisée.
	
	Le choix de $F$ est un choix important pour garantir cette vitesse de convergence. Cette transformation peut se faire de bien des manières. Généralement, on pose $F(x) = x + \lambda(x)f(x)$ où $\lambda$ est une fonction qui ne s'annule pas. De plus, la vitesse de convergence de la suite $F(x_{n}) = x_{n+1}$ que l'on va considéré pour résoudre le système est bien plus importante si $a$ (la solution de l'équation) est un point fixe superattractif. Dans le cadre de notre problème, on est amenée à choisir, $\lambda(x) = -\frac{1}{f'(x)}$ (d'où la suite récurrente).
	
	
	\section{Méthode de Newton}
	
	On donne la version du théorème que l'on souhaite montrer. Dans notre version, l'existence d'une solution est prouvée mais sous des hypothèse bien plus forte que dans le cas de la version générale (un TVI est appliqué). 
	
	\begin{theo}[Méthode de Newton développée]
		Soit $f : [c, d] \to \R$ une fonction de classe $\Cdeux$. On suppose $c < d$ et $f(c) < 0 < f(d)$ et $\forall x \in [c, d], f'(x)>0$. On considère la suite récurrente suivante:
		\begin{displaymath}
		\begin{array}{ccc}
		x_{n+1} =F(x_n), n\geq 0 & \text{ avec } & F(x) = x - \frac{f(x)}{f'(x)}
		\end{array}
		\end{displaymath}
		Alors,
		\begin{enumerate}
			\item $f$ a un unique zéro en $a$.
			\item $\forall x \in [c, d]$, $\exists \alpha >0 $ tel que la suite $\left(x_n\right)_{n \in \N}$ a une convergence quadratique vers $a$ dans $I = [a -\alpha, a + \alpha]$.
			\item De plus, si $f'' > 0$, on a l'équivalent suivant:
			\begin{displaymath}
			x_{n+1} -a \sim \frac{1}{2}\frac{f''(a)}{f'(a)}\left(x_n-a\right)^2
			\end{displaymath}
			\textblue{(L'ajout de l'hypothèse de convexité à la méthode de Newton, nous permet de nous affranchir de l'intervalle $I$ tout en gardant la même vitesse de convergence.)}
		\end{enumerate}
	\end{theo}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement}}}{
		\begin{enumerate}
			\item $f$ admet un unique zéro en $a$.
			\item $\forall x \in [c, d], \exists z \in [a, x]$ tel que $F(x) -a = \frac{f''(z)}{f'(x)}(x-a)^2$.
			\item Il existe $C > 0$ tel que $\forall x \in [a, d]$, $\left|F(x) - a\right| \leq C\left|x - a\right|^{2}$.
			\item $\forall x \in [c, d]$, $\exists \alpha > 0$ tel que la suite $\left(x_n\right)_{n \in \N}$ a une convergence quadratique vers $a$ dans $I$.
			\item On suppose que $f'' > 0$. On a l'équivalent suivant : $x_{n+1} -a \underset{n \to +\infty}{\sim} \frac{1}{2}\frac{f''(a)}{f'(a)}\left(x_n-a\right)^2$.
		\end{enumerate}
	}

	\begin{proof} 
		Soit $f : [c, d] \to \R$ une fonction de classe $\Cdeux$. On suppose $c < d$ et $f(c) < 0 < f(d)$ et $\forall x \in [c, d], f'(x)>0$. On considère la suite récurrente suivante:
		\begin{displaymath}
		\begin{array}{ccc}
		x_{n+1} =F(x_n), n\geq 0 & \text{ avec } & F(x) = x - \frac{f(x)}{f'(x)}
		\end{array}
		\end{displaymath}
		On considère son interprétation géométrique qui est somme toute très intuitive dont voici un exemple (Figure~\ref{fig:Newton}).
		
		\begin{figure}
			\centering
			\includegraphics[scale=1]{./../Figures/MethodeNewton.png}
			\caption{Illustration géométrique de la méthode de Newton sur une fonction convexe.}
			\label{fig:Newton}
		\end{figure}
		
		\paragraph{Étape 1: Montrons que $f$ admet un unique zéro en $a$.}
		
		Comme $f$ est continue sur $[c, d]$ \textblue{(par hypothèse)} et croit strictement \textblue{($f'(x) > 0$)} de $f(c) <0$ à $f(d) > 0$ \textblue{(par hypothèse)}, elle s'annule en un unique point $a \in ]c, d[$ \textblue{(par le TVI (assure l'existence) et la stricte croissance (assure l'unicité)}.
		
		\paragraph{Étape 2: Montrons que $\forall x \in [c, d]$, $\exists z \in [a, x]$ tel que $F(x) - a = \frac{f''(z)}{f'(x)}(x-a)^2$.}
		
		Par calcul direct, on a 
		\begin{displaymath}
		F(a) \underbrace{=}_{f'(a)> 0 \text{ et définition}} a - \frac{f(a)}{f'(a)} \underbrace{=}_{f(a) =0 \text{, par hypothèse}} a
		\end{displaymath}
		et
		\begin{displaymath}
		F'(a) \underbrace{=}_{f'(a)> 0\text{ et déivée}} 1 - \frac{\left(f'(a)\right)^2- f''(a)f(a)}{\left(f'(a)\right)^2} \underbrace{=}_{f(a) =0 \text{, par hypothèse}} 1 - \frac{\left(f'(a)\right)^2}{\left(f'(a)\right)^2} = 0
		\end{displaymath}
		d'où
		\begin{displaymath}
		F(x)-a \underbrace{=}_{\text{définition}} x - a - \frac{f(x)}{f'(x)} \underbrace{=}_{f(a) =0} x - a - \frac{f(x)-f(a)}{f'(x)} \underbrace{=}_{\text{même dénominateur}}  \frac{f(a)-f(x) -(a -x)f'(x)}{f'(x)}
		\end{displaymath}
		
		On applique alors la formule de Taylor--Lagrange à l'ordre deux d'origine $x$ et d'extrémité $a$, alors il existe $z \in ]a, x[$ tel que 
		\begin{displaymath}
		F(x)-a= \frac{1}{2}(a-x)^2\frac{f''(z)}{f'(x)}
		\end{displaymath}
		
		\paragraph{Étape 3: Montrons qu'il existe $C>0$ tel que $\left|F(x)-a\right|\leq C\left|x-a\right|^2$, $\forall x \in [a, d]$ et qu'il existe $\alpha > 0$ tel que $I =[a- \alpha, a +\alpha]$ soit stable par $F$.}
		
		On pose $C = \frac{\max_{x \in [c, d]} \left|f''(x)\right|}{2\min_{x \in [c, d]}f'(x)}$. Par ce qui précède (de l'étape 2), on en déduit que
		
		\begin{tabular}{ccll}
			$\left|F(x)-a\right|$ & $=$ & $\left|\frac{1}{2}\frac{f''(z)}{f'(x)}(a-x)^2\right|$ & \textblue{(par l'étape 2)} \\
			& $=$ & $\left|\frac{1}{2}\frac{f''(z)}{f'(x)}\right|\left|a-x\right|^2$ & \textblue{(par homogénéité de la valeur absolue)} \\
			& \multirow{2}{*}{$\leq$} & \multirow{2}{*}{$\frac{1}{2}\frac{\max_{x \in [c, d]}\left|f''(x)\right|}{\min_{x \in [c, d]}f'(x)}(a-x)^2$} & \textblue{($\left|f''(z)\right| \leq \max_{x \in [c, d]}\left|f''(x)\right|$ et $\max_{x \in [c, d]}\left|f'(x)\right|> 0$)} \\
			& & & \textblue{($\left|f'(x)\right| \geq \min_{x \in [c, d]}f'(x) > 0$)} \\
			& $\leq$ & $C\left|a-x\right|^2$ & \textblue{(définition de $C$)}
		\end{tabular}
		
		Soit $\alpha > 0$ tel que $C\alpha <1$ et assez petit pour que $I = [a - \alpha, a + \alpha] \subset [c, d]$. Soit $x \in I$, alors on a:
		
		\begin{tabular}{ccll}
			$\left|F(x) - a\right|$ & $\leq$ & $C\left|x-a\right|^2$ & \textblue{(par l'inégalité précédent)} \\
			& $\leq$ & $C\alpha^2$ & \textblue{($x \in I \Rightarrow \left|x-a\right| \leq \alpha$)} \\
			& $<$ & $\alpha$ & \textblue{($C\alpha < 1$, par hypothèse)} \\
		\end{tabular}
		
		Donc, par définition de $I$, $F(I) \subset I$.
		
		\paragraph{Étape 4: Montrons que $\forall x \in [c, d]$, $\exists \alpha >0 $ tel que la suite $\left(x_n\right)_{n \in \N}$ a une convergence quadratique vers $a$ dans $I$.}
		Par la précédente étape, on a $F(I) \subset I$. On a donc si $x_0 \in I$, alors $\forall n, x_n \in I$. On va alors en déduire la convergence. 
		Par récurrence sur $n \in \N$, on montre $C \left|x_n - a\right| \leq \left(C\left|x_0 - a\right|\right)^{2^n}$ (\textblue{en effet, l'étape de récurrence s'écrit comme suit
			\begin{displaymath}
			\begin{array}{ccll}
			C\left|x_{n+1}\right| & = & C \left|F\left(x_n\right)-a\right| & \text{égalité de l'étape 3} \\
			& \leq & C\left(C\left|x_n -a\right|^2\right) \\
			& = & \left(C\left|x_n -a\right|\right)^2 & \text{inégalité de l'étape 3} \\
			& \leq & \left(C\left|x_0 - a\right|\right)^{2^{n+1}} & \text{hypothèse de récurrence} \\
			\end{array}
			\end{displaymath}
			L'initialisation se fait trivialement d'où la récurrence}).
		On en déduit alors,
		\begin{displaymath}
		C\left|x_n - a\right| \underbrace{\leq}_{\text{récurrence}} \left(C\underbrace{\left|x_0 - a\right|}_{\leq \alpha}\right)^{2^n} \leq \left(C\underbrace{\alpha}_{\leq 1}\right)^{2^n}
		\end{displaymath}
		Comme $C\alpha < 1$, la convergence de $\left(x_n\right)_{n \in \N}$ est quadratique, si $x_0 \in I$.		
		
		\paragraph{Étape 5: On suppose de plus que $f''>0$. Montrons qu'on a l'équivalent suivant: $x_{n+1} -a \sim \frac{1}{2}\frac{f''(a)}{f'(a)}\left(x_n-a\right)^2$ si $n \to + \infty$.} Comme $f''(x) > 0$, $f'$ est croissante et $f$ est convexe sur $]c, d[$.
		
		Soit $a \leq x \leq d$. On a $f(x) \leq 0$ \textblue{(par hypothèse sur $a$ et par croissance stricte de $f$)} et $f'(x) > 0$ \textblue{(par hypothèse sur $f'$)}. On en déduit que 
		\begin{displaymath}
		F(x) \underbrace{=}_{\text{Définition de }F} x - \frac{f(x)}{f'(x)} \underbrace{\leq}_{\frac{f(x)}{f'(x)} \leq 0} x
		\end{displaymath}
		
		On a de plus,
		\begin{displaymath}
		F(x) - a \underbrace{=}_{\text{égalité de l'étape 2}} \frac{1}{2}\frac{\overbrace{f''(z)}^{>0}}{\underbrace{f'(z)}_{>0}}\underbrace{\left(x-a\right)^2}_{>0} \geq 0
		\end{displaymath}
		
		Comme $I$ est stable par $F$ \textblue{(par l'étape 4)}, si $a < x_0 < d$ alors $a < x_n < d$. Donc la suite $\left(x_n\right)_{n\in \N}$ est strictement décroissante. Dans le cas $x_0 = a$, la suite $\left(x_n\right)_{n \in \N}$ est constante. Donc, dans tous les cas, la suite $\left(x_n\right)_{n \in \N}$ admet une limite $l$ telle que $F(l) =l$, c'est-à-dire $f(l) =0$, donc $l=a$.
		
		La convergence vers $a$ est quadratique et on a $0 leq x_{n+1} - a \leq C\left(x_n-a\right)^{2}$ d'après l'étape 3. Enfin, cette inégalité est essentiellement optimale: si $a < x_0 \leq d$, on a $x_n > 0$ pour tout $n$, on peut donc diviser l'expression de $x_{n+1} -a$ (de l'étape 3) par $\left(x_n - a\right)^2$ qui est toujours non nul, 
		\begin{displaymath}
		\frac{x_{n+1} -a}{\left(x_n - a\right)^2} = \frac{1}{2}\frac{f''\left(z_n\right)}{f\left(x_n\right)}
		\end{displaymath}
		d'après 2, avec $a < z_n <x_n$. On a alors $\lim_{n \to \infty} \frac{x_{n+1} -a}{\left(x_n - a\right)^2} = \frac{1}{2}\frac{f''\left(a\right)}{f\left(a\right)}$. D'où le résultat.
		
		\textblue{\begin{req}
				On est dans le cas, $f$ convexe. Dans ce cas on peut adoucir les hypothèses de la méthode de Newton sans diminuer sa vitesse de convergence.
			\end{req}}
		
	\end{proof}
	
	\section{Compléments: notion de points fixes}
	\input{./../Notions/PointsFixes.tex}
	

	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}