\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}
\usepackage{multirow}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}


\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\D}{\mathcal{D}}
	\newcommand{\A}{\mathfrak{A}}
	
	\sloppy
	
	\newcommand{\textblue}{\textcolor{blue}}
	\newcommand{\textgreen}{\textcolor{OliveGreen}}
	\newcommand{\textred}{\textcolor{red}}
	\newcommand{\textbrown}{\textcolor{Brown}}
	\newcommand{\textmagenta}{\textcolor{DarkOrchid}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{lemme}{Lemme}
	\newtheorem*{appli}{Application}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Théorème de Weierstrass via la convolution}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Gourdon \cite[p.284]{Gourdon-analyse}.
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 203 (Compacité); 228 (Continuité et dérivabilité); 239 (Fonctions par intégrales).
		}}
	
	\section{Introduction}
	
	Un célèbre théorème de Weierstrass affirme que toute fonction continue sur un segment de $\R$ est limite uniforme sur ce segment d'une suite de polynômes. La démonstration originale de ce théorème fut donnée à l'aide de la convolution et des suites de fonctions particulières que nous appelons approximation de l'unité. 
	
	Notons que cette démonstration n'est pas constructive: on ne donne pas la suite de polynôme qui converge vers la fonction continue. On se "contente" de prouver leur existence. Une deuxième démonstration constructive existe. Elle fait intervenir les polynômes de Bernstein et les probabilités, mais nous n'en parlerons pas ici.
	
	L'objectif du développement est donc de montrer le théorème suivant:
	\begin{theo}[Weierstrass]
		Pour tout $f: [a,b] \to \R$ continue où $[a, b]$ est un segment de $\R$, il existe une suite de polynôme convergeant uniformément vers $f$ sur $[a, b]$.
	\end{theo}	
	
	\section{Preuve du théorème de Weierstrass}
	
	\paragraph{Cadre} On note $\mathcal{C}_c^0$ l'ensemble des fonctions continues nulle en dehors d'un compact (soit les fonctions continues à support compact). On muni cet ensemble de la loi de convolution $*$, ce qui nous permet de définir la convolée pour toutes fonctions de $\mathcal{C}_c^0$.
	
	\begin{theo}[Weierstrass]
		Pour tout $f: [a,b] \to \R$ continue où $[a, b]$ est un segment de $\R$, il existe une suite de polynôme convergeant uniformément vers $f$ sur $[a, b]$.
	\end{theo}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement}}}{
		\begin{enumerate}
			\item Montrer que la suite de fonctions $(f * \chi_n)_{n\in\N}$ converge uniformément vers $f$ dans $\R$.
			\item On définit une approximation de l'unité $p_n$ et on montre que $f * p_n$ est un polynôme dans le cas où le support de $f$ est $[-\frac{1}{2}; \frac{1}{2}]$.
			\item On conclut la preuve
		\end{enumerate}
	}
	
	\begin{proof}
		Soit $f \in \mathcal{C}_c^0$ et $(\chi_n)_{n \in \N}$ une approximation de l'unité. Nous allons montrer le théorème en trois étapes.
		
		\paragraph{Étape 1 : montrons que la suite de fonctions $(f * \chi_n)_{n\in\N}$ converge uniformément vers $f$ dans $\R$.} Soit $\epsilon > 0$, montrons que $\lim_{n \to \infty} \mathopen{||}(f * \chi_n) - f \mathclose{||}_{\infty} = 0$.
		
		Comme $f \in \mathcal{C}_c^0$, par le théorème de Heine, $f$ est uniformément continue sur son support. De plus, elle est nulle partout ailleurs donc $f$ est uniformément continue sur $\R$. on en déduit: 
		\begin{displaymath}\begin{array}{cc}
		\exists \eta > 0, \forall (x, y)\in \R^2, \mathopen{|}x-y \mathclose{|}< \eta, & \mathopen{|}f(x)-f(y) \mathclose{|} \leq \epsilon\\
		\end{array}
		\end{displaymath}
		
		Soit $N \in \N$ tel que pour tout $n > N$, on ait $\int_{\mathopen{|}t\mathclose{|}\geq \eta} \chi_n(t)dt < \epsilon$. \textblue{(L'existence d'un tel $N$ est assuré par la définition de $\chi_n$. En effet, comme $\forall \alpha >0, \lim_{n \to \infty} \int_{\mathopen{|}t \mathclose{|} \geq \alpha} \chi_n(t)dt = 0$, si on pose $\alpha = \eta$, par définition de la limite on trouve $N$.)} Soit $n \geq N$, on a alors, pour tout $x \in \R$:
		
		\begin{tabular}{ccll}
			$\mathopen{|}f * \chi_n (x) - f(x) \mathclose{|}$ & $=$ & $\mathopen{|}\int_{-\infty}^{+ \infty}f(x-t)\chi_n(t)dt - f(x) \mathclose{|}$ & \textblue{(def convolution)}\\
			& $=$ & $\mathopen{|}\int_{-\infty}^{+ \infty}f(x-t)\chi_n(t)dt - f(x)\int_{-\infty}^{+ \infty} \chi_n(t)dt \mathclose{|}$ & \textblue{($\forall n \in \N,  \int_{-\infty}^{+\infty} \chi_n(t)dt = 1$)} \\
			& $=$ & $\mathopen{|}\int_{-\infty}^{+ \infty}(f(x-t) - f(x)) \chi_n(t)dt \mathclose{|}$ & \textblue{(linéarité)} \\
			& $\leq$ & $\int_{-\infty}^{+ \infty} \mathopen{|}f(x-t) - f(x)\mathclose{|} \chi_n(t)dt $ & \textblue{($\forall n \in \N, \chi_n \geq 0$)} \\
			& \multirow{3}{*}{$\leq$} & $\int_{\mathopen{|}t\mathclose{|} \geq \eta} \mathopen{|}f(x-t) - f(x)\mathclose{|} \chi_n(t)dt$ & \multirow{3}{*}{\textblue{(séparation de $\int$)}}\\
			& & \multicolumn{1}{c}{$+$} & \\
			& & $\int_{-\eta}^{+ \eta} \mathopen{|}f(x-t) - f(x)\mathclose{|} \chi_n(t)dt $ &  \\
		\end{tabular}
		On veut alors majore chacun des deux termes du membre de droite de l'inégalité. On a:
		
		\begin{tabular}{ccll}
			$\int_{\mathopen{|}t\mathclose{|} \geq \eta} \mathopen{|}f(x-t) - f(x)\mathclose{|} \chi_n(t)dt$ & $\leq$ & $2\mathopen{||}f\mathclose{||}_{\infty} \int_{\mathopen{|}t\mathclose{|} \geq \eta} \chi_n(t)dt$ & \textblue{(def de norme sup et linéarité)}\\
			& $\leq$ & $2\mathopen{||}f\mathclose{||}_{\infty}\epsilon$ & \textblue{(Hypothèse sur $N$)}\\
		\end{tabular}
		et 
		
		\begin{tabular}{ccll}
			$\int_{-\eta}^{+ \eta} \mathopen{|}f(x-t) - f(x)\mathclose{|} \chi_n(t)dt$ & $\leq$ & $\int_{-\eta}^{+ \eta} \epsilon \chi_n(t)dt$ & \textblue{($f$ est uniformément continue)}\\
			& $=$ & $\epsilon \int_{-\eta}^{+ \eta} \chi_n(t)dt$ & \textblue{(linéarité de $\int$)}\\
			& $\leq$ & $\epsilon \int_{-\infty}^{+ \infty} \chi_n(t)dt$ & \textblue{($[-\eta; + \eta] \subseteq [-\infty; + \infty]$ et croissance de $\int$)}\\
			& $\leq$ & $\epsilon$ & \textblue{($\int_{-\infty}^{+ \infty} \chi_n(t)dt = 1$ par def)}\\
		\end{tabular}
		On en déduit que:
		\begin{displaymath}
		\mathopen{|}f * \chi_n (x) - f(x) \mathclose{|} \leq (2\mathopen{||}f\mathclose{||}_{\infty} + 1 )\epsilon
		\end{displaymath}
		En passant au $\sup$ sur $\R$, on obtient que:
		\begin{displaymath}
		\mathopen{||}f * \chi_n - f\mathclose{||}_{\infty} \leq (2\mathopen{||}f\mathclose{||}_{\infty} + 1 )\epsilon
		\end{displaymath}
		Comme, $f$ est bornée \textblue{(car elle continue à support compact)}, $\mathopen{||}f\mathclose{||}_{\infty} < \infty$, on a: $\lim \mathopen{||}f * \chi_n - f\mathclose{||}_{\infty} = 0$. D'où la convergence uniforme.
		
		\paragraph{Étape 2 : on définit une approximation de l'unité $p_n$ et on montre que $f * p_n$ est un polynôme dans le cas où le support de $f$ est $[-\frac{1}{2}; \frac{1}{2}]$.} Pour tout $n \in \N$, on pose
		\begin{displaymath}
		\begin{array}{ccc}
		a_n = \int_{-1}^{1} (1-t^2)^n dt & \mathrm{et} & \begin{array}{cccc}
		p_n : & \R & \to & \C \\
		& t & \mapsto & 
		\left\{ 
			\begin{array}{cc}
			(1 - t^2)^n / a_n & \mathrm{si }~\mathopen{|}t\mathclose{|} \leq 1 \\
			0 & \mathrm{sinon}
			\end{array}
			\right.
		\end{array} \\
		\end{array} \\
		\end{displaymath}
		On remarque que $p_n$ est bien une approximation de l'unité. \textblue{(En effet, 
			\begin{displaymath}
			\int_{-\infty}^{+\infty} p_n \underbrace{=}_{\textrm{définition de } p_n} \int_{-1}^{+1} a_n \underbrace{=}_{\textrm{définition de } a_n} 1
			\end{displaymath}
		De plus, on a que	
		\newline
		\begin{tabular}{ccll}
			$a_n$ & $=$ & $2\int_{0}^{1} (1-t^2)^n dt$ & (par $t^2$)\\
			& $\geq$ & $2\int_{0}^{1} t(1-t^2)^n dt$ & (par $t>0$)\\
			& $=$ & $\left[\frac{-(1 - t^2)^{n+1}}{n+1}\right]_0^1 $ & (par calcul de $\int$ avec primitive)\\
			& $=$ & $\frac{1}{n+1}$ & (par calcul)\\
		\end{tabular}
		\newline
		Donc si $\alpha > 0$ (et $\alpha < 1$), on a $\forall n \in \N^{*}$:
		\newline
		\begin{tabular}{ccll}
			$\int_{\mathopen{|}t \mathclose{|} \geq \alpha} p_n(t)dt$ & $=$ & $\frac{2}{a_n}\int_{\alpha}^{1} (1-t^2)^n dt$ & (par $t^2$)\\
			& $\leq$ & $\frac{2}{a_n}(1-\alpha^2)^n$ & (en majorant brutalement l'intégrale)\\
			& $\leq$ & $2(n+1)(1 - \alpha^2)^n$ & (par le calcul de $a_n$)\\
		\end{tabular}
		\newline
		Et comme $\mathopen{|} 1 -\alpha^2 \mathclose{|} < 1$, on obtient la limite que l'on cherche.)}
		
		Soit $I = [-\frac{1}{2}; \frac{1}{2}]$. Soit $f \in \mathcal{C}_c^{0}$ telle que son support soit $I$. Montrons que $f * p_n$ est un polynôme. On a pour tout $x \in I$:
		\begin{displaymath}
		(f * p_n)(x) \underbrace{=}_{\textrm{commutativité}} (p_n * f)(x) \underbrace{=}_{\textrm{def}} \int_{-\infty}^{+\infty} p_n(x-t)f(t)dt \underbrace{=}_{\textrm{support de }f} \int_{-\frac{1}{2}}^{+\frac{1}{2}} p_n(x-t)f(t)dt 
		\end{displaymath}
		De plus, si $x \in I$ et $t \in I$, on a $\mathopen{|} x - t \mathclose{|} \leq 1$ \textblue{(car la longueur de l'intervalle $I$ est de $1$)}. On en déduit que $p_n(x - t) = \frac{(1 - (x - t)^2)^n}{a_n}$, soit
		
		\begin{tabular}{ccll}
			$p_n(x - t)a_n$ & $=$ & $(1 - (x - t)^2)^n$ & \textblue{(multipliant par $a_n$)} \\
			& \textblue{$=$} & 
			\textblue{$ \sum_{k=0}^n \left(\!\!\!\begin{array}{c}
				n \\
				r
				\end{array}
				\!\!\!\right)(1)^{n-k}\left(-(x-t)^2\right)^k $}
			& \textblue{(en développant la somme dans $n$)} \\
			& \textblue{$=$} & 
			\textblue{$ \sum_{k=0}^n \left(\!\!\!\begin{array}{c}
				n \\
				r
				\end{array}
				\!\!\!\right)\left(-x^2+2xt-t^2\right)^k $}
			& \textblue{(en développant le carré)} \\
			& \textblue{$=$} & 
			\textblue{$ \sum_{k=0}^n \left(\!\!\!\begin{array}{c}
				n \\
				r
				\end{array}
				\!\!\!\right) \left(\sum_{j=0}^k \left(\!\!\!\begin{array}{c}
				k \\
				j
				\end{array}
				\!\!\!\right) \left(-x^2-t^2\right)^j (2tx)^{k-j} \right) $}
			& \textblue{(en développant la somme dans $k$)} \\
			& \textblue{\multirow{2}{*}{$=$}} & 
			\textblue{$ \sum_{k=0}^n \left(\!\!\!\begin{array}{c}
				n \\
				r
				\end{array}
				\!\!\!\right) \left(\sum_{j=0}^k \left(\!\!\!\begin{array}{c}
				k \\
				j
				\end{array}
				\!\!\!\right) \left(\sum_{i=0}^j \left(\!\!\!\begin{array}{c}
				j \\
				i
				\end{array}
				\!\!\!\right) A (2tx)^{k-j} \right) \right) $}
			& \textblue{\multirow{2}{*}{(en développant la somme dans $k$)}} \\
			& & \textblue{où $A = \left((-1)^{i}x^{2i} + (-1)^{j-i}t^{2(j-i)}\right)$} & \\
			& $=$ & $\sum_{h=0}^{2n} q_k(t)x^k$ & avec $q_k$ un polynôme.
		\end{tabular}
		
		D'où, de ce qui précède, on a : 
		\begin{displaymath}
		(f*p_n)(x) = \int_{-\frac{1}{2}}^{+\frac{1}{2}} \left(\sum_{h=0}^{2n} q_k(t)x^k \right) f(t)dt
		\end{displaymath}  
		Soit par linéarité de l'intégrale \textblue{(comme la somme est finie)}, on en déduit que:
		\begin{displaymath}
		(f*p_n)(x) = \sum_{h=0}^{2n}\left(\int_{-\frac{1}{2}}^{+\frac{1}{2}}  q_k(t) f(t)dt\right)x^k
		\end{displaymath}
		qui est bien une fonction polynôme sur $I$. De plus, par le point précédent comme $f$ est limite uniforme de $f*p_n$, on en déduit que $f$ est limite uniforme sur $I$ d'une suite de fonctions polynômes.
		
		\paragraph{Étape 3 : on conclut la preuve} Soient $[a, b]$ un segment de $\R$ et $f : [a, b] \to \R$ une fonction continue. 
		
		Soit $[c, d]$ un intervalle de $\R$ tel que $[a, b] \subseteq [c, d]$ \textblue{(existe car un intervalle de $\R$ est un fermé borné)}. On peut alors prolonger continûment $f$ sur $[c, d]$ par des fonctions affines valant $0$ en $c$ et $f(a)$ en $a$ (de même pour $[b, d]$). Puis, on prolonge $f$ sur tout $\R$ par $0$ hors de $[c, d]$. Donc $f$ ainsi prolongée appartient à $\mathcal{C}^0_c$.
		%TODO: dessin
		
		En effectuant un changement de variable affine \textblue{(on pose $y = \frac{x - \frac{d+c}{2}}{d-c}$ car $\frac{d+c}{2}$ est le centre du segment et $d-c$ sa longueur)} on peut se placer dans le cas où $[c,d] = \left[-\frac{1}{2}, \frac{1}{2}\right]$. On applique ainsi le résultat obtenu précédemment et $f$ est limite uniforme de fonctions polynomiales sur $[c, d]$, elle l'est donc en particulier sur $[a, b]$ \textblue{(car $[a, b] \subseteq [c, d]$)}.
		
	\end{proof}
	
	\begin{req}
		Une fonction qui est limite uniforme de fonctions polynomiales est une fonction polynomiale.
	\end{req}
	
	\section{Compléments autours de l'utilisation de la convolution}
	
	\subsection*{Convolution}
	\input{./../Notions/Convolution.tex}	
		
	\subsection*{Approximation de l'unité}
	\input{./../Notions/ApproxUnite.tex}		
		
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}

\end{document}