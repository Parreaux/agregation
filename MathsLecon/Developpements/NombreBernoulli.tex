\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\Cun}{\mathcal{C}^1}
	\newcommand{\Cdeux}{\mathcal{C}^2}
	\newcommand{\Cinf}{\mathcal{C}^{\infty}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{OliveGreen}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	\newcommand{\textmagenta}[1]{\textcolor{DarkOrchid}{#1}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\newcommand{\pcite}[1]{\protect{\cite{#1}}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\newtheorem*{application}{Application}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{remarque}{Remarque}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	
	\title{Série génératrice des nombres de Bernoulli}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} X-ENS analyse 2 \cite[p.308]{FrancinouGianellaNicolas-an2}.
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 230 (Séries numériques); 243 (Séries entières); 246 (Séries de Fourier).
		}}
	
	\section{Introduction}
	
	La série génératrice des nombres de Bernoulli permet alors de calculer les nombres de Bernoulli. Ils représentent une suite de nombres rationnels correspondant à la valeur de la limite d'une série de nombre puissance $m$ (variable qui nous permet de construire la dite suite). Cette suite peut s'exprimer par une série génératrice dont le calcul nécessite un développement en série de Fourier constituant la base du développement en série entière cherchée.
	
	\section{Série génératrice des nombres de Bernoulli}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement}}}{
		\begin{itemize}
			\item Proposition: $\varphi$ est développable en série de Fourier et calcul de sa somme.
			\begin{enumerate}
				\item Calcul des coefficients de Fourier de $\varphi$.
				\item Calcul de la série de Fourier de $\varphi$.
			\end{enumerate}
			\item Application: calcul d'une série entière 
			\begin{enumerate}
				\item Exprimer $f$ à partir du développement en série Fourier de $\varphi$.
				\item Développer en série entière la fonction $z \mapsto \frac{z^2}{z^2 +4 \pi^2 n^2}$.
				\begin{enumerate}
					\item Exprimer $z \mapsto \frac{z^2}{z^2 +4 \pi^2 n^2}$ comme une somme sur $k$ parcourant $\N$.
					\item Vérifier la sommabilité de la série $u_{n, k}$.
				\end{enumerate}
				\item Calculer la série de $f$.
			\end{enumerate}
		\end{itemize}
	}
	
	\begin{prop}
		Soit $\varphi$ la fonction $2\pi$-périodique définie par $\varphi(x) = e^{\frac{zx}{2\pi}}$ pour $x \in \left] -\pi, \pi\right[$ où $z \in \C \setminus 2i\pi\Z$. Alors $\varphi$ est développable en série de Fourier sur $\R$ et $\forall x \in \R$, $\varphi(x) = \left(e^{\frac{z}{2}} - e^{-\frac{z}{2}}\right)\sum_{n \in \N} \frac{(-1)^n e^{inx}}{z - 2i\pi n}$.
	\end{prop}
	\begin{proof}
		Soit $x \in \R$.
		
		\paragraph{Étape 1: calcul des coefficients de Fourier de $\varphi$}
		La fonction $\varphi$ est continue par morceaux sur $\R$, on peut alors calculer ses coefficients de Fourier. Pour $n \in \Z^*$.
		\begin{displaymath}
		\begin{array}{ccll}
		c_n(\varphi) & = & \frac{1}{2\pi} \int_{-\pi}^{\pi} \varphi(x)e^{inx} dx & \textblue{\text{(Définition des coefficients de Fourier)}} \\
		
		& = & \frac{1}{2\pi} \int_{-\pi}^{\pi} e^{\frac{zx}{2\pi}}e^{inx} dx & \textblue{\text{(Définition de $\varphi$)}} \\
		& = & \frac{1}{2\pi} \frac{1}{\frac{z}{2\pi} - ni} \left[e^{\frac{zx}{2\pi}}e^{inx}\right]_{-\pi}^{\pi} & \textblue{\text{(Par intégration via une primitive)}} \\
		& = & \frac{1}{z - 2\pi ni} \left[e^{\frac{z\pi}{2\pi}}e^{-in\pi} - e^{-\frac{z\pi}{2\pi}}e^{in\pi}\right] & \textblue{\text{(Par calcul de primitive)}} \\
		& = & \frac{1}{z - 2\pi ni} \left[e^{\frac{z}{2}}e^{-in\pi} - e^{-\frac{z}{2}}e^{in\pi}\right] & \textblue{\text{(Simplification par $\pi$)}} \\
		& = & \frac{1}{z - 2\pi ni} \left[e^{\frac{z}{2}}(-1)^n - e^{-\frac{z}{2}}(-1)^n\right] & \textblue{\text{($e^{-i\pi} = e^{i\pi} = -1$)}} \\
		& = & \frac{(-1)^n\left(e^{\frac{z}{2}} - e^{-\frac{z}{2}}\right)}{z - 2\pi ni} & \\
		\end{array}
		\end{displaymath}
		
		\paragraph{Étape 2: calcul de la série de Fourier de $\varphi$}
		La fonction $\varphi$ est $\mathcal{C}^1$ par morceau sur $\R$. Le théorème de Dirichlet permet d'affirmer que sa série de Fourier converge en tout point $x$ de $\R$ vers $\frac{1}{2}\left(\varphi(x + 0) + \varphi(x - 0)\right)$. On a donc, $\forall x \in \R$, $\frac{1}{2}\left(\varphi(x + 0) + \varphi(x - 0)\right) = \left(e^{\frac{z}{2}} - e^{-\frac{z}{2}}\right)\sum_{n \in \Z} \frac{(-1)^n e^{inx}}{z - 2i\pi n}$ \textblue{(écriture de la somme en manipulant les termes qui ne dépendent pas de $n$)}.
	\end{proof}
	
	\begin{application}
		La fonction $f \mapsto \frac{z}{e^z - 1}$ est développable en série entière en $0$:
		\begin{displaymath}
		f(z) = 1 - \frac{z}{2} + 2\sum_{k \in \N^*}\frac{(-1)^{k - 1}}{(2\pi)^{2k}} \left(\sum_{n \in \N^*} \frac{1}{n^{2k}}\right)z^{2k}
		\end{displaymath}
	\end{application}
	\begin{proof}
		Afin de calculer le développement en série entière de la fonction $f$, on va choisir une valeur de $x$ pour appliquer le développement en série de Fourier de $\varphi$. On songe naturellement à $0$ ou à $2\pi$.
		\begin{description}
			\item[Si $x = 0$] On a $\frac{1}{2}\left(\varphi(0) + \varphi(0)\right) = \frac{1}{2}\left(e^0 + e^0\right) = 1$. On ne voit pas apparaître $f(z)$.
			\item[Si $x = 2\pi$] On a $\frac{1}{2}\left(\varphi(\pi + 0) + \varphi(\pi - 0)\right) = \frac{1}{2}\left(e^{\frac{z}{2}} + e^{-\frac{z}{2}}\right)$ \textblue{(en évaluant les limites sans oublier que $\varphi$ est $2\pi$-périodique soit que la limite supérieure en $\pi$ est une limite supérieure en $-\pi$)}. Nous allons donc essayer de faire apparaître la fonction $f$.
			
			\paragraph{Étape 1: exprimons $f$ à partir du développement en série Fourier de $\varphi$}
			\begin{itemize}
				\item En évaluant la série en $\pi$, on obtient : 
				\begin{displaymath}
				\begin{array}{ccll}
				\frac{1}{2}\left(e^{\frac{z}{2}} + e^{-\frac{z}{2}}\right) & = & \left(e^{\frac{z}{2}} - e^{-\frac{z}{2}}\right)\sum_{n \in \N} \frac{(-1)^n e^{in\pi}}{z - 2i\pi n} & \textblue{\text{(Évaluation de la série)}} \\
				& = & \left(e^{\frac{z}{2}} - e^{-\frac{z}{2}}\right)\sum_{n \in \Z} \frac{(-1)^n (-1)^n}{z - 2i\pi n} & \textblue{(e^{in\pi} = (-1)^n)} \\
				& = & \left(e^{\frac{z}{2}} - e^{-\frac{z}{2}}\right)\sum_{n \in \Z} \frac{1}{z - 2i\pi n} & \textblue{\text{(Calcul)}} \\
				& = & \left(e^{\frac{z}{2}} - e^{-\frac{z}{2}}\right)\sum_{n \in \Z} \frac{z - 2i\pi n}{z^2 +4 \pi^2 n^2} & \textblue{\text{(Multiplication par $z - 2i\pi n$ dans la fraction)}} \\
				\end{array}
				\end{displaymath}
				\item En divisant par $\left(e^{\frac{z}{2}} - e^{-\frac{z}{2}}\right) \textblue{\neq 0}$ et en regroupant les termes de la sommes (ils sont égaux pour des $n$ opposés), on obtient : 
				\begin{displaymath}
				\frac{\left(e^{\frac{z}{2}} + e^{-\frac{z}{2}}\right)}{2\left(e^{\frac{z}{2}} - e^{-\frac{z}{2}}\right)} = \underbrace{\frac{1}{z}}_{\textblue{n = 0 \text{ dans la somme}}} + 2\sum_{n \in \N^*} \underbrace{\frac{z}{z^2 +4 \pi^2 n^2}}_{\textblue{\text{$2i\pi n$ disparaît: compansation}}}
				\end{displaymath}
				\item On transforme le premier membre:
				\begin{displaymath}
				\begin{array}{ccll}
				\frac{\left(e^{\frac{z}{2}} + e^{-\frac{z}{2}}\right)}{2\left(e^{\frac{z}{2}} - e^{-\frac{z}{2}}\right)} & = & \frac{\left(e^{z} + 1\right)}{2\left(e^{z} - 1\right)} & \textblue{\text{(Multiplication par $e^{\frac{z}{2}}$)}} \\
				& = & \frac{\left(e^{z} - 1 + 2\right)}{2\left(e^{z} - 1\right)} & \textblue{\text{(Introduction du $-1$)}} \\
				& = & \frac{1}{2}\left(\frac{e^{z} - 1}{e^{z} - 1} + \frac{2}{e^{z} - 1}\right) & \textblue{\text{(Manipulation de la fraction)}} \\
				& = & \frac{1}{2} + \frac{1}{e^{z} - 1} & \textblue{\text{(Simplification et développement)}} \\
				\end{array}
				\end{displaymath}
				On obtient alors : $\frac{1}{2} + \frac{1}{e^{z} - 1} = frac{1}{z} + 2\sum_{n \in \N^*} \frac{z}{z^2 +4 \pi^2 n^2}$.
				\item En multipliant l'égalité par $z$ et en soustrayant par $\frac{z}{2}$, on obtient pour tout $z \in \C \setminus 2i\pi\Z$: $\frac{z}{2} + \frac{z}{e^{z} - 1} -\frac{z}{2} = \frac{z}{z} + 2z\sum_{n \in \N^*} \frac{z}{z^2 +4 \pi^2 n^2} - \frac{z}{2}$, soit après calcul $f(z) = 1- \frac{z}{2}  + 2\sum_{n \in \N^*} \frac{z^2}{z^2 +4 \pi^2 n^2}$.
			\end{itemize}
			
			\paragraph{Étape 2: développons en série entière la fonction $z \mapsto \frac{z^2}{z^2 +4 \pi^2 n^2}$}
			Il nous reste ensuite à développer en série entière la somme que nous venons d'exhiber. Pour cela, nous allons développer en série entière la fonction $z \mapsto \frac{z^2}{z^2 +4 \pi^2 n^2}$.
			
			\subparagraph{Étape a: exprimons $z \mapsto \frac{z^2}{z^2 +4 \pi^2 n^2}$ comme une somme sur $k$ parcourant $\N$} On a, pour $\left|z\right| < 2\pi$ et $n \in \N^*$, $\left|\frac{z}{2\pi n}\right| < 1$ \textblue{(car $n > 0$ et $\frac{2\pi}{2\pi n} = \frac{1}{n}$)}. Donc
			\begin{displaymath}
			\begin{array}{ccll}
				\frac{z^2}{z^2 +4 \pi^2 n^2} & = & \frac{z^2}{z^2 +4 \pi^2 n^2}\frac{1}{1 + \frac{z^2}{4 \pi^2 n^2}} & \textblue{\text{(Factorisation par $4\pi^2n^2$)}} \\
				& = & \frac{z^2}{z^2 +4 \pi^2 n^2} \sum_{k = 0}^{\infty}(-1)^k\frac{z^{2k}}{\left(4 \pi^2 n^2\right)^k} & \textblue{\text{(Série entière de la fonction $x \mapsto \frac{1}{1 + x}$)}} \\
				& = & \sum_{k = 0}^{\infty}(-1)^k\frac{z^{2(k + 1)}}{\left(4 \pi^2 n^2\right)^{k + 1}} & \textblue{\text{(On rentre la constante dans la somme)}} \\
				& = & \sum_{k = 1}^{\infty}(-1)^{k - 1}\frac{z^{2k}}{\left(2\pi n\right)^k} & \textblue{\text{(Changement d'indice et manipulation des carrés)}} \\
			\end{array}
			\end{displaymath}
				
			\subparagraph{Étape b: vérifions la sommabilité de la série $u_{n, k}$} Considérons la série double $\sum u_{n, k}$ avec $u_{n, k} = (-1)^{k+1} \frac{z^{2k}}{(2\pi n)^{2k}}$, pour tout $n, k \in \N$.
			\begin{itemize}
				\item Vérifions sa sommabilité en $k$: $\forall n \in \N^*$, 
				\begin{displaymath}
				\begin{array}{ccll}
				\sum_k \left|u_{n, k}\right| & = & \sum_k \left|(-1)^{k+1} \frac{z^{2k}}{(2\pi n)^{2k}}\right| & \textblue{\text{(Définition de la suite)}} \\
				& = & \sum_k \frac{\left|z\right|^{2k}}{(2\pi n)^{2k}} & \textblue{\text{(Propriétés du module)}} \\
				\end{array}
				\end{displaymath}
				La série converge.
				
				\item Calculons sa somme sur $k$:
				\begin{displaymath}
				\begin{array}{ccll}
				\sum_k \left|u_{n, k}\right| & = & \sum_k \frac{\left|z\right|^{2k}}{(2\pi n)^{2k}} & \textblue{\text{(Par ce qui précède)}} \\
				& = & \frac{z^{2}}{(2\pi n)^{2}}\frac{1}{1 - \frac{\left|z\right|^{2}}{(2\pi n)^{2}}} & \textblue{\text{(On sort la constante puis on trouve une série entière usuelle)}} \\
				& = & \frac{z^{2}}{(2\pi n)^{2} - \left|z\right|^2} & \textblue{\text{(Multipliction)}} \\
				\end{array}
				\end{displaymath}
				
				\item Vérifions la double sommabilité: comme la série $\sum_n \frac{z^{2}}{(2\pi n)^{2} - \left|z\right|^2}$ converge, on en déduit que la série sur $u_{n, k}$ est double sommable. On en déduit que nous pouvons sommer dans le sens que l'on souhaite (cas particulier de Fubini).
			\end{itemize}
			
		\paragraph{Étape 3: calculons la série de $f$} Pour tout $\left|z\right| < 2\pi$ et $z \neq 0$, on a
			\begin{displaymath}
			\begin{array}{ccll}
			f(z) & = & 1 - \frac{z}{2} + 2 \sum_{n \in \N^*} \sum_{k \in \N^*} u_{n, k} & \textblue{\text{(Définition de $u_{n, k}$)}} \\
			& = & 1 - \frac{z}{2} + 2 \sum_{k \in \N^*} \sum_{n \in \N^*} u_{n, k} & \textblue{\text{(Échabge des sommes)}} \\
			& = & 1 - \frac{z}{2} + 2 \sum_{k \in \N^*} \sum_{n \in \N^*} (-1)^{k+1} \frac{z^{2k}}{(2\pi n)^{2k}} & \textblue{\text{(Traduction de $u_{n, k}$)}} \\
			& = & 1 - \frac{z}{2} + 2 \sum_{k \in \N^*} \frac{(-1)^{k-1}}{(2\pi)^{2k}} \left(\sum_{n \in \N^*} \frac{1}{n^{2k}}\right)z^{2k} & \textblue{\text{(Manipulation des sommes)}} \\
			\end{array}
			\end{displaymath}
			Remarquons que $f$ admet un prolongement par continuité en $0$ défini par $f(0) = 1$ et que l'égalité est encore vérifiée pour $z = 0$. On obtient donc, pour tout $\left|z\right| < 2\pi$:
			\begin{displaymath}
			f(z) = 1 - \frac{z}{2} + 2\sum_{k \in \N^*}\frac{(-1)^{k - 1}}{(2\pi)^{2k}} \left(\sum_{n \in \N^*} \frac{1}{n^{2k}}\right)z^{2k}
			\end{displaymath}
		\end{description}
		
		\begin{remarque}
			Ce développement en série entière n'est définie que pour $\left|z\right| < 2\pi$ (son rayon de convergence est de $2\pi$). Cependant, on ne peut pas espérer mieux, car $f$ n'est pas définie en $2\pi$.
		\end{remarque}
	\end{proof}

	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}