\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
} 

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\Cun}{\mathcal{C}^1}
	\newcommand{\Cdeux}{\mathcal{C}^2}
	\newcommand{\Cinf}{\mathcal{C}^{\infty}}
	
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{OliveGreen}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	\newcommand{\textmagenta}[1]{\textcolor{DarkOrchid}{#1}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\newcommand{\pcite}[1]{\protect{\cite{#1}}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	
	\title{Décomposition de Dunford via la méthode de Newton}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Algèbre pour la licence \cite[p.63 et p.176]{RieslerBoyer}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 141 (Polynôme irréductibles); 153 (Réduction); 157 (Triangulaire et nilpotent).
		}}
	
	\section{Introduction}
	
	La décomposition de Dunford permet d'écrire une matrice comme la somme de deux matrices: une matrice diagonale et une matrice nilpotente. Cette décomposition permet de simplifier quelques calculs: la puissance des matrices et l'exponentielle de matrice. De plus, la décomposition permet de mettre en place un méthode de descente de Newton.
	
	Grâce à la théorie de l'extension de corps, $D$ est toujours diagonalisable sur un certain corps. La décomposition de Dunford peut alors être appliquée dans ce corps: elle est toujours applicable.
	
	\section{Décomposition de Dunford}
	
	\noindent\begin{minipage}{.48\textwidth}
		\emph{Cadre}: On prend $A \in \mathcal{M}_d(\R)$. La décomposition de Dunford de $A$ (sous l'hypothèse que le polynôme minimal de $A$ soit scindé) nous donne l'existence et l'unicité de deux matrices $D$, $N \in \mathcal{M}_d(\R)$ telle que
		\begin{itemize}
			\item $A = D + N$
			\item $D$ est diagonalisable (sur $\C$)
			\item $N$ est nilpotente
			\item $D$ et $N$ commutent
		\end{itemize}
		Ces matrices sont alors des polynômes en $A$.
	\end{minipage} \hfill
	\begin{minipage}{.48\textwidth}
		\textblue{\emph{Cadre pour la leçon 1410: }On prend $K$ un sous-corps de $\C$. Soit $E$ un $K$-espace vectoriel de dimension finie et $u \in \mathcal{L}(E)$. La décomposition de Dunford de $u$ nous donne l'existence et l'unicité de deux matrices $D$, $N \in \mathcal{M}_d(\R)$ telle que
			\begin{itemize}
				\item $A = D + N$
				\item $D$ est diagonalisable (sur $\C$)
				\item $N$ est nilpotente
				\item $D$ et $N$ commutent
			\end{itemize}
			Ces matrices sont alors des polynômes en $u$.}
	\end{minipage}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement}}}{
		\begin{enumerate}
			\item Définition de $P$ via la preuve du lemme.
			\item Remarque sur $P(X + Y)$
			\item Définition de la suite de Newton par récurrence.
			\begin{enumerate}
				\item Montrer que pour tout $M$ telle que $P^d(M) = 0$, alors $P'(M)$ est inversible.
				\item Remarque sur $M = A$ (existence d'un tel $M$).
				\item Existence de $(A_n)_n$ au rang $n + 1$.
				\item Calcul de $B_{n +1}$
			\end{enumerate}
			\item Convergence de la suite.
		\end{enumerate}
	}
	
	\paragraph{Étape 1: définition de $P$}
	\begin{lemme}
		Soit $R = \prod_{i = 1}^{n} (X - \lambda_i)^{m_i}$ où $R \in \R[X]$ et les $\lambda_i \in \C$ deux à deux distincts. Soit $P = \prod_{i=1}^{n}(X - \lambda_i)$. Alors, on a $P = \frac{R}{R \wedge R'}$ et donc $P \in \R[X]$.
		\label{lem:1}
	\end{lemme}
	\begin{proof}
		Si $(X - \lambda_i)^{m_i}$ divise $R$, alors $(X - \lambda_i)^{m_i - 1}$ divise $R'$: la multiplicité de $\lambda_i$ dans $R'$ est égale à $m_i - 1$. Ainsi $\mathrm{pgcd}(R, R')$ contient le terme $(X - \lambda_i)$ exactement $m_i - 1$ fois. Donc, $\frac{R}{R \wedge R'}$ sera divisé une unique fois par $(X - \lambda_i)$, $P$ est alors unitaire et donc on obtient l'égalité. Donc, $P \in \R[X]$ \textblue{($R \in \R[X]$ et $R' \in \R[X]$, par dérivation, et $\frac{R}{R \wedge R'} \in \R[X]$, par division euclidienne)}.
	\end{proof}
	
	On applique alors le lemme à $R = \chi_A$ \textblue{($R = \chi_u$)} le polynôme caractéristique de $A$ \textblue{($u$)}.
	\begin{itemize}
		\item $P$ est le produit de $(X - \lambda_i)$ où $\lambda_i$ décrit les valeurs propres de $A$ \textblue{($u$)}.
		\item $P$ est à racine simple et vérifie donc $P \wedge P' = 1$ \textblue{(multiplicité tombe à zéro)}.
	\end{itemize}
	Donc $P \in \R[X]$.
	
	\paragraph{Étape 2: étude de $P(X + Y)$} 
	\begin{req}
		Par le théorème de Taylor avec le reste intégral: $P(X + Y) = P(X) + YP'(X) + Y^2\underbrace{\int_{0}^{1} (1 - t)P''(X + tY) dt}_{Q(X, Y) \in \R[X, Y]}$.
	\end{req}

	\paragraph{Étape 3: étude de la suite de Newton} On pose $A_0 = A$ et $A_{n+1} = A_n - P(A_n)(P'(A_n))^{-1}$. Il nous faut donc vérifier que $P'(A_n)^{-1}$ existe, c'est-à-dire que $P'(A_n)$ est toujours inversible. On le montre par récurrence sur $n \in \N$. On pose $\mathcal{P}_n$: "$\left(A_n\right)_n$ existe jusqu'au rang $n$ et il existe $B_n \in \R[X]$, $P(A_n) = P(A)^{2^n}B_n(A)$".
	\begin{description}
		\item[Initialisation] $n = 0$, on pose alors $B_0 = P$. Ce qui conclut.
		\item[Hérédité] Soit $n \in \N$ tel que $\mathcal{P}_n$ soit vérifié.
		\begin{enumerate}
			\item Montrons que pour toute matrice $M$ tel que $P^d(M) = 0$, on a $U(M)P'(M) = I$. 
			\begin{itemize}
				\item $P^d \wedge P' = 1$ \textblue{(les racines de $P$ ne sont pas celles de $P'$)}.
				\item On applique alors Bézout $\exists U, V \in \R[X]$ tels que $VP^d + UP' = 1$.
				\begin{itemize}
					\item On évalue cette égalité en $M$: $U(M)P'(M) = I - V(M)P^d(M)$.
					\item On en déduit que $U(M)P'(M) = I$: $P'(M)$ est donc inversible par un inverse facilement calculable \textblue{(Bézout et évaluation)}.
				\end{itemize}
			\end{itemize}	
			\item Remarque si $M = A$. Comme $\chi_A$ divise $P^d$ \textblue{($P^d = \frac{\chi_A^d}{(\chi_A \wedge \chi_A')^d}$)}: $P^d(A) = 0$ par Cayley-Hamilton. donc il existe une matrice $M$ vérifiant la propriété précédente.
			\item Montrons l'existence de $A_{n+1}$. Pour cela, montrons que $U(A_n)P'(A_n) = I$. Par la première étape dans l'hérédité, il nous suffit de montrer que $P^d(A_n) = 0$.
			\begin{displaymath}
			\begin{array}{ccll}
			P^d(A_n) & = & P(A_n)^d & \textblue{\text{(même chose mais on inverse la loi)}} \\
			& = & \left(P(A)^{2^n}B_n(A)\right)^d & \textblue{P(A_n) = P(A)^{2^n}B_n(A) \text{ par hypothèse}} \\
			& = & 0 & \textblue{P(A) = 0} \\
			\end{array}
			\end{displaymath}
			Donc, $A_{n+1}$ existe car $P'(A_n)^{-1}$ existe.
			\item Calculons $B_{n + 1}$.
			\begin{itemize}
				\item On pose $Y_n = P(A_n)U(A_n) \in \R[X]$. Exprimons $P(A_{n+1})$ en fonction de $Y_n$.
				\begin{displaymath}
				\begin{array}{ccll}
				P(A_{n+1}) & = & P(A_n -Y_n) & \textblue{(A_{n+1}= A_n - Y_n)} \\
				& = & P(A_n)- Y_nP'(A_n) + Y_n^2\tilde{Q}(A_n, -Y_n) & \textblue{\text{(Taylor avec reste intégral)}} \\
				\end{array}
				\end{displaymath}
				\item Montrons que $P(A_n) - Y_nP'(A_n) = 0$.
				\begin{displaymath}
				\begin{array}{ccll}
				P(A_n) - Y_nP'(A_n) & = & P(A_n) - P(A_n)U(A_n)P(A_n) & \textblue{(\text{en remplaçant } Y_n)} \\
				& = & P(A_n)- P(A_n) & \textblue{(U(A_n) \text{ inverse de } P(A_n))} \\
				& =  & 0 & \\ 
				\end{array}
				\end{displaymath}
				\item En déduire une expression de $P(A_{n+1})$.
				\begin{displaymath}
				\begin{array}{ccll}
				P(A_{n + 1}) & = & P(A_n)^2U(A_n)^2\tilde{Q}(A_n, -Y_n) & \textblue{(Y_n^2 = P(A_n)^2U(A_n)^2)} \\
				& = & P(A)^{2^{n+1}}B_n^2(A)U(A_n)\tilde{Q}(A_n, -Y_n) & \textblue{(\text{par } \mathcal{P}_n)} \\
				& =  & 0 & \\ 
				\end{array}
				\end{displaymath}
				\item On pose $B_{n+1}(A) = B_n^2(A)U(A_n)\tilde{Q}(A_n, -P(A_n)U(A_n)) \in \R[A]$. 
			\end{itemize}
		\end{enumerate}
		Donc on a $\mathcal{P}_{n+1}$. Ce qui achève la récurrence.
	\end{description}
	
	\paragraph{Étape 5: Convergence}
	\begin{itemize}
		\item Il existe $n \in \N$ tel que $2^n < d$, ainsi $P(A)^{2^n}B_n(A) = 0$ car $P^d(A) = 0$. Donc, il existe un rang tel que $A_{n+1} = A_n$ et la suite stagne.
		\item Notons $D = A_n$ cette limite : $P(D) \underbrace{=}_{\text{limite}} P(A_n) \underbrace{=}_{\text{précédent}} 0$. Donc $D$ est diagonalisable sur $\C$.
		\item Posons $N = A - D$.
		\begin{displaymath}
		\begin{array}{ccll}
		N & = & A_0 - A_n & \textblue{(D = A_n, A_0 = A)} \\
		& = & \sum_{k=0}^{n-1} A_k - A_{k+1} & \textblue{\text{(Introduction de la série téléscopique)}} \\
		& = & \sum_{k=0}^{n-1} P(A_k)U(A_k) & \textblue{(\text{Définition de } A_{k+1})} \\
		\end{array}
		\end{displaymath}
		De plus, pour tout $k$, $P(A)$ divise $P(A_k)$: $P(A_k) = P(A)^{2^k}B_k(A)$. Donc $P(A)$ divise $N$: £$N = \sum P(A_k)U(A_k)$. Donc $N^d = $ car  $P(A)^dk = N^d$ et $P(A)^d = 0$.
		\item $N$ et $D \in \R[A]$, ils commutent et on vérifie Dunford.
	\end{itemize}
	
	\section{Algorithme de la décomposition de Dunford}
	
	\emph{Algorithme}: Soit $A$ une matrice de taille $d$, l'algorithme suivant utilise Euclide pour calculer Dunford. La complexité total de cet algorithme est $O(n^md^3\log d)$.
	\begin{algorithm}
		\begin{algorithmic}[1]
			\State Calculer $\chi_A$ \Comment{Calcul de déterminant : $O(d^3)$}
			\State Calculer $\chi_A \wedge \chi_A'$ \Comment{Euclide : $O(k \log^2 k)$}
			\State Calculer $P$ \Comment{Combinaison linéaire : $O(k + k')$}
			\State Calculer $P'$ \Comment{Dérivation : $O(k)$}
			\State Calculer $U, V$ tel que $UP' + VP^d = 1$ \Comment{Euclide étendu : $O(k\log^2 k)$}
			\State Faire $A_0 \gets A$	
			\State Faire $flag \gets \mathrm{false}$
			\State Faire $j \gets 0$
			\While{$j < d$ et non $flag$} \Comment{$\log d$ itérations}
			\State Calculer $P(A_j)$ \Comment{Évaluation: $O((dP)d^3)$}
			\If{$P(A_j) = 0$}
			\State $flag \gets \mathrm{true}$
			\Else
			\State Calculer $U(A_j)$
			\State Calculer $P(A_j)U(A_j)$ \Comment{Évaluation: $O((dP)d^3)$}
			\State Faire $A_{j + 1} \gets A_j - P(A_j)U(A_j)$
			\EndIf
			\EndWhile
			\Return $D = A_j$ et $N = A - D$
		\end{algorithmic}
		\caption{Algorithme de la décomposition de Dunford}
		\label{algo:Brlekamp}
	\end{algorithm}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
	\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}