\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\Cun}{\mathcal{C}^1}
	\newcommand{\Cdeux}{\mathcal{C}^2}
	\newcommand{\Cinf}{\mathcal{C}^{\infty}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{OliveGreen}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	\newcommand{\textmagenta}[1]{\textcolor{DarkOrchid}{#1}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\newcommand{\pcite}[1]{\protect{\cite{#1}}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\newtheorem{tnum}{Théorème}
	\newtheorem{pnum}{Proposition}
	\newtheorem{cnum}{Corollaire}
	\newtheorem{lnum}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{remarque}{Remarque}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	
	\title{Méthode du gradient à pas optimal}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Mathématiques pures et appliquées \cite[p.412]{RamisWarusfel}.
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 162 (Systèmes d'équations linéaires); 219 (Extremum), 223 (Suites numériques), 229 (Fonctions monotones et convexes), 233 (Analyse numérique matricielle).
		}}
	
	\section{Introduction}
	
	La méthode du gradient est une méthode d'analyse numérique permettant de trouver un extremum (qui peut être local) d'une fonction à plusieurs variables sans contraintes. Même sans contraintes, l'optimisation multivariée (comme ici) est complexe numériquement au sens ou sa complexité peut très facilement exploser. 
	
	Il existe plusieurs variantes de cette méthode que l'on nomme descente de gradient. L'idée "magique" des méthodes de descente de gradient est de se ramené à un problème de minimisation sans contrainte à une variable (que l'on sait relativement bien traiter) à l'aide d'une suite minimisante unidirectionnelles. L'idée intuitive est que l'on va minimiser la fonction dans une unique direction. Le choix de cette direction (qui est le gradient de la fonction au point où on est) et combien de temps on l'exploite sont des arguments cruciaux de ces méthodes. En effet, selon leurs choix la convergence va pouvoir être plus ou moins puissante et rapide. Cependant, il faut prendre se rappeler de conserver des complexités raisonnables pour ces calculs.
	
	\section{Méthode du gradient à pas optimal}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement}}}{
		\begin{enumerate}
			\item Donner l'algorithme + illustration par un dessin.
			\item Montrer que $\forall k \in \N$, $\left< R_{k+1}, R_k \right> = 0$.
			\item Montrer la convergence de la suite $X_n$ vers $\overline{X}$.
			\begin{enumerate}
				\item Montrer que $\lVert X_{n + 1} - \overline{X} \rVert_A^2 = -\alpha_{n+1}\lVert R_n \rVert^2 + \lVert X_n - \overline{X} \rVert_A^2$.
				\item Montrer que $\lVert X_n - \overline{X} \rVert_A \leq \left(\frac{\mu_n - \mu_1}{\mu_n + \mu_1}\right)^n\lVert X_0 - \overline{X} \rVert_A$.
			\end{enumerate}
			\item Estimation de l'approximation.
		\end{enumerate}
	}
	
	\subsection{Application aux cas de la résolution d'un système linéaire}
	La méthode du gradient à pas optimal est une méthode de recherche d'extremum basé sur une descente de gradient. Cependant, en changeant le point de vue sur le système, on peut utiliser cette méthode pour trouver la solution.
	
	\begin{theo}[\protect{\cite[p.409~ex.4]{RieslerBoyer}}]
		Soit $A \in S_n^{++}(\R)$. Alors $A X_0 = B$ si et seulement si $X_0$ minimise l'application $f: \R^n \to \R$ qui à $X$ associe $\frac{1}{2} \left< AX, X\right> - \left< B, X\right>$.
	\end{theo} 
	\begin{proof}
		\begin{description}
			\item[$\Leftarrow$] Supposons que $AX_0 = B$ avec $X_0 \in \R^n$. Alors, $\forall H \in \R^n$, comme $A \in S_n^{++}(\R)$, 
			\begin{displaymath}
			\begin{array}{ccll}
			f(X_0 + H) & = & \frac{1}{2} \left< A(X_0 + H), (X_0 + H)\right> - \left< B, (X_0 + H)\right> & \textblue{\text{(Par définition de $f$)}} \\
			& = & \multicolumn{2}{l}{\frac{1}{2} \left< AX_0, X_0 \right> +\frac{1}{2} \left< HX_0, X_0 \right> + \frac{1}{2} \left< AX_0, H \right> +\frac{1}{2} \left< HX_0, H \right> - \left< B, X_0\right> - \left< H, X_0\right>} \\
			& & \multicolumn{2}{r}{\textblue{\text{(Manipulation du produit scalaire)}}} \\
			& = & f(X_0) + \left< AX_0, H\right> + \frac{1}{2} \left< AH, H\right> - \left< B, H\right> & \textblue{\text{(Par définition de $f$)}} \\
			& = & f(X_0) + \left<B, H\right> + \frac{1}{2} \left< AH, H\right> - \left< B, H\right> & \textblue{(B = AX_0)} \\
			& = & f(X_0) + \frac{1}{2} \left< AH, H\right> & \\
			& \geq  & f(X_0) & \textblue{\text{($\frac{1}{2} \left< AH, H\right> > 0$ ssi $H \neq 0$)}}\\
			\end{array}
			\end{displaymath}
			\item[$\Rightarrow$] Supposons que $X_0 \in \R^n$ minimise $f$.
			\begin{itemize}
				\item $\bigtriangledown f(X_0) = 0$ \textblue{(caractérisation des extremums)}
				\item $f(X + H) \to f(X) + \left< AX -B, H\right>$ quand $\lVert H\rVert \to 0$.
			\end{itemize}
			Ainsi, $\bigtriangledown f(X_0) = AX_0 -B = 0$ et $X_0$ est bien solution du système.
		\end{description}
	\end{proof}
	
	\begin{lnum}
		Toute matrice $A \in \Sigma_n^{++}(\R)$ de valeurs propres minimale et maximale $\mu_1$ et $\mu_n$, vérifie pour tout $X \in \R^n \setminus \{0\}$: 
		\begin{displaymath}
		\frac{\lVert X \rVert^4}{\lVert X \rVert^2_{A^{-1}}\lVert X \rVert^2_A} \geq 4 \frac{\mu_1\mu_n}{(\mu_1 + \mu_n)^2}
		\end{displaymath}
		\label{lem1}
	\end{lnum}
	\begin{proof}
		On se place dans une base orthonormée de $\R^n$ constitués des vecteurs propres de $A$ \textblue{(existe par $A \in \Sigma_n^{++}(\R)$)}. On étudie ensuite l'application $\phi(x) = \frac{\mu_n}{x} + \frac{x}{\mu_1}$.
	\end{proof}
	
	
	\subsection{Algorithme du gradient à pas optimal}
	
	On va donc étudier la convergence de l'algorithme du gradient à pas optimal (algorithme~\ref{algo:GradiantOptimal}) dans le cadre de fonction de ces fonctions définies à partir d'un système linéaire. Cet algorithme est un algorithme d'approximation: on se donne $\epsilon$ un critère d'arrêt qui détermine la précision de notre solution. Ce paramètre influe sur la complexité de la méthode puisque ni la convergence ni la vitesse de convergence n'est remise en cause par ce paramètre. De plus, en fonction du critère d'arrêt équivalent que l'on peut choisir, il nous faut garder en mémoire les deux derniers termes de la suite. La figure~\ref{fig:methode} est un exemple d'application de cette méthode.
	
	\noindent\begin{minipage}{.64\textwidth}
		\begin{algorithm}[H]
			\begin{algorithmic}[1]
				\Function \textrm{Gradient-optimal}($X_0 \in \R^n$, $R_0 = AX_0 - B$, $\epsilon \geq 0$)
				\While{$\frac{\lVert R_n \rVert}{\lVert R_0 \rVert} \leq \epsilon$} 
				\State $\alpha_{n+1} \gets \frac{\lVert R_n \rVert^2}{\left< AR_n, R_n \right>}$ \Comment{Pas conjugué}
				\State $X_{n+1} \gets X_n - \alpha_{n+1}R_n$ \Comment{Nouvelle valeur de $X$}
				\State $R_{k+1} \gets AX_n - B$ \Comment{Estimation de l'erreur}
				\EndWhile
				\EndFunction			
			\end{algorithmic}
			\caption{Méthode du gradient à pas optimal}
			\label{algo:GradiantOptimal}
		\end{algorithm}
	\end{minipage} \hfill
	\begin{minipage}{.3\textwidth}
		\begin{figure}[H]
			\centering
			\includegraphics[scale=.4]{./../Figures/MethodeGradientOptimal.png}
			\caption{Illustration de la méthode du gradient}
			\label{fig:methode}
		\end{figure}
	\end{minipage}
	
	\subsection{Correction de cet algorithme et estimation de l'erreur}

	\begin{theo}
		Soit $A \in S_n^{++}(\R)$ et $X_0 \in \R^N$. L'algorithme du gradient (algorithme~\ref{algo:GradiantOptimal}) converge vers la solution $\overline{X}$ du système $AX = B$ et on a pour tout $n \in \N$,
		\begin{displaymath}
		\lVert X_n - \overline{X} \rVert \leq \left(\frac{\mathrm{Cond}(A) - 1}{\mathrm{Cond}(A) + 1}\right)^n \sqrt{\mathrm{Cond}(A)} \lVert X_0 - \overline{X} \rVert
		\end{displaymath}
	\end{theo}
	\begin{proof}
		Soit $A \in S_n^{++}(\R)$ et $x_0 \in \R^N$. On note $\lVert X \rVert^2$ la norme définie par $\left<X, X \right>$ et $\lVert X \rVert_A^2$ la norme définie par $\left<AX, X \right>$ \textblue{(les propriétés sur le produit scalaire nous assure les propriétés de norme)}.
		
		\paragraph{Étape 1: montrons que $\forall k \in \N$, $\left< R_{k+1}, R_k \right> = 0$} Soit $k \in \N$.
		\begin{displaymath}
		\begin{array}{ccll}
		\left< R_{k+1}, R_k \right> & = & \left< AX_{k+1} - B, R_k \right> & \textblue{\text{(Définition de $R_{k+1}$)}} \\
		& = & \left< AX_k - B -\alpha_{k+1}AR_k, R_k \right> & \textblue{\text{(Définition de $X_{k+1}$)}} \\
		& = & \left< AX_k - B, R_k\right> -\alpha_{k+1}\left<AR_k, R_k \right> & \textblue{\text{(Manipulation du produit scalaire)}} \\
		& = & \left< R_k, R_k\right> -\alpha_{k+1}\left<AR_k, R_k \right> & \textblue{\text{(Définition de $R_k$)}} \\
		& = & \lVert R_k \rVert^2 -\frac{\lVert R_k \rVert^2}{\lVert R_k\rVert_A^2}\lVert R_k\rVert_A^2 & \textblue{\text{(Définition des normes et de $\alpha_{k+1}$)}} \\
		& = & \lVert R_k \rVert^2 -\lVert R_k \rVert^2 & \textblue{} \\
		& = & 0 & \textblue{} \\
		\end{array}
		\end{displaymath}
		
		\paragraph{Étape 2: montrons la convergence de la suite $X_n$ vers $\overline{X}$}
		
		\subparagraph{Étape a : montrons que $\lVert X_{n + 1} - \overline{X} \rVert_A^2 = -\alpha_{n+1}\lVert R_n \rVert^2 + \lVert X_n - \overline{X} \rVert_A^2$}
		\begin{displaymath}
		\begin{array}{ccll}
		\lVert X_{n + 1} - \overline{X} \rVert_A^2 & = & \left< A(X_{n+1} - \overline{X}), X_{n + 1} - \overline{X} \right> & \textblue{\text{(Définition de la norme)}} \\
		& = &  \left< A(X_{n+1} - \overline{X}), X_{n + 1} - X_n + X_n - \overline{X} \right> & \textblue{\text{(Introduction de $X_n$)}} \\
		& = & \left< A(X_{n+1} - \overline{X}), X_{n + 1} - X_n\right> +  \left< A(X_{n+1} - \overline{X}), X_n - \overline{X} \right> & \textblue{\text{(Manipulation du produit scalaire)}} \\
		& = & \left< A(X_{n+1} - \overline{X}), -\alpha_{n + 1}R_n\right> +  \left< A(X_{n+1} - \overline{X}), X_n - \overline{X} \right> & \textblue{\text{(Définition de $\alpha_{n + 1}R_n$)}} \\
		& = & \left< AX_{n+1} - B, -\alpha_{n + 1}R_n\right> +  \left< A(X_{n+1} - \overline{X}), X_n - \overline{X} \right> & \textblue{\text{($\overline{X}$ est solution du système)}} \\
		& = & \left< R_{n+1}, -\alpha_{n + 1}R_n\right> +  \left< A(X_{n+1} - \overline{X}), X_n - \overline{X} \right> & \textblue{\text{(Définition de $R_n$)}} \\
		& = & -\alpha_{n + 1}\left< R_{n+1}, R_n\right> +  \left< A(X_{n+1} - \overline{X}), X_n - \overline{X} \right> & \textblue{\text{(Manipulation du produit scalaire)}} \\
		& = & \left< A(X_{n+1} - \overline{X}), X_n - \overline{X} \right> & \textblue{\text{(Étape 1)}} \\
		& = & \left< A(X_n - \alpha_{n+1}R_n - \overline{X}), X_n - \overline{X} \right> & \textblue{\text{(Définition de $X_{n+1}$)}} \\
		& = & \left< A(X_n - \overline{X}), X_n - \overline{X} \right> - \alpha_{n+1}\left<AR_n , X_n - \overline{X} \right> & \textblue{\text{(Manipulation du produit scalaire)}} \\
		& = & \lVert X_n - \overline{X}\rVert_A^2 - \alpha_{n+1}\left< R_n , A(X_n - \overline{X}) \right> & \textblue{\text{(Manipulation du produit scalaire)}} \\
		& = & \lVert X_n - \overline{X}\rVert_A^2 - \alpha_{n+1}\left< R_n , AX_n - B \right> & \textblue{\text{($\overline{X}$ est solution du système)}} \\
		& = & \lVert X_n - \overline{X}\rVert_A^2 - \alpha_{n+1}\left< R_n , R_n \right> & \textblue{\text{(Définition de $R_n$)}} \\
		& = & \lVert X_n - \overline{X}\rVert_A^2 - \alpha_{n+1}\lVert R_n\rVert^2 & \textblue{\text{(Définition de la norme)}} \\
		\end{array}
		\end{displaymath}
		
		\subparagraph{Étape b : montrons que $\lVert X_n - \overline{X} \rVert_A \leq \left(\frac{\mu_n - \mu_1}{\mu_n + \mu_1}\right)^n\lVert X_0 - \overline{X} \rVert_A$} On a \cite[p.413,~ex.7]{RieslerBoyer},
		\begin{displaymath}
		\begin{array}{ccll}
		\lVert X_n - \overline{X}\rVert_A^2 & = & \left< A(X_n - \overline{X}), X_n - \overline{X} \right> & \textblue{\text{(Définition de la norme)}} \\
		& = & \left< A(X_n - \overline{X}), A^{-1}(A(X_n - \overline{X})) \right> & \textblue{\text{(Introduction de $A$)}} \\
		& = & \left< AX_n - B, A^{-1}(AX_n - B)\right> & \textblue{\text{($\overline{X}$ est solution du système)}} \\
		& = & \left< R_n, A^{-1}(R_n)\right> & \textblue{\text{(Définition de $R_n$)}} \\
		& = & \lVert R_n \rVert_{A^{-1}}^2  & \textblue{\text{(Définition de la norme)}} \\
		\end{array}
		\end{displaymath}
		Ainsi,
		\begin{displaymath}
		\begin{array}{ccll}
		\lVert X_{n+1} - \overline{X}\rVert_A^2 & = & \lVert R_n \rVert_{A^{-1}}^2 - \alpha_{n+1}\lVert R_n\rVert^2 & \textblue{\text{(Par ce qui précède)}} \\
		& = & \lVert R_n \rVert_{A^{-1}}^2 - \frac{\lVert R_n\rVert^4}{\lVert R_n\rVert_A^2} & \textblue{\text{(Définition de $\alpha_{n + 1}$)}} \\
		& = & \lVert R_n \rVert_{A^{-1}}^2\left(1 - \frac{\lVert R_n\rVert^4}{\lVert R_n\rVert_A^2\lVert R_n \rVert_{A^{-1}}^2}\right) & \textblue{\text{(En factorisant)}} \\
		& = & \lVert X_n - \overline{X} \rVert_A^2\left(1 - \frac{\lVert R_n\rVert^4}{\lVert R_n\rVert_A^2\lVert R_n \rVert_{A^{-1}}^2}\right) & \textblue{\text{(Par ce qui précède)}} \\
		& \leq &  \lVert X_n - \overline{X} \rVert_A^2\left(1 - \frac{4\mu_1\mu_n}{(\mu_1\mu_n)^2}\right) & \textblue{(A \in S_n^{++}(\R))} \\
		& \leq &  \frac{(\mu_n - \mu_1)^2}{(\mu_1 + \mu_n)^2}\lVert X_n - \overline{X} \rVert_A^2 & \textblue{\text{(Par le lemme~\ref{lem1})}} \\
		\end{array}
		\end{displaymath}
		En passant à la racine carré et en itérant, on obtient $\lVert X_n - \overline{X}\rVert_A \leq \left(\frac{\mu_n - \mu_1}{\mu_1 + \mu_n}\right)^n\lVert X_0 - \overline{X} \rVert_A$. En remarquant que $0 < \frac{\mu_n - \mu_1}{\mu_1 + \mu_n} < 1$, on a bien la convergence de la suite.
		
		\paragraph{Étape 3 : estimons l'approximation} De plus, pour tout $X = \sum x_ie_i \in \R^n$ exprimé dans une base de vecteurs propres de $A$, on a:
		\begin{displaymath}
		\mu_1\lVert X \rVert^2 = \sum \mu_1x_i^2 \leq \lVert X \rVert_A^2 = \sum \mu_ix_i^2 \leq  \sum \mu_nx_i^2 = \mu_n\lVert X \rVert^2
		\end{displaymath}
		Alors, en appliquant les inégalités précédentes, on obtient:
		\begin{displaymath}
		\lVert X_n - \overline{X} \rVert \leq  \frac{1}{\sqrt{\mu_1}} \lVert X_n - \overline{X} \rVert_A \leq  \frac{1}{\sqrt{\mu_1}}\left(\frac{\mu_n - \mu_1}{\mu_1 + \mu_n}\right)^n\lVert X_0 - \overline{X} \rVert_A \leq \sqrt{\frac{\mu_n}{\mu_1}}\left(\frac{\mu_n - \mu_1}{\mu_1 + \mu_n}\right)^n\lVert X_0 - \overline{X} \rVert
		\end{displaymath}
		Or, $\mathrm{Cond}(A) = \frac{\mu_n}{\mu_1}$, d'où $\sqrt{\frac{\mu_n}{\mu_1}}\left(\frac{\mu_n - \mu_1}{\mu_1 + \mu_n}\right) = \sqrt{\mathrm{Cond}(A)}\left(\frac{\mathrm{Cond}(A) - 1}{\mathrm{Cond}(A) + 1}\right)$. D'où le résultat.
	\end{proof}
	
	
	\section{Complément: Méthode du gradient à pas optimal via les suites stationnarisantes}
	
	\input{./../Notions/Opti_gradient-optiStationnaire.tex}
	
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}

\end{document}