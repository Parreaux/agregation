\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\Cun}{\mathcal{C}^1}
	\newcommand{\Cdeux}{\mathcal{C}^2}
	\newcommand{\Cinf}{\mathcal{C}^{\infty}}
	
	\sloppy
	
	\newcommand{\textblue}{\textcolor{blue}}
	\newcommand{\textgreen}{\textcolor{OliveGreen}}
	\newcommand{\textred}{\textcolor{red}}
	\newcommand{\textbrown}{\textcolor{Brown}}
	\newcommand{\textmagenta}{\textcolor{DarkOrchid}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{lemme}{Lemme}
	\newtheorem{lnum}{Lemme}
	\newtheorem*{appli}{Application}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Formule sommatoire de Poisson}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Gourdon \cite[p.272]{Gourdon-analyse} et 40 développements en analyse \cite[p.253]{BernisBernis}.
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 224 (Développement asymptotique); 236 (Calcul d'intégrale); 246 (Série de Fourier); 250 (Transformée de Fourier); 265 (Fonctions usuelles et spéciales).
		}}
	
	\section{Introduction}
	
	La formule sommatoire de Poisson est une formule nous permettant de développer des séries de Fourier pour certaines fonctions. On a ainsi une application de la formule aux fonctions spéciales comme la fonction thêta de Jacobi. On a aussi une application à la théorie du signal avec le théorème de Shannon. Il permet de garantir qu'il n'y a aucune perte d'information lors de l'échantillonnage à au loin au double de sa fréquence maximum. En pratique, on enlève les trop grandes fréquences puis on échantillonne à $2F$. Si on ne respecte pas ces principe, on s'expose à un recouvrement qui peut ne pas bien se passer.
	
	\titlebox{red}{\textred{\textbf{Schéma du développement (\textbf{leçons 224, 246, 265})}}}{
		Formule sommatoire de Poisson et application à la fonctions thêta de Jacobi.
		\begin{enumerate}
			\item Montrer que $\sum_{n \in \Z} f(x+n)$ converge simplement.
			\item Montrer que $F$ est de classe $\Cun$ et est $1-$périodique.
			\item Calcul de la série de Fourier de $F$.
			\item Application à la fonction $\Theta$ de Jacobi \textblue{(Le lemme est admis)}.
		\end{enumerate}
	}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement (leçons 236)}}}{
		Formule sommatoire de Poisson et théorème de Shannon.
		\begin{enumerate}
			\item Dire que $\sum_{n \in \Z} f(x+n)$ converge simplement.
			\item Dire que $F$ est de classe $\Cun$ et est $1-$périodique.
			\item Calcul de la série de Fourier de $F$.
			\item Preuve du lemme.
			\item Application à Shannon.
		\end{enumerate}
	}
	
	\section{Formule sommatoire de Poisson et fonctions thêta de Jacobi}

	\begin{theo}
		Soit $f: \R \to \C$ une fonction de classe $\Cun$ vérifiant $f(x) = O\left(\frac{1}{x^2}\right)$ et $f'(x) = O\left(\frac{1}{x^2}\right)$ lorsque $|x| \to + \infty$. Alors,
		\begin{displaymath}
		\begin{array}{cccc}
		\forall x \in \R & \sum_{n \in \Z} f(x + n) = \sum_{n \in \Z} \widehat{f}(n)e^{2i\pi nx} & \text{où} & \forall n \in \Z, \widehat{f}(n) = \int_{\R} f(t)e^{-2i\pi nt} dt
		\end{array}
		\end{displaymath}
	\end{theo}
	
	\begin{proof}
		Soit $f: \R \to \C$ une fonction de classe $\Cun$ vérifiant $f(x) = O\left(\frac{1}{x^2}\right)$ et $f'(x) = O\left(\frac{1}{x^2}\right)$ lorsque $|x| \to + \infty$.
		
		\paragraph{Étape 1: montrons que $\sum_{n \in \Z} f(x+n)$ converge simplement.} On va montrer qu'elle converge normalement sur tout segment de $\R$. Soit $M > 0$ telle que $\left|f(x)\right| \leq \frac{M}{x^2}$ pour $\left|x\right| \geq 1$, on écrit alors $\forall K >0$.
		\begin{displaymath}
		\forall x \in [-K, K], \forall n \in \Z, |x| > K+1, \left|f(x+n)\right| \underbrace{\leq}_{\text{hypothèse sur }M} \frac{M}{(x+n)^2} \underbrace{\leq}_{-K + n \leq x+n} \frac{M}{(|n| - K)^2}
		\end{displaymath}
		
		En effet, soit $n \in \Z$, on distingue alors deux cas.
		\begin{description}
			\item[Cas $n \geq 0$] $\left|f(x+n)\right| \leq \frac{M}{(x+n)^2} \leq \frac{M}{(n - K)^2}$ avec $n > K+1 \Leftrightarrow 1 < n-K$.
			
			\begin{tabular}{cl}
				$-K \leq x \leq K$ & \textblue{(hypothèse)} \\
				$0 \leq 1 < n-K \leq n + x \leq n+ K$ & \textblue{(addition de $n$)} \\
				$(n-K)^2 \leq (n+x)^2$ & \textblue{(croissance de $x^2$)} \\
				$\frac{1}{(n-K)^2} \geq \frac{1}{(n+x)^2}$ & \textblue{(décroissance de $\frac{1}{x}$)} \\
			\end{tabular}
			
			\item[Cas $n \leq 0$] $\left|f(x+n)\right| \leq \frac{M}{(x+n)^2} \leq \frac{M}{(-n - K)^2}$ avec $-n > K+1 \Leftrightarrow -K -1 < n \leq 0$.
			
			\begin{tabular}{cl}
				$-K \leq x \leq K$ & \textblue{(hypothèse)} \\
				$0 \leq 1 < n-K \leq n + x \leq n+ K$ & \textblue{(addition de $n$)} \\
				$(n+K)^2 \leq (n+x)^2$ & \textblue{(décroissance de $x^2$)} \\
				$\frac{1}{(n+x)^2} \leq \frac{1}{(n+K)^2}$ & \textblue{(décroissance de $\frac{1}{x}$)} \\
			\end{tabular}
			
			Donc la série $\sum_{n\in \Z} f(x +n)$ converge simplement vers $F$ sa limite simple.
		\end{description}
		
		\paragraph{Étape 2: montrons que $F$ est de classe $\Cun$ et est $1-$périodique.}
		\begin{description}
			\item[$F$ est de classe $\Cun$] De même que précédemment, $\sum_{n\in \Z} f'(x +n)$ converge uniformément sur tout segment de $\R$. Par le théorème de dérivation sur les suites de fonctions, $F$ est de classe $\Cun$.
			
			\item[$F$ est $1-$périodique] Soit $x \in \R$, $\forall n \in \N$, $\sum_{n = -N}^{N} f(x+1 + n) = \sum_{n = -N-1}^{N+1} f(x + n)$ \textblue{(par changement d'indice $n = n+1$)}. On conclut en passant à la limite, on en déduit que $F(x+1) = F(x)$.
		\end{description}
		
		\paragraph{Étape 3: calcul de la série de Fourier de $F$.} Soit $n \in \Z$, calculons les coefficients de Fourier.
		
		\begin{tabular}{ccll}
			$c_n\left(F\right)$ & $=$ & $\int_{0}^{1} F(t) e^{-2i\pi nt} dt$ & \textblue{(définition des coefficients)} \\
			& $=$ & $\int_{0}^{1} \sum_{m = -\infty}^{\infty} f(t+m) e^{-2i\pi nt} dt$ & \textblue{(définition de $F$ comme limite de la somme)} \\
			& $=$ & $\sum_{m = -\infty}^{\infty} \int_{0}^{1} f(t+m) e^{-2i\pi nt} dt$ & \textblue{(interversion car on a les convergences absolues)} \\
			& $=$ & $\sum_{m = -\infty}^{\infty} \int_{m}^{m+1} f(t) e^{-2i\pi nt} dt$ & \textblue{(changement de variable $t = t+m$)} \\
			& $=$ & $\int_{-\infty}^{\infty} f(t) e^{-2i\pi nt} dt$ & \textblue{(on "recolle" les intégrales)} \\
			& $=$ & $\widehat{f}(n)$ & \textblue{(définition)} \\
		\end{tabular}
		Comme $F$ est de classe $\Cun$ et $1$-périodique, le théorème de convergence normale de Dirichlet nous assure que pour tout $x \in \R$, $F = \sum_{n \in \N}\widehat{f}(n)e^{2i\pi nx}$ qui est bien la série de  Fourier. D'où la formule sommatoire de Poisson.
	\end{proof}
	
	\paragraph{Étape 4: application à la fonction $\Theta$ de Jacobi.} Pour l'application, on va avoir besoin d'un lemme \cite[p.164, ex~4.3]{Gourdon-analyse} afin d'effectuer les calculs nécessaires.
	
	\begin{lnum}
		Pour tout $x \geq 0$, on a 
		\begin{displaymath}
		I(x) = \int_{-\infty}^{\infty} e^{-t^2}e^{2i\pi tx}dt = \sqrt{\pi}e^{-\pi^2x^2}
		\end{displaymath}
		\label{lem:calcul}
	\end{lnum}
	\begin{proof}
		Par le théorème de dérivation sous le signe intégrable, on montre que $I$ est dérivable et que
		\begin{displaymath}
		\begin{array}{cc}
		\forall x \in \R_+, &  I'(x) = 2i\pi\int_{-\infty}^{\infty} te^{-t^2}e^{2i\pi tx}dt
		\end{array}
		\end{displaymath}
		Avec une intégration par partie, on obtient que
		\begin{displaymath}
		\begin{array}{cc}
		\forall x \in \R_+, &  I(x) = \underbrace{\left[\frac{1}{2i\pi x}e^{-t^2}e^{2i\pi tx}\right]_{-\infty}^{\infty}}_{= 0} + \frac{1}{2i\pi x} \int_{-\infty}^{\infty} 2te^{-t^2}e^{2i\pi tx}dt = \frac{1}{i\pi x}\frac{1}{2i\pi}I'(x)
		\end{array}
		\end{displaymath}
		En résolvant cette équation différentielle vérifié par $I$, on trouve
		\begin{displaymath}
		\begin{array}{cc}
		\forall x \in \R_+, &  I(x) = \sqrt{\pi}e^{-\pi^2x^2}
		\end{array}
		\end{displaymath}
	\end{proof}
	
	\begin{appli}[Application de la fonction $\Theta$ de Jacobi]
		On pose $\theta(x)= \sum_{n \in \Z} x^{n^2}$ et 
		\begin{displaymath}
		\begin{array}{cccl}
		\Theta : &]0, + \infty[ & \to &\R \\
		& x & \mapsto & \sum_{n \in \Z} e^{-\pi n^2 x} = \theta\left(e^{-\pi x}\right)
		\end{array}
		\end{displaymath}
		Alors, $\forall x > 0$, $\Theta(x) = \frac{1}{\sqrt{x}}\Theta\left(\frac{1}{x}\right)$.
	\end{appli}
	\begin{proof}
		Soit $\alpha > 0$. On souhaite appliquer la formule sommatoire de Poisson à la fonction $f : x \mapsto e^{\alpha x^2}$. Les coefficients de Fourier de $\widehat{f}(n)$ sont donnés par, $\forall n \in \Z$,
		\begin{displaymath}
		\widehat{f}(n) = \int_{-\infty}^{\infty}e^{-\alpha t^2}e^{-2i\pi nt} dt \underbrace{=}_{t = \frac{u}{\sqrt{\alpha}}} \frac{1}{\sqrt{\alpha}}\int_{-\infty}^{\infty}e^{-u^2}e^{-2i\pi nu/\sqrt{\alpha}} dt \underbrace{=}_{\text{lemme~\ref{lem:calcul}}} \sqrt{\frac{\pi}{\alpha}}e^{-\pi^2n^2/\alpha}
		\end{displaymath}
		En appliquant la formule de Poisson avec $x =0$, on en déduit
		\begin{displaymath}
		\sum_{n \in \N} e^{-\alpha n^2} = \sqrt{\frac{\pi}{\alpha}} \sum_{n \in \N} e^{-\pi^2n^2/\alpha}
		\end{displaymath}
		Ceci est vrai $\forall \alpha >0$ et en effectuant le changement de variable $\alpha = \pi x$, on obtient le résultat désiré. 
	\end{proof}
	
	\section{Formule sommatoire de Poisson et théorème de Shannon \cite[p.253]{BernisBernis}}
	
	\begin{theo}
		Soit $f$ un élément de $\mathcal{S}(\R)$. Alors,
		\begin{displaymath}
		\begin{array}{cccc}
		\forall t \in \R & \sum_{n \in \Z} f(t + 2\pi n) = \sum_{n \in \Z} \widehat{f}(n)e^{2i\pi t} & \text{où} & \forall n \in \Z, \widehat{f}(n) = \int_{\R} f(t)e^{-int} dt
		\end{array}
		\end{displaymath}
	\end{theo}
	
	\begin{proof}
		Soit $f \in \mathcal{S}(\R)$.
		
		\paragraph{Étape 1: montrons que $\sum_{n \in \Z} f(.+2n\pi)$ et sa dérivée converge normalement sur tout compact.} Soit $K$ un compact de $\R$ et $N \in \N^*$ tel que $K \subset \left[-N, N\right]$. Soit $i \in \{0, 1\}$. Comme $f \in \mathcal{S}(\R)$, il existe $M_i \in \R_+^*$ tel que $\forall t \in \R$
		\begin{displaymath}
		\left|f^{(i)}(t)\right| \leq \frac{M_i}{1 + t^2}
		\end{displaymath}
		\textblue{$f$ et $f'$ sont à décroissance rapide donc $\exists M_i$ tel que $\left|f^{(i)}(t)x^{\alpha}\right| \leq M_i$. De plus, sur $K$ il existe une borne et on se protège du cas $0$. En dehors du compact, c'est donné par la décroissance rapide de $f$ et $f'$.}
		
		Pour tout $t \in K$ et $n \in \Z$ tel que $\left|n\right| \geq N + 1$, on a alors:
		\begin{displaymath}
		\left|f^{(i)}(t + 2\pi n)\right| \leq \frac{M_i}{1 + (t + 2n\pi)^2} \leq \frac{M_i}{\left(\left|n\right| - N\right)^2}
		\end{displaymath}
		\textblue{Arguments de la preuve:
			\begin{description}
				\item[$\left|f^{(i)}(t + 2\pi n)\right| \leq \frac{M_i}{1 + (t + 2n\pi)^2}$] remplace $t$ dans la précédente intégrale
				\item[$\frac{M_i}{1 + (t + 2n\pi)^2} \leq \frac{M_i}{\left(\left|n\right| - N\right)^2}$] on a pour $t \in K$
				\begin{displaymath}
				\begin{array}{cccccl}
				- N & \leq & t & \leq & N & \text{($t \in K$)} \\
				- N + \left|n\right| & \leq & t + \left|n\right| & \leq & N + \left|n\right| & \text{(Ajout de $\left|n\right|$)} \\
				\left(- N + \left|n\right|\right)^2 & \leq & \left(t + \left|n\right|\right)^2 & \leq & \left(N + \left|n\right|\right)^2 & \text{(Croissance de $x \mapsto x^2$)} \\
				\left(- N + \left|n\right|\right)^2 & \leq & \left(t + \left|n\right|\right)^2 + 1  & \multicolumn{3}{l}{\text{(Ajout de $1$ à droite)}} \\
				\frac{1}{\left(- N + \left|n\right|\right)^2} & \geq & \frac{1}{\left(t + \left|n\right|\right)^2 + 1}  & \multicolumn{3}{l}{\text{(Décroissance de $x \mapsto \frac{1}{x}$)}} \\
				\frac{M_i}{\left(- N + \left|n\right|\right)^2} & \geq & \frac{M_i}{\left(t + \left|n\right|\right)^2 + 1}  & \multicolumn{3}{l}{\text{(Multiplication par $M_i$ constante positive)}} \\
				\end{array}
				\end{displaymath}
		\end{description}}
		Or la série $\sum_{\left|n\right| \geq N + 1}  \frac{1}{\left(\left|n\right| - N\right)^2}$ converge \textblue{(série de Riemann $\alpha = 2$)}, donc la série $\sum_{n \in \Z} f^{(i)}(2n\pi + .)$ converge normalement sur $K$. Notons $g$ cette série \textblue{(existence donnée par la convergence normale)}.
		
		\paragraph{Étape 2: étude de $g$.}
		\begin{description}
			\item[$F$ est de classe $\Cun$] 
			\begin{itemize}
				\item $f(2n\pi + .) \in \mathcal{C}^1$ \textblue{($f \in \mathcal{S}(\R)$)}
				\item $\sum_{n \in \Z} f(2n\pi + .)$ converge simplement
				\item $\sum_{n \in \Z} f'(2n\pi + .)$ converge simplement
			\end{itemize}
			Par le théorème de dérivation sous le signe somme, $g \in \Cun$.
			
			\item[$F$ est $2\pi-$périodique] Soit $x \in \R$, $\forall n \in \N$, $g(x + 2\pi) = \sum_{n \in \Z} f(x + n2\pi + 2\pi)$ \textblue{(par définition de $g$)}. Par un changement d'indice $m = n + 1$, $g(x+1 + n2\pi + 2\pi) = \sum_{m \in \Z} f(x + m2\pi) = g(x)$ \textblue{(par définition de $g$)}.
		\end{description}
		
		\paragraph{Étape 3: calcul de la série de Fourier de $g$.} 
		\begin{itemize}
			\item Soit $n \in \Z$, calculons les coefficients de Fourier.
		
			\begin{tabular}{ccll}
				$c_n\left(F\right)$ & $=$ & $\int_{-\pi}^{\pi} g(t) e^{-int} dt$ & \textblue{(Définition des coefficients)} \\
				& $=$ & $\int_{-\pi}^{\pi} \sum_{m = -\infty}^{\infty} f(t+2m\pi) e^{-int} dt$ & \textblue{(Définition de $g$ comme limite de la somme)} \\
				& $=$ & $\sum_{m = -\infty}^{\infty} \int_{-\pi}^{\pi} f(t+2m\pi) e^{-int} dt$ & \textblue{(Interversion car on a les convergences normales} \\
			\end{tabular}
			
			\item Soit $N \in \N^*$
			
			\begin{tabular}{ccll}
				$\sum_{n = -N}^{N} \int_{-\pi}^{\pi} f(t+2m\pi) e^{-int} dt$ & $=$ & $\sum_{n = -N}^{N} \int_{n-\pi}^{n+\pi} f(x\pi) e^{-in(x - 2\pi)} dt$ & \textblue{(Changement de variables $x = t + 2\pi$)} \\
				& $=$ & $\int_{-N-\pi}^{N+\pi} f(x\pi) e^{-in(x - 2\pi)} dt$ & \textblue{(Recollement des intégrales)} \\
			\end{tabular}
			En prenant la limite, on obtient $c_k(g) = \widehat{f}(k)$ lorsque $N \to + \infty$.
			
			\item Comme $g \in \mathcal{C}^1$ et $2\pi$-périodique, le théorème de convergence normale de Dirichlet assure que pour tout $t \in \R$, d'où la formule sommatoire de Poisson.
		\end{itemize} 
	\end{proof}
	
	\paragraph{Étape 4: application au théorème de Shannon}
	\begin{appli}
		Soit $f \in \mathcal{S}(\R)$ telle que le support de $\widehat{f}$ est inclus dans $\left[-F, F\right]$ où $F \in \R^*_+$. Si $2F \leq 1$, alors pour tout réel $t$,
		\begin{displaymath}
		f(t) = \sum_{n = -\infty}^{+\infty} \frac{\sin((n-t)\pi)}{(n-t)\pi}
		\end{displaymath}
	\end{appli}
	\begin{proof}
		Supposons que le support de $\widehat{f}$ est inclus dans $\left[-F, F\right]$ où $2F \leq 1$.
		\begin{itemize}
			\item Comme la transformée de Fourier est une bijection de classe de Schwartz sur elle-même, $\widehat{f} \in \mathcal{S}(\R)$. On peut donc appliquer la formule sommatoire de Poisson: pour tout $\xi \in \R$
			\begin{displaymath}
			\sum_{n \in \Z} \widehat{f}(t + 2\pi n) = \sum_{n \in \Z} \widehat{\widehat{f}}(n)e^{2i\pi t}
			\end{displaymath}
			\item En appliquant la formule d'inversion de Fourier \textblue{(deux fois)}, $\widehat{\widehat{f}}(n) = f(-n)$, et 
			\begin{displaymath}
			\sum_{n \in \Z} \widehat{f}(\xi + 2\pi n) = \sum_{n \in \Z} f(-n)e^{-int}
			\end{displaymath}
			Mais la condition $2F \leq 1$, assure que $\forall k \in \Z^*$, $\widehat{f}(2\pi k + .)$ est nulle sur $\left[-\frac{1}{2}, \frac{1}{2}\right]$ et donc 
			\begin{displaymath}
			\widehat{f} = \mathbb{1}_{\left[-\frac{1}{2}, \frac{1}{2}\right]}\sum_{n \in \Z} \widehat{f}(. + 2\pi n) = \mathbb{1}_{\left[-\frac{1}{2}, \frac{1}{2}\right]}\sum_{n \in \Z} f(-n)e^{-int}
			\end{displaymath}
			\item Soit $t \in \R$, la formule d'inversion de Fourier dans $\mathcal{S}(\R)$ donne 
			\begin{displaymath}
			\begin{array}{ccll}
			f(t) & = & \int_{-\infty}^{+\infty}\left(\mathbb{1}_{\left[-\frac{1}{2}, \frac{1}{2}\right]}(\xi)\sum_{n \in \Z} \left(f(-n)e^{-i(t + n)\xi}\right)e^{-in\xi} d\xi\right) & \textblue{\text{(Inversion de Fourier)}} \\
			& = & \int_{-\frac{1}{2}}^{+\frac{1}{2}}\sum_{n \in \Z} \left(f(-n)e^{-in\xi}\right)e^{-in\xi} d\xi& \textblue{\text{(Support)}} \\
			& = & \int_{-\frac{1}{2}}^{+\frac{1}{2}}\sum_{m \in \Z} \left(f(m)e^{-i(t - m)\xi}\right)e^{-in\xi} d\xi& \textblue{\text{(Changement de variable $m = -n$)}} \\
			\end{array}
			\end{displaymath}
			Or la série $\sum_{m \in \Z} \left(f(m)e^{-i(t - m).}\right)$ converge normalement donc uniformément sur $\left[-\frac{1}{2}, \frac{1}{2}\right]$: $\forall n \in \Z$,
			\begin{displaymath}
			\lVert f(m)e^{-i(t - m).} \rVert_{\infty} = \left|f(n)\right|
			\end{displaymath}
			et la série $\sum_{m \in \Z} \left|f(m)\right|$ converge par convergence normale de $\sum_{m \in \Z} f(2\pi m + .)$. Donc
			\begin{displaymath}
			f(t) = \sum_{m = -\infty}^{+\infty} f(m) \int_{-\frac{1}{2}}^{\frac{1}{2}} e^{i(t - m)\xi} d\xi =  \sum_{n = -\infty}^{+\infty} \frac{\sin((n-t)\pi)}{(n-t)\pi}
			\end{displaymath}
		\end{itemize}
	\end{proof}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}