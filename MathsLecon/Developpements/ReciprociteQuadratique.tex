\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
} 

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\F}{\mathbb{F}}
	\newcommand{\Cun}{\mathcal{C}^1}
	\newcommand{\Cdeux}{\mathcal{C}^2}
	\newcommand{\Cinf}{\mathcal{C}^{\infty}}
	\newcommand{\Sn}{\mathfrak{S}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{OliveGreen}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	\newcommand{\textmagenta}[1]{\textcolor{DarkOrchid}{#1}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{lemme}{Lemme}
	\newtheorem{lnum}{Lemme}
	\newtheorem*{cor}{Corollaire}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	
	\title{Loi de réciprocité quadratique}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} H2G2 \cite[p.182]{Caldero-Germoni}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 120 (Anneaux $\Z/n\Z$); 121 (Nombres premiers); 123 (Corps finis); 126 (Équation en arithmétique); 170 (Formes quadratiques).
		}}
	
	\section{Introduction}
	
	La loi de réciprocité quadratique nous permet de caractériser les carrés dans $\F_q$. Savoir si un nombre est un carré modulo $q$ est un problème difficile puisque la moitié des éléments de $\F_q$ sont des carrés. En effet, la méthode naïve consiste à tester tous les éléments afin de savoir s'ils sont ou non un carré. La loi de réciprocité quadratique nous donne un procédé calculatoire pour savoir si un nombre est un carré modulo $q$.
	
	\section{La loi de réciprocité quadratique via les formes quadratiques}
	
	On commence par donnée une définition importante: on définit le symbole de Legendre.
	\begin{definition}
		Pour $p$ premier impair et $a$ un élément de $\F_p$, on définit le symbole de Legendre de $a$ par
		\begin{displaymath}
		\left(\frac{a}{p}\right) = \left \{ \begin{array}{c @{\text{ si }} l}
		1 & a \text{ est un carré dans } \F_p^*\\
		-1 & a \text{ n'est pas un carré dans } \F_p^* \\
		0 & a \equiv 0 [p]\\
		\end{array}\right.
		\end{displaymath}
	\end{definition}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement (leçons 123 et 126)}}}{
		\begin{enumerate}
			\item Montrer le lemme.
			\begin{enumerate}
				\item Montrer que $a$ est un carré modulo $q$ si et seulement si $a^{-1}$ en est un.
				\item Montrer que $a$ est un carré modulo $q$ si et seulement si $aX^{2} - 1 \in \F_q[X]$ possède deux racines distinctes.
				\item Dénombrer l'ensemble $\left|\{x \in \F_q, ax^2 = 1 \}\right|$ en fonction de si $a$ est ou non un carré dans $\F_q$.
			\end{enumerate}
			\item Montrer à l'aide d'action de groupe que $\left|X\right| = 1 + \left(\frac{p}{q}\right) [p]$.
			\item Montrer à l'aide de forme quadratique que $|X| = q^d\left(q^d + (-1)^{\frac{p-1}{2}\frac{q-1}{2}}\right)$.
			\begin{enumerate}
				\item Dire que les deux formes quadratiques sont équivalentes.
				\item Dénombrer les éléments de $X'$.
				\item En déduire le cardinal de $X$.
			\end{enumerate}
			\item Conclure sur le cardinal de $X$.
		\end{enumerate}
	}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement (leçons 120, 121 et 170)}}}{
		\begin{enumerate}
			\item Énoncer le lemme.
			\item Montrer à l'aide d'action de groupe que $\left|X\right| = 1 + \left(\frac{p}{q}\right) [p]$.
			\item Montrer à l'aide de forme quadratique que $|X| = q^d\left(q^d + (-1)^{\frac{p-1}{2}\frac{q-1}{2}}\right)$.
			\begin{enumerate}
				\item Montrer que les deux formes quadratiques sont équivalentes.
				\item Dénombrer les éléments de $X'$.
				\item En déduire le cardinal de $X$.
			\end{enumerate}
			\item Conclure sur le cardinal de $X$.
		\end{enumerate}
	}
	
	On donne maintenant un lemme qui va nous être utile tout au long de la démonstration (\textblue{on n'est pas obliger de le démontrer à l'oral mais le faire pour les leçons 123 et 126}). 
	\begin{lnum}
		Soit $a$ un entier non nul et $q$ un nombre premier impaire. Alors, 
		\begin{displaymath}
		\left|\{x \in \F_q, ax^2 = 1 \}\right| = 1 + \left(\frac{a}{q}\right)
		\end{displaymath}
		\label{lem:init}
	\end{lnum}
	\begin{proof}
		Soit $a$ un entier non nul et $q$ un nombre premier impaire.
		\paragraph{Étape a : $a$ est un carré modulo $q$ si et seulement si $a^{-1}$ en est un}
		
		\begin{displaymath}
		\begin{array}{ccll}
		a = x^2 [q] & \Leftrightarrow & a^{-1} = x^2\left(a^{-1}\right)^2 [q] & \textblue{(\text{multiplication par } \left(a^{-1}\right)^2 [q])} \\
		& \Leftrightarrow & a^{-1} = y^2 [q] & \textblue{(\text{où } y = xa^{-1})} \\
		\end{array}
		\end{displaymath}
		
			
	\paragraph{Étape b : $a$ est un carré modulo $q$ si et seulement si $aX^{2} - 1 \in \F_q[X]$ possède deux racines distinctes} On raisonne dans $\F_q$. \textblue{(Supposons que $x$ est inversible.)}
			
	\begin{displaymath}
	\begin{array}{ccll}
		a = x^2 [q] & \Leftrightarrow & a\left(x^{-1}\right)^2 = 1 [q] & \textblue{(\text{multiplication par } \left(x^{-1}\right)^2 [q])} \\
		& \Leftrightarrow & a\left(x^{-1}\right)^2 -1 = 0 [q] & \textblue{(\text{par soustraction de } 1)} \\
		& \Leftrightarrow & x^{-1} \text{ est une racine de } aX^2 -1 & \textblue{\text{(par définition)}} \\
		& \Leftrightarrow & aX^2 -1 \text{ a deux racines distinctes} & \textblue{(\text{car } x \text{ est inversible si et seulement si } x \neq 0)} \\
	\end{array}
	\end{displaymath}
	
	\paragraph{Étape c: Dénombrons  l'ensemble $\left|\{x \in \F_q, ax^2 = 1 \}\right|$ en fonction de si $a$ est ou non un carré dans $\F_q$} On peut distingué deux cas.
			\begin{itemize}
				\item Si $a$ n'est pas un carré modulo $q$, alors par ce qu'on vient de dire $\left|\{x \in \F_q, ax^2 = 1 \}\right| = 0 =1 -1 = 1 + \left(\frac{a}{q}\right)$ \textblue{(symbole de Legendre vaut $-1$ car $a$ est non nul par hypothèse)}.
				
				\item Si $a$ est un carré modulo $q$, alors par ce qu'on vient de dire $\left|\{x \in \F_q, ax^2 = 1 \}\right| = 2 =1 +1 = 1 + \left(\frac{a}{q}\right)$ \textblue{(symbole de Legendre vaut $1$)}.
			\end{itemize}
	\end{proof}
	
	\begin{theo}
		Soient $p, q$ premiers impairs distincts. Alors
		\begin{displaymath}
		\left(\frac{p}{q}\right)\left(\frac{q}{p}\right) = (-1)^{\frac{p-1}{2}\frac{q-1}{2}}
		\end{displaymath}
	\end{theo}	
	\begin{proof}
		L'idée de cette preuve qui est intéressante car elle va dénombrer de deux manières différentes un certain ensemble $X$. On pose $X = \{(x_1, \dots, x_p) \in \F_q | x_1 + \dots + x_p =1\}$.
		
		\paragraph{Étape 1: montrons à l'aide d'action de groupe que $\left|X\right| = 1 + \left(\frac{p}{q}\right) [p]$.} \textblue{(On raisonne sur $\F_q$.)} On fait agir $\Z_{/p\Z}$ sur $\F_q^p$ par permutation des coordonnées:
		\begin{displaymath}
			\forall k \in \Z_{/p\Z}, \forall x = \left(x_1, \dots, x_p\right) \in \F_q^p, k.x = \left(x_{1+k \mod p}, \dots,x_{p+k \mod p} \right)
		\end{displaymath}
		Comme l'action permute les coordonnées \textblue{(la somme de leurs carrés reste $1$)}, $X$ est stable sous l'action de $\Z_{/p\Z}$, étudions les orbites des éléments de $X$ par cette actions.
		\begin{itemize}
			\item L'orbite de $(x, \dots, x) \in F_q^p$ où $x \in \F_q$ est triviale, c'est le singleton $\{(x, \dots, x)\}$ \textblue{(l'ensemble des éléments atteignable depuis $x$ est $x$ par permutation de ces coordonnées)} et toutes les orbites triviales sont de cette forme \textblue{(une orbite triviale est une orbite telle que $\{y \in X, k.x = y\} = \{x\}$ et dans ce cas la, comme on agit par permutation des coordonnées, $x$ est telle que on l'a définie)}. On en déduit qu'il y a autant d'orbites triviales que d'éléments $x \in \F_q$ tel que $px^2 =1$. Par le lemme~\ref{lem:init}, on a que $\left|\{x \in \F_q, ax^2 = 1 \}\right| = 1 + \left(\frac{a}{q}\right)$. \textblue{(De plus, leur stabilisateur est $\Z_{/p\Z}$, en effet, comme pour tout élément $k \in \Z_{/p\Z}, k. =x$, cela conclut.)}
			
			\item Les orbites non triviales sont alors des orbites dont le stabilisateur est trivial. En effet, 
			
			\begin{tabular}{ccl}
				$\mathrm{Stab}(x)$ & $=$ & $\{n \in \Z_{/p\Z} | n.x = \left(x_{1+n \mod p}, \dots,x_{p+n \mod p} \right) \}$ \\
				& $=$ & $\{n \in \Z_{/p\Z} | \forall i x_i = x_{i+n \mod p} \}$ \\
				& $=$ & $\{1\}$ \textblue{(car $\exists i \neq j, x_i \neq x_j$)}\\
			\end{tabular}
			
			Comme $\left|\mathrm{Orb}(x)\right| \left|\mathrm{Stab}(x)\right| = \left|\Z_{/p\Z}\right|$, on en déduit que $\left|\mathrm{Orb}(x)\right| = 1$. Par la formule des classes, on a $\left|X\right| = \left|\{x \in \F_q | px^2 =1 \}\right| + kp \mod p$ où $k$ est le nombre d'orbites non triviales. On a alors $\left|X\right| = 1 + \left(\frac{p}{q}\right) \mod p$.
		\end{itemize}
		
		\paragraph{Étape 2: montrons à l'aide de forme quadratique que $|X| = q^d\left(q^d + (-1)^{\frac{p-1}{2}\frac{q-1}{2}}\right)$.} On pose $q_1\left(x_1, \dots, x_p\right) = x_1 + \dots + x_p$ et $q_2\left(z_1, \dots, z_q, y_1, \dots, y_d, t\right) =2\left(y_1z_1 + \dots z_dy_d\right) +at^2$ deux formes quadratiques avec $d = \frac{p-1}{2}$ et $a = (-1)^q$.
		
		\subparagraph{Étape a: montrons que ces deux formes quadratiques sont équivalentes.} Considérons les matrices de ces formes quadratiques:
		
		\begin{displaymath}
		\begin{array}{ccc}
			I_p = \begin{bmatrix}
			1 &  & 0 \\
			 & \ddots & \\
			 0 &  & 1 \\
			\end{bmatrix} & \text{et} & A = \begin{bmatrix}
			\begin{bmatrix}
			0 & 1 \\
			1 & 0 \\
			\end{bmatrix} & & & & \\
			& \begin{bmatrix}
			0 & 1 \\
			1 & 0 \\
			\end{bmatrix} & & & \\
			& & \ddots & \\
			& & & a \\
			\end{bmatrix} \\
		\end{array}
		\end{displaymath}
		
		Les matrices $I_p$ et $A$ sont congruentes. En effet, $\mathrm{rg}\left(I_p\right) = p = \mathrm{rg}\left(A\right)$ et $\mathrm{det}\left(I_p\right) = 1 = \underbrace{(-1)^{\frac{p-1}{2}}}_{\text{développement par les lignes}} \underbrace{(-1)^{\frac{p-1}{2}}}_{a} = \mathrm{det}\left(A\right)$, donc elles ont même discriminant.Le théorème de classification des formes quadratiques sur $\F_q$ s'applique. On en déduit que les formes quadratiques $q_1$ et $q_2$ sont équivalentes. Ainsi le cardinal de $X$ est égal au cardinal de $X' = \{\left(x_1, \dots, x_d, z_1, \dots, z_d, t\right) | q_2\left(x_1, \dots, x_d, z_1, \dots, z_d, t\right) = 1\}$.
		
		\subparagraph{Étape b: dénombrons les éléments de $X'$.} Soit $\left(x_1, \dots, x_d, z_1, \dots, z_d, t\right) \in X'$. On distingue alors deux cas:
		\begin{itemize}
			\item Si $y_1 = y_d = 0$ alors $at^2 = 1$.
			\begin{itemize}
				\item On a $q$ possibilités pour chaque $z_i$ \textblue{(car $z_i \in \F_q$)}.
				\item On a $1 + \left(\frac{a}{q}\right)$ possibilité \textblue{(lemme~\ref{lem:init})}.
			\end{itemize}
			On en conclut qu'on a $q\left(1 + \left(\frac{a}{q}\right)\right)$.
			
			\item Il existe $y_i$ tel que $y_i \neq 0$.
			\begin{itemize}
				\item Une fois les $z_i$ et $t$ sont fixés, il reste à choisir les $x_i$ qui forment un hyperplan affine de $\F_q^d$. On a alors $q^{d-1}$ possibilités.
				\item Pour fixé les $z_i$, on a alors $q^d -1$ possibilités.
				\item Pour fixé $t$, on a $q$ possibilités.
			\end{itemize}
			On en déduit qu'on a $\left(q^d -1\right)qq^{d-1} = \left(q^d -1\right)q^{d}$.
		\end{itemize}
		
		\subparagraph{Étape c: En déduire le cardinal de $X$.} Des calculs précédent, on a:
		
		\begin{tabular}{ccl}
			$\left|X\right| = \left|X'\right|$ & $=$ & $q\left(1 + \left(\frac{a}{q}\right)\right) + \left(q^d -1\right)q^{d}$ \\
			& $=$ & $q^d\left(1 + \left(\frac{a}{q}\right) + q^d -1\right)$ \\
			& $=$ & $q^d\left(a^{\frac{q-1}{2}} + q^d -1\right)$ \\
			& $=$ & $q^d\left((-1)^{\frac{q-1}{2}\frac{p-1}{2}} + q^d\right)$ \\
		\end{tabular}
		
		\paragraph{Étape 3: Conclure sur le cardinal définitif de $X$.} On a:
		
		\begin{tabular}{ccll}
			$1 + \left(\frac{p}{q}\right)$ & $=$ & $\left(\frac{q}{p}\right)\left(\left(\frac{p}{q}\right) + (-1)^{\frac{q-1}{2}\frac{p-1}{2}}\right) ~[p]$ & \\
			$1 + \left(\frac{p}{q}\right)$ & $=$ & $\left(\frac{q}{p}\right) (-1)^{\frac{q-1}{2}\frac{p-1}{2}} +1 ~[p]$ &  \textblue{$\left(\frac{q}{p}\right)\left(\frac{p}{q}\right) = 1$}\\
			$\left(\frac{p}{q}\right)$ & $=$ & $\left(\frac{q}{p}\right) (-1)^{\frac{q-1}{2}\frac{p-1}{2}} ~[p]$ &  \\
		\end{tabular}
		
		Les termes considérés étant dans $\{1, -1\}$ l'égalité reste vraie dans $\Z$, on en conclut: 
		\begin{displaymath}
		\left(\frac{p}{q}\right)\left(\frac{q}{p}\right) = (-1)^{\frac{p-1}{2}\frac{q-1}{2}}
		\end{displaymath}
	\end{proof}
	
	\section{Compléments autours du dénombrement dans des corps finis}
	
	\subsection*{Applications à cette loi}
	
	\begin{prop}
		$3$ est un carré modulo $p$ si et seulement si $p \equiv -1 [12]$.
	\end{prop}
	
	\begin{appli}
		Les nombres de Merssenne.
	\end{appli}
	
	\subsection*{Corps finis}
	\input{./../Notions/CorpsFinis.tex}
	
	\subsection*{Étude des carrés dans $\F_q$}
	\input{./../Notions/CoprsFinis_Carres.tex}
	
	\subsection*{Théorème de classification des formes quadratiques}
	\input{./../Notions/ActionsGroupes_congruence.tex}
	
	\subsection*{Actions de groupes}
	\input{./../Notions/ActionsGroupes.tex}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}