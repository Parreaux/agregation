\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais,english]{babel}%

\usepackage[dvipsnames]{xcolor}

\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{enumitem}
\usepackage{appendix}
\usepackage{stmaryrd}
\usepackage{amsmath}
\usepackage{wasysym}

\usepackage{listings}
\usepackage{multirow}
\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}
\algnewcommand{\LineComment}[1]{\State \(\triangleright\) #1}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}


\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\F}{\mathbb{F}}
	\newcommand{\Sn}{\mathfrak{S}}
	\newcommand{\A}{\mathfrak{A}}
	
	\newcommand{\thm}{\emph{Théorème}}
	\newcommand{\proposition}{\emph{Proposition}}
	\newcommand{\lem}{\emph{Lemme}}
	\newcommand{\corollaire}{\emph{Corollaire}}
	\newcommand{\definiton}{\emph{Définition}}
	\newcommand{\remarque}{\emph{Remarque}}
	\newcommand{\application}{\emph{Application}}
	\newcommand{\exemple}{\emph{Exemple}}
	\newcommand{\cexemple}{\emph{Contre-exemple}}
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textbluesmall}[1]{{\small{\textcolor{blue}{#1}}}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\sloppy
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\theoremstyle{plain}
	\newtheorem{theo}{Théorème}
	\newtheorem{prop}{Proposition}
	\newtheorem{cor}{Corollaire}
	\newtheorem{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem{definition}{Définition}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	
	\title{Méthode de Kacmarz}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Aucune référence connue.
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 159 (Forme linéaire et dualité); 162 (Systèmes d'équations linéaires); 233 (Analyse numérique matricielle).
			
			\textblue{\emph{Leçons où on peut en parler:}} 208 (Espace vectoriel normé).
		}}
	
	\section{Introduction}
	
	La méthode de Kacmarz permet de résoudre des systèmes linéaires sans décomposé la matrice. Elle réalise une suite de projection sur des hyperplan bien choisi. Cette méthode permet alors de résoudre des systèmes d'équations sans calculs de valeurs propres ni d'inverse: on utilise uniquement des projections.
	
	\section{Méthode de Kacmarz}
	
	\emph{Objectif}: Résoudre $Ax = b$ avec $A \in \mathrm{GL}_n\left(\R\right)$ sans décomposition ni calcul de la matrice inverse mais en effectuant une suite de projection.
	
	\titlebox{red}{\textred{\textbf{Schéma du développement}}}{
		\begin{enumerate}
			\item Introduction des notations.
			\item Lemme 1: caractérisation de l'unique solution via les hyperplans.
			\item Lemme 2: caractérisation des projections que l'on utilise.
			\item Présentation de la méthode (Dessin).
			\item Théorème : convergence de la méthode.
			\begin{enumerate}
				\item Convergence de la suite des approximations.
				\item Cette limite vaut $0$.
			\end{enumerate}
		\end{enumerate}
	}
	
	\noindent\emph{Notations}: On se place sur $\R^n$ muni du produit scalaire euclidien $\left< , \right>$.
	\begin{itemize}
		\item $\overline{x}$ est l'unique solution \textblue{(car $A$ est inversible)}
	
		\item 
		\begin{tabular}{ccc}
			$A = \begin{bmatrix}
			^{t}a_1 \\
			\vdots \\
			^ta_N
			\end{bmatrix}$ & $u_i = \frac{a_i}{\mathopen{||}a_i\mathclose{||}} \in \R$ & $\beta_i = \frac{b_i}{a_i} \in \R$ \\
		\end{tabular} \textblue{(où $^ta_i$ est la $i^{\text{ième}}$ ligne de la matrice $A$)}.
	
		\item $H_i = \{z \in \R | ^tu_iz= \beta_i \} = \beta_iu_i + \mathrm{Vect}\left(u_i\right)^{\bot}$ un hyperplan affine \textblue{(la première égalité est la définition, la deuxième se montre par double inclusion:
			\begin{itemize}
				\item  $z \in H_i$, $^tu_iz = \beta_i + ^tu_iy$ car $^tu_iy = 0$ avec $y \in \mathrm{Vect}\left(u_i\right)^{\bot}$ et $z = u_i\beta_i + y \in \beta_iu_i + \mathrm{Vect}\left(u_i\right)^{\bot}$
				\item $z \in \beta_iu_i + \mathrm{Vect}\left(u_i\right)^{\bot}$, alors il existe $y \in \mathrm{Vect}\left(u_i\right)^{\bot}$ tel que $z = \beta_iu_i + y$, d'où $^tu_iz = \beta_i + ^tu_iy = \beta_i$.
			\end{itemize})}. 
		
		\item $\Pi_i$ la projection orthogonale \textblue{(vectorielle)} sur $\mathrm{Vect}(a_i)^{\bot}$ que l'on confondra avec leur matrice $M_i$.
	\end{itemize}
	
	\begin{lemme}
		On a $\{\overline{x}\} = \cap_{i=1}^{n}H_i$.
		\label{lem1:solution}
	\end{lemme}
	\begin{proof}
		On a alors:
		
		\begin{tabular}{ccll}
			$z \in \cap_{i=1}^{n}H_i$ & $\Leftrightarrow$ & $\forall i, ^tu_iz =\beta_i$ & \textblue{(définition de $H_i$)} \\
			& $\Leftrightarrow$ & $\forall i, ^ta_iz =b_i$ & \textblue{(multiplication par $\mathopen{||} a_i \mathclose{||}$)} \\
			& $\Leftrightarrow$ & $Az =b$ & \textblue{(définition de $Az=b$)} \\
			& $\Leftrightarrow$ & $z = \overline{x}$ & \textblue{(définition de $\overline{x}$)} \\
		\end{tabular}
	\end{proof}
	
	\begin{lemme}
		Soit $u \in \R^n$ de norme un. La matrice $M$ de la projection orthogonale sur $\mathrm{Vect}(u)^{\bot}$ est égale à $I_N - u^tu$. De plus $\mathopen{|||} M \mathclose{|||} = 1$ et pour tout $x \notin \mathrm{Vect}(u)^{\bot}$, $\lVert Mx\rVert < \lVert x \lVert$.
		\label{lem2:projection}
	\end{lemme}
	\begin{proof}
		Soit $x \in \R^n$.
		\begin{itemize}
			\item Le projeté orthogonal $\Pi(x) = Mx$ de $x$ est caractérisé par :
			\begin{displaymath}
			\left\{
			\begin{array}{l}
			\Pi(x) \in \mathrm{Vect}(u)^{\bot} \\
			\forall z \in \mathrm{Vect}(u)^{\bot}, \left< z, \Pi(x) - x\right> = 0
			\end{array}
			\right.
			\end{displaymath}
			Notons $y = (I_n - u^tu)x = x - u^tux$ et montrons que $y$ vérifie la caractérisation de $Mx$.
			\begin{itemize}
				\item Montrons que $y \in \mathrm{Vect}(u)^{\bot}$. Pour cela montrons que $\left< u, y\right> = 0$. On a 
				\begin{displaymath}
				\begin{array}{ccll}
				\left< u, y\right> & = & \left< u, x - u^tux\right> & \textblue{(\text{Définition de } y)} \\
				& = & \left< u, x\right> - \left< u, u^tux\right> & \textblue{\text{(Linéarité du produit scalaire)}} \\
				& = & \left< u, x\right> - \left< u, x\right> & \textblue{(u^tu = \lVert u \rVert^2_ 2 = 1 \text{ hypothèse})} \\
				& = & 0 & \\
				\end{array}
				\end{displaymath}
				\item Soit $forall z \in \mathrm{Vect}(u)^{\bot}$, Montrons que  $\left< z, x - y\right> = 0$. On a
				\begin{displaymath}
				\begin{array}{ccll}
				\left< z, x - y\right> & = & \left< z, x\right> - \left< z, y\right> & \textblue{\text{(Linéarité du produit scalaire)}} \\
				& = & \left< z, x\right> - \left< z, x\right> - \left< z, u^tux\right> & \textblue{(\text{Linéarité du produit scalaire et définition de } y)} \\
				& = & - \left< u^tuz, x\right> & \textblue{(u^tu = 1)} \\
				& = & 0 & \textblue{(^tuz = 0 \text{ car } z \in \mathrm{Vect}(u)^{\bot})} \\
				\end{array}
				\end{displaymath}
			\end{itemize}
			Donc $y = \Pi(x)$ ce qui assure que $M = I_n - u^tu$.
			
			\item De plus, $\forall x \in \R^n$,
			\begin{displaymath}
			\lVert x \rVert^2 \underbrace{=}_{\textblue{\text{Introduction de } \pm Mx}} \lVert Mx + (I - M)x \rVert^2 \underbrace{=}_{\textblue{\underset{(M, (I-M) \text{ orthogonaux})}{\text{Pythagore }}}} \lVert Mx \rVert^2 + \lVert (I - M)x \rVert^2 \geq \lVert Mx \rVert^2
 			\end{displaymath}
 			donc $\mathopen{|||}M\mathclose{|||} \leq 1$ \textblue{(car il existe $u$ tel que $\lVert Mx \rVert = \mathopen{|||}M\mathclose{|||}\lVert x \rVert$)} dont l'égalité est atteinte pour tout $x \in \mathrm{Vect}(u)^{\bot}$ et donc $\mathopen{|||}M\mathclose{|||} = 1$.
 			
 			\item Enfin, pour tout $x \notin \mathrm{Vect}(u)^{\bot})$, $(I - M)x \notin 0$ et donc $\rVert Mx \lVert < \rVert x \lVert$.
		\end{itemize}
	\end{proof}
	
	On donne maintenant la méthode de Kackmarz (algorithme~\ref{algo:Kacmarz}). On donne également une figure en dimension $2$ (Figure~\ref{fig:kacmarzDim2}). De plus, cette méthode peut s'écrire comme une suite récurrente définie par:
	\begin{displaymath}
	x_{k+1} = M_r x_k + u_r\beta_r \text{ où } r = (k+1) \mod n
	\end{displaymath}
	
	
	\begin{minipage}{.46\textwidth}
		\begin{algorithm}[H]
			\begin{algorithmic}[1]
				\While{$\mathopen{||}x_n - x_{n+1} \mathclose{||} \geq \epsilon$}
				\State Projeter $x_0$ sur $H_1$ qui donne $x_1$
				\State Projeter $x_1$ sur $H_2$ qui donne $x_2$
				\State $\dots$
				\State Projeter $x_n$ sur $H_1$ qui donne $x_{n+1}$
				\EndWhile			
			\end{algorithmic}
			\caption{Méthode de Kacmarz}
			\label{algo:Kacmarz}
		\end{algorithm}
	\end{minipage} \hfill
	\begin{minipage}{.46\textwidth}
		\begin{figure}[H]
			\includegraphics[scale = 0.6]{./../Figures/Kackmarz.pdf}
			\caption{Illustration de la méthode de Kacmarz en dimension 2.}
			\label{fig:kacmarzDim2}
		\end{figure}
	\end{minipage}
	
	\begin{theo}
		La méthode de Kacmarz converge vers l'unique solution $\overline{x}$.
	\end{theo}
	\begin{proof}
		Notons $\epsilon_k$ l'erreur à l'étape $n$ correspondant de l'approximation de la solution par $\epsilon_k  = x_k - \overline{x}$, et montrons que $\lim_{k \to \infty}\mathopen{||}\epsilon_k \mathclose{||} =0$.
		
		\paragraph{Étape 1: Montrons que la suite $\left(\epsilon_n\right)_n$ converge.}
		Montrons que la suite $\left(\mathopen{||}\epsilon_k \mathclose{||}\right)_k$ est décroissante. Soit $k \in \N$, on a:
		
		\begin{tabular}{ccll}
			$\epsilon_{k+1}$ & $=$ & $x_{k+1} - \overline{x}$ & \textblue{(définition de $\left(\epsilon_k\right)$)} \\
			& $=$ & $\left(M_r x_k\right) + u_r\beta_r- \overline{x}$ & \textblue{(définition de $\left(x_k\right)$)} \\
			& $=$ & $M_r \left(x_k + u_r\beta_r- \overline{x}\right)$ & \textblue{($u_r\beta_r- \overline{x} \in \mathrm{Vect}\left(u_r\right)^{\bot}$)} \\
			& $=$ & $M_r \left(x_k - \overline{x}\right)$ & \textblue{(par la projection utilisée et $x \in \mathrm{Vect}(u_i)^{\bot}, M_ix = x$)} \\
			& $=$ & $=M_r \epsilon_k$ & \textblue{(définition de $\left(\epsilon_k\right)$)} \\
		\end{tabular}
		
		\noindent\textblue{Montrons les petits résultats de cette preuve.
			\begin{itemize}
				\item $u_r\beta_r- \overline{x} \in \mathrm{Vect}\left(u_r\right)^{\bot}$: $\left<u_r\beta_r- \overline{x}, u_r\right> = \beta_r\left<u_r, u_r\right> - \left<\overline{x}, u_r\right>$ par linéarité du produit scalaire. D'où, comme $u_r$ est de norme un pour la norme euclidienne, $\left<u_r\beta_r- \overline{x}, u_r\right> = \beta_r - \left<\overline{x}, u_r\right>$. Comme $\overline{x} \in H_r$ (lemme~\ref{lem1:solution}), il existe $y \in \in \mathrm{Vect}\left(u_r\right)^{\bot}$ tel que $\overline{x} = u_r\beta_r - y$. Par linéarité du produit scalaire, on a : $\left<u_r\beta_r- \overline{x}, u_r\right> = \beta_r - \beta_r - \left<y, u_r\right> = 0$.
				\item Si $x \in \mathrm{Vect}(u_i)^{\bot}$, alors $M_i x = x$. Par le lemme~\ref{lem2:projection}, $M_i = I_n - u_i^tu_i$. On en déduit que $M_ix = (I_n - u_i^tu_i)x = x - u_i^tu_ix = x$ car $tu_ix = 0$ puisque $x \in \mathrm{Vect}(u_i)^{\bot}$.
			\end{itemize}}
		
		
		On en déduit que $\mathopen{||}\epsilon_{k+1} \mathclose{||} \leq \mathopen{||}\epsilon_k \mathclose{||}$ \textblue{(car $\mathopen{||}\epsilon_{k+1} \mathclose{||} \leq \mathopen{|||}M_r \mathclose{|||}\mathopen{||}\epsilon_k \mathclose{||} = \mathopen{||}\epsilon_k \mathclose{||}$ par le lemme~\ref{lem2:projection} puisque $\mathopen{|||}M_r \mathclose{|||} = 1$)}. Comme $\left(\mathopen{||}\epsilon_{k} \mathclose{||}\right)_k$ est une suite décroissante et minorée par $0$ \textblue{(par les propriétés de la norme qui est toujours positive)}, la suite converge vers une limite $l \in \R$.
		
		\paragraph{Étape 2: Montrons que cette limite vaut $0$.} 
		On pose $T= M_nM_{n-1}\dots M_1$ \textblue{(cela correspond à un cycle de notre algorithme (une boucle \textsc{While}))}. Montrons que $\mathopen{||} T\mathclose{||} < 1$ \textblue{(on prend une norme subordonnée à la norme euclidienne)}. Soit $x \in \N^n \setminus \{0\}$. On distingue alors deux cas.
		\begin{itemize}
			\item $\exists i \in \llbracket 1, n \rrbracket$ tel que $\mathopen{||}M_i \dots M_1 x\mathclose{||} < \mathopen{||}x\mathclose{||}$. Dans ce cas, on peut écrire:
			\begin{displaymath}
			\mathopen{||}Tx\mathclose{||} \leq \underbrace{\mathopen{||}M_n \mathclose{||}\dots \mathopen{||}M_{i+1}\mathclose{||}}_{\leq 1 \textblue{\text{ (par le lemme~\ref{lem2:projection}) }}}\underbrace{\mathopen{||}M_i \dots M_1 x\mathclose{||}}_{<\mathopen{||}x\mathclose{||} \text{\textblue{ (par hypothèse)}}} < \mathopen{||}x\mathclose{||}
			\end{displaymath}
			
			\item $\forall i \in \llbracket 1, n \rrbracket$, $\mathopen{||}M_i \dots M_1 x\mathclose{||} = \mathopen{||}x\mathclose{||}$ \textblue{(car $x \in \mathrm{Vect}^{\bot}$)}. Dans ce cas, $\mathopen{||}M_1x\mathclose{||} = \mathopen{||}x\mathclose{||}$ soit $M_1x = x$. On en déduit que $\forall i, x \in \mathrm{Vect}\left(u_i\right)^{\bot}$, donc $x = 0$ car $A \in GL_n(\R)$. Contradiction.
		\end{itemize}
		
		On en déduit que $\mathopen{||}\epsilon_{kn} \mathclose{||} \leq \mathopen{||}T^k{||}\mathopen{||}\epsilon_0 \mathclose{||} \to 0$ \textblue{(par convergence d'une suite géométrique de raison inférieur à $1$ et on trouve cette suite par récurrence)}.
	\end{proof}
	
	\paragraph{Remarque sur l'algorithme}
	Sa complexité est en $O\left(n^2\right)$ et il converge pour toute matrice $A$ même si elle n'est pas inversible.
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}