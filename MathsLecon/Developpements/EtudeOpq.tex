\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}


\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
} 

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\Cun}{\mathcal{C}^1}
	\newcommand{\Cdeux}{\mathcal{C}^2}
	\newcommand{\Cinf}{\mathcal{C}^{\infty}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{OliveGreen}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	\newcommand{\textmagenta}[1]{\textcolor{DarkOrchid}{#1}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\newcommand{\pcite}[1]{\protect{\cite{#1}}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	
	\title{Étude de $O(p, q)$}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} H2G2 \cite[p.210]{Caldero-Germoni} 
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 106 (Groupe linéaire); 156 (Exponentielle de matrice); 170 (Forme quadratique).
		}}
	
	\section{Introduction}
	
	On souhaite étudier le groupe $O(p, q)$ formé de la forme quadratique standard sur $\R^{p+q}$. Pour cela, on fait apparaître un isomorphisme entre $O(p, q)$ et $O(p) \times O(p) \times \R^{pq}$. On utilise alors la notion d'orthogonalité et les propriétés de l'exponentielle de matrices.
	
	On commence par donner le cadre et quelques définitions préliminaires.
	
	\begin{definition}
		On appelle groupe orthogonal l'ensemble $O_n(\R) = \{A \in \mathcal{M}_n(\R), ^tAA=I_n\}$.
	\end{definition}
	
	\begin{definition}
		Soient $p, q \in \N$. On note $O(p, q)$ le sous-groupe de $GL_{p+q}(\R)$ formé des isométries de la forme quadratique standard sur $\R^{p+q}$ de signature $(p, q)$, c'est-à-dire $x_1^2 + \dots + x_{p-1}^2 - \dots - x_{p+q}^2$. La représentation matricielle de cette forme dans la base canonique est donnée par $I_{(p, q)} = \mathring{Diag}(\underbrace{1, \dots, 1}_{p \text{ fois}}, \underbrace{-1, \dots, -1}_{q \text{ fois}})$. On a alors $O(p,q) = \{M ~|~ MI_{(p,q)}~^tM\}$.
	\end{definition}
	
	Montrons alors l'isomorphisme de $O(p, q)$ dans les groupes orthogonaux correspondant: $O(p, q) \simeq O(p) \times O(q) \times \R^{pq}$.
	
	\section{Étude de $O(p,q)$}
	
	\begin{theo}
		Soit $p, q \neq 0$. Il existe un homéomorphisme 
		\begin{displaymath}
			O(p, q) \simeq O(p) \times O(q) \times \R^{pq}
		\end{displaymath}
	\end{theo}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement}}}{\begin{enumerate}
			\item Par la décomposition polaire, on exhibe $S \in S_n^{++}(\R)$ et $O \in O_n(\R)$. De plus, $S, O \in O(p,q)$.
			\begin{enumerate}
				\item $O(p, q)$ est stable par transposition.
				\item $S^2 \in O(p,q)$
				\item $S \in O(p,q)$
			\end{enumerate}
			\item $O(p,q) \cap O_n(\R) \simeq O(p) \times O(q)$
			\item $O(p,q) \cap S_n^{++}(\R) \simeq \R^{pq}$
		\end{enumerate}
	}
	
	\begin{proof}
		Soit $M \in O(p, q) \subset GL_n(\R)$ avec $n = p+ q$. Par la décomposition polaire, il existe deux matrices $O \in O_n(\R)$ et $S \in S_n^++(\R)$ telles que $M = OS$.
		
		\paragraph{Étape 1: Montrons que $S$ et $O$ sont dans $O(p,q)$.} Pour cela, il suffit de montrer que $S$ l'est \textblue{(par la structure de groupe de $O(p,q)$ car $O = MS^{-1} \in O(p, q)$)}. Soit $T = ^tMM$. On a $S^2 = T$.
		
		\subparagraph{Montrer que $O(p, q)$ est stable par transposition} 
		
		\begin{displaymath}
		\begin{array}{ccll}
		M \in O(p, q) & \Rightarrow & MI_{(p, q)}~^tM = I_{(p,q)} & \textblue{\text{(définition de } O(p,q))} \\
		\multicolumn{4}{l}{\textblue{\text{(on pose } \varphi(M) = ^tM^{-1} \text{ un automorphisme de } GL_n(\R))}} \\
		& \Rightarrow & ^tM^{-1}I_{(p, q)}M^{-1} = I_{(p,q)} & \textblue{\text{(applique } \varphi \text{: commute avec la transposée et laisse stable } I_{(p,q)})} \\
		& \Rightarrow & ^tM^{-1} \in O(p, q) & \textblue{\text{(définition de } O(p,q))} \\
		& \Rightarrow & ^tM \in O(p, q) & \textblue{(O(p,q) \text{ est un groupe)}} \\
		\end{array}
		\end{displaymath}
		
		\subparagraph{Montrer que $S \in O(p,q)$} Comme $O(p,q)$ est stable par transposition, $T = ^tMM \in O(p,q)$ \textblue{($O(p, q)$ est un groupe)}. On en déduit que $T = S^2 \in O(p,q)$. Pour conclure, prenons la racine carrée de $T$ via l'exponentielle diviser par deux. Comme $T$ est définie positive, on peut écrire $T = \exp U$ pour $U \in S_n(\R)$ convenable ($\exp$ réalise un homéomorphisme de $S_n$ à $S_n^{++}$).
		
		\begin{displaymath}
		\begin{array}{ccll}
		T \in O(p, q) & \Leftrightarrow & TI_{(p, q)}~^tT = I_{(p,q)} & \textblue{\text{(définition de } O(p,q))} \\
		& \Leftrightarrow & ^tT = I_{(p, q)}^{-1}T^{-1}I_{(p,q)} & \textblue{\text{(multiplication par les inverses)}} \\
		& \Leftrightarrow & ^t(\exp U) = I_{(p, q)}^{-1}(\exp U)^{-1}I_{(p,q)} & \textblue{(T = \exp U)} \\
		& \Leftrightarrow & (\exp ~^tU) = \exp (-I_{(p, q)}^{-1}UI_{(p,q)}) & \textblue{(\text{propriété de l'exponenetielle})} \\
		& \Leftrightarrow & ^tU = U = -I_{(p, q)}^{-1}UI_{(p,q)} & \textblue{\text{(bijectivité de } \exp)} \\
		& \Leftrightarrow & UI_{(p, q)} + I_{(p,q)}U = 0 & \textblue{\text{(linéaire)}} \\
		& \Leftrightarrow & \frac{U}{2}I_{(p, q)} + I_{(p,q)}\frac{U}{2} = 0 & \textblue{\text{(linéaire)}} \\
		& \Leftrightarrow & \frac{^tU}{2} = -I_{(p, q)}^{-1}\frac{U}{2}I_{(p,q)} & \textblue{\text{(linéarité et } U = ^tU)} \\
		& \Leftrightarrow & \exp\left(\frac{^tU}{2}\right) = \exp\left(-I_{(p, q)}^{-1}\frac{U}{2}I_{(p,q)}\right) & \textblue{\text{(bijectivité de }\exp)} \\
		& \Leftrightarrow & ^t\exp\left(\frac{U}{2}\right) = -I_{(p, q)}^{-1}\exp\left(\frac{U}{2}\right)^{-1}I_{(p,q)} & \textblue{\text{(propriété de l'exponentielle)}} \\
		\end{array}
		\end{displaymath}  
		Or, on a $\exp \left(\frac{U}{2}\right) \in S_n(\R)$ et $\exp^2 \left(\frac{U}{2}\right) = \exp(U) = T$. On en déduit que $\exp\left(\frac{U}{2}\right) =S$ et $SI_{(p, q)}~^tS = I_{(p, q)}$, c'est-à-dire $S \in O(p,q)$. Et, $O \in O(p,q)$ et la décomposition polaire $M = OS \mapsto (O, S)$ induit une bijection continue: $O(p, q) \simeq (O(p, q) \cap O(n)) \times (O(p, q) \cap S_n^{++})$.
	
	\paragraph{Montrons que $O(p,q) \cap O_n(\R) \simeq O(p) \times O(q)$} Soit $O \in O(p,q) \cap O_n(\R)$.
	\begin{itemize}
		\item On découpe $O$ en blocs : $O = \begin{pmatrix}
		A & C \\
		B & D \\
		\end{pmatrix} \in O(p, q)$.
		\begin{displaymath}
		^tOI_{(p, q)}O =I_{(p,q)} \Leftrightarrow \left\{
		\begin{array}{ccc}
		^tAA - ^tBB & = & I_p \\
		^tAC - ^tBD & = & 0 \\
		^tCA - ^tDB & = & 0 \\
		^tCC - ^tDD & = & -I_q \\
		\end{array}\right.
		\end{displaymath} \textblue{(application de la transposée et multiplication des matrices par bloc)}
		\item Comme $O \in O(n)$ \textblue{(car dans l'intersection)}, $^tOO = I_n$, on en déduit que $OI_{(p,q)} = I_{(p, q)}O$: $O$ et $I_{(p, q)}$ commutent. Par calcul, on voit que $B$ et $C$ sont nuls, ainsi que $A \in O(p)$ et $D \in O(q)$.
		\item Ainsi $O(p, q) \cap O(n) = \left\{\begin{pmatrix}
		A & 0 \\
		0 & D \\
		\end{pmatrix}, A \in O(p), D\in O(q)\right\} \equiv O(p) \times O(q)$.
	\end{itemize}
	
	\paragraph{Montrons que $O(p,q) \cap S_n^{++}(\R) \simeq \R{pq}$} On utilise le caractère bijectif de la fonction exponentielle: $\exp : S_n(\R) \to S_n^{++}(\R)$.
	\begin{itemize}
		\item $\exp : L = \{u \in \mathcal{M}_n(\R), UI_{(p, q)} + I_{(p, q)} = 0 \} \to O(p, q)$. 
		D'où $S_n(\R) \cap L \simeq O(p, q) \cap S_n^{++}(\R)$.
		\item $\dim \Sigma_n(\R) = \frac{n(n+1)}{2}$ et $\dim L \cap S_n(\R) = pq$. Donc $O(p, q) \cap S_n^{++} \simeq R^{pq}$.
	\end{itemize}
	
	\end{proof}
	
	\section{Compléments autours du groupe $O(p,q)$}
	
	\subsection*{Groupe orthogonal}
	\input{./../Notions/Groupes_orthogonal.tex}
	
	\subsection*{Décomposition polaire et applications}
	\input{./../Notions/Groupes_decompositionPolaire.tex}
	
	\subsection*{Exponentielle de matrices}
	\input{./../Notions/ExponentielleMatrice.tex}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}