\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
		};
		%\draw[color=blue] (box.north west)--(box.north east);
		\node[title] at (box.north) {#2};
		\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\Cun}{\mathcal{C}^1}
	\newcommand{\Cdeux}{\mathcal{C}^2}
	\newcommand{\Cinf}{\mathcal{C}^{\infty}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{OliveGreen}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	\newcommand{\textmagenta}[1]{\textcolor{DarkOrchid}{#1}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\newcommand{\pcite}[1]{\protect{\cite{#1}}}
	
	\theoremstyle{plain}
	\newtheorem{theo}{Théorème}
	\newtheorem{prop}{Proposition}
	\newtheorem{cor}{Corollaire}
	\newtheorem{lemme}{Lemme}
	\newtheorem{application}{Application}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	
	\title{Théorème central limite}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Références du développement:} 40 développements en analyse \cite[p.207]{BernisBernis}, intégration et probabilité \cite[p.220]{GaretKurtzmann}.
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 250 (Transformée de Fourier); 260 (Espérance, variance, moment); 264 (Variables aléatoires discrètes).
		}}

	\section{Introduction}
	
	Le théorème central limite (aussi improprement appelé théorème de la limite centrale ou centrée) établit la convergence en loi de la somme d'une suite de variables aléatoires vers la loi normale. Intuitivement, ce résultat affirme que toute somme de variables aléatoires indépendantes tend dans certains cas vers une variable aléatoire gaussienne. Ce théorème et ses généralisations offrent une explication de l'omniprésence de la loi normale dans la nature : de nombreux phénomènes sont dus à l'addition d'un grand nombre de petites perturbations aléatoires. 

	Historiquement, c'est bien le théorème qui est central, ce qui rend l'appellation « théorème de la limite centrale » impropre en toute rigueur. Elle est cependant souvent employée, les mathématiciens français considérant que l'adjectif « central » s'applique au centre de la distribution, par opposition à sa queue. 
	
	\section{Théorème central limite}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement (leçons 250 et 260)}}}{
		Preuve du théorème~\ref{thm:TCL}
		\begin{enumerate}
			\item Centré et réduire les variable du théorème.
			\item Calcul de la fonction caractéristique d'une variable suivant une loi normale (lemme~\ref{lem:FonctionCaracNormale}): théorème d'holomorphie sous le signe intégrale.
			\item Étude de la convergence de la suite des fonctions caractéristiques induite par $X_i$.
			\begin{enumerate}
				\item Calcul de la fonction caractéristique de la variable $\frac{S_n}{\sqrt{n}}$.
				\item Calcul du développement limité de cette fonction caractéristique.
				\item Étude d'une majoration (lemme~\ref{lem:Majoration})
				\item Étude de la convergence.
			\end{enumerate}
		\end{enumerate}
	}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement (leçon 264)}}}{
		\begin{itemize}
			\item Preuve du théorème~\ref{thm:TCL}
			\begin{enumerate}
				\item Centré et réduire les variable du théorème.
				\item Étude de la convergence de la suite des fonctions caractéristiques induite par $X_i$.
				\begin{enumerate}
					\item Calcul de la fonction caractéristique de la variable $\frac{S_n}{\sqrt{n}}$.
					\item Calcul du développement limité de cette fonction caractéristique.
					\item Étude d'une majoration (lemme~\ref{lem:Majoration})
					\item Étude de la convergence.
				\end{enumerate}
			\end{enumerate}
			\item Preuve de l'application~\ref{appli:approximationBernoulli}
		\end{itemize}
	}
	
	\begin{theo}
		Soit $(\Omega, \mathcal{F}, \mathbb{P})$ un espace probabilisé. Soit $\left(X_n\right)_{n \in \N^*}$ une suite de variables aléatoires réelles, indépendantes et identiquement distribuées et éléments de $L^2(\Omega, \mathcal{F}, \mathbb{P})$. On suppose en outre que $\mathrm{Var}(X_1) \neq 0$. Alors, on a 
		\begin{displaymath}
		\sqrt{n}\left(\overline{X_n} - \mathbb{E}[X_1]\right) \overset{\mathcal{L}}{\underset{n \to \infty}{\longrightarrow}} \mathcal{N}(0, \mathrm{Var}(X_1))
		\end{displaymath}
		\label{thm:TCL}
	\end{theo}
	\begin{proof}
		Soit $\left(X_n\right)_{n \in \N^*}$ une suite de variables aléatoires réelles, indépendantes et identiquement distribuées et éléments de $L^2(\Omega, \mathcal{F}, \mathbb{P})$. On note $S_n = \sum_{i = 0}^{n} X_i$ et $\overline{X_n} = \frac{1}{n}S_n$.
		
		\paragraph{Étape 1: centré et réduire} Quitte à centrée et réduire les variables aléatoires, on peut supposer que $\mathbb{E}[X_1] = 0$ et $\mathrm{Var}(X_1) = 1$. \textblue{(Sinon, on pose $Y_n = \frac{X_n - \mathbb{E}[X_1]}{\sqrt{\mathrm{Var}(X_1)}}$ et si $\sqrt{n}\overline{Y_n} \overset{\mathcal{L}}{\underset{n \to \infty}{\longrightarrow}} \mathcal{N}(0, \mathrm{Var}(X_1))$, alors $\sqrt{n}\left(\overline{X_n} - \mathbb{E}[X_1]\right) \overset{\mathcal{L}}{\underset{n \to \infty}{\longrightarrow}} \sqrt{\mathrm{Var}(X_1)}\mathcal{N}(0, 1) = \mathcal{N}(0, \mathrm{Var}(X_1))$.)}
		
		\paragraph{Étape 2: calcul de la fonction caractéristique d'une variable suivant une loi normale}
		\begin{lemme}[\protect{\cite[p.220]{GaretKurtzmann}}]
			Soit $X \sim \mathcal{N}(0, 1)$, alors $\varphi_X(t) = e^{-\frac{t}{2}}$.
			\label{lem:FonctionCaracNormale}
		\end{lemme}
		\begin{proof}
		Montrons que si $X \sim \mathcal{N}(0, 1)$, alors pour tout $z \in \C$, $\mathbb{E}\left[e^{zx}\right] = e^{\frac{t}{2}}$.
		\begin{itemize}
			\item $t \mapsto e^{\frac{t}{2}}$ est holomorphe
			\item $\mathbb{E}\left[e^{zx}\right] = \frac{1}{\sqrt{2\pi}} \int_{-\infty}^{+\infty} e^{zx - \frac{x^2}{2}} dx$ \textblue{(Application du théorème d'holomorphie sous le signe intégrable)}. Soit,
			\begin{displaymath}
				\begin{array}{ccll}
				\mathbb{E}\left[e^{zx}\right] & = & \frac{1}{\sqrt{2\pi}} \int_{-\infty}^{+\infty} e^{zx - \frac{x^2}{2}} dx & \textblue{\text{(Précédent)}} \\
				& = & \frac{1}{\sqrt{2\pi}} e^{\frac{z^2}{2}}\int_{-\infty}^{+\infty} e^{-\frac{1}{2}(x - z)^2} dx & \textblue{\text{(Factorisation et sortie d'une constante)}} \\
				& = & \frac{1}{\sqrt{2\pi}} e^{\frac{z^2}{2}}\int_{-\infty}^{+\infty} e^{-\frac{y^2}{2}} dy & \textblue{\text{(Changement de variable $y = x - z$)}} \\
				& = & e^{\frac{z^2}{2}} & \textblue{\text{($\int_{-\infty}^{+\infty} e^{-\frac{y^2}{2}} dy = \sqrt{2\pi}$)}} \\
				\end{array}
			\end{displaymath}
			\item Par unicité du prolongement analytique: $\mathbb{E}\left[e^{zx}\right] = e^{\frac{z^2}{2}}$.
			\item On en déduit que $\varphi_X(t) = \mathbb{E}\left[e^{itx}\right] = e^{\frac{(it)^2}{2}} = e^{-\frac{t^2}{2}}$.
		\end{itemize}
		
		\paragraph{Étape 3: étude de la convergence de la suite des fonctions caractéristiques induite par $X_i$} D'après le théorème de Paul Lévy, il suffit de montrer la convergence simple sur $\R$ de la suite des fonctions caractéristique $\left(\varphi_{\frac{S_n}{\sqrt{n}}}\right)_{n \in \N^*}$ vers la fonction caractéristique de la loi normale centrée réduite $t \mapsto e^{-\frac{t^2}{2}}$ (lemme~\ref{lem:FonctionCaracNormale}). 
		
		\subparagraph{Étape a: calcul de la fonction caractéristique de la variable $\frac{S_n}{\sqrt{n}}$} Soit $t \in \R$, $\forall n \in \N^*$, comme $X_1, \dots, X_n$ sont mutuellement indépendantes et identiquement distribuées, on a 
		\begin{displaymath}
		\begin{array}{ccll}
		\varphi_{\frac{S_n}{\sqrt{n}}}(t) & = & \mathbb{E}\left[\exp(it \frac{S_n}{\sqrt{n}})\right] & \textblue{\text{(Définition de la fonction caractéristique)}} \\
		& = & \mathbb{E}\left[\exp(\frac{it}{\sqrt{n}} \sum_{i = 1}^{n} X_i)\right] & \textblue{\text{(Définition de $S_n$)}} \\
		& = & \mathbb{E}\left[\prod_{i = 1}^{n}\exp(\frac{it}{\sqrt{n}}X_i)\right] & \textblue{\text{(Propriété de $\exp$)}} \\
		& = & \prod_{i = 1}^{n}\mathbb{E}\left[\exp(\frac{it}{\sqrt{n}}X_i)\right] & \textblue{\text{(Indépendance des $X_i$)}} \\
		& = & \prod_{i = 1}^{n}\varphi_{X_i}(\frac{t}{\sqrt{n}}) & \textblue{\text{(Définition de la fonction caractéristique)}} \\
		& = & \left(\varphi_{X_1}(\frac{t}{\sqrt{n}})\right)^n & \textblue{\text{(Les $X_i$ sont identiquement distrbuées)}} \\
		\end{array}
		\end{displaymath}
		\end{proof}
		
		\subparagraph{Étape b: calcul du développement limité de cette fonction caractéristique} Comme $X_1 \in L^2(\Omega, \mathcal{F}, \mathbb{P})$, sa fonction caractéristique est de classe $\mathcal{C}^2$ et par la formule de Krönig, on a $\varphi_{X_1}'(0) = i\mathbb{E}[X_1] = 0$ et $\varphi_{X_1}''(0) = -\mathbb{E}[X_1^2] = -\mathrm{Var}(X_1) = -1$. Ainsi, $\varphi_{X_1}$ admet un développement limité au voisinage de $0$ de la forme $\varphi_{X_1} = 1 - \frac{h^2}{2} + \underset{h \to 0}{o}(h^2)$.
		
		\subparagraph{Étape c: étude d'une majoration}
		\begin{lemme}
			Soit $a, b \in \overline{D(0, 1)}$. Alors, pour tout $n \geq 1$, on a $\left|a^n - b^n\right| \leq n\left|a - b\right|$.
			\label{lem:Majoration}
		\end{lemme}
		\begin{proof}
			\begin{displaymath}
			\begin{array}{ccll}
			\left|a^n - b^n\right| & = & \left|(a - b) \sum_{k=0}^{n-1}a^{n-1-k}b^k\right| & \textblue{\text{(Développement)}} \\
			& \leq & \left|a - b\right| \sum_{k=0}^{n-1}\left|a^{n-1-k}b^k\right| & \textblue{\text{(Inégalité triangulaire)}} \\
			& \leq & n\left|a - b\right| & \textblue{\text{($\left|a^{n-1-k}b^k\right| \leq 1$)}} \\
			\end{array}
			\end{displaymath}
		\end{proof}
		
		\subparagraph{Étape d: étude de la convergence}  D'après le développement limité de $\varphi_{X_1}$, on dispose d'une fonction $\epsilon: \R \to \C$ telle que $\varphi_{X_1}(h) = 1 - \frac{h^2}{2} + h^2\epsilon(h)$, avec $\lim\limits_{h \to 0} \epsilon(h) = 0$.
		
		Il est également possible de choisir un rang $N \in \N^*$ tel que $\forall n \geq N$,
		\begin{displaymath}
			\left|1 - \frac{t^2}{2n}\right| \leq 1 \Leftrightarrow 2n - t^2 \leq 2n \Leftrightarrow -t^2 \leq 0
		\end{displaymath}
		de sorte que d'après le lemme, pour tout $n \geq N$, on ait:
		\begin{displaymath}
		\begin{array}{ccll}
		\left|\varphi_{\frac{S_n}{\sqrt{n}}}(t) - \left(1 - \frac{t^2}{2n}\right)^n\right| & = & \left|\varphi_{X_1}(\frac{t}{\sqrt{n}})^n - \left(1 - \frac{t^2}{2n}\right)^n\right| & \textblue{\text{(Calcul de $\frac{S_n}{\sqrt{n}}(t)$}} \\
		& \leq & n\left|\varphi_{X_1}(\frac{t}{\sqrt{n}}) - \left(1 - \frac{t^2}{2n}\right)\right| & \textblue{\text{(Lemme~\ref{lem:Majoration})}} \\
		& = & n\left|1 - \frac{\left(\frac{t}{\sqrt{n}}\right)^2}{2} + \left(\frac{t}{\sqrt{n}}\right)^2\epsilon\left(\frac{t}{\sqrt{n}}\right) - 1 + \frac{t^2}{2n}\right|  & \textblue{\text{(Développement de $\varphi_{X_1}(h)$)}} \\
		& = & \frac{t^2}{n}n\left|\epsilon\left(\frac{t}{\sqrt{n}}\right) \right|  & \textblue{\text{(Simplification)}} \\
		& = &t^2\left|\epsilon\left(\frac{t}{\sqrt{n}}\right) \right|  & \textblue{\text{(Simplification)}} \\
		& \underset{n \to \infty}{\longrightarrow} & 0 & \textblue{\text{Limite de $\epsilon$ en $0$}}
		\end{array}
		\end{displaymath}
		De plus $\left(1 - \frac{t^2}{2n}\right)^n \underset{n \to \infty}{\longrightarrow} \exp(- \frac{t^2}{2})$, donc finalement par inégalité triangulaire, $\varphi_{\frac{S_n}{\sqrt{n}}}(t) \underset{n \to \infty}{\longrightarrow} \exp(- \frac{t^2}{2})$.
	\end{proof}
	
	\begin{application}
		Soit $p \in \left]0, 1 \right[$. Soit $\left(Y_n\right)_{n \in \N^*}$ une suite de variables aléatoires réelles telles que pour tout $n \in \N^*$, $Y_n$ suivent la loi binomiale $\mathcal{B}(n, p)$. Alors, on a 
		\begin{displaymath}
		\frac{Y_n - np}{\sqrt{n(p(1-p))}} \overset{\mathcal{L}}{\underset{n \to \infty}{\longrightarrow}} \mathcal{N}(0, 1)
		\end{displaymath}
		\label{appli:approximationBernoulli}
	\end{application}
	\begin{proof}
		Soit $\left(X_n\right)_{n \in \N}$ une suite de variables aléatoires réelles indépendantes et identiquement distribuées suivant une loi de Bernoulli $\mathcal{B}(1, p)$. En appliquant le théorème central limite à cette suite de variables de carré intégrable, on a
		\begin{displaymath}
		\sqrt{n}(\overline{X_n} - p) \overset{\mathcal{L}}{\underset{n \to \infty}{\longrightarrow}} \mathcal{N}(0, p(1-p))
		\end{displaymath}
		c'est-à-dire, \textblue{(en sortant la variance)}
		\begin{displaymath}
		\frac{\sum_{i = 1}^{n}X_i - np}{\sqrt{np(1-p)}}) \overset{\mathcal{L}}{\underset{n \to \infty}{\longrightarrow}} \mathcal{N}(0, 1)
		\end{displaymath}
		car
		\begin{displaymath}
		\sqrt{n}(\overline{X_n} - p) = \sqrt{n}(\frac{1}{n}\sum_{i = 1}^{n}X_i - p) = \frac{\sum_{i = 1}^{n}X_i - np}{\sqrt{n}}
		\end{displaymath}
		Or, pour tout $n \in \N^*$, les variables aléatoires $Y_n$ et $\sum_{i=1}^{n} X_i$ ont la même loi.
	\end{proof}

	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}