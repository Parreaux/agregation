\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\usepackage{tikz,tkz-tab}


\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}


\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\E}{\mathbb{E}}
	\newcommand{\D}{\mathcal{D}}
	\newcommand{\A}{\mathfrak{A}}
	\newcommand{\Cun}{\mathcal{C}^1}
	\newcommand{\un}{\mathbb{1}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{OliveGreen}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	\newcommand{\textmagenta}[1]{\textcolor{DarkOrchid}{#1}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem{prop}{Proposition}
	\newtheorem*{lemme}{Lemme}
	\newtheorem*{cor}{Corollaire}
	\newtheorem{lnum}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	\newtheorem*{cex}{Contre-exemple}
	
	\title{Processus de Galton--Watson}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Références du développement:} Probabilités pour les nuls  \cite[p.195]{Appel} et Cottrel \cite[p.72]{CottrelGenonDuhamelMeyre}.
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 226 (Suites récurrentes); 229 (Fonctions monotones, fonctions convexes); 260 (Espérance); 264 (Variables aléatoires discrètes) .
		}}
	
	\section{Introduction}
	
	On souhaite étudier la dynamique d'un modèle simple de population issue d'un seul individu. \textblue{Biologiquement, cela semble étrange car on penses d'abord à la reproduction sexué. Lors de l'introduction du problème Francis Galton s'intéressait aux nom de famille d'une famille britannique (seul l'homme transmet son nom de famille). Cependant, ce problème semble beaucoup plus adapté à la descendance d'une bactérie, l'étude d'une épidémie ou encore le nombre de neutron dans un réacteur nucléaire (il permet de modéliser beaucoup de phénomènes naturels).} Pour ce faire, on considère un temps discret représenté par une variable entière $n$ et on note $Z_n$ le nombre d'individu dans notre population. A l'instant $n = 0$, on a $Z_n = 1$. On suppose de plus qu'à chaque instant $n$, chacun des individu donne naissance à un certain nombre de descendants et meurt selon la même loi de probabilité qui est indépendante des autres individus. 
	
	L'objet de ce développement est de formaliser cette modélisation et d'étudier la probabilité que la population s'éteigne. \textblue{On aurait pû considérer bien d'autres questions comme quel est le nombre moyen d'individus à la $n^{\textrm{ième}}$ générations.}
	
	\section{Processus de Galton--Watson}
	
	\paragraph{Contexte} Soit X une variable aléatoire intégrable à valeur dans $N$. 
	
	On note, pour $n \in \N$, $p_n = \Pr(X = n)$ et $m = \E[X] = \sum_{n=0}^{\infty} np_n < \infty$.
	
	Soit $(X_{i,j})_{i, j \in N}$ une famille de variables aléatoires indépendantes et identiquement distribuées, suivant la loi $\Pr_X$. Alors, la variable aléatoire $X_{i,n}$ représente le nombre de descendants de l'individu $i$ à la $n^{\textrm{ième}}$ génération \textblue{(on rappelle que les individus engendrent leurs enfants tout seul)}.
	
	On défini la suite $(Z_n)_{n \in \N}$ représentant le nombre d'individu de la population à la génération $n$, de la façon suivante:
	
	\[ \left  \{
	\begin{array}{c @{ = } ll}
	Z_0 & 1 & \\
	Z_n+1 & \sum_{i=0}^{Z_n} X_{i, n} & \forall n \in \N\\
	\end{array}
	\right.
	\]
	
	On va étudier cette suite $Z_n$ afin de répondre à la question: quelle est la probabilité que la population considérée s'éteigne? On souhaite alors calculer $\Pr(\exists n \in \N, Z_n = 0)$.
	
	\titlebox{red}{\textred{\textbf{Schéma du développement (leçons 226, 229)}}}{
		\begin{enumerate}
			\item Remarques et définitions préliminaires.
			\item Étude des propriétés de la fonction génératrice, $G$, de $X$.
			\item Définir la fonction caractéristique pour $Z_n$ (on ne montre pas ses propriétés).
			\item Calcul de la probabilité d'extinction $x_{\infty}$.
		\end{enumerate}
	}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement (leçons 260, 264)}}}{
		\begin{enumerate}
			\item Remarques et définitions préliminaires.
			\item Définition et propriétés de la fonction génératrice, $G$, de $X$.
			\item Étude de la fonction caractéristique pour $Z_n$.
			\item Calcul de la probabilité d'extinction $x_{\infty}$.
		\end{enumerate}
	}
	
	
	\paragraph{Étape 1 : remarques et définitions préliminaires}

	\begin{req}
		On note que si $Z_n = 0$ alors $Z_{n+1} = 0$.
	\end{req}
	
	\begin{lnum}
		On a pour tout $n \in \N^{*}$, pour tout $i \in \N$, que $Z_n$ et $X_{i, n}$ sont indépendantes.
		\label{lem:independance}
	\end{lnum}
	\begin{proof}
		Soit $n \in \N^{*}$, $Z_n$ ne dépend que de $Z_{n-1}$ et de la famille $(X_{i, n_-1})_{i \in \N}$. Ainsi par une récurrence immédiate, il vient que $Z_n$ ne dépend que de la famille $(X_{i, j})_{i \geq 0, j <n}$. On conclut alors par indépendance des variables $X_{i,j}$.
	\end{proof}
	
	On pose 
	\begin{displaymath}
	G(t) = \E[t^{X}] = \sum_{k=0}^{\infty} p_kt^k
	\end{displaymath}
	la fonction génératrice des $X_{i,j}$. On note $x_n$ la probabilité que la population soit éteinte à la génération $n$: $x_n = \Pr\{Z_n = 0\}$.
	
	On remarque que l'événement $M$: "la population finira par s'éteindre" est la réunion de la suite des événements $(\{Z_n = 0\})_{n\in \N}$ qui est croissante \textblue{(car $Z_n = 0 \Rightarrow Z_{n+1} = 0$)}. On a donc, 
	\begin{displaymath}
	\Pr(M) = \Pr\left(\bigcup_{n = 0}^{\infty} \{Z_n = 0\} \right) = \lim_{n \rightarrow \infty} x_n = x_{\infty}
	\end{displaymath}
	
	Notons que si $p_0 = 1$, alors on a $\forall n \in \N^{*}$, $Z_n = 0$ presque partout et $\Pr(M) = 1$. De plus, si $p_0 = 0$, alors on a $\forall n \in \N^{*}$, $Z_n \geq 1$ presque partout et $\Pr(M) = 0$. On suppose donc désormais $p_0 \in ]0, 1[$.
	
	\paragraph{Étape 2: propriétés de la fonction génératrice, $G$, de $X$  (\textbrown{uniquement dans le cas d'une leçon de la convexité}) \cite[exercice 3.5, q.2]{CottrelGenonDuhamelMeyre}.}
	
	\begin{prop}[Résultats sur la série génératrice de $X$]
		Soit $G$ a série génératrice de $X$ définie comme précédemment. On a alors les résultats suivants: 
		\begin{enumerate}
			\item $G$ est bien définie sur $[0, 1]$ et est de classe $\mathcal{C}^1$.
			\item \begin{enumerate}
				\item $G$ est strictement croissante sur $]0, 1[$.
				\item $G$ est convexe sur $]0, 1[$.
				\item $G$ est strictement convexe sur $]0, 1[$ si et seulement si $p_0 + p1 < 1$
			\end{enumerate}
		\end{enumerate}
		\label{prop:G}
	\end{prop}
	\begin{proof}
		\begin{enumerate}
			\item $\forall k \in \N, s \mapsto p_ks^k$ est de classe $\Cun$ sur $[0, 1]$, la série $\sum_{k \geq 0} p_k1^k$ converge (vers $1$ \textblue{(car la $\sum_{k \geq 0} p_k1^k = \sum_{k \geq 0} p_k = 1$ car les $p_k$ sont des probabilité sur $X$)}). On en déduit, comme $\sum_{k \geq 0} p_ks^k < \sum_{k \geq 0} p_k1^k$, la convergence simple de la suite $\sum_{k \geq 0} p_ks^k$.
			
			La série de fonctions $\sum_{k \geq 1} kp_ks^{k-1}$ converge normalement \textblue{($\sum_{k \geq 1} \sup_{s \in [0, 1]} \left|kp_ks^{k-1}\right| = \sum_{k \geq 1} kp_k < \infty$ car $X$ est intégrable, par hypothèse)} donc la série dérivée uniformément sur $[0,1]$.
			
			Par conséquent \textblue{(par le théorème de dérivation terme à terme)}, la série $\sum_{k \geq 0} p_ks^k$ converge uniformément vers $G$, de classe $\Cun$ sur $[0, 1]$.
			
			\item La série entière $\sum_{k \geq 0} p_ks^k$ ayant un rayon de convergence $\geq 1$, on a $\forall s \in [0, 1[$,
			\begin{displaymath}
			\begin{array}{ccc}
			G'(s) = \sum_{k=1}^{\infty}kp_ks^{k-1} & \text{ et } & G''(s) = \sum_{k=2}^{\infty}k(k-1)p_ks^{k-2} \\
			\end{array}
			\end{displaymath}
			Comme $p_0 < 1$ \textblue{(par hypothèse et comme les $p_k$ sont des probabilités)}, on l'existence de $k_0 > 0$ tel que $p_{k_0} > 0$.
			\begin{enumerate}
				\item Ainsi: $\forall s \in ]0, 1[$, 
				\begin{displaymath}
				G'(s) \underbrace{=}_{\text{Définition}} \sum_{k=1}^{\infty}kp_ks^{k-1} \underbrace{\geq}_{\text{somme de termes positifs}} k_0p_{k_0}s^{k_0-1} \underbrace{>}_{k_0 \neq 0} 0
				\end{displaymath}
				Donc, par la caractérisation par dérivée, $G$ est strictement croissante sur $]0, 1[$.
				
				\item Ainsi: $\forall s \in ]0, 1[$, 
				\begin{displaymath}
				G''(s) \underbrace{=}_{\text{Définition}} \sum_{k=2}^{\infty}k(k-1)p_ks^{k-2} \underbrace{\geq}_{\text{somme de termes positifs}} k_0(k_0-1)p_{k_0}s^{k_0-2} \underbrace{\geq}_{(k_0-1) \geq 0} 0
				\end{displaymath}
				Donc, par la caractérisation par dérivée seconde, $G$ est convexe sur $]0, 1[$.
				
				\item Si $p_0 + p_1 = 1$, alors \textblue{(comme les $p_k$ sont des probabilités)} $G(s) = p_0 + p_1s$ est affine donc n'est pas strictement convexe.
				
				Sinon, $p_0 + p_1 < 1$, alors il existe $k_0 > 1$ tel que $p_{k_0} > 0$ \textblue{(car les $p_k$ sont des probabilités)} et $G'' > 0$ \textblue{(car $k_0 - 1 \neq 0$)} sur $]0, 1[$, d'où la stricte convexité et l'équivalence.
			\end{enumerate}
		\end{enumerate}		
	\end{proof}
	
	\paragraph{Étape 3: définition d'une fonction caractéristique pour $Z_n$  (\textbrown{uniquement dans le cas d'une leçon de probabilité}) \cite{Appel}.} Pour $n \in \N$, on définit la série génératrice de $Z_n$ par $G_n : s \mapsto \E\left[s^{Z_n}\right] = \sum_{k = 0}^{\infty} \Pr\left(Z_n = k\right)s ^k$. Comme précédemment, on peut montrer que $G_n$ est bien définie sur $[0, 1]$.
	
	\begin{prop}
		On a $\forall n \in \N^{*}$, $G_n = \underbrace{G \circ \dots \circ G}_{n \text{ fois}}$ sur $[0,1]$.
		\label{prpo:Gn}
	\end{prop} 
	\begin{proof}
		La preuve se fait par récurrence sur $n$.
		\begin{description}
			\item[Cas $n =1$] On a $Z_1 = X_{0,0}$ suivant une loi de $X$. Donc 
			\begin{displaymath}
			G_1 \underbrace{=}_{\text{def}} \E\left[s^{Z_1}\right] \underbrace{=}_{Z_1 = X_{0,0}} \E\left[s^{X_{0,0}}\right] \underbrace{=}_{X_{0,0}\text{ suit une loi de }X} \E\left[s^{X}\right] \underbrace{=}_{\text{def}} G
			\end{displaymath}.
			
			\item[Cas $n+1$] Soit $n \in \N^{*}$ tel que $G_n = \underbrace{G \circ \dots \circ G}_{n \text{ fois}}$ sur $[0,1]$. Montons que $G_{n+1} = \underbrace{G \circ \dots \circ G}_{n +1 \text{ fois}}$ sur $[0,1]$. Soit $s \in [0,1]$, on a: 
			
			\begin{tabular}{ccll}
				$G_{n+1}(s)$ & $=$ & $\E\left[s^{Z_{n+1}}\right]$ & \textblue{(définition de $G_n$)} \\
				& $=$ & $\E\left[s^{\sum_{i=1}^{Z_n}X_{i, n}}\right]$ & \textblue{(définition de $Z_{n+1}$)} \\
				& $=$ & $\E\left[\prod_{i = 1}^{Z_n}s^{X_{i, n}}\right]$ & \textblue{(propriété de la puissance)} \\
				& $=$ & $\E\left[\sum_{j=0}^{\infty}\un_{\{Z_n = j\}}\prod_{i = 1}^{j}s^{X_{i, n}}\right]$ & \textblue{(découper l'espérance)} \\
				& $=$ & $\sum_{j=0}^{\infty}\E\left[\un_{\{Z_n = j\}}\prod_{i = 1}^{j}s^{X_{i, n}}\right]$ & \textblue{(Fubini--Tonelli, car les termes sont tous positifs)} \\
				& $=$ & $\sum_{j=0}^{\infty}\E\left[\un_{\{Z_n = j\}}\right]\prod_{i = 1}^{j}\E\left[s^{X_{i, n}}\right]$ & \textblue{(indépendance de toutes les viables et lemme~\ref{lem:independance})} \\
				& $=$ & $\sum_{j=0}^{\infty}\Pr\left(Z_n = j\right)\prod_{i = 1}^{j}\E\left[s^{X}\right]$ & \textblue{(espérance d'une indicatrice et $X_{i, n}$ suit une loi de $X$)} \\
				& $=$ & $\sum_{j=0}^{\infty}\Pr\left(Z_n = j\right)\E\left[s^{X}\right]^j$  & \textblue{(produit)} \\
				& $=$ & $\sum_{j=0}^{\infty}\Pr\left(Z_n = j\right)G(s)^j$  & \textblue{(définition de $G$)} \\
				& $=$ & $G_n(G(s))$ & \textblue{(hypothèse de récurrence et formule de transfert)} \\
			\end{tabular}
			D'où le résultat.
		\end{description}
	\end{proof}
	
	\paragraph{Étape 4: calcul de la probabilité d'extinction $x_{\infty}$ \cite{Appel}}
	
	\begin{prop}
		La probabilité d'extinction $x_{\infty}$ est le plus petit point fixe de $G$ sur l'intervalle $[0,1]$.
		\label{prop:probaExtinction}
	\end{prop}
	\begin{proof}
		La proposition~\ref{prpo:Gn} nous assure que $\forall n \in \N$, $\forall s \in [0,1]$, $G_{n+1} = G\left(G_n(s)\right)$. En évaluant en $0$, \textblue{(comme $G_n = x_n \forall n \in \N^*$ par définition)} on obtient $x_{n+1} = G\left(x_n\right)$. Par continuité de $G$ sur $[0, 1]$, on obtient que $x_n$ est un point fixe de $G$ \textblue{(proposition~\ref{prop:G} et passage à la limite)}. Reste à monter que c'est le plus petit.
		
		Soit $u \in [0, 1]$ un point fixe de $G$. Montrons par récurrence sur $n \in \N^*$ que $x_n \leq u$.
		\begin{description}
			\item[Cas $n=1$] On a
			\begin{displaymath}
			x_1 \underbrace{=}_{x_{n+1} = G\left(x_n\right)} G\left(x_0\right) \underbrace{=}_{\text{Définition de } x_0} G\left(\Pr\left(Z_0 = 0\right)\right) \underbrace{=}_{p_0 > 0} G(0) \underbrace{\leq}_{\text{Croissance de } G} G(u) \underbrace{=}_{\text{Point fixe}} u
			\end{displaymath}
			
			\item[Cas $n +1$] Soit $n \in \N^{*}$ tel que $x_n \leq u$. Montrons que $x_{n+1} \leq u$. Par la croissance de $G$ \textblue{(proposition~\ref{prop:G})}, on a $x_{n+1} = G\left(x_n\right) \leq G(u) =u$.
		\end{description}
		On a donc $\forall n \in \N^*$ que $x_n \leq u$, puis par passage à la limite $x_{\infty} \leq u$.
	\end{proof}
	
	\begin{theo}
		Si $m \leq 1$, alors $x_{\infty} = 1$.
		
		Si $m > 1$, alors $x_{\infty}$ est l'unique point fixe de $G$ sur $]0, 1[$.
	\end{theo}
	\begin{proof}
		On rappelle qu'on a deux cas:
		\begin{itemize}
			\item Si $p_0 + p_1 = 1$, alors $G$ est une fonction affine. Comme $p_0 > 0$, la droite représentative de $G$ coupe en un unique point la droite d'équation $y = x$. Nécessairement, ce point d'intersection a pour coordonnée $(1,1)$.
			\item Sinon, $G$ est strictement convexe sur $]0, 1[$; il en va de même pour $x \mapsto G(x) -x$ qui s'annule donc au plus deux fois. \textblue{(En effet, si la fonction s'annule trois fois, par le théorème de Rolle, sa dérivée s'annulera en deux points distincts, $a < b$. Mais $x \mapsto G(x) - x$ est connexe, donc sa dérivée est croissante, donc nulle sur $[a, b]$. Ceci implique que $G' - 1$ n'est pas strictement croissante, et donc $x \mapsto G(x) - x$ n'est pas strictement convexe.)}
			
			\textred{\textbf{Attention}} à l'idée reçue "strictement convexe" équivaut à "dérivée seconde strictement positive" (pour des fonctions deux fois dérivables).
		\end{itemize}
		Dans tous les cas, on a: $G(x) -x$ qui s'annule en au plus deux points sur $[0, 1]$. De plus, $G'(0) = p_1$ et $G'(1) = \sum_{n=1}^{\infty} np_n = m$.
		
		\begin{description}
			\item[Supposons $m > 1$] Alors $G' - 1$ est une fonction croissante de $p_1 - 1 < 0$ (car $p_0 > 0$) à $m-1> 0$, donc elle s'annule en un point $\alpha \in ]0, 1[$. La fonction $G - Id$ est alors décroissante sur $[0, \alpha]$ puis croissante sur $[\alpha, 1]$. Comme $G(0) = 0 = p_0 > 0$ et $G(1) - 1 = 0$, il existe un point dans l'intervalle $]0, \alpha]$ où $G- Id$ s'annule. (Voir tableau de variation, figure~\ref{tab:m>1})
			
			$x_{\infty}$ est donc l'unique point fixe de $G$ sur l'intervalle $]0, 1[$ (car $G$ en a au plus $2$).
			
			\begin{figure}
				\begin{minipage}[c]{.46\linewidth}
					\centering
					\begin{tikzpicture}
					\tkzTabInit{$x$ / 1 , $G'(x) - 1$ / 1, $G(x) - x$ /2 }{$0$, $\alpha$, $1$}
					\tkzTabLine{p_1 -1,- , z, +, m-1}
					\tkzTabVar{+ / $p_0$, -/, +/0}
					\tkzTabVal{1}{2}{0.5}{$x_{\infty}$}{0} % On place 0, son antécédent est pi / 2.
					\end{tikzpicture}
					\caption{Tableau de signe et de variation de $x \mapsto G(x) -x$ dans le cas où $m > 1$.}
					\label{tab:m>1}
				\end{minipage} \hfill
				\begin{minipage}[c]{.46\linewidth}
					\centering
					\begin{tikzpicture}
					\tkzTabInit{$x$ / 1 , $G'(x) - 1$ / 1, $G(x) - x$ /2 }{$0$, $1$}
					\tkzTabLine{p1 -1,-, 0}
					\tkzTabVar{+ / $p_0$,-/0}
					\end{tikzpicture}
					\caption{Tableau de signe et de variation de $x \mapsto G(x) -x$ dans le cas où $m \leq 1$.}
					\label{tab:m<1}
				\end{minipage}
			\end{figure}			
			
			\item[Supposons $m \leq 1$] Alors $G' - 1$ est une fonction croissante sur $[0, 1]$, négative ou nulle en $1$. La fonction $G - Id$ est alors décroissante sur $[0, 1]$, et s'annule en $1$ (voir figure~\ref{tab:m<1}). Comme cette fonction admet au plus deux annulations, elle ne s'annule qu'en $1$ (sinon elle s'annulerait sur un intervalle non réduit à un singleton). Par conséquent $x_{\infty} = 1$.
		\end{description}
	\end{proof}
	
	\section{Compléments: suites et séries de fonctions}
	
	\input{./../Notions/SeriesFonctions_convergence.tex}
	\input{./../Notions/SeriesFonctions_regularite.tex}
	
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}