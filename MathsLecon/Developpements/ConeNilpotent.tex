\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}


\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
} 

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\Cun}{\mathcal{C}^1}
	\newcommand{\Cdeux}{\mathcal{C}^2}
	\newcommand{\Cinf}{\mathcal{C}^{\infty}}
	\newcommand{\F}{\mathbb{F}}
	
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{OliveGreen}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	\newcommand{\textmagenta}[1]{\textcolor{DarkOrchid}{#1}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\newcommand{\pcite}[1]{\protect{\cite{#1}}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	
	\title{Cardinal du cône nilpotent}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} H2G2  \cite[p.213]{Caldero-Germoni2}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 101 (Action de groupe); 104 (Groupe fini); 106 (Groupe linéaire); 157 (Triangulaire et nilpotent).
		}}
	
	\section{Introduction}
	
	Le cône nilpotent des matrices $d \times d$ nilpotentes (c'est bien un cône par sa stabilité par la multiplication et son instabilité par addition), noté $\mathcal{N}_d = \mathcal{N}_d(K)$, sur un corps $K$ est un objet convoité en géométrie algébrique, en théorie de Lie ou encore en théorie des représentations. 
	
	Par un calcul heuristique, via l'orbite du bloc de Jordan de $\mathcal{N}_d(q)$ construite par l'action de 
	$GL_n(\F_q)$ sur $\mathcal{N}_d(\F_q)$ par conjugaison, on obtient que $\left|\mathcal{N}_d(\F_q)\right| = q^{d^2 - d}$. Cette valeur est la valeur exacte du cardinal du cône et cette justesse d'estimation s'explique par le fait que $\mathcal{N}_d(\F_q)$ est une variété algébrique.
	
	L'idée la plus naturelle pour calculer $\left|\mathcal{N}_d(\F_q)\right|$ est de partitionner $\mathcal{N}_d(\F_q)$ selon les orbites de l'action de $GL_n(\F_q)$ par conjugaison sur $\mathcal{N}_d(\F_q)$. On pourra ainsi sommer les classes de similitude. Cependant, ce calcul est fastidieux et la somme ne se simplifie pas facilement. La méthode que nous choisissons d'appliquer est inspirée de la désingularisation de Springer.
	
	\section{Calcul du cardinal du cône nilpotent sur $\F_q$}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement (leçons 101 et 157)}}}{\begin{enumerate}
				\item Énoncer le théorème et la proposition que nous admettons.
				\item Premier calcul via la première composante : $\left|X\right| = n_d(q^d - 1)$.
				\item  Deuxième calcul via la deuxième composante : $\left|X\right| = \sum_{r=1}^{d} \frac{g_d}{g_{d-r}}n_{d-r}$.
				\item Comparaison des deux calculs.
			\end{enumerate}
	}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement (leçons 104 et  106)}}}{\begin{itemize}
			\item Proposition: famille libre induite par $N$ 
			\begin{enumerate}
				\item $\mathcal{E} = \left(e, Ne, \dots, N^{r-1}e\right)$ est une base de $F  = \left<N^se, s \in \N \right>$.
				\item  $N^re = 0$ (prendre une restriction qui stabilise $F$).
			\end{enumerate}
			\item Théorème : cardinal du cône \textblue{(à écrire au début, avant même l'énoncer de la proposition)}
			\begin{enumerate}
				\item Premier calcul via la première composante : $\left|X\right| = n_d(q^d - 1)$.
				\item  Deuxième calcul via la deuxième composante : $\left|X\right| = \sum_{r=1}^{d} \frac{g_d}{g_{d-r}}n_{d-r}$ \textblue{(calculs rapides)}.
				\item Comparaison des deux calculs \textblue{(calculs rapides)}.
			\end{enumerate}
		\end{itemize}
	}

	\begin{prop}
		Soient $E$ un $K$-espace vectoriel où $K$ est un corps et $N \in \mathcal{N}_d(E)$ et $e$ un vecteur non nul de $E$. On note $r$ le nombre maximal tel que $\mathcal{E} = \left(e, Ne, \dots, N^{r-1}e\right)$ est une famille libre. On a alors, $N^re = 0$.
	\end{prop}
	\begin{proof}
		Soit $F = \left<N^se, s \in \N \right>$ le sous-espace vectoriel engendré par $e$ et $N$.
		\paragraph{Étape 1: Montrons que $\mathcal{E}$ est une base $F$}
		\begin{itemize}
			\item $\mathcal{E}$ est une famille libre dans $F$ \textblue{(par construction sinon problème avec la définition de $r$)}.
			\item $\mathcal{E}$ est une famille génératrice dans $F$. Montrons par récurrence sur $k \in \N$ que $N^{r+k}e \in \mathrm{Vect}(\mathcal{E})$. \textblue{(On a besoin de la récurrence car la définition nous assure de la suite des itérés jusqu'à $N^re$ est liée (et libre juste avant) mais ne dit rien sur la famille des itérés jusqu' $N^{r-1}e$ à laquelle on ajoute une itérée quelconque.)}
			\begin{description}
				\item[Cas $k = 0$] Comme la famille $\left(e, Ne, \dots, N^{r-1}e, N^re\right)$ est liée \textblue{(sinon contradiction avec la définition de $r$)}, on peut écrire $N^r e = -\sum_{i = 0}^{r-1} a_iN^ie$ pour $a_0, \dots, a_{r-1}$ non tous nuls \textblue{(le caractère liant de la famille impose $a_r \neq 0$ (on peut donc le supposer égal à $1$) car sinon on contredirait la liberté de la famille $\mathcal{E}$)}.
				\item[Cas $k \in \N$] Soit $k \in \N$ tel que pour tout $j < k$, $N^{r + j} \in \mathrm{Vect}(\mathcal{E})$, montrons que $N^{r + k} \in \mathrm{Vect}(\mathcal{E})$. Notons $s = r + k$, on a alors 
				\begin{displaymath}
				\begin{array}{ccll}
				N^se & = & N^{k +r}e & \textblue{(s = k + r)}\\
				& = & N^kN^re & \textblue{\text{(par manipulation des puissances)}} \\
				& = & N^k\left(-\sum_{i = 0}^{r - 1} a_iN^ie\right) & \textblue{(\text{par le cas } k = 0)} \\
				& = & -\sum_{i = 0}^{r - 1} a_iN^kN^ie & \textblue{(\text{par linéairité de } N)} \\
				& = & -\sum_{i=0}^{r-1} a_iN^{k + i}e & \textblue{\text{(par manipulation des puissances)}} \\
				& \in & \mathrm{Vect}(\mathcal{E}) & \textblue{\text{(par hypothèse de récurrence forte)}} \\
				\end{array}
				\end{displaymath}
			\end{description}
		\end{itemize}
		
		\paragraph{Étape 2: Montrons que $N^re = 0$}
		\begin{itemize}
			\item $N$ stabilise $F$: soit $x \in F$, alors $Nx = NN^se = N^{s+1}e \in F$ pour un certain $s$. On note $\tilde{N} = N_F$ l'endomorphisme induit par la restriction de $N$ à $F$.
			\item Expression de la matrice de $\tilde{N}$ dans la base $\mathcal{E}$: 
			
			\begin{minipage}{.42\textwidth}
				$\mathrm{Mat}_{\mathcal{E}}\left(\tilde{N}\right) = \begin{pmatrix}
				0 & \cdots & \cdots & 0 & a_0 \\
				1 & \ddots & & \vdots & a_ 1 \\
				0 & \ddots & \ddots & \vdots & \vdots \\
				\vdots & \ddots & \ddots & 0 & \vdots \\
				0 & \cdots & 0 & 1 & a_{r-1} \\
				\end{pmatrix}$
			\end{minipage} \hfill
			\begin{minipage}{.5\textwidth}
				\textblue{ 
					\begin{itemize}
						\item $\tilde{N}(e) = Ne$
						\item $\forall s \in \llbracket 0, r - 2 \llbracket$, $\tilde{N}\left(N^se\right) = N^sNe = N^{s + 1}e$
						\item $\tilde{N}\left(N^{r-1}e\right) = N^re = - \sum_{i = 0}^{r-1}a_iN^ie$
					\end{itemize}}
				On reconnaît une matrice compagnon du polynôme $P = X^r + \sum_{i = 0}^{r-1}a_iX^i$ de polynôme caractéristique (qui est également minimal) $P$.
			\end{minipage}
			
			
			\item Comme $\tilde{N}^re = N^re$ \textblue{(car restriction sur $F$ de $N$)} et $\tilde{N}^r = 0$ \textblue{(car $\tilde{N}$ est nilpotente d'indice $r$, donné par son polynôme caractéristique)}, $N^r = 0$.
		\end{itemize}
	\end{proof}
	
	\begin{theo}
		Pour tout corps fini $\F_q$ de cardinal $q$ et tout entier $d$, on a $\left|\mathcal{N}_d(\F_q)\right| = q^{d(d-1)}$.
	\end{theo}
	\begin{proof}
		Soit $E$ un $\F_q$-espace vectoriel de dimension $d$, on pose $\mathcal{N}_d = \mathcal{N}_d(\F_q)$ l'ensemble des matrices nilpotentes de $E$. Soit $L_{r, d}$ l'ensemble des parties libres de $E$ à $r$ éléments. On dit que $N$ respecte une famille $\mathcal{E} = \left(e_1, \dots, e_r\right)$ de $L_{r, d}$ si $\forall s \in \llbracket 1, r \rrbracket$, $Ne_s = e_{s+1}$ et $N_{r+1} = 0$.
		
		Posons $n_d = \left|\mathcal{N}_d\right|$ et donnons une formule de récurrence pour exprimer $n_d$. On calcul alors de deux manières différentes le cardinal de l'ensemble $\{(N, e) ~|~ N \in \mathcal{N}_d, \exists r \in \{1, \dots, d\} \text{ tel que } r \text{ respecte } \mathcal{E}\}$. On note $\pi_1$ (respectivement $\pi_2$) la projection sur la première (respectivement la seconde) composante.
		
		\paragraph{Premier calcul}:
		\begin{itemize}
			\item $\left|X\right| = \sum_{N \in \mathcal{N}_d} \left|\pi^{-1}_1(N)\right|$ \textblue{(en partitionnant $\mathcal{N}_d$ en fonction des valeurs de $e$ dans $X$: $\left|X = \{r, b\}\right| = \sum_{N \in \mathcal{N}_d} \left|\pi_1^{-1}(a, b)\right|$)}.
			\item $\left|X\right| = n_d(q^d - 1)$ \textblue{(la proposition nous assure l'existence d'une bijection entre $E \setminus \{0\}$ et les familles libres respectée par $N$)}.
		\end{itemize}
		
		\paragraph{Deuxième calcul}: Par construction de l'ensemble $X$, on a:
		\begin{displaymath}
		\left|X\right| = \underbrace{\sum_{r = 1}^{d}}_{\textblue{\text{test l'ensemble des } r}} \underbrace{\sum_{e \in L_{r, d}}}_{\textblue{\text{condition: } e \in X}} \left|\pi^{-1}_2(e)\right|
		\end{displaymath}
		On cherche à simplifier cette somme.
		\begin{itemize}
			\item Soit $1 \leq r \leq d$, notons $g_r$ l'ordre de $GL_r(\F_q)$. L'action de $GL(E)$ sur $L_{r, d}$ est transitive par le théorème de la base incomplète.
			\begin{itemize}
				\item $\mathcal{E}$ peut être complété en une base de $E$.
				\item $\forall \mathcal{E}, \mathcal{E}' \in L_{r, d}$, $\exists g \in GL(E)$ $g . \mathcal{E} = \mathcal{E}'$ \textblue{(on complète $\mathcal{E}$ en une base, un changement de base nous donne $\mathcal{E}'$ complété en une base)}.
			\end{itemize}
			Donc $\left|\mathrm{Orb}(\mathcal{E})\right| = \left|L_{r, d}\right|$.
			
			\item Soit $\mathcal{E} \in L_{r, d}$ et $\tilde{\mathcal{E}}$ sa base complété de $\mathcal{E}$. On raisonne maintenant sur cette base. On a $\mathrm{Stab}(\mathcal{E}) = \{\begin{pmatrix} I_r & M \\ 0 & B \\ \end{pmatrix} ~|~ M \in \mathcal{M}_{r, d-r}(\F_q), B \in GL_{d-r}(\F_q)\}$ par définition du stabilisateur. Par la relation d'orbite stabilisateur, $\left|L_{r, d}\right| = \frac{\left|GL(E)\right|}{\left|\mathrm{Stab}(\mathcal{E})\right|} = \frac{g_d}{g_{d-r}q^{r(d-r)}}$ car $g_d$ est l'ordre de $GL(E)$, $g_{d-r}$ celui de $GL_{d-r}(\F_q)$ et $q^{r(d-r)}$ celui de $\mathcal{M}_{r, d-r}(\F_q)$.
			
			\item Une matrice nilpotente $N$ respecte $\mathcal{E}$ si et seulement si la matrice de l'application linéaire associée à $N$ dans une base $\tilde{\mathcal{E}}$ est de la forme $\mathrm{Mat}_{\tilde{\mathcal{E}}} = \begin{pmatrix}
			J_r & M \\
			0 & N_{d-r} \\
			\end{pmatrix}$ où $J_r$ est le bloc de Jordan de taille $n \times r$, $M \in \mathcal{M}_{r, d- r}(\F_q)$ et $N_{d-r} \in \mathcal{N}_{d-r}(\F_q)$ \textblue{(la préservation provient du bloc de Jordan de taille $r$)}.
		\end{itemize}
		On a alors $\left|X\right| = \sum_{r = 1}^{d} \left|L_{r, d}\right|q^{r(d-r)}n_{d-r}$ \textblue{(car $\left|\pi{-1}_2\left(\mathcal{E}\right)\right| = \underbrace{q^{r(d-r)}}_{\left|\mathcal{M}_{r, d-r}\right|} \underbrace{n_{d-r}}_{\left|\mathcal{N}_{r, d-r}\right|}$)}. D'où en remplaçant par la valeur de $\left|L_{r, d}\right|$, $\left|X\right| = \sum_{r=1}^{d} \frac{g_d}{g_{d-r}}n_{d-r}$.
		
		\paragraph{Comparaison des deux calculs} On a alors $n_d(q^d - 1) = \sum_{r=1}^{d} \frac{g_d}{g_{d-r}} n_{d-r}$. Donc en divisant par $g_d > 0$, on obtient
		\begin{displaymath}
		\begin{array}{ccll}
		\frac{n_d}{g_d}(q^d - 1) & = & \sum_{r = 1}^{d} \frac{n_{d-r}}{d_{d-r}} & \textblue{(\text{divsion})} \\
		& = & \sum_{r = 0}^{d-1} \frac{n_{r}}{d_{r}} & \textblue{(\text{changement d'indice dans la somme})} \\
		& = & \frac{n_{d - 1}}{d_{d- 1}} + \sum_{r = 0}^{d-2} \frac{n_{r}}{d_{r}} & \textblue{(\text{on sort le dernier terme})} \\
		& = & \frac{n_{d - 1}}{d_{d- 1}} + \frac{n_{d-1}}{d_{d-1}}(q^{d-1} - 1) & \textblue{(\text{récurrence sur la formule})} \\
		& = & \frac{n_{d-1}}{d_{d-1}}(q^{d - 1}) & \textblue{(\text{factorisation})} \\
		\end{array}
		\end{displaymath}
		On en déduit que $\frac{n_d}{g_d} = \frac{q^{d(d-1)/2}}{\prod_{r = 1}^{d} (q^r - 1)}$ \textblue{(par résolution de la récurrence)}. Puis, $\frac{n_d}{g_d} = \frac{q^{d(d-1)}}{g_d}$ \textblue{(car $g_d = \prod_{r = 1}^{d} (q^r - 1)q^{d(d-1)/2}$, on compte le nombre de possibles)}. Finalement, $n_d = q^{d (d- 1)}$.
	\end{proof}
	
	\section{Compléments autours de ce développement}
	
	\subsection*{Cardinal des ensembles de matrices dans des espaces vectoriels sur corps finis}
	\input{./../Notions/Denombrement_CorpsFini_matrice.tex}
	
	
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}

	
\end{document}