\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}


\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\E}{\mathbb{E}}
	\newcommand{\D}{\mathcal{D}}
	\newcommand{\A}{\mathfrak{A}}
	
	\sloppy
	
	\newcommand{\textblue}{\textcolor{blue}}
	\newcommand{\textgreen}{\textcolor{OliveGreen}}
	\newcommand{\textred}{\textcolor{red}}
	\newcommand{\textbrown}{\textcolor{Brown}}
	\newcommand{\textmagenta}{\textcolor{DarkOrchid}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\newtheorem*{appli}{Application}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Théorème de Weierstrass via les polynômes de Bernstein}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Barde--Ledoux \cite[p.89]{BardeLedoux} et Zully--Queffelec \cite[p.518]{ZuilyQueffelec}.
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 260 (Espérance et variance); 264 (Variables aléatoires discrètes)	.
			
			\textblue{\emph{Leçons où il peut être évoqué:}}  228 (Continuité et dérivabilité).
		}}
	
	\section{Introduction}
	
	Un célèbre théorème de Weierstrass affirme que toute fonction continue sur le segment $[0, 1]$ est limite uniforme sur ce segment d'une suite de polynômes. Bernstein, à l'aide des polynômes qui porte son nom apporte une nouvelle démonstration à ce théorème obtenu initialement à l'aide de la convolution. 
	
	De plus, les polynômes de Bernstein donne une démonstration constructive pour laquelle la convergence de la suite est optimale (on ne le montrera pas durant ce développement). C'est un phénomène bien connu: les polynômes de Bernstein épousent bien les propriétés qualitatives de $f$ (continuité, croissance, convexité, ...), mais converge lentement vers ceux-ci car ils n'oscillent pas assez autour de $f$ comme les polynômes de Tchebychev par exemple.
	
	L'objectif du développement est donc de montrer le théorème suivant:
	\begin{theo}[Weierstrass]
		Pour tout $f: [0,1] \to \R$ continue, il existe une suite de polynôme convergeant uniformément vers $f$ sur $[0, 1]$.
	\end{theo}	
	
	\section{Le théorème de Weierstrass}
	
	\begin{theo}
		Soit $f : [0, 1] \to \R$ une fonction continue, $\omega$ son module de continuité uniforme, i.e. $\omega(h) = \sup \{|f(u) - f(v)|; |u - v| \leq h\}$. Pour $n \leq 1$, on considère le polynôme \[ B_n(f, x) = B_n(x) = \sum_{k=0}^{n}
		\left(\!\!\!\begin{array}{c}
		n \\
		r
		\end{array}
		\!\!\!\right) (1-x)^{n-k}f\mathopen{}\left ( \frac{k}{n} \right )\mathclose{}
		\], le $n^{\mathrm{ième}}$ polynômes de Bernstein de $f$. Alors:
		\begin{enumerate}
			\item $B_n$ converge uniformément vers $f$ sur $[0, 1]$;
			\item Plus précisément, on a $\mathopen{||}f - B_n \mathclose{||}_{\infty}	\leq C\omega\mathopen{}\left ( \frac{1}{\sqrt{n}} \right )\mathclose{}$.
		\end{enumerate}
	\end{theo}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement}}}{
		\begin{enumerate}
			\item Énoncer quelques propriétés sur le module de continuité.
			\item Montrer que $B_n$ est un polynôme en $x$.
			\item Montrer que pour tout $x$, $\left|f - B_n\right|_{\infty}$ est majoré par $\omega(\delta) + \frac{\mathopen{||} f \mathclose{||}_{\infty}}{2n\delta^2}$.
			\item Montrer la convergence uniforme de $B_n$ vers $f$.
			\item Montrer que $\mathopen{||}f - B_n \mathclose{||}_{\infty}	\leq C\omega\mathopen{}\left ( \frac{1}{\sqrt{n}} \right )\mathclose{}$.
		\end{enumerate}
	}
	
	\begin{proof}
		Remarquons d’abord que $\omega$ est bien définie puisque, selon le théorème de Heine, $f$ est uniformément continue sur $[0, 1]$, ce qui assure de plus que $\lim_{h \to 0}\omega(h) = 0$.
		
		\paragraph{Étape 1 : énoncer quelques propriétés sur le module de continuité} Commençons par rappeler quelques propriétés \textblue{(que nous ne montrons pas)} sur le module de continuité avant de rentré dans le vif du sujet.
		\begin{lemme}
			Le module de continuité uniforme de $f$, $\omega$ vérifie les propriétés suivantes:
			\begin{enumerate}
				\item $\omega$ est croissante.
				\item $\forall h_1, h_2 \in [0, 1]$, $\omega(h_1 + h_2) \leq \omega(h_1) + \omega(h_2)$.
				\item $\forall h \in [0, 1]$, $\lambda \in \R_+$ tels que $h\lambda \in [0, 1]$, $_\omega(\lambda h) \leq (\lambda + 1)\omega(h)$
			\end{enumerate}
		\end{lemme}
		
		Montrons maintenant le théorème en commençant par montrer que les polynômes de Bernstein $B_n$ convergent uniformément vers $f$ sur $[0, 1]$.
		
		\paragraph{Étape 2: montrons que $B_n$ est bien un polynôme en $x$.}  Soient $x \in [0, 1]$, $X_1, \dots X_n$ des variables aléatoires discrètes suivant une loi de Bernoulli $\mathcal{B}(x)$ indépendantes. On pose $S_n = \sum_{i=1}^{n} X_i$ qui suit une loi binomiale $\mathcal{B}(n, x)$ \textblue{(car la somme de $n$ variables aléatoires indépendantes suivant une loi de Bernoulli de paramètre $p$ donne une loi binomiale de paramètre $(n, p)$)}. Par la formule de transfert, on a alors: \[\E\mathopen{}\left[ f\mathopen{}\left( \frac{S_n}{n} \mathclose{}\right) \mathclose{}\right] = \sum_{k=0}^{n}
		\left(\!\!\!\begin{array}{c}
		n \\
		r
		\end{array}
		\!\!\!\right) (1-x)^{n-k}f\mathopen{}\left ( \frac{k}{n} \right )\mathclose{} = B_n\]
		qui est donc bien un polynôme en $x$.
		
		
		\paragraph{Étape 3: montrons que pour tout $x$, $\left|f - B_n\right|_{\infty}$ est majoré par $\omega(\delta) + \frac{\mathopen{||} f \mathclose{||}_{\infty}}{2n\delta^2}$.}Soit $\mathopen{||} . \mathclose{||}_{\infty}$  la norme sup sur $[0, 1]$ et $\delta \in [0, 1]$. On a $f(x) - B_n(x) = \E\mathopen{}\left[\mathopen{} f(x) - f\left(\frac{S_n}{n}\mathclose{}\right)\mathclose{}\right]$ \textblue{(par linéarité de l'espérance: comme $\E\mathopen{}\left[ f\mathopen{}\left( \frac{S_n}{n} \mathclose{}\right) \mathclose{}\right] = B_n$ et que $f(x)$ est une constante pour l'espérance)}. Ainsi, on a:
		
		\noindent\begin{tabular}{cclll}
			$\mathopen{|}f(x) - B_n(x) \mathclose{|}$ & $\leq$ & $\E\mathopen{}\left[\mathopen{} \mathopen{|} f(x) - f\left(\frac{S_n}{n}\mathclose{}\right) \mathclose{|}\mathclose{}\right]$ & \multicolumn{2}{c}{\textblue{(propriétés de l'espérance)}} \\
			& $\leq$ & \multicolumn{2}{l}{$\E\mathopen{}\left[\mathopen{} \mathopen{|} f(x) - f\left(\frac{S_n}{n}\mathclose{}\right) \mathclose{|}_{\mathbb{1}_{\mathopen{|} x - \frac{S_n}{n} \mathclose{|} \leq \delta}} + \mathopen{|} f(x) - f\left(\frac{S_n}{n}\mathclose{}\right) \mathclose{|}_{\mathbb{1}_{\mathopen{|} x - \frac{S_n}{n} \mathclose{|} > \delta}} \mathclose{}\right]$} & \textblue{(en coupant en deux)} \\
			& $\leq$ & \multicolumn{2}{l}{$\underbrace{\E\mathopen{}\left[\mathopen{} \mathopen{|} f(x) - f\left(\frac{S_n}{n}\mathclose{}\right) \mathclose{|}_{\mathbb{1}_{\mathopen{|} x - \frac{S_n}{n} \mathclose{|} \leq \delta}}\mathclose{}\right]}_{\textrm{A}} + \underbrace{\E\mathopen{}\left[\mathopen{|} f(x) - f\left(\frac{S_n}{n}\mathclose{}\right) \mathclose{|}_{\mathbb{1}_{\mathopen{|} x - \frac{S_n}{n} \mathclose{|} > \delta}} \mathclose{}\right]}_{\textrm{B}}$}  & \textblue{(inégalité triangulaire)} \\
		\end{tabular}
		On cherche donc à majorer $\textrm{A}$ et $\textrm{B}$. On a, par définition du module de continuité uniforme:
		\begin{displaymath}
		\textrm{A} \leq \omega(\delta)
		\end{displaymath}
		Par une majoration brute de $f$, on obtient par la linéarité de l'espérance:
		\begin{displaymath}
		\textrm{B} \leq 2 \mathopen{||}f\mathclose{||}_{\infty}\E\mathopen{}\left[\mathbb{1}_{\mathopen{|} x - \frac{S_n}{n} \mathclose{|} > \delta}\mathclose{}\right]
		\end{displaymath}
		Soit
		
		\begin{tabular}{ccll}
			$\mathopen{|}f(x) - B_n(x) \mathclose{|}$ & $\leq$ & $\omega(\delta) + 2 \mathopen{||}f\mathclose{||}_{\infty}\E\mathopen{}\left[\mathbb{1}_{\mathopen{|} x - \frac{S_n}{n} \mathclose{|} > \delta}\mathclose{}\right]$ & \textblue{(par les calculs précédents)} \\
			& $\leq$ & $\omega(\delta) + 2 \mathopen{||}f\mathclose{||}_{\infty}\mathbb{P}\mathopen{}\left(\mathopen{|} x - \frac{S_n}{n} \mathclose{|} > \delta \mathclose{}\right)$ & \textblue{(par l'espérance d'une indicatrice)} \\
		\end{tabular}
		On souhaite alors majorer $\mathbb{P}\mathopen{}\left(\mathopen{|} x - \frac{S_n}{n} \mathclose{|} > \delta \mathclose{}\right)$. Par l'inégalité de Tchebychev \cite[p.60]{BardeLedoux}, on obtient:
		
		\begin{tabular}{ccll}
			$\mathbb{P}\mathopen{}\left(\mathopen{|} x - \frac{S_n}{n} \mathclose{|} > \delta \mathclose{}\right)$ & $=$ & $\mathbb{P}\mathopen{}\left(\mathopen{|} S_n- nx \mathclose{|} > n\delta \mathclose{}\right)$ & \textblue{(par multiplication par $n$)} \\
			& $\leq$ & $\frac{\mathrm{Var}(S_n)}{(n\delta)^2}$ & \textblue{(car $\E(S_n) = nx$)} \\
			& $=$ & $\frac{nx(1 - x)}{(n\delta)^2}$ & \textblue{(car $\mathrm{Var}(S_n) = nx(1-x)$)} \\
			& $\leq$ & $\frac{1}{4n\delta^2}$ & \textblue{(car $x(1-x) \leq \frac{1}{4}$)} \\
		\end{tabular}
		
		\noindent On obtient alors:
		\begin{displaymath}
		\mathopen{|}f(x) - B_n(x) \mathclose{|} \leq \omega(\delta) +  \frac{\mathopen{||}f\mathclose{||}_{\infty}}{2n\delta^2}
		\end{displaymath}
		
		
		\paragraph{Etape 4: montrons la convergence uniforme de $B_n$ vers $f$.} On a \cite[p.60]{BardeLedoux},
		\begin{displaymath}
		\sup_{0 \leq x \leq 1}\mathopen{|}f(x) - B_n(x) \mathclose{|} \leq \inf_{\delta > 0} \mathopen{}\left(\omega(\delta) +  \frac{\mathopen{||}f\mathclose{||}_{\infty}}{2n\delta^2}\mathclose{}\right)
		\end{displaymath}
		On en déduit que \cite[p.518]{ZuilyQueffelec}:
		\begin{displaymath}
		\limsup \mathopen{||}f - B_n\mathclose{||}_{\infty} \leq \omega(\delta)
		\end{displaymath}
		Or $f$ est continue sur $[0, 1]$ compact, donc $f$ est uniformément continue donc $\lim\limits_{\delta \rightarrow 0} \omega(\delta) = 0$, on en déduit que $\lim\limits_{n \rightarrow +\infty} \mathopen{||}f - B_n\mathclose{||}_{\infty} = 0$.
		
		On montre qu'on peut contrôler cette convergence. De plus, même si on ne le montre pas ici faute de temps elle est optimale (voir la preuve dans la section~\ref{sec:opti}).
		
		\paragraph{Étape 5: montrons que $\mathopen{||}f - B_n \mathclose{||}_{\infty}	\leq C\omega\mathopen{}\left ( \frac{1}{\sqrt{n}} \right )\mathclose{}$.} On va maintenant vérifier la convergence de la suite de polynôme. 
		
		\begin{tabular}{ccll}
			$\mathopen{|}f(x) - B_n(x) \mathclose{|}$ & $\leq$ & $\E\mathopen{}\left[\mathopen{} \mathopen{|} f(x) - f\left(\frac{S_n}{n}\mathclose{}\right) \mathclose{|}\mathclose{}\right]$ & \textblue{(par ce qui précède)} \\
			& $\leq$ & $\E\mathopen{}\left[\mathopen{}  \omega\left(\underbrace{\left|x - \frac{S_n}{n}\right|}_{1 \leq}\right) \mathclose{}\right]$ & \textblue{(def de $\omega$ et croissance de l'intégrale)} \\
			& $\leq$ & $\E\left[
			\left(\underbrace{\sqrt{n}\left|x - \frac{S_n}{n}\right| + 1}_{\in \R_+}\right)\omega\left(\underbrace{\frac{1}{\sqrt{n}}}_{\in [0,1]}\right)\right]$ & \textblue{(lemme et croissance de l'intégrale)} \\
			& $\leq$ & $\omega\mathopen{}\left(\frac{1}{\sqrt{n}}\mathclose{}\right) \E\mathopen{}\left[
			\mathopen{}\left(\sqrt{n}\mathopen{|}x - \frac{S_n}{n}\mathclose{|} + 1\mathclose{}\right) \mathclose{}\right]$ & \textblue{(linéarité de l'intégrale)} \\
			& $\leq$ & $\omega\mathopen{}\left(\frac{1}{\sqrt{n}}\mathclose{}\right) 
			\mathopen{}\left(\sqrt{n}\E\mathopen{}\left[\mathopen{|}x - \frac{S_n}{n}\mathclose{|}\right] + 1\mathclose{}\right) \mathclose{}$ & \textblue{(linéarité de l'intégrale)} \\
		\end{tabular}
		
		Il nous faut donc maintenant majorer $\E\mathopen{}\left[\mathopen{|}x - \frac{S_n}{n}\mathclose{|}\right]$. 
		
		\begin{tabular}{ccll}
			$\E\mathopen{}\left[\mathopen{|}x - \frac{S_n}{n}\mathclose{|}\right]$ & $\leq$ & $\E\mathopen{}\left[\mathopen{|}x - \frac{S_n}{n}\mathclose{|}^2\right]^{1/2} \E\mathopen{}\left[1^2\right]^{1/2}$ & \textblue{(par l'inégalité de Hölder)} \\
			& $\leq$ & $\mathrm{Var} \mathopen{}\left(\frac{S_n}{n}\mathclose{}\right)^{1/2}$ & \textblue{(définition de la variance)} \\
			& $\leq$ & $\mathopen{}\left(\frac{1}{4n}\mathclose{}\right)^{1/2}$ & \textblue{(par le calcul précédent)} \\
			& $\leq$ & $\frac{1}{2\sqrt{n}}$ &  \\
		\end{tabular}
		
		On a ainsi 
		\begin{displaymath}
		\mathopen{|}f(x) - B_n(x) \mathclose{|} \leq \omega\mathopen{}\left(\frac{1}{\sqrt{n}}\mathclose{}\right) 
		\mathopen{}\left(\sqrt{n}\frac{1}{2\sqrt{n}} + 1\mathclose{}\right) \mathclose{} = \frac{3}{2}\omega\mathopen{}\left(\frac{1}{\sqrt{n}}\mathclose{}\right) 
		\end{displaymath}
		
		D'où le résultat.
	\end{proof}
	
	
	\begin{req}
		On peut étendre le théorème de Weierstrass à tout intervalle fermé borné de $\R$ en appliquant ce résultat à $g(x) = f(a + (b-a)x)$.
	\end{req}
	
	\begin{req}
		Une fonction qui est limite uniforme de fonctions polynomiales est une fonction polynomiale.
	\end{req}
	
	\section{Preuve de l'optimalité}
	\label{sec:opti}
	
	Nous avons étudier la vitesse de convergence de la suite des polynômes vers $f$. Maintenant, nous allons voir qu'elle est optimale, c'est-à-dire, il existe une fonction lipschitzienne $f$ pour laquelle $\mathopen{||}f- B_n \mathclose{||}_{\infty} \geq \frac{\delta}{\sqrt{n}}$ où $\delta$ est une constante numérique.
	
	Soit $f = \mathopen{}\left| x - \frac{1}{2} \right|\mathclose{}$. Par inégalité triangulaire renversée, on a $\omega(h) \leq h$ pour tout $h$.
	
	Soient $X_1, \dots X_n$ une suite de variables aléatoires de Bernoulli de paramètre $\frac{1}{2}$ indépendante et $\epsilon_j = 2X_j - 1$ est alors une suite de variable de Rademacher. Alors, on a
	
	\begin{tabular}{ccl}
		$\mathopen{||}f(x) - B_n(x) \mathclose{||}_{\infty}$ & $\geq$ & $\mathopen{}\left|f\left(\frac{1}{2}\right) - B_n\left(\frac{1}{2}\right)\right|\mathclose{}$  \\
		& $=$ & $\mathopen{}\left| B_n\left(\frac{1}{2}\right)\right|\mathclose{}$ \\
		& $=$ & $\E\mathopen{}\left[\frac{S_n}{n} - \frac{1}{2}\mathclose{}\right]$ \\
		& $=$ & $\frac{1}{2n} \E\mathopen{}\left[
		2S_n - n \mathclose{}\right]$ \\
		& $=$ & $\frac{1}{2n} \E\mathopen{}\left[
		\epsilon_1 + \dots \epsilon_n \mathclose{}\right]$ \\
	\end{tabular}
	
	 D'où via l'inégalité de Khintchine:
	
	\begin{tabular}{ccl}
		$\mathopen{||}f(x) - B_n(x) \mathclose{||}_{\infty}$ & $\geq$ & $\frac{1}{2n} \mathopen{||}
		\epsilon_1 + \dots \epsilon_n \mathclose{||}_{1}$\\
		& $\geq$ & $\frac{1}{2n} \mathopen{||}
		\epsilon_1 + \dots \epsilon_n \mathclose{||}_{2}$ \\
		& $=$ & $\frac{1}{2\sqrt{2}}\frac{1}{\sqrt{n}}$ \\
		& $\geq$ & $\frac{1}{2\sqrt{2}}\omega\left(\frac{1}{\sqrt{n}}\right)$ \\
	\end{tabular}
	
	Ce qui prouve l'optimalité de la convergence.
	
	\section{Compléments autours du module de continuité et des probabilités}
	\subsection*{Module de continuité}
	\input{./../Notions/ModuleContinuite.tex}
	
	\subsection*{Probabilité} On va alors donné quelques outils permettant de réaliser ce développement.
	
	\paragraph{Espérance et variance} 
	\input{./../Notions/Proba_esperance.tex} 
	
	\paragraph{Variables de Rademacher et inégalité de Khintchine} 
	\input{./../Notions/Proba_Rademacher.tex}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}

\end{document}