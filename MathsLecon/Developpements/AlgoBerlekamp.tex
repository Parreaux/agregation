\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
} 

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\F}{\mathbb{F}}
	\newcommand{\B}{\mathcal{B}}
	\newcommand{\Sn}{\mathfrak{S}}
	
	\sloppy
	
	\newcommand{\textblue}{\textcolor{blue}}
	\newcommand{\textgreen}{\textcolor{OliveGreen}}
	\newcommand{\textred}{\textcolor{red}}
	\newcommand{\textbrown}{\textcolor{Brown}}
	\newcommand{\textmagenta}{\textcolor{DarkOrchid}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\newtheorem{lnum}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	
	\title{Algorithme de Berlekamp}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Objectif agrégation \cite[p.244]{BeckMalikPeyre}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 123 (Corps fini); 141 (Polynômes irréductibles); 151 (Dimension d'un espace vectoriel).
		}}
	
	\section{Introduction}
	
	L'algorithme de Berlekamp, dont la correction repose sur le théorème chinois, est la base de la méthode de factoriser tous polynômes sur un corps fini. Il permet de décomposé en polynômes irréductibles tout polynôme sans facteur carrés à coefficient dans un corps finis. L'idée de cet algorithme est de trouver les éléments invariants par puissance $q$ dans l'algèbre $\F_q[X]/\!\!<\!\!P\!\!>$ ce qui revient à calculer les sous-espace propres d'une certaine application linéaire.
	
	\section{L'algorithme de Berlekamp}
	
	Soient $p$ un nombre premier, $q = p^s$ et $\F_q$ le corps fini à $q$ éléments. On considère un polynôme $P \in \F_q[X]$ sans facteur carrés: on l'écrit: $P = \prod_{i=1}^r P_i$ où les $P_i$ sont des polynômes irréductibles premiers deux à deux. On note $x = \left(X \mod P\right)$ dans $\F_q[X]/\!\!<\!\!P\!\!>$ et considérons la base $\B = \{1, x, \dots, x^{deg(P)-1}\}$ de $\F_q[X]/\!\!<\!\!P\!\!>$. L'algorithme de Berlekamp calcule le nombre $r$ de facteurs irréductibles de $P$ et, lorsque $r \geq 2$ donne explicitement les $P_i$.
	
	\begin{algorithm}
		\begin{algorithmic}[1]
			\State On considère l'application linéaire $S_P$ définie telle que:
			\begin{displaymath}
			\begin{array}{ccll}
			S_P : & \F_q[X] / \left < P\right > & \to & \F_q[X] / \left < P\right >  \\
			& Q(X) \mod P & \mapsto & Q\left(X^q\right) \mod P
			\end{array}
			\end{displaymath}
			On calcul la matrice $S_P - Id$ dans $\B$.
			\State Le nombre de facteur irréductibles de $P$ est 
			\begin{displaymath}
			r =\dim \left(\ker \left(S_P - Id\right)\right) = \deg(P) - \mathrm{rg}\left(S_P -Id\right)
			\end{displaymath}
			On calcul $r$ avec un pivot de Gauss.
			\If{$r = 1$}
			\State $P$ est irréductible et on a fini.
			\Else
			\State On choisi $V$ non constant de $\F_q[X] / \left < P\right >$ tel que $V \in \ker\left(S_P - Id \right)$.
			\State Calcul de $\gcd\left(P, V - \alpha\right)$, où $\alpha \in \F_q$ à l'aide de l'algorithme d'Euclide. On a alors 
			\begin{displaymath}
			P = \prod_{\alpha \in \F_q} \gcd\left(P, V - \alpha\right)
			\end{displaymath}.
			\State On applique l'algorithme (à partir de 1) aux facteurs non triviaux de ce produit s'ils existent.
			\EndIf			
		\end{algorithmic}
		\caption{Algorithme de Berlekamp}
		\label{algo:Brlekamp}
	\end{algorithm}
	
	\begin{theo}
		Soit $P \in \F_q[X]$ un polynôme dont la décomposition en facteurs irréductibles est sans carrés. On note $x = \left(X \mod P\right)$ dans $\F_q[X]/\!\!<\!\!P\!\!>$ et considérons la base $\B = \{1, x, \dots, x^{deg(P)-1}\}$ de $\F_q[X]/\!\!<\!\!P\!\!>$. 
		
		Alors, l'algorithme de Berlekamp (algorithme~\ref{algo:Brlekamp}) termine et est correct (c'est-à-dire donne la décomposition en facteur irréductible de $P$)
	\end{theo}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement}}}{
		\begin{enumerate}
			\item Montrer la correction.
			\begin{enumerate}
				\item Montrer que l'application $S_P$ est linéaire \textblue{(lemme 1)} (via la puissance).
				\item Montrer que le nombre de polynômes irréductibles vaut $\dim \ker \left(S_P - Id\right)$ \textblue{(lemme~\ref{lem:nbpolyirr})}.
				\item Montrer la décomposition de $P$ sur $\F_q$ \textblue{(lemme~\ref{lem:decomposition})}.
				\begin{enumerate}
					\item Montrer que pour tout $i \in \llbracket 1, r \rrbracket$, il existe $V \in \F_q[X]$ non constant et $\alpha_i \in \F_q$ tel que $\alpha_i = \left(V \mod P_i\right)$.
					\item Pour tout $\alpha \in \F_q$, montrons que $\gcd \left(P, V- \alpha\right) = \prod_{i, \alpha_i = \alpha} P_i$.
					\item Montrer que $P = \prod_{\alpha \in \F_q} \gcd \left(P, V - \alpha\right)$.
				\end{enumerate}
				\item Montrer que les facteurs de décompositions sont bien propres \textblue{(lemme~\ref{lem:factIrr})}.
			\end{enumerate}
			\item Montrer la terminaison.
		\end{enumerate}
	}
	
	\begin{proof}
		Soit $P \in \F_q[X]$ un polynôme dont la décomposition en facteurs irréductibles est sans carrés. On note $x = \left(X \mod P\right)$ dans $\F_q[X]/\!\!<\!\!P\!\!>$ et considérons la base $\B = \{1, x, \dots, x^{deg(P)-1}\}$ de $\F_q[X]/\!\!<\!\!P\!\!>$. Montrons que l'algorithme de Berlekamp (algorithme~\ref{algo:Brlekamp}) termine et est correct.
		
		\paragraph{Étape 1: preuve de la correction} La preuve de la correction de l'algorithme se fait par induction sur les facteurs irréductibles \textblue{(on a un algorithme récursif)}. Avant de commencer l'induction, on va monter deux lemmes qui montre la correction des deux premières étapes de notre algorithme (algorithme~\ref{algo:Brlekamp}). \textblue{On va alors s'appuyer sur deux lemmes permettant de montrer que les deux premières étapes de l'algorithme sont correctes. L'induction intervient pour montrer la correction de la boucle \textsc{if}.}
		
		\begin{lnum}
			L'application $S_P$ est bien définie, linéaire.
			\label{lem:SpLineaire}
		\end{lnum}
		\begin{proof}
			D'après la proposition universelle des polynômes, 
			\begin{displaymath}
			\begin{array}{cccl}
			\delta_1: & \F_q\left[X\right] & \to & \F_q\left[X\right] \\
			& Q(X) & \mapsto & Q \left(X^q\right) \\
			\end{array}
			\end{displaymath}
			est l'unique morphisme tel que $\delta_1(a) = a$ pour tout $a \in \F_q$ et $\delta_1(X) = X^q$. De plus, on remarque que pour tout $Q \in \F_q[X]$, 
			\begin{displaymath}
			\delta_1(Q) \underbrace{=}_{\text{définition}} Q \left(X^q\right) \underbrace{=}_{\forall a \in \F_q, a^q = a} Q^q
			\end{displaymath}
			
			Soient $\pi : \F_q[X] \to \F_q[X]/\!\!<\!\!P\!\!>$ la surjection cannonique et $\delta = \pi \circ \delta_1$. Comme $\pi$ est un morphisme d'anneaux \textblue{(à monter)}, on a 
			\begin{displaymath}
			\delta(P) \underbrace{=}_{\text{définition}} \pi\left(\delta_1(P)\right) \underbrace{=}_{\text{Remarque précédente}} \pi\left(P^q\right) \underbrace{=}_{\pi \text{ morphisme}}\pi\left(P\right)^q \underbrace{=}_{P \mod P = 0} 0
			\end{displaymath}
			Le théorème de l'anneau quotient implique alors que $\delta$ passe au quotient par $<\!\!P\!\!>$ pour donner $S_P$, elle est donc bien définie.
			
			Montrons maintenant que $S_P$ correspond à l'élévation à la puissance $q$:
			
			\begin{tabular}{ccll}
				$S_P\left(Q \mod P\right)$ & $=$ & $S_P(\pi(Q))$ & \textblue{(Définition de $\pi$)} \\
				& $=$ & $\pi\left(Q\left(X^{q}\right)\right)$ & \textblue{($\pi(Q) = Q \mod P$)} \\
				& $=$ &$\pi\left(Q^{q}\right)$& \textblue{($a^q = a$, $\forall a \in \F_q$)} \\
				& $=$ &$\pi\left(Q\right)^{q}$& \textblue{($\pi$ est un morphisme d'anneau)} \\
				& $=$ &$Q^{q} \mod P$& \textblue{(Définition de $\pi$)} \\
			\end{tabular}
			Comme l'élévation à la puissance est linéaire, on obtient le résultat.
		\end{proof}
		
		\begin{req}
			La preuve de ce lemme est analogue à la preuve qui permet de montrer que le morphisme de Frobenius est linéaire.
		\end{req}
		
		\begin{lnum}
			Soit $P = \prod_{i = 1}^{r} P_i$ où les $P_i$ sont irréductibles et premiers deux-à-deux, $r$ est alors le nombre de polynômes irréductibles de $P$. Alors, $r = \dim\left(\ker\left(S_P - Id\right)\right)$.
			\label{lem:nbpolyirr}
		\end{lnum}
		
		\begin{proof}
			Comme les $P_i$ sont irréductibles, les $\F_q-$espaces vectoriels de dimension fini $K_i = \F_q[X]/\!\!<\!\!P_i\!\!>$ sont des corps de caractéristique $p$ et de cardinal $p^{\deg(P_i)}$. De plus, les $P_i$ sont deux-à-deux premiers entre eux, on peut donc appliquer le théorème chinois: il existe $\varphi$ un isomorphisme de $\F_q-$algèbre:
			\begin{displaymath}
			\begin{array}{ccll}
			\varphi : & \F_q[X]/\!\!<\!\!P\!\!> & \to & K_1 \times \dots \times K_r \\
			& Q \mod P & \mapsto & \left(Q \mod P_1, \dots, Q \mod P_r\right) \\
			\end{array}
			\end{displaymath}
			Dans la suite si $Q \in \F_q[X]/\!\!<\!\!P\!\!>$, on notera $Q_i = Q \mod P$.
			
			On pose $\tilde{S_P} = \varphi \circ S_P \circ \varphi^{-1} : K_1 \times \dots \times K_r \to K_1 \times \dots \times K_r$ correspondant à l'élévation à la puissance $q$ dans l'anneau produit $K_1 \times \dots \times K_r$. En effet, soit $Q \in \F_q[X]/\!\!<\!\!P\!\!>$,
			\begin{tabular}{ccll}
				$\tilde{S_P}(Q_1, \dots, Q_r)$ & $=$ & $\varphi \circ S_P \circ \varphi^{-1}(Q_1, \dots, Q_r)$ & \textblue{(Définition de $\tilde{S_P}$)} \\
				& $=$ & $\varphi \circ S_P(Q)$ & \textblue{(Définition de $\varphi$ qui est un isomorphisme)} \\
				& $=$ & $\varphi \left(Q^{q} \right)$ & \textblue{(comme dans le lemme ~\ref{lem:SpLineaire})} \\
				& $=$ &$(Q_1^q, \dots, Q_r^q)$& \textblue{(Définition de $\varphi$)} \\
			\end{tabular}
			\newline
			Ainsi,
			\newline
			\begin{tabular}{ccll}
				$\left(Q_1, \dots, Q_r\right) \in \ker\left(\tilde{S_P} - Id\right)$ & $\Leftrightarrow$ & $\left(Q_1^q, \dots, Q_r^q\right) = \left(Q_1, \dots, Q_r\right)$ & \textblue{(Définition de $\ker\left(\tilde{S_P} - Id\right)$)} \\
				& $\Leftrightarrow$ & $\forall i \in \llbracket 1, r \rrbracket, Q_i^q - Q_i = 0 \mod P_i$ & \textblue{($Q_i \in K_i$ et $S_P \in \F_q[X]/\!\!<\!\!P\!\!>$)} \\
				& $\Leftrightarrow$ & $\forall i \in \llbracket 1, r \rrbracket, Q_i^q = Q_i$ dans $K_i$ & \textblue{(Définition de $K_i$)} \\
			\end{tabular}
			
			Comme $\forall i \in \llbracket 1, i \rrbracket$, $K_i$ est une extension de corps de $\F_q$ ($\F_q \hookrightarrow K_i$): l'image de $\F_q$ dans $K_i$ est l'ensemble des éléments de $K_i$ vérifiant $x^q = x$. En effet, si $x \in \F_q^{\times}$, le théorème de Lagrange sur $\F_q$ nous assure que $x^{q-1} = 1$, soit en multipliant par $x$, on obtient $x^q = x$. De plus, $0$ vérifie l'équation. Donc $\F_q \subset \{x | x^q = x, x \in K_i\}$. Réciproquement, on remarque que le polyne $X^q -X \in K_i[X]$ admet au plus $q$ racines \textblue{($\deg = q$ et $K_i$ est un corps)} et on a déjà $q$ racines \textblue{(les éléments de $\F_q$ sont racines)}. Donc il n'existe pas d'autre racines dans $K_i \setminus \F_q$. On en déduit que l'image de $\F_q$ dans $K_i$ est l'ensemble des éléments de $K_i$ vérifiant $x^q = x$.
			
			On en déduit donc que
			\begin{displaymath}
			\left(Q_1, \dots, Q_r\right) \in \ker\left(\tilde{S_P} - Id\right) \Leftrightarrow \forall i \in \llbracket i, r \rrbracket, x_i \in \F_q \hookrightarrow K_i
			\end{displaymath}
			et donc $\ker\left(\tilde{S_P} - Id\right) = \left(\F_q\right)^r$. Or $\ker\left(\tilde{S_P} - Id\right) = \varphi\left(\ker\left(S_P - Id\right)\right) = \left(\F_q\right)^r$. Comme $\varphi$ est un isomorphisme de $\F_q-$espaces vectoriels, on en conclut que
			\begin{displaymath}
			\dim_{\F_q} \left(\ker\left(S_P - Id\right)\right) = \dim_{\F_q} \left(\F_q^r\right) = r
			\end{displaymath}
		\end{proof}
		
		\begin{lnum}
			Soit $P = \prod_{i = 1}^{r} P_i$ tel que les $P_i$ sont irréductibles et premiers deux-à-deux et $r > 1$. Montrons que $P = \prod_{\alpha \in \F_q} \gcd(P, V - \alpha)$.
			\label{lem:decomposition}
		\end{lnum}
		
		\begin{req}
			L'ensemble des $U \mod P$ tels que $U$ est constant modulo $P$ est la droite vectoriel de $\F_q[X]/\!\!<\!\!P\!\!>$.
		\end{req}
		
		\begin{proof}
			Soit $P = \prod_{i = 1}^{r} P_i$ tel que les $P_i$ sont irréductibles et premiers deux-à-deux et $r > 1$.
			
			\subparagraph{Étape 1: montrons que pour tout $i \in \llbracket 1, r \rrbracket$, il existe $V \in \F_q[X]$ non constant et $\alpha_i \in \F_q$ tel que $\alpha_i = \left(V \mod P_i\right)$.} Comme $r > 1$, par le lemme~\ref{lem:nbpolyirr}, on a $r = \dim_{\F_q} \left(\ker\left(S_P - Id\right)\right) > 1$. Donc, il existe \textblue{(voir la remarque précédente)} $V \in \F_q[X]$ non contant tel que $(V \mod P) \in \ker\left(S_P - Id\right)$. Or, comme $\varphi$ est un isomorphisme, on a \textblue{(par le lemme~\ref{lem:nbpolyirr})}:
			\begin{displaymath}
			(V \mod P) \in \ker\left(S_P -Id\right) \Leftrightarrow \left(V \mod P_1, \dots, V \mod P_r\right) \in \F_q^r
			\end{displaymath}
			Autrement dit, 
			\begin{displaymath}
			\forall i \in \llbracket 1, i \rrbracket, \alpha_i = (V \mod P_i) \in F_q \subset K_i
			\end{displaymath}
			
			\subparagraph{Etape b: pour tout $\alpha \in \F_q$, montrons que $\gcd \left(P, V- \alpha\right) = \prod_{i, \alpha_i = \alpha} P_i$.} Soit $\alpha \in \F_q$. Comme $\gcd\left(P, V - \alpha\right)$ divise $P$ et que les $P_i$ sont irréductible, alors il existe $I_{\alpha} \subset \llbracket 1, r \rrbracket$ tel que $\gcd \left(P, V - \alpha\right) = \prod_{i \in I_{\alpha}} P_i$. Or les $P_i$ sont premiers deux à deux, donc par le lemme de Gauss, $I_{\alpha} = \{i \in \llbracket 1, r \rrbracket, P_i | V - \alpha \}$.
				
			On remarque que pour tout $i \in llbracket 1, r \rrbracket$:
			\begin{displaymath}
				\alpha_i = \alpha 
				\underbrace{\Leftrightarrow}_{V-\alpha = V - \alpha_i = 0 \mod P_i \text{\textblue{(hypothèse)}}}  V-\alpha = 0 \mod P_i 
				\underbrace{\Leftrightarrow}_{ \text{Définition du modulo}} P_i | (V - \alpha)
			\end{displaymath}
			Donc par ce qui précède, $I_{\alpha} = \{i | \alpha = \alpha_i\}$. Donc, $\gcd \left(P, V- \alpha\right) = \prod_{i, \alpha_i = \alpha} P_i$.
			
			\subparagraph{Étape c: montrons que $P = \prod_{\alpha \in \F_q} \gcd \left(P, V - \alpha\right)$.} On a 
			\begin{displaymath}
			P = \prod_{i = 1}^{r} P_i \underbrace{=}_{
				\left \{
				\begin{array}{l}
				\alpha_i = \alpha \wedge \alpha_i = \beta \Rightarrow \alpha = \beta \\
				V \mod P_i \in \F_q \\
				\end{array}
				\right .} 
			\prod_{\alpha \in \F_q} \left(\prod_{\{i, \alpha = \alpha_i\}} P_i \right) \underbrace{=}_{\text{précédent}} \prod_{\alpha \in \F_q} \gcd\left(P, V - \alpha\right)
			\end{displaymath}
			
			D'où le résultat.
		\end{proof}
		
		\begin{lnum}
 			Soit $P = \prod_{\alpha \in \F_q} \gcd\left(P, V - \alpha\right)$ avec $V$ choisi comme dans le lemme~\ref{lem:decomposition}, alors il existe $\alpha \in \F_q$ tel que $\gcd\left(P, V - \alpha\right)$ soit un facteur strict de $P$.
 			\label{lem:factIrr}
		\end{lnum}
		\begin{proof}
			Le choix de $V$ nous assure qu'il existe $(i, j) \in \llbracket 1, r \rrbracket^2$ tel que $\alpha_i \neq \alpha_j$. En effet, sinon, $\forall i \in \llbracket 1, r \rrbracket, \alpha_i = \alpha \in \F_q$ et 
			\newline
			\begin{tabular}{ccll}
				$\forall i,~ P_i | V - \alpha$ & $\Rightarrow$& $\prod_{i=1}^{r} P_i | V - \alpha$ & \textblue{(Les $P_i$ sont premiers deux à deux)} \\
				& $\Rightarrow$& $V = \alpha = cst \mod P$ & \textblue{(Définition du modulo)} \\
			\end{tabular}
			\newline
			Contradiction car $V$ serait constant modulo $P$.
			
			On en déduit que $P_i | \gcd\left(P, V - \alpha_i\right)$ donc $\deg\left(\gcd\left(P, V - \alpha_i\right)\right) > 0$. De même $P_j \nmid \gcd\left(P, V - \alpha_i\right)$ donc $\deg\left(\gcd\left(P, V - \alpha_i\right)\right) < \deg(P)$. Donc $\gcd\left(P, V - \alpha_i\right)$ est un facteur strict de la décomposition de $P$.
		\end{proof}
		
		Montrons maintenant la correction de l'algorithme. Pour cela, nous allons raisonner par induction sur la décomposition en facteur irréductible de $P$. Nous allons donc raisonner par récurrence sur $r$. Montrons pour tout $r \in \N^{*}$ la propriété $\mathcal{P}_{r}$ : " Si $P = \prod_{i = 1}^{r} P_i$ avec les $P_i$ irréductible et deux à deux distincts, alors l'algorithme de Berlekamp (algorithme~\ref{algo:Brlekamp}) renvoie la décomposition de $P$ en facteur irréductible soit $\prod_{i = 1}^{r} P_i$."
		
		\begin{description}
			\item[Cas $r = 1$] Si $r= 1$ alors le nombre de polynôme irréductible de $P$ est $1$, donc $P$ est irréductible et l'algorithme est correct.
			
			\item[Cas $\mathcal{P}_{r}$] Soit $r$ tel que pour tout $1 \leq i \leq r-1$ la propriété $\mathcal{P}_i$ soit vérifiée Montrons la propriété dans le cas $r$. Soit $p = \prod_{i=1}^{r} P_i$ Par les lemmes~\ref{lem:SpLineaire} et~\ref{lem:nbpolyirr}, l'algorithme de Berlekamp calcul un $r$ correct. En particulier $r > 1$, on applique donc le lemme\ref{lem:decomposition} qui nous assure que $\prod_{\alpha \in \F_q} \gcd\left(P, V - \alpha\right)$. Par le lemme~\ref{lem:factIrr}, il existe $\alpha \in \F_q$ tel que $\gcd\left(P, V - \alpha\right)$ soit un facteur strict de $P$. Comme $\gcd\left(P, V - \alpha\right)$ divise $P$, il est sans facteur carré et possède moins de facteurs irréductibles. De plus comme $p$ est sans facteur carré, $\forall \alpha \in \F_q$, $\gcd\left(P, V - \alpha\right)$ possède moins de facteurs irréductibles \textblue{(sinon, contradiction avec l'hypothèse sur les facteurs carrés)}. Donc, on peut appliquer l'algorithme de Berlekamp à $\gcd\left(P, V - \alpha\right)$, pour tout $\alpha \in \F_q$. De plus, par hypothèse de récurrence, l'algorithme de Berlekamp est correct sur ces polynôme d'où la correction pour $P$. Donc $\mathcal{P}_{r}$ est vraie.
		\end{description}
		On a alors monter la correction de cet algorithme.
		
		\paragraph{Preuve de la terminaison} Pour montrer que l'algorithme termine, il faut montrer que $r$, le nombre de polynômes irréductibles de l'entrée décrois strictement \textblue{(ils forment un ordre bien formé)}. Par le lemme~\ref{lem:factIrr}, on a l'existence de $\alpha \in \F_q$ tel que $\gcd\left(P, V - \alpha\right)$ soit un facteur strict dans la décomposition de $P$, donc il possède moins de facteur irréductible. De plus, $F$ est sans facteurs carrés, donc pour tout $\alpha \in \F_q$, $\gcd\left(P, V - \alpha\right)$ est soit un facteur stricte de la décomposition de $P$ soit une constante. Comme on applique l'algorithme à$\gcd\left(P, V - \alpha\right)$, pour tout $\alpha \in \F_q$, alors $r$ décrois strictement à chaque itération. D'où la terminaison.
	\end{proof}
	
	\section{Compléments autours de cet algorithme}
	
	\subsection*{Complexité de l'algorithme de Berlekamp} En pratique, l'algorithme de Berlekamp nécessite le calcul de pgcd (via l'algorithme d'Euclide) et de rang de matrice (via le pivot de Gauss). On a alors, la complexité du pivot de Gauss en  $O\left(\deg(P)^3\right)$ puisque la taille de la matrice de $S_P - Id$ dans la base $\mathcal{B}$ est de $\deg(P)$. De plus, on effectue $q$ calcul de pgcd dans l'algorithme de d'Euclide car on a $\deg(P)$ étapes de calcul de l'algorithme d'Euclide et une division par $x$ en $\deg(P)$. On en déduit que la factorisation de $P$ se fait en $O\left(q\deg(P)^2\right)$. Donc un appel de l'algorithme de Berlekamp se fait en $O\left(\deg(P)^3 + q\deg(P)^2\right)$.
	
	\subsection*{Application pratique de cet algorithme \cite[p.248]{BeckMalikPeyre}} Une application première de cet algorithme est la factorisation des polynômes sur un corps finis. On donne ici un algorithme basé sur l'algorithme de Berlekamp permettant de réaliser cette factorisation (algorithme~\ref{algo:Factorisation}). Comme, on ne peut pas appliquer l'algorithme de Berlekamp à un polynôme à facteur multiples, il nous faut un test pour savoir comment appliqué cet algorithme. Un outil intéressant dans ce cas est le polynôme dérivée car on détecte les facteurs multiple dans $P$ en calculant $\gcd(P, P')$ (car les facteurs multiples restent lors de la dérivation).
	
	\begin{algorithm}
		\begin{algorithmic}[1]
			\If{$P$ constant}
			\State On a fini
			\Else
			\State On calcul $\gcd(P, P')$
			\If{$\gcd(P, P') = 1$}
			\State On applique Berlekamp (algorithme~\ref{algo:Brlekamp}) à $P$.
			\Else
			\If{$\gcd(P, P') = P$}
			\State On calcul $R$ tel que $R^q = P$.
			\State On s'appelle récursivement sur $R$
			\Else
			\State $P_1 = \gcd(P, P')$ et $P_2 = \frac{P}{\gcd(P, P')}$ sont deux facteurs non triviaux de $P$. 
			\State On s'appelle alors récursivement sur $P_1$ et $P_2$.
			\EndIf
			\EndIf
			\EndIf			
		\end{algorithmic}
		\caption{Algorithme factorisant les polynômes dans un corps fini $k$ de caractéristique $q$.}
		\label{algo:Factorisation}
	\end{algorithm}
	
	\subsection*{Quelques résultats sur les anneaux nécessaires à la démonstration.} 
	\input{./../Notions/Anneaux.tex}
	
	\subsection*{Théorème chinois}
	\input{./../Notions/Anneaux_thmChinois.tex}
	
	\subsection*{Algorithmes en jeux ici.}
	Dans ce développement, nous utilisons deux algorithmes important: l'algorithme d'Euclide (algorithme~\ref{algo:Euclide}) qui nous permet d'obtenir le pgcd entre deux polynôme et le pivot de Gauss (algorithme~\ref{algo:PivotGauss}) qui nous donne le rang d'une matrice.
	\input{./../Notions/Algo_Euclide.tex}
	\input{./../Notions/Algo_PivotGauss.tex}
	
	
	Application ici \cite{Szpirglas}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}