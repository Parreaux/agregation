\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais,english]{babel}%

\usepackage[dvipsnames]{xcolor}

\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{enumitem}
\usepackage{appendix}
\usepackage{stmaryrd}
\usepackage{amsmath}
\usepackage{wasysym}

\usepackage{listings}
\usepackage{multirow}
\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}
\algnewcommand{\LineComment}[1]{\State \(\triangleright\) #1}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\F}{\mathbb{F}}
	\newcommand{\Sn}{\mathfrak{S}}
	\newcommand{\A}{\mathfrak{A}}
	
	\sloppy
	
	\newcommand{\thm}{\emph{Théorème}}
	\newcommand{\proposition}{\emph{Proposition}}
	\newcommand{\lem}{\emph{Lemme}}
	\newcommand{\corollaire}{\emph{Corollaire}}
	\newcommand{\definiton}{\emph{Définition}}
	\newcommand{\remarque}{\emph{Remarque}}
	\newcommand{\application}{\emph{Application}}
	\newcommand{\exemple}{\emph{Exemple}}
	\newcommand{\cexemple}{\emph{Contre-exemple}}
	\newcommand{\cadre}{\emph{Cadre}}
	\newcommand{\methode}{\emph{Méthode}}
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textbluesmall}[1]{{\small{\textcolor{blue}{#1}}}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\theoremstyle{plain}
	\newtheorem{theo}{Théorème}
	\newtheorem{prop}{Proposition}
	\newtheorem{cor}{Corollaire}
	\newtheorem{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem{definition}{Définition}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	\newtheorem*{cex}{Contre-exemple}
	
	\title{Théorème de Cauchy--Lipschitz global}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Rouvière \cite[p.180]{Rouviere}.
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 203 (Compacité); 208 (Espace vectoriel normé); 220 (Équations différentielles (non linéaires)); 221 (Équations différentielles linéaires).
			
			\textblue{\emph{Leçons où il peut être évoqué:}} 226 (Suites récurrentes).
		}}

	\section{Introduction}
	
	Le théorème de Cauchy--Lipschitz trouve de nombreuses applications dans le domaine des équations différentielles car il donne un résultat d'existence et d'unicité de solution pour des équations différentielles. Ici nous allons montrer une version plus forte.
	\begin{theo}
		Soient $\R^m$ muni de sa norme $\mathopen{||} . \mathclose{||}$, $I$ un intervalle de $\R$ et $f : I \times \R^m \to \R^m$ une application continue, supposée globalement lipschitzienne en $y$ au sens suivant: pour tout compact $K \subset I$, il existe $k > 0$ tel que pour tous $t \in K$, $y, z \in \R^m$,
		\begin{displaymath}
		\mathopen{||} f(t, y) - f(t, z) \mathclose{||} \leq k \mathopen{||} y- z \mathclose{||}
		\end{displaymath}
		Alors, pour tous $t_0 \in I$ et $x \in \R^m$, le problème de Cauchy
		\[ \left  \{
		\begin{array}{c @{ = } l}
		y'& f(t,y) \\
		y(t_0) & x\\
		\end{array}
		\right.
		\]
		admet une unique solution $t \mapsto y(t)$ qui est globale (définie sur $I$ tout entier).
	\end{theo}
	
	Le succès de ce théorème (et de la méthode sous-jacente) repose sur son applications aux équations différentielles de la méthode des approximations successives de Picard consistant à approximer la solution à partir d'une constante grâce à des intégrales successives. En effet, lors de l'application du théorème dans cette méthode, les fonctions ne sont plus nécessairement contractante: le choix de la norme est donc cruciale et la norme utilisée dans cette démonstration est importante.
	
	On donne ici une version forte du théorème de Cauchy--Lipschitz car on va supposer que la fonction est globalement lipschitzienne. C'est cette hypothèse qui va nous garantir l'existence d'une unique solution sur l'intervalle de définition. Ce résultat s'applique alors à tous systèmes différentiels linéaires \textblue{(motivation pour la leçon 221)}:
	\[ \left  \{
	\begin{array}{c @{ = } l}
	y'& A(t)y + b(t) \\
	y(t_0) & x\\
	\end{array}
	\right.
	\]
	où la matrice $A(t)$ et le vecteur $b(t)$ sont continues en $t$ sur $I$ (le domaine de définition), ce qui garanti l'existence et l'unicité d'une solution. Cependant, l'existence d'une solution sur tout le domaine de définition ne peut être garanti sans cette hypothèse. Prenons l'exemple de l'équation où $f$ n'est pas globalement lipschitzienne:
	\[ \left  \{
	\begin{array}{c @{ = } l}
	y'& y^2 \\
	y(0) & 1\\
	\end{array}
	\right.
	\]
	dont la solution $y =\frac{1}{(1-t)}$ sagement partie de $1$ en $0$ explose dès que le temps atteint $1$. 
	
	
	\section{Le théorème de Cauchy--Lipschitz global}
	
	Montrons maintenant le résultat que l'on souhaite que l'on rappel ici:
	\begin{theo}
		Soient $m \geq 1$, $\mathopen{||} . \mathclose{||}$ une norme de $\R^m$, $I$ un intervalle non vide ($\vec{I} \neq \emptyset$) de $\R$ et $f : I \times \R^m \to \R^m$ une application continue, supposée globalement lipschitzienne en $y$ au sens suivant: pour tout compact $K \subset I$, il existe $k > 0$ tel que pour tous $t \in K$, $y, z \in \R^m$,
		\begin{displaymath}
		\mathopen{||} f(t, y) - f(t, z) \mathclose{||} \leq k \mathopen{||} y- z \mathclose{||}
		\end{displaymath}
		Alors, pour tous $t_0 \in I$ et $x \in \R^m$, le problème de Cauchy
		\[ (C) \;\; \left  \{
		\begin{array}{c @{ = } l}
		y'& f(t,y) \\
		y(t_0) & x\\
		\end{array}
		\right.
		\]
		admet une unique solution $t \mapsto y(t)$ qui est globale (définie sur $I$ tout entier).
	\end{theo}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement}}}{
		L'idée de la preuve est de se ramené à un problème de recherche de points fixes.
		\begin{enumerate}
			\item Montrer que $y$ est solution de $(C)$ si et seulement si $y$ est un point fixe de $F$.
			\item On supposons que $I$ est compact et on montre que $F$ admet un unique point fixe sur $I$.
			\begin{enumerate}
				\item Montrer que $(E, \mathopen{||} . \mathclose{||}_k)$ est un Banach.
				\begin{enumerate}
					\item Montrer que $\mathopen{||}.\mathclose{||}_k$ est une norme.
					\item Montrer que $\mathopen{||}.\mathclose{||}_k$ et $\mathopen{||}.\mathclose{||}_{\infty}$ sont équivalentes.
				\end{enumerate}
				\item Montrer que $F$ est contractante pour $\mathopen{||} . \mathclose{||}_k$.
			\end{enumerate}
			\item Montrer que $(C)$ admet une unique solution pour $I$ quelconque.
		\end{enumerate}
	}
	
	\begin{proof}
		On note
		\begin{displaymath}
		\begin{array}{cccl} 
		F : & E = \mathcal{C}^0(I, \R^m) & \to & \{I \to \R \} \\
		& y & \mapsto & \left(t \mapsto x + \int_{t_0}^t f(s, y(s)) ds \right)\\
		\end{array}
		\end{displaymath}
		
		\paragraph{Étape 1: montrons que $y$ est solution de $(C)$ si et seulement si $y$ est un point fixe de $F$.} Supposons que $y$ est solution de $(C)$ et montrons que $y$ est un point fixe de $F$ \textblue{(donc $y \in E$)}. Comme $y$ est solution d'une équation différentielle, $y$ est dérivable sur $I$ et $y' = f(., y)$. De plus, $f$ et $y$ sont continues \textblue{(par hypothèse)}, donc $y'$ est continue \textblue{(par composition de fonctions continues)}. Par intégration, pour tout $t \in I$,
		
		\noindent\begin{tabular}{ccll}
			$y(t)$ & $=$ & $\int_{t_0}^{t}y'(s)ds +y(t_0)$ & \textblue{(théorème fondamental de l'analyse: $\int_{t_0}^{t}y'(s)ds = y(t)-y(t_0)$)} \\
			& $=$ & $\int_{t_0}^{t}f(s, y(s))ds +x$ & \textblue{($y(t_0)  x$ et $y' = f(., y)$, par hypothèses)} \\
			& $=$ & $F(y)(t)$ & \textblue{(définition de $F$)} \\
		\end{tabular}
		Donc $y$ est bien un point fixe de $F$.
		
		Réciproquement, supposons que $y$ est un point fixe de $F$ : $y = F(y)$. Par dérivabilité de $F$ sur $I$ \textblue{(théorème fondamental de l'analyse)}, $y$ est dérivable sur $I$ et vérifie $(C)$.
		
		\begin{req}
			On se ramène à un problème de recherche de point fixe: trouver une solution au problème c'est trouver un point fixe à $F$.
		\end{req}
		
		
		\paragraph{Étape 2 : supposons que $I$ est compact et montrons que $F$ admet un unique point fixe sur $I$.} Soient $I$ compact, $k$ la constante de Lipschitz de $f$, $l$ la longueur de $I$ \textblue{(son diamètre mais c'est un intervalle de $\R$)} et $\mathopen{||} y \mathclose{||}_k = \max_{t \in I} \left(e^{-k \mathopen{|}t -t_0 \mathclose{|}}\mathopen{||} y(t) \mathclose{||}\right)$ une norme de $E$ \textblue{(permet d'assure que $F$ va bien être contractante)}. On souhaite appliquer le théorème de point fixe, nous allons en vérifier les hypothèses.
		
		\subparagraph{Étape a : Montrons que $(E, \mathopen{||} . \mathclose{||}_k)$ est un Banach.} \emph{Étape i : commençons par monter que $\mathopen{||} . \mathclose{||}_k$ est bien une norme.} Cela se déduit du fait que $\mathopen{||} . \mathclose{||}$ est une norme de $\R^m$ et du bon comportement du $\max$ vis-à-vis des propriétés d'une norme.
		\textblue{En effet, pour tout $(y, z) \in E^2$ et tout $\lambda \in \R^m$
			\begin{description}
				\item[Séparation] 
				\begin{tabular}{ccll}
					$\mathopen{||} y \mathclose{||}_k =0$ & $\Leftrightarrow$ & $\max_{t\in I} e^{-k\mathopen{|}t - t_0 \mathclose{|}}\mathopen{||} y(t) \mathclose{||} = 0$ & (Définition de $\mathopen{||} . \mathclose{||}_k$) \\
					& $\Leftrightarrow$ & $\forall t \in I, e^{-k\mathopen{|}t - t_0 \mathclose{|}}\mathopen{||} y(t) \mathclose{||} = 0$ & (Définition du $\max$) \\
					& $\Leftrightarrow$ & $\mathopen{||} y(t) \mathclose{||} = 0$ & (par positivité de $e^{-k\mathopen{|}t - t_0 \mathclose{|}} \forall t \in I$) \\
					& $\Leftrightarrow$ & $\forall t \in I, y(t) =0$ & (Car $\mathopen{||} . \mathclose{||}$ est une norme de $\R^m$)
				\end{tabular}  
				\item[Homogénéité] 
					\begin{tabular}{ccll}
						$\mathopen{||} \lambda y \mathclose{||}_k $ & $=$ & $\max_{t\in I} e^{-k\mathopen{|}t - t_0 \mathclose{|}}\mathopen{||} \lambda y(t) \mathclose{||} $ & (Définition de $\mathopen{||} . \mathclose{||}_k$) \\
						& $=$ & $\max_{t\in I} e^{-k\mathopen{|}t - t_0 \mathclose{|}}\mathopen{|}\lambda \mathclose{|}\mathopen{||}  y(t) \mathclose{||} $ & (Car $\mathopen{||} . \mathclose{||}$ est une norme de $\R^m$)\\
						& $=$ & $\mathopen{|}\lambda \mathclose{|}\max_{t\in I} e^{-k\mathopen{|}t - t_0 \mathclose{|}}\mathopen{||}  y(t) \mathclose{||} $ & (Par homogénéité du $\max$)\\
						& $=$ & $\mathopen{|}\lambda \mathclose{|} \, \mathopen{||} y \mathclose{||}_k$ & (Définition de $\mathopen{||} . \mathclose{||}_k$) \\ 
					\end{tabular}  
				\item[Inégalité triangulaire] 
				\begin{tabular}{ccll}
					$\mathopen{||} y + z \mathclose{||}_k $ & $=$ & $\max_{t\in I} e^{-k\mathopen{|}t - t_0 \mathclose{|}}\mathopen{||} y(t) + z(t) \mathclose{||} $ & (Définition de $\mathopen{||} . \mathclose{||}_k$) \\
					& $=$ & $\forall t\in I, e^{-k\mathopen{|}t - t_0 \mathclose{|}}\mathopen{||}  y(t) + z(t)\mathclose{||} $ & (Définition du $\max$)\\
					& $\leq$ & $\forall t\in I, e^{-k\mathopen{|}t - t_0 \mathclose{|}}\left(\mathopen{||}y(t)\mathclose{||} + \mathopen{||}z(t)\mathclose{||} \right)$& ($\mathopen{||} . \mathclose{||}$ est une norme)\\
					& $\leq$ & $\forall t\in I, e^{-k\mathopen{|}t - t_0 \mathclose{|}}\mathopen{||}y(t)\mathclose{||} + e^{-k\mathopen{|}t - t_0 \mathclose{|}}\mathopen{||}z(t)\mathclose{||}$& (En développant)\\
					& \multirow{2}{*}{$\leq$} & $\max_{t\in I} \left(e^{-k\mathopen{|}t - t_0 \mathclose{|}}\mathopen{||}y(t)\mathclose{||} \right) + $ & \multirow{2}{*}{(Définition du $\max$)}\\
					& & $\max_{t\in I} \left(e^{-k\mathopen{|}t - t_0 \mathclose{|}}\mathopen{||}z(t)\mathclose{||}\right)$& \\
					& $\leq$ & $\mathopen{||} y \mathclose{||}_k+ \mathopen{||} z \mathclose{||}_k$ & (Définition de $\mathopen{||} . \mathclose{||}_k$) \\ 
				\end{tabular}  
			\end{description}}
			
			\noindent\emph{Étape ii : montrons maintenant que $\mathopen{||} . \mathclose{||}_k$ est équivalente à $\mathopen{||}.\mathclose{||}_{\infty}$ où $\mathopen{||} y \mathclose{||}_{\infty}= \max_{t \in I} \mathopen{||} y(t) \mathclose{||}$.}  Soit $t \in I$, on a :
			\begin{description}
				\item[$e^{-kl} \mathopen{||} y \mathclose{||}_{\infty} \leq \mathopen{||} y \mathclose{||}_k$] car 
				
				\begin{tabular}{rcl}
					& $l \geq \mathopen{|} t - t_0 \mathclose{|}$ & \textblue{($l$ est la longueur de $I$ et $t, t_0 \in I$)} \\
					$\Rightarrow$ & $-kl \leq -k\mathopen{|} t - t_0 \mathclose{|}$ & \textblue{($k$ est un coefficient de Lipschitz donc $> 0$)} \\
					$\Rightarrow$ &$e^{-kl} \leq e^{-k\mathopen{|} t - t_0 \mathclose{|}}$ & \textblue{(croissance de $e$)} \\
					$\Rightarrow$ & $e^{-kl}\mathopen{||}y(t)\mathclose{||} \leq e^{-k\mathopen{|} t - t_0 \mathclose{|}}\mathopen{||}y(t)\mathclose{||}$ & \textblue{(positivité de la norme $\mathopen{||}.\mathclose{||}$)} \\
					$\Rightarrow$ & $\max_{t \in I} \left(e^{-kl}\mathopen{||}y(t)\mathclose{||} \right) \leq \max_{t \in I} \left(e^{-k\mathopen{|} t - t_0 \mathclose{|}}\mathopen{||}y(t)\mathclose{||} \right)$ & \textblue{(croissance du $\max$)} \\
					$\Rightarrow$ & $e^{-kl}\max_{t \in I} \left(\mathopen{||}y(t)\mathclose{||} \right) \leq \max_{t \in I} \left(e^{-k\mathopen{|} t - t_0 \mathclose{|}}\mathopen{||}y(t)\mathclose{||} \right)$ & \textblue{(homogénéité du $\max$)} \\
					$\Rightarrow$ & $\max_{t \in I} e^{-kl}\mathopen{||}y(t)\mathclose{||}_{\infty} \leq \mathopen{||}y(t)\mathclose{||}_{k}$ & \textblue{(définition des normes)} \\
				\end{tabular}
				
			\item[$\mathopen{||} y \mathclose{||}_{k} \leq \mathopen{||} y \mathclose{||}_{\infty}$] car
			
			\begin{tabular}{rcl}
				& $e^{-k\mathopen{|} t - t_0 \mathclose{|}} \leq 1$ & \textblue{($-k\mathopen{|} t - t_0 \mathclose{|} < 0$)} \\
				$\Rightarrow$ & $e^{-k\mathopen{|} t - t_0 \mathclose{|}}\mathopen{||}y(t)\mathclose{||} \leq \mathopen{||}y(t)\mathclose{||}$ & \textblue{(positivité de la norme $\mathopen{||}.\mathclose{||}$)} \\
				$\Rightarrow$ & $\max_{t \in I} \left(e^{-k\mathopen{|} t - t_0 \mathclose{|}}\mathopen{||}y(t)\mathclose{||} \right) \leq \max_{t \in I} \left(\mathopen{||}y(t)\mathclose{||} \right)$ & \textblue{(croissance du $\max$)} \\
				$\Rightarrow$ & $\mathopen{||} y \mathclose{||}_{k} \leq \mathopen{||} y \mathclose{||}_{\infty}$ & \textblue{(définition des normes)} \\
			\end{tabular}
			\end{description}
		Donc les normes sont bien équivalentes. De plus, comme $(E, \mathopen{||} . \mathclose{||}_{\infty})$ est un Banach, on en déduit que $(E, \mathopen{||} . \mathclose{||}_k)$ est un Banach. 
		
		\begin{req}
			Comme $f$ est continue, $F(y)$ est une fonction continue de $I$ dans $\R^{m}$ \textblue{par composition de fonction continue, $y$ l'est également}). On en déduit que $F: E \to E$.
		\end{req}
		
		\subparagraph{Étape b : montrons que $F$ est contractante pour $\mathopen{||} . \mathclose{||}_k$.} Soit $y, z \in E$ et $t \in I$. On distingue alors deux cas.
		
		\begin{description}
			\item[Cas où $t \geq t_0$] Pour $t \geq t_0$, on a: 
			\begin{displaymath}
			F(y)(t) - F(z)(t) \underbrace{=}_{\textblue{\textrm{par } F}} \int_{t_0}^{t} f(s, y(s)) ds - \int_{t_0}^{t} f(s, z(s)) ds \underbrace{=}_{\textblue{\textrm{linéarité } \int}} \int_{t_0}^{t} \left( f(s, y(s)) - f(s, z(s))  \right) ds
			\end{displaymath}
			
			On en déduit que: $A = e^{-k(t-t_0)}\mathopen{||} F(y)(t) - F(z)(t) \mathclose{||}$
			
			\begin{tabular}{ccll}
				 $A$ & $\leq$ &  $e^{-k(t-t_0)}\int_{t_0}^{t} \left( \mathopen{||}f(s, y(s)) - f(s, z(s))\mathclose{||}  \right) ds$ & \textblue{(norme et $\int$ + positivité de $\exp$)}\\
				 %& \multicolumn{2}{c}{\textblue{(norme et intégrale + positivité de $\exp$)}} \\
				 & $\leq$ &  $e^{-k(t-t_0)}\int_{t_0}^{t} \left( k\mathopen{||}y(s) - z(s)\mathclose{||}  \right) ds$ & \textblue{($f$ est $k-$lipschitzienne)}\\
				 & $\leq$ &  $e^{-k(t-t_0)}\int_{t_0}^{t} \left( ke^{k(s-t_0)}e^{-k(s-t_0)}\mathopen{||}y(t) - z(t)\mathclose{||}  \right) ds$ & \\
				 & $\leq$ &  $e^{-k(t-t_0)}\int_{t_0}^{t} \left( ke^{k(s-t_0)}\mathopen{||}y- z \mathclose{||}_{k}  \right) ds$ & \textblue{(définition du $\max$)}\\
				 & $\leq$ &  $e^{-k(t-t_0)}\int_{t_0}^{t} \left( ke^{k(s-t_0)}\right) ds \mathopen{||}y-z \mathclose{||}_{k}$ & \textblue{(linéarité de $\int$)}\\
				 & $\leq$ &  $e^{-k(t-t_0)} \left(e^{k(s-t_0)} -1 \right) ds \mathopen{||}y-z \mathclose{||}_{k}$ & \textblue{(par primitive)}\\
				 & & \multicolumn{2}{c}{\textblue{$\int_{t_0}^{t} \left(ke^{k(s-t_0)}\right) ds = \left[e^{k(s-t_0)}\right]_{t_0}^t = e^{k(t-t_0)} -1 $}} \\
				 & $\leq$ &  $\left( 1 - e^{-k(t-t_0)}\right) \mathopen{||}y-z \mathclose{||}_{k}$ &\\

			\end{tabular}
			
			\item[Cas où $t \leq t_0$] on raisonne de même en faisant attention aux bornes de l'intégrale: on va alors remplacer $\int_{t_0}^{t}$ par $\int_{t}^{t_0}$ et on obtient:
			\begin{displaymath}
			e^{k(t-t_0)}\mathopen{||} F(y)(t) - F(z)(t) \mathclose{||} \leq \left( 1 - e^{k(t-t_0)}\right) \mathopen{||}y-z \mathclose{||}_{k}
			\end{displaymath}
		\end{description}
		
		On voit alors apparaître la valeur absolue autour de $t - t_0$, pour tout $t \in I$, on obtient:
		\begin{displaymath}
		e^{k\mathopen{|}t-t_0\mathclose{|}}\mathopen{||} F(y)(t) - F(z)(t) \mathclose{||} \leq \left( 1 - e^{k\mathopen{|}t-t_0\mathclose{|}}\right) \mathopen{||}y- z\mathclose{||}_{k}
		\end{displaymath}
		En passant au $\max$ sur $I$, on obtient:
		\begin{displaymath}
		\mathopen{||} F(y)(t) - F(z)(t) \mathclose{||}_k \leq \left( 1 - e^{kl}\right) \mathopen{||}y- z\mathclose{||}_k
		\end{displaymath}
		
		Comme $\left( 1 - e^{kl}\right) \leq 1$, $F$ est contractante pour la norme $\mathopen{||}.\mathclose{||}_k$. On applique alors le théorème de point fixe qui nous assure l'existence d'un unique point fixe pour $F$ sur $I$. Donc par le point précédent, $(C)$ admet une unique solution sur $I$.
		
		\paragraph{Étape 3 : montrons que $(C)$ admet une unique solution pour $I$ quelconque.} Comme $I$ est un intervalle quelconque de $\R$, on peut écrire $I$ comme la réunion d'une suite croissante de compacts contenant tous $t_0$, soit: $I = \cup_{n} K_n$ avec $t_0 \in K_0 \subset K_1 \subset \dots \subset K_n \subset \dots$. Par ce qui précède, notons $y_n$ l'unique solution de $(C)$ sur $K_n$:
		\[\left \{ 
			\begin{array}{r @{=} ll}
				y_n'(t) & f(t, y_n) & t \in K_n \\
				y_n(t_0) & x & \\
			\end{array}
			\right.\]
		
		Si $y(t)$ est une solution de $(C)$ sur $I$
		\[\left \{ 
		\begin{array}{r @{=} ll}
		y'(t) & f(t, y) & t \in I \\
		y(t_0) & x & \\
		\end{array}
		\right.\]
		alors sa restriction à $K_n$ vaut $y_n$: $y_{|K_n} = y_n$, par unicité sur $K_n$. D'où l'unicité de la solution de $(C)$ sur $I$.
		
		Inversement, les $y_n$ se raccroche : la fonction $y(t) = y_n(t)$ pour tout $j$ tel que $t \in K_n$ est bien unicité par l'unicité sur les $K_n$ et donne une solution du $I$ par l'exhaustivité des $K_n$. D'où l'existence d'une solution de $(C)$ sur $I$. 
		
	\end{proof}
	
	\section{Compléments autours du théorème de Cauchy--Lipschitz}
	
	\subsection*{Équivalence entre résultats} Le point fixe de Picard, le théorème de Cauchy--Lipschitz, le théorème des fonctions implicites et le théorème d'inversion locale sont des résultats équivalents.
	
	\subsection*{Théorème de Cauchy--Lipschitz local} 
	\input{./../Notions/EquaDiff_ExistenceUniciteSol.tex}
	
	\subsection*{Points fixes}
	\input{./../Notions/PointsFixes.tex}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}	
\end{document}