\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{pgf,tikz,pgfplots}
%\pgfplotsset{compat=1.15}
\usepackage{mathrsfs}
\usetikzlibrary{arrows}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\definecolor{sexdts}{rgb}{0.1803921568627451,0.49019607843137253,0.19607843137254902}
	\definecolor{ffqqqq}{rgb}{1,0,0}
	\definecolor{dtsfsf}{rgb}{0.8274509803921568,0.1843137254901961,0.1843137254901961}
	\definecolor{wrwrwr}{rgb}{0.3803921568627451,0.3803921568627451,0.3803921568627451}
	\definecolor{rvwvcq}{rgb}{0.08235294117647059,0.396078431372549,0.7529411764705882}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\Pro}{\mathbb{P}}
	
	\sloppy
	
	\newcommand{\textblue}{\textcolor{blue}}
	\newcommand{\textgreen}{\textcolor{OliveGreen}}
	\newcommand{\textred}{\textcolor{red}}
	\newcommand{\textbrown}{\textcolor{Brown}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Groupe circulaire}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Audin \cite[p.203]{Audin}.
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 108 (parties génératrices); 182 (complexes en géométrie); 183 (groupes en géométrie).
		}}

	\begin{definition}[Groupe circulaire]
		On appelle groupe circulaire, $G$, le groupe des transformations de $\Pro_{1}(\C)$ qui préserve l'ensemble des droites et des cercles de $\Pro_{1}(\C)$.
	\end{definition}
	
	\textblue{A noter: la préservation seule des droites ou des cercles suffit. En effet, en fonction de la représentation de $\Pro_{1}(\C)$, on obtient sous des droites (sur la droite projective), soit une sphère (la sphère de Riemann). On peut alors passer "facilement" de l'un à l'autre par projection stéréographique. Nous utilisons cette définition car nous allons utiliser les deux représentations de $\Pro_{1}(\C)$ dans la suite du développement.}
	
	\begin{theo}
		Le groupe circulaire $G$ est engendré par les homographies \textblue{($z \mapsto \frac{az+b}{cz+d}$ avec $ad-bc \neq 0$)} et la conjugaison complexe \textblue{($z \mapsto \overline{z}$)}.
	\end{theo}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement}}}{
		\begin{enumerate}
			\item Montrer que $G' \subseteq G$.
			\item Montrer que $G' \subset G$.
			\begin{enumerate}
				\item Montrer que $\phi$ préserve les divisions harmoniques.
				\item Montrer que $\phi$ est un automorphisme de corps de $\C$.
				\begin{enumerate}
					\item Montrer que $\phi(a+b) = \phi(a) + \phi(b)$.
					\item Montrer que $\phi\left(a^2\right) = \phi(a)^2$.
					\item Montrer que $\phi(ab) = \phi(a)\phi(b)$.
				\end{enumerate}
				\item Conclure.
			\end{enumerate}
		\end{enumerate}
	}
	
	\begin{proof}
		On note $G'$ le groupe engendré par les homographies et la conjugaison complexe. Montrons que $G' = G$. Pour cela, on raisonne par double inclusion.
		\paragraph{Étape 1: montrons que $G' \subseteq G$.} On commence par montrer un lemme qui caractérise l'alignement et la co-cyclicité par le birapport.
		
		\begin{lemme}{\cite[Proposition VI.7.5, p.202]{Audin}}
			Pour que quatre points de $\C$ soient alignés ou co-cycliques, il faut et il suffit que leur birapport soit réel.			
		\end{lemme}
		\begin{proof}
			Soient $a, b, c, d$ quatre points du plan. Comme on parle du birapport de ces quatre points, on a $a, b, c$ deux à deux distincts.
			\begin{itemize}
				\item Si $d$ coïncide avec $a, b$ ou $c$, alors $[a, b, c, d] = \infty, 1$ ou $0$. Donc $[a, b, c, d] \in \R$.
				\item Sinon, $[a, b, c, d] = \frac{c-a}{c-b} / \frac{d-a}{d-c}$ a pour argument une mesure de l'angle $(\overrightarrow{ca}, \overrightarrow{cb}) - (\overrightarrow{da}, \overrightarrow{db})$. Donc $[a, b, c, d] \in \R$ si et seulement si une mesure de l'angle $(\overrightarrow{ca}, \overrightarrow{cb}) - (\overrightarrow{da}, \overrightarrow{db})$ est nulle si et seulement si les quatre points sont cocycliques ou alignés.
			\end{itemize}
		\end{proof}
		
		Comme les homographies \textblue{(le birapport est un invariant du groupe projectif (des homographies))} et la conjugaison complexe \textblue{il faut l'écrire} préserve le birapport, on en déduit que les homographies et la conjugaison complexe préserve les droites et les cercles.
		
		\paragraph{Étape 2: montrons que $G' \subset G$.} On procède en trois étapes. Soit $\phi : \Pro_{1}(\C) \to \Pro_{1}(\C)$ une bijection préservant les droites et les cercles. Montrons qu'elle est la composée de homographies et de la conjugaison complexe. Quitte à composée par une homographie, on peut supposer $\phi(\infty) = \infty$.
		
		\subparagraph{Étape a: montrons que $\phi$ préserve les divisions harmoniques.} On rappelle alors que quatre points $a, b, c, d$ sont en division harmonique si $[a, b, c, d] = -1$.
		
		\begin{lemme}
			Si $a, b, c, d$ sont en division harmonique, alors $\phi(a), \phi(b), \phi(c)$ et $\phi(d)$ sont en division harmonique.
		\end{lemme} 
		\begin{proof}
			On considère trois points distincts $a, b, c \in \Pro_1(\C)$ et on souhaite construire l'unique point $d \in \Pro_1(\C)$ tel que $[a, b, c, d] = -1$ uniquement à l'aide de droite et de cercle \textblue{(comme $\phi$ conserve les droites et le cercles, elle va conservé les propriétés l'alignement et de co-cyclicité de ces points)}. On suppose que $c = \infty$ \textblue{(en effet, si $a, b, c$ sont alignés, on doit envoyer $d$ à l'infini donc en permutant $c$ et $d$, on obtient ce qu'on veut. Et sinon, on envoi $c$ à l'infini. D'où l'hypothèse)}. On pose $m \in \C$ tel que $abm$ soit un triangle équilatéral et on construit $d$ tel que le dessin sur la Figure \ref{fig:ConstructionD} \textblue{(on considère deux droites parallèles à $ab$ qui coupent $am$ et $bm$ en $x, z$ et en $y, t$. On note $d'$ le point d'intersection de $xt$ et $yz$. On construit $d$ comme étant le point d'intersection entre $ab$ et la droite passant pas $m$ et $d'$.)}). \textred{A partir de maintenant, on raisonne sutr $\Pro_{2} (\R)$.}
			
			Comme $[a, b, \infty, d] = -1$ si et seulement si $d$ est le milieu du segment $[ab]$, on montre que $(md)$ est la médiane issue de $m$ du triangle $abm$ \textblue{(cela montre que $d$ est bien le milieu de $[ab]$)}. Notons $\mathcal{D}$ cette médiane et montrons que $md = \mathcal{D}$. Comme $abm$ est un triangle équilatéral, $\mathcal{D}$ est aussi une bissectrice de $abm$ issue du point $m$. On en déduit que $x$ et $y$ (respectivement $z$ et $t$) sont symétriques par rapport à $\mathcal{D}$. Donc, $d' \in \mathcal{D}$ et $(md) = \mathcal{D}$.
			
			De plus, les droites $(abc), (xyc)$	et $(ztc)$ s'intersectent uniquement en $c$. Comme $\phi$ conserve les droites et est bijective les images de ces droites par $\phi$ on un unique point d'intersection $\phi(c) = \infty$. On montre que $\phi(d)$ est le milieu de $[\phi(a)\phi(b)]$ en se ramenant au cas précédent par une transformation affine \textblue{(on a bien conservation du parallélisme et de la conservation des milieux)}. 
			
			D'où le résultat.
		\end{proof}
		
		\begin{figure}
			\centering
	\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1cm,y=1cm]

\clip(-2.93,-0.08666666666666663) rectangle (5.354444444444446,5.3);

\fill[line width=2pt,color=rvwvcq,fill=rvwvcq,fill opacity=0.10000000149011612] (0.57,4.08) -- (-1.21,1.08) -- (2.25,1.04) -- cycle;

\draw [line width=2pt,color=rvwvcq] (0.57,4.08)-- (-1.21,1.08);

\draw [line width=2pt,color=rvwvcq] (-1.21,1.08)-- (2.25,1.04);

\draw [line width=2pt,color=rvwvcq] (2.25,1.04)-- (0.57,4.08);

\draw [line width=2pt,color=wrwrwr,domain=-2.93:5.354444444444446] plot(\x,{(--10.229066666666666-0.04*\x)/3.46});

\draw [line width=2pt,color=wrwrwr,domain=-2.93:5.354444444444446] plot(\x,{(--7.694933333333334-0.04*\x)/3.46});

\draw [line width=2pt,color=dtsfsf] (-0.5276257909777505,2.230068891610533)-- (1.1986068585425596,2.942520922637273);

\draw [line width=2pt,color=dtsfsf] (-0.09602393345580741,2.9574877526025722)-- (1.6059614206981012,2.2054031434986734);

\draw [line width=2pt,color=sexdts,domain=-2.93:5.354444444444446] plot(\x,{(--0.7067110421804026-1.406529160000542*\x)/-0.023286906622526082});

\begin{scriptsize}

\draw [fill=rvwvcq] (0.57,4.08) circle (2.5pt);

\draw[color=rvwvcq] (0.39444444444444454,4.273333333333332) node {$m$};

\draw [fill=rvwvcq] (-1.21,1.08) circle (2.5pt);

\draw[color=rvwvcq] (-1.5255555555555558,1.0733333333333328) node {a};

\draw [fill=rvwvcq] (2.25,1.04) circle (2.5pt);

\draw[color=rvwvcq] (2.323333333333334,1.233333333333333) node {b};

\draw [fill=wrwrwr] (-0.09602393345580741,2.9574877526025722) circle (2pt);

\draw[color=wrwrwr] (-0.2188888888888889,3.18) node {y};

\draw [fill=wrwrwr] (-0.5276257909777505,2.230068891610533) circle (2pt);

\draw[color=wrwrwr] (-0.5922222222222223,2.415555555555555) node {x};

\draw [fill=wrwrwr] (1.1986068585425596,2.942520922637273) circle (2pt);

\draw[color=wrwrwr] (1.2655555555555558,3.1177777777777766) node {$s$};

\draw [fill=wrwrwr] (1.6059614206981012,2.2054031434986734) circle (2pt);

\draw[color=wrwrwr] (1.6744444444444448,2.38) node {$t$};

\draw [fill=ffqqqq] (0.5467130933774739,2.673470839999458) circle (2pt);

\draw[color=ffqqqq] (0.847777777777778,2.7266666666666657) node {$d'$};

\draw [fill=sexdts] (0.52,1.06) circle (2pt);

\draw[color=sexdts] (0.6611111111111112,0.9577777777777774) node {$d$};

\end{scriptsize}

\end{tikzpicture}
		\caption{Construction de $d$.}
		\label{fig:ConstructionD}
		\end{figure}
		
		
		\subparagraph{Étape b: montrons que $\phi$ est un automorphisme de corps de $\C$} Quitte à composer par une homographie $\phi$, on peut supposer que $\phi(\infty) = \infty$, $\phi(0) = 0$ et $\phi(1) = 1$. Montrons que $\phi$ est un automorphisme de corps de $\C$.
		\begin{enumerate}
			\item Montrons que $\phi(a+b) = \phi(a) + \phi(b)$. Soit $a, b, c$ trois points de $\C$ distincts. Comme $[a, b, c, \infty] = -1$ si et seulement si $c = \frac{a+b}{2}$, alors $\phi$ conserve les milieux \textblue{(par ce qui précède)}: $\phi(\frac{a+b}{2}) = \frac{\phi(a) + \phi(b)}{2}$. En particulier pour $a = 0$, on a  $\phi(\frac{a}{2}) = \frac{\phi(a)}{2}$ soit $2\phi(\frac{a}{2}) = \phi(a)$. D'où $\phi(a+b) = \phi(a) + \phi(b)$.
			\item Montrons que $\phi(a^2) = \phi(a)^2$. $\forall a \in \C \setminus \{0; 1\}$, on a $[a, -a, a^2, 1] = -1$ donc comme $\phi$ conserve les divisions harmoniques, $[\phi(a), -\phi(a), \phi(a^2), 1] = -1$ et $[\phi(a), -\phi(a), \phi(a)^2, 1] = -1$ \textblue{(car vrai pour tout $a$)}, on obtient que $\phi(a^2) = \phi(a)^2$.
			\item Montrons que $\phi(ab) = \phi(a)\phi(b)$ pour $a$ et $b$ distincts. Par l'identité de polarisation $ab = \frac{1}{4}((a+b)^2 - (a-b)^2)$ et par conservation des milieux et des carrés, on obtient que $\phi(ab) = \phi(\frac{1}{4}((a+b)^2 - (a-b)^2)) = \frac{1}{4}((\phi(a)+\phi(b))^2 - (\phi(a)-\phi(b))^2) = \phi(a)\phi(b)$. D'où $\phi(ab) = \phi(a)\phi(b)$.
		\end{enumerate}
		
		\subparagraph{Étape c: conclure} Finalement, on sait que $\phi(\R) = \R$
		\textblue{(par préservation de l’ensemble des droites-cercles)}, et que $\phi_{|\Q} = Id_\Q$. Or $\phi_{|\R}$
		est croissante : en effet, pour tous $x, y \in \R$ tels que
		$x > y$, $\phi(x) - \phi(y) = \phi(\sqrt{x-y})^2 \geq 0$.
		On en déduit que $\phi_{|\R} = Id_\R$. En conclusion, $\phi$ est soit l’identité, soit la conjugaison complexe, ce qui achève la démonstration.
		
		
	\end{proof}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}