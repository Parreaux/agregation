\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#3]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#2
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#1};
	\end{tikzpicture}\bigskip%
} 

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\D}{\mathcal{D}}
	\newcommand{\Si}{\mathfrak{S}}
	\newcommand{\Sn}{\mathfrak{S}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\newtheorem{lnum}{Lemme}
	\newtheorem*{application}{Application}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{ex}{Exemple}
	\newtheorem*{appli}{Application}
	
	\title{Isométries du cube}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} H2G2  \cite[p.365,375]{Caldero-Germoni}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
		\textblue{\emph{Leçons où on présente le développement:}} 101 (actions de groupes); 105 (groupe des permutations); 182 (complexes en géométrie); 190 (dénombrement).
			
		\textblue{\emph{Leçons où on peut l'évoquer:}} 104 groupes finis); 108 (générateurs de groupes).
	}}
	
	\section{Introduction}
	
	Les isométries préservent le cube dans le sens où si j'applique une isométrie à un cube alors l'image de celle-ci reste un cube. Cette propriété importante nous permet d'étudier les isométries sur des sous-espaces (de dimension 3) afin de les caractériser ou d'en donner quelques propriétés. Les solides platoniciens convexe sont souvent utilisés à cet effet, on peut alors parler du tétraèdre. On utilise des actions de groupes et étudier le dénombrement du nombre de coloration du cube.
	
	Étudier les isométries des solides platoniciens convexes comme le tétraèdre et le cube donne de nombreuses applications comme le dénombrement (comme nous allons le présenter) ou encore les représentations linéaires avec la table de caractères de $\Si_4$.
	
	\section{Isométries du cube et application du coloriage}
	
	\titlebox{\textred{\textbf{Schéma du développement}}}{\parbox{0.9\textwidth}{
			\begin{enumerate}
				\begin{minipage}{.5\textwidth}
					\item Existence d'un morphisme de $C$ vers $\Si_4$.
					\item Injectivité du morphisme $\varphi$.
				\end{minipage} \hfill
				\begin{minipage}{.46\textwidth}
					\item Surjectivité du morphisme $\varphi$.
					\item Application au coloriage.
				\end{minipage}
			\end{enumerate}
		}}{red}
	
	\begin{theo}
		Soit $C$ un cube. Alors $Is^{+}(C) \simeq \Si_4$. \textblue{L'ensemble $Is^{+}(C)$ est l'ensemble des isométries positives du cube. ce sont les isométries que l'on peut réaliser sur un vrai cube. Elles sont également appelée déplacements.}
	\end{theo}
	
	\begin{proof}
		Soit $C$ un cube dont on numérote ses grandes diagonale de $\D_1$ à $\D_4$ et on note leur ensemble $\D$ et on centre $C$ en l'origine $O$ \textblue{(cela nous permet de considérer des isométries vectorielles : directement sur l'espace vectoriel sous-jacent)}.
			
		\paragraph{Étape 1 : existence d'un morphisme de $C$ vers $\mathfrak{S}_4$} Soit $f \in Is^{+}(C)$. Comme $f$ conserve les longueurs, elle conserve en particulier les longueurs des grandes diagonales \textblue{(qui est la plus grandes distances entre deux points du cube)}. Donc $f$ envoie $\D$ sur $\D$ \textblue{une isométrie transforme une grande diagonale en une grande diagonale}. 
		
		On fait donc agir $Is^{+}(C)$ sur $\D$, l'ensemble des grandes diagonales du cube. Ainsi 
		\begin{displaymath}
		\begin{tabular}{cccc}
			$\varphi$: & $Is^{+}(C)$ & $\to$ & $\Si_4$ \\
			& $g$ & $\mapsto$ & $g_{|\D}$
		\end{tabular}
		\end{displaymath}
		est un morphisme de groupe \textblue{(les distances sont conservées)}.
		
		\paragraph{Étape 2 : injectivité de ce morphisme $\varphi$} Montrons que l'action que nous venons de définir est fidèle. Soit $g$ tel que $\varphi(g) = id_{|\D}$ et montrons que $g$ est le neutre. On note $D_i$ la grande diagonale d'extrémité $A_iB_i$. Comme $g$ fixe $D_i$, alors soit $g$ fixe $A_i$ et $B_i$ soit il les permute.
		\begin{itemize}
			\item Si $g$ laisse fixe $A_i$ et $B_i$ pour un $i$ donnée, alors, montrons qu'il laisse fixe l'ensemble du cube. Sans perte de généralité, on suppose que $i=1$. \textblue{On montre qu'il laisse fixe quatre sommets: $A_1$, $A_2$, $A_4$ et $B_1$. Par hypothèse $A_1$ et $B_1$ sont laisser fixe par $g$.}
			\begin{itemize}
				\item Comme $A_1A_2 \neq A_1B_2$ \textblue{(on parle surtout de la distance entre deux points)} et que $g$ est une isométrie \textblue{(elle conserve les longueurs)} alors $A_2$ ne peut pas être envoyer sur $B_2$. De plus, $g$ laisse fixe les grandes diagonales donc $A_2$ est envoyer sur lui-même.
				\item On raisonne de même pour montrer que $A_4$ est envoyer sur lui-même.
			\end{itemize}
			$g$ est alors une isométrie qui laisse fixe le repère affine $A_1, A_2, A_4, B_1$ donc $g$ est l'identité.
			
			\item Si $g$ permute $A_i$ et $B_i$, alors $s_Og$ envoie $A_i$ sur $A_i$ où $s_O$ est la symétrie centrale du cube. De ce qui précède $s_Og = id$, or $\det s_O = -1$ ce qui est impossible car $\det g = 1$. \textblue{Autre méthode: Par un raisonnement analogue au cas précédent, si $g$ permute les extrémités d'une grande diagonale, il permute toutes les extrémités des grandes diagonales. Donc $g = s_O = - id$. D'où la contradiction.}
		\end{itemize}
		Donc $g$ est le neutre et $\varphi$ est donc injective.
		
		\textblue{\emph{Rappel}: une action fidèle si l'intersection des stabilisateurs des points de $X$ est trivial, soit s'il n'existe pas d'éléments de $G$ autre que $e$ qui fixe tous les points de $X$, soit le noyau du morphisme de l'action est trivial.}
		
		\paragraph{Étape 3 : surjectivité de ce morphisme $\varphi$} Les transpositions sont réalisée par une rotation d'angle $\pi$ et d'axe le milieu des arêtes joignant les deux diagonales. Donc comme les transpositions engendre $\Si_4$, $\varphi$ bien surjective. \textblue{(Dessiner ici le cube avec la rotation pour une des transitions.)}
		
		On en déduit que $Is^{+}(C) \simeq \Si_4$
	\end{proof}
	
	\paragraph{Étape 4 : application au dénombrement du coloriage.}
	\begin{application}
		Il existe $57$ manière différentes de colorier un cube avec $3$ couleurs.
	\end{application}
	
	\begin{proof}
		Dans la preuve précédente, nous avons montrer que $Is^{+}$ agit sur le cube (en particulier, il agit sur le cube colorié). Par la formule de Burnside, on a que le nombre de coloriage différent $c$ est égal au nombre d'orbite par cette action \textblue{(deux coloriages sont dits identiques s'il existe une isométrie positive permettant de passer de l'un à l'autre. Le nombre de coloriage différent est donc le nombre d'orbites de l'action et le même nombre d'orbite de l'action (car elle traduit exactement ceci))}. On en déduit que 
		\begin{displaymath}
		c = \frac{1}{|Is^{+}(C)|} \sum_{\sigma\in Is^{+}(C)}|Fix(\sigma)|
		\end{displaymath} 
		où $|Fix(\sigma)|$ est le nombre de coloriage invariant par la transformation.
		
		Pour chacune des transformation, nous allons calculer cette quantité. \textblue{Nous devons énumérer chacune des transformations de $\Si_4$ et donnée leur action sur le cube afin de regarder combien de coloriages restent invariant par cette transformation. L'ordre de cette énumération est variable mais il est plus simples de commencer par les opérations qui sont le plus facile à expliquer.}
		\begin{description}
			\item[Transposition] Comme nous l'avons vu dans la preuve précédente c'est une rotation d'angle $\pi$ d'axe la droite passant par le milieu des arêtes reliant les diagonales considéré. Il y a six isométries qui peuvent réaliser toutes les transpositions: autant que de paire de diagonales. Dans ce cas les faces vont par paires \textblue{(la face est envoyé sur son opposé et les faces bleu et rouges sont envoyés sur la face adjacente dont la frontière porte l'axe de rotation)}. Donc fixer trois couleurs suffit à tout avoir. Il nous faut donc $3^3$ points fixes.
			
			\item[$3$-cycles] Un $3$-cycle est réalisé par une rotation autours de la grande diagonale $\D_i$ qui n'intervient pas dans le cycle, d'angle $\frac{2\pi}{3}$ ou $-\frac{2\pi}{3}$. Il y a huit isométries qui peuvent réaliser toutes les transpositions: deux par diagonale (une par angle de rotation). La rotation découpe le cube en deux groupes de trois faces: deux couleurs suffisent. On obtient $3^2$ points fixes.
			
			\item[$4$-cycles] Un $4$-cycle est réalisé par une rotation autours de l'axe pris au centre de deux faces opposées \textblue{(on choisi les faces dont la numérotation suit l'ordre du $4$-cycle (dans le sens ou non des aiguilles d'une montre))}, d'angle $\frac{\pi}{2}$ ou $-\frac{\pi}{2}$. Il y a six isométries qui peuvent réaliser toutes les transpositions: deux par paire de faces (une par angle de rotation). On peut alors colorier les deux faces par lesquelles passent l'axe de la rotation comme on veut car elles restent invariantes. Cependant les quatre autres faces doivent avoir la même couleur \textblue{(car chacune doit avoir la couleur de ces deux adjacentes puisqu'elle va être ou va remplacé celles-ci)}. On obtient $3^3$ points fixes.
			
			\item[Double transpositions] Une double transposition est réalisé par une rotation  autours de l'axe pris au centre de deux faces opposées \textblue{(on choisi les faces dont les diagonales sont une extrémité des grandes diagonales agissant par la double transposition)}, d'angle $\pi$. Il y a trois isométries qui peuvent réaliser toutes les transpositions: une par paire de faces. On peut alors colorier les deux faces par lesquelles passent l'axe de la rotation comme on veut car elles restent invariantes. Les quatre autres faces vont deux par deux \textblue{(car le cube fait un demi-tour)}. On obtient $3^4$ points fixes.
		\end{description}
		
		On a alors $c = \frac{1}{26}(6*3^{3} + 8*3^2 + 6*3^3 + 3*3^4 + 1*3^6)= 57$. D'où le résultat.
	\end{proof}

	\section{Compléments autours des applications des actions de groupes}
	
	\subsection*{Isométries et leurs applications}
	\input{./../Notions/Geometrie_Isometries.tex}
	
	\subsection*{Compléments autours de la formule de Burnside} Dans l'application, nous dénombrons le nombre de coloriage du cube grâce à la formule de Burnside. Nous allons la redémontrer ici. Pour cela, nous commençons par rappeler quelques notions sur les actions de groupes.
	\input{./../Notions/ActionsGroupes.tex}
	
	\subsection*{Collier de perles}
	\input{./../Notions/Groupes_CollierPerles.tex}	
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
	
\end{document}