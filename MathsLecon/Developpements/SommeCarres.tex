\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\F}{\mathbb{F}}
	\newcommand{\Cun}{\mathcal{C}^1}
	\newcommand{\Cdeux}{\mathcal{C}^2}
	\newcommand{\Cinf}{\mathcal{C}^{\infty}}
	\newcommand{\Sn}{\mathfrak{S}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{OliveGreen}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	\newcommand{\textmagenta}[1]{\textcolor{DarkOrchid}{#1}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\newcommand{\pcite}[1]{\protect{\cite{#1}}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\newtheorem{lem}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	
	\title{Somme de deux carrés}
	
	\author{Julie Parreaux}
	\date{2018-2019}
		
	\maketitle
		
	\noindent\emph{Référence du développement:} Perrin \cite[p.56]{Perrin}.
		
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
		\textblue{\emph{Leçons où on présente le développement:}} 120 (Anneaux $\Z/n\Z$); 126 (Équations arithmétiques).
	}}
	
	\section{Introduction}
	
	Le théorème des deux carrés de Fermat énonce les conditions pour qu’un nombre entier soit la somme de deux carrés parfaits (c'est-à-dire de deux carrés d’entiers) et précise de combien de façons différentes il peut l’être. Dans le cadre des équations diophantiennes (ici c'est le cas) la simplicité de l'énoncé cache une difficulté réelle de démonstration. Certaines des preuves proposées ont aidé à la mise au point d'outils parfois sophistiqués: ici nous utilisons les entiers de Gauss. 
	
	
	\section{Théorème des deux carrés de Fermat}
	
	\noindent\emph{Problème}: Déterminer $n \in \N$ tel que $n = a^2 + b^2$ avec $a, b \in \N$. Autrement dit, quels sont les entiers de somme deux carrés? On cherche donc à déterminer $\Sigma = \{n \in \N | n = a^2 + b^2, a, b \in \Z\}$. 
	
	\begin{ex}
		$0, 1, 2, 4, 5, 8, 9, 10 \in \Sigma$ et $3, 6, 7, 11, 12 \notin \Sigma$
	\end{ex}
	
	\begin{req}
		Si $n \equiv 3 [4]$ alors $n \notin \Sigma$. En effet, si $a$ est paire alors $a^2 \equiv 0 [4]$ et si $a$ est impaire, $a^2 \equiv 1 [4]$ donc $n \not\equiv 3 [4]$.
	\end{req}
	
	\titlebox{red}{\textred{\textbf{Schéma du développement}}}{
		\emph{Idée}: On utilise les entiers de Gauss pour étudier $\Sigma$. En effet, si on se place dans $\C$ et que $n \in \Sigma$, alors $n = (a + ib)(a - ib)$. 
		\begin{enumerate}
			\item Étude de l'anneau $\Z[i]$.
			\begin{itemize}
				\item $\Z[i]$ est intègre.
				\item $\Z[i]^* = \{-1, -i, 1, i\}$ \textblue{(lemme~\ref{lem:inverseZ[i]})}.
				\item $\Z[i]$ est euclidien donc principal \textblue{(lemme~\ref{lem:euclidien})}.
			\end{itemize}
			\item Preuve dans le cas $p$ premier.
			\begin{description}
				\item[$\Leftarrow$] Immédiat par contraposée.
				\item[$\Rightarrow$] Caractérisation des irréductibles \textblue{(lemme~\ref{lem:irreductible})}.
			\end{description}
			\item Preuve du théorème.
			\begin{description}
				\item[$\Leftarrow$] Stabilité de $\Sigma$ par multiplication \textblue{(lemme~\ref{lem:multiplication})}.
				\item[$\Rightarrow$] Récurrence.
			\end{description}
		\end{enumerate}
	}
	
	\begin{theo}
		Soit $n \geq 1$ un entier. 
		\begin{displaymath}
		n \in \Sigma \Leftrightarrow v_p(n) \text{ est pair pour } p \equiv 3 [4]
		\end{displaymath}
		où $v_p(n)$ est défini par la décomposition en facteur premier de $n$: $n = \prod_{p \in \mathcal{P}} p^{v_p(n)}$.
	\end{theo}
	\begin{proof}
		\emph{Idée}: On utilise les entiers de Gauss pour étudier $\Sigma$. En effet, si on se place dans $\C$ et que $n \in \Sigma$, alors $n = (a + ib)(a - ib)$. 
		
		On pose $\Z[i] = \{a + ib \in \C | a, b \in \Z\}$.
		
		\paragraph{Étape 1 : étude de $\Z[i]$.}
		\begin{itemize}
			\item $\Z[i]$ est intègre car il est inclut dans $\C$.
			\item On muni $\Z[i]$ de l'automorphisme $\sigma : z = a + ib \mapsto \overline{z} = a - ib$.
			\item On muni $\Z[i]$ d'une norme multiplicative $N : z = a + ib \mapsto \overline{z}z = a^2 + b^2$
		\end{itemize}
		
		\begin{lem}
			$\Z[i]^* = \{-1, -i, 1, i\}$
			\label{lem:inverseZ[i]}
		\end{lem}
		\begin{proof}
			\begin{description}
				\item[$\subseteq$] 
				\begin{itemize}
					\item Si $z = a+ i b \in Z[i]^*$, alors il existe $z'$ tel que $zz' = 1$.
					\item En appliquant la norme \textblue{(multiplicative)}, $N(zz') = 1 = N(z)N(z')$.
					\item Comme $N(z), N(z') \in \N$ \textblue{(définition de la norme)}, $N(z) = N(z') = 1$.
					\item On en déduit que $a^2 + b^2 = 1$ soit $a= 0$ et $b \in \{1, -1\}$, soit $b = 0$ et $a \in \{1, -1\}$.
				\end{itemize}
				\item[$\supseteq$] Immédiat par calcul.
			\end{description}
		\end{proof}
		
		\begin{lem}
			L'anneaux $\Z[i]$ est euclidien relativement à $N$ donc il est principal.
			\label{lem:euclidien}
		\end{lem}
		\begin{proof}
			Soit $z, t \in \Z[i] \setminus \{0\}$. On cherche $q, r \in \Z[i]$ tel que $z = qt + r$ avec $N(r) < N(t)$.
			\begin{itemize}[label=$\to$]
				\item Soit $\frac{z}{t} \in \C$ \textblue{(possible dans $\C$)}, alors $\frac{z}{t} =x + iy$. On souhaite alors approcher $\frac{z}{t}$ par des entiers de Gauss.
				\item Soit $a, b$ les entiers les plus proches de $x, y$ \textblue{(existe par les propriétés sur les entiers)}. 
				\begin{itemize}
					\item Posons $q = a + ib$ et on a:
					\begin{displaymath}
					\begin{array}{ccll}
					\left|\frac{z}{t} - q\right| & = & \left|x + iy - a - ib \right| & \text{\textblue{(définition)}} \\
					& = & \sqrt{\left|x - a\right|^2 + \left|y - b\right|^2} & \text{\textblue{(définition du module)}}\\
					& \leq & \sqrt{\left(\frac{1}{2}\right)^2 + \left(\frac{1}{2}\right)^2} & \text{\textblue{(entier les plus proches)}}\\
					& = & \sqrt{\frac{2}{4}} & \text{\textblue{(calcul)}} \\
					& \leq & 1 & \\
					\end{array}
					\end{displaymath}
					
					\item Posons $r = z - qt$ et on a:
					\begin{itemize}[label=$\bullet$]
						\item $z \in \Z[i]$ \textblue{($z, t, q \in \Z[i]$)}
						\item $r = t\left(\frac{z}{t} - q\right)$ soit $\left|r\right| < \left|t\right|$. D'où en élevant au carré, $N(r) < N(t)$.
					\end{itemize}
				\end{itemize}
			\end{itemize}
		\end{proof}
		
		
		\paragraph{Étape 2 : preuve dans le cas $p$ premier.}
		
		\begin{prop}
			Soit $p \in \N$ un nombre premier.
			\begin{displaymath}
			p \in \Sigma \Leftrightarrow p \equiv 2 [4] \text{ou} p \equiv 1 [4]
			\end{displaymath}
		\end{prop}
		\begin{proof}
			\begin{description}
				\item[$\Leftarrow$] Immédiat par contraposée car par la remarque, on a que si $p \equiv 3 [4]$ alors $p \notin \Sigma$.
				\item[$\Rightarrow$] On utilise le lemme suivant.
				\begin{lem}
					$p \in \Sigma \Leftrightarrow p$ n'est pas irréductible dans $\Z[i]$.
					\label{lem:irreductible}
				\end{lem}
				\begin{proof}
					\begin{description}
						\item[$\Rightarrow$] Si $p = a^2 + b^2$ alors $p = (a+ ib)(a - ib)$ avec $a, b \neq 0$ \textblue{(sinon $p$p ne serait pas premier)}. Par le lemme~\ref{lem:inverseZ[i]} $a + ib$ et $a - ib$ ne sont donc pas inversibles. Donc $p$ n'est pas irréductible.
						
						\item[$\Leftarrow$] Si $p = zz'$ avec $z , z' \notin \Z[i]$ \textblue{(définition de ne pas être irréductible)}, on a $N(p) = N(z)N(z') = p^2$ \textblue{(par définition de la norme et car $p$ est premier)}. Comme $N(z), N(z') \in \N \setminus \{0, 1\}$, alors $p = N(z) = a^2 + b^2$ et $p$ est donc premier. Donc $p \in \Sigma$.
					\end{description}
				\end{proof}
				\begin{itemize}
					\item Comme $\Z[i]$ est principal \textblue{(lemme~\ref{lem:euclidien})}, donc factoriel, dire que $p$ est non irréductible, c'est dire que l'idéal $(p) = p\Z[i]$ est non premier. Donc $\Z[i] / (p)$ n'est pas intègre.
					
					\item Pour étudier ce quotient, on utilise l'isomorphisme suivant: $\Z[i] \simeq \Z[X] / (X^2 + 1)$ \textblue{(division euclidienne)}. Puis le théorème d'isomorphisme nous donne:
					\begin{displaymath}
					\begin{array}{ccll}
					\Z[i] / (p) & \simeq & \Z[X] / (X^2 + 1, p) & \text{\textblue{(définition de $\Z[i]$)}} \\
					& \simeq & \left(\Z[X] / (p)\right) / (X^2 + 1) & \text{\textblue{(théorème)}} \\
					& \simeq & \left(\Z/p\Z[X]\right) / (X^2 + 1) & \text{\textblue{(définition de $\Z/p\Z$)}} \\
					& \simeq & \left(\F_p[X]\right) / (X^2 + 1) & \text{\textblue{($p$ premier)}} \\
					\end{array}
					\end{displaymath}
					On en déduit que 
					\begin{displaymath}
					\begin{array}{ccl}
					(p) \text{ non premier} & \Leftrightarrow & X^2 + 1 \text{ non irréductible dans } \F_p \\
					& \Leftrightarrow & X^2 + 1 \text{ a une racine dans } \F_p \\
					& \Leftrightarrow & -1 \text{ est un carré dans } \F_p \\
					& \Leftrightarrow & p \equiv 1, 2 [4] \\
					\end{array}
					\end{displaymath}
				\end{itemize}
			\end{description}
		\end{proof}
		
		\paragraph{Étape 3: preuve du théorème.}
		Montrons maintenant le théorème dans le cas général.
		\begin{description}
			\item[$\Leftarrow$] On utilise le lemme suivant.
			\begin{lem}
				$\Sigma$ est stable par multiplication.
				\label{lem:multiplication}
			\end{lem}
			\begin{proof}
				\begin{itemize}
					\item $n \in \Sigma \Leftrightarrow \exists z \in \Z[i]$, $n = N(z)$ \textblue{(déjà vu)}.
					\item si $n, n' \in \Sigma$, $n = N(z)$ et $n' = N(z')$ donc $nn' = N(zz') \in \Sigma$
				\end{itemize}
			\end{proof}
			$\Sigma$ étant stable par multiplication si $p \equiv 1, 2 [4]$ alors $p^{v_p(n)} \in \Sigma$. Sinon, $p \equiv 3 [4]$, $p \in \Sigma$ donc comme $v_p(n)$ est pair et $p^{v_p(n)} \in \Sigma$. On conclut par la stabilité de $\Sigma$.
			
			\item[$\Rightarrow$] Supposons que $p \equiv 3 [4]$. Montrons par récurrence sur $n \in \N$ que $v_p(n)$ est paire.
			\begin{description}
				\item[Initialisation] $v_p(0) = 0$ ok
				\item[Hérédité] soit $n$ tel que pour tout $m < n$, $v_p(m)$ est pair. Si $v_p(n) = 0$ ok. Sinon, $p | n = a^2 + b^2 = (a + ib)(a - ib)$. Comme $p \in \Z$, $p | a$ et $p | b$ donc $p^2 | n$. On en déduit que $a = pa'$ et $b = pb'$ soit $\frac{n}{p^2} = a'^2 + b'^2 \in \Sigma$. On en déduit $v_p\left(\frac{n}{p^2}\right) = v_p(n) - 2$ est pair \textblue{(car $v_p\left(\frac{n}{p^2}\right)$ est pair par hypothèse de récurrence)}.
			\end{description}
		\end{description}
	\end{proof}

	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}