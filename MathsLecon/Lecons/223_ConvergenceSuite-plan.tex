\chapter*{Leçon 223: Suites numériques. Convergence, valeurs d'adhérence. Exemple et application.}
\addcontentsline{toc}{chapter}{Leçon 223: Suites numériques. Convergence, valeurs d'adhérence. Exemple et application.}

\titlebox{blue}{Références pour la leçon}{
	\textblue{\cite{Gourdon-analyse}} Gourdon, \emph{Analyse.}
	
	\textblue{\cite{Marco-Thieullen-Weil}} Marco, Thieullen et Weil, \emph{Mathématiques pour la L2.}
	
	\textblue{\cite{Rouviere}} Rouvière, \emph{Petit guide de calcul différentiel à l'usage de la licence et de l'agrégation.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.35\textwidth}
		Suite de polygones
	\end{minipage} \hfill
	\begin{minipage}{.5\textwidth}
		Méthode de Newton
	\end{minipage}
}

\section*{Motivation}

\subsection*{Défense}

\begin{itemize}
	\item Notion d'itération introduite par Archimède afin de calculer une valeur de $\pi$.
	\item Formalisation via les travaux de Cauchy et de Gauss.
	\item Étude des suites dans un espace vectoriel normé s'y ramène.
\end{itemize}

\subsection*{Ce qu'en dit le jury}

Cette leçon permet souvent aux candidats de s’exprimer. Il ne faut pas négliger les suites de nombres complexes mais les suites vectorielles (dans $R^n$) ne sont pas dans le sujet. Le jury attire l’attention sur le fait que cette leçon n’est pas uniquement à consacrer à des suites convergentes, mais tout comportement asymptotique peut être présenté. Le théorème de Bolzano-Weierstrass doit être cité et le candidat doit être capable d’en donner une démonstration. On attend des candidats qu’ils parlent des limites inférieure et supérieure d’une suite réelle bornée, et qu’ils en maîtrisent le concept. Les procédés de sommation peuvent être éventuellement évoqués mais le théorème de Cesàro doit être mentionné et sa preuve maîtrisée par tout candidat à l’agrégation. Les résultats autour des sous-groupes additifs de $R$ permettent d’exhiber des suites denses remarquables et l’ensemble constitue un joli thème. Des thèmes des leçons 225 et 226 peuvent également se retrouver dans cette leçon. 

Pour aller plus loin, un développement autour de l’équirépartition est tout à fait envisageable. La méthode de Newton peut aussi illustrer la notion de vitesse de convergence.

\section*{Métaplan}

\begin{footnotesize}
	\noindent\emph{Cadre}: On se place dans un corps $K$ qui est $\R$ ou $\C$.
\end{footnotesize}

\begin{description}
	\item[I.] \textbf{Convergence d'une suite}
	\begin{small}
		\textblue{L'étude de la convergence d'une suite est une propriété que l'on vérifie dès que nous manipulons des suites.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Notion de convergence}
		\begin{small}
			\textblue{Nous commençons par définir la notion de convergence pour la suite et la divergence d'une suite.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.35\textwidth}
					\item \definiton: Suite
					\item \proposition: Unicité de la limite
					\item \proposition: Espace vectoriel
					\item \definiton: Divergence
				\end{minipage} \hfill
				\begin{minipage}{.5\textwidth}
					\item \definiton: Convergence
					\item \application: Caractérisation séquentielle de la continuité
					\item \application: Limites géométrique et arithmétique
					\item \remarque: Suite divergente
				\end{minipage}
				\item \definiton: Notation de Landau
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Propriétés de la limite}
		\begin{small}
			\textblue{Étudions le comportement pour une suite monotone, bornée et sur les opérations.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.45\textwidth}
					\item \thm: Comportement des limites $+$, $\times$, $\leq$
				\end{minipage} \hfill
				\begin{minipage}{.35\textwidth}
					\item \definiton: Suite monotone, bornée
				\end{minipage}
				\item \proposition: Monotone et bornée est convergence
				\item \proposition: Convergence implique suite bornée
				
				\begin{minipage}{.4\textwidth}
					\item \remarque: Réciproque est fausse
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \proposition: Critère de convergence
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Valeurs d'adhérence}
		\begin{small}
			\textblue{Les valeurs d'adhérence sont des limites de suites extraites On définie alors la notion de liminf et limsup qui sont des limites moins puissantes que les autres.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Suite extraite
					\item \thm: Théorème de Bolzano--Weierstrass
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \definiton: Valeurs d'adhérence
					\item \application: Principe des compacts croissant
				\end{minipage}
				\item \proposition: Lien entre valeur d'adhérence et limite
				\item \definiton: Limite supérieure et inférieure d'adhérence
				
				\begin{minipage}{.35\textwidth}
					\item \proposition: Inégalité
					\item \remarque: Existence
				\end{minipage} \hfill
				\begin{minipage}{.45\textwidth}
					\item \application: Critère d'Hadamard
					\item \proposition: Liminf - Limsup et valeurs d'adhérence
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[D.] \emph{Convergence au sens des complexes}
		\begin{small}
			\textblue{Lorsqu'on regarde une suite de complexe, sa convergence se lit dans la convergence de la suite induite par sa norme.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.3\textwidth}
					\item \definiton: Convergence
				\end{minipage} \hfill
				\begin{minipage}{.55\textwidth}
					\item \proposition: Caractérisation via la partie réelle et imaginaire
				\end{minipage}
				\begin{minipage}{.5\textwidth}
					\item \remarque: Analogue au cas d'un espace vectoriel
				\end{minipage} \hfill
				\begin{minipage}{.35\textwidth}
					\item \textred{\application: Suite de polygones}
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[II.] \textbf{Convergence de suites particulières}
	\begin{small}
		\textblue{Nous donnons quelques études de convergences pour quelques suites particulières: suite adjacente, de Cauchy, définie par récurrence.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Suites adjacentes et de Cauchy}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.5\textwidth}
					\item \definiton: Suite adjacente
					\item \application: Théorème des valeurs intermédiaires
				\end{minipage} \hfill
				\begin{minipage}{.35\textwidth}
					\item \proposition: Même limite
					\item \application: Séries alternées
				\end{minipage}
				\begin{minipage}{.4\textwidth}
					\item \thm: Théorème des gendarmes
					\item \exemple: Série de Riemann
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \application: Comparaison série-intégrale
					\item \definiton: Convergence et suite de Cauchy
				\end{minipage}
				\item \application: Divergence de $H_n$
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Suites récurrentes}
		\begin{small}
			\textblue{Les suites définies par récurrence peuvent faire l'objet d'une leçon entière. Cependant, dans le cadre  de la convergence, la régularité de la fonction $f$ va jouer un rôle prédominant.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.45\textwidth}
					\item \emph{Cadre}: $u_{n + 1} = f\left(u_n\right)$
					\item \application: $u_{n+1} = au_n + b$ et $u_{n+1} = \frac{au_n + b}{cu_n + d}$
					\item \thm: Point fixe de Picard
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \proposition: Continue: point fixe
					\item \proposition: Lien entre monotonie et limité
					\item \textred{\thm: Méthode de Newton}
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[III.] \textbf{Sommation}
	\begin{small}
		\textblue{La sommation est une suite logique dans l'étude de la convergence des suites (on peut les voir comme des suites). Nous donnons donc quelques résultats sur les séries et leurs convergences, leur équivalents, ...}
	\end{small}
	\begin{footnotesize}
		\begin{itemize}[label=$\rightsquigarrow$]
			\item \proposition: Sommation et notations de Landau
			
			\begin{minipage}{.4\textwidth}
				\item \proposition: Série harmonique
				\item \thm: Lemme de Cesàro
			\end{minipage} \hfill
			\begin{minipage}{.4\textwidth}
				\item \proposition: Stirling
				\item \application: Convergence lente
			\end{minipage}
		\end{itemize}
	\end{footnotesize}
\end{description}