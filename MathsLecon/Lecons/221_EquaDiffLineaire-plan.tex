\chapter*{Leçon 221: Équations différentielles linéaires. Systèmes d'équations différentielles linéaires. Exemples et applications.}
\addcontentsline{toc}{chapter}{Leçon 221: Équations différentielles linéaires. Systèmes d'équations différentielles linéaires. Exemples et applications.}

\titlebox{blue}{Références pour la leçon}{
	\textblue{\cite{Berthelin}} Berthelin, \emph{Équations différentielles.}
	
	\textblue{\cite{Demailly}} Demailly, \emph{Analyse numérique et équations différentielles.}
	
	\textblue{\cite{Gourdon-analyse}} Gourdon, \emph{Analyse.}
	
	\textblue{\cite{ZuilyQueffelec2}} Queffélec et Zuily, \emph{Éléménts d'analyse pour l'agrégation.}
	
	\textblue{\cite{Rouviere}} Rouvière, \emph{Petit guide de calcul différentiel à l'usage de la licence et de l'agrégation.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.4\textwidth}
		Théorème de Cauchy -- Lipschitz
	\end{minipage} \hfill
	\begin{minipage}{.45\textwidth}
		Nombre de zéros d'une équation
	\end{minipage}
}

\section*{Motivation}

\subsection*{Défense}

\begin{itemize}
	\item $XVII^{\text{ième}}$ siècle: apparition du calcul différentiel et intégrable (apporte une riche théorie mathématiques)
	\item Les équations différentielles apparaissent alors en géométrie ou en mécanique (applications)
	\item À leur début, pas de recherche d'existence ni d'unicité (on ne cherche qu'à les résoudre sans savoir si c'est possible).
	\item On peut linéariser les équations non-linéaires. On étudie ensuite cette équation linéarisée.
\end{itemize}

\subsection*{Ce qu'en dit le jury}

Le jury attend d’un candidat qu’il sache déterminer rigoureusement la dimension de l’espace vectoriel des solutions. Le cas des systèmes à coefficients constants fait appel à la réduction des matrices qui doit être connue et pratiquée. Le jury attend qu’un candidat puisse mettre en œuvre la méthode de variation des constantes pour résoudre une équation différentielle linéaire d’ordre 2 simple (à coefficients constants par exemple) avec second membre ; un trop grand nombre de candidats se trouve déstabilisés par ces questions.

L’utilisation des exponentielles de matrices a toute sa place ici et doit être maîtrisée. Les problématiques de stabilité des solutions et le lien avec l’analyse spectrale devraient être exploitées.

Le théorème de Cauchy--Lipschitz linéaire constitue un exemple de développement pertinent pour cette leçon. Les résultats autour du comportement des solutions, ou de leurs zéros, de certaines équations linéaires d’ordre 2 (Sturm, Hill--Mathieu,...) sont aussi d’autres possibilités.

Pour aller plus loin, la résolution au sens des distributions d’équations du type $T' = 0$, ou des situations plus ambitieuses, trouvera sa place dans cette leçon.

\section*{Métaplan}

\begin{footnotesize}
	\noindent\emph{Cadre}: $K = \R$ ou $\C$, $n \in \N^*$, $I$ intervalle de $\R$.
\end{footnotesize}

\begin{description}
	\item[I.] \textbf{Équations différentielles linéaires}
	\begin{small}
		\textblue{Lorsque nous étudions les équations différentielles, qu'appelle-t-on les équations différentielles linéaires? Nous nous intéressons au cas très particulier des équations différentielles linéaires. Le cadre linéaire apportent quelques propriétés sur les solutions, leurs existences et leurs unicités.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Notion d'équation différentielle linéaire \cite[p.357]{Gourdon-analyse}}
		\begin{small}
			\textblue{Nous définissons ce que nous appelons équation différentielle linéaire.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Équation différentielle linéaire
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \definiton: Équation homogène
				\end{minipage}
				\item \proposition: Revenir à l'ordre $1$
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Notions de solutions \cite[p.131]{Demailly}}
		\begin{small}
			\textblue{Lorsqu'on se donne une équation différentielle linéaire qu'appelle-t-on une solution? Dans de nombreuses situations concrète, la variable $t$ représente le temps et $y$ représente les différents paramètres de l'état de notre système. Résoudre un problème de Cauchy revient à décrire l'état d'un système en fonction du temps à partir d'un temps et des configurations initiaux. Prendre un unique exemple de ces notions sur une équation différentielle que nous pouvons commenter à l'oral.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton~\cite[p.353]{Gourdon-analyse}: Solution
					\item \definiton: Prolongement des solutions
					\item \thm: Existence d'une solution maximale
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \definiton: Problème de Cauchy
					\item \definiton: Solution maximale
					\item \definiton: Solution globale
				\end{minipage}
				\item \proposition~\cite{Berthelin}: Une solution globale est maximale
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Existence et unicité des solutions}
		\begin{small}
			\textblue{Comment savoir si une équation différentielle linéaire admet une solution? Le cas linéaire nous facilite bien l'existence quand à ces questions.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \proposition~\cite[p.377]{Gourdon-analyse}: Lemme de Grönwall
				\item \textred{\thm~\cite[p.180]{Rouviere}: Théorème de Cauchy--Lipschitz}
				\item \thm~\cite{Berthelin}: Théorème de Cauchy--Lipschitz linéaire
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[II.] \textbf{Propriétés des solutions}
	\begin{small}
		\textblue{Nous allons donner quelques propriétés des solutions: nous effectuons une étude qualitative de celles-ci.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Ensemble des solutions \cite{Berthelin}}
		\begin{small}
			\textblue{Dans le cadre des équations différentielles linéaires nous sommes capable de caractériser l'ensemble des solutions. De plus, plus précisément, nous sommes capable d'en donner une structure. On obtient ainsi une méthode afin de trouver les solutions de notre équation.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Notation}: $S_H$ et $S$
					\item \corollaire: Structure de $S$
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \proposition: Structure de $S_H$
					\item \application: Calcul d'une solution générale
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Stabilité des solutions \cite[p.380]{ZuilyQueffelec2}}
		\begin{small}
			\textblue{On étudie la stabilité des solutions  c'est-à-dire leur comportement à l'infini. On peut ainsi tracer des portrait de phases.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Système stable \textblue{(Dessin)}
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \definiton: Système instable \textblue{(Dessin)}
				\end{minipage}
				\item \definiton: Système asymptotiquement stable \textblue{(Dessin)}
				\item \remarque: On se limite à l'étude de la fonction nulle
				
				\begin{minipage}{.44\textwidth} 
					\item \thm: Stabilité dans le cas linéaire
					\item \thm~\cite[p.132]{Rouviere}: Théorème de Lyapunov
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \definiton~\cite[p.132]{Rouviere}: Système linéarisé
					\item \exemple~\cite[p.132]{Rouviere}: Équation du pendule
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Étude qualitative de l'équation $y'' + py' + qy = 0$}
		\begin{small}
			\textblue{On réalise une étude qualitative des solutions de cette famille d'équation. Pour ce donner une idée de la tête des solutions, on commence par la résoudre dans le cas constant (on voit apparaître la résolution via les polynômes caractéristiques). Ensuite, on étudie le nombre de zéros de cette équation. Enfin on termine par l'étude de la régularité de ces solutions.}
		\end{small}
		
		\begin{footnotesize}
			\noindent\textbf{Formes des solutions dans le cas constant \cite{Berthelin}}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.38\textwidth}
					\item \definiton: Équation caractéristique
					\item \application: Oscillateur harmonique
				\end{minipage} \hfill
				\begin{minipage}{.48\textwidth}
					\item \proposition: Résolution via le polynôme caractéristique
					\item \application: Équation d'Euler d'ordre 2
				\end{minipage}
			\end{itemize}
			
			\noindent\textbf{Étude des zéros des solution \cite[p.404]{ZuilyQueffelec2}}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \textred{\thm: Dénombrement des zéros}
				\end{minipage} \hfill
				\begin{minipage}{.46\textwidth}
					\item \exemple: $Id$, $t \mapsto t^2$
				\end{minipage}
				\item \cexemple: $q(x) = \frac{1}{4x^2}$; $q'(x) = \frac{1}{2x^3}$; $y'' = q(t)y$.
			\end{itemize}
			
			\noindent\textbf{Régularité des solutions \cite[p.404]{ZuilyQueffelec2}}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.48\textwidth}
					\item \proposition~\cite[p.130]{Demailly}: Régularité des solutions
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \proposition~\cite[p.378]{Gourdon-analyse}: Solution est bornée
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[III.] \textbf{Résolution explicite d'équations différentielles linéaires}
	\begin{small}
		\textblue{Le cas des équations différentielles linéaires est le cas facile de la résolution d'équation (d'où la recherche d'équation linéarisé). Nous allons présenter quelques méthode afin de résoudre des équations dans ce cadre.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Le cas de l'ordre un \cite{Berthelin}}
		\begin{small}
			\textblue{Le cas de l'ordre un est le plus facile, en intégrant et en appliquant la méthode de résolution vu dans la partie II, on fait juste apparaître une exponentielle.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.5\textwidth}
					\item \proposition: Solution d'une équation homogène
				\end{minipage} \hfill
				\begin{minipage}{.35\textwidth}
					\item \proposition: Solution générale
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Cas particulier des coefficients constants \cite{Berthelin}}
		\begin{small}
			\textblue{La résolution dans ce cas reste relativement simple. Cependant, elle demande l'introduction de nouveau outils comme l'exponentielle de matrices.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.44\textwidth}
					\item \emph{Cadre}: Solution que nous souhaitons résoudre
				\end{minipage} \hfill
				\begin{minipage}{.36\textwidth}
					\item \definiton: Exponentielle de matrice
				\end{minipage} 
				\item \proposition: Calcul de cet exponentielle de matrice
				\item \proposition: Comportement face à la dérivation
				\item \proposition: Propriétés d'une exponentielle de matrice
				\item \proposition: Solution d'un tel système d'équation
				\item \proposition: Base de l'espace des solutions
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Résolvantes et Wronskien \cite{Demailly}}
		\begin{small}
			\textblue{Lorsque les coefficients ne sont plus constants, la résolution est toujours possible mais nous devons faire appelle à des outils plus puissant comme la résolvante et le Wronskien}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Cadre}: Équation que l'on souhaite résoudre
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \definiton: Système fondamental de solution
				\end{minipage} 
				\begin{minipage}{.45\textwidth}
					\item \definiton: Résolvante
					\item \proposition: Comportement face à la dérivation
					\item \proposition: Solution d'un tel système d'équation
				\end{minipage} \hfill
				\begin{minipage}{.35\textwidth}
					\item \proposition: Caractère isomorphique
					\item \proposition: Propriétés de la résolvante
					\item \definiton: Wronskien
				\end{minipage} 
				\item \proposition~\cite{Berthelin}: Identité d'Abel
			\end{itemize}
		\end{footnotesize}
		\item[D.] \emph{Méthode de la variation de la constante \cite{Berthelin}}
		\begin{small}
			\textblue{La résolvante est un outil lourd et difficile à manipuler. La méthode de la constante permet de simplifier la résolution de tels équations différentielles.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.6\textwidth}
					\item \emph{Principe}: Solution homogène donne une solution globale
				\end{minipage} \hfill
				\begin{minipage}{.2\textwidth}
					\item \emph{Méthode}
				\end{minipage} 
				\item \proposition: Formule de Duhamel
			\end{itemize}
		\end{footnotesize}
	\end{description}
\end{description}
	