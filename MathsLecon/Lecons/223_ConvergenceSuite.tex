\documentclass[a4paper,8pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais,english]{babel}%

\usepackage[dvipsnames]{xcolor}

\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{enumitem}
\usepackage{appendix}
\usepackage{stmaryrd}
\usepackage{amsmath}
\usepackage{wasysym}

\usepackage{listings}
\usepackage{multirow}
\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}
\algnewcommand{\LineComment}[1]{\State \(\triangleright\) #1}

\parskip=0\baselineskip %0.5 d'habitude


\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\F}{\mathbb{F}}
	\newcommand{\Sn}{\mathfrak{S}}
	\newcommand{\A}{\mathfrak{A}}
	
	\newcommand{\thm}{\emph{Théorème}}
	\newcommand{\proposition}{\emph{Proposition}}
	\newcommand{\lem}{\emph{Lemme}}
	\newcommand{\corollaire}{\emph{Corollaire}}
	\newcommand{\definiton}{\emph{Définition}}
	\newcommand{\remarque}{\emph{Remarque}}
	\newcommand{\application}{\emph{Application}}
	\newcommand{\exemple}{\emph{Exemple}}
	\newcommand{\cexemple}{\emph{Contre-exemple}}
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textbluesmall}[1]{{\small{\textcolor{blue}{#1}}}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\sloppy
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\theoremstyle{plain}
	\newtheorem{theo}{Théorème}
	\newtheorem{prop}{Proposition}
	\newtheorem{cor}{Corollaire}
	\newtheorem{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem{definition}{Définition}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	
	\title{Leçon 223: Suites numériques. Convergence, valeurs d'adhérence. Exemple et application.}
	
	%\author{Julie Parreaux}
	\date{}
	\maketitle
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
	
	\section*{Motivation}
	
	\subsection*{Défense}
	
	\begin{itemize}
		\item Notion d'itération introduite par Archimède afin de calculer une valeur de $\pi$.
		\item Formalisation via les travaux de Cauchy et de Gauss.
		\item Étude des suites dans un espace vectoriel normé s'y ramène.
	\end{itemize}

	\subsection*{Ce qu'en dit le jury}
	
	Cette leçon permet souvent aux candidats de s’exprimer. Il ne faut pas négliger les suites de nombres complexes mais les suites vectorielles (dans $R^n$) ne sont pas dans le sujet. Le jury attire l’attention sur le fait que cette leçon n’est pas uniquement à consacrer à des suites convergentes, mais tout comportement asymptotique peut être présenté. Le théorème de Bolzano-Weierstrass doit être cité et le candidat doit être capable d’en donner une démonstration. On attend des candidats qu’ils parlent des limites inférieure et supérieure d’une suite réelle bornée, et qu’ils en maîtrisent le concept. Les procédés de sommation peuvent être éventuellement évoqués mais le théorème de Cesàro doit être mentionné et sa preuve maîtrisée par tout candidat à l’agrégation. Les résultats autour des sous-groupes additifs de $R$ permettent d’exhiber des suites denses remarquables et l’ensemble constitue un joli thème. Des thèmes des leçons 225 et 226 peuvent également se retrouver dans cette leçon. 
	
	Pour aller plus loin, un développement autour de l’équirépartition est tout à fait envisageable. La méthode de Newton peut aussi illustrer la notion de vitesse de convergence.
	
	\section*{Métaplan}
	
	\begin{footnotesize}
		\emph{Cadre}: On se place dans un corps $K$ qui est $\R$ ou $\C$.
	\end{footnotesize}
	
	\begin{description}
		\item[I.] \textbf{Convergence d'une suite}
		
		\begin{small}
			\textblue{L'étude de la convergence d'une suite est une propriété que l'on vérifie dès que nous manipulons des suites.}
		\end{small}
		\begin{description}
			\item[A.] \emph{Notion de convergence}
			
			\begin{small}
				\textblue{Nous commençons par définir la notion de convergence pour la suite et la divergence d'une suite.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \definiton: Suite
						\item \definiton: Convergence
						\item \proposition: Unicité de la limite
						\item \application: Caractérisation séquentielle de la continuité
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \proposition: Espace vectoriel
						\item \application: Limites géométrique et arithmétique
						\item \definiton: Divergence
						\item \remarque: Suite divergente
						\item \definiton: Notation de Landau
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[B.] \emph{Propriétés de la limite}
			
			\begin{small}
				\textblue{Étudions le comportement pour une suite monotone, bornée et sur les opérations.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \thm: Comportement des limites $+$, $\times$, $\leq$
						\item \definiton: Suite monotone, bornée
						\item \proposition: Monotone et bornée est convergence
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \proposition: Convergence implique suite bornée
						\item \remarque: Réciproque est fausse
						\item \proposition: Critère de convergence
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[C.] \emph{Valeurs d'adhérence}
			
			\begin{small}
				\textblue{Les valeurs d'adhérence sont des limites de suites extraites On définie alors la notion de liminf et limsup qui sont des limites moins puissantes que les autres.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \definiton: Suite extraite
						\item \definiton: Valeurs d'adhérence
						\item \thm: Théorème de Bolzano--Weierstrass
						\item \application: Principe des compacts croissant
						\item \proposition: Lien entre valeur d'adhérence et limite
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \definiton: Limite supérieure et inférieure
						\item \proposition: Inégalité
						\item \application: Critère d'Hadamard
						\item \remarque: Existence
						\item \proposition: Liminf - Limsup et valeurs d'adhérence
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[D.] \emph{Convergence au sens des complexes}
			
			\begin{small}
				\textblue{Lorsqu'on regarde une suite de complexe, sa convergence se lit dans la convergence de la suite induite par sa norme.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.5\textwidth}
						\item \definiton: Convergence
						\item \proposition: Caractérisation via la partie réelle et imaginaire
					\end{minipage} \hfill
					\begin{minipage}{.32\textwidth}
						\item \remarque: Analogue au cas d'un espace vectoriel
						\item \application: Suite de polygones \textred{(DEV)}
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
		\end{description}
		\item[II.] \textbf{Convergence de suites particulières}
		
		\begin{small}
			\textblue{Nous donnons quelques études de convergences pour quelques suites particulières: suite adjacente, de Cauchy, définie par récurrence.}
		\end{small}
		\begin{description}
			\item[A.] \emph{Suites adjacentes et de Cauchy}
			
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \definiton: Suite adjacente
						\item \proposition: Même limite
						\item \application: Théorème des valeurs intermédiaires
						\item \application: Séries alternées
						\item \thm: Théorème des gendarmes
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \application: Comparaison série-intégrale
						\item \exemple: Série de Riemann
						\item \definiton: Convergence et suite de Cauchy
						\item \application: Divergence de $H_n$
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[B.] \emph{Suites récurrentes}
			
			\begin{small}
				\textblue{Les suites définies par récurrence peuvent faire l'objet d'une leçon entière. Cependant, dans le cadre  de la convergence, la régularité de la fonction $f$ va jouer un rôle prédominant.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \emph{Cadre}: $u_{n + 1} = f\left(u_n\right)$
						\item \proposition: Continue: point fixe
						\item \application: $u_{n+1} = au_n + b$ et $u_{n+1} = \frac{au_n + b}{cu_n + d}$
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \proposition: Lien entre monotonie et limité
						\item \thm: Point fixe de Picard
						\item \thm: Méthode de Newton \textred{(DEV)}
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
		\end{description}
		\item[III.] \textbf{Sommation}
		
		\begin{small}
			\textblue{La sommation est une suite logique dans l'étude de la convergence des suites (on peut les voir comme des suites). Nous donnons donc quelques résultats sur les séries et leurs convergences, leur équivalents, ...}
		\end{small}
	
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \proposition: Sommation et notations de Landau
					\item \proposition: Série harmonique
					\item \proposition: Stirling
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \thm: Lemme de Cesàro
					\item \application: Convergence lente
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	
	\section*{Quelques notions importantes}
	
	

	
\end{document}