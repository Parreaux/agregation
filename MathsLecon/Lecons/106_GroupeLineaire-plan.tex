\chapter*{Leçon 106 : Groupe linéaire d’un espace vectoriel de dimension finie $E$, sous-groupes de $GL(E)$. Applications.}
\addcontentsline{toc}{chapter}{Leçon 106 : Groupe linéaire d’un espace vectoriel de dimension finie $E$, sous-groupes de $GL(E)$. Applications.}

\titlebox{blue}{Références pour la leçon}{
	\textblue{\cite{Caldero-Germoni}} Caldero et Germoni, \emph{Histoires hédonistes de groupes et de géométrie, tome 1.}
	
	\textblue{\cite{Caldero-Germoni-N2}} Caldero et Germoni, \emph{Nouvelles histoires hédonistes de groupes et de géométrie, tome 2.}
	
	\textblue{\cite{Cognet}} Cognet, \emph{Algèbre linéaire.}
	
	\textblue{\cite{FrancinouGianellaNicolas-al2}} Francinou, Gianella et Nicolas, \emph{Oraux X-ENS, Algère 2}
	
	\textblue{\cite{Gourdon-algebre}} Gourdon, \emph{Algèbre.}
	
	\textblue{\cite{Nourdin}} Nourdin, \emph{Agrégation de mathématiques épreuve orale.}
	
	\textblue{\cite{Perrin}} Perrin, \emph{Cours d'algèbre.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.35\textwidth}
		Cardinal du cône nilpotent
	\end{minipage} \hfill
	\begin{minipage}{.5\textwidth}
		Caractérisation de $O_{p, q}$
	\end{minipage}
}
	
\section*{Motivation}

\subsection*{Défense}

Le groupe linéaire est un des premiers groupes que l'on peut introduire pour parler de théorie des groupes en algèbre linéaire. Il est composé des automorphismes d'un espace vectoriel. On est alors amené à utiliser des notions d'algèbre linéaire pour étudier un groupe.

\subsection*{Ce qu'en dit le jury}

Cette leçon ne doit pas se résumer à un catalogue de résultats épars sur $GL(E)$. Il est important de savoir faire correspondre les sous-groupes du groupe linéaire avec les stabilisateurs de certaines actions naturelles (sur des formes quadratiques, symplectiques, sur des drapeaux, sur une décomposition en somme directe, etc.). On doit présenter des systèmes de générateurs de $GL(E)$ et étudier la topologie de ce groupe en précisant pourquoi le choix du corps de base est important. Les liens avec le pivot de Gauss sont à détailler. Il faut aussi savoir réaliser $\mathfrak{S}_n$ dans $GL_n(K)$ et faire le lien entre signature et déterminant, et entre les classes de conjugaison et les classes de similitude.

S’ils le désirent, les candidats peuvent aller plus loin en remarquant que la théorie des représentations permet d’illustrer l’importance de $GL_n(\C)$ et de son sous-groupe unitaire.

\section*{Métaplan}

\begin{footnotesize}
	\emph{Cadre}: $K$ est un corps et $E$ un $K$-espace vectoriel de dimension finie $n$.
\end{footnotesize}

\begin{description}
	\item[I.] \textbf{Le groupe linéaire \cite[p.95]{Perrin}}	
	\begin{small}
		\textblue{Le groupe linéaire est le groupe formé des applications linéaires inversibles dont l'outil matriciel permet son étude.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Groupes $GL(E)$ et $SL(E)$}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.46\textwidth}
					\item \emph{Définition}: groupe linéaire
					\item \emph{Proposition} \cite[p.115]{Gourdon-algebre}: caractérisation de $GL(E)$
					\item \emph{Proposition}: morphisme déterminant
					\item \emph{Remarque}: $SL(E) \simeq SL_n(K)$ \textblue{(interprétation)}
				\end{minipage} \hfill
				\begin{minipage}{.46\textwidth}
					\item \emph{Remarque}: $GL(E) \simeq GL_n(K)$ \textblue{(outil matriciel)}
					\item \emph{Proposition} \cite[p.208]{Nourdin} : $GL_n(E) \simeq GL_m(E)$
					\item \emph{Définition}: groupe linéaire spécial
					\item \emph{Remarque}: $SL(E)$ distingué dans $GL(E)$
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Système de générateurs pour ces groupes}
		\begin{small}
			\textblue{On cherche les générateurs les plus simples: ici ils sont donnés par des hyperplans. La méthode du pivot de Gauss est soit une application soit un élément pour exhiber les générateurs.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\noindent\begin{minipage}{.46\textwidth}
					\item \emph{Définition-Proposition}: Dilatation
					\item \emph{Définition-Proposition}: Transvection
					\item \emph{Théorème}: Générateur de $SL(E)$
					\item \emph{Application}: Pivot de Gauss
				\end{minipage} \hfill
				\begin{minipage}{.46\textwidth}
					\item \emph{Remarque}: Définition des réflexions
					\item \emph{Proposition}: Conjugaison des transvections
					\item \emph{Corollaire}: Générateur de $GL(E)$ 
					\item \emph{Application}: Calcul de l'inverse
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Exemple de sous-groupes de $GL(E)$ et de $SL(E)$}
		\begin{small}
			\textblue{Deux exemples de sous-groupes: les centres et les sous-groupes finis.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.46\textwidth}
					\item \emph{Théorème}: Centre
					\item \emph{Remarque}: Groupe projectif linéaire
				\end{minipage} \hfill
				\begin{minipage}{.46\textwidth}
					\item \emph{Application}: La non-simplicité des groupes
					\item \emph{Définition} \cite[p.207]{Cognet}: Matrice de permutation
				\end{minipage}
				
				\begin{minipage}{.54\textwidth}
					\item \emph{Remarque} \cite[p.207]{Cognet}: lien entre déterminant et signature
					\item \emph{Corollaire}: Un groupe fini $G$ se plonge dans $GL_n(K)$
				\end{minipage} \hfill
				\begin{minipage}{.28\textwidth}
					\item \emph{Théorème}: Cayley
					\item \emph{Théorème} \cite[p.185]{FrancinouGianellaNicolas-al2} : Burnside
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[II.] \textbf{Actions de $GL(E)$}
	\begin{small}
		\textblue{$GL(E)$ agit sur de nombreux espaces : les matrices, les groupes; avec plusieurs actions classiques}
	\end{small}
	\begin{description}
		\item[A.] \emph{Actions naturelles}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.42\textwidth}
					\item \emph{Définition}: Action transitivement à gauche
					\item \emph{Définition}: Action par conjugaison
					\item \emph{Définition}: Classe de similitude
				\end{minipage}\hfill
				\begin{minipage}{.42\textwidth}
					\item \emph{Remarque}: Changement de base pour un vecteur
					\item \emph{Remarque}: Changement de base
					\item \emph{Définition}: Invariant
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Action par équivalence \cite[p.5]{Caldero-Germoni}}
		\begin{small}
			\textblue{Pour toute application $\varphi$ on peut lui associé une représentation matricielle dans une base adéquate. C'est ce que fait cette action (elle nous permet de reformuler le théorème du rang)}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \emph{Cadre}: $G = GL_n(K) \times GL_n(K)$ où $K$ est un corps
				
				\begin{minipage}{.46\textwidth}
					\item \emph{Définitions}: Action; orbite et stabilisateur
				\end{minipage}\hfill
				\begin{minipage}{.46\textwidth}
					\item \emph{Exemple}: $I_{m,n,r}$
				\end{minipage}
				\item \emph{Théorème}: Caractérisation des orbites avec le rang \textblue{(reformuler le théorème du rang)}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Action par congruence \cite[p.250]{Caldero-Germoni}}
		\begin{small}
			\textblue{L'action par congruence permet de donner une classification de deux matrices congruentes afin de classer les formes quadratiques.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \emph{Définition}: Groupe agissant par congruence
				\item \emph{Théorème}: Caractérisation des orbites selon le corps
				
				\begin{minipage}{.46\textwidth}
					\item \emph{Exemples}: $I_n$ et le groupe orthogonal
				\end{minipage}\hfill
				\begin{minipage}{.46\textwidth}
					\item \emph{Application}: Loi de réciprocité quadratique 
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[III.] \textbf{Le rôle du corps de base}
	\begin{small}
		\textblue{Le corps de base joue un rôle essentiel dans certaines propriétés du groupe linéaire. Nous allons en étudier certaine.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Groupe linéaire fini: corps finis \cite[p.60]{Caldero-Germoni-N2}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.35\textwidth}
					\item \emph{Proposition}: Cardinal de $GL_n(\F_q)$
				\end{minipage} \hfill
				\begin{minipage}{.52\textwidth}
					\item \emph{Théorème}: Nombre de matrice diagonalisable sur $GL_n(\F_q)$ 
				\end{minipage}
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Groupe spécial projectif
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \textred{\emph{Théorème}: Cardinal du cône nilpotent}
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Groupe linéaire réel et complexe}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.46\textwidth}
					\item \emph{Cadre}: $K = \R$ ou $\C$
					\item \emph{Application}: $\chi_{AB} = \chi_{BA}$
					\item \emph{Proposition}: $GL_n(\C)$ est connexe par arcs
				\end{minipage} \hfill
				\begin{minipage}{.46\textwidth}
					\item \emph{Proposition}: $GL_n(K)$ est dense dans $\mathcal{M}_n(K)$
					\item \emph{Application}: Différentielle du déterminant
					\item \emph{Remarque}:  $GL_n(\R)$ ne l'est pas
				\end{minipage}
				\item \emph{Application}: ensemble des projecteurs de rang $p$ est connexe
				\item \emph{Proposition}: $SL(\C)$ et $SL(\R)$ sont connexes par arcs
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[IV.] \textbf{Groupe orthogonal}
	\begin{small}
		\textblue{Le groupe orthogonal est un sous-groupe particulier de $GL(E)$. On peut le voir et l'utiliser avec de nombreux points de vus : forme quadratique, action de groupe, orthogonalité, ...}
	\end{small}
	\begin{description}
		\item[A.] \emph{Groupe orthogonal et groupe orthogonal spécial \cite[p.141]{Perrin}}
		\begin{small}
			\textblue{Le groupe orthogonal peut également être vu comme le groupe du stabilisateur de $I_n$ par l'action de congruence sur $GL(E)$}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.46\textwidth}
					\item \emph{Définition} \cite[p.259]{Caldero-Germoni}: Groupe orthogonal
					\item \emph{Proposition}: Dimension: $\frac{n(n-1)}{2}$
					\item \emph{Définition}: Groupe orthogonal spécial
				\end{minipage} \hfill
				\begin{minipage}{.46\textwidth}
					\item \emph{Remarque}: Sous-groupe de $GL(E)$.
					\item \emph{Proposition}: Morphisme déterminant
					\item \emph{Application} \cite[p.259]{Caldero-Germoni}: Angle du plan
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Décomposition polaire \cite[p.347]{Caldero-Germoni}}
		\begin{small}
			\textblue{Le principe diviser pour régner appairait aussi dans les mathématiques: les décompositions de Dunford ou polaire en sont des exemples intéressants : elle permet de régner sur les groupes classiques.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \emph{Théorème}: Compacité de $O_n(\R)$ et $SO_n(\R)$ dans $\mathcal{M}_n(\R)$
				\item \emph{Application}: décomposition polaire est un homéomorphisme
				\item \emph{Corollaire}: spectre d'une matrice inversible
				\item \emph{Application}: Sous-groupe compact de $GL_n(\R)$ contenant $O_n(\R)$ est $O_n(\R)$
				\item \emph{Proposition}: Sous-groupe fini de $GL_n(\R)$ conjugué à $O_n(\R)$
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Étude du groupe $O_{p,q}$ \cite[p210]{Caldero-Germoni}}
		\begin{small}
			\textblue{Le groupe orthogonal peut également être vu comme le groupe du stabilisateur de $I_(p,q)$ par l'action de congruence sur $GL(E)$}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: $O_{p,q}$
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \textred{\emph{Théorème}: Caractérisation de $O_{p,q}$}
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
\end{description}