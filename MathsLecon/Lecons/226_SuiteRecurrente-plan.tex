\chapter*{Leçon 226: Suites vectorielles et réelles définies par une relation de récurrence $u_n = f(u_{n - 1})$. Exemples. Applications à la résolution approchée d’équations.}
\addcontentsline{toc}{chapter}{Leçon 226: Suites vectorielles et réelles définies par une relation de récurrence $u_n = f(u_{n - 1})$. Exemples. Applications à la résolution approchée d’équations.}

\titlebox{blue}{Références pour la leçon}{	
	\textblue{\cite{AllaireKaber}} Allaire et Kaber, \emph{Algèbre linéaire numérique.}
	
	\textblue{\cite{Demailly}} Demailly, \emph{Analyse numérique et équations différentielles.}
	
	\textblue{\cite{Gourdon-analyse}} Gourdon, \emph{Analyse.}
	
	\textblue{\cite{Rouviere}} Rouvière, \emph{Petit guide du calcul différentiel à l'usage de la licence et de l'agrégation.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.35\textwidth}
		Suite de polygones
	\end{minipage} \hfill
	\begin{minipage}{.5\textwidth}
		Méthode de Newton
	\end{minipage}
}

\section*{Motivation}

\subsection*{Défense}

\begin{itemize}
	\item Analyse numérique:
	\begin{itemize}
		\item recherche d'extrema;
		\item résolution de système d'équation.
	\end{itemize}
	\item Probabilité: analyse de processus stochastique.
\end{itemize}

\subsection*{Ce qu'en dit le jury}

Citer au moins un théorème de point fixe est évidemment pertinent et savoir le mettre en œuvre sur un exemple simple est indispensable. Le jury est toutefois surpris que des candidats évoquent un théorème de point fixe dans les espaces de Banach... sans être capables de définir ce qu’est un espace de Banach ou d’en donner un exemple ! On peut déjà commencer par énoncer un théorème de point fixe sur $\R$. Le jury attend d’autres exemples que la sempiternelle suite récurrente $u_{n + 1} = \sin(u_n)$ (dont il est souhaitable de savoir expliquer les techniques sous-jacentes et le jury ne se privera pas de vérifier ce point sur un exercice). La notion de points attractifs ou répulsifs peut illustrer cette leçon.

L’étude des suites linéaires récurrentes d’ordre $p$ est souvent mal connue, notamment le lien avec l’aspect vectoriel, d’ailleurs ce dernier point est trop souvent négligé. Le comportement des suites vectorielles définies par une relation linéaire $X_{n+1} = AX_n$ fournit pourtant un matériel d’étude conséquent.

La formulation de cette leçon invite résolument à évoquer les problématiques de convergence d’algorithmes (notamment savoir estimer la vitesse) d’approximation de solutions de problèmes linéaires et non linéaires : dichotomie, méthode de Newton (avec sa généralisation au moins dans $R^2$), algorithme du gradient, méthode de la puissance, méthodes itératives de résolution de systèmes linéaires, schéma d’Euler,...

\section*{Métaplan}

\begin{footnotesize}
	\noindent\cadre : $E$ un espace vectoriel, $f$ une fonction telle que $f(I) \subseteq I$ pour $I$ un intervalle de $\R$.
\end{footnotesize}

\begin{description}
	\item[I.] \textbf{Itération d'une fonction}
	\begin{footnotesize}
		\begin{itemize}[label=$\rightsquigarrow$]
			\begin{minipage}{.35\textwidth}
				\item \definiton: Suite définie par $f$
			\end{minipage} \hfill
			\begin{minipage}{.45\textwidth}
				\item \emph{Idée}: La régularité de la fonction va nous être utile
			\end{minipage}
		\end{itemize}
	\end{footnotesize}
	\begin{description}
		\item[A.] \emph{Cas de $f$ continue \cite[p.258]{Rouviere}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.36\textwidth}
					\item \definiton: Point fixe d'une fonction
				\end{minipage} \hfill
				\begin{minipage}{.5\textwidth}
					\item \proposition: Lien continuité, convergence et points fixes
				\end{minipage}
				\begin{minipage}{.44\textwidth}
					\item \cexemple: Continuité et convergence
					\item \thm: Caractérisation de la convergence
				\end{minipage} \hfill
				\begin{minipage}{.42\textwidth}
					\item \consequence: Caractérisation de la convergence
					\item \exemple~\cite[p.192]{Gourdon-analyse}: Suite homographique
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Cas de $f$ monotones \cite[p.257]{Gourdon-analyse}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.5\textwidth}
					\item \proposition: Caractérisation de la monotonie de $u_n$
					\item \cexemple: Décroissance et non bornée
				\end{minipage} \hfill
				\begin{minipage}{.44\textwidth}
					\item \thm: Caractérisation des points fixes
					\item \exemple: Suite arithmético-géométrique
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Application à la dichotomie}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.4\textwidth}
					\item \thm: Valeurs intermédiaires
				\end{minipage} 
				\begin{minipage}{.4\textwidth}
					\item \algo: Dichotomie
				\end{minipage}
				\item \remarque: Convergence
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[II.] \textbf{Récurrences linéaires}
	\begin{description}
		\item[A.] \emph{Suite récurrente linéaire à coefficients constants \cite{Gourdon-analyse}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \definiton: Suite récurrente d'ordre $p$ homogène
				
				\begin{minipage}{.5\textwidth}
					\item \remarque: Système matriciel (écriture)
					\item \methode: Résolution via l'équation caractéristique
				\end{minipage} \hfill
				\begin{minipage}{.3\textwidth}
					\item \textred{\exemple: Suite de polynômes}
					\item \exemple: Fibonacci
				\end{minipage} 
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Résolution numérique de système par itérations \cite{AllaireKaber}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.4\textwidth}
					\item \emph{Problème}; Résolution de système
					\item \methode: Jacobi
					\item \methode: Relaxation
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \thm: Convergence de la méthode
					\item \methode: Gauss--Seidel
					\item \thm: Convergence
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[III.] \textbf{Méthode de points fixes}
	\begin{description}
		\item[A.] \emph{Théorème de point fixe}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.45\textwidth}
					\item \thm~\cite[p.101]{Demailly}: Théorème de points fixe
					\item \cexemple: $f(I) \not\subset I$
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \cexemple: $I$ non compact
					\item \cexemple: $f$ non contractante
				\end{minipage}
				\item \application~\cite{Rouviere}: Théorème de Cauchy--Lipschitz
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Classification des points fixes \cite[p.103]{Demailly}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.4\textwidth}
					\item \thm: Classification
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \remarque: Le cas $\left|\varphi'(a)\right| = 1$
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Application à la méthode de Newton \cite{Rouviere}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.4\textwidth}
					\item \textred{\thm: Méthode de Newton}
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \remarque: Extension
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
\end{description}

