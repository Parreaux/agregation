\chapter*{Leçon 224: Exemples de développements asymptotiques de suites et de fonctions.}
\addcontentsline{toc}{chapter}{Leçon 224: Exemples de développements asymptotiques de suites et de fonctions.}

\titlebox{blue}{Références pour la leçon}{
	\textblue{\cite{ArnaudiesFraysse-2}} Arnaudies et Fraysse, \emph{Analyse, tome 1.}
	
	\textblue{\cite{Gourdon-analyse}} Gourdon, \emph{Analyse.}
	
	\textblue{\cite{ZuilyQueffelec2}} Quffelec et Zuily, \emph{Éléments d'analyse pour l'agrégation.}
	
	\textblue{\cite{Rouviere}} Rouvière, \emph{Petit guide de calcul différentiel à l'usage de la licence et de l'agrégation.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.4\textwidth}
		Nombre de zéros d'une équation
	\end{minipage} \hfill
	\begin{minipage}{.45\textwidth}
		Méthode de Newton
	\end{minipage}
}


\section*{Motivation}

\subsection*{Défense}

\begin{itemize}
	\item Utile en physique : calcul du potentiel d'un dipôle loin des sources.
	\item Étude de voisinage de singularité de certaines fonction.
	\item Vitesse de convergence de méthodes numérique.
	\item Simplification d'étude complexe en se ramenant à l'étude de fonctions plus simples
\end{itemize}

\subsection*{Ce qu'en dit le jury}

Cette leçon doit permettre aux candidats d’exprimer leur savoir-faire sur les techniques d’analyse élémentaire que ce soit sur les suites, les séries ou les intégrales. On peut par exemple établir un développement asymptotique à quelques termes des sommes partielles de la série harmonique, ou bien la formule de Stirling que ce soit dans sa version factorielle ou pour la fonction $\Gamma$. On peut également s’intéresser aux comportements autour des singularités de fonctions spéciales célèbres. Du côté de l’intégration, on peut évaluer la vitesse de divergence de l’intégrale de la valeur absolue du sinus
cardinal, avec des applications pour les séries de Fourier, voire présenter la méthode de Laplace.

Par ailleurs, le thème de la leçon permet l’étude de suites récurrentes (autres que le poncif $u_{n+1} = \sin u_n)$, plus généralement de suites ou de fonctions définies implicitement, ou encore des études asymptotiques de solutions d’équations différentielles (sans résolution explicite).

On peut aller plus loin en abordant des techniques de phase stationnaire et en en discutant des applications.

\section*{Métaplan}

\begin{footnotesize}
	\noindent\emph{Cadre}: Soit $f$ une fonction définie sur un intervalle $I$ différent d’un point à valeurs dans $\R$, $a \in \overline{I}$ et $n \in \N$.
\end{footnotesize}

\begin{description}
	\item[I.] \textbf{Définition d'un développement asymptotique \cite[p.86]{Gourdon-analyse}}
	\begin{description}
		\item[A.] \emph{Relations de comparaison}
		\begin{small}
			\textblue{Cette partie constitue en l'études notations de Landau: leurs définitions, et leurs propriétés. Ces notations vont nous servir tout au long de la leçon.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.45\textwidth}
					\item \definiton: Notation de Landau (fonction et suite)
				\end{minipage} \hfill
				\begin{minipage}{.36\textwidth}
					\item \proposition: Comportement des ces objets
				\end{minipage}
				\item \proposition: $o \Rightarrow O$ et $\sim \Rightarrow O$ dans les deux sens
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Développement asymptotique}
		\begin{small}
			\textblue{Les échelle de comparaison permettent de réduire des fonctions compliquées à des combinaisons linéaires de fonctions modèles plus simples.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Échelle de comparaison
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \definiton: Développement asymptotique
				\end{minipage}
				\begin{minipage}{.3\textwidth}
					\item \proposition: Unicité
				\end{minipage} \hfill
				\begin{minipage}{.55\textwidth}
					\item \application: Formule sommatoire de Poisson et fonction $\Theta$
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Un cas particulier: développement limité}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \definiton: Développement limité en $0$ à l'ordre $k$
				
				\begin{minipage}{.45\textwidth}
					\item \remarque: On peut toujours se ramener en $0$
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \thm: Taylor--Young
				\end{minipage}
				\item \cexemple: Développable en $0$ mais pas $2$ fois dérivables: $x^2 \sin\left(\frac{1}{x}\right)$
				
				\begin{minipage}{.5\textwidth}
					\item \proposition: Réciproque pour fonction continue
					\item \proposition: Somme, multiplication, composée
				\end{minipage} \hfill
				\begin{minipage}{.35\textwidth}
					\item \application: Théorème central limite
					\item \proposition: Dérivation
				\end{minipage}
				\item \application: Calcul de limite
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[II.] \textbf{Développement asymptotique et intégration}
	\begin{description}
		\item[A.] \emph{Intégration d'un développement limité \cite[p.88]{Gourdon-analyse}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \proposition: Intégration d'un développement limité
				\item \application: Calcul du DL(0) de $\ln(1 + x)$ et $\arctan(x)$
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Intégration des relations de comparaison \cite[p.159]{Gourdon-analyse}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.5\textwidth}
					\item \proposition: Intégration des relations par comparaison
				\end{minipage} \hfill
				\begin{minipage}{.3\textwidth}
					\item \exemple: $\ln x = O(x^{\alpha})$ 
				\end{minipage}
				\item \application: Développement asymptotique du log intégrable
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Étude qualitative d'équation différentielle: le nombre de zéros d'une solution \cite{ZuilyQueffelec2}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \textred{\proposition: Nombre de zéros d'une solution différentielle}	
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[III.] \textbf{Développement asymptotique de suites et séries}
	\begin{description}
		\item[A.] \emph{Suites récurrentes}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \thm: Convergence de Cesàro
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \application: Convergence lente
				\end{minipage}
				\item \textred{\application~\cite{Rouviere}: Newton}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Sommation des relations de comparaisons \cite[p.202]{Gourdon-analyse}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.5\textwidth}
					\item \proposition: Sommation des relations par comparaison 
					\item \application: $H_n \underset{n \to \infty}{\sim} \ln n$
				\end{minipage} \hfill
				\begin{minipage}{.3\textwidth}
					\item \cexemple: Positivité
					\item \application: Master théorème
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Comparaison série/intégrale \cite[p.282]{Gourdon-analyse}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \proposition: Comparaison série/intégrale
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \application: Série de Bertrand
				\end{minipage}
				\item \application: Équivalence des sommes de Riemann
			\end{itemize}
		\end{footnotesize}
	\end{description}
\end{description}
