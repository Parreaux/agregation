\documentclass[a4paper,8pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais,english]{babel}%

\usepackage[dvipsnames]{xcolor}

\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{enumitem}
\usepackage{appendix}
\usepackage{stmaryrd}
\usepackage{amsmath}
\usepackage{wasysym}

\usepackage{listings}
\usepackage{multirow}
\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}
\algnewcommand{\LineComment}[1]{\State \(\triangleright\) #1}

\parskip=0\baselineskip %0.5 d'habitude


\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\F}{\mathbb{F}}
	\newcommand{\Sn}{\mathfrak{S}}
	\newcommand{\A}{\mathfrak{A}}
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textbluesmall}[1]{{\small{\textcolor{blue}{#1}}}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\sloppy
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\theoremstyle{plain}
	\newtheorem{theo}{Théorème}
	\newtheorem{prop}{Proposition}
	\newtheorem{cor}{Corollaire}
	\newtheorem{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem{definition}{Définition}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	
	\title{Leçon 156 : Exponentielle de matrices. Applications.}
	
	%\author{Julie Parreaux}
	\date{}
	\maketitle
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
	
	\section*{Motivation}
	
	\subsection*{Défense}
	
	\begin{itemize}
		\item Résolutions d'équations différentielles linéaires à coefficients constants
		\item Résolutions d'équations différentielles non linéaires
		\item Théorie des groupes de Lie : pour comprendre un groupe de matrice, il suffit de le comprendre au voisinage de l'identité $I_n$. Pour cela, on veut regarder l'espace tangent (qui est un espace vectoriel). On obtient alors un champs de vecteurs qui donne une solution différentielle de $e^{tA}$ où $A$ est le vecteur tangent (ici c'est bien une matrice).
	\end{itemize}
	
	\subsection*{Ce qu'en dit le jury}
	
	Bien que ce ne soit pas une leçon d’analyse, il faut toutefois pouvoir justifier clairement la convergence de la série exponentielle. La distinction entre le cas réel et complexe doit être clairement évoqué.
	
	Les questions de surjectivité ou d’injectivité doivent être abordées. Par exemple la matrice $A = \begin{pmatrix} 1 & 1 \\ 0 & 1 \end{pmatrix}$ est-elle l’exponentielle d’une matrice à coefficients réels ? La matrice définie par blocs $B = \begin{pmatrix} A & 0 \\ 0 & A \\ \end{pmatrix}$ est-elle l’exponentielle d’une matrice à coefficients réels ?
	
	La décomposition de Dunford multiplicative (décomposition de Jordan) de $_exp(A)$ trouve toute son utilité dans cette leçon. Notons que l’exponentielle fait bon ménage avec la décomposition polaire dans bon nombre de problèmes sur les sous-groupes du groupe linéaire. L’étude du logarithme (quand il est défini) trouve toute sa place dans cette leçon. Si l’on traite du cas des matrices nilpotentes, on pourra évoquer le calcul sur les développements limités.
	
	Il est bon de connaître l’image par exponentielle de certains sous-ensembles de matrices (ensemble des matrices symétriques, hermitiennes, ou antisymétriques).
	
	Les applications aux équations différentielles méritent d’être présentées sans toutefois constituer l’essentiel de la leçon. On pourra par exemple faire le lien entre réduction et comportement asymptotique, mais le jury déconseille aux candidats de proposer ce thème dans un développement de cette leçon, sauf à avoir bien compris comment les apports algébriques permettent ici de simplifier les conclusions analytiques.
	
	S’ils le désirent, les candidats peuvent s’aventurer vers les sous-groupes à un paramètre du groupe linéaire (on peut alors voir si ces sous-groupes constituent des sous-variétés fermées de $GL_n(\R)$) ou vers les algèbres de Lie.
	
	\section*{Métaplan}
	
	\begin{footnotesize}
		\noindent\emph{Cadre}: $K = \R$ ou $\C$ \textblue{(ou tout corps de caractéristique nulle et complet pour une distance)}; $E$ un $K$-ev de dimension finie $n$.
	\end{footnotesize}
	
	\begin{description}
		\item[I.] \textbf{Exponentielle de matrice et propriétés algébriques \cite[p.57]{MneimneTestard}}
		
		\begin{description}
			\item[A.] \emph{Exponentielle de matrice}
			
			\begin{small}
				\textblue{L'exponentielle de matrice est définie comme la limite d'une série absolument convergente. Elle possède de nombreuses propriétés algébrique (conjugaison, transposée, inversible). L'exponentielle d'une matrice est un polynôme d'un endomorphisme. Cependant, on a pas un polynôme unique pour toute matrice (sinon, une de ses dérivées successives d'annulerait). }
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.36\textwidth}
						\item \emph{Définition}: Exponentielle de matrice
						\item \emph{Remarque} \cite{Gourdon-algebre}: Borne sur la norme
						\item \emph{Proposition}: Exponentielle et conjugaison
						\item \emph{Application}: Matrice semblable
						\item \emph{Proposition}: Exponentielle et trace
						\item \emph{Proposition}: Exponentielle et transposée
						\item \emph{Application}: Matrice symétrique
					\end{minipage} \hfill
					\begin{minipage}{.5\textwidth}
						\item \emph{Proposition}: $\det(\exp A) = \exp (\mathrm{Tr}(A))$
						\item \emph{Corollaire}: $\exp(A) \in GL_n(\C)$
						\item \emph{Proposition}: Commutativité implique $\exp(A + B) = \exp(A)\exp(B)$
						\item \emph{Contre-exemples}: dans le cas non commutatif: égalité ou non
						\item \emph{Proposition}: $\exp(A) \in K[A]$
						\item \emph{Application}: Commutativité des exponentielle
						\item \emph{Application}: Calcul de l'inverse
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[B.] \emph{Calcul d'une exponentielle de matrice}
			
			\begin{small}
				\textblue{Le calcul d'une exponentielle de matrice est difficile : nous devons calculer des puissances successives de matrices et en déterminer leur limites. Dans des cas particulier (matrices diagonales ou nilpotentes) ce calcul devient bien plus facile. }
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \emph{Exemple}: Matrice diagonale
						\item \emph{Exemple}: Matrice nilpotente
						\item \emph{Théorème} \cite[p.63 et p.176]{RieslerBoyer}: Décomposition de Dunford
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \emph{Application}: Calcul d'une exponentielle
						\item \emph{Application}: Critère de diagonale de l'exponentielle
						\item \emph{Méthode}: Calcul via les polynômes
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
		\end{description}
		\item[II.] \textbf{Propriétés de la fonction exponentielle \cite[p.57]{MneimneTestard}}
		
		\begin{small}
			\textblue{L'exponentielle est une fonction sur l'espace des matrices. Nous allons en étudier les propriétés : sa régularité, son inverse si elle est définie, ses images et son injectivité.}
		\end{small}
		\begin{description}
			\item[A.] \emph{Régularité}
			
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \emph{Proposition}: Caractère $\mathcal{C}^{\infty}$
						\item \emph{Proposition}: Différentielle en $0$
						\item \emph{Corollaire}: $\mathcal{C}^{1}$ difféomorphisme
						\item \emph{Application}: Sous-groupe arbitrairement petit
						
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \emph{Définition}: Logarithme
						\item \emph{Proposition}: Propriétés du logarithme
						\item \emph{Proposition}: $\exp : \mathcal{N} \to \mathcal{U}$
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[B.] \emph{Injectivité et surjectivité de cette application}
			
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.46\textwidth}
						\item \emph{Remarque}: La non-injectivité de l'exponentielle
						\item \emph{Proposition}: $\exp : \mathcal{D}_n(K) \to GL_n(K)$ est injective
						\item \emph{Théorème} \cite[p.47]{Zavidovique}: Surjectivité de l'exponentielle \textred{DEV}
						\item \emph{Contre-exemple}: Non surjectivité dans $GL_n(\R)$
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \emph{Proposition}: $\exp : \mathcal{A}_n(\R) \to SO_n(\R)$ est surjective
						\item \emph{Théorème}: Homéomorphisme $S_n(\R) \to S_n^{++}(\R)$
						\item \emph{Application} \cite[p.210]{Caldero-Germoni}: Étude $O(p, q)$ \textred{DEV}
						\item \emph{Corollaire} \cite[p.210]{Caldero-Germoni}: Compacité de $O(p, q)$
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
		\end{description}
		\item[III.] \textbf{Équation différentielle}
		
		\begin{small}
			\textblue{Une application principal de l'exponentielle de matrice est la résolution des équations différentielles. Nous allons voir quels sont les équations qui sont résolues avec l'exponentielle de matrice. Nous avons également quelques résultats comme le théorème de Liapinov qui nous permet d'étendre les équations que nous pouvons résoudre grâce à ce procédé.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.46\textwidth}
					\item \emph{Théorème} \cite[p.199]{Demailly}: Existence des solutions pour $Y' = AY + B(t)$
					\item \emph{Corollaire} \cite[p.199]{Demailly}: Expression de la solution
					\item \emph{Définition} \cite[p.380]{ZuilyQueffelec2}: Stabilité d'un système différentiel autonome
				\end{minipage} \hfill
				\begin{minipage}{.36\textwidth}
					\item \emph{Théorème} \cite[p.380]{ZuilyQueffelec2}: Stabilité d'une telle solution
					\item \emph{Théorème} \cite[p.132]{Rouviere}: Liapunov
					\item \emph{Application} \cite[p.132]{Rouviere}: Équation du pendule
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	
	\section*{Quelques notions importantes}
	
	\subsection*{Exponentielle de matrices}
	\input{./../Notions/ExponentielleMatrice.tex}
	
	\subsection*{Solution d'une équation différentielle linéaire}
	\input{./../Notions/EquaDiff_LineaireSol.tex}

	
\end{document}