\chapter*{Leçon 229: Fonctions monotones. Fonctions convexes.}
\addcontentsline{toc}{chapter}{Leçon 229: Fonctions monotones. Fonctions convexes.}

\titlebox{blue}{Références pour la leçon}{	
	\textblue{\cite{Gourdon-analyse}} Gourdon, \emph{Analyse.}
	
	\textblue{\cite{Hauchecorne}} Hauchecorne, \emph{Les contre-exemples en mathématiques.}
	
	\textblue{\cite{Nourdin}} Nourdin, \emph{Agrégation de mathématiques épreuve orale.}
	
	\textblue{\cite{RamisWarusfel}} Ramis et Warusfel, \emph{Cours de mathématiques pures et appliquées.}
	
	\textblue{\cite{Rouviere}} Rouvière, \emph{Petit guide du calcul différentiel à l'usage de la licence et de l'agrégation.}
	
	\textblue{\cite{Rudin}} Rudin, \emph{Principes d'analyse mathématiques.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.35\textwidth}
		Caractérisation de $\Gamma$
	\end{minipage} \hfill
	\begin{minipage}{.5\textwidth}
		Méthode du gradient à pas optimal
	\end{minipage}
}

\section*{Motivation}

\subsection*{Défense}

\begin{itemize}
	\item La monotonie décrit les variations d'une fonctions (extremum local d'une fonction)
	\item La convexité des fonctions (extremum global)
\end{itemize}

\subsection*{Ce qu'en dit le jury}

L’énoncé et la connaissance de la preuve de l’existence de limites à gauche et à droite pour les fonctions monotones sont attendues. Ainsi on doit parler des propriétés de continuité et de dérivabilité à gauche et à droite des fonctions convexes de la variable réelle. Il est souhaitable d’illustrer la présentation de la convexité par des dessins clairs. On notera que la monotonie concerne les fonctions réelles d’une seule variable réelle, mais que la convexité concerne également les fonctions définies sur une partie convexe de $\R^n$ , qui fournissent de beaux exemples d’utilisation. L’étude de la fonctionnelle quadratique ou la minimisation de $\mathopen{||}Ax - b\mathclose{||}^2$ pourront illustrer agréablement cette leçon. 

Pour aller plus loin, la dérivabilité presque partout des fonctions monotones est un résultat remarquable (dont la preuve peut être éventuellement admise). L’espace vectoriel engendré par les fonctions
monotones (les fonctions à variation bornée) relève de cette leçon. Enfin, la dérivation au sens des distributions fournit les caractérisations les plus générales de la monotonie et de la convexité ; les candidats maîtrisant ces notions peuvent s’aventurer utilement dans cette direction.

\section*{Métaplan}

\begin{footnotesize}
	\noindent\emph{Cadre}: $I$ intervalle de $\R$
\end{footnotesize}

\begin{description}
	\item[I.] \textbf{Fonctions monotones}
	\begin{description}
		\item[A.] \emph{Définitions}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.46\textwidth}
					\item \definiton: Fonction croissante, décroissante
					\item \remarque: Monotonie de $f$ et $-f$
					\item \application~\cite[p.257]{Rouviere}: Monotonie d'une suite
				\end{minipage} \hfill
				\begin{minipage}{.45\textwidth}
					\item \definiton: Fonction monotone
					\item \remarque: Stricte croissance / décroissance
					\item \exemple : Fonction caractéristique
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Régularité des fonctions monotones}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \proposition: Limite à gauche et à droite en tout points
				\item \cexemple~\cite{Hauchecorne}: stricte croissante et discontinuité
				
				\begin{minipage}{.36\textwidth}
					\item \proposition: Bijection et $f^{-1}$
				\end{minipage} \hfill
				\begin{minipage}{.44\textwidth}
					\item \thm~(ADMIS): Dérivable presque partout
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Étude des variations}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.46\textwidth}
					\item \proposition : Caractérisation de la croissance
				\end{minipage} \hfill
				\begin{minipage}{.35\textwidth}
					\item \cexemple: $x \mapsto x^3$
				\end{minipage}
				
				\item \remarque : Généralisation de la stricte croissance	
			\end{itemize}
		\end{footnotesize}
		\item[D.] \emph{Suites et séries de fonctions \cite[p.8]{Nourdin}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \lem : Dini
					\item \thm: Comparaison série-intégrale
				\end{minipage} \hfill
				\begin{minipage}{.46\textwidth}
					\item \application: Théorème de Glivenko--Ganteli
					\item \application~\cite[p.204]{Gourdon-analyse}: Séries de Bertrand
				\end{minipage}	
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[II.] \textbf{Fonctions convexes}
	\begin{description}
		\item[A.] \emph{Définitions}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Fonction convexe (stricte)
				\end{minipage} \hfill
				\begin{minipage}{.45\textwidth}
					\item \exemple: Norme convexe mais pas stricte linéaire
				\end{minipage}
				\begin{minipage}{.34\textwidth}
					\item \definiton: Fonction concave
				\end{minipage} \hfill
				\begin{minipage}{.6\textwidth}
					\item \proposition: Comportements limites et combinaison linéaire
				\end{minipage}
				\item \proposition: Fonction affine convexe (concave)
				
				\begin{minipage}{.46\textwidth}
					\item \thm : Caractérisation par épigraphe
				\end{minipage} \hfill
				\begin{minipage}{.46\textwidth}
					\item \remarque: Représentation graphique
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Caractérisation}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.5\textwidth}
					\item \lem: 3 pentes
					\item \textred{\application~\cite{Rudin}: Caractérisation de Gamma}
					\item \proposition: $f$ est dérivable
					\item \proposition: $f$ est $2$ fois dérivable
					\item \cexemple: $x \mapsto x^4$
				\end{minipage} \hfill
				\begin{minipage}{.44\textwidth}
					\item \application: Bornée $\Rightarrow$ constante
					\item \proposition: Régularité
					\item \remarque: Généralisation différentiable
					\item \remarque: Généralisation différentiable
					\item \exemple: Convexité de $exp$
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Inégalités \cite{Rouviere}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \proposition: Inégalité de la moyenne
					\item \proposition: Inégalité de Hölder
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \proposition: Inégalité de Young
					\item \application: Inégalité de Minkowski
				\end{minipage}
				\item Inégalité de Jensen (caractérisation via affine et variance)
			\end{itemize}
		\end{footnotesize}
		\item[D.] \emph{Optimisation}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.5\textwidth}
					\item \proposition: Unicité d'extremum locaux - globaux
					\item \definiton: Fonction coersive (apport de la convexité)
				\end{minipage} \hfill
				\begin{minipage}{.34\textwidth}
					\item \proposition: Méthode de Newton
					\item \textred{\proposition \cite{RamisWarusfel}: Gradient à pas optimal}
				\end{minipage}
				\item Inégalité de Jensen (caractérisation via affine et variance)
			\end{itemize}
		\end{footnotesize}
	\end{description}
\end{description}

