\chapter*{Leçon 108: Exemples de parties génératrices de groupe. Applications.}
\addcontentsline{toc}{chapter}{Leçon 108: Exemples de parties génératrices de groupe. Applications.}

\titlebox{blue}{Références pour la leçon}{
	\textblue{\cite{BaillyMaitre}} Bailly-Maitre, \emph{Arithmétique et cryptologie.}
	
	\textblue{\cite{Berhuy}} Berhuy, \emph{Algèbre: le grand combat.}
	
	\textblue{\cite{Calais}} Calais, \emph{Éléments de la théorie des groupes.}
	
	\textblue{\cite{Nourdin}} Nourdin, \emph{Agrégation de mathématiques épreuve orale.}
	
	\textblue{\cite{Perrin}} Perrin, \emph{Cours d'algèbre.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.35\textwidth}
		Simplicité de $\A_n$
	\end{minipage} \hfill
	\begin{minipage}{.5\textwidth}
		Générateurs de $\textsc{SL}_{2}(\Z)$
	\end{minipage}
}
	
\section*{Motivation}

\subsection*{Défense}
\begin{itemize}
	\item Étudier et identifier la structure des groupes;
	\item Simplifier des études de morphismes (cas d'égalité);
	\item Montrer des propriétés sur les groupes;
\end{itemize}

\subsection*{Ce qu'en dit le jury}

\begin{small}
	C’est une leçon qui doit être illustrée par des exemples très variés qui peuvent être en relation avec les groupes de permutations, les groupes linéaires ou leurs sous-groupes, comme $\textsc{SL}_n(K)$, $O_n(K)$ ou $SO_n(R)$. Les groupes $\frac{\Z}{n\Z}$, fournissent aussi des exemples intéressants. La connaissance de parties génératrices s’avère très utile dans l’analyse des morphismes de groupes ou pour montrer la connexité par arcs de certains sous-groupes de $\textsc{GL}_n(\R)$ par exemple.

	Tout comme dans la leçon 106, la présentation du pivot de Gauss et de ses applications est envisageable.

	Il est important de présenter les différents systèmes de générateurs du groupe symétrique et de savoir mettre en évidence l’intérêt du choix de ces systèmes dans divers exemples. 

	Le candidat pourra également parler des générateurs du groupe diédral et, si il le souhaite, il pourra donner une présentation par générateurs et relations d’un groupe (groupe diédral, groupe symétrique, ou groupe des tresses).

	Il est également possible de parler du logarithme discret et de ces applications à la cryptographie (algorithme de Diffie-Hellman, cryptosystème de El Gamal).
\end{small}

\section*{Métaplan}

\begin{description}
	\item[I.] \textbf{Introduction \cite[p.27-29]{Calais}}
	\begin{small}
		\textblue{On rappelle la notion de parties génératrices. La taille des parties génératrices ne montre pas la complexité du dit groupe.}
	\end{small}
	\begin{footnotesize}
		\emph{Cadre}: $G$ est un groupe de type fini; $E$ est un $\mathbb{K}$-espace vectoriel de dimension finie où $\mathbb{K}$ est un corps.
	\end{footnotesize}
	\begin{footnotesize}
		\begin{itemize}[label=$\rightsquigarrow$]
			\item \emph{Définition} : Groupe engendré par une partie que l'on note $<S>$
			
			\begin{minipage}{.44\textwidth}
				\item \emph{Exemple}: $S= {x}$; $G = \Z$ et $<S> = \{x^n; n \in \Z \}$
			\end{minipage} \hfill
			\begin{minipage}{.36\textwidth}
				\item \emph{Définition}: Partie génératrice d'un groupe
			\end{minipage}
			\item \emph{Exemple}: $G = \Z$ qui est engendré par $\{1\}$.
			\item \emph{Définition}: Le rang d'un groupe $G$ est le cardinal minimal d'une partie génératrice de $G$.

			\begin{minipage}{.44\textwidth}
				\item \emph{Exemple}: Par ce qui précède, le rang de $\Z$ est $1$.
			\end{minipage} \hfill
			\begin{minipage}{.36\textwidth}
				\item \emph{Définition}: Un groupe de type fini
			\end{minipage}			
			\item \emph{Exemple} : $\Z$ est de type fini
			\item \emph{Contre-exemple}: $U = \cup_{n \in \N} U_n$ où $U_n$ est l'ensemble des racines de l'unités.
		\end{itemize}
	\end{footnotesize}
	\item[II.] \textbf{\textbf{Les groupes monogènes et cycliques \cite[p.156]{Berhuy}}}
	\begin{small}
		\textblue{Ce sont les groupes qui possède le système de générateur le plus simple : un élément. Ils sont de rang un.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Définitions et caractérisations des groupes monogènes et cycliques}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Groupe monogène
					\item \emph{Définition}: Groupe cyclique
				\end{minipage} \hfill
				\begin{minipage}{.45\textwidth}
					\item \emph{Exemple}: $\Z$ est monogène.
					\item \emph{Exemple}: $\Z/_{n\Z}$ est cyclique.
				\end{minipage}
				\item \emph{Théorème}: Caractérisation des groupes monogène.
				\item \emph{Application}: Deux groupes monogènes infinis sont isomorphes. 
				\item \emph{Application}: Deux groupes cycliques de même ordre sont isomorphes.
				
				\begin{minipage}{.54\textwidth}
					\item \emph{Proposition} Tout groupe fini d'ordre premier $p$ est cyclique
				\end{minipage} \hfill
				\begin{minipage}{.28\textwidth}
					\item \emph{Contre-exemple} à la réciproque.
				\end{minipage}
				\item \emph{Application}: Si $q = p^n$ avec $p$ premier alors $\mathbb{F}_{q}^{*}$ est cyclique.
				\item Sous-groupe d'un groupe cyclique.				
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Étude des générateurs des groupes monogènes et cycliques}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \emph{Définition}: Indicatrice d'Euler $\phi$
				\item \emph{Théorème}: Caractérisation des générateurs d'un groupe monogène
				\item \emph{Application}: Si $G$ est un groupe cyclique d'ordre $n$, alors il possède $\phi(n)$ générateurs distincts
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Problème du logarithme discret et application à la cryptographie \cite[p.217]{BaillyMaitre}}
		\begin{small}
			\textblue{Une application aux générateurs d'un groupe est la cryptographie grâce à la difficulté du problème du logarithme discret.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \emph{Définition}: le problème du logarithme discret.
				\item \emph{Remarque}: Le problème est dure si $n$ est un facteur d'un grand nombre premier.
				\item \emph{Applications} : Diffie-Helmann et cryptosystème d'elderman
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[II.] \textbf{Exemples de groupes de rang 2}
	\begin{description}
		\item[A.] \emph{Le groupe symétrique \cite[p.214]{Berhuy}}
		\begin{small}
			\textblue{Le groupe symétrique est un bon moyen de connaître les groupes finis. Donc le connaître est primordial}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Groupe symétrique
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Signature
				\end{minipage}					
				\item \emph{Théorème}: Système de générateurs du groupe symétrique.
				\item \emph{Applications}: contexte d'utilisation de ces systèmes de générateurs
				\item \emph{Théorème}: décomposition de cycle à support disjoint
				\item \emph{Application}: Le groupe des isométrie conservant le tétraèdre (le cube) est isomorphe à $\Sn$. \textblue{C'est un exemple assez trivial d'utilisation des générateurs du groupe afin de montrer la surjectivité d'un morphisme (étude de morphismes)}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Le groupe alternée \cite[p.215]{Berhuy}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.38\textwidth}
					\item \emph{Définition}: Groupe alterné
					\item \emph{Définition}: Simplicité
					\item \textred{\emph{Théorème}: $A_n$ est simple pour $n> 5$}
				\end{minipage} \hfill
				\begin{minipage}{.46\textwidth}
					\item \emph{Théorème}: Système de générateur du groupe alterné
					\item \emph{Principe}: Preuve de la simplicité
					\item \emph{Application}: Sous-groupes distingué de $\Sn_n$.
				\end{minipage}
				\item \emph{Application}: Pas de surjection entre $\Sn_n$ et $\Sn_{n+1}$
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Le groupe $\textsc{SL}_{2}(\Z)$ et l'utilisation de la géométrie}
		\begin{small}
			\textblue{On utilise la géométrie et les actions de groupes pour trouver les générateurs. On a un groupe de matrice de rang 2.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.46\textwidth}
					\item \emph{Définitions}: Droite projective et homographie
					\item \emph{Définition}: Le groupe $\textsc{SL}_{2}(\Z)$
				\end{minipage} \hfill
				\begin{minipage}{.46\textwidth}
					\item \emph{Proposition}: Propriétés des homographies
					\item \textred{\emph{Propriété}: L'ensemble de ces générateurs}
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[III.] \textbf{Les groupes de rang $r$ quelconque}
	\begin{footnotesize}
		\begin{itemize}[label=$\rightsquigarrow$]
			\item \emph{Théorème} \cite[p.207]{Nourdin}: théorème de structure pour les groupes abéliens de type fini
			\item \emph{Application}: Dans ce cas on étudie uniquement les groupes $\Z / n\Z$.
		\end{itemize}
	\end{footnotesize}
	\begin{description}
		\item[A.] \emph{Le groupe linéaire \cite[p.95]{Perrin}}
		\begin{small}
			\textblue{\textblue{On cherche les générateurs les plus simples: ici ils sont donnés par des hyperplans. La méthode du pivot de Gauss est soit une application soit un élément pour exhiber les générateurs.}}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.46\textwidth}
					\item \emph{Définition}: $\textsc{GL}(E)$, $\textsc{SL}(E)$, $\textsc{GL}_n(\mathbb{K})$, $\textsc{SL}_n(\mathbb{K})$
				\end{minipage} \hfill
				\begin{minipage}{.46\textwidth}
					\item \emph{Définitions}: Transvection et dilatation
				\end{minipage}
				\item \emph{Théorème}: $\textsc{SL}(E)$ est engendré par les transvections
				\item \emph{Théorème}: $\textsc{GL}(E)$ est engendré par les transvections et les dilatations
				
				\begin{minipage}{.4\textwidth}
					\item \emph{Application}: $\textsc{SL}(E)$ est connexe par arcs
				\end{minipage} \hfill
				\begin{minipage}{.5\textwidth}
					\item \emph{Application}: Le pivot de Gauss et sa complexité
				\end{minipage}
				\item \emph{Application}: Le calcul du centre de $\textsc{SL}(E)$ et de $\textsc{GL}(E)$
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Le groupe orthogonal \cite[p.141]{Perrin}}
		\begin{small}
			\textblue{Le groupe orthogonal est un sous-groupe de $GL(E)$ qui possède son propre système de générateurs.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: $\textsc{O}(E)$, $\textsc{SO}(E)$, $\textsc{O}_n(\mathbb{K})$, $\textsc{SO}_n(\mathbb{K})$
					\item \emph{Définition}: Réflexion
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Symétrie orthogonale
					\item \emph{Définition}: Renversement
				\end{minipage}
				\item \emph{Théorème}: $\textsc{SO}(E)$ est engendré par les renversement
				\item \emph{Théorème}: $\textsc{O}(E)$ est engendré par les symétries hyperplans
				
				\begin{minipage}{.4\textwidth}
					\item \emph{Théorème}: Cartan--Dieudonné
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Théorème}: $SO_3(\R)$ est simple
				\end{minipage}
				\item \emph{Application}: $SO_n(\R)$ est connexe par arc
			\end{itemize}
		\end{footnotesize}			
	\end{description}
\end{description}
	
