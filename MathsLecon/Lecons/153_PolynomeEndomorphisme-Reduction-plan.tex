\chapter*{Leçon 153 : Polynômes d'endomorphismes en dimension finie. Réduction d'un endomorphisme en dimension finie. Application.}
\addcontentsline{toc}{chapter}{Leçon 153 : Polynômes d'endomorphismes en dimension finie. Réduction d'un endomorphisme en dimension finie. Application.}

\titlebox{blue}{Références pour la leçon}{
	\textblue{\cite{Berhuy}} Berhuy, \emph{Algèbre: le grand combat.}
	
	\textblue{\cite{Cognet}} Cognet, \emph{Algèbre linéaire.}
	
	\textblue{\cite{Gourdon-analyse}} Gourdon, \emph{Analyse.}
	
	\textblue{\cite{Gourdon-algebre}} Gourdon, \emph{Algèbre.}
	
	\textblue{\cite{Kieffer}} Kieffer, \emph{66 leçons pour l'agrégation de mathématiques.}
	
	\textblue{\cite{Zavidovique}} Zavidovique, \emph{Un max de maths.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.35\textwidth}
		Surjectivité de l'exponentielle
	\end{minipage} \hfill
	\begin{minipage}{.5\textwidth}
		Décomposition de Dunford
	\end{minipage}
}

\section*{Motivation}

\subsection*{Défense}

La réduction d'un endomorphisme en dimension fini consiste à trouver une représentation plus simple dans une base.
\begin{itemize}
	\item Faciliter les calculs des puissances et des exponentielles (résolution d'équations différentielles et systèmes dynamiques discrets).
	\item Classer les endomorphismes à similitude près.
\end{itemize}
Les polynômes d'endomorphismes sont un outil puissant pour effectuer des réductions ou connaître des les réductions que nous pouvons calculer.

\subsection*{Ce qu'en dit le jury}

Cette leçon ne doit pas être un catalogue de résultats autour de la réduction qui est ici un moyen pour démontrer des théorèmes ; les polynômes d’endomorphismes doivent y occuper une place importante. Il faut consacrer une courte partie de la leçon à l’algèbre $K[u]$ et connaître sa dimension sans hésitation.

Il est ensuite possible de s’intéresser aux propriétés globales de cette algèbre. Les liens entre réduction d’un endomorphisme $u$ et la structure de l’algèbre $K[u]$ sont importants, tout comme ceux entre les idempotents et la décomposition en somme de sous-espaces caractéristiques. Il faut bien préciser que, dans la réduction de Dunford, les composantes sont des polynômes en l’endomorphisme, et en connaître les conséquences théoriques et pratiques.

L’aspect applications est trop souvent négligé. Il est possible, par exemple, de mener l’analyse spectrale de matrices stochastiques. On attend d’un candidat qu’il soit en mesure, pour une matrice simple de justifier la diagonalisabilité et de déterminer un polynôme annulateur (voire minimal). Il est bien sûr important de ne pas faire de confusion entre diverses notions de multiplicité pour une valeur propre $\lambda$ donnée (algébrique ou géométrique). Enfin, calculer $A^k$ ne nécessite pas, en général, de réduire $A$ (la donnée d’un polynôme annulateur de $A$ suffit souvent). Il est possible d’envisager des applications aux calculs d’exponentielles de matrices.

S’il le souhaite, le candidat pourra étudier des équations matricielles et de calcul fonctionnel, avec par exemple l’étude de l’extraction de racines ou du logarithme.

\section*{Métaplan}

\begin{description}
	\item[I.] \textbf{Polynômes d'endomorphismes \cite[p.943]{Berhuy}}
	\begin{small}
		\textblue{Les polynômes d'endomorphismes sont des outils importants pour la réductions. Cependant, ils définissent également une algèbre intéressante à étudier. On commence par celle-ci puis on étudiera plus précisément deux polynômes: le polynôme minimal et caractéristique.}
	\end{small}
	\begin{description}
		\item[A.] \emph{L'algèbre $K[u]$}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.45\textwidth}
					\item \emph{Définition}: Polynôme
					\item \emph{Proposition}: Algèbre commutative de $\mathcal{L}(E)$.
				\end{minipage} \hfill
				\begin{minipage}{.35\textwidth}
					\item \emph{Définition}: Morphisme d'évaluation
					\item \emph{Application} \cite[p.47]{Gourdon-analyse}: $K[u]$ est fermée
				\end{minipage}
				\item \emph{Application} \cite[p.47]{Zavidovique}: Définition de l'exponentielle de matrice
				\item \emph{Théorème}: Décomposition des noyaux
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Le polynôme minimal}
		
		\begin{footnotesize}
			\noindent\textbf{Définition du polynôme minimal} \textblue{Ce polynôme nous permet d'étudier plus profondément la structure d'algèbre de $K[u]$. En effet, il est le générateur de l'idéal définit par le noyaux du morphisme d'évaluation. On commence, également, à voir les liens avec la réductions via les valeurs propres et les sous-espaces propres.}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Polynôme minimal $\pi_u$
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Remarque}: Polynôme annulateur
				\end{minipage}
				\item \emph{Proposition} \cite[p.271]{Cognet}: endomorphisme semblable : même $\pi$
				\item \emph{Remarque} \cite[p.271]{Cognet}: Réciproque est fausse
			\end{itemize}
			
			\noindent\textbf{Structure de l'idéal et conséquences} \textblue{Le polynôme $\pi_u$ engendre un idéal, il possède une propriété de divisibilité (notamment sur les polynômes annulateurs). La divisibilité nous permet d'obtenir des propriétés intéressantes comme la surjectivité de l'exponentielle.}

			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.35\textwidth}
					\item \emph{Proposition}: $\pi_u$ divise les annulateurs
				\end{minipage} \hfill
				\begin{minipage}{.45\textwidth}
					\item \textred{\emph{Application} \cite[p.48]{Zavidovique}: Surjectivité de l'exponentielle}
				\end{minipage}
				\item \emph{Corollaire}: Si $\exists P$, $P(u) = 0$ et $P(0) \neq 0$, alors $u$ inversible
				
				\begin{minipage}{.4\textwidth}
					\item \emph{Application}: Calcul d'inverse
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Application}: Calcul de puissance
				\end{minipage}
			\end{itemize}
		
			\noindent\textbf{Décomposition de $K[u]$ et sous-espace stable \cite[p.170]{Gourdon-algebre}} \textblue{La décomposition du $\pi_u$ permet de décomposer $K[u]$ est sous-espace: on donne ainsi sa dimension. On introduit les sous-espaces propres qui permettent d'avoir une réciproque au théorème de décomposition: les sous-espaces propres décompose $K[u]$ et permettent de calculer le$\pi_u$.}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Théorème}: Décomposition de $K[u]$ via $\pi_u$
					\item \emph{Définition}: Sous-espace stable
					\item \emph{Proposition}: Application induite $u|_F$
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Corollaire}: Dimension de $K[u]$
					\item \emph{Remarque}: $\mathrm{im}(u)$ et $\ker u$ sont stables par $u$
					\item \emph{Proposition}: $\pi_{u|_F} ~|~ \pi_u$
				\end{minipage}
				\item \emph{Théorème}: $\pi_u$ via une décomposition stable de $K[u]$
			\end{itemize}
			
			\noindent\textbf{Valeurs propres \cite[p.173]{Gourdon-algebre}} \textblue{On commence à voir apparaître un lien entre valeurs propres et polynômes d'endomorphisme via $\pi_u$.}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \emph{Définition} \cite[p.159]{Gourdon-algebre}: Valeurs propres et spectre
				\item \emph{Propriété}: Valeurs propres annulent polynômes annulateurs
				\item \emph{Remarque}: Réciproque est fausse ($\pi_u(X -a)$ où $a \notin Sp(u)$)
				\item \emph{Proposition}: Les racines de $\pi_u$ sont exactement les valeurs propres
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Le polynôme caractéristique \cite[p.948]{Berhuy}}
		\begin{small}
			\textblue{Le polynôme caractéristique appartient à l'idéal engendré par le $\pi_u$. Il possède alors les propriétés des polynômes annulateur d'un endomorphisme. Mais alors pourquoi le présenté puisque le $\pi_u$ caractérise cet idéal? Ce polynôme nous permet d'approfondir le lien avec le spectre de l'endomorphisme. C'est alors un outils fondamental de la théorie de la réduction.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.36\textwidth}
					\item \emph{Définition}: Polynôme caractéristique $\chi_u$
				\end{minipage} \hfill
				\begin{minipage}{.44\textwidth}
					\item \emph{Remarque}: Matrice semblable (indépendance base)
				\end{minipage}
				\begin{minipage}{.45\textwidth}
					\item \emph{Proposition}: Degré + relation coefficient racine
				\end{minipage} \hfill
				\begin{minipage}{.35\textwidth}
					\item \emph{Application}: Calcul en dimension $2$
				\end{minipage}
				\item \emph{Proposition}: Spectre est les racines de $\chi$
				\item \emph{Application}: $K$ algébriquement clos: tout $u$ admet une valeur propre
				
				\begin{minipage}{.5\textwidth}
					\item \emph{Théorème} \cite[p.174]{Gourdon-algebre}: Cayley-Hamilton
					\item \emph{Application}: Caractérisation endomorphismes nilpotents
				\end{minipage} \hfill
				\begin{minipage}{.3\textwidth}
					\item \emph{Définition}: Multiplicité algébrique
					\item \emph{Définition}: Sous-espace propre
				\end{minipage}
				\item \emph{Définition}: Multiplicité géométrique
				\item \emph{Proposition} \cite[p.290]{Cognet}: Borne sur la dimension des sous-espaces propres
				\item \emph{Application}: Décomposition en somme direct via les sous-espaces propres
				\item \emph{Proposition}: Stabilité et divisibilité
				\item \emph{Application} \cite[p.87]{Kieffer}: Résoudre des équations différentielles.
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[II.] \textbf{Un outils pour les réductions \cite[p.956]{Berhuy}}
	\begin{small}
		\textblue{Une réduction c'est choisir une base de telle sorte que la matrice que l'on étudie soit dans une forme agréable (souvent on la cherche diagonale). Lorsque cela n'est pas possible (ou que la forme n'est pas assez agréable), on cherche d'autre formes qui peuvent être des sommes ou des produits plus pratique à manipuler. Les polynômes d'endomorphismes sont des outils pratiques pour les réductions. Ils permettent de les caractériser (si elles existent ou non) et de les calculer (Dunford).}
	\end{small}
	\begin{description}
		\item[A.] \emph{Diagonalisation}
		\begin{small}
			\textblue{Trouver une base dans laquelle la matrice est sous forme diagonale est le Graal. Les conditions de diagonalisation se lisent dans le polynôme minimal. La décomposition quant à elle se lit dans le polynôme caractéristique.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Diagonalisation
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Proposition}: Caractérisation via $\pi_u$ et $\chi_u$
				\end{minipage}
				\item \emph{Application}: Diagonalisation sur sous-espace stable
				
				\begin{minipage}{.46\textwidth}
					\item \emph{Application}: Caractérisation de la co-diagonalisation
				\end{minipage} \hfill
				\begin{minipage}{.33\textwidth}
					\item \emph{Application}: Calcul de l'exponentielle
				\end{minipage}
				\item \emph{Remarque}: Pas la meilleur méthode mais peut servir dans le cas des équations différentielles.
				\item \emph{Méthode}: Calcul de la matrice diagonale
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Trigonalisation}
		\begin{small}
			\textblue{Si l'endomorphisme n'est pas diagonalisable, un bon compromis peut être la trigonalisation.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Trigonalisation
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Proposition}: Caractérisation via $\pi_u$ et $\chi_u$
				\end{minipage}
				\item \emph{Application}: Dans un corps algébriquement clos: tout est trigonalisable
				\item \emph{Application}: $\det(\exp(A)) = \exp(\mathrm{Tr}(A))$ : $\exp(A) \in GL_n(\C)$
				\item \emph{Application}: Suite récurrentes linéaires à coefficients constants
				
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Sous-espace caractéristique
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Proposition}: Diagonaliser par bloc
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Réduction}
		\begin{small}
			\textblue{Nous donnons ici quelques méthodes de réductions pour obtenir des matrices presque diagonales. La décomposition de Dunford est la plus simple et permet de calculer efficacement des puissances de matrices. Cependant, elle ne permet pas de répondre à la question de la classification: montrer que deux matrices sont semblables sous leur décomposition de Dunford, revient à montrer que les deux matrices sont semblables pour la même matrice $P$. La décomposition de Jordan vient alors remédier à ce problème: elle permet de caractériser les matrices semblables. Cependant l'application d'une telle méthode demande une connaissance du spectre et donc la factorisation du polynôme minimal. La réduction de Frobenius ne demande pas le calcul des valeurs propres et réalise cette classification.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \textred{\emph{Théorème}: Décomposition de Dunford}
				\end{minipage} \hfill
				\begin{minipage}{.45\textwidth}
					\item \emph{Application}: Calcul de l'exponentielle de matrice
				\end{minipage}
				\begin{minipage}{.55\textwidth}
					\item \emph{Application}: Caractérisation de la diagonalisation de $\exp(A)$.
				\end{minipage} \hfill
				\begin{minipage}{.25\textwidth}
					\item \emph{Définition}: Bloc de Jordan
				\end{minipage}
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Suite de noyau itéré
					\item \emph{Remarque}: Injection de Frobenius
					\item \emph{Application}: Classification
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Proposition}: Propriété de cette suite
					\item \emph{Théorème}: Réduction de Jordan 
					\item \emph{Théorème}: Décomposition de Frobenius
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
\end{description}
	
