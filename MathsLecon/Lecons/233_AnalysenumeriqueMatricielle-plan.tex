\chapter*{Leçon 233: Analyse numérique matricielle: résolution approchée de systèmes linéaires, recherche de vecteurs propres.}
\addcontentsline{toc}{chapter}{Leçon 233: Analyse numérique matricielle: résolution approchée de systèmes linéaires, recherche de vecteurs propres.}

\titlebox{blue}{Références pour la leçon}{	
	\textblue{\cite{AllaireKaber}} Allaire et Kaber, \emph{Algèbre linéaire numérique.}
	
	\textblue{\cite{BrezinskiRedivoZaglia}} Brezinski et Redivo--Zaglia, \emph{Méthodes numériques itératives.}
	
	\textblue{\cite{RamisWarusfel}} Ramis et Warusfel, \emph{Cours de mathématiques pures et appliquées.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.35\textwidth}
		Méthode de Kackmarz
	\end{minipage} \hfill
	\begin{minipage}{.5\textwidth}
		Méthode du gradient à pas optimal
	\end{minipage}
}

\section*{Motivation}

\subsection*{Défense}

\begin{itemize}
	\item Rayon spectral est difficile à calculer mais utile, par exemple pour le web.
	\item Problèmes résolus grâce à ces méthodes:
	\begin{itemize}
		\item $Ax = b$ (inverser $A$): schéma numériques, résolution approchées d'équations différentielles, calcul de bases d'un espace vectoriel.
		\item Valeurs propres de $A$ (diagonaliser): stat, stabilité de système différentiel, estimation du rayon spectral.
	\end{itemize}
	\item Méthodes itératives:
	\begin{itemize}
		\item Rapidité
		\item Stabilité: méthode directe (exacte): arrondit, solution loin de la réalité, comment se comporte les solutions lorsqu'elles sont perturbées?
	\end{itemize}
\end{itemize}

\subsection*{Ce qu'en dit le jury}

Le jury reprend une formulation antérieure de l’intitulé car la leçon se focalisait trop exclusivement sur la résolution de systèmes linéaires par des méthodes itératives. Le jury souhaite un sujet plus ouvert et des propositions qui ne négligent plus la recherche de vecteurs propres et, de manière générale, l’exploitation de techniques d’analyse pour aborder la résolution approchée de systèmes linéaires et de leurs propriétés spectrales et approfondir la compréhension des algorithmes.

Dans cette leçon de synthèse, les notions de norme matricielle et de rayon spectral sont centrales, en lien avec le conditionnement et avec la convergence des méthodes itératives ; elles doivent être développées et maîtrisées. Le conditionnement d’une matrice symétrique définie positive doit être connu et un lien avec $\sup_{\mathopen{||}x\mathclose{||}= 1} x^TAx$ doit être fait. Le résultat général de convergence, relié au théorème du point fixe de Banach, doit être enrichi de considérations sur la vitesse de convergence.

Le jury invite les candidats à étudier diverses méthodes issues de contextes variés : résolution de systèmes linéaires, optimisation de fonctionnelles quadratiques (du type $\frac{1}{2}(Ax, x) - (b, x)$), recherche
de valeurs propres,... Parmi les points intéressants à développer, on peut citer les méthodes de type Jacobi pour la résolution de systèmes linéaires, les méthodes de gradient dans le cadre quadratique, les méthodes de puissance et QR pour la recherche de valeurs propres. Les candidats pourront illustrer leur propos sur des matrices issues de schémas numériques pour les équations différentielles ou aux dérivées partielles linéaires.

\section*{Métaplan}

\begin{footnotesize}
	\noindent\cadre : $\R$ ou $\C$
\end{footnotesize}

\begin{description}
	\item[I.] \textbf{Normes matricielles, rayon spectral et conditionnement \cite[p.1]{BrezinskiRedivoZaglia}}
	\begin{small}
		\textblue{But: conditionnement petit}
	\end{small}
	\begin{footnotesize}
		\begin{itemize}[label=$\rightsquigarrow$]
			\begin{minipage}{.46\textwidth}
				\item \definiton: norme subordonnée
				\item \exemple: Norme subordonnée à $2$
				\item \definiton: Rayon spectral
				\item \definiton: Conditionnement d'une matrice
				\item \proposition~\cite[p.90]{AllaireKaber}: Lien avec la résolution
			\end{minipage} \hfill
			\begin{minipage}{.45\textwidth}
				\item \proposition: Propriété de cette norme
				\item \cexemple: Frobenius
				\item \remarque: Lien avec la norme
				\item \proposition: Propriétés du conditionnement
				\item \proposition~\cite[p.91]{AllaireKaber}: Lien avec les valeurs propres
			\end{minipage}
		\end{itemize}
	\end{footnotesize}
	\item[II.] \textbf{Résolution approchée de systèmes linéaires}
	\begin{small}
		\textblue{But: problème à résoudre.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Méthodes itératives de décomposition \cite[p. 157]{AllaireKaber}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Méthodes itératives
					\item \definiton: Erreur
					\item \definiton: Méthode de Jacobi
					\item \definiton: Méthode de Gauss--Seidel
				\end{minipage} \hfill
				\begin{minipage}{.45\textwidth}
					\item \definiton: Convergence d'une méthode
					\item \thm: Critère de convergence
					\item \proposition: Convergence dans le cas hermitien
					\item \proposition: Convergence dans le cas hermitien
				\end{minipage}
				\item \remarque~\cite[p.19]{BrezinskiRedivoZaglia} (ADMIS): Comparaison des convergences
				
				\begin{minipage}{.4\textwidth}
					\item \definiton: Méthode de la relaxation
					\item \thm: Convergence et sa vitesse
				\end{minipage} \hfill
				\begin{minipage}{.45\textwidth}
					\item \remarque: Valeurs de $\omega$
					\item \thm (ADMIS): Cas des matrices triangulaires
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Méthodes itératives géométriques}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.5\textwidth}
					\item \textred{\methode: Kackmarz}
				\end{minipage} \hfill
				\begin{minipage}{.44\textwidth}
					\item \lem: Caractérisation des solutions
				\end{minipage}
				\item \thm: Convergence de la méthode
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Optimisation \cite{RamisWarusfel}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.45\textwidth}
					\item \definiton: Lien entre $Ax = b$ et fonction
					\item \proposition: Existence et unicité des solutions
				\end{minipage} \hfill
				\begin{minipage}{.44\textwidth}
					\item \proposition: Gradient de cette fonctionnelle
					\item \textred{\methode: Gradient à pas optimal}
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[III.] \textbf{Recherche de valeurs et de vecteurs propres \cite[p.215]{AllaireKaber}}
	\begin{description}
		\item[A.] \emph{Calcul de la plus grande valeur propre}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Méthode de la puissance
					\item \thm: Terminaison
				\end{minipage} \hfill
				\begin{minipage}{.45\textwidth}
					\item \thm: Correction
					\item \proposition: Vitesse de convergence
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Calcul des autres valeurs propres}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.46\textwidth}
					\item \definiton: Méthode de la puissance inverse
					\item \thm: Terminaison
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \thm: Correction
					\item \remarque: Lien avec ce qui précède
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
\end{description}

