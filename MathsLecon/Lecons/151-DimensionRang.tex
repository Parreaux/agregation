\documentclass[a4paper,8pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais,english]{babel}%

\usepackage[dvipsnames]{xcolor}

\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{enumitem}
\usepackage{appendix}
\usepackage{stmaryrd}
\usepackage{amsmath}
\usepackage{wasysym}

\usepackage{listings}
\usepackage{multirow}
\usepackage{algorithm}
\usepackage{algpseudocode}
\algnewcommand{\LineComment}[1]{\State \(\triangleright\) #1}

\parskip=0\baselineskip %0.5 d'habitude


\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\F}{\mathbb{F}}
	\newcommand{\Sn}{\mathfrak{S}}
	\newcommand{\A}{\mathfrak{A}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textbluesmall}[1]{{\small{\textcolor{blue}{#1}}}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\theoremstyle{plain}
	\newtheorem{theo}{Théorème}
	\newtheorem{prop}{Proposition}
	\newtheorem{cor}{Corollaire}
	\newtheorem{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem{definition}{Définition}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	
	\title{Leçon 151 : Dimension d'un espace vectoriel (on se limitera au cas fini). Rang. Exemples et applications.}
	
	%\author{Julie Parreaux}
	\date{}
	\maketitle
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
	
	\section*{Motivation}
	
	\subsection*{Défense}
	
	\begin{itemize}
		\item Démonstration par récurrence sur le rang ou la dimension.
		\item Classification des endomorphisme.
		\item Retrouver des résultats qu'on avait sur les ensembles finis.
		\item Avoir une description finie d'un ensemble infini.
	\end{itemize}
	
	\subsection*{Ce qu'en dit le jury}
	
	Dans cette leçon, il est indispensable de présenter les résultats fondateurs de la théorie des espaces vectoriels de dimension finie en ayant une idée de leurs preuves. Ces théorèmes semblent simples car ils ont été très souvent pratiqués, mais leur preuve demande un soin particulier. Il est important de savoir justifier pourquoi un sous-espace vectoriel d’un espace vectoriel de dimension finie est aussi de dimension finie. Le pivot de Gauss ainsi que les diverses notions et caractérisations du rang trouvent leur place dans cette leçon. Les applications sont nombreuses, on peut par exemple évoquer l’existence de polynômes annulateurs ou alors décomposer les isométries en produits de réflexions.
	
	On pourra utiliser les caractérisations du rang pour démontrer l’invariance du rang par extension de
	corps, ou pour établir des propriétés topologiques (sur $\R$ ou $\C$). S’ils le désirent, les candidats peuvent déterminer des degrés d’extensions dans la théorie des corps ou s’intéresser aux nombres algébriques.
	
	On pourra également explorer des applications en analyse comme les extrémas liés ou l’étude de l’espace
	vectoriel engendré par les translatés d’une application de $\R$ dans $\R$.
	
	Dans un autre registre, il est pertinent d’évoquer la méthode des moindres carrés dans cette leçon,	par exemple en faisant ressortir la condition de rang maximal pour garantir l’unicité de la solution et s’orienter vers les techniques de décomposition en valeurs singulières pour le cas général. On peut alors naturellement explorer l’approximation d’une matrice par une suite de matrices de faible rang.
	
	\section*{Métaplan}
	
	\begin{footnotesize}
		\noindent\emph{Pré-requis}: Espace vectoriel; Sous-espace vectoriel; Application linéaire; Matrice; Système linéaire; Corps
			
		\noindent\emph{Cadre}: $E$ espace vectoriel sur un corps $K$ commutatif
	\end{footnotesize}
	
	\begin{description}
		\item[I.] \textbf{Base et dimension \cite[p.10]{Grifone}}
		
		\begin{small}
			\textblue{Les notions de bases et de dimension sont très liés puisque le cardinal d'une base donne la dimension d'un espace vectoriel (en dimension fini).}
		\end{small}
		\begin{description}
			\item[A.] \emph{Famille libres, génératrices et bases}
			
			\begin{small}
				\textblue{Intuitivement les familles génératrices permettent d'exprimer tout autre vecteur comme un combinaison linéaire de la famille. Une telle famille n'existe pas toujours : on définie donc les espaces de dimension finie ou non. Une base permet de garantir l'existence et unicité de la décomposition.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.36\textwidth}
						\item \emph{Définition}: famille génératrice
						\item \emph{Définition}: dimension finie et infinie
						\item \emph{Définition}: famille libre
						\item \emph{Remarque}: famille non-libre est une famille liée
						\item \emph{Proposition}: caractérisation de la liberté via la généricité
						\item \emph{Remarque}: utilisation du pivot de Gauss
					\end{minipage} \hfill
					\begin{minipage}{.44\textwidth}
						\item \emph{Proposition}: propriétés des familles libres et génératrices
						\item \emph{Définition}: base
						\item \emph{Proposition}: sur et sous famille génératrice et libre
						\item \emph{Théorème}: existence d'une base 
						\item \emph{Théorèmes}: base incomplète, trop complète 
						\item \emph{Proposition} \cite[p.63]{Grifone}: Application injective/surjective et famille libre/génératrice.
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[B.] \emph{Dimension finie}
			
			\begin{small}
				\textblue{Nous allons maintenant donner le lien précis entre base et dimension finie: le cardinal donne le second. Pour établir ce résultat, nous utilisons le lien entre liberté et généricité comme décrite dans le premier lemme. Nous donnerons ensuite une version plus classique de ce résultat.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.36\textwidth}
						\item \emph{Lemme}: Famille génératrice à $n$ éléments donne famille lié si plus de $n$ éléments.
						\item \emph{Théorème}: Dans un espace de dimension finie, toute base à même cardinal
						\item \emph{Définition}: $\dim = \mathrm{card}$ de la base et notation
						\item \emph{Application}: Théorème de la base de Burnside
						\item \emph{Corollaire}: Caractérisation via la dimension des familles libres et génératrices
					\end{minipage} \hfill
					\begin{minipage}{.44\textwidth}
						\item \emph{Théorème}: Caractérisation base avec cardinal
						\item \emph{Application}: Preuve par récurrence sur les dimensions 
						\item \emph{Application}: réduction des auto-adjoints \textred{DEV}
						\item \emph{Application}: algorithme de Berkhamp \textred{DEV}
						\item \emph{Théorème} \cite[p.63]{Grifone}: Deux espaces vectoriels sont isomorphes si et seulement si ils ont même dimension
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[C.] \emph{Sous-espace vectoriel de dimension finie}
			
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \emph{Théorème}: sous-espace et dimension
						\item \emph{Proposition} \cite[p.85]{Grifone}: dimension de $\mathcal{L}(E, F)$ et du dual
						\item \emph{Proposition}: dimension des espaces produits
						\item \emph{Définitions}: somme
						\item \emph{Proposition}: dimension de la somme (formule de Grassmann)
					\end{minipage} \hfill
					\begin{minipage}{.44\textwidth}
						\item \emph{Définition}: somme directe, supplémentaire
						\item \emph{Théorème}: existence et dimension d'un supplémentaire
						\item \emph{Application}: dimension et forme linéaire
						\item \emph{Corollaire}: caractérisation des espaces somme directe
						\item \emph{Définition} \cite[p.956]{Berhuy}: sous-espaces propres
						\item \emph{Théorème} \cite[p.956]{Berhuy}: caractérisation de la diagonalisation
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[D.] \emph{Application à l'extension de corps \cite[p.65]{Perrin}}
			
			\begin{small}
				\textblue{La théorie de la dimension s'applique dans les extension de corps : le degré de l'extension est la dimension de l'espace vectoriel induit pas cette extension.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \emph{Définition}: Extension de corps
						\item \emph{Remarque}: $L$ extension du corps $K$ est un $K$-espace vectoriel 
						\item \emph{Définition}: degrés de l'extension: $dim_{K}(L)$
						\item \emph{Théorème}: Théorème de la base télescopique
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \emph{Corollaire}: Multiplicité des degrés
						\item \emph{Définition}: Éléments algébrique, polynôme minimal
						\item \emph{Théorème}: Caractérisation des éléments algébriques
						\item \emph{Application}: Si $K \subset L, \{x \in L ~|~ x~algébrique~dans~K\}$ est un sous-corps de $L$
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
		\end{description}
		\item[II.] \textbf{Un peu de topologie \cite[p.46]{Gourdon-analyse}}
		
		\begin{small}
			\textblue{La dimension finie nous donne des résultats intéressant en topologie des espaces vectoriels normés.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Théorème}: norme équivalente
					\item \emph{Théorème}: compacité
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Proposition}: fonction continue
					\item \emph{Théorème}: boule unité fermé compacte
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[III.] \textbf{Rang et applications linéaires en dimension finie}
		
		\begin{small}
			\textblue{Le rang est la dimension de l'image d'une application linéaire. Nous pouvons alors l'étudier sur l'application linéaire ou sur des matrices. Nous allons définir le rang sur les deux objets et en fonction des applications que nous souhaitons en faire, nous allons choisir l'une ou l'autre de ces représentation.}
		\end{small}
		\begin{description}
			\item[A.] \emph{Notion de rang \cite[p.61]{Grifone}}
			
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.38\textwidth}
						\item \emph{Définition}: rang d'une application linéaire
						\item \emph{Théorème}: théorème du rang
						\item \emph{Application} \cite[p.114]{Gourdon-algebre}: projecteur (endomorphisme $p^{2} = p$)
						\item \emph{Corollaire}: caractérisation de la bijection
						\item \emph{Contre-exemple}: faux en dimension infini (dérivée d'un polynôme) et multiplication par une variable
					\end{minipage} \hfill
					\begin{minipage}{.42\textwidth}
						\item \emph{Proposition}: Dimension et surjectivité/injectivité
						\item \emph{Application} \cite[p.113]{Gourdon-algebre}: quotient d'espace vectoriel
						\item \emph{Application}: intersection de noyau de formes linéaires
						\item \emph{Application} \cite[p.154]{BeckMalikPeyre}: polynôme interpolateur de Lagrange 
						\item \emph{Définition}: rang d'une famille de vecteur, de matrice
						\item \emph{Proposition}: rang d'endomorphisme et de matrice associée
						\item \emph{Remarque}: caractérisation par le rang: application et inversion.
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[B.] \emph{Caractérisation du rang \cite[p.121]{Gourdon-algebre}}
			
			\begin{small}
				\textblue{On étudie le rang pour des matrices vérifiant certaines propriétés (souvent données par des actions de groupes).}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \emph{Définition}: matrice équivalente
						\item \emph{Théorème}: Deux matrice équivalente ont le même rang
						\item \emph{Application}: action de Steinitz, caractérisation avec les orbites et systèmes de représentant
						\item \emph{Définition}: matrice extraite
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \emph{Théorème}: Caractérisation du rang avec les matrices extraites
						\item \emph{Définition}: matrice semblables
						\item \emph{Théorème}: Deux matrices semblables ont même rang
						\item \emph{Exemple}: Contre-exemple de la réciproque
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[C.] \emph{Calcul du rang}
			
			\begin{small}
				\textblue{Le calcul du rang se fait essentiellement via le pivot de Gauss.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.46\textwidth}
						\item \emph{Proposition}: opérations élémentaires ne modifie pas le rang
						\item \emph{Application} \cite[p.205]{Cognet}: méthode du pivot de Gauss
						\item \emph{Proposition}: existence et unicité des solutions d'un système suivant le rang
					\end{minipage} \hfill
					\begin{minipage}{.36\textwidth}
						\item \emph{Proposition}: $rg A = rg(^{t}A)$
						\item \emph{Application}: calcul de noyaux, d'image, de famille libre, ...
						\item \emph{ Application}: théorème des extremum liés + inégalité d'Hadamard \textred{DEV}
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
		\end{description}
	\end{description}
		
	
	\section*{Quelques notions importantes}
	
	\subsection*{Famille libre, génératrice et base}
	\input{./../Notions/AlgebreLineaire_FamilleLibreGeneratriceBase.tex}
	
	\subsection*{Théorie de la dimension}
	\input{./../Notions/AlgebreLineaire_Dimension.tex}
	
	\subsection*{Somme directe}
	\input{./../Notions/AlgebreLineaire_SommeDirecte.tex}
	
	\subsection*{Diagonalisation}
	\input{./../Notions/AlgebreLineaire_Diagonalisation.tex}
	
	\subsection*{Extension de corps}
	\input{./../Notions/Corps_extension.tex}
	
	\subsection*{La notion de rang}
	\input{./../Notions/AlgebreLineaire_Rang.tex}
	
	\subsection*{Pivot de Gauss}
	\input{./../Notions/Algo_PivotGauss.tex}






\end{document}