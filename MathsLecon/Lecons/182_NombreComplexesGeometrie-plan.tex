\chapter*{Leçon 182: Application des nombres complexes à la géométrie.}
\addcontentsline{toc}{chapter}{Leçon 182: Application des nombres complexes à la géométrie.}

\titlebox{blue}{Références pour la leçon}{	
	\textblue{\cite{Audin}} Audin, \emph{Géométrie.}
	
	\textblue{\cite{Eiden}} Eiden, \emph{Géométrie analytique classique.}
	
	\textblue{\cite{Trignan}} Trignan, \emph{La géométrie des nombres complexes.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.35\textwidth}
		Coloration du cube
	\end{minipage} \hfill
	\begin{minipage}{.5\textwidth}
		Générateurs de $\textsc{SL}_2(\Z)$
	\end{minipage}
}

\section*{Motivation}

\subsection*{Défense}

\begin{itemize}
	\item Au $XVII^{\text{ième}}$ siècle: résolution d'équation de degré $2$ ou $3$. Introduction du corps $\C$.
	\item Définir le plan grâce à $\C$ qui est un corps commutatif à la place de $\R^2$.
\end{itemize}

\subsection*{Ce qu'en dit le jury}

Cette leçon ne doit pas rester au niveau de la classe de Terminale. L’étude des inversions est tout à fait appropriée, en particulier la possibilité de ramener un cercle à une droite et inversement ; la formule de Ptolémée illustre bien l’utilisation de cet outil. On peut parler des suites définies par récurrence par une homographie et leur lien avec la réduction dans $SL_2(\C)$.

S’ils le désirent, les candidats peuvent aussi étudier l’exponentielle complexe et les homographies de la sphère de Riemann. La réalisation du groupe SU 2 dans le corps des quaternions et ses applications peuvent trouver leur place dans la leçon. Il est possible de présenter les similitudes, les homographies et le birapport.

\section*{Métaplan}

\begin{description}
	\item[I.] \textbf{Géométrie euclidienne}
	\begin{small}
		\textblue{Nous allons nous concentrer sur l'utilisation des nombres complexes dans le cadre de la géométrie euclidienne.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Le plan complexe \cite[p.219]{Eiden}}
		\begin{small}
			\textblue{Le plan euclidien peut être interprété comme un plan complexe: nous allons étudier ce lien.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Coordonnées complexes
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \definiton: Affixe d'un point
				\end{minipage}
				\item \definiton: Affixe d'un vecteur: norme et argument
				\item \proposition ~\cite[p.39]{Trignan}: Traduction de la notion de colinéarité
				\item \proposition ~\cite[p.40]{Trignan}: Traduction de la notion d'orthogonalité
				\item \proposition: Traduction de la notion d'alignement
				
				\begin{minipage}{.45\textwidth}
					\item \proposition: Équation d'un cercle ou d'une droite
				\end{minipage} \hfill
				\begin{minipage}{.35\textwidth}
					\item \application: Aire d'un triangle
				\end{minipage}
				\item \application: Coordonnées exponentielles
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Définition des angles \cite[p.73]{Audin}}
		\begin{small}
			\textblue{La notion d'angle apparaît rapidement dans le cadre d'étude de complexe puisqu'ils peuvent être caractérisé par une norme et un angle.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \proposition: Définition d'un angle par une rotation
				
				\begin{minipage}{.4\textwidth}
					\item \definiton: Angle orienté
					\item \definiton: Rotation matricielle
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \proposition: Relation de Chasles
					\item \application: Coordonnées polaires
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Transformations du plan \cite[p89]{Audin}}
		\begin{small}
			\textblue{Les isométries du plan en sont les transformations. Ces transformations peuvent être exprimées à l'aide de complexe.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Similitude
					\item \application: Conservation des angles
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \proposition: Générateur du groupe
					\item \definiton: Groupe des isométries
				\end{minipage}
				\begin{minipage}{.3\textwidth}
					\item \definiton: Conjugaison
				\end{minipage} \hfill
				\begin{minipage}{.55\textwidth}
					\item \proposition~\cite[p.135]{Trignan}: Traduction via les nombres complexes
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[D.] \emph{Coordonnées barycentriques \cite[p.5]{Eiden}}
		\begin{small}
			\textblue{Le système de coordonnées barycentrique est un système de coordonnées important permettant de donner des coordonnées à partir d'un triangle de base. Nous allons étudier quelques objets mathématiques via ces coordonnées.} 
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Barycentre
					\item \application: Théorème de Desargues
					\item \proposition: Lien avec les cartésiennes
					\item \application: Aire d'un triangle
					\item \definiton: Équation barycentrique
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \proposition: Propriétés du barycentre
					\item \definiton: Coordonnées barycentrique
					\item \application: Définition des vecteurs
					\item \application: Point de Lemoine
					\item \definiton: Droite et concours
				\end{minipage}
				\item \definiton: Équation de cercle
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[II.] \textbf{Géométrie projective complexe}
	\begin{small}
		\textblue{Le plan complexe peut être utilisé comme support à la géométrie projective. On commence par définir celle-ci dans le cas complexe.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Géométrie projective \cite[p.177]{Audin}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Droite projective
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \proposition: Sphère de Riemann
				\end{minipage}
				\item \definiton: Repère projectif
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Homographie \cite[p.255]{Eiden}}
		\begin{small}
			\textblue{Les homographies sont à la géométrie projective ce que sont les isométries sont à la géométrie. Nous allons les définir et en étudier certaines propriétés.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Homographie
					\item \application: Ellipse et Protéisme de Steiner
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \proposition: Propriétés des homographies
					\item \application: Théorème de Pasacal
				\end{minipage}
				\item \proposition: Action de $GL_n(\Z)$ sur le demi-plan de Poincaré par homographie
				\item \textred{\thm: Générateur de $SL_2(\Z)$}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Birapport \cite[p.261]{Eiden}}
		\begin{small}
			\textblue{Le birapport est une opération conservant les homographies.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Birapport
					\item \proposition: Caractérisation de co-cyclicité
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \proposition: Calcul d'un birapport
					\item \proposition: Caractérisation de l'alignement
				\end{minipage}
				\begin{minipage}{.35\textwidth}
					\item \proposition: Caractérisation du milieu
				\end{minipage} \hfill
				\begin{minipage}{.44\textwidth}
					\item \proposition: Homographie préserve le birapport
				\end{minipage}
				\item \application: Générateur du groupe circulaire
			\end{itemize}
		\end{footnotesize}
		\item[D.] \emph{Inversion \cite[p.244]{Eiden}}
		\begin{small}
			\textblue{Les inversions sont des transformations qui inverse les distances par rapport à un point donnée.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Inversion
					\item \application: Sphère de Riemann
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item Interprétation
					\item \thm: Théorème de Ptomélé
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
\end{description}
