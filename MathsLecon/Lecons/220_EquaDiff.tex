\documentclass[a4paper,8pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais,english]{babel}%

\usepackage[dvipsnames]{xcolor}

\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{enumitem}
\usepackage{appendix}
\usepackage{stmaryrd}
\usepackage{amsmath}
\usepackage{wasysym}

\usepackage{listings}
\usepackage{multirow}
\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}
\algnewcommand{\LineComment}[1]{\State \(\triangleright\) #1}

\parskip=0\baselineskip %0.5 d'habitude


\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\F}{\mathbb{F}}
	\newcommand{\Sn}{\mathfrak{S}}
	\newcommand{\A}{\mathfrak{A}}
	
	\newcommand{\thm}{\emph{Théorème}}
	\newcommand{\proposition}{\emph{Proposition}}
	\newcommand{\lem}{\emph{Lemme}}
	\newcommand{\corollaire}{\emph{Corollaire}}
	\newcommand{\definiton}{\emph{Définition}}
	\newcommand{\remarque}{\emph{Remarque}}
	\newcommand{\application}{\emph{Application}}
	\newcommand{\exemple}{\emph{Exemple}}
	\newcommand{\cexemple}{\emph{Contre-exemple}}
	\newcommand{\cadre}{\emph{Cadre}}
	\newcommand{\methode}{\emph{Méthode}}
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textbluesmall}[1]{{\small{\textcolor{blue}{#1}}}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\sloppy
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\theoremstyle{plain}
	\newtheorem{theo}{Théorème}
	\newtheorem{prop}{Proposition}
	\newtheorem{cor}{Corollaire}
	\newtheorem{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem{definition}{Définition}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	\newtheorem*{cex}{Contre-exemple}
	
	\title{Leçon 220 : Équations différentielles $X' = f(t, X)$. Exemples d'étude des solutions en dimension $1$ et $2$.}
	
	%\author{Julie Parreaux}
	\date{}
	\maketitle
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
	
	\section*{Motivation}
	
	\subsection*{Défense}
	
	\begin{itemize}
		\item $XVII^{\text{ième}}$ siècle: apparition du calcul différentiel et intégrable (apporte une riche théorie mathématiques)
		\item Les équations différentielles apparaissent alors en géométrie ou en mécanique (applications)
		\item À leur début, pas de recherche d'existence ni d'unicité (on ne cherche qu'à les résoudre sans savoir si c'est possible).
	\end{itemize}
	

	
	\subsection*{Ce qu'en dit le jury}
	
	Une nouvelle fois, le jury s’alarme des nombreux défauts de maîtrise du théorème de Cauchy--Lipschitz et, plus généralement, de l’extrême faiblesse des connaissances sur les équations différentielles. Il est regrettable de voir des candidats ne connaître qu’un énoncé pour les fonctions globalement lipschitziennes ou, plus grave, mélanger les conditions sur la variable de temps et d’état. Les notions de solution maximale et de solution globale sont souvent confuses. Le théorème de sortie de tout compact est attendu. Bien évidemment, le jury attend des exemples d’équations différentielles non linéaires. Le lemme de Grönwall semble trouver toute sa place dans cette leçon mais est trop rarement énoncé. L’utilisation du théorème de Cauchy--Lipschitz doit pouvoir être mise en œuvre sur des exemples concrets. Les études qualitatives doivent être préparées et soignées. 
	
	Pour les équations autonomes, la notion de point d’équilibre permet des illustrations pertinentes comme par exemple les petites oscillations du pendule. Trop peu de candidats pensent à tracer et discuter des portraits de phase alors que l’intitulé de la leçon y invite clairement.
	
	Il est possible d’évoquer les problématiques de l’approximation numérique dans cette leçon en présentant le point de vue du schéma d’Euler. On peut aller jusqu’à aborder la notion de problèmes raides et la conception de schémas implicites pour autant que le candidat ait une maîtrise convenable de ces questions.
	
	\section*{Métaplan}
	
	\begin{footnotesize}
		\emph{Cadre}: On se place en dimension finie dans $\R^m$.
	\end{footnotesize}
	
	\begin{description}
		\item[I.] \textbf{Théorie des équations différentielles}
		
		\begin{small}
			\textblue{On se place dans un cadre très général pour donner la théorie des équations différentielles et notamment l'étude des solutions.}
		\end{small}
		\begin{description}
			\item[A.] \emph{Notions de solutions \cite[p.125]{Demailly}}
			
			\begin{small}
				\textblue{Lorsqu'on se donne une équation différentielle qu'appelle-t-on une solution? Dans de nombreuses situations concrète, la variable $t$ représente le temps et $y$ représente les différents paramètres de l'état de notre système. Résoudre un problème de Cauchy revient à décrire l'état d'un système en fonction du temps à partir d'un temps et des configurations initiaux. Prendre un unique exemple de ces notions sur une équation différentielle que nous pouvons commenter à l'oral.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \definiton~\cite[p.353]{Gourdon-analyse}: Solution
						\item \remarque\cite[p.353]{Gourdon-analyse}: On se ramène au cas d'un problème d'ordre 1.
						\item \cadre: Équations que l'on cherche à résoudre
						\item \definiton: Problème de Cauchy
						\item \definiton: Prolongement des solutions
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \definiton: Solution maximale
						\item \thm: Existence d'une solution maximale
						\item \definiton: Solution globale
						\item \remarque: Globale implique maximale mais pas réciproque
						\item \cexemple: $y' = y^2$ sur $U = \R \times \R$
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[B.] \emph{Existence et unicité des solutions \cite[p.131]{Demailly}}
			
			\begin{small}
				\textblue{Avant de se mettre à la recherche des solutions, nous voulons nous assurer qu'elles existent. C'est Cauchy qui est le premier à avoir formaliser l'existence de solution (la différence entre le local et le global apparaît à ce moment dans les mathématiques). Il existe une caractérisation via l'intégrale du problème de Cauchy, cependant cette caractérisation demande l'étude de la dite intégrale. On utilise donc des critère plus simples à manipuler. Le théorème de Cauchy--Peano nous garantissant l'existence des solutions pour des fonctions continues. Pour obtenir l'unicité nous devons avoir un caractère Lipschitz supplémentaire, ce qui nous donnes les théorèmes de Cauchy--Lipschitz local et global. On réfléchie des hypothèses des plus faibles aux plus fortes.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.44\textwidth}
						\item \lem: Caractérisation des solutions via l'intégrale
						\item \definiton: Cylindre de sécurité
						\item \thm: Théorème de Cauchy--Peano--Arzela
						\item \corollaire: Existence de solution maximale \textblue{(interprétation)}
						\item \exemple: $y' = \left|y\right|^{\frac{2}{3}}$
					\end{minipage} \hfill
					\begin{minipage}{.36\textwidth}
						\item \definiton: Fonctions lipschitziennes
						\item \thm: Cauchy--Lipschitz local
						\item \exemple: $y' = \left|y\right|^{\frac{2}{3}}$
						\item \thm~\cite[p.170]{Rouviere}: Cauchy-Lipschitz global \textred{(DEV)}
						\item \exemple~\cite[p.170]{Rouviere}: Équation du pendule
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[C.] \emph{Propriétés des solutions \cite[p.377]{Gourdon-analyse}}
			
			\begin{small}
				\textblue{Maintenant que nous sommes capables de savoir si une équation possède ou non des solutions, nous donnes quelques propriétés sur ces équations.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \thm~\cite[p.130]{Demailly}: Régularité des solutions
						\item \thm: Lemme de Grönwall
						\item \corollaire: Lemme de Grönwall
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \application: Distance entre deux solutions
						\item \thm: Sortie de tout compact
						\item \application: Champs de vecteurs complets
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
		\end{description}
		\item[II.] \textbf{Résolution explicite et étude qualitative}
		
		\begin{small}
			\textblue{Maintenant que nous savons si une équation différentielle possède ou non une solution, nous souhaitons calculer cette solution. Dans certains cas la résolution est facile (cas des équations différentielles linéaires), mais dans la majorité des cas nous devons nous contenter d'une étude qualitative des solutions. Nous aurions également pû parler de calcul numérique des solutions par approximations successives (schéma d'Euler).}
		\end{small}
		\begin{description}
			\item[A.] \emph{Solutions des équations différentielles linéaires \cite[p.201]{Demailly}}
			
			\begin{small}
				\textblue{Les équations différentielles linéaires sont des équations pour lesquelles la résolution est simple. De nombreux résultats et méthodes nous permettent de résoudre ses équations. Attention, nous avons besoins de l'exponentielle de matrice.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.44\textwidth}
						\item \proposition: Espace des solutions et dimension
						\item \thm: Solution d'équations sans deuxième membre
						\item \application~\cite[p.363]{Gourdon-analyse}: Système linéaire en dimension 3
					\end{minipage} \hfill
					\begin{minipage}{.36\textwidth}
						\item \thm: Solution générale
						\item \application: Résolution de $y'' + 4y = \tan t$
						\item \methode: Variation de la constante
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[B.] \emph{Équations remarquables \cite[p.164]{Demailly}}
			
			\begin{small}
				\textblue{Voici quelques exemples d'équations que nous sommes capables de résoudre explicitement: soit elles se ramène à des équations linéaires, soit elles utilisent des outils analytiques comme les séries entières.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item Équation de Bernoulli
						\item Équation de Ricatti
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item Équation de Bessel
						\item Équation homogène
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[C.] \emph{Étude qualitative des solutions}
			
			\begin{small}
				\textblue{Dans la majorité des cas, nous ne sommes pas capable de résoudre une équation différentielle. Nous réalisons alors une étude quantitative des solutions pour en connaître quelques propriétés. On cherche à connaître leur régularité, leur monotonie ou leur zéros.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \emph{Principe}
						\item \remarque: Approximation des solutions
						\item \exemple: Équation du pendule
					\end{minipage} \hfill
					\begin{minipage}{.46\textwidth}
						\item \exemple: Équation de Catho--Voltena
						\item \proposition~\cite[p.378]{Gourdon-analyse}: Solution de $y'' + qy = 0$ bornée
						\item \proposition: Nombre de zéros de cette équation
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
		\end{description}
		\item[III.] \textbf{Stabilité des systèmes autonomes}
		
		\begin{small}
			\textblue{Cette partie est également une étude qualitative des équations différentielles : on les étudie asymptotiquement afin de savoir si elles restent ou non dans un compact.}
		\end{small}
		\begin{description}
			\item[A.] \emph{Systèmes autonomes stables dans le cas linéaire \cite[p.380]{ZuilyQueffelec2}}
			
			\begin{small}
				\textblue{Dans ce cas, c'est toujours plus facile.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \definiton: Système stable \textblue{(Dessin)}
						\item \definiton: Système instable \textblue{(Dessin)}
					\end{minipage} \hfill
					\begin{minipage}{.46\textwidth}
						\item \definiton: Système asymptotiquement stable \textblue{(Dessin)}
						\item \thm: Stabilité dans le cas linéaire
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[B.] \emph{Étude de la stabilité dans le cas non linéaire \cite[p.132]{Rouviere}}
			
			\begin{small}
				\textblue{Dans ce cadre cette étude est plus compliquée. Cependant, nous sommes capable de nous ramené à une système linéaire qui va conserver les mêmes caractéristiques de stabilité.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \definiton: Système linéarisé
						\item \thm: Théorème de Lyapunov
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \cexemple:
						\item \exemple: Équation de Van der Pol pour $\epsilon > 0$
						\item \exemple: Équation du pendule
					\end{minipage} 
				\end{itemize}
			\end{footnotesize}
		\end{description}
	\end{description}
	
	\section*{Quelques notions importantes}
	
	\subsection*{Solution d'une équation différentielle}
	\input{./../Notions/EquaDiff_Solution.tex}
	
	\subsection*{Existence et unicité des solution d'une équation différentielle}
	\input{./../Notions/EquaDiff_ExistenceUniciteSol.tex}
	
	\subsection*{Propriétés des solution d'une équation différentielle}
	\input{./../Notions/EquaDiff_PropSol.tex}
	
	\subsection*{Exponentielle de matrices}
	\input{./../Notions/ExponentielleMatrice.tex}

	\subsection*{Solution d'une équation différentielle linéaire}
	\input{./../Notions/EquaDiff_LineaireSol.tex}
	
	\subsection*{Équation différentielle particulière}
	\input{./../Notions/EquaDiff_Particuliere.tex}
	
\end{document}