\chapter*{Leçon 228: Continuité et dérivabilité d'une fonction réelle à une variable réelle. Exemples et applications.}
\addcontentsline{toc}{chapter}{Leçon 228: Continuité et dérivabilité d'une fonction réelle à une variable réelle. Exemples et applications.}

\titlebox{blue}{Références pour la leçon}{	
	\textblue{\cite{Gourdon-analyse}} Gourdon, \emph{Analyse.}
	
	\textblue{\cite{Hauchecorne}} Hauchecorne, \emph{Les contre-exemples en mathématiques.}
	
	\textblue{\cite{Kieffer}} Kieffer, \emph{66 leçons pour l'agrégation de mathématiques.}
	
	\textblue{\cite{Marco}} Marco, \emph{Mathématiques L3, Analyse}
	
	\textblue{\cite{Ouvrard-1}} Ouvrard, \emph{Probabilité 1.}
	
	\textblue{\cite{Rombaldi}} Rombaldi, \emph{Éléments d'analyse réel: capes et agrégation.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.35\textwidth}
		Théorème de Weierstrass
	\end{minipage} \hfill
	\begin{minipage}{.5\textwidth}
		Méthode de Newton
	\end{minipage}
}

\section*{Motivation}

\subsection*{Défense}

\begin{itemize}
	\item La notion de continuité a été très tôt intuitée mais il a fallut attendre les travaux de Peano pour ébauché la théorie actuelle.
	\item La notion de dérivation s'est quand à elle développée avec les travaux de Leibniz et de Newton sur l'infinitésimal.
	\item Comme nous le verrons dans la suite de la leçon ses deux notions sont très proches l'une de l'autre et interagissent à bien des égares.
\end{itemize}

\subsection*{Ce qu'en dit le jury}

Cette leçon permet des exposés de niveaux très variés. Les théorèmes de base doivent être maîtrisés et illustrés par des exemples intéressants, par exemple le théorème des valeurs intermédiaires pour la dérivée. Le jury s’attend évidemment à ce que le candidat connaisse et puisse calculer la dérivée des fonctions usuelles. Les candidats doivent disposer d’un exemple de fonction dérivable de la variable réelle qui ne soit pas continûment dérivable. La stabilité par passage à la limite des notions de continuité
et de dérivabilité doit être comprise par les candidats. De façon plus fine, on peut s’intéresser à des exemples de fonctions continues nulle part dérivables.

Les propriétés de régularité des fonctions convexes peuvent être mentionnées. Pour aller plus loin, la dérivabilité presque partout des fonctions lipschitziennes ou des fonctions monotones relève de cette leçon. L’étude des liens entre dérivée classique et dérivée au sens des distributions de fonctions telles que la fonction de \textsc{Heaviside}, de la valeur absolue ou de la fonction $x \mapsto \int_{a}^{x}f(y)dy$, $f$ étant intégrable, peuvent trouver leur place dans cette leçon. On peut aussi relier la dérivée faible et la limite du taux d’accroissement au sens des distributions et établir le lien entre fonction croissante et dérivée faible positive.

\section*{Métaplan}

\begin{footnotesize}
	\noindent\emph{Cadre}: $I$ désigne un ensemble $\R$, $a$ un point de $I$ et $f$ une fonction de $I$ dans $\R$.
\end{footnotesize}

\begin{description}
	\item[I.] \textbf{Continuité d'une fonction}
	\begin{description}
		\item[A.] \emph{Définitions et premières propriétés}
		\begin{small}
			\textblue{\emph{Objectif}: formaliser la notion de continuité et en dégager les premières propriétés.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \definiton~\cite[p. 37]{Rombaldi} : Continuité en un point 
				\item \exemple~\cite[p. 39]{Rombaldi} : $\mathbb{1}_{\Q}$ n'est continue en aucun point de $\R$
				\item \remarque~\cite[p.208]{Rombaldi} : Continue en $a$ $\Leftrightarrow$ DL d'ordre 0 en $a$
				\item \definiton~\cite[p. 37]{Rombaldi} : Fonction continue
				\item \exemple : Fonctions usuelles et leurs réciproques sont continues
				\item \remarque + \exemple~\cite[p.149]{Hauchecorne} : Réciproque d'une fonction continue
				\item \thm~\cite[p. 38]{Rombaldi} :  Caractérisation séquentielle de la continuité
				\item \application~\cite[p.39]{Rombaldi} : Discontinuité de la fonction en un point: $x \mapsto \frac{\sin(x)}{x}$
				
				\begin{minipage}{.45\textwidth}
					\item \thm~\cite[p.56, thm 2.28]{Rombaldi}: TVI
					\item \application~\cite[p.56]{Rombaldi}: Dichotomie
					\item \thm~\cite[p.40]{Rombaldi} : Prolongement continue
				\end{minipage} \hfill
				\begin{minipage}{.45\textwidth}
					\item \cexemple~\cite{Hauchecorne}: $x \mapsto cos (\frac{1}{x}) ~si~ x \neq 0;~ 1 ~sinon$ 
					\item \application~\cite[p.397]{Kieffer}: Théorème de point fixe
					\item \exemple~\cite[p.40]{Rombaldi} : $x \longmapsto \frac{sin x}{x}$
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Propriétés globales des fonctions continues}
		\begin{small}
			\textblue{\emph{Objectif}: Étendre la notion de continuité (qui est locale) à des propriétés globales sur les fonctions continues.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton~\cite[p.47]{Rombaldi}: Continuité uniforme
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \thm: Heine
				\end{minipage}
				
				\item \remarque~\cite[p.48]{Rombaldi}: Uniforme continuité $\Rightarrow$ continuité mais pas $\Leftrightarrow$
				\item \exemple~\cite[p.48]{Rombaldi}: $x \mapsto \sqrt{x}$ est uniformément continue
				\item \cexemple~\cite[p.47]{Rombaldi}: $x \mapsto x^2$ n'est pas uniformément continue sur $\R$.					
				\item \proposition~\cite[p.48]{Rombaldi}: Fonctions lipschitziennes sont uniformément continue.
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Exemples de fonctions continues}
		\begin{small}
			\textblue{\emph{Objectif}: Donner quelques classes de fonctions remarquable au vue de la continuité.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \thm~\cite[p.44]{Rombaldi} : Fonction monotone et continuité 
				\item \application~\cite[p.44]{Rombaldi} : Fonction monotone et points de discontinuité
				\item \proposition~\cite{Gourdon-analyse} : La convergence uniforme préserve la continuité
				
				\begin{minipage}{.35\textwidth}
					\item \thm~\cite[p. 229]{Gourdon-analyse} : Lemmes de Dini
				\end{minipage} \hfill
				\begin{minipage}{.4362\textwidth}
					\item \textred{\thm ~ \cite[p.225]{Gourdon-analyse} : Weierstrass} 
				\end{minipage}
				
				\item \thm~\cite[p. 157]{Gourdon-analyse} : Continuité sous le signe intégrale 
				\item \cexemple~\cite[p.231]{Hauchecorne}: $f_n : [0, 1] \mapsto \R$ telle que $f_n(x) = x^n$ 
				\item \application : Transformée de Fourier d'une fonction $L^{1}(\R)$ est continue.	
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[II.] \textbf{Dérivabilité}
	\begin{description}
		\item[A.] \emph{Définitions et lien avec la continuité}
		\begin{small}
			\textblue{\emph{Objectif}: Formaliser la notion de dérivabilité et la mettre en relation avec la notion de continuité.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \definiton~\cite[p.69]{Gourdon-analyse}: Dérivabilité en un point, sur un intervalle, fonction dérivable, fonction dérivé
				\item \remarque~\cite[p.70]{Gourdon-analyse}: Dérivabilité $\rightarrow$ continuité
				\item \remarque~\cite[p.70]{Gourdon-analyse}: Fonction dérivée n'est pas forcément continue. 
				\item \exemple: $f(x) = x^{2}\sin(\frac{1}{x})$ si $x \neq 0$ et $0$ sinon. La dérivée de $f$ est non continue en $0$.
				\item \remarque~\cite[p.208]{Rombaldi} : $f$ est dérivable en $a$ $\Leftrightarrow$ $f$ admet un DL d'ordre 1 en $a$
				\item \application~\cite{Rombaldi}: Caractérisation par dérivabilité pour les fonctions lipschitziennes
				\item \proposition~\cite{Rombaldi}: $f$ continue, strictement monotone et dérivable en $a$. La réciproque est dérivable en $f(a) \Leftrightarrow f'(a) \neq 0$ et dans ce cas $(f^{-1})'(f(a))=\frac{1}{f'(a)}$.
				
				\begin{minipage}{.46\textwidth}
					\item \exemple~\cite{Rombaldi}: $x \longmapsto arcsin'(x)$
				\end{minipage} \hfill
				\begin{minipage}{.46\textwidth}
					\item \thm~\cite{Rombaldi}:  Darboux
				\end{minipage}
				\item \application: Recherche de racine de $f'$ par dichotomie.
				
				\begin{minipage}{.46\textwidth}
					\item \thm~\cite{Gourdon-analyse}: Prolongement de dérivée
				\end{minipage} \hfill
				\begin{minipage}{.46\textwidth}
					\item \exemple~\cite{Gourdon-analyse}:  $x \longmapsto \int_{0}^{x}\frac{\sin t}{t}dt$
				\end{minipage}	
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Dérivées d'ordre supérieure}
		\begin{small}
			\textblue{\emph{Objectif} : dériver des fonctions dérivées.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]				
				\begin{minipage}{.5\textwidth}
					\item \definiton~\cite{Rombaldi}: Dérivée d'ordre $n$ + classes $\mathcal{C}^n$ et $\mathcal{C}^{\infty}$
				\end{minipage} \hfill
				\begin{minipage}{.44\textwidth}
					\item \cexemple: Lien avec les DL d'ordre $n$
				\end{minipage}
				\item \exemple: La fonction déterminant est $\mathcal{C}^{\infty}$ car c'est un polynôme.
				\item \application~\cite{Rombaldi}: Caractérisation des fonctions convexes par dérivée seconde
				\item \proposition~\cite{Rombaldi}: Leibniz
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Interprétation géométrique de la dérivée}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.3\textwidth}
					\item \thm: Rolle
					\item \thm: Accroissement
				\end{minipage} \hfill
				\begin{minipage}{.55\textwidth}
					\item \cexemple: $t \longmapsto e^{it}$
					\item \thm: Fondamental de l'analyse $\int_{a}^{b} f(t)dt = f(b)-f(a)$
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[III.] \textbf{Continuité et dérivabilité: des outils puissants}
	\begin{description}
		\item[A.] \emph{Étude variationnelle des fonctions}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.44\textwidth}
					\item \proposition: Signe de la dérivé et monotonie
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \proposition: $f'>0 \Rightarrow$ stricte croissance
				\end{minipage}
				\begin{minipage}{.3\textwidth}
					\item \cexemple: $x \longmapsto x^3$
				\end{minipage} \hfill
				\begin{minipage}{.58\textwidth}
					\item \application: Caractérisation des fonctions convexes par monotonie
				\end{minipage}
				\begin{minipage}{.5\textwidth}
					\item \proposition: Fonction dérivée monotone est continue
				\end{minipage} \hfill
				\begin{minipage}{.35\textwidth}
					\item \proposition: Extremum local
				\end{minipage}
				\begin{minipage}{.44\textwidth}
					\item \cexemple: $w \longmapsto x^3$
					\item \proposition: Règle de l'Hôpital
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \textred{\application: Méthode de Newton}
					\item \exemple: $\lim_{x \rightarrow 0} \frac{sin x}{x}= 1$
				\end{minipage}
				\item \cexemple: $x \longmapsto sin(x^2)$ et $x \longmapsto x$
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Formules de Taylor \cite[p.181]{Rombaldi}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.44\textwidth}
					\item \thm: Formule de Taylor--Lagrange
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \thm: Formule de Taylor--Young
				\end{minipage}
				\item \thm: Formule de Taylor avec reste intégrable
				\item \application~\cite[p.231]{Ouvrard-1}: Théorème central limite
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Quelques résultats de densité}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \thm: Fonction continue partout, dérivable en aucun point 
				\item \application: Densité de ces fonctions dans les fonctions continues
				\item \thm: Densité des fonctions $\mathcal{C}^{n}$ à support compact dans $L^{p}(\R)$ pour $1 \leq p < \infty$.
			\end{itemize}
		\end{footnotesize}
	\end{description}
\end{description}

