\documentclass[a4paper,8pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais,english]{babel}%

\usepackage[dvipsnames]{xcolor}

\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{enumitem}
\usepackage{appendix}
\usepackage{stmaryrd}
\usepackage{amsmath}
\usepackage{wasysym}

\usepackage{listings}
\usepackage{multirow}
\usepackage{algorithm}
\usepackage{algpseudocode}


\parskip=0\baselineskip %0.5 d'habitude


\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\F}{\mathbb{F}}
	\newcommand{\Sn}{\mathfrak{S}}
	\newcommand{\A}{\mathfrak{A}}
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textbluesmall}[1]{{\small{\textcolor{blue}{#1}}}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\theoremstyle{plain}
	\newtheorem{theo}{Théorème}
	\newtheorem{prop}{Proposition}
	\newtheorem{cor}{Corollaire}
	\newtheorem{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem{definition}{Définition}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	
	\title{Leçon 106 : Groupe linéaire d’un espace vectoriel de dimension finie $E$, sous-groupes de $GL(E)$. Applications.}
	
	%\author{Julie Parreaux}
	\date{}
	\maketitle
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
	
	\section*{Motivation}
	
	\subsection*{Défense}
	
	Le groupe linéaire est un des premiers groupes que l'on peut introduire pour parler de théorie des groupes en algèbre linéaire. Il est composé des automorphismes d'un espace vectoriel. On est alors amené à utiliser des notions d'algèbre linéaire pour étudier un groupe.
	
	\subsection*{Ce qu'en dit le jury}
	
	Cette leçon ne doit pas se résumer à un catalogue de résultats épars sur $GL(E)$. Il est important de savoir faire correspondre les sous-groupes du groupe linéaire avec les stabilisateurs de certaines actions naturelles (sur des formes quadratiques, symplectiques, sur des drapeaux, sur une décomposition en somme directe, etc.). On doit présenter des systèmes de générateurs de $GL(E)$ et étudier la topologie de ce groupe en précisant pourquoi le choix du corps de base est important. Les liens avec le pivot de Gauss sont à détailler. Il faut aussi savoir réaliser $\mathfrak{S}_n$ dans $GL_n(K)$ et faire le lien entre signature et déterminant, et entre les classes de conjugaison et les classes de similitude.
	
	S’ils le désirent, les candidats peuvent aller plus loin en remarquant que la théorie des représentations permet d’illustrer l’importance de $GL_n(\C)$ et de son sous-groupe unitaire.
	
	\section*{Métaplan}
	
	\begin{footnotesize}
		\emph{Cadre}: $K$ est un corps et $E$ un $K$-espace vectoriel de dimension finie $n$.
	\end{footnotesize}
	
	\begin{description}
		\item[I.] \textbf{Le groupe linéaire \cite[p.95]{Perrin}}
		
		\begin{small}
			\textblue{Le groupe linéaire est le groupe formé des applications linéaires inversibles dont l'outil matriciel permet son étude.}
		\end{small}
		\begin{description}
			\item[A.] \emph{Groupes $GL(E)$ et $SL(E)$}
				
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\noindent\begin{minipage}{.46\textwidth}
						\item \emph{Définition}: groupe linéaire
						\item \emph{Remarque}: $GL(E) \simeq GL_n(K)$ \textblue{(outil matriciel)}
						\item \emph{Proposition} \cite[p.115]{Gourdon-algebre}: caractérisation de $GL(E)$
						\item \emph{Proposition} \cite[p.208]{Nourdin} : $GL_n(E) \simeq GL_m(E)$
					\end{minipage} \hfill
					\begin{minipage}{.46\textwidth}
						\item \emph{Proposition}: morphisme déterminant
						\item \emph{Définition}: groupe linéaire spécial
						\item \emph{Remarque}: $SL(E) \simeq SL_n(K)$ \textblue{(interprétation)}
						\item \emph{Remarque}: $SL(E)$ distingué dans $GL(E)$
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[B.] \emph{Système de générateurs pour ces groupes}
				
			\begin{small}
				\textblue{On cherche les générateurs les plus simples: ici ils sont donnés par des hyperplans. La méthode du pivot de Gauss est soit une application soit un élément pour exhiber les générateurs.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\noindent\begin{minipage}{.46\textwidth}
						\item \emph{Définition-Proposition}: dilatation
						\item \emph{Remarque}: définition des réflexions
						\item \emph{Définition-Proposition}: transvection
						\item \emph{Proposition}:conjugaison des transvections
					\end{minipage} \hfill
					\begin{minipage}{.46\textwidth}
						\item \emph{Théorème}: générateur de $SL(E)$
						\item \emph{Corollaire}: générateur de $GL(E)$ 
						\item \emph{Application}: pivot de Gauss
						\item \emph{Application}: calcul de l'inverse
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[C.] \emph{Exemple de sous-groupes de $GL(E)$ et de $SL(E)$}
			
			\begin{small}
				\textblue{Deux exemples de sous-groupes: les centres et les sous-groupes finis.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.46\textwidth}
						\item \emph{Théorème}: centre
						\item \emph{Application}: la non-simplicité des groupes
						\item \emph{Remarque}: groupe projectif linéaire
						\item \emph{Définition} \cite[p.207]{Cognet}: matrice de permutation
					\end{minipage} \hfill
					\begin{minipage}{.46\textwidth}
						\item \emph{Remarque} \cite[p.207]{Cognet}: lien entre déterminant et signature
						\item \emph{Théorème}: Cayley
						\item \emph{Corollaire}: un groupe fini $G$ se plonge dans $GL_n(K)$
						\item \emph{Théorème} \cite[p.185]{FrancinouGianellaNicolas-al2} : Burnside
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
		\end{description}
		\item[II.] \textbf{Actions de $GL(E)$}
		
		\begin{small}
			\textblue{$GL(E)$ agit sur de nombreux espaces : les matrices, les groupes; avec plusieurs actions classiques}
		\end{small}
		\begin{description}
			\item[A.] \emph{Actions naturelles}

			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\noindent\begin{minipage}{.46\textwidth}
						\item \emph{Définition}: action transitivement à gauche
						\item \emph{Remarque}: changement de base pour un vecteur
						\item \emph{Définition}: action par conjugaison
					\end{minipage}\hfill
					\begin{minipage}{.46\textwidth}
						\item \emph{Remarque}: changement de base
						\item \emph{Définition}: classe de similitude
						\item \emph{Définition}: invariant
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[B.] \emph{Action par équivalence \cite[p.5]{Caldero-Germoni}}
				
			\begin{small}
				\textblue{Pour toute application $\varphi$ on peut lui associé une représentation matricielle dans une base adéquate. C'est ce que fait cette action (elle nous permet de reformuler le théorème du rang)}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\noindent\begin{minipage}{.46\textwidth}
						\item \emph{Cadre}: $G = GL_n(K) \times GL_n(K)$ où $K$ est un corps
						\item \emph{Définitions}: action; orbite et stabilisateur
					\end{minipage}\hfill
					\begin{minipage}{.46\textwidth}
						\item \emph{Exemple}: $I_{m,n,r}$
						\item \emph{Théorème}: Caractérisation des orbites avec le rang \textblue{(reformuler le théorème du rang)}
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[C.] \emph{Action par congruence \cite[p.250]{Caldero-Germoni}}
				
			\begin{small}
				\textblue{L'action par congruence permet de donner une classification de deux matrices congruentes afin de classer les formes quadratiques.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.46\textwidth}
						\item \emph{Définition}: Groupe agissant par congruence
						\item \emph{Théorème}: Caractérisation des orbites selon le corps
					\end{minipage}\hfill
					\begin{minipage}{.46\textwidth}
						\item \emph{Exemples}: $I_n$ et le groupe orthogonal
						\item \emph{Application}: Loi de réciprocité quadratique 
						\item \emph{Théorème}: Inertie de Sylvester
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			
		\end{description}
		\item[III.] \textbf{Le rôle du corps de base}
		
		\begin{small}
			\textblue{Le corps de base joue un rôle essentiel dans certaines propriétés du groupe linéaire. Nous allons en étudier certaine.}
		\end{small}
		\begin{description}
			\item[A.] \emph{Groupe linéaire fini: corps finis \cite[p.60]{Caldero-Germoni-N2}}
			
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\item \emph{Proposition}: cardinal de $GL_n(\F_q)$
					\item \emph{Théorème}: nombre de matrice diagonalisable sur $GL_n(\F_q)$ \textred{DEV}
					\item \emph{Définition}: groupe spécial projectif
				\end{itemize}
			\end{footnotesize}
			\item[B.] \emph{Groupe linéaire réel et complexe}
			
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.46\textwidth}
						\item \emph{Cadre}: $K = \R$ ou $\C$
						\item \emph{Proposition}: $GL_n(K)$ est dense dans $\mathcal{M}_n(K)$
						\item \emph{Application}: $\chi_{AB} = \chi_{BA}$
						\item \emph{Application}: Différentielle du déterminant
					\end{minipage} \hfill
					\begin{minipage}{.46\textwidth}
						\item \emph{Proposition}: $GL_n(\C)$ est connexe par arcs
						\item \emph{Remarque}:  $GL_n(\R)$ ne l'est pas
						\item \emph{Application}: ensemble des projecteurs de rang $p$ est connexe
						\item \emph{Proposition}: $SL(\C)$ et $SL(\R)$ sont connexes par arcs
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
		\end{description}
		\item[IV.] \textbf{Groupe orthogonal}
		
		\begin{small}
			\textblue{Le groupe orthogonal est un sous-groupe particulier de $GL(E)$. On peut le voir et l'utiliser avec de nombreux points de vus : forme quadratique, action de groupe, orthogonalité, ...}
		\end{small}
		\begin{description}
			\item[A.] \emph{Groupe orthogonal et groupe orthogonal spécial \cite[p.141]{Perrin}}
				
			\begin{small}
				\textblue{Le groupe orthogonal peut également être vu comme le groupe du stabilisateur de $I_n$ par l'action de congruence sur $GL(E)$}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.46\textwidth}
						\item \emph{Définition} \cite[p.259]{Caldero-Germoni}: groupe orthogonal
						\item \emph{Remarque}: sous-groupe de $GL(E)$.
						\item \emph{Proposition}: dimension: $\frac{n(n-1)}{2}$
					\end{minipage} \hfill
					\begin{minipage}{.46\textwidth}
						\item \emph{Proposition}: morphisme déterminant
						\item \emph{Définition}: groupe orthogonal spécial
						\item \emph{Application} \cite[p.259]{Caldero-Germoni}: angle du plan
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[B.] \emph{Décomposition polaire \cite[p.347]{Caldero-Germoni}}
				
			\begin{small}
				\textblue{Le principe diviser pour régner appairait aussi dans les mathématiques: les décompositions de Dunford ou polaire en sont des exemples intéressants : elle permet de régner sur les groupes classiques.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.46\textwidth}
						\item \emph{Théorème}: Compacité de $O_n(\R)$ et $SO_n(\R)$ dans $\mathcal{M}_n(\R)$
						\item \emph{Application}: décomposition polaire est un homéomorphisme
						\item \emph{Corollaire}: spectre d'une matrice inversible
					\end{minipage} \hfill
					\begin{minipage}{.3\textwidth}
						\item \emph{Application}: Sous-groupe compact de $GL_n(\R)$ contenant $O_n(\R)$ est $O_n(\R)$
						\item \emph{Proposition}: Sous-groupe fini de $GL_n(\R)$ conjugué à $O_n(\R)$
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[C.] \emph{Étude du groupe $O_{p,q}$ \cite[p210]{Caldero-Germoni}}
			
			\begin{small}
				\textblue{Le groupe orthogonal peut également être vu comme le groupe du stabilisateur de $I_(p,q)$ par l'action de congruence sur $GL(E)$}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\item \emph{Définition}: $O_{p,q}$
					\item \emph{Théorème}: caractérisation de $O_{p,q}$ \textred{DEV}
					\item \emph{Théorème}: reformulation du théorème d'inertie de Sylvester
				\end{itemize}
			\end{footnotesize}
		\end{description}
	\end{description}
	
	\section*{Quelques notions importantes}
	
	\subsection*{Les groupes linéaire et linéaire spécial}
	\input{./../Notions/Groupes_lineaire.tex}
	
	\subsection*{Pivot de Gauss}
	\input{./../Notions/Algo_PivotGauss.tex}
	
	\subsection*{Actions de groupes}
	\input{./../Notions/ActionsGroupes.tex}
	
	\subsection*{Action de groupe par translation}
	\input{./../Notions/ActionsGroupes_translation.tex}
	
	\subsection*{Action de groupe par conjugaison}
	\input{./../Notions/ActionsGroupes_conjugaison.tex}
	
	\subsection*{Action de groupe par équivalence}
	\input{./../Notions/ActionsGroupes_equivalence.tex}
	
	\subsection*{Action de groupe par congruence}
	\input{./../Notions/ActionsGroupes_congruence.tex}
	
	\subsection*{Groupe linéaire sur corps fini}
	\input{./../Notions/Denombrement_CorpsFini_matrice.tex}
	
%	\subsection*{Topologie du groupe linéaire réel ou complexe}
%	\input{./../Notions/Groupe_lineaire-topologie.tex}
	
	\subsection*{Groupe orthogonal}
	\input{./../Notions/Groupes_orthogonal.tex}
	
	\subsection*{Décomposition polaire et applications}
	\input{./../Notions/Groupes_decompositionPolaire.tex}
	
	\subsection*{Théorèmes d'inerties de Sylvester}
	\input{./../Notions/Groupes_Sylvester.tex}

	
\end{document}