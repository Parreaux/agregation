\chapter*{Leçon 101 : Groupe opérant sur un ensemble. Exemples et applications.}
\addcontentsline{toc}{chapter}{Leçon 101 : Groupe opérant sur un ensemble. Exemples et applications.}
	
\titlebox{blue}{Références pour la leçon}{
	\textblue{\cite{Audin}} Audin, \emph{Géométrie.}
	
	\textblue{\cite{Berhuy}} Berhuy, \emph{Algèbre : le grand combat.}
	
	\textblue{\cite{Boyer}} Boyer, \emph{Algèbre et géométries.}
	
	\textblue{\cite{Caldero-Germoni}} Caldero et Germoni, \emph{Histoires hédonistes de groupes et de géométrie, tome 1.}
	
	\textblue{\cite{Caldero-Germoni2}} Caldero et Germoni, \emph{Histoires hédonistes de groupes et de géométrie, tome 2.}
	
	\textblue{\cite{Caldero-Germoni-N2}} Caldero et Germoni, \emph{Nouvelles histoires hédonistes de groupes et de géométrie, tome 2.}
	
	\textblue{\cite{Combes}} Combes, \emph{Algèbre et géométrie.}
	
	\textblue{\cite{Garnier}} Garnier, \emph{Groupes et géométrie pour l'agrégation.}
	
	\textblue{\cite{Ulmer}} Ulmer, \emph{Théorie des groupes.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.35\textwidth}
		Cardinal du cône nilpotent
	\end{minipage} \hfill
	\begin{minipage}{.5\textwidth}
		Coloriage d'un cube
	\end{minipage}
}
	
\section*{Motivation}

\subsection*{Défense}

Les actions de groupes sont un outil puissant de la théorie des groupes. Elles sont également à la base de la théorie de la géométrie (unification des différentes théorie). Les applications des actions de groupes sont nombreuses : rendre plus facile un problème en géométrie, dénombrer des ensembles finis (coloriage, sur des corps finis, ...)

\subsection*{Ce qu'en dit le jury}

Dans cette leçon, il faut bien dominer les deux approches de l’action de groupe : l’approche naturelle et l’approche via le morphisme du groupe agissant vers le groupe des permutations de l’ensemble sur lequel il agit. La formule des classes et ses applications immédiates sont incontournables. Des exemples de natures différentes doivent être présentés : actions sur un ensemble fini, sur un espace vectoriel (en particulier les représentations), sur un ensemble de matrices, sur des groupes ou des anneaux. Les exemples issus de la géométrie ne manquent pas (groupes d’isométries d’un solide ou d’un polygone régulier). Il est important de savoir calculer des stabilisateurs et des orbites notamment dans le cadre de l’action par conjugaison. Les théorèmes de Sylow peuvent avoir leur place dans cette leçon.

Parmi les applications des actions de groupes, on pourra citer des résultats de dénombrement, comme par exemple la formule de Lucas qui permet de calculer efficacement les cœfficients binomiaux.

S’ils le désirent, les candidats peuvent aller plus loin en décrivant les actions naturelles de $PGL(2, \mathbb{F}_q)$
sur la droite projective, ou de $SL_2(\Z)$ sur le demi-plan de Poincaré

En notant que l’injection du groupe de permutations dans le groupe linéaire par les matrices de permutations donne lieu à des représentations, ils pourront facilement en déterminer le caractère.

\section*{Métaplan}

\begin{description}
	\item[I.] \textbf{Actions de groupe \cite[p.171]{Berhuy}}
	\begin{small}
		\textblue{Les actions de groupes permettent d'étudier et de représenter les éléments d'un groupe en étudiant des sous-groupe ou des ensembles induits par l'action du groupe.}
	\end{small}
	\begin{footnotesize}
		\begin{itemize}[label=$\rightsquigarrow$]
			\item \emph{Définitions}: Action de groupe + Morphisme \textbrown{(Une action n'est à priori pas un morphisme!)}
			\item \emph{Définition}: Action de groupe fidèle (via l'action et du morphisme)
			\item \emph{Exemples}: Action triviale \textblue{(le noyau de l'action est le noyau du morphisme sous-jacent est l'ensemble (= éléments qui agissent trivialement))}; action par permutation; action par multiplication dans $\Z$
			\item \emph{Définitions}: Stabilisateur + action libre + exemples via les précédents
			\item \emph{Propositions}~\cite[p.15]{Caldero-Germoni}: Propriété du stabilisateur 
			\item \emph{Propositions}\cite[p.31]{Ulmer}: Caractérisation du noyau \textblue{(caractérisation de la fidélité)}
			\item \emph{Lemme}: Existence d'une relation d'équivalence 
			\item \emph{Définitions}: Orbite + action transitive + points fixes + exemples via les précédents
			\item \emph{Proposition} \cite[p.16]{Caldero-Germoni}: Caractérisation des actions transitives
		\end{itemize}
	\end{footnotesize}
	\item[II.] \textbf{Action de groupes sur un ensemble fini}
	\begin{small}
		\textblue{Les notions d'orbites et de stabilisateurs tels que nous les avons présenté donnent des propriétés intéressantes sur le groupe ou sur l'ensemble. Plus particulièrement, si au moins un de ceux-ci est fini, nous avons du dénombrement.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Formule des classes et conséquences}
		\begin{small}
			\textblue{Les orbites forment une partitions de l'ensemble. Si l'ensemble est fini, on peut en déduire des propriétés sur son cardinal.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.44\textwidth}
					\item \emph{Proposition}~\cite{Ulmer}: Lien entre orbite et stabilisateur 
				\end{minipage} \hfill
				\begin{minipage}{.36\textwidth}
					\item \emph{Lemme}~\cite[p.175]{Berhuy}: Équation aux classes 
				\end{minipage}
				\item \emph{Application}~\cite[p.57]{Caldero-Germoni-N2}: Dénombrement de matrice sur un corps fini : matrice inversibles, groupe spécial linéaire, famille libre (et donc d'application injective), endomorphisme diagonalisable.
				
				\begin{minipage}{.46\textwidth}
					\item \textred{\emph{Application}~\cite{Caldero-Germoni2}: Cône nilpotent}
					\item \emph{Application}~\cite[p.138]{Ulmer}: Les sous-groupe de $SO_3(\R)$
				\end{minipage} \hfill
				\begin{minipage}{.34\textwidth}
					\item \emph{Proposition}: Formule de Burnside
					\item \emph{Application}~\cite[p.44]{Combes}: Collier de perles 
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Étude des $p$-groupes via les actions de groupes \cite[p.69]{Ulmer}}	
		\begin{small}
			\textblue{Les actions de groupes sont bien pratiques pour étudier les $p$-groupes qui sont une classe de groupe fini particulière.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.42\textwidth}
					\item \emph{Définition}: $p-$groupe 
					\item \emph{Théorème}: Théorème de Cauchy
					\item \emph{Théorème}: Caractérisation des $p-$groupes
					\item \emph{Application}: $p^2$ toujours abélien
				\end{minipage} \hfill
				\begin{minipage}{.46\textwidth}
					\item \emph{Proposition} : Points fixe d'un $p$-groupe
					\item \emph{Exemples}: $G = \{e\}$; $D_4$
					\item \emph{Proposition}: centres des $p-$groupes
					\item \textblue{\textbf{Culture générale}: \emph{Théorème}: Théorèmes de Sylow }
				\end{minipage}	
			\end{itemize}
		\end{footnotesize}	
	\end{description}
	\item[III.] \textbf{Actions d'un groupe sur lui-même}
	\begin{small}
		\textblue{Les actions d'un groupe sur lui-même peuvent être correctement caractérisée: elles se classent par catégorie.}
	\end{small}
	\begin{description}	
		\item[A.] \emph{Action par translation \cite[p.31]{Ulmer}}
		\begin{small}
			\textblue{Les actions par translations permettent de facilité l'étude des groupes finis en diminuant leur taille: on montre qu'ils sont isomorphes à un groupe de taille inférieur.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Action par translation à gauche
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Remarque}: Même chose à droite
				\end{minipage}	
				
				\item \emph{Remarque}: Dans le cas de matrices, on change la base d'un vecteur
				\item \emph{Exemple}: $G$ opère sur $\mathcal{P}(G)$
				\item \emph{Exemple}: $G$ opère sur $G \setminus H$ \textblue{(en pratique: étude des groupe fini sur ordinateur)}
				\item \emph{Proposition}: $\mathrm{Stab}(G) = \{e\}$; l'action est fidèle, libre et transitive
				\item \emph{Application}: Théorème de Cayley
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Action par conjugaison \cite[p.33]{Ulmer}}
		\begin{small}
			\textblue{Dans le cadre de groupe sur les matrices, l'action par conjugaison permet de définir des classes de similitudes et des invariants. De plus, on définit grâce à celle-ci la notion de matrices équivalente.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \emph{Définition}: Groupe agissant par conjugaison + centralisateur
				\item \emph{Définition}: Dans le cas d'un groupe de matrice : changement de base
				\item \emph{Proposition}: Action jamais libre mais fidèle et transitive que si $G = \{e\}$
				
				\begin{minipage}{.45\textwidth}
					\item \emph{Proposition}: Caractérisation du centralisateur
				\end{minipage} \hfill
				\begin{minipage}{.45\textwidth}
					\item \emph{Définition}: Éléments conjugués 
				\end{minipage}
				
				\begin{minipage}{.3\textwidth}
					\item \emph{Définition}: Normalisateur
				\end{minipage} \hfill
				\begin{minipage}{.6\textwidth}
					\item \emph{Proposition}: Caractérisation d'un sous-groupe distingué
				\end{minipage}
				
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Classe de similitude 
					\item \emph{Définition}: Invariant
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Exemple} : $GL_n(\C)$
					\item \emph{Exemple}: spectre 
				\end{minipage}
				
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[IV.] \textbf{Actions sur un ensemble de matrices}
	\begin{small}
		\textblue{Il existe des actions groupes particulière sur les matrices nous permettant de caractériser un nombre important de propriétés.}
	\end{small}
	\begin{description}	
		\item[A.] \emph{Action par équivalence \cite[p.5]{Caldero-Germoni}}
		\begin{small}
			\textblue{Pour toute application $\varphi$ on peut lui associé une représentation matricielle dans une base adéquate. C'est ce que fait cette action (elle nous permet de reformuler le théorème du rang)}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.48\textwidth}
					\item \emph{Cadre}: $G = GL_n(K) \times GL_n(K)$ où $K$ est un corps
				\end{minipage} \hfill
				\begin{minipage}{.46\textwidth}
					\item \emph{Définitions}: action; orbite et stabilisateur
				\end{minipage}
				
				\item \emph{Exemple}: $I_{m,n,r}$
				\item \emph{Théorème}: Caractérisation des orbites avec le rang \textblue{(reformuler le théorème du rang)}	
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Action par congruence \cite[p.250]{Caldero-Germoni}}
		\begin{small}
			\textblue{L'action par congruence permet de donner une classification de deux matrices congruentes afin de classer les formes quadratiques.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.46\textwidth}
					\item \emph{Définition}: Groupe agissant par congruence
					\item \emph{Exemples}: $I_n$ et le groupe orthogonal
				\end{minipage} \hfill
				\begin{minipage}{.46\textwidth}
					\item \emph{Théorème}: Caractérisation orbites selon corps
					\item \emph{Application}: Loi de réciprocité quadratique 
				\end{minipage}	
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[V.] \textbf{Application à la géométrie}
	\begin{small}
		\textblue{La première application des actions de groupes est en géométrie: elles ont permis l'unification de la théorie.}
	\end{small}
	\begin{description}	
		\item[A.] \emph{Isométrie \cite[p.52]{Audin}}
		\begin{small}
			\textblue{Les isométries sont des applications sur un espace que l'on définie par les actions de groupes. De plus, on les utilisent pour caractériser le groupe des isométries dans certains cas.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \emph{Définition} \cite[p.108]{Boyer}: espace affine via les actions de groupe
				\item \emph{Définition}: groupe isométrie + isométrie positive \textblue{(conservation de l'orientation)}
				
				\begin{minipage}{.44\textwidth}
					\item \emph{Théorème}: générateur du groupe des isométrie 
				\end{minipage} \hfill
				\begin{minipage}{.36\textwidth}
					\item \emph{Application}: les isométrie en dimension 3
				\end{minipage}
				\item \emph{Théorème} \cite{Caldero-Germoni}: Caractérisation des isométrie du tétraèdre
				
				\begin{minipage}{.48\textwidth}
					\item \emph{Théorème} \cite{Caldero-Germoni}: Caractérisation des isométrie du cube 
				\end{minipage} \hfill
				\begin{minipage}{.32\textwidth}
					\item \textred{\emph{Application} \cite{Caldero-Germoni}: Coloriage du cube}
				\end{minipage}				
			\end{itemize}
		\end{footnotesize}
	
		\item[B.] \emph{Angles \cite[p.313]{Garnier}}
		\begin{small}
			\textblue{On sait depuis très longtemps manipuler les angles: mais comment définir cette notion? Les actions de groupes et les rotations nous permettent de définir des objets que nous ne connaissons pas bien.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \emph{Définition}: Ensembles des demi-droites $\mathcal{D}$ et des vecteurs unitaires $\mathcal{U}$
				\item \emph{Proposition}: Ces ensembles sont en bijection
				\item \emph{Théorème}: Le groupe $O^+(\mathcal{P})$ agit simplement transitivement sur $\mathcal{U}$ et $\mathcal{UD}$
				\item \emph{Définition}: relation d'équivalence issues de ces actions + notions d'angle
				\item \emph{Théorème}: Bijection canonique sur l'ensemble des angles
				\item \emph{Définition}: Groupe des angle de demi-droites vectorielles \textbrown{\textbf{Attention}: l n'existe pas de groupe des angles : ils sont toujours affiliés à un sous-groupe}
				\item \emph{Théorème}: théorème fondamental de la mesure des angles.
				\item \emph{Application}: angle géométrique et somme des angles d'un triangle
			\end{itemize}
		\end{footnotesize}
	\end{description}
\end{description}

