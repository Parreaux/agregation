\chapter*{Leçon 239: Fonctions définies par une intégrale dépendant d'un paramètre.}
\addcontentsline{toc}{chapter}{Leçon 239: Fonctions définies par une intégrale dépendant d'un paramètre.}

\titlebox{blue}{Références pour la leçon}{
	\textblue{\cite{ArnaudiesFraysse-2}} Arnaudies et Fraysse, \emph{Analyse, tome 2.}
	
	\textblue{\cite{BeckMalikPeyre}} Beck, Malik et Peyre, \emph{Objectif agrégation.}
		
	\textblue{\cite{BriancePages}} Briance et Pagès, \emph{Théorie de l'intégration.}
	
	\textblue{\cite{Gourdon-analyse}} Gourdon, \emph{Analyse.}
	
	\textblue{\cite{Hauchecorne}} Hauchecorne, \emph{Les contres-exemples en mathématiques.}
	
	\textblue{\cite{Nourdin}} Nourdin, \emph{Agrégation de mathématiques épreuve orale.}
	
	\textblue{\cite{Rudin}} Rudin, \emph{Principe d'analyse mathématiques.}
	
	\textblue{\cite{ZuilyQueffelec2}} Queffelec et Zuily, \emph{Éléments d'analyse pour l'agrégation.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.35\textwidth}
		Caractérisation de $\Gamma$
	\end{minipage} \hfill
	\begin{minipage}{.5\textwidth}
		Théorème de Weierstrass
	\end{minipage}
}

\section*{Motivation}

\subsection*{Défense}

\emph{Application}: Définir de nouvelles fonctions.

\subsection*{Ce qu'en dit le jury}

Les candidats incluent les théorèmes de régularité (version segment — a minima — mais aussi version « convergence dominée ») ce qui est pertinent mais la leçon ne doit pas se réduire seulement à cela. Cette leçon doit être riche en exemples, ce qui parfois n’est pas suffisamment le cas, et peut être encore enrichie par des études et méthodes de comportements asymptotiques. Les propriétés de la fonction $\Gamma$ d’Euler fournissent un développement standard, mais non sans risque (on pourra y inclure le comportement asymptotique, voire son prolongement analytique) ; certains candidats sont trop ambitieux pour le temps dont ils disposent. Le jury invite donc à bien préciser ce que le candidat souhaite montrer pendant son développement. Les différentes transformations classiques (Fourier, Laplace,... ) relèvent aussi naturellement de cette leçon. On peut en donner des applications pour obtenir la valeur d’intégrales classiques (celle de l’intégrale de Dirichlet par exemple). Le théorème d’holomorphie sous le signe intégrale est trop peu souvent cité.

Pour aller encore plus loin, on peut par exemple développer les propriétés des transformations mentionnées (notamment la transformée de Fourier, par exemple en s’attardant sur le lien entre régularité de la fonction et décroissance de sa transformée de Fourier), ainsi que de la convolution.

\section*{Métaplan}

\begin{description}
	\item[I.] \textbf{Régularité des intégrales à paramètres}
	\begin{description}
		\item[A.] \emph{Continuité \cite[p.138]{BriancePages}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.25\textwidth}
					\item \thm: Continuité
				\end{minipage} \hfill
				\begin{minipage}{.6\textwidth}
					\item \exemple~\cite[p.442]{ArnaudiesFraysse-2}: Conséquence de $f$ continue et existence de $\int_0^{\infty} f$
				\end{minipage}
				\begin{minipage}{.45\textwidth}
					\item \cexemple~\cite[p.					224]{Hauchecorne}: $\int f$ non continue
				\end{minipage} \hfill
				\begin{minipage}{.45\textwidth}
					\item \cexemple~\cite[p.					224]{Hauchecorne}: si $f$ continue
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Régularité d'ordre supérieur}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.52\textwidth}
					\item \thm~\cite[p.140]{BriancePages}: Dérivabilité
					\item \cexemple~\cite[p.224]{Hauchecorne}: $f$ $\Ck^1$ tel que $\int f$ non définie
					\item \exemple~\cite[p.444]{ArnaudiesFraysse-2}: Dérivée de fonctions usuelles
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \remarque: Extension à $\Ck^{\infty}$
					\item \exemple~\cite[p.442]{ArnaudiesFraysse-2}: $\int_{0}^{\infty} e^{-at}f(t)dt$
					\item \application~\cite[p.163]{Gourdon-analyse}: Calcul de Gauss
				\end{minipage}
				\item \exemple~\cite[p.444]{ArnaudiesFraysse-2}: Calcul de $\int_{0}^{\infty} \frac{1 - \cos xt}{t^2}dt$
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[II.] \textbf{Étude d'intégrales à paramètres particulières}
	\begin{description}
		\item[A.] \emph{La fonction $\Gamma$}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.45\textwidth}
					\item \definiton~\cite[p.159]{Gourdon-analyse}: Fonction Gamma, $\Gamma$
					\item \textred{\thm~\cite[p.179]{Rudin}: Caractérisation sur $\R$}
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \proposition~\cite[p.159]{Gourdon-analyse}: Propriétés de $\Gamma$
					\item \application~\cite[p.179]{Rudin}: $\Gamma = \underset{n \to \infty}{\lim} \frac{n!x^n}{x(x + 1) \dots (x + n)}$
				\end{minipage} 
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Transformée de Fourier \cite[p.327]{ZuilyQueffelec2}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.42\textwidth}
					\item \definiton: Transformée de Fourier
					\item \exemple: $e^{-x^2}$
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \thm: Régularité de $\hat{f}$
					\item \thm: Inversion
				\end{minipage}
				\item \application: Fonction caractéristique
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[III.] \textbf{Convolution \cite[p.113]{BeckMalikPeyre}}
	\begin{small}
		\textblue{But: régulariser des fonctions. Remarque: les théorèmes de densité de déduisent de la convolution.}
	\end{small}
	\begin{footnotesize}
		\begin{itemize}
			\item \definiton: Convolution si elle existe
		\end{itemize}
	\end{footnotesize}
	\begin{description}
		\item[A.] \emph{Existence de la convolution}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.45\textwidth}
					\item \proposition: Convolution dans $L^1$
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \application: Convolution de $\hat{f}$
				\end{minipage}
				\item \proposition: Convolution dans $L^p$ et $L^q$
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Régularisation de fonctions}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.4\textwidth}
					\item \thm: Convolution et dérivation
					\item \application: Convolution par Gaussienne
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \application: Si $f \in \Ck^{\infty}$ alors $f * g \in \Ck^{\infty}$
					\item \application: Équation de la chaleur
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Approximation de fonctions}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.46\textwidth}
					\item \definiton: Approximation de l'identité
					\item \textred{\thm~\cite[p.284]{Gourdon-analyse}: Théorème de Weierstrass}
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \exemple: Dilatation
					\item \application~\cite[p.3]{Nourdin}: Noyau de Fejer
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
\end{description}

