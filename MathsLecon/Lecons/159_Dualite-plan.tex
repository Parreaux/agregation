\chapter*{Leçon 159: Formes linéaires et dualité en dimension finie. Exemples et applications.}
\addcontentsline{toc}{chapter}{Leçon 159: Formes linéaires et dualité en dimension finie. Exemples et applications.}

\titlebox{blue}{Références pour la leçon}{
	\textblue{\cite{Avez}} Avez, \emph{Calcul différentiel.}
	
	\textblue{\cite{BeckMalikPeyre}} Beck, Malik et Peyre, \emph{Objectif agrégation.}
	
	\textblue{\cite{Cognet}} Cognet, \emph{Algèbre linéaire.}
	
	\textblue{\cite{Cognet-bilineaire}} Cognet, \emph{Algèbre bilinéaire.}
	
	\textblue{\cite{FrancinouGianellaNicolas-al1}} Francinou, Gianella et Nicolas, \emph{Oraux X-ENS, Algèbre 1.}
	
	\textblue{\cite{Gourdon-algebre}} Gourdon, \emph{Algèbre.}
	
	\textblue{\cite{Grifone}} Grifone, \emph{Algèbre linéaire.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.35\textwidth}
		Méthode de Kackmarz
	\end{minipage} \hfill
	\begin{minipage}{.5\textwidth}
		Théorème des extrema liés
	\end{minipage}
}

\section*{Motivation}

\subsection*{Défense}

\begin{itemize}
	\item Pourquoi étudier le dual? Structure d'un espace vectoriel et son dual sont très liés. De plus, les hyperplans donnent une interprétation géométrique des formes linéaires
\end{itemize}

\subsection*{Ce qu'en dit le jury}

Il est important de bien placer la thématique de la dualité dans cette leçon ; celle-ci permet de mettre en évidence des correspondances entre un morphisme et son morphisme transposé, entre un sous-espace et son orthogonal (canonique), entre les noyaux et les images ou entre les sommes et les intersections. Bon nombre de résultats d’algèbre linéaire se voient dédoublés par cette correspondance. Les liens entre base duale et fonctions de coordonnées doivent être parfaitement connus. Le passage d’une base à sa base duale ou antédualte, ainsi que les formules de changement de base, doivent être maîtrisés. On pourra s’intéresser aux cas spécifiques où l’isomorphisme entre l’espace et son dual est canonique (cas 	euclidien, cas des matrices).

Savoir calculer la dimension d’une intersection d’hyperplans via la dualité est important dans cette leçon. L’utilisation des opérations élémentaires sur les lignes et les colonnes permet facilement d’obtenir les équations d’un sous-espace vectoriel ou d’exhiber une base d’une intersection d’hyperplans.

Cette leçon peut être traitée sous différents aspects : géométrique, algébrique, topologique ou analytique. Il faut que les développements proposés soient en lien direct avec la leçon. Enfin rappeler que la différentielle d’une fonction à valeurs réelles est une forme linéaire semble incontournable.

Il est possible d’illustrer la leçon avec un point de vue probabiliste, en rappelant que la loi d’un vecteur aléatoire $X$ est déterminée par les lois unidimensionnelles de $X . u$ pour tout vecteur $u$.

\section*{Métaplan}

\begin{description}
	\item[I.] \textbf{Formes linéaires}
	\begin{description}
		\item[A.] \emph{Formes linéaires \cite[p.126]{Gourdon-algebre}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.45\textwidth}
					\item \definiton: Formes linéaires
					\item \proposition: Décomposition via la base de $E$
					\item \exemple: Forme linéaire coordonné 
					\item \application~\cite[p.2]{BeckMalikPeyre}: Application différentiable
				\end{minipage} \hfill
				\begin{minipage}{.38\textwidth}
					\item \definiton: Espace dual
					\item \exemple: Trace
					\item \application: Ensemble connexe de $O_n(\R)$
					\item \exemple: Cas de l'ordre $1$
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Hyperplan \cite[p.92]{Cognet}}
		\begin{small}
			\textblue{Les hyperplans donnent une interprétation géométrique des formes linéaires. De plus, résoudre un système linéaire revient à caractériser une intersection d'hyperplans.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Hyperplan
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \remarque: Dimension d'un hyperplan
				\end{minipage}
				\item \proposition: Caractérisation de la proportionnalité
				\item \remarque: Résolution de système d'équations
				
				\begin{minipage}{.4\textwidth}
					\item \corollaire: Équation d'un hyperplan
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \textred{\application: Méthode de Kaczmarz}
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[II.] \textbf{Dualité \cite[p.126]{Gourdon-algebre}}
	\begin{small}
		\textblue{Nous souhaitons caractériser les espaces duaux et biduaux. Pour cela, nous définissons leurs base. Une base duale interviennent dans la décomposition des formes de Gauss. Elles permettent également de transporter un objet dans son espace dual. Le bidual permet l'opération "inverse" puisqu'il nous permet de retourner dans un espace isomorphe à l'espace de départ.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Base duale}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Base duale
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \exemple: Base duale sur $K_n[X]$.
				\end{minipage}
				\item \proposition~\cite[p.85]{Grifone}: $E$ et $E^*$ sont isomorphes
				\item \remarque~\cite[p.85]{Grifone}: Isomorphisme non unique
				
				\begin{minipage}{.4\textwidth}
					\item \thm ~\cite[p.103]{BeckMalikPeyre}: Théorème Riesz
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \thm ~\cite[p.329]{FrancinouGianellaNicolas-al1}: Dual de $\mathcal{M}_n(K)$
				\end{minipage}
				\item \application ~\cite[p.329]{FrancinouGianellaNicolas-al1}: Caractérisation du centre
				\item \application ~\cite[p.329]{FrancinouGianellaNicolas-al1}: Hyperplans intersectent $GL_n$
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Bidual}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Bidual
					\item \definiton: Base antéduale
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \proposition: $E \simeq E^{**}$
					\item \proposition: Unicité de la base antéduale
				\end{minipage}
				\item \application \cite[p.154]{BeckMalikPeyre}: Polynôme de Lagrange
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[III.] \textbf{Orthogonalité}
	\begin{small}
		\textblue{L'orthogonalité est une notion importante qui permet de simplifier des calcul comme nous l'avons étudier dans la méthode de Kaczmarz. On peut définir cette notion de plusieurs manière (depuis tout petit nous la définissons via les produits scalaires). Nous allons alors étudier les liens entres toutes ces définitions.}
	\end{small}
	\begin{description}
		\item[A.] \emph{L'orthogonalité via la dualité \cite[p.127]{Gourdon-algebre}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.25\textwidth}
					\item \definiton: Orthogonal
				\end{minipage} \hfill
				\begin{minipage}{.55\textwidth}
					\item \remarque: Coïncidence avec l'orthogonalité euclidienne (Riesz)
				\end{minipage}
				\begin{minipage}{.4\textwidth}
					\item \proposition: Caractérisation via la base
					\item \thm: Dimension et orthogonalité
					\item \textred{\application ~\cite[p.103]{Avez}: Extremums liés}
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \proposition: Propriétés de ces orthogonal
					\item \proposition: Lien avec les hyperplan
					\item \thm: Invariant de similitude
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Lien avec l'orthogonalité via les formes quadratiques \cite[p.24]{Cognet-bilineaire}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Formes quadratiques
					\item \definiton: Groupe orthogonal
					\item \definiton: Noyau
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \proposition: Définition de la forme polaire
					\item \remarque: Lien avec celui du dual
					\item \proposition: Lien avec la dimension
				\end{minipage} 
				\item \application: Décomposition de Gauss
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[IV.] \textbf{Application transposée \cite[p.129]{Gourdon-algebre}}
	\begin{small}
		\textblue{L'application transposée est une application définie sur un espace dual. Elle permet également de définir un morphisme des applications linéaires vers les applications linéaires sur les espaces duaux. On s'intéresse alors à ces propriétés et à sa représentation matricielle. Comme on le vera, elle est également très liée à la notion d'orthogonalité.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Définition de la transposée }
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Transposée
					\item \proposition: Représentation matricielle
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \remarque: Morphisme
					\item \application: $^t(^t u) = u$
				\end{minipage}
				\item \proposition: Propriétés linéaires, composition et inverse
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Lien avec l'orthogonalité}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.45\textwidth}
					\item \proposition: Image, noyaux, rang de la transposée
					\item \application: Stabilité des vecteurs propres
				\end{minipage} \hfill
				\begin{minipage}{.35\textwidth}
					\item \proposition: Stabilité via la transposée
					\item \application: Co-trigonalisation
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
\end{description}
	