\documentclass[a4paper,8pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais,english]{babel}%

\usepackage[dvipsnames]{xcolor}

\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{enumitem}
\usepackage{appendix}
\usepackage{stmaryrd}
\usepackage{amsmath}
\usepackage{wasysym}

\usepackage{listings}
\usepackage{multirow}
\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}
\algnewcommand{\LineComment}[1]{\State \(\triangleright\) #1}

\parskip=0\baselineskip %0.5 d'habitude


\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\F}{\mathbb{F}}
	\newcommand{\Sn}{\mathfrak{S}}
	\newcommand{\A}{\mathfrak{A}}
	
	\newcommand{\thm}{\emph{Théorème}}
	\newcommand{\proposition}{\emph{Proposition}}
	\newcommand{\lem}{\emph{Lemme}}
	\newcommand{\corollaire}{\emph{Corollaire}}
	\newcommand{\definiton}{\emph{Définition}}
	\newcommand{\remarque}{\emph{Remarque}}
	\newcommand{\application}{\emph{Application}}
	\newcommand{\exemple}{\emph{Exemple}}
	\newcommand{\cexemple}{\emph{Contre-exemple}}
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textbluesmall}[1]{{\small{\textcolor{blue}{#1}}}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\sloppy
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\theoremstyle{plain}
	\newtheorem{theo}{Théorème}
	\newtheorem{prop}{Proposition}
	\newtheorem{cor}{Corollaire}
	\newtheorem{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem{definition}{Définition}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	
	\title{Leçon 203: Utilisation de la notion de compacité.}
	
	%\author{Julie Parreaux}
	\date{}
	\maketitle
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
	
	\section*{Motivation}
	
	\subsection*{Défense}
	
	\begin{itemize}
		\item Notion topologique importante dans le cas métrique.
		\item Permet de ramener des raisonnements de type fini sur des ensembles infinis.
		\item Permet de passer certaines propriétés du local au global, c'est-à-dire qu'une propriété vraie au voisinage de chaque point devient valable de façon uniforme sur tout le compact. 
		\item Donne quelques résultats d'existences.
	\end{itemize}

	
	\subsection*{Ce qu'en dit le jury}
	
	Il est important de ne pas concentrer la leçon sur la compacité en général et d’éviter la confusion entre utilisation de la notion compacité et notion de compacité. Le jury recommande vivement de rester en priorité dans le cadre métrique. Néanmoins, on attend des candidats d’avoir une vision synthétique de la compacité. Des exemples d’applications comme le théorème de Heine et le théorème de Rolle doivent y figurer et leur démonstration être connue. Par ailleurs, le candidat doit savoir quand la boule unité d’un espace vectoriel normé est compacte. Des exemples significatifs d’utilisation comme le théorème de Stone-Weierstrass (version qui utilise la compacité), des théorèmes de point fixe, voire l’étude qualitative d’équations différentielles, sont tout à fait envisageables. Le rôle de la compacité pour des problèmes d’existence d’extrema mériterait d’être davantage étudié. On peut penser ensuite à des exemples en dimension $n \geq 2$.
	
	Pour aller plus loin, les familles normales de fonctions holomorphes fournissent des exemples fondamentaux d’utilisation de la compacité. Les opérateurs auto-adjoints compacts sur un espace de Hilbert relèvent également de cette leçon, et on pourra développer l’analyse de leurs propriétés spectrales.
	
	\section*{Métaplan}
	
	\begin{footnotesize}
		\emph{Cadre}: $E$ est un espace métrique
	\end{footnotesize}
	
	\begin{description}
		\item[I.] \textbf{Rappel sur la notion de compacité et premières applications}
		
		\begin{small}
			\textblue{Cette section a pour but de poser les premières définitions de la compacité ainsi que ces première propriétés. Nous en donnons également quelques applications rapide: elle n'a pas vocation à être le coeur de la leçon.}
		\end{small}
		\begin{description}
			\item[A.] \emph{Propriété de Borel--Lebesgue \cite[p.33]{SaintRaymond}}
			
			\begin{small}
				\textblue{Une première caractérisation de la compacité se fait par la propriété de Borel--Lebesgue. Nous donnons quelques caractérisations de compacts qui en découle directement.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.3\textwidth}
						\item \definiton: Recouvrement
						\item \definiton: Espace compact
						\item \exemple~\cite[p.27]{Gourdon-analyse}: $[0, 1]$
						\item \remarque: Cas non métrique
						\item \cexemple~\cite[p.302]{Hauchecorne}: Espace vérifiant la propriété et non compact
					\end{minipage} \hfill
					\begin{minipage}{.5\textwidth}
						\item \application: Ensemble des valeurs d'une suite et limite : compact
						\item \proposition~\cite[p.229]{TeytaudAntoniniBorgnatChateauLebeau}: Compact implique borné
						\item \application~\cite[p.27]{Gourdon-analyse}: Utilisation de la contraposé comme caractérisation
						\item \proposition~\cite[p.28]{Gourdon-analyse}: Réunion finie: compacte
						\item \application: Ensemble fini est compact
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[B.] \emph{Caractérisation séquentielle de la compacité \cite[p.28]{Gourdon-analyse}}
			
			\begin{small}
				\textblue{La propriété de Borel--Lebesgue est équivalente à la caractérisation séquentielle dans le cas métrique: c'est ce que nous appelons le théorème de Bolzano-Weierstrass. Ce point de vu nous donne de nouvelles applications.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \thm: Bolzano--Weierstrass
						\item \application: Caractérisation de limite via l'exponentielle
						\item \remarque: Cas des espaces non métrique
						\item \proposition: Unicité de la valeur d'adhérence
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \application~\cite[p.8]{Nourdin}: Convergence en loi de variables gaussiennes
						\item \thm: Théorème de Tychonoff (dénombrable)
						\item \remarque: Le théorème est vrai dans le cas non-dénombrable
						\item \application: Compacts dans $\R^n$
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[C.] \emph{Caractérisation par les fermés \cite[p.34]{SaintRaymond}}
			
			\begin{small}
				\textblue{La propriété de Borel--Lebesgue possède une propriété dual permettant de caractériser les espaces compacts grâce à des fermés.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.44\textwidth}
						\item \thm: Caractérisation par les fermés
						\item \thm: Sous-espace fermé d'un compact est compact
						\item \thm: Compact sont fermés
						\item \cexemple~\cite[p.302]{Hauchecorne}: Compact non fermé
					\end{minipage} \hfill
					\begin{minipage}{.36\textwidth}
						\item \application~\cite[p.229]{TeytaudAntoniniBorgnatChateauLebeau}: $O_n(\R)$ et $SO_n(\R)$ sont compacts
						\item \thm: Surjectivité des applications
						\item \application: Compacts dans $\R$
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
		\end{description}
		\item[II.] \textbf{La compacité: une vue globale de ce qui est local}
		
		\begin{small}
			\textblue{La compacité permet d'adopter un point de vu fini sur un ensemble infini. Elle permet donc de transformée des propriétés locales propriétés globales}
		\end{small}
		\begin{description}
			\item[A.] \emph{Continuité uniforme}
			
			\begin{small}
				\textblue{La continuité uniforme (consistant à l'inversion des bons quantificateurs) transforme la notion de continuité qui est locale en une notion globale.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \definiton~\cite[p.28]{SaintRaymond}: Continuité uniforme
						\item \thm~\cite[p.43]{SaintRaymond}: Théorème de Heine
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \application: Approximation de fonctions continues par des suites de fonctions affines
						\item \thm~\cite[p.8]{Nourdin}: Deuxième théorème de Dini
						\item \application~\cite[p.8]{Nourdin}: Théorème de Glivenko--Cantelli
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[B.] \emph{Approximation de fonctions continues sur un compact ~\cite[p.8]{Nourdin}}
			
			\begin{small}
				\textblue{L'approximation de fonction permet de donner des propriétés de régularité à une fonction.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \thm: Premier théorème de Dini
						\item \application: Approximation uniforme via des polynômes : $t \mapsto \sqrt{t}$ sur $[0, 1]$
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \definiton~\cite[p.284]{Gourdon-analyse}: Approximation de l'unité
						\item \thm~\cite[p.284]{Gourdon-analyse}: Théorème de Weierstrass \textred{(DEV)}
						\item \application: Théorème de Féjer
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
		\end{description}
		\item[III.] \textbf{Compacité et problème d'existences}
		
		\begin{small}
			\textblue{La compacité permet d'exhiber des problèmes d'existences.}
		\end{small}
		\begin{description}
			\item[A.] \emph{Recherche d'extremum \cite[p.42]{SaintRaymond}}
			
			\begin{small}
				\textblue{La recherche d'extremum sur un compact est plus facile car nous avons la garantie de l'existence de cet extremum.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.44\textwidth}
						\item \thm: Image d'un compact est un compact
						\item \application: Continue sur compact atteint ses bornes
						\item \application: Cas réel
						\item \proposition~\cite[p.156]{SaintRaymond}: Théorème de Rolle
						\item \proposition~\cite[p.156]{SaintRaymond}: Théorème des accroissements finis
						\item \proposition~\cite[p.44]{Testard}: Équivalence des normes
					\end{minipage} \hfill
					\begin{minipage}{.38\textwidth}
						\item \application~\cite[p.99]{SaintRaymond}: Théorème de Riesz
						\item \application: Caractérisation des compacts
						\item \definiton~\cite[p.44]{Testard}: Fonction coersive
						\item \proposition~\cite[p.44]{Testard}: Borne d'une telle fonction
						\item \application~\cite[p.44]{Testard}: Théorème de d'Alembert
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[B.] \emph{Résolution des équations différentielles}
			
			\begin{small}
				\textblue{La compacité nous permet de résoudre des équations différentielles.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.48\textwidth}
						\item \thm~\cite[p.56]{SaintRaymond}: Espace de fonction continue sur un compact est un Banach
						\item \application~\cite[p.180]{Rouviere}: Théorème de Cauchy--Lipschitz \textred{(DEV)}
					\end{minipage} \hfill
					\begin{minipage}{.32\textwidth}
						\item \exemple: Problème de Cauchy
						\item \thm~\cite[p.83]{SaintRaymond}: Théorème d'Ascoli
						\item \application: Théorème de Cauchy--Peano
					\end{minipage} 
				\end{itemize}
			\end{footnotesize}
		\end{description}
	\end{description}
	
%	\section*{Quelques notions importantes}
	
	

	
\end{document}