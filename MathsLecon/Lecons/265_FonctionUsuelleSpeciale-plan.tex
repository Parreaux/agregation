\chapter*{Leçon 265: Exemples d'études et d'applications des fonctions usuelles et spéciales.}
\addcontentsline{toc}{chapter}{Leçon 265: Exemples d'études et d'applications des fonctions usuelles et spéciales.}

\titlebox{blue}{Références pour la leçon}{
	\textblue{\cite{Gourdon-analyse}} Gourdon, \emph{Analyse.}
	
	\textblue{\cite{GrouxSoulat}} Groux et Soulat, \emph{Les fonctions spéciales vues par les problèmes.}
	
	\textblue{\cite{Jolissant}} Jolissant, \emph{Fonction d'une variable complexe.}
	
	\textblue{\cite{Rudin}} Rudin, \emph{Principe d'analyse mathématiques.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.35\textwidth}
		Caractérisation de $\Gamma$
	\end{minipage} \hfill
	\begin{minipage}{.5\textwidth}
		Formule de Poisson et fonction $\Theta$
	\end{minipage}
}

\section*{Motivation}

\subsection*{Défense}


\subsection*{Ce qu'en dit le jury}

Cette leçon est très riche ; c’est une leçon de synthèse qui doit permettre d’explorer de nombreux pans du programme. Évidemment, la leçon ne doit pas se cantonner au seul champ des fonctions usuelles (logarithme, exponentielle, trigonométriques, hyperboliques et réciproques). Le jury attend surtout d’un agrégé qu’il soit en mesure de présenter rapidement les définitions et les propriétés fondamentales de ces fonctions, qu’il sache les tracer sans difficultés, qu’il puisse mener l’étude aux bornes de leur domaine, ainsi que discuter leurs prolongements éventuels, leurs développements de Taylor ou en série entière, leurs applications au calcul intégral, les équations fonctionnelles associées ou formules particulières, etc. Le jury n’attend pas un catalogue mais plutôt un choix pertinent et réfléchi, avec des applications en probabilité, convexité, études de courbes, ou autour des développements asymptotiques. Les déterminations du logarithme complexe peuvent tout à fait mériter une discussion approfondie dans cette leçon et donner lieu à des développements de bon niveau, pouvant aller jusqu’à leur interprétation géométrique.

Le domaine des fonctions spéciales est très vaste. Il faut absolument éviter l’écueil d’une taxonomie fastidieuse et dépourvue de motivation ; il vaut bien mieux se concentrer sur des exemples restreints, mais fouillés, par exemple une étude approfondie (d’une) des fonctions $\Gamma$, $\zeta$ ou $\theta$, leurs propriétés fonctionnelles, leurs prolongements, leur étude asymptotique aux bornes et les domaines d’applications de ces fonctions.

Il y a donc bien des manières, très différentes, de construire valablement cette leçon. Par exemple, on peut bâtir un exposé organisé selon des problématiques et des techniques mathématiques : suites et séries de fonctions, fonctions holomorphes et méromorphes, problèmes de prolongement, développements asymptotiques, calculs d’intégrales et intégrales à paramètres, transformées de Fourier ou de Laplace, etc. Mais on pourrait tout aussi bien suivre un fil conducteur motivé par un domaine d’application :
\begin{itemize}
	\item en arithmétique pour évoquer, par exemple, la fonction $\zeta$ et la distribution des nombres premiers,
	\item en probabilités où la loi normale et la fonction erreur sont évidemment incontournables mais on peut aussi évoquer les lois Gamma et Bêta, les fonctions de Bessel et leurs liens avec la densité du $\chi^2$ non centrée et celle de la distribution de Von Mises-Fisher ou plus simplement comme loi du produit de variables aléatoires normales et indépendantes, la loi $\zeta$ et ses liens avec 	la théorie des nombres,...
	\item en analyse des équations aux dérivées partielles où les fonctions spéciales interviennent notamment pour étudier le problème de Dirichlet pour le Laplacien ou l’équation des ondes,
	\item il est aussi possible d’évoquer les polynômes orthogonaux, leurs propriétés et leurs diverses applications, en physique (oscillateur harmonique et polynômes de Hermite), en probabilités (polynômes de Hermite pour les lois normales, de Laguerre pour les lois Gamma, de Jacobi pour les lois Bêta...), pour l’étude d’équations aux dérivées partielles ou pour l’analyse de méthodes numériques,
	\item en théorie des représentations de groupes avec les fonctions de Bessel, 
	\item en algèbre en abordant les fonctions $p$-elliptiques.
\end{itemize}

Là encore, le jury renouvelle sa mise en garde d’éviter de faire un catalogue qui s’avérerait stérile, il s’agit bien plutôt de se tenir à détailler l’un ou l’autre de ces points de vue. Au final, cette leçon peut être l’occasion de montrer un véritable investissement personnel, adossé aux goûts du candidat.

\section*{Métaplan}

\begin{description}
	\item[I.] \textbf{La fonction exponentielle \cite[p.157]{Rudin}}
	\begin{small}
		\textblue{Une fonction très importante en mathématiques dont l'étude amène à beaucoup de mathématiques.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Séries entières}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Fonction analytique
					\item \corollaire: Dérivée d'ordre supérieures
					\item \application: Produit de Cauchy
				\end{minipage} \hfill
				\begin{minipage}{.45\textwidth}
					\item \thm: Convergence, dérivabilité et continuité
					\item \thm: Abel
					\item \thm: Développement en série entière et unicité
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Fonction exponentielle}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.4\textwidth}
					\item \definiton: Fonction exponentielle
					\item \thm: Propriétés de cette fonction
					\item \application: Transformée de Fourier
					\item \remarque: Lien avec l'exponentielle
				\end{minipage} \hfill
				\begin{minipage}{.5\textwidth}
					\item \remarque: Existence
					\item \application: Loi sans mémoire
					\item \application: Définition des fonctions trigonométriques
					\item \thm: $\exp$ est $2\pi$-périodique
				\end{minipage}
				\item \corollaire: Fonction trigonométrique sont $2\pi$-périodique
				\item \application: Théorème de d'Alembert
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Inversion de la fonction exponentielle \cite[p.91]{Jolissant}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.46\textwidth}
					\item \definiton: $\ln$ réciproque de $\exp$
					\item \proposition: Pas d'inverse dans $\C$
					\item \remarque: Pas de détermination sur des ouverts
					\item \remarque: $\log(z_1z_2) \neq \log(z_1)\log(z_2)$
					\item \proposition: Développement du $\log$
				\end{minipage} \hfill
				\begin{minipage}{.45\textwidth}
					\item \remarque: Définition par primitive
					\item \definiton: Détermination (et principal) du $\log$
					\item \remarque: $f$ détermination du $\log$ holomorphe
					\item \remarque: Détermination du $\log$ sur demi-droite
					\item \application: Définition des fonctions puissances					
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[II.] \textbf{La fonction Gamma $\Gamma$}
	\begin{small}
		\textblue{La fonction $\Gamma$ est un prolongement sur $\C$ de la fonction $n!$ sur $\N$}
	\end{small}
	\begin{description}
		\item[A.] \emph{Étude de cette fonction dans $\R^*_+$ \cite[p.178]{Rudin}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.45\textwidth}
					\item \definiton: $\Gamma$ sur $\R^*_+$
					\item \remarque~\cite{GrouxSoulat}: Formule de Gauss
					\item \proposition: Formule de Stirling
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \textred{\thm~\cite{Rudin}: Caractérisation}
					\item \proposition: Formule des compléments
					\item \corollaire: $\Gamma(x) = \frac{2^{x - 1}}{\sqrt{\pi}} \Gamma\left(\frac{x}{2}\right) \Gamma\left(\frac{x + 1}{2}\right)$
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Étude complexe de $\Gamma$}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.5\textwidth}
					\item \notation: $B_0$
					\item \remarque: La fonction est bien définie
					\item \proposition~(ADMIS): $\Gamma$ se prolonge sur $\C$ 
					\item \application: Loi Gamma
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \definiton: $\Gamma$ sur $B_0$
					\item \proposition: Propriété de cette fonction
					\item \lem: Expressions de $\Gamma$
					\item \thm: Formule des compléments
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Produit infini et $\Gamma$ \cite[p.147]{GrouxSoulat}}
		\begin{small}
			\text{Ne pas oublier $\zeta$.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.48\textwidth}
					\item \definiton: Produit infini et convergence
					\item \textred{\proposition~\cite{Gourdon-analyse}: Formule sommatoire de Poisson}
					\item \proposition: Définition de $\Gamma$ via $\theta$
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \lem: Caractérisation de cette convergence
					\item \textred{\application~\cite{Gourdon-analyse}: $\theta$ de Jacobi}
					\item \application~(Culturel): les $0$ de $\zeta$
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
\end{description}

