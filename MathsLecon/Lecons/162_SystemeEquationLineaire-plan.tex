\chapter*{Leçon 162 : Système d'équations linéaires; opérations élémentaires, aspects algorithmiques et conséquences théoriques.}
\addcontentsline{toc}{chapter}{Leçon 162 : Système d'équations linéaires; opérations élémentaires, aspects algorithmiques et conséquences théoriques.}

\titlebox{blue}{Références pour la leçon}{
	\textblue{\cite{AllaireKaber}} Allaire et Kaber, \emph{Algèbre linéaire numérique.}
	
	\textblue{\cite{Caldero-Germoni2}} Caldero et Germoni, \emph{Histoires hédonistes de groupes et de géométries, tome 2.}
	
	\textblue{\cite{Cognet}} Cognet, \emph{Algèbre linéaire.}
	
	\textblue{\cite{Gourdon-algebre}} Gourdon, \emph{Algèbre.}
	
	\textblue{\cite{Grifone}} Grifone, \emph{Algèbre linéaire.}
	
	\textblue{\cite{RamisWarusfel}} Ramis et Warusfel, \emph{Cours de mathématiques pures et appliquées.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.35\textwidth}
		Méthode de Kackmarz
	\end{minipage} \hfill
	\begin{minipage}{.5\textwidth}
		Méthode du gradient à pas optimal
	\end{minipage}
}

\section*{Motivation}

\subsection*{Défense}

\begin{itemize}
	\item La résolution de système d'équation linéaire intervient dans de nombreuses applications mathématiques : algèbre linéaire; résolution d'équation différentielle linéaire; ... Mais ils interviennent dans de nombreuses autres sciences: économie, physique, ...
	\item Dualité des problèmes : théorique (existence des solutions, calcul théorique, conséquences) et pratique (comment on les implémente sur un ordinateur, convergence, complexité)
\end{itemize}

\subsection*{Ce qu'en dit le jury}

Dans cette leçon, les techniques liées au simple pivot de Gauss constituent l’essentiel des attendus. Il est impératif de faire le lien avec la notion de système échelonné, (dont on donnera une définition précise et correcte), et de situer l’ensemble dans le contexte de l’algèbre linéaire (sans oublier la dualité). Un point de vue opératoire doit accompagner l’étude théorique et l’intérêt algorithmique des méthodes présentées doit être expliqué. On pourra illustrer cela par des exemples simples (où l’on attend parfois une résolution explicite).

Parmi les conséquence théoriques, les candidats pourront notamment donner des systèmes de générateurs de $GL_n(K)$ et $SL_n(K)$. Ils peuvent aussi présenter les relations de dépendance linéaire sur les colonnes d’une matrice échelonnée qui permettent de décrire simplement les orbites de l’action à gauche de $GL_n(K)$ sur $M_n(K)$ donnée par $(P, A) \to P A$.

S’ils le désirent, les candidats peuvent exploiter les propriétés des systèmes d’équations linéaires pour définir la dimension des espaces vectoriels et obtenir une description de l’intersection de deux sous-espaces vectoriels donnés par des systèmes générateurs, ou d’une somme de deux sous-espaces vectoriels donnés par des équations.

De même, des discussions sur la résolution de systèmes sur Z et la forme normale de Hermite peuvent trouver leur place dans cette leçon. Enfin, il est possible de présenter les décompositions LU et de Choleski, en évaluant le coût de ces méthodes ou encore d’étudier la résolution de l’équation normale associée aux problèmes des moindres carrés et la détermination de la solution de norme minimale par la méthode de décomposition en valeurs singulières.

\section*{Métaplan}

\begin{footnotesize}
	\noindent\emph{Cadre}: $A \mathcal{M}_{m, n}(K)$ où $K$ est un corps commutatif quelconque.
\end{footnotesize}

\begin{description}
	\item[I.] \textbf{Systèmes d'équations linéaires}
	\begin{small}
		\textblue{Nous commençons par donner quelques notions de vocabulaires autours des systèmes d'équations linéaires. Ce vocabulaire nous servira tout au long de la leçon.}
	\end{small}
	\begin{footnotesize}
		\begin{itemize}[label=$\rightsquigarrow$]
			\item \definiton: Forme classique, forme matricielle et $Ax = b$
			
			\begin{minipage}{.4\textwidth}
				\item \definiton: Système compatible: $b \in \mathrm{im}(A)$
			\end{minipage} \hfill
			\begin{minipage}{.4\textwidth}
				\item \definiton: Rang du système: $\mathrm{rg}(A)$
			\end{minipage}
			\item \remarque: Résoudre revient à chercher intersection d'hyperplan
		\end{itemize}
	\end{footnotesize}
	\begin{description}
		\item[A.] \emph{Systèmes de Cramer \cite[p.138]{Gourdon-algebre}}
		\begin{small}
			\textblue{Le système de Cramer est un des premier problème que nous sommes capable de résoudre. Il fait des hypothèses sur la taille de la matrice: il faut qu'elle soit inversible. Cependant, donner explicitement les solution d'un tel système est en $O(n!)$. On a donc besoin de méthodes plus efficaces.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: $A \in GL_n(K)$
					\item \thm: Calcul via le déterminant					
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \proposition: Existence et unicité des solutions
					\item \proposition: Complexité: $O(n!)$
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Cas général \cite[p.138]{Gourdon-algebre}}
		\begin{small}
			\textblue{On décrit ici la méthode général pour résoudre un système. Comme précédemment la complexité d'une telle résolution nous oblige à chercher une méthode plus efficace.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \cadre: $r$ est le rang du système et le déterminant extrait de taille $r$ est non nul
				
				\begin{minipage}{.4\textwidth}
					\item \definiton: Déterminant caractéristique
					\item \remarque: Description des solutions
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \thm: Théorème de Rouché-Fontné
					\item \definiton: Système homogène
				\end{minipage}
				\item \proposition: Solutions
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[II.] \textbf{Pivot de Gauss et application}
	\begin{small}
		\textblue{Une méthode plus efficace est l'algorithme du pivot de Gauss qui s'exécute en $O(n^3)$. Il est basé sur des opérations élémentaires données par des matrices élémentaires qui agissent sur l'ensemble des matrices. Cette action permet de mettre le système sous-forme échelonnée : triangulaire supérieure. L'algorithme de Gauss est une méthode appliquant cette action. Gardons à l'esprit que cette méthode est applicable sur des anneaux intègre (la remontée n'est pas réalisable).}
	\end{small}
	\begin{description}
		\item[A.] \emph{Opérations élémentaires \cite[p.201]{Caldero-Germoni-N2}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton~\cite[p.205]{Cognet}: Matrices de dilatation
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \definiton~\cite[p.205]{Cognet}: Matrices de permutation
				\end{minipage}
				\item \definiton~\cite[p.205]{Cognet}: Matrices de transvection
				\item \proposition~\cite[p.205]{Cognet}: Déterminant de ces matrices
				
				\begin{minipage}{.38\textwidth}
					\item \definiton: Action à droite de $GL_n(K)$
				\end{minipage} \hfill
				\begin{minipage}{.52\textwidth}
					\item \remarque: Traduction des opérations via les matrices
				\end{minipage}
				\item \proposition: Action conserve le rang du système
				
				\begin{minipage}{.4\textwidth}
					\item \proposition: Action conserve les solutions
					\item \lem~\cite[p.205]{Cognet}: Inverse de ces matrices
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \application~\cite[p.205]{Cognet}: Connexité par arcs
					\item \remarque: De même: action à droite
				\end{minipage}
				\item \thm~\cite[p.205]{Cognet}: Générateurs de $SL_n(K)$ et $GL_n(K)$
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Système échelonné \cite[p.201]{Caldero-Germoni-N2}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Pivot
					\item \remarque: Analogue en colonne
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \definiton: Système échelonné
					\item \application: Remonter
				\end{minipage}
				\item \remarque: Nombre de pivot est le rang
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Pivot de Gauss \cite[p.107]{AllaireKaber}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \thm: Mise sous forme échelonnée
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \algo: Pivot de Gauss
				\end{minipage}
				\begin{minipage}{.35\textwidth}
					\item \remarque: Complexité : $O(mn^2)$
				\end{minipage} \hfill
				\begin{minipage}{.45\textwidth}
					\item \proposition: Résolution de système triangulaire
				\end{minipage}
				\item \remarque: Complexité de la remontée: $O(n^2)$
				\item \thm: Caractérisation des orbites par le rang
				
				\begin{minipage}{.4\textwidth}
					\item \application~\cite[p.50]{Grifone}: Résolution de système
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \application: Liberté de famille
				\end{minipage}
				\item \application: Intersection de sous-espaces vectoriel
				
				\begin{minipage}{.45\textwidth}
					\item \application: Espace vectoriel via les équations
					\item \application: Déterminant
				\end{minipage} \hfill
				\begin{minipage}{.35\textwidth}
					\item \application: Base duale
					\item \application: Inverse
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[D.] \emph{Méthodes de décomposition \cite[p.113]{AllaireKaber}}
		\begin{small}
			\textblue{Dans certain cas, le pivot de Gauss n'est pas la méthode la plus efficace pour résoudre le problème: des méthodes de décomposition peuvent simplifier le problème afin d'obtenir une résolution plus rapide.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \principe: Se ramener à plusieurs systèmes triangulaires
				
				\begin{minipage}{.45\textwidth}
					\item \thm: Décomposition LU
					\item \proposition: Deux systèmes triangulaire: $O(n^3)$
					\item \thm: Décomposition de Cholesky
				\end{minipage} \hfill
				\begin{minipage}{.35\textwidth}
					\item \remarque: Calcul direct via Gauss
					\item \proposition: $A$ trigonale: $O(n)$
					\item \proposition: Deux système triangulaire
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[III.] \textbf{Méthode d'analyse matricielle}
	\begin{small}
		\textblue{Pour certaine matrice une résolution exacte n'est pas nécessaire ou prend trop de temps à être calculer. Nous utilisons alors des méthodes d'analyse matricielle comme les méthodes itératives ou le gradient à pas optimal. Cette dernière transforme notre système est une fonction dont on cherche un extremum.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Méthode itératives \cite[p.155]{AllaireKaber}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Décomposition régulière
					\item \thm: Caractérisation de la convergence
					\item \application: Méthode de Jacobi
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \definiton: Méthode itérative
					\item \definiton: Convergence
					\item \application: Méthode de Gauss-Seidel
				\end{minipage}
				\begin{minipage}{.35\textwidth}
					\item \application: Méthode de Relaxation
				\end{minipage} \hfill
				\begin{minipage}{.45\textwidth}
					\item \thm: Convergence de la méthode de relaxation
				\end{minipage}
				\item \textred{\thm: Méthode de Kaczmarz}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Méthode du gradient \cite[p.411]{RamisWarusfel}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.45\textwidth}
					\item \definiton: Conditionnement
					\item \proposition: Propriétés du conditionnement
					\item \textred{\algo: Méthode de gradient à pas optimal}
				\end{minipage} \hfill
				\begin{minipage}{.35\textwidth}
					\item \remarque: Interprétation
					\item \thm: Traduction du problème
					\item \textred{\thm: Convergence de l'algorithme}
				\end{minipage} 
			\end{itemize}
		\end{footnotesize}
	\end{description}
\end{description}
