\chapter*{Leçon 250: Transformation de Fourier. Applications.}
\addcontentsline{toc}{chapter}{Leçon 250: Transformation de Fourier. Applications.}

\titlebox{blue}{Références pour la leçon}{
	\textblue{\cite{ElAmrani-2}} El Amrani, \emph{Analyse de Fourier dans les espaces fonctionnels.}
	
	\textblue{\cite{BernisBernis}} Bernis et Bernis, \emph{Analyse pour l'agrégation de mathématiques: 40 développements.}
	
	\textblue{\cite{GaretKurtzmann}} Garet et Kurtzmann, \emph{De l'intégration aux probabilités.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.35\textwidth}
		Théorème Central Limite
	\end{minipage} \hfill
	\begin{minipage}{.5\textwidth}
		Formule de Poisson et théorème de Shannon
	\end{minipage}
}

\section*{Motivation}

\subsection*{Défense}

\emph{Motivation}: Historique.

\subsection*{Ce qu'en dit le jury}

Cette leçon offre de multiples facettes. Les candidats peuvent adopter différents points de vue : $L^1$, $L^2$ et/ou distributions. L’aspect « séries de Fourier » n’est toutefois pas dans l’esprit de cette leçon ; il ne s’agit pas de faire de l’analyse de Fourier sur n’importe quel groupe localement compact mais sur $\R$ ou $\R^d$.

La leçon nécessite une bonne maîtrise de questions de base telle que la définition du produit de convolution de deux fonctions de $L^1$. En ce qui concerne la transformation de Fourier, elle ne doit pas se limiter à une analyse algébrique de la transformation de Fourier. C’est bien une leçon d’analyse, qui nécessite une étude soigneuse des hypothèses, des définitions et de la nature des objets manipulés. Le lien entre la régularité de la fonction et la décroissance de sa transformée de Fourier doit être fait, même sous des hypothèses qui ne sont pas minimales. Les candidats doivent savoir montrer le lemme de Riemann-Lebesgue pour une fonction intégrable.

La formule d’inversion de Fourier pour une fonction $L^1$ dont la transformée de Fourier est aussi $L^1$ est attendue ainsi que l’extension de la transformée de Fourier à l’espace $L^2$ par Fourier-Plancherel. Des exemples explicites de calcul de transformations de Fourier, classiques comme la gaussienne ou $(1 + x^2)^{-1}$, paraissent nécessaires.

Pour aller plus loin, la transformation de Fourier des distributions tempérées ainsi que la convolution dans le cadre des distributions tempérées peuvent être abordées. Rappelons une fois de plus que les attentes du jury sur ces questions restent modestes, au niveau de ce qu’un cours de première année de master sur le sujet peut contenir. Le fait que la transformée de Fourier envoie $\mathcal{S}\left(\R^d\right)$ dans lui même avec de bonnes estimations des semi-normes doit alors être compris et la formule d’inversion de Fourier maîtrisée dans ce cadre. Des exemples de calcul de transformée de Fourier peuvent être donnés dans des contextes liés à la théorie des distributions comme par exemple la transformée de Fourier de la valeur principale. Dans un autre registre, il est aussi possible d’orienter la leçon vers l’étude de propriétés de fonctions caractéristiques de variables aléatoires.

La résolution de certaines équations aux dérivées partielles telles que, par exemple, l’équation de la chaleur sur $\R$, peut être abordée, avec une discussion sur les propriétés qualitatives des solutions.

\section*{Métaplan}

\begin{footnotesize}
	\begin{itemize}[label=$\rightsquigarrow$]
		\item \cadre: On se limite à la dimension $1$. Sinon, on introduit un produit scalaire dans l'exponentiel.
		
		\begin{minipage}{.45\textwidth}
			\item \definiton: Transformation de Fourier (si elle existe)
		\end{minipage} \hfill
		\begin{minipage}{.4\textwidth}
			\item \remarque: Plusieurs définitions
		\end{minipage}
	\end{itemize}
\end{footnotesize}

\begin{description}
	\item[I.] \textbf{Transformée de Fourier dans $L^1$ \cite[p.109]{ElAmrani-2}}
	\begin{description}
		\item[A.] \emph{Existence de la transformée de Fourier}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \lem: Riemann-Lebesgue
					\item \corollaire: Application linéaire et continue
					\item \definiton: Fourier conjuguée
					\item \corollaire: Propriétés de parité
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \thm: Fonction transformée de Fourier
					\item \exemple: Classique
					\item \proposition: Symétrie et conjugaison
					\item \proposition: Translation
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Convolée et transformation de Fourier}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.4\textwidth}
					\item \proposition: Convolution $\Rightarrow$ produit
					\item \thm: Formule de dualité
					\item \application: Résoudre $f * e^{-2\left|x\right|} = e^{-3\left|x\right|}$
					\item \proposition: Convolution $\Leftarrow$ produit
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \application: Résoudre l'équation $f * f = f$
					\item \thm: Injectivité de $f$
					\item \thm: Formule d'inversion
					\item \remarque: $f \in \mathcal{L}^1 \not\Rightarrow \hat{f} \in \mathcal{L}^1$
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Transformée de Fourier et dérivabilité}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \proposition: Dérivée de la transformée
				\end{minipage} \hfill
				\begin{minipage}{.45\textwidth}
					\item \application: Résolution d'équation différentielle
				\end{minipage}
				\item \proposition: Transformée de la dérivée
			\end{itemize}
		\end{footnotesize}
		\item[D.] \emph{Application aux probabilités \cite[p.212]{GaretKurtzmann}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.45\textwidth}
					\item \definiton: Fonction caractéristique
					\item \exemple: Fonction caractéristique de $\mathcal{N}(0,1)$
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \thm: Caractérisation d'une loi
					\item \proposition: Beppo--Lévi
				\end{minipage}
				\item \textred{\thm: Théorème Central Limite}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[II.] \textbf{Transformée de Fourier dans $L^2$}
	\begin{description}
		\item[A.] \emph{Existence de la transformation de Fourier}
		\begin{small}
			\textblue{Comme $L^2 \subset L^1$, la construction est un peu délicate mais on en a toujours voulue une dans $L^2$ car $f$ et $\hat{f}$ ont le même rôle.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.44\textwidth}
					\item \thm: Formule de Plancherel--Perceval
					\item \thm: Construction de la transformée
					\item \definiton: Transformée de Fourier dans $L^2$
					\item \remarque: Conservation des propriétés
					\item \corollaire: Formule d'inversion pour $\hat{f} \in L^1$
				\end{minipage} \hfill
				\begin{minipage}{.45\textwidth}
					\item \remarque: Interprétation comme produit scalaire
					\item \remarque: $L^1 \cap L^2$ dense dans $L^2$
					\item \proposition: Transformée sur $L^1 = L^2$
					\item \proposition: Symétries
					\item \thm: Dualité
				\end{minipage} 
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Convolution et transformation de Fourier}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.4\textwidth}
					\item \thm: Produit vs Convolution
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \proposition: Convolution vs Produit
				\end{minipage}
				\item \thm: Formule d'inversion
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[III.] \textbf{Transformée de Fourier dans $\mathcal{S}(\R)$}
	\begin{description}
		\item[A.] \emph{Transformée de Fourier des fonctions à décroissance rapide}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.45\textwidth}
					\item \definiton: Fonction à décroissance rapide
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \exemple: $e^{-\left|x\right|}$
				\end{minipage}
				\item \lem: $x^{\alpha}f \in L^1(\R)$, $\forall \alpha$ si $f$ décroissance rapide
				\item \proposition: Transformation de Fourier à décroissance rapide
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Espace de Schwartz $\mathcal{S}(\R)$}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.4\textwidth}
					\item \definiton: Espace de Schwartz
				\end{minipage} \hfill
				\begin{minipage}{.45\textwidth}
					\item \remarque: Intégrable, limite $0$ ainsi que leur dérivée
				\end{minipage}
				\item \proposition: Caractérisation de cet espace
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Transformée de Fourier dans $\mathcal{S}(\R)$}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.5\textwidth}
					\item \thm: Stabilité de la transformée de Fourier
					\item \thm: Transformée de Fourier: une application
					\item \textred{\application~\cite{BernisBernis}: Théorème de Shanon}
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \definiton: Suite tendant vers $0$
					\item \textred{\application~\cite{BernisBernis}: Formule de Poisson}
					\item \proposition: $\mathcal{S}(\R)$ dense dans $L^2$
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
\end{description}

