\chapter*{Leçon 203: Utilisation de la notion de compacité.}
\addcontentsline{toc}{chapter}{Leçon 203: Utilisation de la notion de compacité.}

\titlebox{blue}{Références pour la leçon}{
	\textblue{\cite{Gourdon-analyse}} Gourdon, \emph{Analyse.}
	
	\textblue{\cite{Hauchecorne}} Hauchecorne, \emph{Les contre-exemples en mathématiques.}
	
	\textblue{\cite{Nourdin}} Nourdin, \emph{Agrégation mathématiques épreuve orale.}
	
	\textblue{\cite{SaintRaymond}} Saint Raymond, \emph{Topologie, calcul différentiel et variable complexe.}
	
	\textblue{\cite{Rouviere}} Rouvière, \emph{Petit guide de calcul différentiel à l'usage de la licence et de l'agrégation.}
	
	\textblue{\cite{Testard}} Testard, \emph{Analyse mathématiques. Maîtrise de l'implicite.}
	
	\textblue{\cite{TeytaudAntoniniBorgnatChateauLebeau}} Teytaud, Antonini, Borgnat, Chateau et Lebeau, \emph{Les maths pour l'agreg.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.5\textwidth}
		Théorème de Weierstrass via la convolution
	\end{minipage} \hfill
	\begin{minipage}{.4\textwidth}
		Théorème de Cauchy -- Lipschitz
	\end{minipage}
}

\section*{Motivation}

\subsection*{Défense}

\begin{itemize}
	\item Notion topologique importante dans le cas métrique.
	\item Permet de ramener des raisonnements de type fini sur des ensembles infinis.
	\item Permet de passer certaines propriétés du local au global, c'est-à-dire qu'une propriété vraie au voisinage de chaque point devient valable de façon uniforme sur tout le compact. 
	\item Donne quelques résultats d'existences.
\end{itemize}


\subsection*{Ce qu'en dit le jury}

Il est important de ne pas concentrer la leçon sur la compacité en général et d’éviter la confusion entre utilisation de la notion compacité et notion de compacité. Le jury recommande vivement de rester en priorité dans le cadre métrique. Néanmoins, on attend des candidats d’avoir une vision synthétique de la compacité. Des exemples d’applications comme le théorème de Heine et le théorème de Rolle doivent y figurer et leur démonstration être connue. Par ailleurs, le candidat doit savoir quand la boule unité d’un espace vectoriel normé est compacte. Des exemples significatifs d’utilisation comme le théorème de Stone-Weierstrass (version qui utilise la compacité), des théorèmes de point fixe, voire l’étude qualitative d’équations différentielles, sont tout à fait envisageables. Le rôle de la compacité pour des problèmes d’existence d’extrema mériterait d’être davantage étudié. On peut penser ensuite à des exemples en dimension $n \geq 2$.

Pour aller plus loin, les familles normales de fonctions holomorphes fournissent des exemples fondamentaux d’utilisation de la compacité. Les opérateurs auto-adjoints compacts sur un espace de Hilbert relèvent également de cette leçon, et on pourra développer l’analyse de leurs propriétés spectrales.

\section*{Métaplan}

\begin{footnotesize}
	\noindent\emph{Cadre}: $E$ est un espace métrique
\end{footnotesize}

\begin{description}
	\item[I.] \textbf{Rappel sur la notion de compacité et premières applications}
	\begin{small}
		\textblue{Cette section a pour but de poser les premières définitions de la compacité ainsi que ces première propriétés. Nous en donnons également quelques applications rapide: elle n'a pas vocation à être le coeur de la leçon.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Propriété de Borel--Lebesgue \cite[p.33]{SaintRaymond}}
		\begin{small}
			\textblue{Une première caractérisation de la compacité se fait par la propriété de Borel--Lebesgue. Nous donnons quelques caractérisations de compacts qui en découle directement.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Recouvrement
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \definiton: Espace compact
				\end{minipage}
				\item \exemple~\cite[p.27]{Gourdon-analyse}: $[0, 1]$
				\item \cexemple~\cite[p.302]{Hauchecorne}: Espace vérifiant la propriété et non compact
				\item \remarque: Cas non métrique
				\item \application: Ensemble des valeurs d'une suite et limite : compact
				\item \proposition~\cite[p.229]{TeytaudAntoniniBorgnatChateauLebeau}: Compact implique borné
				\item \application~\cite[p.27]{Gourdon-analyse}: Utilisation de la contraposé comme caractérisation
				
				\begin{minipage}{.5\textwidth}
					\item \proposition~\cite[p.28]{Gourdon-analyse}: Réunion finie: compacte
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \application: Ensemble fini est compact
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Caractérisation séquentielle de la compacité \cite[p.28]{Gourdon-analyse}}
		\begin{small}
			\textblue{La propriété de Borel--Lebesgue est équivalente à la caractérisation séquentielle dans le cas métrique: c'est ce que nous appelons le théorème de Bolzano-Weierstrass. Ce point de vu nous donne de nouvelles applications.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.3\textwidth}
					\item \thm: Bolzano--Weierstrass
				\end{minipage} \hfill
				\begin{minipage}{.5\textwidth}
					\item \application: Caractérisation de limite via l'exponentielle
				\end{minipage}
				\begin{minipage}{.4\textwidth}
					\item \remarque: Cas des espaces non métrique
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \proposition: Unicité de la valeur d'adhérence
				\end{minipage}
				\item \application~\cite[p.8]{Nourdin}: Convergence en loi de variables gaussiennes
				\item \thm: Théorème de Tychonoff (dénombrable)
				\item \remarque: Le théorème est vrai dans le cas non-dénombrable
				\item \application: Compacts dans $\R^n$
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Caractérisation par les fermés \cite[p.34]{SaintRaymond}}
		\begin{small}
			\textblue{La propriété de Borel--Lebesgue possède une propriété dual permettant de caractériser les espaces compacts grâce à des fermés.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \thm: Caractérisation par les fermés
				\item \thm: Sous-espace fermé d'un compact est compact
				
				\begin{minipage}{.35\textwidth}
					\item \thm: Compact sont fermés
				\end{minipage} \hfill
				\begin{minipage}{.5\textwidth}
					\item \cexemple~\cite[p.302]{Hauchecorne}: Compact non fermé
				\end{minipage}
				\item \application~\cite[p.229]{TeytaudAntoniniBorgnatChateauLebeau}: $O_n(\R)$ et $SO_n(\R)$ sont compacts
				
				\begin{minipage}{.4\textwidth}
					\item \thm: Surjectivité des applications
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \application: Compacts dans $\R$
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[II.] \textbf{La compacité: une vue globale de ce qui est local}
	\begin{small}
		\textblue{La compacité permet d'adopter un point de vu fini sur un ensemble infini. Elle permet donc de transformée des propriétés locales propriétés globales}
	\end{small}
	\begin{description}
		\item[A.] \emph{Continuité uniforme}
		\begin{small}
			\textblue{La continuité uniforme (consistant à l'inversion des bons quantificateurs) transforme la notion de continuité qui est locale en une notion globale.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton~\cite[p.28]{SaintRaymond}: Continuité uniforme
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \thm~\cite[p.43]{SaintRaymond}: Théorème de Heine
				\end{minipage}
				\item \application: Approximation de fonctions continues par des suites de fonctions affines
				\item \thm~\cite[p.8]{Nourdin}: Deuxième théorème de Dini
				\item \application~\cite[p.8]{Nourdin}: Théorème de Glivenko--Cantelli
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Approximation de fonctions continues sur un compact ~\cite[p.8]{Nourdin}}
		\begin{small}
			\textblue{L'approximation de fonction permet de donner des propriétés de régularité à une fonction.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \thm: Premier théorème de Dini
				\item \application: Approximation uniforme via des polynômes : $t \mapsto \sqrt{t}$ sur $[0, 1]$
				\item \definiton~\cite[p.284]{Gourdon-analyse}: Approximation de l'unité
				
				\begin{minipage}{.5\textwidth}
					\item \textred{\thm~\cite[p.284]{Gourdon-analyse}: Théorème de Weierstrass}
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \application: Théorème de Féjer
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[III.] \textbf{Compacité et problème d'existences}
	\begin{small}
		\textblue{La compacité permet d'exhiber des problèmes d'existences.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Recherche d'extremum \cite[p.42]{SaintRaymond}}
		\begin{small}
			\textblue{La recherche d'extremum sur un compact est plus facile car nous avons la garantie de l'existence de cet extremum.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \thm: Image d'un compact est un compact
				
				\begin{minipage}{.5\textwidth}
					\item \application: Continue sur compact atteint ses bornes
				\end{minipage} \hfill
				\begin{minipage}{.3\textwidth}
					\item \application: Cas réel
				\end{minipage}
				\item \proposition~\cite[p.156]{SaintRaymond}: Théorème de Rolle
				\item \proposition~\cite[p.156]{SaintRaymond}: Théorème des accroissements finis
				
				\begin{minipage}{.45\textwidth}
					\item \proposition~\cite[p.44]{Testard}: Équivalence des normes
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \application~\cite[p.99]{SaintRaymond}: Théorème de Riesz
				\end{minipage}
				\begin{minipage}{.45\textwidth}
					\item \application: Caractérisation des compacts
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \definiton~\cite[p.44]{Testard}: Fonction coersive
				\end{minipage}
				\item \proposition~\cite[p.44]{Testard}: Borne d'une telle fonction
				\item \application~\cite[p.44]{Testard}: Théorème de d'Alembert
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Résolution des équations différentielles}
		\begin{small}
			\textblue{La compacité nous permet de résoudre des équations différentielles.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \thm~\cite[p.56]{SaintRaymond}: Espace de fonction continue sur un compact est un Banach
				
				\begin{minipage}{.5\textwidth}
					\item \textred{\application~\cite[p.180]{Rouviere}: Théorème de Cauchy--Lipschitz}
				\end{minipage} \hfill
				\begin{minipage}{.3\textwidth}
					\item \exemple: Problème de Cauchy
				\end{minipage} 
				\begin{minipage}{.4\textwidth}
					\item \thm~\cite[p.83]{SaintRaymond}: Théorème d'Ascoli
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \application: Théorème de Cauchy--Peano
				\end{minipage} 
			\end{itemize}
		\end{footnotesize}
	\end{description}
\end{description}
