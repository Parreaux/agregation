\documentclass[a4paper,8pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais,english]{babel}%

\usepackage[dvipsnames]{xcolor}

\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{enumitem}
\usepackage{appendix}
\usepackage{stmaryrd}
\usepackage{amsmath}
\usepackage{wasysym}

\usepackage{listings}
\usepackage{multirow}
\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}
\algnewcommand{\LineComment}[1]{\State \(\triangleright\) #1}

\parskip=0\baselineskip %0.5 d'habitude


\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\F}{\mathbb{F}}
	\newcommand{\Sn}{\mathfrak{S}}
	\newcommand{\A}{\mathfrak{A}}
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textbluesmall}[1]{{\small{\textcolor{blue}{#1}}}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\theoremstyle{plain}
	\newtheorem{theo}{Théorème}
	\newtheorem{prop}{Proposition}
	\newtheorem{cor}{Corollaire}
	\newtheorem{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem{definition}{Définition}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	
	\title{Leçon 141 : Polynômes irréductibles à une indéterminée. Corps de Rupture. Exemples et applications.}
	
	%\author{Julie Parreaux}
	\date{}
	\maketitle
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
	
	\section*{Motivation}
	
	\subsection*{Défense}
	
	\begin{itemize}[label=$\to$]
		\item Construction des corps finis
		\item Analogie à $\C$ (irréductibilité des polynômes)
		\item Connaître des corps où les polynômes ont des racines
	\end{itemize}
	
	
	\subsection*{Ce qu'en dit le jury}
	
	La présentation du bagage théorique permettant de définir corps de rupture, corps de décomposition, ainsi que des illustrations dans différents types de corps (réel, rationnel, corps finis) sont inévitables. Les corps finis peuvent être illustrés par des exemples de polynômes irréductibles de degré $2$, $3$, $4$ sur $\F_2$ ou $\F_3$. Il est nécessaire de présenter des critères d’irréductibilité de polynômes et des polynômes minimaux de quelques nombres algébriques.
	
	Il faut savoir qu’il existe des corps algébriquement clos de caractéristique nulle autres que $\C$; il est bon de savoir montrer que l’ensemble des nombres algébriques sur le corps $\Q$ des rationnels est un corps algébriquement clos. Le théorème de la base téléscopique, ainsi que les utilisations arithmétiques (utilisation de la divisibilité) que l’on peut en faire dans l’étude de l’irréductibilité des polynômes, est incontournable.
	
	\section*{Métaplan}
	
	\begin{footnotesize}
		\emph{Cadre}: $A$ est un anneau commutatif unitaire intègre, $k$ est un corps
	\end{footnotesize}
	
	\begin{description}
		\item[I.] \textbf{Notion de polynôme irréductible \cite[p.9]{Gozard}}
		
		\begin{small}
			\textblue{Savoir si un polynôme est irréductible ou non va nous être essentiel dans la suite: on ne peut faire des corps de rupture ou de décomposition que pour des polynômes irréductibles (il est donc important de les connaître).}
		\end{small}
		\begin{description}
			\item[A.] \emph{Définition et première propriété}
			
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \emph{Définition}: polynôme irréductible
						\item \emph{Proposition}: caractérisation par le quotient
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \emph{Proposition}: Infinité de polynômes irréductibles
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[B.] \emph{Lien entre irréductibilité et racines}
			
			\begin{small}
				\textblue{Ce lien est très important et va justifier les extensions de corps que nous allons réaliser.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \emph{Proposition}: Caractérisation de l'irréductibilité avec les racines
						\item \emph{Remarque}: \textred{Attention}: aux cas des degré 2 et 3
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \emph{Remarque}: Irréductibilité et sous-corps
						\item \emph{Proposition}: Cas parfait : irréductibilité et dérivation
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[C.] \emph{Généralités sur les anneaux factoriels}
			
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \emph{Cadre}: $A$ factoriel et $k =\mathrm{Frac}(A)$.
						\item \emph{Définition}: Contenu et polynôme primitif
						\item \emph{Lemme}: Gauss
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \emph{Théorème}: Gauss
						\item \emph{Application}: Critère d'irréductibilité
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[D.] \emph{Critère d'irréductibilité}
			
			\begin{small}
				\textblue{On cherche des moyens afin de savoir si un polynôme est irréductible ou non (degré est supérieur à 2).}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \emph{Théorème}: Critère d'Eisenstein
						\item \emph{Applications}: Exemples de polynômes irréductibles
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \emph{Théorème} \cite[p.77]{Perrin}: Critères de réduction
						\item \emph{Contre-exemple}: $X^4 + 1$ irréductible sur $\Z$ mais sur aucun des $\F_p$ pour $p$ premier
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
		\end{description}
		\item[II.] \textbf{Corps de rupture et autres extensions de corps}
		
		\begin{small}
			\textblue{Étant donnée un polynôme irréductible, peut-on trouver un corps dans lequel ce polynôme a une racine?}
		\end{small}
		
		\begin{footnotesize}
			\emph{Cadre}: $K$ et $L$ deux corps.
		\end{footnotesize}
		
		\begin{description}
			\item[A.] \emph{Extension algébrique et polynôme minimal \cite[p.65]{Perrin}}
			
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \emph{Théorème}: Base télescopique
						\item \emph{Corollaire}: Cas fini
						\item \emph{Définition}: Nombre algébrique et polynôme minimal
						\item \emph{Proposition} \cite[p.782]{Berhuy}: Caractérisation d'un nombre algébrique
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \emph{Remarque} \cite[p.782]{Berhuy}: $\frac{K[X]}{min(a, K)} \simeq K[a]$
						\item \emph{Définition}: Extension monogène (ou simple)
						\item \emph{Définition}: Extension algébrique
						\item \emph{Proposition}: Extension finie $\Rightarrow$ extension algébrique
						\item \emph{Remarque}: Réciproque est fausse + contre-exemple
						\item \emph{Théorème}: Corps des éléments algébrique
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[B.] \emph{Corps des racines d'un polynôme \cite[p.57]{Gozard}}
			
			\begin{small}
				\textblue{Ce sont des corps où on adjoint des racines (on prend l'union étendu afin d'en conserver la structure de groupe.)}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.30\textwidth}
						\item \emph{Définition}: Corps de rupture
						\item \emph{Remarque}: Extension algébrique
						\item \emph{Théorème} \cite[p.70]{Perrin}: Existence et unicité
						\item \emph{Contre-exemple} \cite[p.820]{Berhuy}: $f$ non irréductible : pas d'unicité
						\item \emph{Théorème}: Critère d'irréductibilité avec les extensions de corps
					\end{minipage} \hfill
					\begin{minipage}{.5\textwidth}
						\item \emph{Théorème} \cite[p.79]{Perrin}: Irréductibilité et extensions
						\item \emph{Définition}: Corps de décomposition
						\item \emph{Remarque}: Extension algébrique finie
						\item \emph{Théorème} \cite[p.71]{Perrin}: Existence et unicité.
						\item \emph{Application} (algèbre linéaire): Décomposition de Dunford \textred{DEV}
						\textblue{(on se place sur cet anneaux pour étudier la décomposition de Dunford via la méthode de Newton...)}
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[C.] \emph{Clôtures algébriques \cite[p.62]{Gozard}}
			
			\begin{small}
				\textblue{La notion de corps algébriquement clos: garder en tête que $\C$ n'est pas le seul corps algébriquement clos de caractéristique $0$.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \emph{Définition}: Corps algébriquement clos
						\item \emph{Théorème}: Alembert--Gauss
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \emph{Définition}: Clôture algébrique
						\item \emph{Théorème}: Théorème de Steinitz
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
		\end{description}
		\item[III.] \textbf{Application aux corps finis}
		
		\begin{small}
			\textblue{Les résultats que nous avons vus permettent de construire les corps finis. De plus, il existe un algorithme permettant de factoriser les polynômes dans les corps finis : l'algorithme de Berlekamp.}
		\end{small}
		\begin{description}
			\item[A.] \emph{Construction des corps finis \cite[p.73]{Perrin}}
			
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \emph{Théorème}: Construction des corps finis via les corps de décompositions
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \item \emph{Application}: Construction explicité de $\F_4$ et $\F_9$
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[B.] \emph{Factorisation des corps finis \cite[p.244]{BeckMalikPeyre}}
			
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \emph{Algorithme}: Berlekamp \textblue{(test l'irréductibilité)} \textred{DEV}
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \emph{Application}: Factorisation
					\end{minipage} 
				\end{itemize}
			\end{footnotesize}
		\end{description}
	\end{description}
	
	\section*{Quelques notions importantes}
	
	\subsection*{Extension de corps}
	\input{./../Notions/Corps_extension.tex}
	

	
\end{document}