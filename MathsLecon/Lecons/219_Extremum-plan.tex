\chapter*{Leçon 219: Extremums : existence, caractérisation, recherche. Exemples et applications.}
\addcontentsline{toc}{chapter}{Leçon 219: Extremums : existence, caractérisation, recherche. Exemples et applications.}

\titlebox{blue}{Références pour la leçon}{
	\textblue{\cite{BeckMalikPeyre}} Beck, Malik et Peyre, \emph{Objectif agrégation.}
	
	\textblue{\cite{Hauchecorne}} Hauchecorne, \emph{Les contre-exemples en mathématiques.}
	
	\textblue{\cite{Rouviere}} Rouvière, \emph{Petit guide de calcul différentiel à l'usage de la licence et de l'agrégation.}
	
	\textblue{\cite{Testard}} Testard, \emph{Analyse mathématique. La maîtrise de l'implicite.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.65\textwidth}
		Théorème des extremums liés et inégalité d'Hadamard
	\end{minipage} \hfill
	\begin{minipage}{.3\textwidth}
		Gradient à pas optimal
	\end{minipage}
}

\section*{Motivation}

\subsection*{Défense}

\begin{itemize}
	\item Les problèmes d'existences utilisent souvent la notion d'extremum.
	\item On cherche à savoir quels sont les éléments maximaux de notre fonction.
	\item En fonction de l'espace de départ et des propriétés de la fonction: plus ou moins facile.
\end{itemize}


\subsection*{Ce qu'en dit le jury}

Comme souvent en analyse, il est tout à fait opportun d’illustrer dans cette leçon un exemple ou un raisonnement à l’aide d’un dessin. Il faut savoir faire la distinction entre propriétés locales (caractérisation d’un extremum local) et globales (existence par compacité, par exemple). Dans le cas important des fonctions convexes, un minimum local est également global. Les applications de la minimisation des fonctions convexes sont nombreuses et elles peuvent illustrer cette leçon.

L’étude des algorithmes de recherche d’extremums y a toute sa place : méthode du gradient et analyse de sa convergence, méthode à pas optimal,... Le cas particulier des fonctionnelles sur $\R^n$ de la forme $\frac{1}{2}(Ax|x) - (b|x)$, où $A$ est une matrice symétrique définie positive, ne devrait pas poser de difficultés (la coercivité de la fonctionnelle pose problème à de nombreux candidats). Les problèmes de minimisation sous contrainte amènent à faire le lien avec les extrema liés et la notion de multiplicateur de Lagrange. Sur ce point, certains candidats ne font malheureusement pas la différence entre recherche d’extremums sur un ouvert ou sur un fermé. Une preuve géométrique des extrema liés sera 	fortement valorisée par rapport à une preuve algébrique, formelle et souvent mal maîtrisée. On peut ensuite mettre en œuvre ce théorème en justifiant une inégalité classique : arithmético-géométrique, Hölder, Carleman, etc... Enfin, la question de la résolution de l’équation d’Euler-Lagrange peut donner l’opportunité de mentionner la méthode de Newton.

Les candidats pourraient aussi être amenés à évoquer les problèmes de type moindres carrés, ou, dans un autre registre, le principe du maximum et des applications.

\section*{Métaplan}

\begin{description}
	\item[I.] \textbf{Problème d'extremum \cite[p.370]{Rouviere}}
	\begin{small}
		\textblue{Nous commençons par fixer le cadre de notre leçon: nous nous plaçons sur un espace vectoriel de dimension finie. De plus, nous étudions uniquement les fonctions continues. La différence entre un extremum local et global est à la portée de celui-ci: un extremum global est un champion du monde et un extremum local est un champion régional.}
	\end{small}
	\begin{footnotesize}
		\begin{itemize}[label=$\rightsquigarrow$]
			\begin{minipage}{.55\textwidth}
				\item \cadre: Espace vectoriel de dimension finie (norme euclidienne)
			\end{minipage} \hfill
			\begin{minipage}{.25\textwidth}
				\item \cadre: Fonction continue
			\end{minipage}
			\begin{minipage}{.5\textwidth}
				\item \definiton: Extremum
				\item \definiton: Extremum global 
			\end{minipage} \hfill
			\begin{minipage}{.32\textwidth}
				\item \definiton: Extremum local 
				\item \definiton: Extremum strict 
			\end{minipage}
		\end{itemize}
	\end{footnotesize}
	\item[II.] \textbf{Extremum et aspects topologiques \cite[p.44]{Testard}}
	\begin{small}
		\textblue{L'utilisation des notions topologiques nous permet de nous assurer de l'existence des extremums. Ces extremums sont globaux.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Utilisation de la compacité}
		\begin{small}
			\textblue{La compacité est une notion topologique qui nous assure l'existence d'un maximum et d'un minimum local.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.42\textwidth}
					\item \proposition: Image continue d'un compact
					\item \exemple: Norme infinie sur boule unité
				\end{minipage} \hfill
				\begin{minipage}{.44\textwidth}
					\item \thm: Existence du minimum et du maximum
					\item \cexemple~\cite[p.202]{Hauchecorne}: Continuité
				\end{minipage}
				\item \application: Équivalence de normes
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Extremum sur un non-compact}
		\begin{small}
			\textblue{Dans le cas d'un ensemble non-compact (et en particulier non borné), nous définissons les fonctions coersives qui nous donne l'existence.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \principe: Où sont les extremums?
					\item \definiton: Fonction coersive
					\item \thm: Minimum d'une fonction coersive
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \definiton: Distance d'une partie
					\item \interpretation: Va à l'infini sur les bords
					\item \application: Théorème de d'Alembert
				\end{minipage}
				\item \thm: Maximum d'une fonction tendant vers $0$
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[III.] \textbf{Extremum et aspects différentiels \cite[p.147]{Testard}}
	\begin{small}
		\textblue{La notion de différentielle nous permet de faire une caractérisation locale des extremums. Cette caractérisation est local et dépend de la régularité des fonctions.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Utilisation de la différentielle}
		\begin{small}
			\textblue{À l'ordre un, on est uniquement capable de définir les points critiques (nous ne pouvons en donné la nature).}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \cadre: $f$ est $\mathcal{C}^1$
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \definiton: Points critiques
				\end{minipage}
				\item \thm: Caractérisation via les points critiques
				
				\begin{minipage}{.4\textwidth}
					\item \cexemple: Réciproque
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \application~\cite[p.77]{Testard}: Théorème de Rolle
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Utilisation des différentielles secondes}
		\begin{small}
			\textblue{À l'ordre deux, nous sommes capable d'identifier la nature des points critiques non dégénérés.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \cadre: $f$ est $\mathcal{C}^1$
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \definiton: Points critiques non dégénérés
				\end{minipage} 
				\item \thm: Caractérisation via ces points critiques
				
				\begin{minipage}{.4\textwidth}
					\item \remarque: Lien avec valeurs propres
					\item \application: Perpendiculaire communes
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \remarque: Local
					\item \remarque: Passer du local au global
				\end{minipage} 
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Méthode de recherche des extremums}
		\begin{small}
			\textblue{Nous avons alors une méthode de recherche des extremums qui commence par l'application de la méthode de Newton.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \methode: Étapes de la recherche
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \application: Recherche de points critiques
				\end{minipage} 
				\item \thm~\cite[p.152]{Rouviere}: Méthode de Newton
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[IV.] \textbf{Extremum sous contraintes \cite[p.20]{BeckMalikPeyre}}
	\begin{small}
		\textblue{Nous cherchons les extremums sous contraintes qui peuvent être une courbe, une surface ou autrement. Une analogie: les extremums libres sur une montagne sont ses sommets, les extremums liées sont les cols de dans notre montagne.}
	\end{small}
	\begin{footnotesize}
		\begin{itemize}[label=$\rightsquigarrow$]
			\begin{minipage}{.32\textwidth}
				\item \textred{\thm: Extremums liées}
				\item \application: Inégalités
			\end{minipage} \hfill
			\begin{minipage}{.55\textwidth}
				\item \textred{\application~\cite[p.372]{Rouviere}: Inégalité de Hadamard}
				\item \application: Diagonalisation des endomorphismes symétriques 
			\end{minipage}
		\end{itemize}
	\end{footnotesize}
	\item[V.] \textbf{Extremum et apport de la convexité \cite[p.245]{Testard}}
	\begin{small}
		\textblue{La notion de convexité fait un lien entre global et local car les extremums locaux d'une fonction convexe sont globaux. Cette remarque va alors nous facilité notre recherche d'extremum}
	\end{small}
	\begin{description}
		\item[A.] \emph{Extremum et convexité}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Fonction convexe / concave
					\item \thm: Caractérisation des extremums
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \proposition: Continuité / Lipschitz
					\item \remarque: Local = global
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Recherche par méthode de descente}
		\begin{small}
			\textblue{La connexité permet d'accéléré la recherche (pas d'extremums locaux, une convergence plus rapide). Cependant si les fonctions ne sont pas convexes ces méthodes s'appliquent quand même mais peuvent tomber dans des extremums locaux.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.48\textwidth}
					\item \remarque: Permet d'avoir une meilleure recherche
				\end{minipage} \hfill
				\begin{minipage}{.35\textwidth}
					\item \application: Formes quadratiques
				\end{minipage}
				\begin{minipage}{.4\textwidth}
					\item \proposition: Convergence de Newton
					\item \principe: Méthode de descente
					\item \methode: Gradient à pas fixe
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \textred{\methode: Gradient à pas optimal}
					\item \application: Formes quadratiques
					\item \remarque: Si non convexe
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
\end{description}
