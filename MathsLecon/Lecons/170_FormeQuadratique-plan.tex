\chapter*{Leçon 170 : Formes quadratiques sur un espace vectoriel de dimension finie. Orthogonalité, isotropie. Applications.}
\addcontentsline{toc}{chapter}{Leçon 170 : Formes quadratiques sur un espace vectoriel de dimension finie. Orthogonalité, isotropie. Applications.}

\titlebox{blue}{Références pour la leçon}{	
	\textblue{\cite{Caldero-Germoni}} Caldero et Germoni, \emph{Histoires hédonistes de groupes et de géométries, tome 1.}
	
	\textblue{\cite{SeguinPazzis}} Seguin Pazzis, \emph{Invitation aux formes quadratiques.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.35\textwidth}
		Étude de $O(p, q)$
	\end{minipage} \hfill
	\begin{minipage}{.5\textwidth}
		Loi de réciprocité quadratique
	\end{minipage}
}

\section*{Motivation}

\subsection*{Défense}

\begin{itemize}
	\item Rassembler les théories géométriques et arithmétiques.
	\item \textblue{Il y a plusieurs points de vus sur les formes quadratiques plus ou moins adaptées à une situation: via les formes bilinéaires symétriques, via les fonctions polynomiales homogènes ou via une représentation matricielle.}
\end{itemize}

\subsection*{Ce qu'en dit le jury}

Il faut tout d’abord noter que l’intitulé implique implicitement que le candidat ne doit pas se contenter de travailler sur $\R$. Le candidat pourra parler de la classification des formes quadratiques sur le corps des complexes et sur les corps finis. L’algorithme de Gauss doit être énoncé et pouvoir être mis en œuvre sur une forme quadratique simple.

Les notions d’isotropie et de cône isotrope sont un aspect important de cette leçon. On pourra rattacher cette notion à la géométrie différentielle.

\section*{Métaplan}

\begin{footnotesize}
	\emph{Cadre}: $K$ un corps de caractéristique différente de $2$; $E$ un $K$-espace vectoriel de dimension finie
\end{footnotesize}

\begin{description}
	\item[I.] \textbf{Formes quadratiques \cite[p.24]{SeguinPazzis}}
	\begin{small}
		\textblue{Nous allons présenter les différentes formes quadratiques et leurs premières applications. On commence par présenter les formes quadratiques définies par une forme bilinéaire symétrique. Ensuite, nous enchaînons avec la représentation matricielle qui nous permet de faciliter nos calculs. Pour finir, on fait le lien avec les polynômes. On remarque alors que sur $\R$, on peut ainsi définir la notion de produit scalaire qui est une forme bilinéaire symétrique particulière et donc une forme quadratique particulière.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Formes quadratiques via les formes bilinéaires}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.35\textwidth}
					\item \definiton: Forme bilinéaire
					\item \definiton: Alternée
					\item \proposition: Alternée $\Leftrightarrow$ antisymétrique
					\item \definiton: Forme quadratique
					\item \proposition: Forme polaire (unicité)
					\item \proposition: Formule de polarisation
					\item \definiton: Domaine et universalité
				\end{minipage} \hfill
				\begin{minipage}{.44\textwidth}
					\item \definiton: Symétrique
					\item \definiton: Antisymétrique
					\item \remarque: Caractéristique $2$
					\item \remarque: Équivalence entre nulle et alternée
					\item \exemple: Espace hyperbolique canonique
					\item \application: Recherche de forme bilinéaire associée
					\item \definiton: Rang et noyau
				\end{minipage}
				\item \definiton: Non dégénérée
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Formes quadratiques via la représentation matricielle}
		\begin{small}
			\textblue{Pour avoir une matrice, il nous faut une base. Cette représentation nous oblige à connaître ou à trouver une base de notre espace vectoriel (on sait qu'il en existe une, on est en dimension finie). On a des propriétés de changement de base, on voit alors apparaître la théorie des matrices conjuguées et des classes de conjugaison.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.35\textwidth}
					\item \definiton: Forme quadratique
				\end{minipage} \hfill
				\begin{minipage}{.5\textwidth}
					\item \remarque: Lien avec forme bilinéaire (matrice associée)
				\end{minipage}
				\begin{minipage}{.4\textwidth}
					\item \exemple: Différentielle seconde (Hessienne)
					\item \corollaire: Classe de conjugaison
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \proposition: Changement de base
					\item \remarque: Formes quadratiques canoniques
				\end{minipage}
				
				\item \definiton: Noyau et rang
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Formes quadratiques via les polynômes}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.36\textwidth}
					\item \definiton: Forme quadratique
				\end{minipage} \hfill
				\begin{minipage}{.46\textwidth}
					\item \remarque: Lien avec les définitions précédentes
				\end{minipage}
				\item \definiton: Noyau et rang
				\item \application: Déterminant non forme quadratique mais le coefficient de $X^{n-2}$ dans le polynôme caractéristique l'est \textblue{(application dans l'étude du cône nilpotent)}.
			\end{itemize}
		\end{footnotesize}
		\item[D.] \emph{Notion de produit scalaire \cite[p.102]{SeguinPazzis}}		
		\begin{small}
			\textblue{Le produit scalaire est une application bilinéaire symétrique définie positive. Autrement dit, dans le cadre des formes quadratiques, c'est une forme associée à une forme bilinéaire définie positive.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \definiton: Forme bilinéaire définie, positive, négative
				
				\begin{minipage}{.45\textwidth}
					\item \remarque: Cas de la représentation matricielle
					\item \corollaire: Inégalité de Minkowski
				\end{minipage} \hfill
				\begin{minipage}{.38\textwidth}
					\item \proposition: Inégalité de Cauchy--Schwartz
					\item \proposition: $\sqrt{q}$ est une norme
				\end{minipage}
				
				\item \thm: Théorème de Schwartz pour application deux fois différentiables
				\item \application: Recherche d'extremum
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[II.] \textbf{Orthogonalité et isotropie}
	\begin{description}
		\item[A.] \emph{Orthogonalité via les formes quadratiques \cite[p.67]{SeguinPazzis}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Vecteur orthogonaux
					\item \proposition: Théorème de Pythagore
					\item \exemple: Interprétation géométrique
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \proposition: Lien avec symétrique
					\item \definiton: Sous-espace vectoriel orthogonaux
					\item \proposition: Propriétés des orthogonaux
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Groupe orthogonal \cite[p.54]{SeguinPazzis}}
		\begin{small}
			\textblue{On souhaite conserver les endomorphismes qui préservent les formes quadratiques. Ceux-ci ont une structure de groupe: on les appelle groupe orthogonal.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.45\textwidth}
					\item \definiton: Automorphisme orthogonaux
					\item \proposition: Déterminant des éléments de $O(q)$
					\item \definiton~\cite[p.210]{Caldero-Germoni}: Groupe $O(p,q)$
				\end{minipage} \hfill
				\begin{minipage}{.35\textwidth}
					\item \definiton: Groupe orthogonaux
					\item \definiton: Groupe spécial orthogonal
					\item \textred{\proposition~\cite[p.210]{Caldero-Germoni}: Étude de $O(p, q)$}
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Base orthonormée et réduction \cite[p.57]{SeguinPazzis}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.45\textwidth}
					\item \definiton: Base $q$-orthogonale
					\item \proposition: Caractérisation
					\item \corollaire: Conséquences
					\item \methode~\cite[p.9]{SeguinPazzis}: Méthode de Gauss matricielle
				\end{minipage} \hfill
				\begin{minipage}{.35\textwidth}
					\item \remarque: Base orthonormée
					\item \thm: Existence
					\item \methode: Méthode de Gauss analytique
					\item \remarque: Lien entre les deux
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[D.] \emph{Isotropie}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Vecteur isotrope
					\item \definiton: Cône isotrope
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \remarque: Mêmes vecteurs isotropes
					\item \proposition: Noyau inclut dans le cône
				\end{minipage}
				\item \remarque: Réciproque est fausse
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[III.] \textbf{Classification des formes quadratiques}
	\begin{description}
		\item[A.] \emph{Le problème de la classification \cite[p.39]{SeguinPazzis}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \definiton: Formes quadratiques équivalentes
				\item \proposition: Caractérisation de  l'équivalence
				\item \proposition: Caractérisation via les classes d'équivalence
				\item \proposition: Équivalence via le rang et le noyau
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Classification sur $\C$ \cite[p.100]{SeguinPazzis}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \thm: Théorème de classification
				\end{minipage} \hfill
				\begin{minipage}{.45\textwidth}
					\item \corollaire: Régulière implique base orthonormale
				\end{minipage} 
				\item \corollaire: Matrices diagonalisables
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Classification sur $\R$ \cite[p.104]{SeguinPazzis}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Signature
					\item \corollaire: Même signature
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \thm: Théorème d'inertie de Sylvester
					\item \remarque: Lien avec Gauss
				\end{minipage} 
			\end{itemize}
		\end{footnotesize}
		\item[D.] \emph{Classification sur un corps fini \cite[p.182]{Caldero-Germoni}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \thm: Théorème de classification
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \textred{\application: Loi de réciprocité quadratique}
				\end{minipage} 
			\end{itemize}
		\end{footnotesize}
	\end{description}
\end{description}