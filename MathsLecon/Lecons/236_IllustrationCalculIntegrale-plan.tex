\chapter*{Leçon 236: Illustration par des exemples quelques méthodes de calcul d'intégrales.}
\addcontentsline{toc}{chapter}{Leçon 236: Illustration par des exemples quelques méthodes de calcul d'intégrales.}

\titlebox{blue}{Références pour la leçon}{	
	\textblue{\cite{BriancePages}} Briance et Pagès, \emph{Théorie de l'intégration.}
	
	\textblue{\cite{Demailly}} Demailly, \emph{Analyse numérique et équations différentielles.}
	
	\textblue{\cite{Gourdon-analyse}} Gourdon, \emph{Analyse.}
	
	\textblue{\cite{ZuilyQueffelec2}} Queffelec et Zuily, \emph{Éléments d'analyse pour l'agrégation.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.35\textwidth}
		Intégrale de Fresnel
	\end{minipage} \hfill
	\begin{minipage}{.5\textwidth}
		Formule de Poisson et fonction $\Theta$
	\end{minipage}
}

\section*{Motivation}

\subsection*{Défense}

\subsection*{Ce qu'en dit le jury}

Cette leçon doit être très riche en exemples, que ce soit l’intégrale $\int_0^{+\infty} \frac{\sin t}{t} dt$ ou bien d’autres encore. Il est tout à fait pertinent de commencer par les différentes techniques élémentaires (intégration par parties, changement de variables, décomposition en éléments simples, intégrale à paramètres,...). On peut également présenter des utilisations du théorème des résidus, ainsi que des exemples faisant intervenir les intégrales multiples comme le calcul de l’intégrale d’une gaussienne. Le calcul du volume de la boule unité de $R^n$ ne doit pas poser de problèmes insurmontables. Le calcul de la transformation de Fourier d’une gaussienne a sa place dans cette leçon.

On peut aussi penser à l’utilisation du théorème d’inversion de Fourier ou du théorème de Plancherel. Certains éléments de la leçon précédente, comme par exemple l’utilisation des théorèmes de convergence monotone, de convergence dominée et/ou de Fubini, sont aussi des outils permettant le calcul de certaines intégrales.

Enfin, il est tout à fait pertinent d’évoquer les méthodes de calcul approché d’intégrales (méthodes des rectangles, méthode de Monte-Carlo, etc.).

\section*{Métaplan}

\begin{description}
	\item[I.] \textbf{Méthodes de calcul direct}
	\begin{description}
		\item[A.] \emph{Utilisation des primitives \cite[p.133]{Gourdon-analyse}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.45\textwidth}
					\item \thm~\cite[p.123]{Gourdon-analyse}: Existence d'une primitive
					\item \methode: Décomposition en éléments simples
				\end{minipage} \hfill
				\begin{minipage}{.45\textwidth}
					\item \exemple: Fonctions usuelles (complément annexe)
					\item \methode: Linéarisation des $\cos$ et des $\sin$
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Intégration par partie}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.5\textwidth}
					\item \thm~\cite[p.123]{Gourdon-analyse}: Intégration par partie
					\item \exemple $\int_{0}^{x} \arctan(u) du$
					\item \application~\cite[p.211]{Gourdon-analyse}: Stirling
					\item \methode~\cite[p.137]{Gourdon-analyse}: $\int \sqrt{ax^2 + bx + c} dx$
					\item \thm: Fubini
				\end{minipage} \hfill
				\begin{minipage}{.44\textwidth}
					\item \exemple $\int_{0}^{x} \ln(u) du$
					\item \exemple~\cite[p.126]{Gourdon-analyse}: Intégrale de Wallis
					\item \exemple: Fonction $\Gamma(n + 1) = n!$
					\item \exemple: $\int \sqrt{t^2 - 1} dt$
					\item \application~\cite[p.265]{BriancePages}: Gauss
				\end{minipage}
				\item \textred{\exemple: Intégrale de Fresnel}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Changement de variables \cite[p.139]{Gourdon-analyse}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.42\textwidth}
					\item \thm: Cas de la dimension $1$
					\item \application~\cite[p.124]{Gourdon-analyse}: Fonctions impaires
					\item \exemple: Polynôme en $\cos$ et en $\sin$
					\item \exemple: Règle de Bioche
				\end{minipage} 
				\begin{minipage}{.4\textwidth}
					\item \application~\cite[p.124]{Gourdon-analyse}: Fonctions paires
					\item \application~\cite[p.124]{Gourdon-analyse}: Fonctions périodiques
					\item \exemple: Fraction rationnelle en $\cos$ et en $\sin$
					\item \exemple: Intégrale Abélienne
				\end{minipage}
				\item \thm~\cite[p.335]{Gourdon-analyse}: Cas de la dimension $d$
				\item \application~\cite[p.335]{Gourdon-analyse}: Passage en coordonnées polaires
				\item \application~\cite[p.246]{BriancePages}: Volume de la boule unité
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[II.] \textbf{Calculs via des fonctions auxiliaires et leurs propriétés}
	\begin{description}
		\item[A.] \emph{Suites et séries de fonctions}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.42\textwidth}
					\item \thm~\cite[p.117]{BriancePages}: Beppo--Lévi
					\item \thm~\cite[p.134]{BriancePages}: Convergence dominée
				\end{minipage} \hfill
				\begin{minipage}{.44\textwidth}
					\item \application: TCL
					\item \exemple: Limite de $I_n(\alpha) = \int_{0}^{n} \left(1 + \frac{x}{n}\right)^ne^{-\alpha x} dx$
				\end{minipage}
				\begin{minipage}{.5\textwidth}
					\item \thm~\cite[p.137]{BriancePages}: Interversion série-intégrale
				\end{minipage} \hfill
				\begin{minipage}{.3\textwidth}
					\item \exemple: Calcul de $\int_{1}^{1} \frac{\ln t}{1 + t^2} dt$
				\end{minipage}
				\item \application: Théorème de Borel--Canteli 
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Régularité des intégrales à paramètres \cite[p.138]{BriancePages}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.4\textwidth}
					\item \thm: Continuité
					\item \thm: Dérivabilité
					\item \application: Convolution
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \application: Convolution
					\item \remarque: Analogue pour $\Ck^{\infty}$
					\item \exemple: $\Gamma$ est $\Ck^{\infty}$
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Transformée de Fourier}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.42\textwidth}
					\item \definiton~\cite[p.140]{BriancePages}: Transformée de Fourier
					\item \thm~\cite[p.330]{ZuilyQueffelec2}: Inversion
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \thm~\cite[p.140]{BriancePages}: Régularité de $\hat{f}$
					\item \textred{\application~\cite[p.272]{Gourdon-analyse}: Formule de Poisson}
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[III.] \textbf{Méthodes de calcul approché}
	\begin{description}
		\item[A.] \emph{Sommes de Riemman \cite[p.124]{Gourdon-analyse}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.45\textwidth}
					\item \definiton: Somme de Riemann
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \thm: Calcul de l'intégral
				\end{minipage}
				\item \application: Calcul pour les fonctions continues par morceaux
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Méthodes numériques \cite[p.63]{Demailly}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.4\textwidth}
					\item \methode: Rectangle
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \methode: Trapèzes
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Méthode de Monté--Carlos}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.4\textwidth}
					\item \methode: Monté--Carlos
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \thm: Correction
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
\end{description}

