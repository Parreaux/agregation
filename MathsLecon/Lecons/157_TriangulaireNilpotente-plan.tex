\chapter*{Leçon 157 : Endomorphisme triangulaire. Endomorphisme nilpotent.}
\addcontentsline{toc}{chapter}{Leçon 157 : Endomorphisme triangulaire. Endomorphisme nilpotent.}

\titlebox{blue}{Références pour la leçon}{
	\textblue{\cite{BeckMalikPeyre}} Beck, Malik et Peyre, \emph{Objectif agrégation.}
	
	\textblue{\cite{Berhuy}} Berhuy, \emph{Algèbre: le grand combat.}
	
	\textblue{\cite{Caldero-Germoni2}}, Caldero et Germoni, \emph{Histoires hédonistes de groupes et de géométries, tome 2.}
	
	\textblue{\cite{Gourdon-algebre}} Gourdon, \emph{Algèbre.}
	
	\textblue{\cite{Hauchecorne}} Hauchecorne, \emph{Les contre-exemples en mathématiques.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.35\textwidth}
		Cardinal du cône nilpotent
	\end{minipage} \hfill
	\begin{minipage}{.5\textwidth}
		Décomposition de Dunford
	\end{minipage}
}

\section*{Motivation}

\subsection*{Défense}

La réduction d'un endomorphisme en dimension fini consiste à trouver une représentation plus simple dans une base.
\begin{itemize}
	\item Faciliter les calculs des puissances et des exponentielles (résolution d'équations différentielles et systèmes dynamiques discrets).
	\item Classer les endomorphismes à similitude près.
\end{itemize}
Les endomorphismes trigonalisables et nilpotents sont les endomorphismes que l'on cherche à obtenir lors de ces décompositions.

\subsection*{Ce qu'en dit le jury}

Il est bon de savoir expliquer pourquoi l’application induite par un endomorphisme trigonalisable (respectivement nilpotent) sur un sous-espace stable est encore trigonalisable (respectivement nilpotent). L’utilisation des noyaux itérés est fondamentale dans cette leçon, par exemple pour déterminer si deux matrices nilpotentes sont semblables. Il est intéressant de présenter des conditions suffisantes de trigonalisation simultanée ; l’étude des endomorphismes cycliques a toute sa place dans cette leçon. L’étude des nilpotents en dimension 2 débouche naturellement sur des problèmes de quadriques et l’étude sur un corps fini donne lieu à de jolis problèmes de dénombrement.

S’ils le désirent, les candidats peuvent aussi présenter la décomposition de Frobenius, ou des caractérisations topologiques des endomorphismes nilpotents, ou encore des propriétés topologiques de l’ensemble des endomorphismes nilpotents.

\section*{Métaplan}

\begin{description}
	\item[I.] \textbf{Outils \cite[p.956]{Berhuy}}
	\begin{small}
		\textblue{Nous présentons ici les premiers outils mathématiques nécessaires à cette leç:on. Pour le lemme des noyaux si on a un polynôme annulateur de $u$, on peut décomposer $E$ en somme directe de sous espaces $u$-stables. Si on concatène des bases de ces sous espaces, on obtient une base dans laquelle la matrice de $u$ est diagonale par blocs c'est-à-dire simple}
	\end{small}
	\begin{footnotesize}
		\begin{itemize}[label=$\rightsquigarrow$]
			\begin{minipage}{.45\textwidth}
				\item \emph{Définitions}: Valeurs propres
				\item \emph{Définition}: Polynôme minimal
				\item \emph{Proposition}: Caractérisation des valeurs propres
				\item \emph{Application}: Démonstration par récurrence
			\end{minipage} \hfill
			\begin{minipage}{.35\textwidth}
				\item \emph{Définition}: Sous-espaces propre
				\item \emph{Définition}: Polynôme minimal 
				\item \emph{Proposition}: Propriétés de stabilité
				\item \emph{Théorème}: Lemme des noyaux
			\end{minipage}
		\end{itemize}
	\end{footnotesize}
	\item[II.] \textbf{Endomorphisme trigonalisable \cite[p.956]{Berhuy}}
	\begin{small}
		\textblue{Si l'endomorphisme n'est pas diagonalisable, un bon compromis peut être la trigonalisation.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Être trigonalisable}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.38\textwidth}
					\item \emph{Définition}: Endomorphisme triangulaire
					\item \emph{Définition}: Endomorphisme trigonalisable
				\end{minipage} \hfill
				\begin{minipage}{.42\textwidth}
					\item \emph{Remarque}: Semblable (supérieur ou inférieur)
					\item \emph{Proposition}: Matrices trigonalisables
				\end{minipage}
				\item \emph{Proposition}: Caractérisation via $\pi_u$ et $\chi_u$
				\item \emph{Application}: Dans un corps algébriquement clos: tout est trigonalisable
				\item \emph{Application}: $\det(\exp(A)) = \exp(\mathrm{Tr}(A))$ : $\exp(A) \in GL_n(\C)$
				\item \emph{Application}: Suite récurrentes linéaires à coefficients constants
				
				\begin{minipage}{.3\textwidth}
					\item \emph{Proposition}: Stabilité
				\end{minipage} \hfill
				\begin{minipage}{.5\textwidth}
					\item \emph{Proposition}: Expression avec les valeurs propres
				\end{minipage}
				\item \emph{Application}: Spectre des polynômes d'endomorphismes
				\item \emph{Application}: Théorème de Cayley-Hamilton
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Trigonalisation simultanée}
		\begin{small}
			\textblue{Nous sommes également amené à chercher si on peut trigonaliser deux endomorphismes simultanément. Cela revient à chercher une base dans laquelle les deux endomorphismes sont sous forme triangulaire.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Proposition}: Stabilité
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Théorème}: Trigonalisation simultanée
				\end{minipage}
				\item \emph{Remarque}: La réciproque est fausse: cela revient à dire que deux matrices triangulaires commutent ce qui est faux.
				\item \emph{Proposition}: Somme qui commutent
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[III.] \textbf{Endomorphisme nilpotent \cite[p.169]{BeckMalikPeyre}}
	\begin{small}
		\textblue{Un endomorphisme nilpotent est un endomorphisme dont une certaine itéré s'annule. Le calcul de ces puissances itérées est finie (il y a qu'un nombre fini de puissance non nul). Nous allons commencer par caractériser les endomorphismes nilpotents. Ensuite nous étudierons le cône nilpotent (les endomorphismes nilpotent ne sont pas stables par addition donc ce n'est pas un sous-espace vectoriel). Nous finirons par une généralisation de ce concept avec les endomorphismes cycliques.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Être nilpotent}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \emph{Définition}: Endomorphisme nilpotent
				\item \emph{Contre-exemple} \cite[p.67]{Hauchecorne}: $f$ tel que $\forall x, \exists p, f^p(x) = 0$ non nilpotent
				
				\begin{minipage}{.4\textwidth}
					\item \emph{Proposition}: Matrice nilpotente
					\item \emph{Proposition}: Caractérisation de la nilpotente
					\item \emph{Contre-exemple}: En caractéristique $p$: $I_p$
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Indice de nilpotence
					\item \emph{Proposition}: Caractérisation via la trace
					\item \emph{Proposition}: Caractérisation via le quotient
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Nature de l'ensemble des endomorphismes nilpotents}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \emph{Notation}: L'ensemble des endomorphisme nilpotent : $\mathcal{N}(E)$
				
				\begin{minipage}{.45\textwidth}
					\item \emph{Proposition}: $\mathcal{N}(E)$ est un cône
					\item \emph{Proposition}: Opérations sur les nilpotent
				\end{minipage} \hfill
				\begin{minipage}{.35\textwidth}
					\item \emph{Remarque}: Pas de stabilité via l'addition
					\item \emph{Théorème}: $\mathrm{Vect}(\mathcal{N}(E))$
				\end{minipage}
				\begin{minipage}{.45\textwidth}
					\item \textred{\emph{Lemme} \cite[p.213]{Caldero-Germoni2}: Base via les matrices nilpotentes}
				\end{minipage} \hfill
				\begin{minipage}{.33\textwidth}
					\item \textred{\emph{Théorème}: Cardinal du cône nilpotent}
				\end{minipage}  
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Généralisation: endomorphismes cycliques}
		\begin{small}
			\textblue{Les endomorphismes nilpotent se généralisent avec les endomorphismes cycliques. Pour ceux-ci nous ne demandons pas l'annulation des puissances itérées mais que les première puissances itérées forment une base de $E$ (qui est également une propriété des nilpotents). Ce sont les structures de bases de la réduction de Frobenius. Les endomorphismes nilpotents sont donc des endomorphismes cycliques particulier (on n'a pas tout à fait une base de $E$ mais d'un sous-espace vectoriel de $E$).}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Matrice compagnon
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Endomorphisme cyclique
				\end{minipage} 
				\item \emph{Théorème}: Caractérisation
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[IV.] \textbf{Application aux réductions \cite[p.956]{Berhuy}}
	\begin{small}
		\textblue{Les réductions permettent de choisir une bonne base afin qu'une matrice s'écrive de manière simplifier. Elles permettent de simplifier les calculs comme de l'exponentielle de matrice et de classer à similitude près les différents endomorphismes.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Sous-espaces caractéristiques \cite[p.189]{Gourdon-algebre}}
		\begin{small}
			\textblue{Les sous-espaces caractéristiques, à l'instar des sous-espaces propres, donnent une décomposition d'un espace vectoriel. On les calcul à partir du polynôme caractéristique sur lequel on ne fait aucune hypothèse.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Sous-espace caractéristique
					\item \emph{Application}: Diagonalisation par bloc
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Théorème}: Décomposition
					\item \emph{Application}: Puissance de matrices
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Décomposition de Dunford}
		\begin{small}
			\textblue{La décomposition de Dunford est la plus simple et permet de calculer efficacement des puissances de matrices. Cependant, elle ne permet pas de répondre à la question de la classification: montrer que deux matrices sont semblables sous leur décomposition de Dunford, revient à montrer que les deux matrices sont semblables pour la même matrice $P$.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.35\textwidth}
					\item \textred{\emph{Théorème}: Dunford}
				\end{minipage} \hfill
				\begin{minipage}{.45\textwidth}
					\item \emph{Application}: Calcul d'exponentielle de matrices
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Décomposition de Jordan}
		\begin{small}
			\textblue{La décomposition de Jordan vient alors remédier à ce problème: elle permet de caractériser les matrices semblables. On commence par étudier cette décomposition dans le cas particulier des endomorphismes nilpotents.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Bloc de Jordan
					\item \emph{Proposition}: Propriété de cette suite
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Suite de noyaux itérés
					\item \emph{Remarque}: Injectivité de Frobenius
				\end{minipage}
				\item \emph{Théorème}: Décomposition de Jordan pour les endomorphismes nilpotents
				\item \emph{Application}: Caractérisation des endomorphismes semblables
				
				\begin{minipage}{.4\textwidth}
					\item \emph{Contre-exemple}:
					\item \emph{Remarque}: Dimension de $\ker f$
					\item \emph{Proposition} Caractérisation de la cyclicité
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Remarque}: Indice de nilpotence
					\item \emph{Proposition}: Caractérisation de la nilpotence
					\item \emph{Théorème}: Réduction de Jordan
				\end{minipage}
				\begin{minipage}{.55\textwidth}
					\item \emph{Corollaire}: Caractérisation des endomorphismes semblables
				\end{minipage} \hfill
				\begin{minipage}{.25\textwidth}
					\item \emph{Remarque}: Système invariant
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[D.] \emph{Réduction de Frobenius}
		\begin{small}
			\textblue{L'application de la réduction de Jordan demande une connaissance du spectre et donc la factorisation du polynôme minimal. La réduction de Frobenius ne demande pas le calcul des valeurs propres et réalise cette classification.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.5\textwidth}
					\item \emph{Théorème}: Définition de la suite d'invariant de similitude
					\item \emph{Corollaire}: Caractérisation des matrices semblables
				\end{minipage} \hfill
				\begin{minipage}{.3\textwidth}
					\item \emph{Théorème}: Réduction de Frobenius
					\item \emph{Remarque}: Invariant de similitude
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
\end{description}
	