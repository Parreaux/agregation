\documentclass[a4paper,8pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais,english]{babel}%

\usepackage[dvipsnames]{xcolor}

\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{enumitem}
\usepackage{appendix}
\usepackage{stmaryrd}
\usepackage{amsmath}
\usepackage{wasysym}

\usepackage{listings}
\usepackage{multirow}
\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}
\algnewcommand{\LineComment}[1]{\State \(\triangleright\) #1}

\parskip=0\baselineskip %0.5 d'habitude


\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	\newcommand{\F}{\mathbb{F}}
	\newcommand{\Sn}{\mathfrak{S}}
	\newcommand{\A}{\mathfrak{A}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textbluesmall}[1]{{\small{\textcolor{blue}{#1}}}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\theoremstyle{plain}
	\newtheorem{theo}{Théorème}
	\newtheorem{prop}{Proposition}
	\newtheorem{cor}{Corollaire}
	\newtheorem{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem{definition}{Définition}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{appli}{Application}
	\newtheorem*{ex}{Exemple}
	
	\title{Leçon 152: Déterminants. Exemples et applications.}
	
	%\author{Julie Parreaux}
	\date{}
	\maketitle
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
	
	\section*{Motivation}
	
	\subsection*{Défense}
	
	\begin{itemize}
		\item Une première notion de déterminant a été introduite par Cramer (1750) afin de résoudre des systèmes équations linéaires. 
		\item Au cours du $XIX^{ième}$ siècle, Cauchy en donne une première formalisation.
		\item Il existe de nombreuses applications à cette notion à priori algébrique: en algèbre, en géométrie (\textblue{toute l'information contenu dans le déterminant peut être utile et est géométrique}) mais également en analyse. C'est un outils très puissant.
	\end{itemize}
	
	\subsection*{Ce qu'en dit le jury}
	
	Dans cette leçon, il faut commencer par définir correctement le déterminant. Il est possible d’entamer la leçon en disant que le sous-espace des formes $n$-linéaires alternées sur un espace de dimension $n$ est de dimension $1$ et, dans ce cas, il est essentiel de savoir le montrer. Le plan doit être cohérent ; si le déterminant n’est défini que sur $\R$ ou $\C$, il est délicat de définir $\det A - XI_n)$ avec $A$ une matrice carrée. L’interprétation du déterminant comme volume est essentielle. On peut rappeler son rôle dans les formules de changement de variables, par exemple pour des transformations de variables aléatoires.
	
	Le calcul explicite est important, mais le jury ne peut se contenter d’un déterminant de Vandermonde ou d’un déterminant circulant. Les opérations élémentaires permettant de calculer des déterminants, avec des illustrations sur des exemples, doivent être présentées. Il est bienvenu d’illustrer la continuité du déterminant par une application, ainsi que son caractère polynomial. Pour les utilisations des propriétés topologiques, on n’omettra pas de préciser le corps de base sur lequel on se place. 
	
	S’ils le désirent, les candidats peuvent s’intéresser aux calculs de déterminants sur $\Z$ avec des méthodes multimodulaires. Le résultant et les applications simples à l’intersection ensembliste de deux courbes algébriques planes peuvent aussi trouver leur place dans cette leçon pour des candidats ayant une pratique de ces notions.
	
	\section*{Métaplan}
	
	\begin{small}
		\emph{Cadre}: $E$ est un $\mathbb{K}$-espace vectoriel où $\mathbb{K}$ est un corps commutatif.
	\end{small}
	
	\begin{description}
		\item[I.] \textbf{Définition du déterminant \cite[p.134]{Gourdon-algebre}}
		
		\begin{small}
			\textblue{On choisis de se placer sur un corps (la théorie reste vraie pour un anneau commutatif).}
		\end{small}
		\begin{description}
			\item[A.] \emph{Forme $n$-linéaire et déterminant d'une famille de vecteur}
			
			\begin{small}
				\textblue{La définition du déterminant se fait grâce aux formes $n$-linéaires alternées et antisymétriques. Dans le cas d'un corps de caractéristique $2$, il faut faire attention car l'équivalence entre alterné et antisymétrique n'existe plus.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.36\textwidth}
						\item \emph{Définition}: Formes $n$-linéaires
						\item \emph{Définition}: Formes linéaires alternées
						\item \emph{Définition}: Formes linéaires anti-symétrique
						\item \emph{Théorème}: Équivalence entre ces deux formes
						\item \emph{Remarque}: En caractéristique $2$
						\item \emph{Théorème}: Existence du déterminant
						\item \emph{Définition}: Déterminant
					\end{minipage} \hfill
					\begin{minipage}{.46\textwidth}
						\item \emph{Proposition}: Changement de base
						\item \emph{Proposition}: Déterminant et famille liée
						\item \emph{Proposition}: Déterminant et échange de deux lignes
						\item \emph{Proposition}: Déterminant est linéaire en chaque ligne
						\item \emph{Proposition}: Déterminant et combinaison linéaire des lignes
						\item \emph{Remarque}: Fonctionne aussi pour les colonnes
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[B.] \emph{Déterminant d'un endomorphisme et d'une matrice}
			
			\begin{small}
				\textblue{Le déterminant d'une matrice est d'un endomorphisme sont deux notions très proche puisqu'à une base choisie une matrice représente un endomorphisme.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.56\textwidth}
						\item \emph{Définition}: Déterminant d'un endomorphisme
						\item \emph{Définition}: Déterminant d'une matrice 
						\item \emph{Proposition}: Formule explicite
						\item \emph{Proposition}: Lien entre endomorphisme et matrice
						\item \emph{Proposition}: Déterminant et produit (composition)
						\item \emph{Proposition}: Déterminant et inversibilité
						\item \emph{Application}: $A$ et $B$ semblable implique $\det A = \det B$
					\end{minipage} \hfill
					\begin{minipage}{.36\textwidth}
						\item \emph{Remarque}: $\det \begin{pmatrix}
						1 & 0 \\
						1 & 2 \\
						\end{pmatrix}= \det \begin{pmatrix}
						0 & -1 \\
						2 & 1 \\
						\end{pmatrix}$ mais pas semblable.
						\item \emph{Proposition}: Indépendance de la base
						\item \emph{Proposition}: Déterminant et transposé
						\item \emph{Proposition}: Déterminant et inverse de matrice
						\item \emph{Proposition}: $\det (\exp A) = \exp(\mathrm{Tr}(A))$
						\item \emph{Application}: $\exp(A) \in GL_n(\C)$
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
		\end{description}
		\item[II.] \textbf{Méthodes de calculs et déterminants usuelles \cite[p.134]{Gourdon-algebre}}
		
		\begin{small}
			\textblue{Calculer un déterminant peut s'avérer être une tâche difficile.}
		\end{small}
		\begin{description}
			\item[A.] \emph{Calcul dans un cas favorable}
			 
			\begin{small}
				\textblue{Dans le cas de ces matrices, le calcul est facile: on a des formule toute simple.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \emph{Méthode}: Méthode de calcul de matrice $2*2$
						\item \emph{Proposition}: Déterminant de matrice diagonale 
						\item \emph{Remarque}: Cas des matrices diagonales par bloc
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \emph{Proposition}: Déterminant de matrices nilpotentes
						\item \emph{Proposition}: Déterminant de matrices  unipotentes
						\item \emph{Application}: Dunford implique égalité des spectres
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[B.] \emph{Méthodes algorithmiques de calcul}
			
			\begin{small}
				\textblue{Plusieurs méthodes de calculs existent pour pouvoir calculer un déterminant: elles ne sont pas toutes aussi efficaces. L'algorithme de Gauss est un moyen efficace : on met ainsi la matrice sous forme triangulaire supérieure puis on applique la formule dans ce cas. Le calcul du déterminant peut alors se faire en $n^3$.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \emph{Méthode 1}: calcul brut en $n \times n!$
						\item \emph{Définitions}: mineurs, cofacteurs, comatrice
						\item \emph{Proposition}: Développer par les lignes et les colonnes
						\item \emph{Méthode 2}: développement $n!$
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \emph{Méthode 3} \cite[p.205]{Cognet}: Algorithme de Gauss en $n^3$
						\item \emph{Application}: Déterminant de matrice triangulaire
						\item \emph{Remarque}: Cas des matrices triangulaire par bloc 
						\item \emph{Proposition}: Relation avec les comatrices
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[C.] \emph{Exemples de déterminant usuel}
			
			\begin{small}
				\textblue{On présente maintenant quelques déterminants usuels ainsi que leur calcul.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.46\textwidth}
						\item \emph{Calcul} \cite[p.137]{Gourdon-algebre}: Déterminant de Vandermond 
						\item \emph{Application}: Transformée de Fourier discrète rapide (FFT) 
						\item \emph{Calcul} \cite[p.147]{Gourdon-algebre}: Déterminant circulant 
					\end{minipage} \hfill
					\begin{minipage}{.36\textwidth}
						\item \emph{Application}: Suite de polygones \textred{DEV} 
						\item \emph{Calcul} \cite[p.144]{Gourdon-algebre}: Déterminant de Cauchy 
						\item \emph{Application}: Théorème de Müntz
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
		\end{description}
		\item[III.] \textbf{Le déterminant: un outil algébriste}
		
		\begin{small}
			\textblue{Le déterminant est un outil algébrique puissant: il nous permet de calculer des inverse ou de résoudre des systèmes d'équations.}
		\end{small}
		\begin{description}
			\item[A.] \emph{Calcul avec le déterminant \cite[p.137]{Gourdon-algebre}}
			
			\begin{small}
				\textblue{Le déterminant nous permet de calculer l'inverse d'une matrice ou de résoudre un système.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.46\textwidth}
						\item \emph{Théorème}: Inversion d'une matrice par le déterminant
						\item \emph{Application}: $GL_2(\Z)$ sont de déterminant $\{-1, 1\}$ 
						\item \emph{Remarque}: En pratique, on utilise le pivot de Gauss qui nous évite le calcul du déterminant et de la comatrice. 
					\end{minipage} \hfill
					\begin{minipage}{.35\textwidth}
						\item \emph{Définition}: Système de Cramer
						\item \emph{Théorème}: Existence et unicité des solutions
						\item \emph{Proposition}: Formule de Cramer
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[B.] \emph{Déterminant et réduction d'endomorphisme}
			
			\begin{small}
				\textblue{Le déterminant calculer sur un corps de fraction rationnelle nous permet de définir le polynôme caractéristique d'une matrice (ou d'un endomorphisme). Ce polynôme est important car il nous permet de réduire la matrice.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \emph{Cadre}: $\mathcal{M}_n(K[X])$ ou $\mathcal{M}_n(K(X))$ si on veut un corps
						\item \emph{Définition}: polynôme caractéristique 
						\item \emph{Application}: Matrice diagonale et triangulaire
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \emph{Théorème}: Cayley Hamilton 
						\item \emph{Corollaire}: Les valeurs propres sont des racines
						\item \emph{Application}: Matrice compagnon
					\end{minipage} 
				\end{itemize}
			\end{footnotesize}
			\item[C.] \emph{L'interprétation géométrique du déterminant \cite[p.139]{Grifone}}
			
			\begin{small}
				\textblue{Le déterminant est également un outil géométrique: il permet le calcul d'équations et de cercle. on retrouve l'orientation d'un espace vectoriel et la notion de volume. L'inégalité d'Hadamard utilise des théorèmes puissants d'analyse: le théorèmes des extremums liés. Enfin le déterminant permet de caractériser quelques groupes de matrices comme les isométries.}
			\end{small}
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.46\textwidth}
						\item \emph{Proposition}: Calcul d'équation de cercle - ellipse 
						\item \emph{Définition}: Orientation d'un espace vectoriel
						\item \emph{Proposition}: Partage des bases selon leur orientation
						\item \emph{Théorème}: Lien volume-déterminant
						\item \emph{Proposition} \cite[p.103]{Avez}: Théorème des extremums liés \textred{DEV}
						\item \emph{Théorème} \cite[p.380]{Rouviere}: Inégalité d'Hadamard \textred{DEV}
					\end{minipage} \hfill
					\begin{minipage}{.36\textwidth}
						\item \emph{Application}: Majoration du déterminant
						\item \emph{Définition}\cite[p.259]{Caldero-Germoni} : Groupe orthogonal
						\item \emph{Proposition}: Déterminant d'une isométrie
						\item \emph{Définition}: Groupe orthogonal spécial
						\item \emph{Application}: Groupe des rotations dans $\R^2$
					\end{minipage} 
				\end{itemize}
			\end{footnotesize}
		\end{description}
		\item[IV.] \textbf{Le déterminant: un outil analyste}
		
		\begin{small}
			\textblue{Le déterminant est également un outil d'analyse: on peut étudier la fonction déterminant. De plus, il permet d'effectuer des changements de variables.}
		\end{small}
		\begin{description}
			\item[A.] \emph{La fonction déterminant \cite[p.312]{Gourdon-analyse}}
			
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \emph{Proposition}: Fonction polynomiale $C^{\infty}$ 
						\item \emph{Application}: Densité dans $\textsc{GL}_n(\R)$ 
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \emph{Application}: Étude du Wronskien
						\item \emph{Proposition}: Différentielle du déterminant
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[B.] \emph{Le changement de variable \cite[p.334]{Gourdon-analyse}}
			
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \emph{Théorème}: Changement de variables
						\item \emph{Application}: Calcul d'une gaussienne 
					\end{minipage} \hfill
					\begin{minipage}{.4\textwidth}
						\item \emph{Application}: Passage en coordonnée polaire
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
		\end{description}
	\end{description}
	
	\section*{Quelques notions importantes}
	
	\subsection*{Équation via le déterminant}
	\input{./../Notions/AlgebreLineaire_Equations.tex}
	
	\subsection*{Définition du déterminant}
	\input{./../Notions/AlgebreLineaire_Determinant-def.tex}
	
	\subsection*{Calcul du déterminant}
	\input{./../Notions/AlgebreLineaire_Determinant-calcul.tex}
	
	\subsection*{Pivot de Gauss}
	\input{./../Notions/Algo_PivotGauss.tex}

	
	
	\bibliographystyle{alpha}
	\bibliography{./../../Livre}

	
\end{document}