\chapter*{Leçon 230: Séries de nombres réels et complexes. Comportement des restes et des sommes partielles des séries numériques.}
\addcontentsline{toc}{chapter}{Leçon 230: Séries de nombres réels et complexes. Comportement des restes et des sommes partielles des séries numériques.}

\titlebox{blue}{Références pour la leçon}{	
	\textblue{\cite{FrancinouGianellaNicolas-al1}} Francinou, Gianella et Nicolas, \emph{Oraux X-ENS, algèbre 1.}
	
	\textblue{\cite{FrancinouGianellaNicolas-an2}} Francinou, Gianella et Nicolas, \emph{Oraux X-ENS, analyse 2.}
	
	\textblue{\cite{Gourdon-analyse}} Gourdon, \emph{Analyse.}
	
	\textblue{\cite{Hauchecorne}} Hauchecorne, \emph{Les contre-exemples en mathématiques.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.35\textwidth}
		Nombre de Bell
	\end{minipage} \hfill
	\begin{minipage}{.5\textwidth}
		Nombre de Bernoulli
	\end{minipage}
}

\section*{Motivation}

\subsection*{Défense}


\subsection*{Ce qu'en dit le jury}

De nombreux candidats commencent leur plan par une longue exposition des conditions classiques assurant la convergence ou la divergence des séries numériques. Sans être hors sujet, cette exposition ne doit pas former l’essentiel de la matière de la leçon. Un thème important de la leçon est en effet le comportement asymptotique des restes et sommes partielles (équivalents, développements asymptotiques — par exemple pour certaines suites récurrentes — cas des séries de Riemann, comparaison séries et intégrales,...). Le manque d’exemples est à déplorer.

On peut aussi s’intéresser à certaines sommes particulières, que ce soit pour exhiber des nombres irrationnels (voire transcendants), ou mettre en valeur des techniques de calculs non triviales (par exemple en faisant appel aux séries de Fourier ou aux séries entières).

Enfin, si le jury apprécie que le théorème des séries alternées (avec sa version sur le contrôle du reste) soit maîtrisé, il rappelle aussi que ses généralisations possibles utilisant la transformation d’Abel trouvent toute leur place dans cette leçon.

\section*{Métaplan}

\begin{footnotesize}
	\begin{itemize}
		\begin{minipage}{.46\textwidth}
			\item \definiton: $\sum u_n$
		\end{minipage} \hfill
		\begin{minipage}{.46\textwidth}
			\item \definiton: $S_n$
		\end{minipage}
	\end{itemize}
\end{footnotesize}

\begin{description}
	\item[I.] \textbf{Séries à termes positifs \cite[p.200]{Gourdon-analyse}}
	\begin{footnotesize}
		\begin{itemize}[label=$\rightsquigarrow$]
			\begin{minipage}{.46\textwidth}
				\item \cadre: $\forall n \in \N$, $u_n \geq 0$
				\item \proposition: Convergence $\Rightarrow$ $u_n \to 0$
			\end{minipage} \hfill
			\begin{minipage}{.45\textwidth}
				\item \definiton: Convergence de $\sum u_n$
				\item \definiton: Divergence grossière
			\end{minipage}
		\end{itemize}
	\end{footnotesize}
	\begin{description}
		\item[A.] \emph{Critère de convergence}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.5\textwidth}
					\item \proposition: Majoration par une série convergente
					\item \thm: Comparaison série-intégrale
					\item \cexemple~\cite[p.122]{Hauchecorne}
					\item \application: $H_n$ et Stirling
					\item \proposition: Règle de d'Alembert
					\item \proposition: Règle de Cauchy
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \exemple~\cite[p.211]{Gourdon-analyse}: Zéta
					\item \exemple: Série de Riemann
					\item \proposition : Convergence et Lando
					\item \exemple: Série de Bertrand
					\item \cexemple~\cite[p.119]{Hauchecorne}
					\item \cexemple~\cite[p.120]{Hauchecorne}
				\end{minipage}
				\item \proposition: Règle de Roab--Duahamel
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Comportements des restes des sommes partielles}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]				
				\begin{minipage}{.36\textwidth}
					\item \proposition: Bijection et $f^{-1}$
				\end{minipage} \hfill
				\begin{minipage}{.44\textwidth}
					\item \thm~(ADMIS): Dérivable presque partout
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[II.] \textbf{Séries à termes quelconques \cite[p.210]{Gourdon-analyse}}
	\begin{small}
		\textblue{On obtient la motivation pour la partie précédente.}
	\end{small}
	\begin{footnotesize}
		\begin{itemize}
			\begin{minipage}{.46\textwidth}
				\item \cadre: $u_n$ quelconques dans $\R$ ou $\C$
				\item \remarque: Convergence absolue $\Rightarrow$ simple
			\end{minipage} \hfill
			\begin{minipage}{.46\textwidth}
				\item \definiton: Convergence absolue
				\item \definiton: Série semi-convergente
			\end{minipage}
		\end{itemize}
	\end{footnotesize}
	\begin{description}
		\item[A.] \emph{Critères de convergence}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \proposition: Séries alternées
					\item \proposition: Cauchy-Hadamard
				\end{minipage} \hfill
				\begin{minipage}{.45\textwidth}
					\item \proposition: Transformation d'Abel
					\item \proposition: Cas complexe
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Comportement des restes}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.5\textwidth}
					\item \proposition: Reste de série absolument convergente
				\end{minipage} \hfill
				\begin{minipage}{.44\textwidth}
					\item \proposition: Comparaison reste-intégrale
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[III.] \textbf{Calculs de sommes}
	\begin{description}
		\item[A.] \emph{Cas de séries faciles}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \proposition: Série télescopique
				\end{minipage} \hfill
				\begin{minipage}{.45\textwidth}
					\item \proposition: Linéarité de la somme
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Utilisation des séries entières}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.4\textwidth}
					\item \definiton: Série entière
					\item \application: Calcul de $\sum \frac{2^n}{n!}$
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \thm: Abel
					\item \textred{\application~\cite{FrancinouGianellaNicolas-al1}: Nombre de Bell}
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Utilisation des séries de Fourier}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.35\textwidth}
					\item \definiton: Série de Fourier
					\item \application: Calcul de $\sum \frac{1}{n^2}$
				\end{minipage} \hfill
				\begin{minipage}{.5\textwidth}
					\item \thm: Dirichlet
					\item \textred{\application~\cite{FrancinouGianellaNicolas-an2}: Série des nombres de Bernoulli}
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
\end{description}

