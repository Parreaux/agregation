\chapter*{Leçon 151 : Dimension d'un espace vectoriel (on se limitera au cas fini). Rang. Exemples et applications.}
\addcontentsline{toc}{chapter}{Leçon 151 : Dimension d'un espace vectoriel (on se limitera au cas fini). Rang. Exemples et applications.}

\titlebox{blue}{Références pour la leçon}{
	\textblue{\cite{Avez}} Avez, \emph{Calcul différentiel.}
	
	\textblue{\cite{BeckMalikPeyre}} Beck, Malik et Peyre, \emph{Objectif agrégation.}
		
	\textblue{\cite{Berhuy}} Berhuy, \emph{Algèbre: le grand combat.}

	\textblue{\cite{Cognet}} Cognet, \emph{Algèbre linéaire.}
	
	\textblue{\cite{Gourdon-algebre}} Gourdon, \emph{Algèbre.}
	
	\textblue{\cite{Grifone}} Grifone, \emph{Algèbre linéaire.}
	
	\textblue{\cite{Perrin}} Perrin, \emph{Cours d'algèbre}
	
	\textblue{\cite{Rouviere}} Rouvière, \emph{Petit guide de calcul différentiel à l'usage de la licence et de l'agrégation.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.3\textwidth}
		Algorithme de Berlekamp
	\end{minipage} \hfill
	\begin{minipage}{.6\textwidth}
		Théorème des extrema liés et l'inégalité d'Hadamard
	\end{minipage}
}

\section*{Motivation}

\subsection*{Défense}

\begin{itemize}
	\item Démonstration par récurrence sur le rang ou la dimension.
	\item Classification des endomorphisme.
	\item Retrouver des résultats qu'on avait sur les ensembles finis.
	\item Avoir une description finie d'un ensemble infini.
\end{itemize}

\subsection*{Ce qu'en dit le jury}

Dans cette leçon, il est indispensable de présenter les résultats fondateurs de la théorie des espaces vectoriels de dimension finie en ayant une idée de leurs preuves. Ces théorèmes semblent simples car ils ont été très souvent pratiqués, mais leur preuve demande un soin particulier. Il est important de savoir justifier pourquoi un sous-espace vectoriel d’un espace vectoriel de dimension finie est aussi de dimension finie. Le pivot de Gauss ainsi que les diverses notions et caractérisations du rang trouvent leur place dans cette leçon. Les applications sont nombreuses, on peut par exemple évoquer l’existence de polynômes annulateurs ou alors décomposer les isométries en produits de réflexions.

On pourra utiliser les caractérisations du rang pour démontrer l’invariance du rang par extension de
corps, ou pour établir des propriétés topologiques (sur $\R$ ou $\C$). S’ils le désirent, les candidats peuvent déterminer des degrés d’extensions dans la théorie des corps ou s’intéresser aux nombres algébriques.

On pourra également explorer des applications en analyse comme les extrémas liés ou l’étude de l’espace
vectoriel engendré par les translatés d’une application de $\R$ dans $\R$.

Dans un autre registre, il est pertinent d’évoquer la méthode des moindres carrés dans cette leçon,	par exemple en faisant ressortir la condition de rang maximal pour garantir l’unicité de la solution et s’orienter vers les techniques de décomposition en valeurs singulières pour le cas général. On peut alors naturellement explorer l’approximation d’une matrice par une suite de matrices de faible rang.

\section*{Métaplan}

\begin{footnotesize}
	\noindent\emph{Pré-requis}: Espace vectoriel; Sous-espace vectoriel; Application linéaire; Matrice; Système linéaire; Corps
		
	\noindent\emph{Cadre}: $E$ espace vectoriel sur un corps $K$ commutatif
\end{footnotesize}

\begin{description}
	\item[I.] \textbf{Base et dimension \cite[p.10]{Grifone}}
	\begin{small}
		\textblue{Les notions de bases et de dimension sont très liés puisque le cardinal d'une base donne la dimension d'un espace vectoriel (en dimension fini).}
	\end{small}
	\begin{description}
		\item[A.] \emph{Famille libres, génératrices et bases}
		\begin{small}
			\textblue{Intuitivement les familles génératrices permettent d'exprimer tout autre vecteur comme un combinaison linéaire de la famille. Une telle famille n'existe pas toujours : on définie donc les espaces de dimension finie ou non. Une base permet de garantir l'existence et unicité de la décomposition.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.35\textwidth}
					\item \emph{Définition}: Famille génératrice
					\item \emph{Définition}: Famille libre 
				\end{minipage} \hfill
				\begin{minipage}{.45\textwidth}
					\item \emph{Définition}: Dimension finie et infinie
					\item \emph{Remarque}: Famille non-libre est une famille liée
				\end{minipage}
				\item \emph{Proposition}: Caractérisation de la liberté via la généricité
				\item \emph{Remarque}: Utilisation du pivot de Gauss
				
				\begin{minipage}{.5\textwidth}
					\item \emph{Proposition}: Propriétés des familles libres et génératrices
					\item \emph{Proposition}: Sur et sous famille génératrice et libre
				\end{minipage} \hfill
				\begin{minipage}{.3\textwidth}
					\item \emph{Définition}: Base
					\item \emph{Théorème}: Existence d'une base 
				\end{minipage}
				\item \emph{Théorèmes}: Base incomplète, trop complète 
				\item \emph{Proposition} \cite[p.63]{Grifone}: Application injective/surjective et famille libre/génératrice.	
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Dimension finie}
		\begin{small}
			\textblue{Nous allons maintenant donner le lien précis entre base et dimension finie: le cardinal donne le second. Pour établir ce résultat, nous utilisons le lien entre liberté et généricité comme décrite dans le premier lemme. Nous donnerons ensuite une version plus classique de ce résultat.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \emph{Lemme}: Famille génératrice à $n$ éléments donne famille lié si plus de $n$ éléments.
				\item \emph{Théorème}: Dans un espace de dimension finie, toute base à même cardinal
				
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: $\dim = \mathrm{card}$ de la base
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Notation}: Dimension
				\end{minipage}
				\item \emph{Application}: Théorème de la base de Burnside
				\item \emph{Corollaire}: Caractérisation via la dimension des familles libres et génératrices
				\item \emph{Théorème}: Caractérisation base avec cardinal
				\item \emph{Application}: Preuve par récurrence sur les dimensions 
				
				\begin{minipage}{.4\textwidth}
					\item \emph{Application}: Réduction des auto-adjoints
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \textred{\emph{Application}: Algorithme de Berkhamp}
				\end{minipage}
				\item \emph{Théorème} \cite[p.63]{Grifone}: Deux espaces vectoriels sont isomorphes $\Leftrightarrow$ ils ont même dimension
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Sous-espace vectoriel de dimension finie}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.38\textwidth}
					\item \emph{Théorème}: Sous-espace et dimension
				\end{minipage} \hfill
				\begin{minipage}{.48\textwidth}
					\item \emph{Proposition} \cite[p.85]{Grifone}: dimension de $\mathcal{L}(E, F)$ et du dual
				\end{minipage}
				\begin{minipage}{.5\textwidth}
					\item \emph{Proposition}: Dimension des espaces produits
				\end{minipage} \hfill
				\begin{minipage}{.3\textwidth}
					\item \emph{Définition}: Somme
				\end{minipage}
				\item \emph{Proposition}: Dimension de la somme (formule de Grassmann)
				
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Somme directe
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Supplémentaire
				\end{minipage}
				\item \emph{Théorème}: Existence et dimension d'un supplémentaire
				\item \emph{Application}: Dimension et forme linéaire
				\item \emph{Corollaire}: Caractérisation des espaces somme directe\item \emph{Définition} \cite[p.956]{Berhuy}: Sous-espaces propres
				\item \emph{Théorème} \cite[p.956]{Berhuy}: caractérisation de la diagonalisation
			\end{itemize}
		\end{footnotesize}
		\item[D.] \emph{Application à l'extension de corps \cite[p.65]{Perrin}}
		\begin{small}
			\textblue{La théorie de la dimension s'applique dans les extension de corps : le degré de l'extension est la dimension de l'espace vectoriel induit pas cette extension.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.3\textwidth}
					\item \emph{Définition}: Extension de corps
				\end{minipage} \hfill
				\begin{minipage}{.52\textwidth}
					\item \emph{Remarque}: $L$ extension du corps $K$ est un $K$-espace vectoriel 
				\end{minipage}
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Degrés de l'extension: $dim_{K}(L)$
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Théorème}: Théorème de la base télescopique
				\end{minipage}
				\begin{minipage}{.3\textwidth}
					\item \emph{Corollaire}: Multiplicité des degrés
				\end{minipage} \hfill
				\begin{minipage}{.5\textwidth}
					\item \emph{Définition}: Éléments algébrique, polynôme minimal 
				\end{minipage}
				\item \emph{Théorème}: Caractérisation des éléments algébriques
				\item \emph{Application}: Si $K \subset L, \{x \in L ~|~ x~algébrique~dans~K\}$ est un sous-corps de $L$
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[II.] \textbf{Un peu de topologie \cite[p.46]{Gourdon-analyse}}
	\begin{small}
		\textblue{La dimension finie nous donne des résultats intéressant en topologie des espaces vectoriels normés.}
	\end{small}
	\begin{footnotesize}
		\begin{itemize}[label=$\rightsquigarrow$]
			\begin{minipage}{.4\textwidth}
				\item \emph{Théorème}: Norme équivalente
				\item \emph{Proposition}: Fonction continue
			\end{minipage} \hfill
			\begin{minipage}{.4\textwidth}
				\item \emph{Théorème}: Compacité
				\item \emph{Théorème}: Boule unité fermé compacte
			\end{minipage}
		\end{itemize}
	\end{footnotesize}
	\item[III.] \textbf{Rang et applications linéaires en dimension finie}
	\begin{small}
		\textblue{Le rang est la dimension de l'image d'une application linéaire. Nous pouvons alors l'étudier sur l'application linéaire ou sur des matrices. Nous allons définir le rang sur les deux objets et en fonction des applications que nous souhaitons en faire, nous allons choisir l'une ou l'autre de ces représentation.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Notion de rang \cite[p.61]{Grifone}}
		
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Rang d'une application linéaire
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Théorème}: Théorème du rang
				\end{minipage}
				\begin{minipage}{.5\textwidth}
					\item \emph{Application} \cite[p.114]{Gourdon-algebre}: Projecteur (morphisme $p^{2} = p$)
				\end{minipage} \hfill
				\begin{minipage}{.31\textwidth}
					\item \emph{Corollaire}: Caractérisation bijection
				\end{minipage}				
				\item \emph{Contre-exemple}: Faux en dimension infini (dérivée d'un polynôme)
				\item \emph{Contre-exemple}: Multiplication par une variable
				\item \emph{Proposition}: Dimension et surjectivité/injectivité
				\item \emph{Application} \cite[p.113]{Gourdon-algebre}: Quotient d'espace vectoriel
				\item \emph{Application}: Intersection de noyau de formes linéaires
				\item \emph{Application} \cite[p.154]{BeckMalikPeyre}: Polynôme interpolateur de Lagrange 
				\item \emph{Définition}: Rang d'une famille de vecteur, de matrice
				\item \emph{Proposition}: Rang d'endomorphisme et de matrice associée
				\item \emph{Remarque}: Caractérisation par le rang: application et inversion.
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Caractérisation du rang \cite[p.121]{Gourdon-algebre}}
		\begin{small}
			\textblue{On étudie le rang pour des matrices vérifiant certaines propriétés (souvent données par des actions de groupes).}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.3\textwidth}
					\item \emph{Définition}: Matrices équivalentes
				\end{minipage} \hfill
				\begin{minipage}{.5\textwidth}
					\item \emph{Théorème}: Deux matrice équivalente ont le même rang
				\end{minipage}
				\item \emph{Application}: Action de Steinitz, caractérisation avec les orbites et systèmes de représentant
				\item \emph{Définition}: Matrices semblables
				\item \emph{Théorème}: Caractérisation du rang avec les matrices extraites
				
				\begin{minipage}{.3\textwidth}
					\item \emph{Définition}: Matrices extraites
				\end{minipage} \hfill
				\begin{minipage}{.5\textwidth}
					\item \emph{Théorème}: Deux matrices semblables ont même rang
				\end{minipage}
				\item \emph{Exemple}: Contre-exemple de la réciproque
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Calcul du rang}
		\begin{small}
			\textblue{Le calcul du rang se fait essentiellement via le pivot de Gauss.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \emph{Proposition}: opérations élémentaires ne modifie pas le rang
				\item \emph{Application} \cite[p.205]{Cognet}: méthode du pivot de Gauss
				\item \emph{Proposition}: existence et unicité des solutions d'un système suivant le rang
				
				\begin{minipage}{.3\textwidth}
					\item \emph{Proposition}: $rg A = rg(^{t}A)$
				\end{minipage} \hfill
				\begin{minipage}{.5\textwidth}
				\item \emph{Application}: calcul de noyaux, d'image, de famille libre, ...
				\end{minipage} 
				\item \textred{\emph{Application}: théorème des extremum liés + inégalité d'Hadamard}
			\end{itemize}
		\end{footnotesize}
	\end{description}
\end{description}
