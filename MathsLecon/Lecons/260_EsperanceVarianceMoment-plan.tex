\chapter*{Leçon 260: Espérance, variance et moments d'une variable aléatoire.}
\addcontentsline{toc}{chapter}{Leçon 260: Espérance, variance et moments d'une variable aléatoire.}

\titlebox{blue}{Références pour la leçon}{
	\textblue{\cite{Appel}} Appel, \emph{Probabilités pour les non-probabilistes.}
	
	\textblue{\cite{BardeLedoux}} Barde et Ledoux, \emph{Probabilité.}
	
	\textblue{\cite{BernisBernis}} Bernis et Bernis, \emph{Analyse pour l'agrégation de mathématiques: 40 développements.}
	
	\textblue{\cite{GaretKurtzmann}} Garet et Kurtzmann, \emph{De l'intégration aux probabilités.}
	
	\textblue{\cite{Hauchecorne}} Hauchecorne, \emph{Les contres-exemples en mathématiques.}
	
	\textblue{\cite{Ouvrard-1}} Ouvrard, \emph{Probabilité, tome 1.}
	
	\textblue{\cite{ZuilyQueffelec2}} Queffelec et Zuily, \emph{Éléments d'analyse pour l'agrégation.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.35\textwidth}
		Théorème de Weierstrass
	\end{minipage} \hfill
	\begin{minipage}{.5\textwidth}
		Théorème Central Limite
	\end{minipage}
}

\section*{Motivation}

\subsection*{Défense}

\subsection*{Ce qu'en dit le jury}

Le jury attend des candidats qu’ils donnent la définition des moments centrés, qu’ils rappellent les implications d’existence de moments (décroissance des $L^p$). Les variables aléatoires à densité sont trop souvent négligées. Le candidat peut citer — mais doit surtout savoir retrouver rapidement — les espérances et variances de lois usuelles, notamment Bernoulli, binomiale, géométrique, Poisson, exponentielle, normale. La variance de la somme de variables aléatoires indépendantes suscite souvent des hésitations. Les inégalités classiques (de Markov, de Bienaymé-Chebyshev, de Jensen et de Cauchy-Schwarz) pourront être données, ainsi que les théorèmes de convergence (lois des grands nombres et théorème central limite). La notion de fonction génératrice des moments pourra être présentée ainsi que les liens entre moments et fonction caractéristique.

Pour aller plus loin, le comportement des moyennes empiriques pour une suite de variables aléatoires indépendantes et identiquement distribuées n’admettant pas d’espérance pourra être étudié. Pour les candidats suffisamment à l’aise avec ce sujet, l’espérance conditionnelle pourra aussi être abordée.

\section*{Métaplan}

\begin{footnotesize}
	\noindent\cadre: $(\Omega, A, \mathbb{P})$ un espace probabilisé et $X$ une variable aléatoire réelle
\end{footnotesize}

\begin{description}
	\item[I.] \textbf{Espérance \cite[p.143]{GaretKurtzmann}}
	\begin{small}
		\textblue{Espérance = moyenne}
	\end{small}
	\begin{description}
		\item[A.] \emph{Espérance et premières propriétés}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \definiton: Espérance de $X$
					\item \exemple: $X = \lambda$ pp, $\E[X] = \lambda$
					\item \proposition: Espérance est linéaire
					\item \proposition: Interprétation de l'espérance
				\end{minipage} \hfill
				\begin{minipage}{.44\textwidth}
					\item \definiton: Variable aléatoire intégrable
					\item \exemple: Espérance d'une indicatrice
					\item \corollaire: Variables aléatoire intégrables est un ev
					\item \proposition: Croissance
				\end{minipage}
				\item \definiton: Variables centrées
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Calcul de l'espérance \cite[p.148]{GaretKurtzmann}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.4\textwidth}
					\item \thm: Formule de transfert
					\item \thm: Cas d'une variable discrète
					\item \application~\cite[couverture]{Appel}: Lois usuelles
					\item \thm: Cas d'une variable à densité
					\item \remarque: $f$ paire $\Rightarrow$ $X$ toujours intégrable
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \remarque: Lien avec la loi
					\item \corollaire: Caractérisation
					\item \cexemple~\cite[p.358]{Hauchecorne}: $\mathbb{P}(X = 2^n) = \frac{1}{2^n}$
					\item \corollaire: Caractérisation
					\item \application~\cite[couverture]{Appel}: Lois usuelles
				\end{minipage}
				\item \cexemple~\cite[p358]{Hauchecorne}: Loi de Cauchy
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Applications de l'espérance}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.5\textwidth}
					\item \proposition~\cite[p.144]{GaretKurtzmann}: Inégalité de Markov
					\item \proposition~\cite[p.152]{GaretKurtzmann}: Inégalité de Jensen
					\item \definiton: Famille mutuellement indépendante
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \application~\cite{Cormen}: Hachage parfait
					\item \exemple: $x \mapsto x^2$
					\item \proposition: Calcul de l'espérance
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[II.] \textbf{Variance}
	\begin{small}
		\textblue{La variance donne l'écartement à l'espérance.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Moment d'ordre $2$ et variance \cite[p.154]{GaretKurtzmann}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.44\textwidth}
					\item \definiton: $X$ de carré intégrable
					\item \proposition: Formule de calcul
				\end{minipage} \hfill
				\begin{minipage}{.45\textwidth}
					\item \definiton: Variance et écart type
					\item \exemple~\cite[couverture]{Appel}: Variance de lois usuelles
				\end{minipage} 
				\begin{minipage}{.5\textwidth}
					\item \cexemple~\cite[p.359]{Hauchecorne}: Cas discret et cas densité
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \proposition: $Var(aX + b) = a^2Var(X)$
				\end{minipage} 
				\item \proposition~\cite[p.160]{GaretKurtzmann}: Inégalité de Bienaymé--Chebychev
				\item \textred{\application: Théorème de Stone--Weierstrass via Bernstein}
				\item \proposition: Inégalité de Cauchy--Schwartz
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Covariance}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.4\textwidth}
					\item \definiton: Covariance
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \proposition: Propriétés de la covariances
				\end{minipage}
				\item \remarque: Variance est une forme quadratique de forme polaire la covariance
				
				\begin{minipage}{.35\textwidth}
					\item \definiton: Matrice de cavariance
				\end{minipage} \hfill
				\begin{minipage}{.5\textwidth}
					\item \remarque: Représentation matricielle de la covariance
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Indépendance et corrélation}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.45\textwidth}
					\item \definiton: Corrélation
					\item \proposition: Indépendance $\Rightarrow$ non corrélé
					\item \proposition: Indépendance et variance
					\item \proposition: Identité de Bienaymé
				\end{minipage} \hfill
				\begin{minipage}{.44\textwidth}
					\item \cexemple~\cite[p.360]{Hauchecorne}
					\item \cexemple: $X \sim \mathcal{N}(0, 1)$ et $Y = X^2$
					\item \thm~\cite[p.290]{GaretKurtzmann}: Gaussienne et réciproque
					\item \application: $\mathbb{P}\left[\left|\sum x_i - \E[X_i]\right| \geq t\right] \leq \sum \frac{Var X_i}{t^2}$
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[III.] \textbf{Moment d'ordre $p$}
	\begin{description}
		\item[A.] \emph{Moments d'ordre $p$ et conséquences \cite[p.125]{Ouvrard-1}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.5\textwidth}
					\item \definiton: Moment d'ordre $p$
					\item \proposition: Inégalité de Hölder
					\item \emph{Interprétation}: $\E[X^k]$ existe alors existe $\forall k' \leq k$
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \exemple: $X \sim \mathcal{N}(0, 1)$
					\item \proposition: Inégalité de Minkowski
					\item \application: $L^1 \supset L^2 \supset \dots$
				\end{minipage}
				\begin{minipage}{.4\textwidth}
					\item \definiton: Convergence des $L^p$
				\end{minipage} \hfill
				\begin{minipage}{.5\textwidth}
					\item \application: convergence dans $L^p$ donc dans $L^q$, $p \geq q$
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Fonction génératrice \cite[p.209]{GaretKurtzmann}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.5\textwidth}
					\item \definiton: Fonction génératrice $G$
					\item \exemple: Lois usuelles
					\item \proposition~\cite[p.160]{Appel}: $G$ est croissante et convexe
					\item \application~\cite[p.195]{Appel}: Processus de Galtson--Watson
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \remarque: Rayon de définition
					\item \proposition~\cite[p.160]{Appel}: $\left|G(s)\right| \leq 1$ et $G(1) = 1$
					\item \thm: Caractérisation de la loi
					\item \thm: Espérance et fonction génératrice
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Fonction caractéristique \cite[p.212]{GaretKurtzmann}}
		\begin{small}
			\textblue{Généralisation des fonctions génératrices aux variables via la transformée de Fourier.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]	
				\begin{minipage}{.5\textwidth}
					\item \definiton: Fonction caractéristique
					\item \thm: $\varphi_X = \varphi_Y \Rightarrow \mathbb{P}_X = \mathbb{P}_Y$
					\item \exemple: $X \sim \mathcal{N}(0, 1)$
					\item \thm: Fonction caractéristique et indépendance
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \remarque: Interprétation en transformée de Fourier
					\item \corollaire: $\varphi_X$ caractérise la loi
					\item \exemple: $X \sim \mathcal{P}(\lambda)$
					\item \thm: Fonction caractéristique et moment
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[IV.] \textbf{Convergence}
	\begin{footnotesize}
		\begin{itemize}[label=$\rightsquigarrow$]	
			\begin{minipage}{.5\textwidth}
				\item \definiton~\cite[p.240]{GaretKurtzmann}: Convergence en probabilité
				\item \definiton~\cite[p.237]{GaretKurtzmann}: Convergence presque sûre
				\item \definiton~\cite[p.265]{GaretKurtzmann}: Convergence en loi
				\item \textred{\thm~\cite{BernisBernis}: Théorème central limite}
			\end{minipage} \hfill
			\begin{minipage}{.45\textwidth}
				\item \thm: Loi faible des grands nombres
				\item \thm~\cite[p.247]{GaretKurtzmann}: Loi forte des grands nombres
				\item \remarque: Lien entre les convergence
				\item \textred{\application~\cite{BernisBernis}: Approximation de la loi de Poisson}
			\end{minipage}
		\end{itemize}
	\end{footnotesize}
\end{description}

