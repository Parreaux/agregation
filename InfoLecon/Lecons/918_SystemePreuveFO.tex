\documentclass[a4paper,11pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais,english]{babel}%
\usepackage[]{geometry} 

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}
\usepackage{subcaption}

\usepackage{algorithm}
\usepackage{algpseudocode}%

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{listings}


\parskip=0\baselineskip %0.5 d'habitude
\usetikzlibrary{automata,positioning,shadows}
\usetikzlibrary{calc, shapes, backgrounds,arrows}

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{cex}{Contre-exemple}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	
	\title{Leçon 918: Systèmes formels de preuve en logique du premier ordre. Exemples.}
	
	\date{2018 - 2019}
	\author{Julie Parreaux}
	
	\maketitle
	
	\titlebox{blue}{Références pour la leçon}{
		\textblue{\cite{Duparc}} Duparc, \emph{La logique pas à pas.}
		
		\textblue{\cite{DavidNourRaffali}} David, Nour et Raffali, \emph{Introduction à la logique. Théorie de la démonstration.}
		
		\textblue{\cite{Stern}} Stern, \emph{Fondements mathématiques de l'informatique.}
	}
	
	\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
		\begin{minipage}{.5\textwidth}
			Complétude de la déduction naturelle
		\end{minipage} \hfill
		\begin{minipage}{.45\textwidth}
			Fonctionnement de Prolog
		\end{minipage} 
	}
	
	\renewcommand{\contentsname}{Plan de la leçon}
	\tableofcontents
	
	\section*{Motivation}
	
	\subsection*{Défense}
	
	La mathématicien Hilbert au début du $XIX^{\text{ième}}$ a cherché à formaliser et à comprendre la notion de preuve mathématiques. Il cherchait alors le nombre minimal d'axiomes mathématiques nécessaires pour prouver l'ensemble des mathématiques connus. Les logiciens se sont alors intéressés à la notion de preuve et ils ont tentés de répondre à la question: \emph{qu'est-ce qu'une preuve}?
	
	Les informaticiens se sont ensuite emparés du sujet, notamment avec Curry et Howard dont la correspondance dit que programmer est en réalité prouver (pour cela, ils utilisent le $\lambda$-calcul simplement typé et la logique intuitionniste). De cette idée, on a cherché (et on cherche toujours) à automatiser au mieux les preuves à l'aide de la programmation logique ou sous-contrainte, en utilisant des assistants de preuves. Ces derniers sont devenus une aide aux mathématiciens pour certains théorème comme le théorème des quatre couleurs. Mais attention, comme pour tout système informatique, leur correction n'est pas simple à vérifier. Dans ce cas, ils ont tous un noyau qui n'a pas été prouvé: c'est le noyau de confiance de l'assistant auquel on se doit de faire confiance pour assurer la fiabilité des résultats.
	
	\subsection*{Ce qu'en dit le jury}
	
	Le jury attend du candidat qu’il présente au moins la déduction naturelle ou un calcul de séquents et qu’il soit capable de développer des preuves dans ce système sur des exemples classiques simples. La présentation des liens entre syntaxe et sémantique, en développant en particulier les questions de correction et complétude, et de l’apport des systèmes de preuves pour l’automatisation des preuves est également attendue.
	
	Le jury appréciera naturellement si des candidats présentent des notions plus élaborées comme la stratégie d’élimination des coupures mais est bien conscient que la maîtrise de leurs subtilités va au-delà du programme.

	\section*{Introduction}
	\addcontentsline{toc}{section}{Introduction}
	
	\noindent\emph{Motivation}:
	\begin{itemize}[label=$*$]
		\item Hilbert: qu'est-ce qu'une preuve
		\item Programmer c'est prouver: correspondance de Curry--Howard
		\item Assistant de preuve
	\end{itemize}
	
	
	\section{Logique du premier ordre}
	\textblue{Pourquoi l'étude de la logique du premier ordre? C'est une logique assez puissante pour exprimer le langage naturel. Il est donc naturel lorsqu'on cherche à formaliser les preuves mathématiques (qui s'exprime via un langage humain) à se tourner vers cette logique. Cette section est une section de rappel: il n'est pas nécessaire d'en mettre trop (moins il en a, plus on a de systèmes de preuve et autres).}
	
	\subsection{Syntaxe de la logique du premier ordre \cite[p.9]{DavidNourRaffali}}
	\begin{itemize}
		\item \emph{Définition}: Langage de la logique du premier ordre
		\item \emph{Définition}: Terme et terme clos
		\item \emph{Définition}: Formule
		\item \emph{Remarque}: Lien avec le calcul propositionnel
		\item \emph{Définition}: Variable libre
		\item \emph{Définition}: Formule close
		\item \emph{Définition}: Théorie
		\item \emph{Définition}: Substitution
	\end{itemize}
	
	\subsection{Sémantique de la logique du premier ordre \cite[p.75]{DavidNourRaffali}}
	\begin{itemize}
		\item \emph{Définition}: Modèle \textblue{(On définit le modèle au sens de modèle, on se passe de la définition de signature donc on met directement dans la définition de modèle la satisfiabilité.)}
		\item \emph{Remarque}: Pour les théories
		\item \emph{Exemple}~\cite[p.220]{Stern}: Modèle de Herbrand
		\item \emph{Définition}: Équivalence sémantique
		\item \emph{Définition}: Théorie contradictoire
		\item \emph{Problèmes}: \textsf{Valide} et \textsf{T-Valide}
	\end{itemize}
	
	
	\section{Systèmes de preuves}
	\textblue{Les systèmes de preuve sont uniquement syntaxique. En effet, une preuve est basée et conduite par la syntaxe de la formule et non sa sémantique. Cependant à l'aide de théorème de complétude et de correction, on arrive à lier syntaxe et sémantique. Nos systèmes de preuve ne font donc pas n'importe quoi.}
	
	\subsection{La déduction naturelle}
	\textblue{La déduction naturelle est un système de preuve dont toutes les déductions se fond sur le but: on passe tout dans le but pour les manipuler. Cela alourdit la manipulation et même si les règles sont relativement facile, il est délicat à automatiser.}
	
	\paragraph{Présentation du système de preuve \cite[p.24]{DavidNourRaffali}} \textblue{Nous donnons ici la présentation du système de preuve: les objets qu'il manipule ainsi que les règles de manipulation. On obtient un nombre fini de règles qui permettent de prouver l'ensemble des formules prouvables: ce qui n'était pas évident au départ.}
	\begin{itemize}
		\item \emph{Définition}: Séquent
		\item \emph{Définition}: Règle \textblue{(voir si on les met en annexe ou non)}
		\item \emph{Définition}: Séquent et formule prouvable par déduction naturelle
	\end{itemize}
	
	\paragraph{Lien entre syntaxe et sémantique \cite[p.79]{DavidNourRaffali}} \textblue{Les systèmes de preuves sont des passerelles entre la syntaxe et la sémantique. Les preuves sont des objets purement syntaxique qui ne fond pas n'importe quoi sur la sémantique. Ce sont l'objets de théorème de correction et de complétude de la logique.}
	\begin{itemize}
		\item \emph{Définition}: Théorie consistante, théorie complète
		\item \emph{Théorème}: Théorie consistante est non-contradictoire \textred{(DEV)}
		\item \emph{Corollaire}: Complétude et correction de la déduction naturelle
		\item \emph{Application}: Théorème de compacité
		\item \emph{Application}:~\cite[p.99]{DavidNourRaffali} Théorème de Lowhein--Skolem
		\item \emph{Proposition}: Les problèmes \textsf{Valide} et \textsf{T-Valide} sont indécidables
		\end{itemize}
	
	\subsection{Le calcul des séquents \cite[p.185]{DavidNourRaffali}} 
	\textblue{Le calcul des séquents est plus facile à manipuler que la déduction naturelle car on peut manipuler le contexte comme les conclusions (ce qui n'est pas possible en déduction naturelle) : on obtient des règles symétrique. Cela facilite leur manipulation (c'est souvent ce système de preuve qui est automatisé) même s'il est moins naturel que la déduction naturelle pour formaliser des preuves.}
	\begin{itemize}
		\item \emph{Définition}: Séquent
		\item \emph{Définition}: Règle \textblue{(voir si on les met en annexe ou non)}
		\item \emph{Définition}: Séquent et formule prouvable par calcul des séquents
		\item \emph{Théorème}: Équivalence des systèmes de preuves
		\item \emph{Théorème} \textred{(ADMIS)}: Élimination des coupures \textblue{(Dans le contexte de la formalisation des mathématiques, la coupure est une règle qui nous permet de faire des lemmes et ainsi de formaliser les jolies preuves. Cependant, dans le cadre de l'automatisation cette règle est ingérable: comment choisir où couper? Ce théorème implique que nous pouvons nous en passer, ce qui est un premier pas vers l'automatisation des preuves. L'idée de la preuve est de recopier la preuve de la coupure et du lemme partout où on a besoin (formellement c'est une preuve par induction sur l'arbre de preuve).)}
	\end{itemize}
	
	
	\section{Automatisation des preuves \cite[p.231]{Stern}}
	\textblue{On a ensuite cherché à automatiser les preuve via l'informatique avec l'idée que programmer c'est prouver. Cependant, même si on a un nombre fini de règles, leur automatisation n'est pas si simple (surtout dû au quantificateur existentiel) qui demande l'intervention de l'homme: ce sont les assistants de preuve. On a donc développé plusieurs stratégies basées notamment sur la résolution.}
	
	\subsection{Unification \cite[p.248]{DavidNourRaffali}}
	\textblue{L'unification est un premier pas vers l'automatisation car elle permet de repérer deux termes syntaxiquement équivalents et d'appliquer une substitution afin de les rendre identiques.}
	\begin{itemize}
		\item \emph{Définition}: Terme unifiable
		\item \emph{Définition}: Unificateur (principal)
		\item \emph{Définition}: Équations unifiables
		\item \emph{Algorithme}: Unification
		\item \emph{Théorème}: Correction de l'algorithme d'unification
	\end{itemize}
	
	\subsection{Résolution \cite[p.264]{DavidNourRaffali}}
	\textblue{La résolution est un système de preuve car il ne possède que deux règles. Il est également très utile dans l'automatisation des preuves.}
	\begin{itemize}
		\item \emph{Principe}: On cherche à montrer qu'un ensemble de formule est contradictoire
		\item \emph{Définition}: Règles \textblue{(écrire les deux règles et pas une seule)}
		\item \emph{Méthode}
		\item \emph{Lemme}: Correction de la méthode
	\end{itemize}
	
	\subsection{Programmer c'est prouver}
	\textblue{La preuve automatique par les programmes est une conséquence de la correspondance de Curry-Howard \cite[p.527]{Duparc}. Nous allons étudier quelques exemples de cet adage même si en toute généralité nous devrons parler de $\lambda$-calcul simplement typé et logique intuitionniste.}
	\begin{itemize}
		\item \emph{Définition}: Clause de Horn
		\item \emph{Proposition}: Résolution sur les clauses de Horn \textred{(DEV)}
		\item \emph{Application}: Prolog et la programmation logique (puis sous contraintes)
		\item \emph{Exemple}: Requête SQL sur une base de donnée
	\end{itemize}
	
	
	\bibliographystyle{plain}
	\bibliography{./../../Livre}
	

	
\end{document}