\documentclass[a4paper,11pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais,english]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{algorithm}
\usepackage{algpseudocode}%

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{enumitem}
\usepackage{appendix}
\usepackage{stmaryrd}
\usepackage{amsmath}
\usepackage{wasysym}
\usepackage{tikz}

\usepackage{listings}


\parskip=0\baselineskip %0.5 d'habitude
\usetikzlibrary{automata,positioning,shadows}
\usetikzlibrary{calc, shapes, backgrounds,arrows}

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}{\textcolor{blue}}
	\newcommand{\textgreen}{\textcolor{Green}}
	\newcommand{\textred}{\textcolor{red}}
	\newcommand{\textbrown}{\textcolor{Brown}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Leçon 930 : Sémantique des langages de programmation. Exemples.}
	
	\date{2018 - 2019}
	\author{Julie Parreaux}
	
	\maketitle
	
	\titlebox{blue}{Références pour la leçon}{
		\textblue{\cite{NielsonNielson}} Nielson et Nielson, \emph{Semantics with application.}
	}
	
	\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
		\begin{minipage}{.5\textwidth}
			Équivalence petit pas et grand pas
		\end{minipage} \hfill
		\begin{minipage}{.45\textwidth}
			Complétude de la logique de Hoare
		\end{minipage} 
	}
	
	\renewcommand{\contentsname}{Plan de la leçon}
	\tableofcontents

	\section*{Motivation}
	
	\subsection*{Défense}
	
	Qu'est qu'un programme ? Un programme est une suite d'instructions qui à partir d'une entrée vérifiant une précondition donne une sortie vérifiant une postcondition. Ces deux conditions donnent alors la spécification de notre programme.
	
	On s'intéresse alors à plusieurs problème. Le premier qui est le plus naturel est de savoir si un programme est correct, c'est-à-dire si celui-ci vérifie sa spécification (quand on lui donne une entrée vérifiant la précondition nous renvoie une sortie qui vérifie la postcondition). Le suivant est de savoir si un programme est équivalent à un autre : si on donne la même entrée aux deux programmes alors on obtient le même résultat. Le troisième problème et le dernier que nous abordons est la preuve de la correction d'une transformation de programmes qui intervient notamment lors de la compilation. On cherche alors à automatiser ces preuves en formalisant et en axiomatisant la sémantique d'un programme (en lui donnant un certain sens).
	
	La sémantique d’un langage de programmation c’est un modèle mathématique permettant de raisonner sur le comportement attendu des programmes de ce langages. Une sémantique peut prendre des formes mathématiques variées et nous en présentants quelques unes ici.
	
	\subsection*{Ce qu'en dit le jury}
		
	L’objectif est de formaliser ce qu’est un programme : introduction des sémantiques opérationnelle et dénotationnelle, dans le but de pouvoir faire des preuves de programmes, des preuves d’équivalence, des preuves de correction de traduction.
		
	Ces notions sont typiquement introduites sur un langage de programmation (impératif) jouet. On peut tout à fait se limiter à un langage qui ne nécessite pas l’introduction des CPOs et des théorèmes de point fixe généraux. En revanche, on s’attend ici à ce que les liens entre sémantique opérationnelle et 	dénotationnelle soient étudiés (toujours dans le cas d’un langage jouet). Il est aussi important que la leçon présente des exemples d’utilisation des notions introduites, comme des preuves d’équivalence de programmes ou des preuves de correction de programmes.
		
	
	\section{Le langage IMP \cite[p.7]{NielsonNielson}}
	
	\textblue{On travaille sur un langage jouet impératif mettant en place la notion de mémoire via les affectation et la boucle \textsf{while}.}
	
	\subsection{Syntaxe du langage IMP}
	\begin{itemize}
		\item \emph{Définition}: Syntaxe et grammaire du langage IMP
		\item \emph{Exemple}: Factorielle et Fibonacci (arbre de syntaxe)
		\item \textblue{Parler de syntaxe concrète vs la syntaxe abstraite}
	\end{itemize}
	
	\textblue{Quelques remarques: le parenthésage de notre séquence d'instruction n'est pas fondamental car ils sont tous équivalents car la séquence d'instruction est associative. Contre-exemple quand ce n'est pas le cas?}
	
	\subsection{Sémantique dénotationnelle des expressions}
	
	\textblue{Les sémantiques, ou plus exactement les fonctions sémantiques que l'on définies ici, sont très générique, mais à l'aide des expressions légales on les spécialise comme on le souhaite.}
	
	\begin{itemize}
		\item \emph{Idée}: Les sémantiques dénotationnelles modélisent le comportement par une fonction mathématique.
		\item \emph{Définition}: Sémantique dénotationnelle des expressions
		\begin{itemize}
			\item Sémantique des nombres : $\mathcal{N} : NUM \to \Z$ \textblue{(on identifie les nombreux aux entiers)}.
			\item Sémantique des variables \textblue{(amène la question de la mémoire)} : $STATE~: Var \to \Z$ 
			\item Sémantique des expressions arithmétiques $\mathcal{A}$ et booléennes $\mathcal{B}$ \textblue{(indépendance à la syntaxe + expressivité de nos booléens : on peut exprimer toutes les fonctions booléennes)} .
		\end{itemize}
		\item \emph{Remarque} : On suppose $State$ totale  \textblue{(toutes les variables sont définies et on les initialise à $0$; même si elle pourrait être partielle , fait l'impasse sur les cas d'erreurs)} + représentation avec liste finie.
		\item \emph{Exemple} : $s = [x \mapsto 3]$; $\mathcal{A} \llbracket x + 1 \rrbracket_s =  4$ et $\mathcal{B} \llbracket \neg (x = 1) \rrbracket_s =  tt$. 
		\item \emph{Remarque} : totalité des fonctions $\mathcal{A}$ et $\mathcal{B}$ + \emph{contre-exemple} : ajout d'une opération binaire de division.
	\end{itemize}
	
	 \textblue{Quelques notions : la notion de variable libre toutes les variables que l'on considères sont libres et de substitution : on souhaite décrire certaines propriétés sur nos sémantiques future: ces notions permettent de formaliser deux concepts : la portée d'une variable (lorsqu'on a des fonctions ou des blocs) et la dépendance syntaxique (est-on atteint par la substitution)). Ces notions ne sont pas nécessaires car on suppose que toutes les variables sont libres et on n'utilise pas la notion de substitution.}
		
	\section{Sémantiques opérationnelles}
	
	\noindent\emph{Idée} : Les sémantiques opérationnelles modélisent le comportement par un système de transitions dont les règles $\left< S, s \right> \to s'$ modélisent le fait "l'exécution de $S$ sur l'état $s$ termine dans l'état $s'$".
	
	\subsection{Sémantique opérationnelle à grand pas  \textblue{(sémantique naturelle)} \cite[p.20]{NielsonNielson}}
	
	\begin{description}
		\item[\textblue{Définition de la sémantique}]
		\begin{itemize}
			\item \emph{Définition} : règles inductives
			\item \emph{Définition} : d'arbre de dérivation \textblue{(Notion de terminaison?)} 
			\item \emph{Exemple} : factorielle 
		\end{itemize} 
		\item[\textblue{Fonction sémantique}]
		\begin{itemize}
			\item \emph{Proposition}: sémantique déterministe \textblue{($\Rightarrow$ induction sur la hauteur de l'arbre; $\Leftarrow$ évident)}
			\item \emph{Application} : IMP est déterministe \textblue{(Induction sur la structure des instructions)}
			\item \emph{Contre-exemple} : instruction \textsf{choose} et IMP est non-déterministe
			\item \emph{Définition} : fonction sémantique (fonction car déterministe) \textblue{(on s'en sert pour montrer l'équivalence de programme)}
		\end{itemize} 
		\item[\textblue{Propriétés de notre sémantique}] 
		\begin{itemize}
			\item \emph{Définition}: Équivalence de langage
			\item \emph{Exemple} $\mathsf{while}~b~\mathsf{do}~S$ est sémantiquement équivalent à $\mathsf{if}~b~\mathsf{then}~(S;~\mathsf{while}~b~\mathsf{do}~S)~\mathsf{else~skip}$ \textblue{(utile dans le DEV 1)} et $S_1; (S_2; S_3)$ et $(S_1; S_2); S_3$ sont sémantiquement équivalents
			\item \emph{Contre-exemple} : $x := 3; x := x \oplus 1$ n'est pas équivalent à $x := x \oplus 1; x := 3$
		\end{itemize}
	\end{description}
	
	\textblue{Extension du langage \textsf{repeat until} ou \textsf{for}}
	
	\textblue{Preuve de programme \cite[p.206]{NielsonNielson}}
	
	\emph{Limite}: La sémantique à grand pas ne permet pas d'exprimer les interruptions de programmes du à la division par $0$ par exemple.
	
	\subsection{Sémantique opérationnelle à petit pas \textblue{(sémantique structurelle)} \cite[p.33]{NielsonNielson}}
	
	\textblue{La sémantique à petit pas ne traite de la terminaison que par la fonction sémantique qui n'est pas toujours facile à manipuler. La sémantique à petit pas vient combler ce manque.}
	
	\noindent\emph{Idée}: La sémantique à petits pas est plus fine que la précédente : elle se concentre sur les étapes de l'exécution (affectations et tests).
	
	\begin{description}
		\item[\textblue{Définition de la sémantique}] 
		\begin{itemize}
			\item \emph{Définition}: règles inductives 
			\item \emph{Définition}: séquence de dérivation \textblue{Notion de terminaison avec des séquences finies ou non?}
			\item \emph{Exemple}: factorielle
			\item \textblue{Structure horizontale qui représente l'évolution temporelle de notre calcul et une structure verticale qui représente les sous calcul que nous devons effectuer.} 
		\end{itemize}
		\item[\textblue{Fonction sémantique}] 
		\begin{itemize}
			\item \emph{Proposition}: Langage déterministe sous la sémantique à petits pas
			\item \emph{Application}: IMP est déterministe \textblue{(induction sur $k$, longueur de la séquence)}
			\item \emph{Définition}: fonction sémantique \textblue{on s'en sert pour montrer l'équivalence de programme}
		\end{itemize} 
		\item[\textblue{Propriétés de notre sémantique}] 
		\begin{itemize}
			\item \emph{Proposition}: Propriétés de comportement de la sémantique à grand pas sur la séquence (décomposition du nombre de chemin et indépendance de l'exécution sur les actions suivantes) \textblue{(utiles pour le DEV 1)} \item \emph{Contre-exemple} : $<S_1; S_2, s> \Rightarrow^* <S_2, s'>$ n'implique pas nécessairement $<S_1, s> \Rightarrow^* s'$ \textblue{(on en dérive un principe d'induction sur le nombre de pas dans notre séquence)}.
			\item \emph{Définition}: Équivalence de langage
			\item \emph{Exemples} $\mathsf{while}~b~\mathsf{do}~S$ est sémantiquement équivalent à $\mathsf{if}~b~\mathsf{then}~(S;~\mathsf{while}~b~\mathsf{do}~S)~\mathsf{else~skip}$; $S_1; (S_2; S_3)$ et $(S_1; S_2); S_3$ sont sémantiquement équivalent 
			\item \emph{Contre-exemple} : $S_1; S_2$ n'est pas équivalent à $S_2; S_1$.
		\end{itemize}
	\end{description}
	 
	 \textblue{Extension du langage \textsf{repeat until} ou \textsf{for}}

	\textblue{Preuve de programme \cite[p.206]{NielsonNielson}}
	
	\subsection{Équivalence des deux sémantiques}
	
	\textblue{On a défini deux sémantiques opérationnelles permettant de décrire le sens d'un programme, on voudrait maintenant savoir si l'une d'entre elles est plus expressive. En réalité, les deux sémantiques que nous avons définies sont équivalentes: elles expriment la même chose.}
	
	\begin{itemize}
		\item \emph{Théorème fondamental} : Équivalence petits pas et grands pas 
		$\forall S$, $\mathcal{S}_{NS} \llbracket S \rrbracket = \mathcal{S}_{SOS} \llbracket S \rrbracket$ \textred{DEV}
		\item \emph{Corollaire} : L'exécution d'une instruction à partir de n'importe quel état termine dans une sémantique si et seulement si elle termine dans l'autre. \textblue{Le théorème fondamental exprime cette propriétés.}
	\end{itemize}
	
	\section{Sémantique dénotationnelle \cite[p.91]{NielsonNielson}}
	
	\begin{itemize}
		\item \emph{Idée}: On définie la sémantique dénotationnelle pour les instructions du langage IMP (modélisation par les fonctions compositionnelles). On utilise alors la théorie du point fixe afin de définir la sémantique du \textsf{while}. \textblue{Elle nous donne le résultat sans forcément expliquer comment on calcul en décrivant une relation entre les entrées et les sorties.}
		\item \emph{Remarque}: On commence par définir la sémantique pour l'ensemble des instructions. Puis, on montre que cette sémantique est bien définie en détaillant la théorie du point fixe.
	\end{itemize}
	
	\subsection{Sémantique dénotationnelle}
	
	\begin{description}
		\item[\textblue{Définition de la sémantique}] 
		\begin{itemize}
			\item \emph{Définition}: Règles : introduction de la fonction $F$ pour le \textsf{while} \textblue{on essaye (et on réussit) à faire passer l'intuition de pourquoi on en a besoin (car elle est uniquement compositionnelle, il nous faut donc des sous-terme strict ce que la boucle \textsf{while} ne peut nous garantir) et de la complexité de répondre à cette question (le nombre de choix possibles pour une instruction données car les contraintes sont uniquement sur les instructions qui terminent); on met le type de $Fix$ qui se lit le plus petit points fixes.} 
			\item \emph{Exemple}: instruction simple avec deux points fixes possible \textblue{(plusieurs fonctions points fixes possibles)}
		\end{itemize}
		\item[\textblue{Propriété de la sémantique}] 
		\begin{itemize}
			\item \emph{Définition}: Équivalence de langage
			\item \emph{Exemple}: $S$ et $S; \textsf{skip}$ sont sémantiquement équivalents
		\end{itemize}
	\end{description} 
	
	\textblue{Exemples de transformation de programme}
	
	\subsection{Théorie du point fixe}
	\textblue{Permet de montrer que $Fix F$ est bien définie et donc que notre sémantique est bien définie}
	\begin{itemize}
		\item \emph{Définitions}: ordre partiel (exemple : $State \hookrightarrow State$) et chaîne
		\item \emph{Exemple}: $g_n s = \left\{ \begin{array}{cl}
		undef & \text{si } x > n \\
		s[x \mapsto 1] & \text{si } 0 \leq x \leq n \\
		s & sinon
		\end{array}\right.$
		\item \emph{Définition}: CCPO
		\item \emph{Proposition}: $\bot$ est la borne inférieure de $State \hookrightarrow State$
		\item \emph{Définition}: Fonctions monotones et continues
		\item \emph{Proposition}: $F(g)$ est continue
		\item \emph{Théorème}: Kleene (point fixe)
		\item \emph{Conséquence}: Bonne définition de la sémantique
	\end{itemize}
	
	\subsection{Équivalence des sémantiques}
	
	\begin{itemize}
		\item \emph{Théorème}: Équivalence avec la sémantique opérationnelle à petits pas 
		\item \emph{Corollaire}: Équivalence avec la sémantique opérationnelle à grands pas 
	\end{itemize}
	
	\section{Sémantique axiomatique (Hoare partiel) \cite[p.213]{NielsonNielson}}
	
	\textblue{Afin de faciliter la preuve automatiser, nous allons définir une logique particulière qui va représenter la sémantique.}
	
	\begin{itemize}
		\item \emph{Idée} : Cette sémantique facilite la preuve de programmes et son automatisation. On se base sur les systèmes de déduction en logique que nous sommes capable de bien manipuler. \textblue{(correction partielle + terminaison = correction totale)}
		\item \emph{Correction partielle}: on donne des garanties que si le programme termine.
	\end{itemize}
	
	\subsection{Triplet de Hoare}
	\textblue{On définit la syntaxe et la sémantique de cette logique. Dans le Nielson \cite{NielsonNielson}, on définie la logique de Hoare comme une sur-logique: on met n'importe quelle logique dans ces prédicat. La définition syntaxe, sémantique, déduction est alors un peu bancale: il faut au moins en avoir conscience.}
	\begin{itemize}
		\item \emph{Définition}: Syntaxe des prédicats
		\item \emph{Définition}: Sémantique des prédicats
		\item \emph{Définition}: triplet de Hoare
		\item \emph{Exemple}: triplet pour la factorielle
		\item \emph{Définition}: validité \textblue{On la définie avec la sémantique à grands pas mais on pourrait le faire avec n'importe quelle autre (par l'équivalence).}
		\item \emph{Définition}: équivalence de programme
		\item \emph{Exemple}: programme équivalent
	\end{itemize} 
	
	\subsection{Logique de Hoare partielle}
	\textblue{On présente ici le système de déduction de cette logique : cette sémantique.}
	\begin{itemize}
		\item \emph{Définition}: Règles d'inférence de la logique
		\item \emph{Exemple}: Preuve d'un programme simple
		\item \emph{Définition}: Arbre de preuve
		\item \emph{Exemple}: Preuve d'un programme simple
		\item \emph{Proposition}: pour tout $S$ et $P$, on a $\vdash_p \{P\} S \{\mathsf{True}\}$
		\item \emph{Définition}: Prouvé sémantiquement
		\item \emph{Exemple}: $S; \textsf{skip}$ et $S$ prouvé sémantiquement
	\end{itemize}	
	 
	\subsection{Correction et complétude}
	\textblue{La logique que nous avons définie ne fait pas n'importe quoi.}
	\begin{itemize}
		\item \emph{Théorème}: Correction de la logique
		\item \emph{Définition}: Précondition la plus faible $wlp$
		\item \emph{Lemmes}: propriétés sur cette précondition
		\item \emph{Théorème}: Complétude de la logique \textred{DEV}
	\end{itemize}	
	
	\section{Ouverture}
	\addcontentsline{toc}{section}{Ouverture}
	\begin{itemize}
		\item Les sémantiques offrent de nombreuses sémantiques.
		\begin{itemize}
			\item Certification d'un compilateur
			\item Vérification de la sécurité d'un programme
		\end{itemize}
		\item D'autres sémantiques: sémantique à continuation pour la sémantique opérationnelle à petit pas.
	\end{itemize}	
	
	\section*{Quelques notions importantes}
	
	\subsection*{Logique de Hoare}
	\paragraph{La correction de la logique de Hoare partielle}
	\input{./../Notions/Semantique_HoareCorrection.tex}
	
	\paragraph{Logique de Hoare}
	\input{./../Notions/Semantique_Hoare.tex}
		
	\subsection*{Les sémantiques opérationnelles}
	\paragraph{Sémantique opérationnelle à grands pas}
	\input{./../Notions/Semantique_GrandsPas.tex}
	
	\paragraph{Sémantique opérationnelle à grands pas}
	\input{./../Notions/Semantique_PetitsPas.tex}
	
	\bibliographystyle{plain}
	\bibliography{./../../Livre}
	
\end{document}