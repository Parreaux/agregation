\documentclass[a4paper,11pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais,english]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}
\usepackage{subcaption}
\usepackage{multirow}

\usepackage{algorithm}
\usepackage{algpseudocode}%

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{listings}
\usepackage{wasysym}


\parskip=0\baselineskip %0.5 d'habitude
\usetikzlibrary{automata,positioning,shadows}
\usetikzlibrary{calc, shapes, backgrounds,arrows}

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Leçon 903 : Exemples d'algorithmes de tri. Correction et complexité }
	
	\date{2018 - 2019}
	\author{Julie Parreaux}
	
	\maketitle
	
	\titlebox{blue}{Références pour la leçon}{
		\textblue{\cite{Beauquier-Berstel-Chretienne}} Beauquier, Berstel et Chretienne, \emph{Éléments d'algorithmique.}
		
		\textblue{\cite{Cormen}} Cormen, \emph{Algorithmique.}
		
		\textblue{\cite{Froidevaux-Gaudel-Soria}} Froidevaux, Gaudel et Soria, \emph{Types de données et algorithmes.}
	}
	
	\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
		\begin{minipage}{.35\textwidth}
			Le tri par tas
		\end{minipage} \hfill
		\begin{minipage}{.5\textwidth}
			Le tri topologique
		\end{minipage}
	}
	
	\renewcommand{\contentsname}{Plan de la leçon}
	\tableofcontents

	\section*{Motivation}
	
	\subsection*{Défense}
	
	Le problème de tri est considéré par beaucoup comme un problème fondamental en informatique. Mais pourquoi trier?
	\begin{itemize}
		\item Le problème de tri peut être inhérent à l'application : on cherche très souvent à classer des objets suivant leurs clés, comme lors de l'établissement des relevés bancaires.
		\item Le tri est donc souvent utilisé en pré-traitement dans de nombreux domaines de l'algorithmique : la marche de Jarvis (enveloppe convexe), l'algorithme du peintre (rendu graphique : quel objet je dois afficher en dernier pour ne pas l'effacer par d'autres?), la recherche dans un tableau (dichotomie), l'algorithme de Kruskal (arbre couvrant minimal) ou encore l'implémentation de file de priorité.
		\item Le tri a un intérêt historique: de nombreuses techniques ont été élaborées pour optimiser le tri.
		\item Le problème de tri est un problème pour lequel on est capable de trouver un minorant (en terme de complexité).
		\item L'implémentation d'algorithme de tri fait apparaître de nombreux problèmes techniques que l'on cherche à résoudre via l'algorithmique.
	\end{itemize}
	
	Dans cette leçon, nous effectuons deux hypothèses importantes : les éléments à trier tiennent uniquement en mémoire vive et l'ordre sur ces éléments (sous lequel on tri) est total.
	
	\subsection*{Ce qu'en dit le jury}
	
	Sur un thème aussi classique, le jury attend des candidats la plus grande précision et la plus grande rigueur.
	
	Ainsi, sur l’exemple du tri rapide, il est attendu du candidat qu’il sache décrire avec soin l’algorithme de partition et en prouver la correction en exhibant un invariant adapté. L’évaluation des complexités dans le cas le pire et en moyenne devra être menée avec rigueur : si on utilise le langage des probabilités, il importe que le candidat sache sur quel espace probabilisé il travaille.
	
	On attend également du candidat qu’il évoque la question du tri en place, des tris stables, des tris externes ainsi que la représentation en machine des collections triées.
	
	\section{Le problème de tri}
	\begin{itemize}
		\item Problème du tri \cite[p.122]{Beauquier-Berstel-Chretienne} \textblue{les objets sont triés selon une clé (données satellites ou non)}
		
		\begin{tabular}{cl}
			entrée & $n$ éléments $a_1, \dots, a_n$ d'un ensemble $E$ totalement ordonné \\
			sortie & une permutation des éléments $\sigma \in \mathfrak{S}_n$ telle que $s_{\sigma(1)} \leq \dots \leq a_{\sigma(n)}$
		\end{tabular}
		
		\item \emph{Hypothèses}: tri interne (tout est en mémoire vive) et ordre total
		\item \emph{Définition}: tri stable \cite[p.307]{Froidevaux-Gaudel-Soria}
		\item \emph{Exemples} de tri dont un stable et l'autre non
		\item \emph{Définition}: tri en place \cite[p.136]{Cormen}
		\item Critère de comparaison des algorithmes de tri: complexité temporelle (pire cas / en moyenne); complexité spatiale; stabilité; caractère en place.
	\end{itemize}
	
	\begin{center}
		\textblue{\begin{tabular}{|c|ccccc|}
			\hline
			Tri & Pire cas & En moyenne & Spatiale & Stable & En place \\
			\hline
			Sélection & $O(n^2)$ & $O(n^2)$ & $O(1)$ & $\CheckedBox$ & $\CheckedBox$ \\
			Insertion & $O(n^2)$ & $O(n^2)$ & $O(1)$ & $\CheckedBox$ & $\CheckedBox$ \\
			Fusion & $O(n \log n)$ & $O(n \log n)$ & $O(n)$ & $\XBox$ & $\XBox$ \\
			Tas & $O(n \log n)$ & $-$ & $O(1)$ & $\XBox$ & $\CheckedBox$ \\
			Rapide & $O(n^2)$ & $O(n \log n)$ & $O(1)$ & $\XBox$ & $\CheckedBox$ \\
			Dénombrement & $O(k + n)$ & $O(k + n)$ & $O(k)$ & $\CheckedBox$ & $\XBox$ \\
			Base & $O(d(k + n))$ & $O(d(k + n))$ & $O(dk)$ & $\CheckedBox$ & $\XBox$ \\
			Paquets & $O(n^2)$ & $O(n)$ & $O(n)$ & $\XBox$ & $\XBox$ \\
			\hline
		\end{tabular}}
	\end{center}
	
	\section{Les tris par comparaison}
	\textblue{Ici la complexité de nos algorithmes peuvent être calculer en nombre de comparaisons effectuées.}
	
	\begin{itemize}
		\item \emph{Définition}: Tri par comparaison \cite[p.178]{Cormen} \textblue{On s'autorise alors uniquement cinq tests sur les données à l'entrée : $=, <, >, \leq, \geq$.}
		\item \emph{Théorème}: borne optimale des tris par comparaisons $\Omega(n \log n)$ \cite[p.179]{Cormen} \textblue{On peut faire un premier classement des tris: ceux qui sont optimaux en moyenne, au pire cas ou pas du tout.}
	\end{itemize}
	
	\subsection{Les tris naïfs \cite[p.310]{Froidevaux-Gaudel-Soria}}
	\textblue{On commence par étudier les tris naïfs: ceux qui ne mettent en place aucun paradigme de programmation ni structures de données élaborées.}
	
	\paragraph{Tri par sélection}
	\begin{itemize}
		\item \emph{Principe}: on cherche le minimum des éléments non triés et on le place à la suite des éléments triés
		\item Algorithmes classique (algorithme~\ref{algo:tri_selection-naif}) \textblue{(se fait de manière récursive en cherchant le minimum de manière itérative à chaque fois)} et tri à bulle (algorithme~\ref{algo:tri_selection-bulle}) \textblue{le tri à bulle est un des tri par sélection le plus simple à programmer : il se base sur l'idée que l'on part de la fin de la liste et qu'on fait remonter chacun des éléments tant qu'il est plus petit que celui devant lui. On peut également parler du tri boustrophédon qui est un tri à bulle dans les deux sens (on fait descendre les éléments les plus lourds et remonter les plus légers).}
		\item \emph{Complexité}: $O(n^2)$
		\item \emph{Propriétés}: stable et en place
	\end{itemize}
	
	\paragraph{Tri par insertion} \textblue{(le tri par insertion est aussi appeler la méthode du joueur de carte)}
	\begin{itemize}
		\item \emph{Principe}: On insère un à un les éléments parmi ceux déjà trié.
		\item Algorithme~\ref{algo:tri_insertion-sequentiel} 
		\item \emph{Complexité}: $O(n^2)$
		\item \emph{Propriétés}: stable et en place
		\item \emph{Remarques}: très efficace sur des petits tableau ou sur des tableau presque trié. \textblue{Java implémente ce tri pour des tableau de taille inférieure ou égale à 7.}
	\end{itemize}
	
	\subsection{Diviser pour régner}
	\textblue{On présente maintenant des algorithmes de tri basé sur le paradigme diviser pour régner: le premier découpe simplement le tableau de départ (tri fusion) tandis que le second combine facilement les deux sous tableau triés (tri rapide).}
	\paragraph{Tri fusion \cite[p.30]{Cormen}}
	\begin{itemize}
		\item \emph{Principe}: On découpe l'ensemble des données en deux sous-ensembles de même taille que l'on tri séparément. Ensuite, nous combinons ces deux ensembles en les entrelaçant.
		\item Algorithme~\ref{algo:tri_fusion}
		\item \emph{Complexité} (pire cas et moyenne): $O(n \log n)$
		\item \emph{Application}: Calcul de jointure dans le cadre des bases de données
	\end{itemize}
	
	\paragraph{Tri rapide \cite[p.157]{Cormen}} \textblue{Cet exemple est intéressant d'un point de vu du tri puisqu'il possède de bonne performance. De plus, c'est un algorithme soumis au principe de diviser pour régner dont on ne connaît pas la taille des sous-problème à priori: c'est un bon contre-exemple au Master theorem.}
	\begin{itemize}
		\item \emph{Principe}: On sépare l'ensemble des données en deux sous-ensembles tels que le premier sous-ensemble ne contient que des valeurs inférieures à un pivot et que le second que les valeurs supérieures à ce pivot. On tri ensuite chacun de ces deux sous-ensembles et on les combines en les concaténant.
		\item Algorithme~\ref{algo:tri_rapide}
		\item \emph{Complexité} dans le pire cas: $O(n^2)$; en moyenne $O(n \log n)$
		\item \emph{Propriété}: en place; le plus rapide en pratique
	\end{itemize}
	
	\textblue{Il existe des tris mixtes qui en fonctions de l'ensemble des données (taille, caractère trié) que l'on présente choisi l'un ou l'autre des algorithmes de tri.}
	
	\subsection{Structure de données \cite[p.140]{Cormen}}
	\textblue{C'est un algorithme de tri qui se base sur les propriétés d'une structure de données bien précise: le tas. Elle permet de gérer les données. Le terme tas qui fut d'abord inventé dans ce contexte est aujourd'hui également utilisé dans un contexte d'exécution d'un programme: c'est une partie de la mémoire qu'utilise un programme. Un programme lors qu'il s'exécute utilise une mémoire de pile (qui donne les instructions suivantes à faire) et un tas (qui contient les valeurs des variables, de la mémoire auxiliaire pour faire tourner le programme). Ces deux mémoires grandissent l'une vers l'autre (l'erreur de \emph{stack overflow} arrive quand la pile rencontre le tas).}
	\begin{itemize}
		\item \emph{Définition}: un tas
		\item \emph{Principe}: On construit un tas avec les éléments de l'entrée et on extrait le maximum de ce tas itérativement.
		\item Algorithme, correction et complexité \textred{DEV}
		\item \emph{Propriétés}: en place
		\item \emph{Application}: file de priorité
	\end{itemize}
	
	\textblue{Le tri par tas est un tri en place et de complexité $O(n\log n)$ en moyenne. C'est donc un des meilleurs tri par comparaison que l'on possède.}
	
	\section{Les tris linéaires}
	\textblue{On fait des hypothèses sur l'entrée de nos algorithmes afin d'obtenir un tri linéaire. De plus, ces algorithmes fond appel à d'autres opérations que les comparaisons: on fait abstraction de la borne de minimalité.}
	
	\paragraph{Tri par dénombrement \cite[p.180]{Cormen}} 
	\begin{itemize}
		\item \emph{Hypothèse}: on suppose que les valeurs de l'entrées sont comprises entre $0$ et $k \in \N$ fixé.
		\item \emph{Principe}: déterminer pour chaque élément $x$ combien sont inférieur à lui. 
		\item \emph{Méthode}: dans un tableau $C$ de longueur $k$ : $C[i]$ contient le nombre de clés $\leq i$.
		\item Algorithme~\ref{algo:tri_denombrement} et exemple (Figure~\ref{fig:tri-denombrement}) 
		\item \emph{Propriétés}: tri stable \textblue{Faire attention si plusieurs valeurs sont égales.}
		\item \emph{Complexité}: $O(n + k)$ \textblue{si $k = O(n)$ on retrouve bien le linéaire}
	\end{itemize}
	
	\paragraph{Tri par paquets \cite[p.185]{Cormen}}
	\begin{itemize}
		\item \emph{Hypothèse}: les clés sont uniformément distribuées sur $[0, 1[$.
		\item \emph{Principe}: on divise $[0, 1[$ en $n$ intervalles de même taille: on distribue les entrées dans ces intervalles et on les tri par insertion dans chaque intervalle avant de les concaténer.
		\item Algorithme~\ref{algo:tri_paquets} 
		\item \emph{Complexité espérée}: $O(n)$.
	\end{itemize}
	
	\paragraph{Tri par base \cite[p.182]{Cormen}}
	\begin{itemize}
		\item \emph{Hypothèse}: $d$ sous-clés entières bornées muni d'un poids de plus en plus fort
		\item \emph{Principe}: trier les sous-clé du poids le plus faible à l'aide d'un tri par dénombrement \textblue{sa stabilité est essentielle}
		\item \emph{Applications}: trier des cartes perforées; trier des dates; trier des chiffres + Exemple (Figure~\ref{fig:tri-base}) 
		\item \emph{Propriétés}: tri stable; pas en place \textblue{(tri par dénombrement)}
		\item \emph{Complexité}: $O(n)$
	\end{itemize}
	
	Tri par dénombrement, tri par paquet (tri suffixe), tri par base
	
	\section{Affaiblissement des hypothèses pour le tri}
	
	\textblue{Au début de la leçon nous avons posé quelques hypothèses sur le tri que nous allons faire (l'ordre que nous allons utilisé et l'espace mémoire nécessaire pour stoker l'ensemble des clés). Dans cette partie, nous allons affaiblir certaine d'entre elles pour examiner les tris que nous pouvons alors obtenir.}
	
	\subsection{Tri topologique \cite[p.565]{Cormen}}
	\textblue{\emph{Hypothèse}: on n'utilise maintenant plus qu'un ordre partiel: on utilise l'ordre donné par un graphe.}
	\begin{itemize}
		\item Problème du tri topologique
		
		\begin{tabular}{cl}
			entrée & $G = (V, E)$  un graphe orienté acyclique. \\
			sortie & $L$ une liste constituée des éléments de $S$ telle que si $(u, v) \in E$, $u$ apparaît avant $v$ dans $L$.
		\end{tabular}
		\item \emph{Remarque}: ordre partiel induit par le graphe
		\item Algorithme du tri topologique et correction \textred{DEV}
	\end{itemize}
	
	\subsection{Tri externe}
	On s'autorise des données qui ne tiennent pas en mémoire vive.
	
	\section*{Ouverture}
	\addcontentsline{toc}{section}{Ouverture}
	Tri de shell
	
	Réseau de tri
	
	
	
	\section*{Quelques notions importantes}
	
	\subsection*{Les tris par comparaisons}
	\input{./../Notions/Tri_comparaison.tex}
	
	\subsection*{Les tris linéaires}
	\input{./../Notions/Tri_Lineaire.tex}
	
	\bibliographystyle{plain}
	\bibliography{./../../Livre}


	
\end{document}