\documentclass[a4paper,11pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais,english]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{multirow}
\usepackage{multicol}
\usepackage{caption}

\usepackage{algorithm}
\usepackage{algpseudocode}%
\usepackage{tikz}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{listings}


\parskip=0\baselineskip %0.5 d'habitude
\usetikzlibrary{automata,positioning,shadows}
\usetikzlibrary{calc, shapes, backgrounds,arrows}

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}{\textcolor{blue}}
	\newcommand{\textgreen}{\textcolor{Green}}
	\newcommand{\textred}{\textcolor{red}}
	\newcommand{\textbrown}{\textcolor{Brown}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{cex}{Contre-exemple}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	
	\title{Leçon 913:  Machines de Turing. Applications.}
	
	\date{2018 - 2019}
	\author{Julie Parreaux}
	
	\maketitle
	
	\titlebox{blue}{Références pour la leçon}{
		\textblue{\cite{Lassaigne-Rougemont}} Lassaigne et Rougemont, \emph{Logique et fondements de l'informatique. Logique du 1$^{er}$ ordre, calculabilité et $\lambda$-calcul.}
		
		\textblue{\cite{Sisper}} Sipser, \emph{Introduction to the Theory of Computation.}
		
		\textblue{\cite{Stern}} Stern, \emph{Fondements mathématiques de l'informatique.}
		
		\textblue{\cite{Turing-Girard}} Turing et Girard, \emph{La machine de Turing.}
		
		\textblue{\cite{Wolper}} Wolper, \emph{Introduction à la calculabilité.}
	}
	
	\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
		\begin{minipage}{.5\textwidth}
			Turing-calculable $\Rightarrow$ $\mu$-récursive
		\end{minipage} \hfill
		\begin{minipage}{.45\textwidth}
			Théorème de Savitch
		\end{minipage} 
	}
	
	
	\renewcommand{\contentsname}{Plan de la leçon}
	\tableofcontents	
	

	\section*{Motivation}
	
	\subsection*{Défense}
	
	Les machines de Turing on été introduites par Turing en 1936.
	
	Contexte historique : répondre à la question d'Hilbert: Qu'est-ce qui est calculable? Elle s'inscrive dans la formalisation des modèles de calculs formels qui tendent à répondre à cette question. On peut également cité les fonctions récursives et le $\lambda$-calcul ou la logique car calculer c'est prouver (Gödel). 
	
	Les modèle de calcul formel permettent d'appréhender les limites non-physiques mais bien conceptuelles du calcul et de l'informatique. Les fonctions récursives s'inscrivent dans une démarche qui identifie les fonctions non-calculables. Turing se tourne quant-a lui vers le calcul et tendent à répondre à la question comment calcule-t-on? Les machines de Turing sont alors une réponse de logicien à l'action du calcul tel que réalisé par un humain. Elles servent aujourd'hui d'étalon dans la théorie de la complexité. Les machines de Turing semblent "stable": à vouloir améliorer les Machines de Turing, on retombe toujours sur des machines reconnaissant la même classe de langages: elles semblent englober toute idée de procédure effective.
	
	\subsection*{Ce qu'en dit le jury}
	
	Il s’agit de présenter un modèle de calcul. Le candidat doit expliquer l’intérêt de disposer d’un modèle formel de calcul et discuter le choix des machines de Turing. La leçon ne peut se réduire à la leçon 914 ou à la leçon 915, même si, bien sûr, la complexité et l’indécidabilité sont des exemples d’applications. Plusieurs développements peuvent être communs avec une des leçons 914, 915, mais il est apprécié qu’un développement spécifique soit proposé, comme le lien avec d’autres modèles de calcul, ou le lien entre diverses variantes des machines de Turing. 
	
	\section{Introduction}
	\addcontentsline{toc}{section}{Introduction}
	
	\noindent \emph{Motivations}: 
	\begin{itemize}
		\item Pourquoi avoir un modèle de calcul formel? Répondre à la question de Hilbert: Qu'est-ce qui est calculable. Plusieurs modèles de calculs existent: les fonctions récursives, le $\lambda$-calcul, les machines de Turing, ... Ils tentent tous de répondre à la question.
		\item Pourquoi les machines de Turing ? Même si elles n'ont pas été le premier modèle de calcul formelle, elle sont aujourd’hui considéré comme le modèle abstrait des ordinateurs (modèle RAM). De plus, même si elles n'ont pas apporté de nouvelles réponses à la question de Hilbert, par son approche novatrice elle a permis de classifier les fonctions calculables (et donc les problèmes).
	\end{itemize} 
	
	\section{Les Machines de Turing: un modèle de calcul formel}
	
	\emph{Définition}\cite[p.115]{Lassaigne-Rougemont}: Un modèle de calcul.
	
	\subsection{Vocabulaire autours des machines de Turing \cite[p.103]{Wolper}}
	
	\emph{Définition}: Une machine de Turing déterministe: $(\mathcal{Q}, \Gamma, \Sigma, \delta, s, \#, F)$.
	
	\emph{Définition}: Une configuration $c \in \Gamma^{*} \times \mathcal{Q} \times (\epsilon \cup \Gamma^{*}(\Gamma \setminus \{\#\}))$. (\textblue{Attention, la position de la tête de lecture est donnée implicitement par la taille de la première composition.})
	
	\emph{Exemple}: {dessin d'une configuration illustrant cette position implicite}
	
	\emph{Application} \cite[p.132]{Lassaigne-Rougemont}: Les automates finies: elle parcours le ruban une seule fois sans écrire (machines de Turing sans mémoire).
	
	\emph{Définition}: Configurations suivantes
	
	\emph{Définition}: Une exécution: suite de configurations
	
	\emph{Définition}: Langage accepté / langage décidé (\textblue{Notions clés des théories basés sur les Machines de Turing (décidabilité et complexité)})
	
	\emph{Exemple}: $a^{n}b^{n}c^{n}$ est décidé par une machine de Turing.
	
	
	\subsection{Les machines de Turing calculent \cite[p.59]{Stern}}
	
	\textblue{On veut pouvoir calculer avec les machines de Turing.}
	
	\emph{Exemples}: Additionneur, soustracteur, multiplicateur (par 2, de deux entiers) \textblue{Dessins ajoutés en annexe}
	
	\emph{Définition} \cite{Wolper}: Fonction calculable par une machine de Turing.
	
	\section{Justification de la thèse de Church.}
	
	\emph{Thèse de Church} \cite[p.109]{Wolper}: Les fonctions calculables par un algorithme sont les fonctions calculables par une machine de Turing.
	
	\textblue{C'est une thèse (ni une hypothèse, ni un théorème). Nous ne pouvons pas le montrer formellement (facilement) car la notion d'algorithme ne possède pas de définition formelle. Cependant nous allons mettre en avant deux arguments qui nous conforte cette thèse
		\begin{itemize}
			\item Pas d'extension qui améliore le langage décidé (toute équivalente à une machine de Turing) (sous-section A);
			\item Équivalence avec les autres modèles de calculs évoqué plus tard (sous-section B).
		\end{itemize}}
	
	\subsection{Les extensions d'une machine de Turing décident les mêmes langages.}
	
	\paragraph{Cas des machines à plusieurs rubans} \cite[p.112]{Wolper}
	\begin{itemize}
		\item \emph{Fonction transition}: $\delta : \mathcal{Q} \times \Gamma^{k} \rightarrow \mathcal{Q} \times \Gamma^{k} \times \{\leftarrow; \rightarrow\}^{k}$;
		\item \emph{Configuration}: $c \in (\Gamma^{*})^{k} \times \mathcal{Q} \times (\epsilon \cup \Gamma^{*}(\Gamma \setminus \{\#\}))^{k}$;
		\item \emph{Simulation par une machine à un ruban}: les cases du i$^{ieme}$ ruban sont représentées par les cases de positions $i$ modulo $k$.
	\end{itemize}
	
	\paragraph{Cas des machines à ruban bi-infini} \cite[p.110]{Wolper}
	\begin{itemize}
		\item \emph{Configuration}: $c \in ((\epsilon \cup (\Gamma \setminus \{\#\})\Gamma^{*}) \times \mathcal{Q} \times (\epsilon \cup \Gamma^{*}(\Gamma \setminus \{\#\}))$;
		\item \emph{Simulation par une machine à deux rubans}: on fixe une case d'indice $0$. Le premier ruban représente la partie droite du ruban bi-infini et le second la partie gauche.
	\end{itemize}
	
	\paragraph{Cas des machines non-déterministes} \cite[p.114]{Wolper}
	\begin{itemize}
		\item \emph{Relation du transition}: $\delta \subseteq (\mathcal{Q} \times \Gamma) \times (\mathcal{Q} \times \Gamma \times \{\leftarrow; \rightarrow\})$;
		\item \emph{Configuration suivante}: la machine choisie parmi l'ensemble des triplets obtenus par la relation de transition;
		\item \emph{Langage accepté}: un mot $w$ est accepté si il existe une suite de choix définissant une exécution acceptante. Le langage accepté est l'ensemble des mots acceptés;
		\item \emph{Simulation par une machine déterministe à trois rubans}: le premier ruban contient l'entrée en lecture seule. Le second permet de lister les différentes suites des choix possibles (finies). Le troisième simule la machine non-déterministe en effectuant les choix indiqués par le second ruban.
		\item \emph{Exemple d'utilisation}: satisfiabilité d'une formule du langage propositionnel; langage pour lequel la déterminisation d'un de ces automate explose (\textblue{On a bien l'équivalence mais on met en avant que la taille de la machine déterministe explose ce qui a un impact sur la complexité.}).
	\end{itemize}
	
	\subsection{Les autres modèles de calcul décident les mêmes langages.}
	
	\paragraph{Cas des fonctions récursives} \cite[p.131]{Wolper}:
	\begin{itemize}
		\item \emph{Définition}: Syntaxe des expressions des fonctions $\mu$-récursive.
		\item \emph{Définition}: Sémantique des expressions des fonctions $\mu$-récursive.
		\item \emph{Définition}: Fonction $\mu$-récursive.
		\item \emph{Exemple}: Fonction primitive récursive / Fonction $\mu$-récursive
		\item \emph{Théorème}: Les fonctions $\mu$-récursives sont exactement les fonctions Turing-calculable. \textred{DEV: Turing $\Rightarrow$ $\mu$-récursive.}
		\item \emph{Remarque}: On peut réécrire la thèse de Church avec des fonctions
	\end{itemize}
	
	\paragraph{Cas du $\lambda$-calcul} \cite[p.185]{Lassaigne-Rougemont}:
	\begin{itemize}
		\item \emph{Définition}: Termes.
		\item \emph{Exemple}: Codage des entiers avec des termes du $\lambda$-calcul.
		\item \emph{Théorème}: Les fonctions $\mu$-récursives sont exactement représentable par les termes du $\lambda$-calcul.
		\item \emph{Corollaire}: Les fonctions Turing calculable sont exactement représentable par les termes du $\lambda$-calcul.
	\end{itemize}

	\textblue{D'autres modèles de calcul pourrait être exploités: les circuits booléens par exemple}

	\subsection{La théorie de la calculabilité. \cite[p.139]{Wolper}}
	
	\textblue{La calculabilité se fonde sur la thèse de Church-Turing; elle permet aussi de montrer l’existence de fonctions non-calculables, par un simple argument de diagonalisation.}
	
	\emph{Définition} \cite[p.116]{Wolper}: Machines universelles.
	
	\emph{Définition}: Les langages R et RE + les langages décidables
	\textblue{Donner "l'équivalence" entre langages et programmes}
	
	\noindent \emph{Problème}: \textsc{Arrêt}
	\begin{description}
		\item[entrée] une machine de Turing déterministe $M$; un mot $w$
		\item[sortie] oui si $M(w)$ s'arrête; non sinon
	\end{description}
	
	\emph{Théorème}: Le problème de l'arrêt est indécidable et dans RE.
	
	\emph{Définition}: La réduction \textblue{Dessin}.
	
	\emph{Théorème}: Utilisation des réductions
	
	\noindent \emph{Problème}: \textsc{Pavage de Wang}
	\begin{description}
		\item[entrée] une famille finie de tuiles
		\item[sortie] oui si le jeu de tuiles permet de paver le plan; non sinon
	\end{description}
	
	\emph{Application}: Le pavage de Wang est indécidable 
	
	\emph{Théorème}: Théorème de Rice
	
	 \emph{Application}: Lien avec sémantique: correction de programmes

	\section{Les machines de Turing classent les fonctions calculables}

	\textblue{Les machines de Turing servent d'étalon pour définir les classes de complexité.}
	
	Dans cette partie, nous différencions les machines de Turing déterministes et non-déterministe (\textblue{le facteur exponentielle existant dans la déterminisation ne nous permet plus d'exploité leur équivalence}). Nous avons une première application des différentes variantes de nos machines de Turing.
	
	\emph{Définition}: Arbre de calcul
	
	\emph{Définition}: Machine décide un langage en temps/espace f (\textblue{Attention, en espace: petit piège sur les variantes des machines de Turing : si $f(n) < n$ alors il nous faut une machine à plusieurs rubans}).
	
	\emph{Définition}: classes (N)TIME et (N)SPACE
	
	\emph{Définition}: classes P -> EXPSPACE
	
	\emph{Théorème}: Savitch \textred{DEV} \textblue{Plusieurs versions du théorème, bien être au claire sur celle que nous développons}
	
	\emph{Corollaire} NPSAPCE = PSPACE
	
	\noindent \emph{Problème} \textsc{Universalité}
	\begin{description}
		\item[entrée] une expression rationnelle $E$
		\item[sortie] oui si $L(E) = \Sigma^{*}$; non sinon
	\end{description}
	
	\emph{Proposition} Le problème \textsc{Universalité} est dans PSPACE
	
	\emph{Définition}: NP-complétude
	
	\noindent \emph{Problème}: \textsc{SAT}
	\begin{description}
		\item[entrée] une formule $\phi$ de la logique propositionnelle
		\item[sortie] oui si $\phi$ est satisfiable; non sinon
	\end{description}
	
	\emph{Théorème}: Cook
	
	\emph{Définition}: classes (N)L avec des machines à plusieurs rubans \textblue{Nécessité d'avoir plusieurs types de Machines de Turing.}
	
	\noindent \emph{Problème} \textsc{Accessibilité}
	\begin{description}
		\item[entrée] Un graphe orienté $G$ est deux sommets $s$ et $t$
		\item[sortie] Oui si il existe un chemin de $s$ à $t$ dans $G$
	\end{description}
	
	\emph{Proposition}: Le problème \textsc{Accessibilité} est dans NL.
	
	\emph{Remarque}: Si $G$ est un graphe non-orienté alors le problème arrive dans L.
	
	
	\section*{Ouverture}
	\addcontentsline{toc}{section}{Ouverture}

	On peut parler de la relation grammaire - machine de Turing via la hiérarchie de Chomsky.
	
	Autres modèles de calculs: les circuits booléens. 
	
	
	\section*{Quelques notions importantes}
	
	
	\subsection*{Les machines de Turing calculent}
	\input{./../Notions/MachineTuring-calculent.tex}
	
	\subsection*{Autres variantes des machines de Turing}
	\input{./../Notions/MachineTuring-variante.tex}
	
	\subsection*{Technique de preuve : la réduction}
	\input{./../Notions/Indecidabilite_reduction.tex}
	
	\subsection*{Indécidabilité sur les machines de Turing}
	\input{./../Notions/MT_pbDecisions.tex}
	
	\bibliographystyle{plain}
	\bibliography{./../../Livre}
\end{document}