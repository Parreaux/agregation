\documentclass[a4paper,11pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais,english]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}
\usepackage{subcaption}

\usepackage{algorithm}
\usepackage{algpseudocode}%

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{listings}


\parskip=0\baselineskip %0.5 d'habitude
\usetikzlibrary{automata,positioning,shadows}
\usetikzlibrary{calc, shapes, backgrounds,arrows}

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\small}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{cex}{Contre-exemple}
	\newtheorem*{ex}{Exemple}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	
	\title{Leçon 916 : Formule du calcul propositionnel: représentation, forme normale, satisfiabilité. Applications.}
	
	\date{2018 - 2019}
	\author{Julie Parreaux}
	
	\maketitle
	
	\titlebox{blue}{Références pour la leçon}{
		\textblue{\cite{Duparc}} Duparc, \emph{La logique pas à pas.}
		
		\textblue{\cite{CoriLascar1}} Cori et Lascar, \emph{La logique mathématique, tome 1.}
		
		\textblue{\cite{Stern}} Stern, \emph{Fondements mathématiques de l'informatique.}
	}
	
	\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
		\begin{minipage}{.5\textwidth}
			Transformation de Tseitin
		\end{minipage} \hfill
		\begin{minipage}{.45\textwidth}
			2-SAT est NL-complet (donc dans P)
		\end{minipage} 
	}
	
	\renewcommand{\contentsname}{Plan de la leçon}
	\tableofcontents

	\section*{Motivation}
	
	\subsection*{Défense}
	
	La logique propositionnelle est une logique très simple permettant d'exprimer des contraintes: c'est la première logique que nous utilisons. 
	
	\subsection*{Ce qu'en dit le jury}
	
	Le jury attend des candidats qu’ils abordent les questions de la complexité de la satisfiabilité. Pour autant, les applications ne sauraient se réduire à la réduction de problèmes NP-complets à SAT.
	
	Une partie significative du plan doit être consacrée à la représentation des formules et à leurs formes normales.
	
	\section{Le langage de la logique propositionnelle}
	\textblue{On formalise la logique grâce à une syntaxe, une sémantique et un lien entre les deux donné par un système de preuve (sur la syntaxe). Nous allons également donner plusieurs représentations possibles (dans un ordinateur) de la syntaxe.}
	\subsection{Syntaxe \cite[p.68]{Duparc}}
	\begin{itemize}
		\item \emph{Définition}: langage avec connecteurs minimaux (induction)  + exemples
		\item \emph{Notation}: Autres connecteurs
		\item \emph{Remarque} : Ambiguïté et parenthèses + exemples
	\end{itemize}
	
	\subsection{Représentations}
	\textblue{Comment on représente les mots de ce langage dans un ordinateur? Laquelle est la plus efficace (sous quels critères) ?}
	\begin{itemize}
		\item Représentation linéaire
		\item Représentation arborescente \textblue{(fait apparaître l'induction)} + \emph{Théorème} de la lecture unique \cite[p.57]{CoriLascar1} \textblue{(unicité de la représentation par l'arbre, représentation fortement non ambiguë)}
		\item Représentation DAG (direct acyclique graph) : moins de mémoire \textblue{(utilisé pour la satisfiabilité)}
		\item Représentation BD
		\item Représentation circuit \textblue{(donne de nouvelles classes de complexités)}
	\end{itemize}
	
	\subsection{Sémantique \cite[p.83]{Duparc}}
	\textblue{Comment donner du sens à ce qu'on écrit? Que signifie ces formules?}
	\begin{itemize}
		\item Valuation sur un modèle
		\item \emph{Application} : traduction du langage naturel
		\item \emph{Problèmes}: validité et satisfiabilité \textblue{(deux notions duales)}
		\item Une représentation : table de vérité \textblue{(c'est une méthode pratique, ordonnée, pour tester la validité d'une formule)}
		\item \emph{Remarque}: complexité de calcul \textblue{(Conséquence de Cook)}
		\item \emph{Théorème} Cook
		\item \emph{Application} : réduction de problème pour NP-dureté (problème de séparation des automates)
		\item \emph{Définition} : tautologie / contradictions
	\end{itemize}
	
	\subsection{Théories et preuves}
	\textblue{Vérifier que nous n'avons pas fait n'importe quoi avec ces définitions...}
	\begin{itemize}
		\item \emph{Définition} : théorie
		\item \emph{Théorème} : compacité
		\item \emph{Applications} : coloriage de sous-graphe // pavage // logique du premier ordre
		\item \emph{Définition}: CNF \textblue{(on voit après comment la mettre sous forme normale)} + résolution
		\item \emph{Théorème} : correction et complétude \textblue{Les autres logiques?}	
		\item Taille des arbres et certificats.
	\end{itemize}
	
	\section{Équivalence de formules}
	\textblue{Afin de trouver des moyens de résoudre le problème de satisfiabilité, on peut chercher à mettre nos formules sous différentes forme qui sont équivalentes : elles expriment les mêmes contraintes. Pour cela nous avons besoins de la notions d'équivalence et de formes normales.}
	
	\subsection{Équivalence sémantique et équisatisfiabilité \cite[p.106]{Duparc}}
	\textblue{Comparer deux formules c'est connaître les modèles qui les discrimine (satisfait une des formule mais pas l'autre). Lorsque nous ne pouvons pas les différentier, nous parlons d'équivalence sémantique (c'est l'équivalence que l'on souhaite).}
	\begin{itemize}
		\item \emph{Définition}: équivalence sémantique + \emph{Exemples}: loi de Morgan
		\item \emph{Proposition}: caractérisation avec la validité
		\item \emph{Conséquence}: Complexité du problème \textsc{VALIDITE}
		\item \emph{Définition}: équisatisfiabilité \textblue{(l'équivalence sémantique est trop contraignante mais pour la satisfiabilité, celle-ci suffit)}
		\item \emph{Proposition}: équivalent implique équisatisfiable.
	\end{itemize}
	
	\subsection{Systèmes de connecteurs}
	\textblue{L'ensemble des connecteurs logiques, appelé système de connecteurs, que nous choisissons pour écrire nos formules peuvent exprimer plus ou moins de choses. On en veut le moins de symbole possibles pour décrire notre logique.}
	\begin{itemize}
		\item \emph{Définition}: systèmes complets et minimaux + \emph{Exemples}
		\item \emph{Proposition}: $\{\neg, \vee\}$, $\{\neg, \wedge\}$, $\{\textsf{nand}\}$ sont des systèmes complets minimaux.
		\item \emph{Remarque}: $\{\neg, \wedge, \vee\}$ est un système complet mais pas minimal.
	\end{itemize}
	
	\subsection{Formes normales conjonctives ou disjonctives}
	\textblue{Les formes normales sont une manière de représenté une formule par une autre formule dont les connecteurs logiques sont restreints et ordonnés selon un certain ordre. On présente leur mise en forme, leurs points forts et leurs limites}
	
	\paragraph{Forme normale négative}\textblue{Elle est à l'origine de toutes les autres}
	\begin{itemize}
		\item \emph{Définition}: Forme normale négative (par sa grammaire)
		\item \emph{Proposition}: Transformation en une forme normale négative
	\end{itemize}
	
	\paragraph{Forme normale conjonctive}
	\begin{itemize}
		\item \emph{Proposition}: Existence d'une formule CNF équivalente + \emph{Remarque}: transformation exponentielle ($\varphi_n = (a_1 \wedge b_1) \vee \dots \vee (a_n \wedge b_n)$)
		\emph{Remarque}: C'est une forme normale négative
		\item \emph{Proposition}: Existence d'une formule CNF équisatisfiable de taille linéaire (Transformation de Tseitin) \textred{DEV}
		\item \emph{Corollaire}: Le problème \textsc{CNF-SAT} est $NP$-complet
		\item \emph{Remarque}: Le problème \textsc{CNF-VALIDE} est dans $P$.
	\end{itemize}
	
	\paragraph{Forme normale disjonctive} \textblue{Les formules conjonctives (et uniquement conjonctives) permettent de représenter un modèle.}
	\begin{itemize}
		\item \emph{Définitions}: forme normale disjonctive DNF
		\item \emph{Proposition}: Existence d'une formule DNF équivalente
		\item \emph{Théorème}: Le problème \textsc{DNF-SAT} est dans $P$ 
		\item \emph{Conséquence} : Par Cook et $P \neq NP$, transformation exponentielle
		\item \emph{Exemple}: $\varphi_n = (a_1 \vee b_1) \wedge \dots \wedge (a_n \vee b_n)$
		\item \emph{Remarque}: Le problème \textsc{DNF-VALIDE} est dans $NP$-complet
	\end{itemize}	
	
	
	\section{Résoudre la satisfiabilité en pratique}
	\textblue{On a vu que savoir si une formule est satisfiable est difficile. Cependant, en pratique on est souvent amené à résoudre cette question : on pose des contraintes (emplois du temps) ou issue de problème NP-complet. On aimerai alors un moyen de résoudre la satisfiabilité de manière efficace.}
	
	\subsection{Considérer des fragments de la logique}
	\textblue{Limiter la logique (si le problème le permet) peut nous donner des algorithmes qui résolvent la satisfiabilité en temps linaire.}
	\begin{itemize}
		\item \emph{Problème}: 3SAT -> NP-complet \textblue{(pas assez réduit)}
		\item \emph{Problème}: 2SAT ->  NL-dur (donc dans P) \textred{DEV} \textblue{Attention expressivité n'est pas la même} 
		\item \emph{Application}: problème d'ordonnancement multiprocesseurs où les processeurs n'ont que deux créneaux pour les tâches 
		\item \emph{Problème}: Horn -> P
		\item \emph{Application}~\cite{Stern}: Prolog avec les clauses de Horn
		\item \emph{Application}: Analyse syntaxique : calcul de premier, LL(1), problèmes sur grammaires algébriques
	\end{itemize}
	
	\subsection{Essayer de résoudre astucieusement}
	\textblue{Si on provient d'un problème NP-complet, on peut mettre la formule sous forme CNF et résoudre à l'aide d'un algorithme dont le pire cas s'exécute en temps exponentiel mais dont on espère que celui-ci soit meilleur dans la pratique.}
	\begin{itemize}
		\item DPLL / CDLL 
		\item \emph{Application}: SAT-solver \textblue{(Un des moyen de contrer la NP-complétude)}
	\end{itemize}
	
	\section*{Quelques notions importantes}
	
	\subsection*{Équivalence sémantique et équisatisfiabilité}
	\input{./../Notions/LogiqueProp_equivalence.tex}
	
	\subsection*{Les formes normales}
	\input{./../Notions/LgiqueProp_formesNormales.tex}
	
	\subsection*{Systèmes de connecteurs}
	\input{./../Notions/LogiqueProp_systConnecteur.tex}
	
	\subsection*{Problèmes de décisions en logique}
	\input{./../Notions/Logique_pbDecisions.tex}
	
	\bibliographystyle{plain}
	\bibliography{./../../Livre}

\end{document}