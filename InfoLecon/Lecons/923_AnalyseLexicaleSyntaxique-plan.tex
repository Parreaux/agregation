\chapter*{Leçon 923: Analyse lexicale et analyse syntaxique. Applications.}
\addcontentsline{toc}{chapter}{Leçon 923: Analyse lexicale et analyse syntaxique. Applications.}
	
\titlebox{blue}{\textblue{Références pour la leçon}}{
	\textblue{\cite{Carton}} Carton, \emph{Langages formels, calculabilité et complexité}
	
	\textblue{\cite{LegendreSchwarzentruber}} Legendre et Schwarzentruber, \emph{Compilation: Analyse lexicale et syntaxique du texte à sa structure en informatique.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.5\textwidth}
		$\mathcal{L}(G_{post})$ est $LL(1)$
	\end{minipage} \hfill
	\begin{minipage}{.45\textwidth}
		Construction des premiers
	\end{minipage} 
}
	
\section*{Motivation}

\subsection*{Défense}

La compilation permet de transformer un programme écrit dans un langage source en un programme sémantiquement équivalent écrit dans un langage cible. Généralement, on compile un langage de programmation comme C vers l'assembleur (le langage machine). Cependant, ce n'est pas la seule compilation que nous pouvons effectuer: on transforme du code latex en fichier en format pdf, ou en code html. De plus, pour compiler un langage de programmation comme OCaml ou Python, on utilise des langages intermédiaire comme C.

L'analyse lexicale et l'analyse syntaxique sont les premières étapes de la compilations. Dans un compilateur actuel, elles sont réalisées en étroite collaboration: l'analyse lexicale donnant à l'analyse syntaxique les mots dont elle a besoin pour continuer. Elles permettent de transformer un texte en un arbre de syntaxe abstraite qui sera la structure sur laquelle nous pourrons continuer la compilation. Cet arbre nous permet ensuite de réaliser une analyse sémantique (vérification de type, levée de certaines exceptions, ...) avant de produire un code intermédiaire au langage que nous souhaitons atteindre. Sur ce code intermédiaire, nous effectuons des opérations d'optimisation qui vise à rendre l'exécution plus rapide que nous l'avons écrite. On finit alors par traduire le bout qui manque.

Ces deux analyses reposent sur des outils théoriques simples et dont l'expressivité nous permet d'obtenir des calcul efficaces: les expressions rationnelles et les grammaires algébriques. En étudiant ces deux étapes de la compilation, on remarque que les automates finis sont pratiques pour couper un texte en mots mais qu'ils se révèle insuffisant pour ordonnancer les éléments en fonction de leurs opérandes. Pour cela (et afin de construire l'arbre de syntaxe abstraite), nous sommes obligé d'utiliser la puissance d'expression des grammaires algébriques.

\subsection*{Ce qu'en dit le jury}

Cette leçon ne doit pas être confondue avec la 909, qui s’intéresse aux seuls langages rationnels, ni avec la 907, sur l’algorithmique du texte.

Si les notions d’automates finis et de langages rationnels et de grammaires algébriques sont au cœur de cette leçon, l’accent doit être mis sur leur utilisation comme outils pour les analyses lexicale et syntaxique. Il s’agit donc d’insister sur la différence entre langages rationnels et algébriques, sans perdre de vue l’aspect applicatif : on pensera bien sûr à la compilation. Le programme permet également des développements pour cette leçon avec une ouverture sur des aspects élémentaires d’analyse sémantique.

\section*{Métaplan}

\begin{footnotesize}
	\begin{itemize}[label=$\rightsquigarrow$]
		\begin{minipage}{.4\textwidth}
			\item \emph{Motivation}: Chaîne de compilation
		\end{minipage} \hfill
		\begin{minipage}{.4\textwidth}
			\item \emph{Motivation}: Importance de la compilation
		\end{minipage}
	\end{itemize}
\end{footnotesize}

\begin{description}
	\item[I.] \textbf{Analyse lexicale \cite{LegendreSchwarzentruber}}
	\begin{small}
		\textblue{On commence par la première étape de la compilation: l'analyse lexicale qui s'avère être la plus facile.}
	\end{small}
	\begin{footnotesize}
		\begin{itemize}[label=$\rightsquigarrow$]
			\item \emph{Problème}: Transformer une suite de lettre en lexème
			\item \emph{Méthode}: Pattern-matching sur les expressions régulières
			\item \emph{Valeur ajoutée}: Erreur lexicale, filtrer les programmes, décoration des lexèmes produits
		\end{itemize}
	\end{footnotesize}
	\begin{description}
		\item[A.] \emph{Expressions rationnelles et automates finis \cite[p.38]{Carton}}
		\begin{small}
			\textblue{On rappelle succinctement ces notions et leur équivalence. Celle-ci donnent les outils efficaces au cœur de l'analyse lexicale: les automates finis reconnaissants les expressions régulières caractérisant les mots autorisé dans le langage.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Expression régulière
					\item \emph{Théorème}: Théorème de Kleene
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Automate fini
					\item \emph{Preuve}: Construction de Thomson 
				\end{minipage}
				\item \emph{Exemple}: Automate des motifs
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Analyseur lexical}
		\begin{small}
			\textblue{On décrit maintenant les différentes méthodes d'analyse lexicale ainsi que leur complexité.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \emph{Méthode}: Via les automates finis munis d'une priorité
				
				\begin{minipage}{.4\textwidth}
					\item \emph{Remarque}: Détection d'erreurs
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Proposition}: Complexité au pire cas: $O(n^2)$ 
				\end{minipage}
				\item \emph{Remarque}: Dans le cas d'un langage de programmation, on est généralement en $O(n)$.
				\item \emph{Méthode}: Via la programmation dynamique
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[II.] \textbf{Analyse syntaxique \cite{LegendreSchwarzentruber}}
	\begin{small}
		\textblue{L'analyse syntaxique est une étape plus compliquée à mettre en place qui demande l'utilisation d'outils plus conséquents: les grammaires algébriques.}
	\end{small}
	\begin{footnotesize}
		\begin{itemize}[label=$\rightsquigarrow$]
			\item \emph{Problème}: Transformer une suite de lexème en arbre de syntaxe abstraite
			
			\begin{minipage}{.46\textwidth}
				\item \emph{Remarque}: Problème qui est plus difficile
			\end{minipage} \hfill
			\begin{minipage}{.38\textwidth}
				\item \emph{Valeur ajoutée}: Erreur syntaxique
			\end{minipage}
			\item \emph{Méthode}: ascendante ou descendante sur les grammaires
		\end{itemize}
	\end{footnotesize}
	\begin{description}
		\item[A.] \emph{Grammaire algébrique \cite{Carton}}
		\begin{small}
			\textblue{On rappelle succinctement les notions autours des grammaires algébriques. Ces notions vont nous être utile pour réaliser l'analyse syntaxique à partir de la grammaire de notre langage source.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.34\textwidth}
					\item \emph{Définition}: Grammaire algébrique
					\item \emph{Définition}: Arbre de dérivation 
					\item \emph{Définition}: Ambiguïté
					\item \emph{Définition}: Langage engendré
				\end{minipage} \hfill
				\begin{minipage}{.46\textwidth}
					\item \emph{Définition}: Dérivation (gauche / droite)
					\item \emph{Remarque}: Dérivation vs arbre de dérivation
					\item \emph{Remarque}: Impact lors l'analyse syntaxique
					\item \emph{Remarque}: Les rationnels sont algébriques.
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Analyse générique}
		\begin{small}
			\textblue{Regardons les méthodes génériques: elles peuvent être utilisées pour toutes les grammaires algébriques.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \emph{Remarque}: La méthode "naïve" qui utilise du backtracking est exponentielle 
				\item \emph{Définition}: Grammaire de Chomsky
				\item \emph{Théorème}: Toute grammaire peut être mise sous forme de Chomsky
				
				\begin{minipage}{.4\textwidth}
					\item \emph{Remarque}: Transformation peu coûteuse
					\item \emph{Algorithme}: CYK et sa complexité 
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Proposition}: Le problème du mot est dans P
					\item \emph{Application}: Analyse syntaxique
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Méthode descendante}
		\begin{small}
			\textblue{L'algorithme générique nous donne une analyse cubique. En réfléchissant sur la structure des grammaires des langages de programmation (qui sont pour la plus part agréable), nous pouvons grâce à des méthodes gloutonnes obtenir une analyse linéaire. Une première analyse est par méthode descendante dans la grammaire: on part de l'axiome pour obtenir le mot.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Principe}: 
					\item \textred{\emph{Exemple}: $\mathcal{L}\left(G_{post}\right)$ est $LL(1)$.}
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Grammaire LL(1)
					\item \textred{\emph{Proposition}: Caractérisation du premier}
				\end{minipage}
				
				\item \emph{Remarque}: Expressivité: langages de programmations concernés
				
				\begin{minipage}{.4\textwidth}
					\item \emph{Algorithme}
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Limite}: Langages non $LL(1)$
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[D.] \emph{Méthode ascendante}
		\begin{small}
			\textblue{Une première analyse est par méthode ascendante: on part des terminaux pour obtenir l'axiome.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Principe}: 
					\item \emph{Définition}: Grammaire LR(1)
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Grammaire LR(0)
					\item \emph{Algorithme}:
				\end{minipage}
				\item \emph{Remarque}: Expressivité: langages de programmations concernés
				\item \emph{Limite}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\begin{footnotesize}
		\emph{Conclusion}: Récapitulation des différents types de grammaires et de leurs méthodes d'analyse.
	\end{footnotesize}
	\item[Ouverture] 
	\begin{footnotesize}
		Analyse sémantique:  $\lambda$-calcul simplement typé et correspondance de Curry-Howard.
	\end{footnotesize}
\end{description}
