\chapter*{Leçon 926 : Analyse des algorithmes : Complexité. Exemples.}
\addcontentsline{toc}{chapter}{Leçon 926 : Analyse des algorithmes : Complexité. Exemples.}

\titlebox{blue}{\textblue{Références pour la leçon}}{
	\textblue{\cite{Beauquier-Berstel-Chretienne}} Beauquier, Berstel et Chretienne, \emph{Éléments d'algorithmique.}
	
	\textblue{\cite{Carton}} Carton, \emph{Langages formels, calculabilité et complexité.}
	
	\textblue{\cite{Cormen}} Cormen, \emph{Algorithmique.}
	
	\textblue{\cite{Froidevaux-Gaudel-Soria}} Froidevaux, Gaudel et Soria, \emph{Types de données et algorithmes.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.5\textwidth}
		Étude d'Union-Find pour la complexité
	\end{minipage} \hfill
	\begin{minipage}{.45\textwidth}
		Algorithme de Dijkstra
	\end{minipage} 
}

\section*{Motivation}

\subsection*{Défense}

Lors de la conception, puis de l'étude d'un algorithme, deux notions sont extrêmement importantes:
\begin{itemize}
	\item la correction de l'algorithme : fait-il ce que l'on souhaite?
	\item l'efficacité de l'algorithme: à quelle vitesse s'exécute-t-il?; est-il optimal?
\end{itemize}
La complexité (temporelle ou spatiale) intervient alors pour comparer deux algorithmes corrects répondant à la même question.

\subsection*{Ce qu'en dit le jury}

Il s’agit ici d’une leçon d’exemples. Le candidat prendra soin de proposer l’analyse d’algorithmes portant sur des domaines variés, avec des méthodes d’analyse également variées : approche combinatoire ou
probabiliste, analyse en moyenne ou dans le pire cas.

Si la complexité en temps est centrale dans la leçon, la complexité en espace ne doit pas être négligée. La notion de complexité amortie a également toute sa place dans cette leçon, sur un exemple bien choisi, comme union find (ce n’est qu’un exemple).

\section*{Métaplan}

\begin{description}
	\item[I.] \textbf{Quantifier la complexité}
	\begin{small}
		\textblue{Définir la complexité d'un algorithme n'est pas facile. Intuitivement la complexité d'un algorithme est un indicateur de la difficulté pour résoudre le problème traité par l'algorithme. Mais cette vue de l'esprits n'est pas simple à quantifier. Nous allons alors définir la complexité comme une fonction qui en fonction des entrées sur notre programme donnera la consommation d'une certaine ressource.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Qu'est-ce que la complexité?}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \emph{Définition}: Données d'entrée d'un algorithme 
				\item \emph{Définition}: Taille d'une entrée: $f: \{entree\} \to \N^k$
				\item \emph{Définition}: Complexité $f: \{\text{entrée}\} \to \{\text{ressource}\}$
				\item \emph{Remarque}: En pratique: unités de bases 
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Mesurer la complexité \cite[p.40]{Cormen}}
		\begin{small}
			\textblue{On ne peut pas toujours calculer la complexité exacte (avec les constantes) car généralement, elle dépend de l'implémentation que nous utilisons. Pour cette même raison le calcul de la complexité exacte peut s'avérer inutile.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \emph{Définition}: Données d'entrée d'un algorithme 
				\item \emph{Définition}: Taille d'une entrée: $f: \{entree\} \to \N^k$
				
				\begin{minipage}{.4\textwidth}
					\item \emph{Exemples}: Tri bulle $ O(n^2)$; B-arbre $O(\log n)$
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Proposition}: L'ordre de croissance 
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Différentes approches de la complexité \cite[p.19]{Froidevaux-Gaudel-Soria}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Complexité pire cas
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Complexité meilleur cas 
				\end{minipage}
				\item \emph{Définition}: Complexité en moyenne \textblue{(probabilité)}
				\item \emph{Remarque}: Distribution uniforme : simplification de l'écriture mais pas toujours vrai \textbrown{Attention}
				\item \emph{Définition}: Complexité constante
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[II.] \textbf{Techniques de calcul de la complexité}
	\begin{small}
		\textblue{Maintenant que nous avons donné un cadre à notre théorie de la complexité, nous souhaitons calculer les complexités d'algorithmes usuels. Pour cela, nous allons présenter quelques techniques de calcul élémentaires.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Le calcul direct}
		\begin{small}
			\textblue{On peut parfois calculer directement la complexité en dénombrant les opérations que l'on doit calculer. Efficace dans le cas d'algorithmes itératifs}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \emph{Exemple}: Recherche de maximum dans un tableau 
				\item \emph{Exemple}~\cite[p.198]{Carton}: CYK, complexité: $O(n^3)$ 
				\item \emph{Exemple}~\cite[p.389]{Beauquier-Berstel-Chretienne}: Marche de Jarvis, complexité: $O(hn)$ 
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{La résolution de récurrence \cite[p.20]{Beauquier-Berstel-Chretienne}}
		\begin{small}
			\textblue{On peut parfois exprimer la complexité pour une donnée de taille $n$ par rapport à une donnée de taille strictement inférieure. Résoudre l'équation ainsi obtenue nous donne la complexité. Efficace dans le cas d'algorithmes récursifs.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.5\textwidth}
					\item \emph{Proposition}: Suite récurrente linéaire d'ordre 1  
					\item \emph{Proposition}: Suite récurrente linéaire d'ordre 2 
					\item \emph{Proposition}: Master theorem 
					\item \emph{Remarque}: Ne capture pas toutes les équations 
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Exemples} Factorielle et Euclide
					\item \emph{Exemple} Fibonacci
					\item \emph{Exemples} Tri fusion et Strassen
					\item \emph{Contre-exemple}: Tri rapide
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Le calcul de la complexité moyenne par l'espérance}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.38\textwidth}
					\item \emph{Remarque}: Distribution uniforme
				\end{minipage} \hfill
				\begin{minipage}{.42\textwidth}
					\item \emph{Exemple}: Tri rapide randomisé
				\end{minipage}
				\item \emph{Remarque}: Hypothèse d'une distribution uniforme : introduction d'erreurs.
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[III.] \textbf{Raffinement de l'étude de la complexité : la complexité amortie}
	\begin{small}
		\textbrown{La complexité amortie n'est pas une complexité moyenne!} \textblue{La complexité amortie est une amélioration de l'analyse dans le pire cas s'adaptant (ou calculant) aux besoins de performance des structures de données.}
	\end{small}
	\begin{footnotesize}
		\begin{itemize}[label=$\rightsquigarrow$]
			\begin{minipage}{.4\textwidth}
				\item \emph{Définition}: la complexité amortie 
			\end{minipage} \hfill
			\begin{minipage}{.4\textwidth}
				\item \emph{Exemple}~\cite[p.428]{Cormen}: table dynamique 
			\end{minipage}
		\end{itemize}
	\end{footnotesize}
	\begin{description}
		\item[A.] \emph{Méthode de l'agrégat}
		\begin{small}
			\textblue{Dans cette méthode, le coût amortie est le même pour toutes les opérations de la séquence même si elle contient différentes opérations}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Principe}~\cite[p.418]{Cormen}: On calcul la complexité dans le pire cas d'une séquence qu'on divise par son nombre d'opérations.
				\end{minipage} \hfill
				\begin{minipage}{.38\textwidth}
					\item \emph{Exemple} : Table dynamique
					\item \textred{\emph{Exemple} : Union find + Kruskal}
					\item \emph{Exemple} : Balayage de Gram
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Méthode comptable}
		\begin{small}
			\textblue{Cette méthode se différencie de la précédente en laissant la possibilité que toutes les opérations ait un coup amortie différent.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.36\textwidth}
					\item \emph{Principe}: On attribut à chaque opération un crédit et une dépense
				\end{minipage} \hfill
				\begin{minipage}{.42\textwidth}
					\item \emph{Exemple} : table dynamique
					\item \emph{Exemple} : KMP
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Méthode du potentiel}
		\begin{small}
			\textblue{Cette méthode a été popularisé lors de la preuve de la complexité amortie de la structure de données Union Find, implémentée à l'aide d'une forêt et des heuristiques qui vont bien. Elle est moins facile que les autres à mettre en place.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \emph{Principe}~\cite[p.424]{Cormen}: Au lieu d'assigner des crédits à des opérations, on va associer une énergie potentielle $\varphi$ à la structure elle-même.
				 
				\begin{minipage}{.4\textwidth}
					\item \emph{Exemple} : Table dynamique
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Exemple} : Union Find
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[IV.] \textbf{Amélioration de l'étude de la complexité}
	\begin{small}
		\textblue{Plusieurs pistes existe afin d'améliorer la complexité d'un algorithme : utiliser une structure de données plus adaptée, utiliser un peu plus de mémoire ou au contraire se souvenir de moins de choses, ... Cependant, quelques fois nous sommes capable de mettre une borne minimale sur la complexité d'une famille de problème : quand nous avons atteint cette borne on sait que nos algorithmes sont optimaux. }
	\end{small}
	\begin{description}
		\item[A.] \emph{Borne minimale de la complexité sur une classe de problème}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \emph{Proposition} : Borne minimal d'un algorithme de tri
				\item \emph{Exemple} : Tri par insertion $O(n^2) \geq O(n \log n)$						
				\item \emph{Exemple} : Tri fusion $O(n \log n)$ atteint cette borne
				\item \emph{Remarque}: Optimiser les constantes 
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Utilisation de structures de données adaptées}
		\begin{small}
			\textblue{Une piste pour améliorer un algorithme : utiliser une bonne structure de données}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.26\textwidth}
					\item \emph{Exemple} : Tri par tas 
				\end{minipage} \hfill
				\begin{minipage}{.6\textwidth}
					\item \emph{Remarque} : Dépend de la manière dont on implémente la structure
				\end{minipage}
				
				\begin{minipage}{.4\textwidth}
					\item \emph{Exemple} : Représentation d'un graphe 
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Remarque}: Utilisation de ces représentation
				\end{minipage}
				\item \emph{Exemple} : Prim (liste d'adjacence) + Floyd Warshall (matrice d'adjacence)
				
				\begin{minipage}{.5\textwidth}
					\item \emph{Remarque}: On peut changer de structure de données
				\end{minipage} \hfill
				\begin{minipage}{.3\textwidth}
					\item \textred{\emph{Exemple} : Dijstra}
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Compromis espace/temps}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.38\textwidth}
					\item \emph{Principe} : Compromis espace/temps
					\item \emph{Principe}~\cite[p.338]{Cormen} : La mémoïsation 
				\end{minipage} \hfill
				\begin{minipage}{.42\textwidth}
					\item \emph{exemple} : Fibonacci
					\item \emph{Exemple} : Découpage de barre
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[Ouverture] 
	\begin{footnotesize}
		Évaluer la complexité d'un algorithme (hors implémentation) n'est pas une tâche facile. Souvent plusieurs astuces sont nécessaire pour trouver la meilleure borne sur notre complexité possible. Quelque fois, un approximation grossière de notre complexité (évaluation de la complexité pour le tri par tas) suffit.
	\end{footnotesize}
\end{description}