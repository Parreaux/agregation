\documentclass[a4paper,11pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais,english]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}
\usepackage{subcaption}

\usepackage{multirow}

\usepackage{algorithm}
\usepackage{algpseudocode}%

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{listings}


\parskip=0\baselineskip %0.5 d'habitude
\usetikzlibrary{automata,positioning,shadows}
\usetikzlibrary{calc, shapes, backgrounds,arrows}

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Leçon 928 : Problèmes NP-complets : exemples et réductions.}
	
	\date{2018 - 2019}
	\author{Julie Parreaux}
	
	\maketitle
	
	\titlebox{blue}{Références pour la leçon}{
		\textblue{\cite{Carton}} Carton, \emph{Langages formels, calculabilité et complexité.}
		
		\textblue{\cite{Cormen}} Cormen, \emph{Algorithmique.}
		
		\textblue{\cite{FloydBiegel}} Floyd et Biegel, \emph{Le langage des machines.}
		
		\textblue{\cite{KleinbergTardos}} Kleinberg et Tardos, \emph{Algorithms design.}
		
		\textblue{\cite{LegendreSchwarzentruber}} Legendre et Schwarzentruber, \emph{Compilation: Analyse lexicale et syntaxique du texte à sa structure en informatique.}
		
		\textblue{\cite{GareyJohnson}} Garey et Johnson, \emph{Computers and Intractability: A Guide to the Theory of the NP-Completness.}
		
		\textblue{\cite{Sisper}} Sipser, \emph{Introduction to the Theory of Computation.}
	}
	
	\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
		\begin{minipage}{.45\textwidth}
			Le problème PSA est NP-complet
		\end{minipage} \hfill
		\begin{minipage}{.5\textwidth}
			Approximation ou non du problème TSP
		\end{minipage} 
	}
	
	\renewcommand{\contentsname}{Plan de la leçon}
	\tableofcontents

	\section*{Motivation}
	
	\subsection*{Défense}
	
	Thèse de Cobham--Edmonds
	
	\subsection*{Ce qu'en dit le jury}
	
	L’objectif ne doit pas être de dresser un catalogue le plus exhaustif possible ; en revanche, pour chaque exemple, il est attendu que le candidat puisse au moins expliquer clairement le problème considéré, et indiquer de quel autre problème une réduction permet de prouver sa NP-complétude.
	
	Les exemples de réduction polynomiale seront autant que possible choisis dans des domaines variés : graphes, arithmétique, logique, etc. Si les dessins sont les bienvenus lors du développement, le jury attend une définition claire et concise de la fonction associant, à toute instance du premier problème, une instance du second ainsi que la preuve rigoureuse que cette fonction permet la réduction choisie et que les candidats sachent préciser comment sont représentées les données.
	
	Un exemple de problème NP-complet dans sa généralité qui devient P si on contraint davantage les hypothèses pourra être présenté, ou encore un algorithme P approximant un problème NP-complet.
	
	\section{Des problèmes NP-complets}
	
	\subsection{Les classes P et NP}
	\begin{itemize}
		\item \emph{Définition}: Classe de problème P + exemple
		\item \emph{Proposition}: Propriété de la classe P (stabilité)
		\item \emph{Définition}: Classe de problème NP + exemple
		\item \emph{Définition}: Vérifieur + caractérisation 
	\end{itemize}
	
	\subsection{La NP-complétude}
	\begin{itemize}
		\item \emph{Définition}: Réduction en temps polynomial + notation
		\item \emph{Proposition}: Conséquences de la réduction 
		\item \emph{Définition}: Problème NP-complet
		\item \emph{Remarque}: Caractérisation de P = NP
		\item \emph{Théorème}: Cook
		\item \emph{Exemple}: Vertex cover \textblue{éclairage}
		\item \emph{Exemple}: Clique \textblue{serveur web, communauté}
		\item \emph{Exemple}: independant set \textblue{activité compatible}
	\end{itemize}
	
	\subsection{Technique de preuve de la NP-complétude}
	\begin{itemize}
		\item Restriction d'un problème : 3SAT à SAT
		\item Remplacement locale : SAT à 3SAT
		\item Gadget : 3SAT à 3-Col
		\item Autre : Séparabilité d'un automate \textred{DEV}
		\item Utilisation des classes supérieures : Regexp non universelle
	\end{itemize}
	
	\section{la NP-complétude en pratique}
	\textblue{Quand on tombe sur un problème NP-complet, on n'a pas tout perdu...}
	
	\subsection{Restreindre des entrées}
	\textblue{\emph{Idée}: Perte d'expressivité du problème}
	\begin{itemize}
		\item Réduction de la taille de l'entrée : 2SAT / 2Col
		\item Passage aux réels : problème du sac à dos réel
		\item Logique de Hoare + application à l'analyse syntaxique et prolog
	\end{itemize}
	
	\subsection{Approximer les solutions}
	\textblue{\emph{Idée}: Perte de précision}
	\begin{itemize}
		\item \emph{Définition}: Problème d'optimisation
		\item \emph{Remarque}: Lien avec les problèmes de décision
		\item \emph{Définition}: Approximation + Schéma d'approximation
		\item Problème TSP \textred{DEV} (glouton)
		\item Autre?
	\end{itemize}
	
	\subsection{Explorer l'ensemble des solutions}
	\textblue{\emph{Idée}: Perte de temps ;)}
	\begin{itemize}
		\item Backtracking : DPLL (autre?)
		\item Branch and Bound 
		\item Programmation linéaire
	\end{itemize}
	
	\section*{Quelques notions importantes}
	
	\subsection*{Technique de preuve : la réduction}
	\input{./../Notions/Indecidabilite_reduction.tex}
	
	\subsection*{Le théorème de Cook}
	\input{./../Notions/NPC_Cook.tex}
	
	\bibliographystyle{plain}
	\bibliography{./../../Livre}

	
\end{document}