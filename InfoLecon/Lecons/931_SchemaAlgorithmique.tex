\documentclass[a4paper,11pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais,english]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}
\usepackage{subcaption}

\usepackage{algorithm}
\usepackage{algpseudocode}%

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{listings}


\parskip=0\baselineskip %0.5 d'habitude
\usetikzlibrary{automata,positioning,shadows}
\usetikzlibrary{calc, shapes, backgrounds,arrows}

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\newtheorem*{definition}{Définition}
	\newtheorem*{notation}{Notation}
	\newtheorem*{theorem}{Théorème}
	\newtheorem*{lemme}{Lemme}
	
	\title{Leçon 931 : Schéma algorithmiques. Exemples et application.}
	
	\date{2018 - 2019}
	\author{Julie Parreaux}
	
	\maketitle
	
	\titlebox{blue}{Références pour la leçon}{
		\textblue{\cite{Beauquier-Berstel-Chretienne}} Beauquier, Berstel et Chretienne, \emph{Éléments d'algorithmique.}
		
		\textblue{\cite{Cormen}} Cormen, \emph{Algorithmique.}
		
		\textblue{\cite{KleinbergTardos}} Kleinberg et Tardos, \emph{Algorithms design.}
		
		\textblue{\cite{MoretShapiro}} Moret et Shapiro, \emph{Algorithms from P to NP.}
		
		\textblue{\cite{Sisper}} Sipser, \emph{Introduction to the Theory of Computation.}
	}
	
	\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
		\begin{minipage}{.5\textwidth}
			Étude de l'alignement optimal de deux mots
		\end{minipage} \hfill
		\begin{minipage}{.45\textwidth}
			Algorithme de Dijkstra
		\end{minipage} 
	}
	
	\renewcommand{\contentsname}{Plan de la leçon}
	\tableofcontents

	\section*{Motivation}
	
	\subsection*{Défense}
	
	La méthode naïve, le brute force n'est pas toujours efficace. Il existe plusieurs types d'algorithmes permettant de résoudre un problème. Selon la nature du problème (optimisation, récursif, ...) ces approches peuvent être plus ou moins efficaces. Nous allons en étudier quelques unes ici, et plus particulièrement des paradigmes de partitionnement et d'exploration.
	
	\subsection*{Ce qu'en dit le jury}
	
	Cette leçon permet au candidat de présenter différents schémas algorithmiques, en particulier «diviser pour régner », programmation dynamique et approche gloutonne. Le candidat pourra choisir de se concentrer plus particulièrement sur un ou deux de ces paradigmes. Le jury attend du candidat qu’il illustre sa leçon par des exemples variés, touchant des domaines différents et qu’il puisse discuter les intérêts et limites respectifs des méthodes. Le jury ne manquera pas d’interroger plus particulièrement le
	candidat sur la question de la correction des algorithmes proposés et sur la question de leur complexité, en temps comme en espace.
	
	\section*{Introduction}
	\addcontentsline{toc}{section}{Introduction}
	
	\begin{itemize}
		\item Brute force : exemple et contre-exemple
	\end{itemize}
	
	\section{Algorithmes de partitionnement}
	\textblue{Une première approche de l'algorithmique est de partager le problème en sous problème dont ceux-ci sont à priori plus simple à calculer.}
	
	\subsection{Diviser pour régner \cite[p.59]{Cormen}}
	\textblue{Diviser pour régner est plutôt un paradigme récursif. Le principe est de partager la résolution du problème en le divisant puis en recombinant les sous-instance ainsi résolu.}
	\begin{itemize}
		\item Paradigme + exemple tri fusion / enveloppe convexe
		\item Master théorème + applications + contre-exemple : tri rapide  \cite{Beauquier-Berstel-Chretienne}
		\item Application théorique : théorème de Savitch
		\item Application approximation de problème NP-complet TSPE \cite[p.480]{MoretShapiro}
		\item Limites : deux infinité: infinité de l'espace de la solution: pile d'appel (problème d'optimisation NP-complet) et infinité structurelle: recoupement (Fractale, Fibonacci)
	\end{itemize}
	
	\subsection{Programmation dynamique \cite[p.333]{Cormen}}
	\textblue{Cette approche vient rectifier les lacunes du à l'infinité structurelle du paradigme diviser pour régner en stockant dans des tables les résultats intermédiaires. Il s'applique généralement aux problèmes d'optimisation.}
	\begin{itemize}
		\item Quand doit-on l'utiliser?
		\item Paradigme (la recette de cuisine) + exemples (distance d'édition \textred{DEV}, PLSC, CYK)
		\item Memoisation + exemple (découpe de barre)
		\item Application à l'approximation de problème NP-complet Sac à dos
	\end{itemize}
	
	\subsection{Algorithmes glouton : une heuristique}
	\textblue{Cette approche permet d'optimiser des problèmes en considérant une approche locale: elle vient rectifier les limite dû à l'infinité de l'espace des solutions. C'est une première heuristique simple qui peut parfois donner de bons résultats. Elle vient rectifier les lacunes de l'approche diviser pour régner de du à l'infinité de l'espace des solutions.}
	\begin{itemize}
		\item Paradigme
		\item Exemple optimal : Dijkstra \textred{DEV} + Prim Kruskal \textblue{(ces algorithmes sont optimaux)}
		\item Application à la compilation: analyse syntaxique (LL(k), LR(k))
		\item Application à l'approximation de problème NP-complet (Set cover, TSP)
	\end{itemize}
	
	
	\section{Algorithmes d'exploration}
	\textblue{Lorsque l'ensemble des solutions devient vraiment très important: le partage choisi par les algorithmes de partitionnement ne sont pas nécessairement les meilleurs. On utilise alors des algorithmes d'explorations qui d'une certaine manière partitionne l'espace des solutions mais sont l'exécution se base sur l'exploration d'un arbre. Remarque: on a aussi des algorithmes d'exploration sur les graphes.}
	
	\subsection{Backtracking}
	\textblue{Cette approche de l'exploration est plus efficace dans le cadre d'un algorithme d'exploration sur un problème de décision (où on cherche à construire une instance). On a des améliorations de cette approche nous permettant de ne pas explorer des parties de l'arbre (backjumping avec CDCL).}
	\begin{itemize}
		\item Principe \textblue{On fait un parcours en profondeur de notre arbre des possibles.}
		\item Application à la logique: DPLL 
		\item Application à la compilation: analyse syntaxique pour toute grammaire \textblue{Attention, si on a CYK, ce n'est plus nécessaire}
	\end{itemize}
	
	\subsection{Branch and Bound}
	\textblue{Cette approche de l'exploration est plus efficace dans le cadre d'un algorithme d'exploration sur un problème d'optimisation. On se donne une fonction de coût, une heuristique facile à calculer, (qui majore ou minore celle du problème) qui nous permet de classer les solutions en fonction de leur affinité avec la solution.}
	\begin{itemize}
		\item Principe \textblue{On effectue un sorte de parcours en largeur.}
		\item Exemple ?
	\end{itemize}
	
	\textblue{Remarque: Dans les deux cas, on peut être amené à explorer l'ensemble de l'arbre qui nous sert de base.}
	
	\section*{Quelques notions importantes}
	
	\subsection*{Les principes de la programmation dynamique}
	\input{./../Notions/ProgrammationDynamique_Principe.tex}
	
	\subsection*{Les principes du diviser pour régner}
	\input{./../Notions/DiviserRegner_Principe.tex}
	
	\bibliographystyle{plain}
	\bibliography{./../../Livre}

\end{document}