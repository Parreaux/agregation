\documentclass[a4paper,11pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais,english]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}
\usepackage{subcaption}

\usepackage{algorithm}
\usepackage{algpseudocode}%

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{listings}


\parskip=0\baselineskip %0.5 d'habitude
\usetikzlibrary{automata,positioning,shadows}
\usetikzlibrary{calc, shapes, backgrounds,arrows}

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{cex}{Contre-exemple}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	
	\title{Leçon 923: Analyse lexicale et analyse syntaxique. Applications.}
	
	\date{2018 - 2019}
	\author{Julie Parreaux}
	
	\maketitle
	
	\titlebox{blue}{Références pour la leçon}{
		\textblue{\cite{Carton}} Carton, \emph{Langages formels, calculabilité et complexité.}
		
		\textblue{\cite{LegendreSchwarzentruber}} Legendre et Schwarzentruber, \emph{Compilation: Analyse lexicale et syntaxique du texte à sa structure en informatique.}
	}
	
	\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
		\begin{minipage}{.5\textwidth}
			$\mathcal{L}(G_{post})$ est $LL(1)$
		\end{minipage} \hfill
		\begin{minipage}{.45\textwidth}
			Construction des premiers
		\end{minipage} 
	}
	
	\renewcommand{\contentsname}{Plan de la leçon}
	\tableofcontents
	
	\section*{Motivation}
	
	\subsection*{Défense}
	
	La compilation permet de transformer un programme écrit dans un langage source en un programme sémantiquement équivalent écrit dans un langage cible. Généralement, on compile un langage de programmation comme C vers l'assembleur (le langage machine). Cependant, ce n'est pas la seule compilation que nous pouvons effectuer: on transforme du code latex en fichier en format pdf, ou en code html. De plus, pour compiler un langage de programmation comme OCaml ou Python, on utilise des langages intermédiaire comme C.
	
	L'analyse lexicale et l'analyse syntaxique sont les premières étapes de la compilations. Dans un compilateur actuel, elles sont réalisées en étroite collaboration: l'analyse lexicale donnant à l'analyse syntaxique les mots dont elle a besoin pour continuer. Elles permettent de transformer un texte en un arbre de syntaxe abstraite qui sera la structure sur laquelle nous pourrons continuer la compilation. Cet arbre nous permet ensuite de réaliser une analyse sémantique (vérification de type, levée de certaines exceptions, ...) avant de produire un code intermédiaire au langage que nous souhaitons atteindre. Sur ce code intermédiaire, nous effectuons des opérations d'optimisation qui vise à rendre l'exécution plus rapide que nous l'avons écrite. On finit alors par traduire le bout qui manque.
	
	Ces deux analyses reposent sur des outils théoriques simples et dont l'expressivité nous permet d'obtenir des calcul efficaces: les expressions rationnelles et les grammaires algébriques. En étudiant ces deux étapes de la compilation, on remarque que les automates finis sont pratiques pour couper un texte en mots mais qu'ils se révèle insuffisant pour ordonnancer les éléments en fonction de leurs opérandes. Pour cela (et afin de construire l'arbre de syntaxe abstraite), nous sommes obligé d'utiliser la puissance d'expression des grammaires algébriques.
	
	\subsection*{Ce qu'en dit le jury}
	
	Cette leçon ne doit pas être confondue avec la 909, qui s’intéresse aux seuls langages rationnels, ni avec la 907, sur l’algorithmique du texte.
	
	Si les notions d’automates finis et de langages rationnels et de grammaires algébriques sont au cœur de cette leçon, l’accent doit être mis sur leur utilisation comme outils pour les analyses lexicale et syntaxique. Il s’agit donc d’insister sur la différence entre langages rationnels et algébriques, sans perdre de vue l’aspect applicatif : on pensera bien sûr à la compilation. Le programme permet également des développements pour cette leçon avec une ouverture sur des aspects élémentaires d’analyse sémantique.

	\section*{Introduction}
	\addcontentsline{toc}{section}{Introduction}
	
	\emph{Motivation}: Chaîne de compilation + importance de la compilation
	
	\section{Analyse lexicale}
	
	\textblue{On commence par la première étape de la compilation: l'analyse lexicale qui s'avère être la plus facile.}
	
	\begin{itemize}
		\item \emph{Problème}: Transformer une suite de lettre en lexème
		\item \emph{Valeur ajoutée}:
		\begin{itemize}[label=$\bullet$]
			\item Erreur lexicale (motif interdit dans le langage)
			\item Filtrer les programmes sur les blancs et les commentaires (équivalence de programme)
			\item Décoration des lexèmes produit (construire les futurs messages d'erreurs)
		\end{itemize}
		\item \emph{Méthode}: Pattern-matching sur les expressions régulières
	\end{itemize}
	
	\subsection{Expressions rationnelles et automates finis \cite[p.38]{Carton}}
	\textblue{On rappelle succinctement ces notions et leur équivalence. Celle-ci donnent les outils efficaces au cœur de l'analyse lexicale: les automates finis reconnaissants les expressions régulières caractérisant les mots autorisé dans le langage.}
	\begin{itemize}
		\item \emph{Définition}: Expression régulière
		\item \emph{Définition}: Automate fini
		\item \emph{Théorème}: Théorème de Kleene
		\item \emph{Preuve}: Construction de Thomson \textblue{(des expressions régulières aux automates)}
		\item \emph{Exemple}: Automate des motifs
	\end{itemize}
	
	\subsection{Analyseur lexical}
	\textblue{On décrit maintenant les différentes méthodes d'analyse lexicale ainsi que leur complexité.}
	\begin{itemize}
		\item \emph{Méthode}: Via les automates finis munis d'une priorité
		\item \emph{Remarque}: Détection d'erreurs
		\item \emph{Proposition}: Complexité au pire cas: $O(n^2)$
		\item \emph{Remarque}: Dans le cas d'un langage de programmation, on est généralement en $O(n)$.
		\item \emph{Méthode}: Via la programmation dynamique 
	\end{itemize}
	
	\section{Analyse syntaxique}
	\textblue{L'analyse syntaxique est une étape plus compliquée à mettre en place qui demande l'utilisation d'outils plus conséquents: les grammaires algébriques.}
	
	\begin{itemize}
		\item \emph{Problème}: Transformer une suite de lexème en arbre de syntaxe abstraite
		\item \emph{Remarque}: Problème qui est plus difficile
		\item \emph{Valeur ajoutée}: Erreur syntaxique
		\item \emph{Méthode}: ascendante ou descendante sur les grammaires
	\end{itemize}
	
	
	\subsection{Grammaire algébrique}
	\textblue{On rappelle succinctement les notions autours des grammaires algébriques. Ces notions vont nous être utile pour réaliser l'analyse syntaxique à partir de la grammaire de notre langage source.}
	\begin{itemize}
		\item \emph{Définition}: Grammaire algébrique
		\item \emph{Définition}: Dérivation (gauche / droite)
		\item \emph{Définition}: Arbre de dérivation 
		\item \emph{Remarque}: Différence entre une dérivation et un arbre de dérivation
		\item \emph{Définition}: Ambiguïté
		\item \emph{Remarque}: Impact lors l'analyse syntaxique
		\item \emph{Définition}: Langage engendré
		\item \emph{Remarque}: Les rationnels sont algébriques.
	\end{itemize}
	
	\subsection{Analyse générique}
	\textblue{Regardons les méthodes génériques: elles peuvent être utilisées pour toutes les grammaires algébriques.}
	\begin{itemize}
		\item \emph{Remarque}: La méthode "naïve" qui utilise du backtracking est exponentielle \textblue{(oups)}
		\item \emph{Définition}: Grammaire de Chomsky
		\item \emph{Théorème}: Toute grammaire peut être mise sous forme de Chomsky
		\item \emph{Remarque}: De plus cette transformation n'est pas coûteuse
		\item \emph{Algorithme}: CYK et sa complexité \textblue{(programmation dynamique)}
		\item \emph{Proposition}: Le problème du mot est dans P
		\item \emph{Application}: Analyse syntaxique
	\end{itemize}
	
	\subsection{Méthode descendante}
	\textblue{L'algorithme générique nous donne une analyse cubique. En réfléchissant sur la structure des grammaires des langages de programmation (qui sont pour la plus part agréable), nous pouvons grâce à des méthodes gloutonnes obtenir une analyse linéaire. Une première analyse est par méthode descendante dans la grammaire: on part de l'axiome pour obtenir le mot.}
	\begin{itemize}
		\item \emph{Principe}: 
		\item \emph{Définition}: Grammaire LL(1)
		\item \emph{Proposition}: Caractérisation du premier \textred{DEV}
		\item \emph{Algorithme}
		\item \emph{Remarque}: Expressivité: langages de programmations concernés
		\item \emph{Limite}
	\end{itemize}
	
	\subsection{Méthode ascendante}
	\textblue{Une première analyse est par méthode ascendante: on part des terminaux pour obtenir l'axiome.}
	\begin{itemize}
		\item \emph{Principe}: 
		\item \emph{Définition}: Grammaire LR(0)
		\item \emph{Définition}: Grammaire LR(1)
		\item \emph{Algorithme}:
		\item \emph{Remarque}: Expressivité: langages de programmations concernés
		\item \emph{Limite}
	\end{itemize}
	
	\emph{Conclusion}: Récapitulation des différents types de grammaires et de leurs méthodes d'analyse.
	
	\section*{Ouverture}
	\addcontentsline{toc}{section}{Ouverture}
	Analyse sémantique:  $\lambda$-calcul simplement typé et correspondance de Curry-Howard.

	
	
	
	\bibliographystyle{plain}
	\bibliography{./../../Livre}

	
\end{document}