\chapter*{Leçon 903 : Exemples d'algorithmes de tri. Correction et complexité.}
\addcontentsline{toc}{chapter}{Leçon 903 : Exemples d'algorithmes de tri. Correction et complexité.}

\titlebox{blue}{\textblue{Références pour la leçon}}{
	\textblue{\cite{Beauquier-Berstel-Chretienne}} Beauquier, Berstel et Chrétienne, \emph{Éléments d'algorithmique}
	
	\textblue{\cite{Cormen}} Cormen, \emph{Algorithmique}
	
	\textblue{\cite{Froidevaux-Gaudel-Soria}} Froidevaux, Gaudel et Soria, \emph{Types de données et algorithmes}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.35\textwidth}
		Le tri par tas
	\end{minipage} \hfill
	\begin{minipage}{.5\textwidth}
		Le tri topologique
	\end{minipage}
}

\section*{Motivation}
	
\subsection*{Défense}
	
Le problème de tri est considéré par beaucoup comme un problème fondamental en informatique. Mais pourquoi trier?
\begin{itemize}
	\item Le problème de tri peut être inhérent à l'application : on cherche très souvent à classer des objets suivant leurs clés, comme lors de l'établissement des relevés bancaires.
	\item Le tri est donc souvent utilisé en pré-traitement dans de nombreux domaines de l'algorithmique : la marche de Jarvis (enveloppe convexe), l'algorithme du peintre (rendu graphique : quel objet je dois afficher en dernier pour ne pas l'effacer par d'autres?), la recherche dans un tableau (dichotomie), l'algorithme de Kruskal (arbre couvrant minimal) ou encore l'implémentation de file de priorité.
	\item Le tri a un intérêt historique: de nombreuses techniques ont été élaborées pour optimiser le tri.
	\item Le problème de tri est un problème pour lequel on est capable de trouver un minorant (en terme de complexité).
	\item L'implémentation d'algorithme de tri fait apparaître de nombreux problèmes techniques que l'on cherche à résoudre via l'algorithmique.
\end{itemize}
	
Dans cette leçon, nous effectuons deux hypothèses importantes : les éléments à trier tiennent uniquement en mémoire vive et l'ordre sur ces éléments (sous lequel on tri) est total.
	
\subsection*{Ce qu'en dit le jury}
	
Sur un thème aussi classique, le jury attend des candidats la plus grande précision et la plus grande rigueur.
	
Ainsi, sur l’exemple du tri rapide, il est attendu du candidat qu’il sache décrire avec soin l’algorithme de partition et en prouver la correction en exhibant un invariant adapté. L’évaluation des complexités dans le cas le pire et en moyenne devra être menée avec rigueur : si on utilise le langage des probabilités, il importe que le candidat sache sur quel espace probabilisé il travaille.
	
On attend également du candidat qu’il évoque la question du tri en place, des tris stables, des tris externes ainsi que la représentation en machine des collections triées.
	
\section*{Métaplan}
	
\begin{description}
	\item[I.] \textbf{Le problème de tri}
	\begin{small}
		\textblue{Les objets que l'on souhaite triés sont triés selon une clé (données satellites ou non)}
	\end{small}
		
	\begin{footnotesize}
		\begin{itemize}[label=$\rightsquigarrow$]
			\begin{minipage}{.4\textwidth}
				\item \emph{Problème}~\cite[p.122]{Beauquier-Berstel-Chretienne}: Problème du tri 
				\item \emph{Hypothèses}: tri interne (tout est en mémoire vive) et ordre total
				\item \emph{Définition}~\cite[p.307]{Froidevaux-Gaudel-Soria}: tri stable 
			\end{minipage} \hfill
			\begin{minipage}{.4\textwidth}
				\item \emph{Exemples} de tri dont un stable et l'autre non
				\item \emph{Définition}~\cite[p.136]{Cormen}: tri en place
				\item Critère de comparaison des algorithmes de tri
			\end{minipage}
		\end{itemize}
	\end{footnotesize}
	\item[II.] \textbf{Les tris par comparaison}
	\begin{small}
		\textblue{Ici la complexité de nos algorithmes peuvent être calculer en nombre de comparaisons effectuées puisque nos algorithmes de tris ne réalisent uniquement des comparaisons pour établir l'ordre. On s'autorise alors uniquement cinq tests sur les données à l'entrée : $=, <, >, \leq, \geq$. Il existe une borne minimale sur la complexité de ces algorithmes : on peut faire un premier classement des tris: ceux qui sont optimaux en moyenne, au pire cas ou pas du tout.}
	\end{small}
	
	\begin{footnotesize}
		\begin{itemize}[label=$\rightsquigarrow$]
			\begin{minipage}{.4\textwidth}
				\item \emph{Définition}~\cite[p.178]{Cormen}: Tri par comparaison 
			\end{minipage} \hfill
			\begin{minipage}{.4\textwidth}
				\item \emph{Théorème}~\cite[p.179]{Cormen}: Borne optimale 
			\end{minipage}
		\end{itemize}
	\end{footnotesize}
		
	\begin{footnotesize}
		\begin{center}
			\textblue{\begin{tabular}{|c|ccccc|}
				\hline
				Tri & Pire cas & En moyenne & Spatiale & Stable & En place \\
				\hline
				Sélection & $O(n^2)$ & $O(n^2)$ & $O(1)$ & $\CheckedBox$ & $\CheckedBox$ \\
				Insertion & $O(n^2)$ & $O(n^2)$ & $O(1)$ & $\CheckedBox$ & $\CheckedBox$ \\
				Fusion & $O(n \log n)$ & $O(n \log n)$ & $O(n)$ & $\XBox$ & $\XBox$ \\
				Tas & $O(n \log n)$ & $-$ & $O(1)$ & $\XBox$ & $\CheckedBox$ \\
				Rapide & $O(n^2)$ & $O(n \log n)$ & $O(1)$ & $\XBox$ & $\CheckedBox$ \\
				Dénombrement & $O(k + n)$ & $O(k + n)$ & $O(k)$ & $\CheckedBox$ & $\XBox$ \\
				Base & $O(d(k + n))$ & $O(d(k + n))$ & $O(dk)$ & $\CheckedBox$ & $\XBox$ \\
				Paquets & $O(n^2)$ & $O(n)$ & $O(n)$ & $\XBox$ & $\XBox$ \\
				\hline
			\end{tabular}}
		\end{center}
	\end{footnotesize}
		
	\begin{description}
		\item[A.] \emph{Les tris naïfs \cite[p.310]{Froidevaux-Gaudel-Soria}}
		\begin{small}
			\textblue{On commence par étudier les tris naïfs: ceux qui ne mettent en place aucun paradigme de programmation ni structures de données élaborées.}
		\end{small}
			
		\begin{footnotesize}
			\textbf{Tri par sélection}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.45\textwidth}
					\item \emph{Principe}: on cherche le minimum des éléments non triés et on le place à la suite des éléments triés
				\end{minipage}\hfill
				\begin{minipage}{.3\textwidth}
					\item Algorithmes classique et tri à bulle
					\item \emph{Complexité}: $O(n^2)$
					\item \emph{Propriétés}: stable et en place
				\end{minipage}
			\end{itemize}
					
			\textbf{Tri par insertion} \textblue{(Le tri par insertion est aussi appeler la méthode du joueur de carte. Java implémente ce tri pour des tableau de taille inférieure ou égale à 7.)}
				\begin{itemize}[label=$\rightsquigarrow$]
					\item \emph{Principe}: On insère un à un les éléments parmi ceux déjà trié.
					
					\begin{minipage}{.2\textwidth}
						\item \emph{Algorithme}
					\end{minipage}\hfill
					\begin{minipage}{.3\textwidth}
						\item \emph{Complexité}: $O(n^2)$
					\end{minipage}\hfill
					\begin{minipage}{.3\textwidth}
						\item \emph{Propriétés}: stable et en place
					\end{minipage}
					\item \emph{Remarques}: très efficace sur des petits tableau ou sur des tableau presque trié.
				\end{itemize}
			\end{footnotesize}
			\item[B.] \emph{Diviser pour régner}
			\begin{small}
				\textblue{On présente maintenant des algorithmes de tri basé sur le paradigme diviser pour régner: le premier découpe simplement le tableau de départ (tri fusion) tandis que le second combine facilement les deux sous tableau triés (tri rapide). Il existe des tris mixtes qui en fonctions de l'ensemble des données (taille, caractère trié) que l'on présente choisi l'un ou l'autre des algorithmes de tri.}
			\end{small}
			
			\begin{footnotesize}
				\textbf{Tri fusion \cite[p.30]{Cormen}}
				\begin{itemize}[label=$\rightsquigarrow$]
					\item \emph{Principe}: Séparation facile, recollement avec fusion
					
					\begin{minipage}{.36\textwidth}
						\item \emph{Algorithme}
					\end{minipage}\hfill
					\begin{minipage}{.4\textwidth}
						\item \emph{Complexité} (pire cas et moyenne): $O(n \log n)$
					\end{minipage}
					
					\item \emph{Application}: Calcul de jointure dans le cadre des bases de données
				\end{itemize}
				
				
				\textbf{Tri rapide \cite[p.157]{Cormen}} \textblue{Contre-exemple au Master theorem.}
				\begin{itemize}[label=$\rightsquigarrow$]
					\item \emph{Principe}: Séparation avec pivot, recollement facile
					
					\begin{minipage}{.4\textwidth}
						\item \emph{Algorithme}
						\item \emph{Complexité} dans le pire cas: $O(n^2)$
					\end{minipage}\hfill
					\begin{minipage}{.4\textwidth}
						\item \emph{Complexité} en moyenne $O(n \log n)$
						\item \emph{Propriété}: en place; le plus rapide en pratique
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[C.] \emph{Structure de données \cite[p.140]{Cormen}}
			\begin{small}
				\textblue{C'est un algorithme de tri qui se base sur les propriétés d'une structure de données bien précise: le tas. Elle permet de gérer les données. Le terme tas qui fut d'abord inventé dans ce contexte est aujourd'hui également utilisé dans un contexte d'exécution d'un programme: c'est une partie de la mémoire qu'utilise un programme. Un programme lors qu'il s'exécute utilise une mémoire de pile (qui donne les instructions suivantes à faire) et un tas (qui contient les valeurs des variables, de la mémoire auxiliaire pour faire tourner le programme). Ces deux mémoires grandissent l'une vers l'autre (l'erreur de \emph{stack overflow} arrive quand la pile rencontre le tas). Le tri par tas est un tri en place et de complexité $O(n\log n)$ en moyenne. C'est donc un des meilleurs tri par comparaison que l'on possède.}
			\end{small}
			
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.42\textwidth}
						\item \emph{Définition}: un tas
						\item \emph{Principe}: Construction d'un tas avec les entrées et extraction du maximum.
					\end{minipage} \hfill
					\begin{minipage}{.34\textwidth}
						\item \textred{Algorithme, correction et complexité}
						\item \emph{Propriétés}: en place
						\item \emph{Application}: file de priorité
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
		\end{description}
		
		\item[III.] \textbf{Les tris linéaires}
		\begin{small}
			\textblue{On fait des hypothèses sur l'entrée de nos algorithmes afin d'obtenir un tri linéaire. De plus, ces algorithmes fond appel à d'autres opérations que les comparaisons: on fait abstraction de la borne de minimalité.}
		\end{small}
		
		\begin{footnotesize}
			\textbf{Tri par dénombrement \cite[p.180]{Cormen}} 
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Hypothèse}: Valeurs de l'entrées $\in \llbracket 0, k \rrbracket$ avec $k$ fixé.
					\item \emph{Principe}: Combien sont inférieur à $x$. 
					\item \emph{Méthode}: Utilisation d'un tableau
				\end{minipage} \hfill
				\begin{minipage}{.38\textwidth}
					\item Algorithme et exemple 
					\item \emph{Propriétés}: tri stable \textblue{(Attention si valeurs égales)}
					\item \emph{Complexité}: $O(n + k)$ \textblue{(Linéaire: si $k = O(n)$)}
				\end{minipage}
			\end{itemize}
			
			
			\textbf{Tri par paquets \cite[p.185]{Cormen}}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \emph{Hypothèse}: Clés uniformément distribuées sur $[0, 1[$.
				\item \emph{Principe}: Divise $[0, 1[$ en $n$ intervalles de même taille et tri par insertion 
				
				\begin{minipage}{.34\textwidth}
					\item Algorithme
				\end{minipage} \hfill
				\begin{minipage}{.38\textwidth}
					\item \emph{Complexité espérée}: $O(n)$
				\end{minipage}
			\end{itemize}
			
			\textbf{Tri par base \cite[p.182]{Cormen}}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \emph{Hypothèse}: $d$ sous-clés entières bornées muni d'un poids
				\item \emph{Principe}: Application d'un tri par dénombrement \textblue{(stable)} sur les sous-clés
				
				\begin{minipage}{.46\textwidth}
					\item \emph{Application}: Trier des cartes perforées 
					\item \emph{Application}: Trier des dates
					\item \emph{Application}: trier des chiffres
				\end{minipage} \hfill
				\begin{minipage}{.38\textwidth}
					\item \emph{Propriétés}: tri stable; pas en place 
					\item \emph{Complexité}: $O(n)$
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[IV.] \textbf{Affaiblissement des hypothèses pour le tri}
		\begin{small}
			\textblue{\emph{Hypothèse}: on n'utilise maintenant plus qu'un ordre partiel: on utilise l'ordre donné par un graphe.}
		\end{small}
		
		\begin{description}
			\item[A.] \emph{Tri topologique \cite[p.565]{Cormen}}
			\begin{small}
				\textblue{On commence par étudier les tris naïfs: ceux qui ne mettent en place aucun paradigme de programmation ni structures de données élaborées.}
			\end{small}
			
			\begin{footnotesize}
				\begin{itemize}[label=$\rightsquigarrow$]
					\begin{minipage}{.4\textwidth}
						\item \emph{Problème}: Problème du tri topologique
						\item \emph{Remarque}: ordre partiel induit par le graphe
					\end{minipage}\hfill
					\begin{minipage}{.4\textwidth}
						\item \textred{Algorithme du tri topologique et correction}
					\end{minipage}
				\end{itemize}
			\end{footnotesize}
			\item[B.] \emph{Tri externe}
			\begin{small}
				\textblue{On s'autorise des données qui ne tiennent pas en mémoire vive.}
			\end{small}
		\end{description}
		
		\item[Ouverture] 
		\begin{footnotesize}
			Tri de shell; Réseau de tri
		\end{footnotesize}
	\end{description}	
