\documentclass[a4paper,11pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais,english]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}
\usepackage{subcaption}
\usepackage{multirow}

\usepackage{algorithm}
\usepackage{algpseudocode}%

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{listings}


\parskip=0\baselineskip %0.5 d'habitude
\usetikzlibrary{automata,positioning,shadows}
\usetikzlibrary{calc, shapes, backgrounds,arrows}

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{lemme}{Lemme}
	\newtheorem*{cor}{Corollaire}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Leçon 926 : Analyse des algorithmes : Complexité. Exemples.}
	
	\date{2018 - 2019}
	\author{Julie Parreaux}
	
	\maketitle
	
	\titlebox{blue}{Références pour la leçon}{
		\textblue{\cite{Beauquier-Berstel-Chretienne}} Beauquier, Berstel et Chretienne, \emph{Éléments d'algorithmique.}
		
		\textblue{\cite{Carton}} Carton, \emph{Langages formels, calculabilité et complexité.}
		
		\textblue{\cite{Cormen}} Cormen, \emph{Algorithmique.}
		
		\textblue{\cite{Froidevaux-Gaudel-Soria}} Froidevaux, Gaudel et Soria, \emph{Types de données et algorithmes.}
	}
	
	\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
		\begin{minipage}{.5\textwidth}
			Étude d'Union-Find pour la complexité
		\end{minipage} \hfill
		\begin{minipage}{.45\textwidth}
			Algorithme de Dijkstra
		\end{minipage} 
	}
	
	\renewcommand{\contentsname}{Plan de la leçon}
	\tableofcontents

	\section*{Motivation}
	
	\subsection*{Défense}
	
	Lors de la conception, puis de l'étude d'un algorithme, deux notions sont extrêmement importantes:
	\begin{itemize}
		\item la correction de l'algorithme : fait-il ce que l'on souhaite?
		\item l'efficacité de l'algorithme: à quelle vitesse s'exécute-t-il?; est-il optimal?
	\end{itemize}
	La complexité (temporelle ou spatiale) intervient alors pour comparer deux algorithmes corrects répondant à la même question.
	
	\subsection*{Ce qu'en dit le jury}
	
	Il s’agit ici d’une leçon d’exemples. Le candidat prendra soin de proposer l’analyse d’algorithmes portant sur des domaines variés, avec des méthodes d’analyse également variées : approche combinatoire ou
	probabiliste, analyse en moyenne ou dans le pire cas.
	
	Si la complexité en temps est centrale dans la leçon, la complexité en espace ne doit pas être négligée. La notion de complexité amortie a également toute sa place dans cette leçon, sur un exemple bien choisi, comme union find (ce n’est qu’un exemple).
	
	\section{Quantifier la complexité}
	\textblue{Définir la complexité d'un algorithme n'est pas facile. Intuitivement la complexité d'un algorithme est un indicateur de la difficulté pour résoudre le problème traité par l'algorithme. Mais cette vue de l'esprits n'est pas simple à quantifier. Nous allons alors définir la complexité comme une fonction qui en fonction des entrées sur notre programme donnera la consommation d'une certaine ressource.}
	
	\subsection{Qu'est-ce que la complexité?}
	\begin{itemize}
		\item \emph{Définition}: donnée d'entrée d'un algorithme (= ensemble des variables externes à l'algorithmes sur lesquelles on exécute celui-ci)
		\item \emph{Exemple}: un algorithme de tri sur un tableau prend un tableau en entrée
		\item \emph{Définition}: taille d'une entrée: $f: \{entree\} \to \N^k$
		\item \emph{Exemples}: mot: nombre de caractère; tableau : nombre de cellule ou l'espace mémoire utilisé
		\item \emph{Définition}: complexité comme fonction $\phi$ des entrées vers une ressources qui peut être temporelle, spatiale, le nombre d'accès à un disque externe, ...
		\item \emph{Remarque}: en pratique on compte des unités de bases (peut être des cellules mémoires, des fonctions que l'on considère de bases)
		\item \emph{Exemples}: complexité temporelle: tri bulle : $\frac{n(n-1)}{2}$; complexité disque dur : recherche B-arbre $h$ où $h$ hauteur; \textblue{complexité temporelle : plus longue sous-séquence commune}
	\end{itemize}
	
	\subsection{Mesurer la complexité \cite[p.40]{Cormen}}
	On ne peut pas toujours calculer la complexité exacte (avec les constantes) car généralement, elle dépend de l'implémentation que nous utilisons. \textblue{Pour cette même raison le calcul de la complexité exacte peut s'avérer inutile.}
	\begin{itemize}
		\item \emph{Définition} Notation de Landau ($O$ et $\Theta$) + abus de notation
		\item \emph{Proposition}: Équivalence des notations + illustration
		\item \emph{Exemples}: Tri bulle $ O(n^2)$; B-arbre $O(\log n)$
		\item \emph{Proposition}: L'ordre de croissance
	\end{itemize}
	
	\subsection{Différentes approches de la complexité \cite[p.19]{Froidevaux-Gaudel-Soria}}
	\begin{itemize}
		\item \emph{Définition}: Complexité pire cas, meilleur cas en moyenne \textblue{(avec les probabilités)}
		\item \emph{Remarque}: Distribution uniforme : simplification de l'écriture mais pas toujours vrai \textred{Attention}
		\item \emph{Définition}: complexité constante
	\end{itemize}
	
	\section{Techniques de calcul de la complexité}
	\textblue{Maintenant que nous avons donné un cadre à notre théorie de la complexité, nous souhaitons calculer les complexités d'algorithmes usuels. Pour cela, nous allons présenter quelques techniques de calcul élémentaires.}
	
	\subsection{Le calcul direct}
	On peut parfois calculer directement la complexité en dénombrant les opérations que l'on doit calculer. \textblue{Efficace dans le cas d'algorithmes itératifs}
	
	\noindent\emph{Exemple} : Recherche de maximum dans un tableau / CYK (Algorithme~\ref{algo:CYK}) $O(n^3)$ \cite[p.198]{Carton} / Marche de Jarvis (Algorithme~\ref{algo:envConv_Jarvis}) $O(hn)$ \cite[p.389]{Beauquier-Berstel-Chretienne}
	
	\subsection{La résolution de récurrence \cite[p.20]{Beauquier-Berstel-Chretienne}}
	On peut parfois exprimer la complexité pour une donnée de taille $n$ par rapport à une donnée de taille strictement inférieure. Résoudre l'équation ainsi obtenue nous donne la complexité. \textblue{Efficace dans le cas d'algorithmes récursifs.}
	\begin{itemize}
		\item \emph{Proposition} suite récurrente linéaire d'ordre 1 + \emph{Exemples} Factorielle et Euclide 
		\item \emph{Proposition} suite récurrente linéaire d'ordre 2 + \emph{Exemple} Fibonacci
		\item \emph{Proposition}: Master theorem + \emph{Exemples} Tri fusion et Strassen
		\item \emph{Remarque}: Ne capture pas toutes les équations par récurrences.
	\end{itemize}
	
	\subsection{Le calcul de la complexité moyenne par l'espérance}
	\begin{itemize}
		\item \emph{Remarque}: on a une distribution uniforme donc $c_{moy} = \frac{1}{|D_n|} \sum_{x \in D_n} \phi(x)$
		\item \emph{Exemple}: tri rapide randomisé
		\item \emph{Remarque}: on fait souvent l'hypothèse d'avoir une distribution uniforme sur les données de l'entrée, cependant c'est généralement faux et cela nous introduit des erreurs.
	\end{itemize}
	
	\section{Raffinement de l'étude de la complexité : la complexité amortie}
	\textred{La complexité amortie n'est pas une complexité moyenne!} \textblue{La complexité amortie est une amélioration de l'analyse dans le pire cas s'adaptant (ou calculant) aux besoins de performance des structures de données.}
	\begin{itemize}
		\item \emph{Définition}: la complexité amortie : $c_{amo} = \underset{op_1, \dots, op_k}{\max} \sum_{i=1}^{k} \frac{c(op_i)}{k}$
		\item \emph{Exemple}: table dynamique où on souhaite la complexité d'un élément qu'on insère en nombre d'allocation et d'écriture \cite[p.428]{Cormen}
	\end{itemize}
	
	\subsection{Méthode de l'agrégat}
	\textblue{Dans cette méthode, le coût amortie est le même pour toutes les opérations de la séquence même si elle contient différentes opérations}
	\begin{itemize}
		\item \emph{Principe}: On calcul la complexité dans le pire cas pour cette séquence. La complexité amortie d'une opération est donc cette complexité divisée par le nombre d'opérations de la séquence \cite[p.418]{Cormen}.
		\item \emph{Exemples} : table dynamique, Union find + Kruskal \textred{DEV}, balayage de Gram
	\end{itemize}
	
	
	\subsection{Méthode comptable}
	\textblue{Cette méthode se différencie de la précédente en laissant la possibilité que toutes les opérations ait un coup amortie différent.}
	\begin{itemize}
		\item \emph{Principe}: On attribut à chaque opération un crédit et une dépense \textblue{(qui peuvent être différent de leur coût réel)}. Ces crédits sont plus importants pour les opérations coûteuses afin de rattraper leur dépenses importantes. Cependant, elles doivent vérifier la propriété suivante : $\sum_{i = 1}^{k} cred(op_i) - \sum_{i = 1}^{k} dep(op_i) \geq 0$. On a alors : $c_{amo}(op) = c_{reel}(op) + cred(op) - dep(op)$.
		\item \emph{Exemples} : table dynamique, KMP
	\end{itemize}
	
	\subsection{Méthode du potentiel}
	\textblue{Cette méthode a été popularisé lors de la preuve de la complexité amortie de la structure de données Union Find, implémentée à l'aide d'une forêt et des heuristiques qui vont bien. Elle est moins facile que les autres à mettre en place.}
	\begin{itemize}
		\item \emph{Principe}: Au lieu d'assigner des crédits à des opérations, on va associer une énergie potentielle $\varphi$ à la structure elle-même. Cette énergie vérifie les propriétés suivantes :$\varphi(\text{structure vide}) = 0$ et $\forall T, \varphi(T) \geq 0$. On a alors $c_{amo}(op) = c_{reel}(op) + \varphi(T) - \varphi(T')$ lorsque l'opération $op$ permet de passer de $T$ à $T'$ \cite[p.424]{Cormen}.
		\item \emph{Exemples} : table dynamique
	\end{itemize}
	
	\section{Amélioration de l'étude de la complexité}
	Plusieurs pistes existe afin d'améliorer la complexité d'un algorithme : utiliser une structure de données plus adaptée, utiliser un peu plus de mémoire ou au contraire se souvenir de moins de choses, ... Cependant, quelques fois nous sommes capable de mettre une borne minimale sur la complexité d'une famille de problème : quand nous avons atteint cette borne on sait que nos algorithmes sont optimaux. 

	\subsection{Borne minimale de la complexité sur une classe de problème}
	\begin{itemize}
		\item \emph{Proposition} : Borne minimal d'un algorithme de tri
		\item \emph{Exemple} : Tri par insertion $O(n^2) \geq O(n \log n)$;
		Tri fusion $O(n \log n)$ atteint cette borne
		\item \emph{Remarque}: même si on atteint la borne optimale asymptotique, on peut vouloir optimiser les constantes (tri rapide est en moyenne plus rapide que le tri fusion).
	\end{itemize}
	
	\subsection{Utilisation de structures de données adaptées}
	\textblue{Une piste pour améliorer un algorithme : utiliser une bonne structure de données}
	\begin{itemize}
		\item \emph{Exemple} : tri par tas + \emph{Remarque} : dépend de la manière dont on implémente la structure
		\item \emph{Exemple} : représentation d'un graphe + \emph{Remarque}: utilisation de ces représentation
		\item \emph{Exemple} : Prim (liste d'adjacence) + Floyd Warshall (matrice d'adjacence)
		\item On peut changer de structure de données
		\item \emph{Exemple} : Dijstra \textred{DEV}
	\end{itemize}

	\subsection{Compromis espace/temps}
	\begin{itemize}
		\item \emph{Principe} : On peut vouloir améliorer le temps au détriment de l'espace ou vise-versa.
		\item \emph{exemple} : Fibonacci
		\begin{itemize}
			\item récursif : $O(n^2)$ en temps et $O(1)$ en espace \textblue{(amélioration de l'espace)} 
			\item programmation dynamique : $O(n)$ pour le temps et l'espace \textblue{(amélioration du temps)}
			\item utilisation de deux variables : $O(n)$ pour le temps et $O(1)$ pour l'espace \textblue{(gagnant sur les deux tableaux)}
		\end{itemize}
		\item \emph{Principe}: le mémoïsation \cite[p.338]{Cormen} + \emph{Exemple} : découpage de barre
	\end{itemize}	
	
	\section*{Ouverture}
	\addcontentsline{toc}{section}{Ouverture}
	Évaluer la complexité d'un algorithme (hors implémentation) n'est pas une tâche facile. Souvent plusieurs astuces sont nécessaire pour trouver la meilleure borne sur notre complexité possible. Quelque fois, un approximation grossière de notre complexité (évaluation de la complexité pour le tri par tas) suffit.
	
	\section*{Quelques notions importantes}
	
	\subsection*{Notation de Landau}
	\input{./../Notions/Complexite_Landau.tex}
	
	\subsection*{Quelques analyses d'algorithmes}
	\input{./../Notions/Complexite_Exemples.tex}
	
	\subsection*{Analyse de relation par récurrence}
	\input{./../Notions/Complexite_Reccurence.tex}
	
	
	\bibliographystyle{plain}
	\bibliography{./../../Livre}

	
\end{document}