\documentclass[a4paper,11pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais,english]{babel}%

\usepackage[dvipsnames,svgnames]{xcolor}
\usepackage{graphicx}%
\usepackage{subcaption}
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{algorithm}
\usepackage{algpseudocode}%

\usepackage{array}
\usepackage{tikz}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{listings}
\usepackage{multirow}


\parskip=0\baselineskip %0.5 d'habitude
\usetikzlibrary{automata,positioning,shadows}
\usetikzlibrary{calc, shapes, backgrounds,arrows}

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
} 


\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Leçon 901 : Structure de données. Exemples et applications.}
	
	\date{2018 - 2019}
	\author{Julie Parreaux}
	
	\maketitle
	
	\titlebox{blue}{Références pour la leçon}{
		\textblue{\cite{Beauquier-Berstel-Chretienne}} Beauquier, Berstel et Chretienne, \emph{Éléments d'algorithmique.}
		
		\textblue{\cite{Cormen}} Cormen, \emph{Algorithmique.}
		
		\textblue{\cite{Froidevaux-Gaudel-Soria}} Froidevaux, Gaudel et Soria, \emph{Types de données et algorithmes.}
	}
	
	\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
		\begin{minipage}{.35\textwidth}
			Les B-arbres
		\end{minipage} \hfill
		\begin{minipage}{.5\textwidth}
			La structure de donnée d'Union-Find
		\end{minipage}
	}
	
	\renewcommand{\contentsname}{Plan de la leçon}
	\tableofcontents
	
	\section*{Motivation}
	
	\subsection*{Défense}
	
	Les structures de données jouent un rôle important dès que on souhaite stoker et manipuler (efficacement) des ensembles de  données (qui sont généralement dynamique). La notion de type abstrait apparaît alors pour s'abstraire du langage de programmation. Elle intervient dans la conception des algorithmes qui est longue. On fait des raffinements successifs: la première version se fait indépendamment des structures de données; la dernière version implémente dans un langage de programmation cette structure de données.
	
	L'utilisation de cette structure abstraite se fait de deux manières différentes: la méthode ascendante qui part du langage et donne une structure abstraire ou la méthode descendante qui part de la structure abstraite et qu'on adapte au langage.
	
	Le choix d'une structure de données est importante. Elle doit s'adapter au besoins et à la "sémantique" de l'algorithme. De plus, en fonction du problème (donc de l'algorithme), certaines structures de données sont plus efficaces que d'autre. On peut alors optimiser la complexité de nos algorithmes.
	\newline
	
	\noindent\textred{\textbf{Remarque importante:} Cette leçon n'est pas évidente : on peut très vite tombé dans un catalogue de structure de données sans aucune logique et le rapport du jury n'est pas nécessairement d'une grande aide. Mettre en avant les choix d'implémentations tout au long de la leçon et ne pas oublier de présenter des algorithmes utilisant ces structures. Cette leçon est fourre tout: toutes les notions d'algorithmique au programme (ou presque) peuvent se caser ici. Il y a alors un grand nombre de développements que l'on peut casé ici (on ne les a pas indiqué sur le plan...), il est donc important de soigné son plan.}
	
	\noindent\textblue{Cette leçon se prête tout particulièrement aux dessins. Des dessins illustrant les structures de données classiques et leurs opérations pas nécessairement triviale peut s'avérer être intéressant (pensé à prendre le temps de les faire).}
	
	\subsection*{Ce qu'en dit le jury}
	
	Le mot algorithme ne figure pas dans l’intitulé de cette leçon, même si l’utilisation des structures de données est évidemment fortement liée à des questions algorithmiques. La leçon doit donc être orientée plutôt sur la question du choix d’une structure de données. Le jury attend du candidat qu’il présente différents types abstraits de structures de données en donnant quelques exemples de leur usage avant de s’intéresser au choix de la structure concrète. Le candidat ne peut se limiter à des structures linéaires simples comme des tableaux ou des listes, mais doit présenter également quelques structures plus complexes, reposant par exemple sur des implantations à l’aide d’arbres. Les notions de complexité des opérations usuelles sur la structure de données sont bien sûr essentielles dans cette leçon.
	
	\section*{Introduction}
	\addcontentsline{toc}{section}{Introduction}
	
	\noindent\emph{Objectif}: Comment choisir une bonne structure de données? \textblue{(phrase d'accroche)}
	
	\noindent\emph{Motivation}: Stoker et manipuler (efficacement) un ensemble de données (dynamiques).
	
	\begin{definition}
		Un type de données abstrait \cite[p.37]{Beauquier-Berstel-Chretienne} est la description d'un ensemble de données et les opérations que l'on peut y appliquer. 
	\end{definition}
	
	\begin{definition}
		Un type de données concret \cite[p.37]{Beauquier-Berstel-Chretienne} est la description d'un format de représentations internes des données en machine. 
	\end{definition}
		
	\begin{definition}
		Une structure de données pour un type abstrait  \cite[p.37]{Beauquier-Berstel-Chretienne} est la donnée d'un type concret et des implémentations des différentes fonctions associées. 
	\end{definition}
	
	\textblue{Tout au long de la leçon nous allons faire un va et vient entre la notion de type de donné abstrait et structure de données. Nous étudions notamment quelques types concrets pour les types abstraits que l'on présente.}
	
	
	\section{Un type abstrait général (le type abstrait de base)}
	
	Nous commençons par donner un type abstrait de base correspondant à la structure "optimale" que nous voulons construire. Elle contient toutes les opérations de base que nous souhaitons réaliser sur une structure de données afin de manipuler ces données. Nous allons ensuite données trois structures de données pour ce type abstrait.
	
	\subsection{Les opérations souhaitées}
	
	Une liste d'opérations : \textsf{create}, \textsf{union}, \textsf{find}, \textsf{insert}, \textsf{remove}, \textsf{find-earliest}, \textsf{find-min}.
	
	\textblue{Remarque: on présente dans cette section des structures de données implémentant ce type abstrait. Dans la suite de la leçon nous allons également nous intéresser des structures de données qui implémentent qu'une partie de ces fonctions pour répondre à des besoins précis.}
	
	\subsection{La table dynamique : une structure de données contiguë}
	
		\begin{center}
			\begin{tabular}{|c|lll|}
				\hline
				\multirow{2}{*}{\emph{Type concret}} & \multicolumn{2}{l}{Espace mémoire contiguë de taille fixe} & \\
				& \multicolumn{2}{l}{Entier conservant le nombre de données} & \\
				\hline
				\multirow{6}{*}{\emph{Implémentation}} &  \multirow{3}{*}{\textsf{insert}} & Si le tableau est plein, création un nouveau qui vaut & \\
				& & deux fois la taille du premier; sinon ajout de l'élément & \\
				& & dans la première case vide & \\
				\cline{2-4}
				& \textsf{delete} & Mettre le dernier élément sur l'élément supprimé & \\
				\cline{2-4}
				& \textsf{find-earliest} & Impossible & \\
				\cline{2-4}
				& \multirow{2}{*}{\textsf{union}} & Création d'un tableau de la taille des deux contenant & \\
				& & les mêmes valeurs & \\
				\cline{2-4}
				& \multicolumn{2}{l}{Le reste des opérations se fait en parcourant le tableau séquentiellement} & \\
				\hline
			\end{tabular}
		\end{center}	
	
	\subsection{La liste (l'apparition des pointeurs)}
	
	\begin{center}
		\begin{tabular}{|c|lll|}
			\hline
			\emph{Type concret} & \multicolumn{2}{l}{une liste chaînée (à l'aide de pointeurs)} & \\
			\hline
			\multirow{6}{*}{\emph{Implémentation}} &  \textsf{insert} & Insertion le nouvel élément en tête & \\
			\cline{2-4}
			& \multirow{2}{*}{\textsf{delete}} & Manipulation de pointeurs (un cran avant car  & \\
			& & le prédécesseur va pointer sur le successeur) & \\
			\cline{2-4}
			& \textsf{find-earliest} & Premier élément de la liste & \\
			\cline{2-4}
			&\multirow{2}{*}{\textsf{union}} & le dernier élément d'une liste va pointer sur le 1er élément & \\
			& & de la 2ième & \\
			\cline{2-4}
			& \multicolumn{2}{l}{Le reste des opérations se fait en parcourant séquentiellement les pointeurs} & \\
			\hline
		\end{tabular}
	\end{center}
	
	\begin{prop}[Comparaison des complexité selon le type concret choisi]
	On note $n$ le nombre d'éléments dans la structure et $m$ le nombre d'éléments dans la deuxième structure si besoin.
		
		\begin{center}
			\begin{tabular}{|c|cc|cc|}
				\hline
				Opération & \multicolumn{2}{c|}{Tableau} & \multicolumn{2}{c|}{Liste} \\
				\hline
				find & $O(n)$ & \textblue{(Au pire: parcourir tout le tableau)} & $O(n)$ & \textblue{(Au pire : parcourir tout la liste)}  \\
				\hline
				create & $O(n)$ & \textblue{(Réserver $n$ places mémoires)} & $O(1)$ & \textblue{(Réserver une place mémoire)} \\
				\hline
				\multirow{3}{*}{union} & \multirow{3}{*}{$O(n + m)$} & \textblue{(Construire un nouveau tableau}  & \multirow{3}{*}{$O(1)$} & \textblue{(Connexion de la première} \\
				& & \textblue{de taille $n+m$ et recopier les} & & \textblue{à la dernière (un pointeur} \\		
				& & \textblue{deux précédents dans celui-ci)} & & \textblue{sur le dernier élément))} \\	
				\hline
				\multirow{2}{*}{insert} & $O(n)$ & \textblue{(au pire: créer tableau de taille $2n$)} & \multirow{2}{*}{$O(1)$} & \multirow{2}{*}{\textblue{(au pire: rajout d'un pointeur)}} \\
				& $O(1)$ & \textblue{(complexité amortie)} & & \\
				\hline
				delete & $O(n)$ & \textblue{(find et recopie des suivants)} & $O(n)$ & \textblue{(find et pointeurs)} \\
				\hline
				find-min & $O(n)$ & \textblue{(parcourir le tableau)} & $O(n)$ & \textblue{(parcourir la liste)} \\
				\hline
				find-earliest & & impossible & $O(1)$ & \textblue{(le premier élément de la liste)} \\
				\hline
			\end{tabular}
		\end{center}
	\end{prop}

	\textblue{Dans la suite de la leçon nous réduisons le nombre d'opérations dans les structures de données afin de les spécialiser.}
	
	\section{Les structures de données d'ordonnancement}
	
	\textblue{Les structures de données d'ordonnancement nous donne des structures qui nous permettent de ranger les données selon un certains ordre : de les trier.}
	
	\subsection{La pile et la file: des structures séquentielles}
	
	\begin{center}
		\begin{tabular}{|c|ll|}
			\hline
			\multirow{2}{*}{\emph{Type abstrait}} & \textbf{Pile} & \textsf{insert}, \textsf{create}, \textsf{find-earliest}, \textsf{delete-ealiest} \\
			& \textbf{File} & \textsf{insert}, \textsf{create}, \textsf{find-oldest}, \textsf{delete-oldest} \\
			\hline
			\multirow{2}{*}{\emph{Application}} & \multicolumn{2}{l|}{parcours en profondeurs de graphe (algorithme~\ref{algo:parcoursGraphe}) \cite[p.102]{Beauquier-Berstel-Chretienne}} \\
			& \multicolumn{2}{l|}{complexité: $O(m+n)$ ($m$ le nombre de sommets et $n$ le nombre d'arcs de $G$)} \\
			\hline
			\emph{Type concret} & \multicolumn{2}{l|}{une liste doublement chaînée} \\
			\hline
			\multirow{3}{*}{\emph{Implémentation}} &  \textsf{insert} & on insère le nouvel élément en tête \\
			& \textsf{delete} & supprimer le premier ou le dernier élément de la liste \\
			& \textsf{find} & donner le premier ou le dernier élément de la liste doublement chaînée\\
			\hline
		\end{tabular}
	\end{center}
	
	\begin{algorithm}
		\begin{algorithmic}[1]
			\Function{\textsc{Parcours-profondeur}}{$G, s$} 
			\State  create $S$ \Comment{$S$ est une file}
			\While {$S \neq \emptyset$} 
			\State $(p, v)$ = find-oldest $S$
			\State delete-oldest $S$
			\If {$v$ est non marqué} 
			\State Marquer $v$
			\State parent$[v] \gets p$
			\For {$(w, v) \in G$}
			\State insert $(v, w)$
			\EndFor
			\EndIf
			\EndWhile
			\EndFunction
		\end{algorithmic}
		\caption{Parcours en profondeur d'un graphe $G$ à l'aide d'une pile}
		\label{algo:parcoursGraphe}
	\end{algorithm}
	
	\begin{prop}
		L'ensemble des opérations définies sur la pile ou la file s'effectue en $O(1)$ à l'aide de la représentation par liste doublement chaînée.
	\end{prop}
	
	\subsection{La file de priorité: une généralisation}
	
	\begin{center}
		\begin{tabular}{|c|ll|}
			\hline
			\emph{Type abstrait} & \textbf{Pile} & \textsf{insert}, \textsf{create}, \textsf{find-max}, \textsf{delete-max} \\
			\hline
			\multirow{3}{*}{\emph{Applications}} & \multicolumn{2}{l|}{algorithme de Dijkstra (algorithme~\ref{algo:Disjktra}) \cite[p.600]{Cormen}} \\
			& & complexité dépend du type concret choisi \\
			& \multicolumn{2}{l|}{tri par tas (pour tout valeurs on trouve le max et on l'enlève)} \\
			\hline
			\emph{Type concret} & \multicolumn{2}{l|}{tas binaire vu comme un tableau \cite[p.140]{Cormen}} \\
			\hline
			\multirow{4}{*}{\emph{Implémentation} \cite[p.140]{Cormen}} &  \textsf{insert} & algorithme~\ref{algo:file-insert} \\
			& \textsf{delete} & algorithme~\ref{algo:file-delete} \\
			& \textsf{find-max} & prendre la racine\\
			& \textsf{create} & insertion une à une des données dans un tas vide \\
			\hline
		\end{tabular}
	\end{center}
	
	\begin{req}
		On peut faire une file de priorité analogue en min. De plus, l'ordre sur les clé est général puisqu'il détermine le min ou la max de cette structure de données.
	\end{req}
	
	\begin{algorithm}
		\begin{algorithmic}[1]
			\Function{\textsc{Dijkstra}}{$G, w, s$} 
			\State unique-initialisation($G, s$) \Comment{Initiale tous les poids de tous les sommets vers $s$}
			\State  $E = \emptyset$ \Comment{$E$ est un ensemble}
			\State $F = G.S$ \Comment{$F$ est une file de priorité}
			\While {$F \neq \emptyset$} 
			\State $u$ = find-min $F$
			\State $E \gets E \cup \{u\}$
			\For {$(u, v) \in G.E$}
			\State relâcher $(u, v, w)$ \Comment{amélioration de l'estimation du sommet vers $s$}
			\EndFor
			\EndWhile
			\EndFunction
		\end{algorithmic}
		\caption{Algorithme de Dijkstra}
		\label{algo:Disjktra}
	\end{algorithm}
	
	\begin{minipage}[l]{.46\linewidth}
		\begin{algorithm}[H]
			\begin{algorithmic}[1] 
				\State Insertion la plus à gauche possible de $x$
				\While {$x$ n'est pas racine et $x > \text{pere}(x)$} 
				\State échange $x$ et $\text{pere}(x)$
				\EndWhile
			\end{algorithmic}
			\caption{Insertion dans une file de priorité}
			\label{algo:file-insert}
		\end{algorithm}
	\end{minipage} \hfill
	\begin{minipage}[r]{.46\linewidth}
		\begin{algorithm}[H]
			\begin{algorithmic}[1] 
				\State On retire la racine
				\State On échange avec la plus base des valeurs $x$
				\While {$x$ a des fils et $x < \max \text{fils}(x)$} 
				\State échange $x$ et $\max \text{fils}(x)$
				\EndWhile
			\end{algorithmic}
			\caption{Suppression dans une file de priorité}
			\label{algo:file-delete}
		\end{algorithm}		
	\end{minipage}
		
	\begin{definition}
		Un tas binaire est un arbre dont les descendant d'un nœud sont tous supérieurs ou égaux à ce nœud.
	\end{definition}
	
	\begin{prop}[Comparaison des complexités en fonctions des types concrets] On note $n$ est le nombre de données.
		
		\begin{center}
			\begin{tabular}{|c|c|c|c|c|}
				\hline
				type concret & create & insert & find-max & delete-max \\
				\hline
				liste & $O(n)$ & $O(1)$ & $O(n)$ & $O(1)$ \\
				liste triée & $O(n\log n)$ & $O(n)$ & $O(1)$ & $O(1)$ \\
				tas binaire & $O(n\log n)$ & $O(\log n)$ & $O(1)$ & $O(\log n)$ \\
				tas binomiale & $O(n\log n)$& $O(\log n)$ & $O(1)$ & $O(\log n)$ \\
				tas de Fibonacci & $O(n)$ & $O(1)$ & $O(1)$ & $O(\log n)$ amortie \\
				\hline
			\end{tabular}
		\end{center}
	\end{prop}
	
	\subsection{Le graphe: des relations d'ordres partiels}
	
	\begin{center}
		\begin{tabular}{|c|ll|}
			\hline
			\multirow{2}{*}{\emph{Type abstrait}} & \multicolumn{2}{l|}{relationnel de test principal donnant s'il y a une connexion entre} \\ & \multicolumn{2}{l|}{2 sommets. Il est non-orienté si le test est symétrique et orienté sinon.} \\
			\hline
			\emph{Application} & \multicolumn{2}{l|}{tri topologique (algorithme~\ref{algo:tri-topologique}) \cite[p.567]{Cormen}; complexité : $O(|V| + |E|)$} \\
			\hline
			\multirow{2}{*}{\emph{Types concrets}} & liste d'adjacence & $O(1)$ l'ajout/suppression et $O(n)$ test \\
			& matrice d'adjacence & $O(1)$ test et ajout/suppression $O(n)$ \\
			\hline
		\end{tabular}
	\end{center}
	
	\begin{algorithm}
		entrée : $G = (V, E)$; sortie : $L$ une liste contenant tous les sommets de $V$
		\begin{algorithmic}[1] 
			\State Pour chaque sommet on fait un parcours en profondeur pour calculer les dates de fin de traitement
			\State A chaque fin de traitement, insérer l'élément dans $L$
			\State Renvoie $L$
		\end{algorithmic}
		\caption{Tri topologique}
		\label{algo:tri-topologique}
	\end{algorithm}	
	
	\begin{req}
		Le graphe induit un ordre non total (on a un ordre partiel) dans les données.
	\end{req}
	
	\section{Les structures de recherche : dictionnaires}
	
	\textblue{Les structures de recherche et en particulier le dictionnaire sont des structures facilitant la recherche dans un ensemble de données. Pour cela, elles vont les trier de manière à faciliter la recherche. On cherche à optimiser la fonction de recherche et les fonctions insertion, suppression ne sont pas forcément optimiser.}
	
	\noindent\emph{Type abstrait}: \textbf{dictionnaire}: \textsf{insert}, \textsf{delete}, \textsf{find}
	
	\subsection{Les types concrets arborescents}
	
	\begin{definition}
		Un arbre binaire de recherche \cite[p.267]{Cormen} est un arbre binaire est un arbre dont les valeurs dans le sous-arbre droit d'un noeud dont inférieures à celui-ci et dont les valeurs dans le sous-arbre gauche sont supérieures.
	\end{definition}
	
	\noindent\emph{Implémentation} : Quelque soit le type concret, \textsf{find} consiste à descendre dans l'arbre tant qu'on n'a pas trouver la valeur que l'on recherche : on va à droite si la valeur recherchée est plus grande que le noeud courant et à gauche sinon. Pour \textsf{insert} et \textsf{delete} cela dépendant de la structure de données \textblue{(c'est à ce moment que nous maintenons la structure ordonnée afin d'optimiser la recherche selon nos critères)}.
	
	\begin{prop}
		Dans le pire cas, toutes ses opérations se fond en $O(h)$ où $h$ est la hauteur de l'arbre.
	\end{prop}
	
	Les arbres de recherche équilibré : AVL \cite[p.310]{Cormen}
	
	\begin{center}
		\begin{tabular}{|c|l|}
			\hline
			\textsf{insert} et \textsf{delete} & analogue au \textsf{find} mais on peut le déséquilibrer \\
			\hline
			Rééquilibrage & rotations gauche et droite (Figures~\ref{fig:Droit} et~\ref{fig:Gauche}) \\
			\hline
			Complexité & $\Theta(\log n)$ \\
			\hline
		\end{tabular}
	\end{center}
	
	Les arbres optimaux \cite[p.368]{Cormen}. Lorsque les recherches sur les données ne sont pas uniforme, un arbre non équilibré qui laisse accessible les clés les plus cherchées près de la racine. Un type concret est alors un arbre optimal construit aléatoirement suivant la fréquence de la recherche.
	
	\begin{prop}
		La création d'un arbre se fait en $\Theta(n^3)$ où $n$ est le nombre de clés.
	\end{prop}
	
	\subsection{Un type concret séquentiel : la table de hachage \cite[p.235]{Cormen}}
	
	\begin{definition}
		Une table de hachage est une généralisation d'un tableau où la case de stockage d'une donnée est donnée par une fonction souvent surjective.
	\end{definition}
	
	\begin{definition}
		Une collision entre deux données implique qu leur valeurs de hachage (par la fonction) sont égales.
	\end{definition}
	
	\begin{center}
		\begin{tabular}{|c|ll|}
			\hline
			\multirow{3}{*}{\emph{Type concret}} & \multicolumn{2}{l|}{tableau de la taille de l'espace d'arriver de la fonction de hachage} \\
			& \multicolumn{2}{l|}{(souvent plus petit que celui de départ) et} \\
			& \multicolumn{2}{l|}{une liste simplement chaînée pour chacune de ces cellules} \\
			\hline
			\multirow{3}{*}{\emph{Implémentation}} &  \textsf{insert} & calcul du hasch et insertion dans la bonne liste\\
			& \textsf{delete} & calcul du hasch et suppression dans la liste correspondante \\
			& \textsf{find} & calcul du hasch et recherche dans la liste correspondante\\
			\hline
		\end{tabular}
	\end{center}
	
	\textblue{Les problématiques autours du hachage parfait ne sont pas nécessaire dans cette leçon même si elles ne sont pas hors sujet. Maintenant, c'est une notion à garder en tête dès que l'on parle de hachage (même si le terme n'apparaît pas dans la leçon).}
	
	\subsection{Stoker et manipuler des données en grands nombres : la base de données relationnelle}
	
	\noindent\emph{Objectif} : On souhaite une structure de données qui permet de stocker et de manipuler des données en grand nombre efficacement \textblue{(ces données ne peuvent pas être entièrement stockées sur un disque dur)}. Elle doit donc gérer un stockage externe efficace.
	
	\textblue{Dans ce contexte, on cherche à minimiser le nombre d'accès au disque car cet accès est vraiment très lent (facteur d'un million).}
	
	\begin{center}
		\begin{tabular}{|c|ll|}
			\hline
			\emph{Type abstrait} & \textbf{Base de données relationnelle} & \textsf{create}, \textsf{find}, \textsf{insert}, \textsf{delete}  \\
			\hline
			\emph{Application} & \multicolumn{2}{l|}{recherche sur des données stockées sur un disque externe (algorithme~\ref{algo:B-arbre_recherche})} \\
			\hline
			\emph{Types concrets} & \multicolumn{2}{l|}{B-arbres} \\
			\hline
		\end{tabular}
	\end{center}
	
	\begin{definition}
		Un B-arbre $T$ possède les propriétés suivantes:
		\begin{itemize}
			\item chaque noeud possède les attributs suivants:
			\begin{itemize}
				\item $x.n$ le nombre de clés;
				\item $x.cle_1 \leq \dots \leq x.cle_n$ les $n$ clés;
				\item $x.feuille$ qui indique si le noeud est une feuille;
				\item $x.c_i$ les $n + 1$ pointeurs vers ses fils (s'il n'est pas une feuille);
			\end{itemize}
			\item si $k_i$ est stocké dans l'arbre de racine $x.c_i$ alors $x.c_{i-1} \leq k_i \leq x.c_i$;
			\item les feuilles sont toutes à la même hauteur;
			\item $t$ est le degré minimal de l'arbre $T$ : $t-1 \leq x.n \leq 2t-1$ pour tout noeud de l'arbre sauf la racine \textblue{(dans ce cas on a $1 \leq racine.n \leq 2t - 1$)}.
		\end{itemize}
	\end{definition}
	
	\begin{prop}
		Toutes les opérations sur un B-arbre (sauf \textsf{create}) s'effectuent en $O\left(\log_t n\right)$ où $t$ est le degré minimal de l'arbre et $n$ le nombre de données.
	\end{prop}
	
	\textblue{Dans la propriété on garde le $t$ car dans la vrai vie le $t$ est très grand (de l'ordre du millier) ce qui nous donne une hauteur de l'arbre très petit (de l'ordre de l'unité). Donc il influence grandement la constante.}
	
	\textblue{Dans cette leçon, il n'est pas nécessaire de parler d'arbre B+ mais il peut être intéressant de les garder en tête. Ils viennent généraliser les B-arbre dans le sens où ils permettent d'effectuer les opérations de recherche d'intervalles efficacement. Pour cela, on les construit comme les B-arbres sauf qu'on envoie une copie des clés présentes dans l'arbre dans ses feuilles.}
	
	\section{Partitionner un ensemble : Union-Find \cite[p.519]{Cormen}}
	
	\textblue{La structure d'union-find est une structure permettant de représenter un ensemble de données à l'aide d'une partition. C'est une des rares structures de données qui cherche à optimiser l'union de deux structures et la recherche. Notons que la structure de donnée générale présentée en début de leçon met des gyrophares sur cette structure de données.}
	
	\noindent\emph{Objectif}: On veut une structure de données constituées d'un ensemble de données permettant de savoir (efficacement) à quelle structures (vues comme des classes d'équivalences) chaque données intervient et unifier ces structures.
	
	\begin{center}
		\begin{tabular}{|c|ll|}
			\hline
			\emph{Type abstrait} & \textbf{Union-find} & \textsf{create}, \textsf{find}, \textsf{union}  \\
			\hline
			\multirow{2}{*}{\emph{Application}} & \multicolumn{2}{l|}{algorithme de Kruskal (les arbres couvrants de poids minimal) (algorithme~\ref{algo:Kruskal})} \\
			& complexité & $O(|E| \log |V|)$ optimal (avec structure union-find optimale) \\
			\hline
			\multirow{2}{*}{\emph{Types concrets}} & \multicolumn{2}{l|}{listes chaînées et union par rang} \\
			& \multicolumn{2}{l|}{forêts et union par rang, compression de chemin} \\
			\hline
		\end{tabular}
	\end{center}
	
	\emph{Hypothèse}: La structure union-find ne touche pas aux données : l'ensemble des données admettent un pointeur (et réciproquement) vers leur copie dans union-find.
	
	\begin{prop}
		Une séquence de $m$ opérations \textsf{find}, \textsf{create} et \textsf{union} telle qu'il y ait $n$ opérations \textsf{create} se fait en $O\left(n+m\right)$ pour le type concret liste chaînée avec union par rang.
	\end{prop}
	
	\begin{prop}
		Une séquence de $m$ opérations \textsf{find}, \textsf{create} et \textsf{union} telle qu'il y ait $n$ opérations \textsf{create} se fait en $O\left(m\alpha(n)\right)$ pour le type concret forêt avec union par rang et compression de chemin.
	\end{prop}
	
	\begin{algorithm}
		entrée : $G = (V, E), w$; sortie : $L$ un ensemble contenant les arêtes formant l'arbre couvrant minimal
		\begin{algorithmic}[1] 
			\State $L \gets \emptyset$
			\For{$v \in V$}
			\State \textsf{create}($v$)
			\EndFor
			\State Trier les arêtes de $E$ par ordre croissant de poids
			\For{$(u, v) \in E$ pris par ordre croissant}
			\If{\textsf{find}($u$) $\neq$ \textsf{find}($v$)}
			\State $L \gets L \cup \{(u, v)\}$
			\State \textsf{union}($u$, $v$)
			\EndIf
			\EndFor
			\State Renvoie $L$
		\end{algorithmic}
		\caption{Algorithme de Kruskal pour les arbres couvrant minimaux.}
		\label{algo:Kruskal}
	\end{algorithm}	
	
	\section*{Quelques notions importantes}
	
	\subsection*{La file de priorité}
	\input{./../Notions/FilesPriorite.tex}
	
	\subsection*{AVL}
	\input{./../Notions/AVL.tex}
	
	\subsection*{Arbres rouge-noirs}
	\input{./../Notions/ArbreRougeNoir.tex}
	
	\subsection*{B-arbres}
	\input{./../Notions/BArbre.tex}
	
	\subsection*{Les arbres splay}
	\input{./../Notions/ArbreSplay.tex}
	
	
	
	\bibliographystyle{plain}
	\bibliography{./../../Livre}

	
\end{document}