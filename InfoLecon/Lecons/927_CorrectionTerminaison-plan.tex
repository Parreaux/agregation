\chapter*{Leçon 927: Exemples de preuves d'algorithmes: correction et terminaison.}
\addcontentsline{toc}{chapter}{Leçon 927: Exemples de preuves d'algorithmes: correction et terminaison.}
	
\titlebox{blue}{\textblue{Références pour la leçon}}{
	\textblue{\cite{Carton}} Carton, \emph{Langages formels, calculabilité et complexité.}
	
	\textblue{\cite{Cormen}} Cormen, \emph{Algorithmique.}
	
	\textblue{\cite{NielsonNielson}} Nielson et Nielson, \emph{Semantics with applications.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.5\textwidth}
		Correction de l'algorithme de Dijkstra
	\end{minipage} \hfill
	\begin{minipage}{.45\textwidth}
		Complétude de la logique de Hoare
	\end{minipage} 
}
	
\section*{Motivation}

\subsection*{Défense}

Lors de l'écriture d'un programme, savoir s'il termine et s'il est correcte est une question légitime et difficile. C'est pourtant l'essence de l'informatique: l'étude de ces objets (comme les nombres pour les mathématiques). En effet, par les théorèmes d'indécidabilité du problème de l'arrêt et de Rice nous savons que répondre à ces questions est indécidable. Leur automatisation ne peut être complète et pour la plus part des problèmes nous devons prouver au moins une partie de ces résultats à la main: ce qui peut être long, fastidieux et même délicat.

\subsection*{Ce qu'en dit le jury}

Le jury attend du candidat qu’il traite des exemples d’algorithmes récursifs et des exemples d’algorithmes itératifs.

En particulier, le candidat doit présenter des exemples mettant en évidence l’intérêt de la notion d’invariant pour la correction partielle et celle de variant pour la terminaison des segments itératifs.

Une formalisation comme la logique de Hoare pourra utilement être introduite dans cette leçon, à condition toutefois que le candidat en maîtrise le langage. Des exemples non triviaux de correction d’algorithmes seront proposés. Un exemple de raisonnement type pour prouver la correction des algorithmes gloutons pourra éventuellement faire l’objet d’un développement.

\section*{Métaplan}
\begin{footnotesize}
	\begin{itemize}[label=$\rightsquigarrow$]
		\item \emph{Motivation}: Étude des programmes : essence de la science informatique.
		\item \emph{Motivation}: Savoir si un programme termine ou s'il est correct est une notion importante.
		\item \emph{Motivation}: Indécidabilité de l'automatisation.
		\item \emph{Motivation}: Automatisation de la vérification: vérifier pas simple (souvent) long et fastidieux.
		\item \emph{Motivation}: Programmer c'est prouver (correspondance de Curry--Howard).
		
		\begin{minipage}{.4\textwidth}
			\item \emph{Définition}: Spécification
		\end{minipage} \hfill
		\begin{minipage}{.4\textwidth}
			\item \emph{Remarque}: Le plus difficile à donner
		\end{minipage}
	\end{itemize}
\end{footnotesize}

\begin{description}
	\item[I.] \textbf{Terminaison}
	\begin{small}
		\textblue{Le théorème de l'arrêt et son indécidabilité pose les limites de l'algorithmique: on ne peut obtenir un algorithme qui permettent de prouver la terminaison de tous les algorithmes. On étudie alors quelques techniques que nous utilisons à la main. Gardons en tête la preuve des programmes parallèles.}
	\end{small}
	\begin{footnotesize}
		\begin{itemize}[label=$\rightsquigarrow$]
			\begin{minipage}{.4\textwidth}
				\item \emph{Définition}: Terminaison d'un algorithme
				\item \emph{Exemple}: Terminaison et boucle \textsc{Tant que}
				\item \emph{Théorème}~\cite{Carton}: \textsc{Arrêt} est indécidable
			\end{minipage} \hfill
			\begin{minipage}{.45\textwidth}
				\item \emph{Remarque}: Boucle \textsc{Pour} préserve la terminaison
				\item \emph{Définition}: Problème \textsc{Arrêt}
				\item \emph{Exemple}: Syracuse
			\end{minipage}
			\item \emph{Conséquence}: Pas de procédure effective mais heuristique pour certaine classe
		\end{itemize}
	\end{footnotesize}
	\begin{description}
		\item[A.] \emph{Ensemble bien fondé}
		\begin{small}
			\textblue{La notion d'ensemble bien fondé est un outil puissant pour montrer la terminaison.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.3\textwidth}
					\item \emph{Définition}: Ensemble bien fondé
				\end{minipage} \hfill
				\begin{minipage}{.5\textwidth}
					\item \emph{Exemple}: $\N$
				\end{minipage}
				
				\begin{minipage}{.2\textwidth}
					\item \emph{Contre-exemple}: $\Z$
				\end{minipage} \hfill
				\begin{minipage}{.6\textwidth}
					\item \emph{Proposition}: $\N^2$ muni de l'ordre lexicographique est bien fondé
				\end{minipage}
				\item \emph{Proposition}: Caractérisation de l'ordre lexicographique
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Terminaison des algorithmes récursifs}
		\begin{small}
			\textblue{Dans le cas des algorithmes récursifs, l'ensemble bien fondé suffit pour montrer la terminaison de tels algorithmes.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.5\textwidth}
					\item \emph{Théorème}: Terminaison des algorithmes récursifs
					\item \emph{Exemple}: Calcul du pgcd via l'algorithme d'Euclide
				\end{minipage} \hfill
				\begin{minipage}{.3\textwidth}
					\item \emph{Exemple}: Fonction d'Ackermann
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Terminaison des algorithmes itératifs}
		\begin{small}
			\textblue{Dans le cas des algorithmes itératifs, la notion d'ensemble bien fondé ne s'applique pas directement: on utilise la notion d'invariant de boucle.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.5\textwidth}
					\item \emph{Définition}: Variant de boucle
					\item \emph{Théorème}: Terminaison des algorithmes itératifs
				\end{minipage} \hfill
				\begin{minipage}{.3\textwidth}
					\item \emph{Exemple}: Division euclidienne
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[II.] \textbf{Correction}
	\begin{small}
		\textblue{Le théorème de Rice et son résultat d'indécidabilité pose les limites de l'algorithmique: on ne peut obtenir un algorithme qui permettent de prouver la correction de tous les algorithmes. On étudie alors quelques techniques que nous utilisons à la main. De plus, via la correspondance de Curry--Howard, on remarque que programmer c'est prouver et donc programmer c'est prouver la correction.}
	\end{small}
	\begin{footnotesize}
		\begin{itemize}[label=$\rightsquigarrow$]
			\item \emph{Idée}: Le programme (algorithme) vérifie se spécification
			\item \emph{Définition}: Correction (totale et partielle)
			\item \emph{Exemple}: Tri topologique est totalement correct
			\item \emph{Exemple}: Syracuse est partiellement correct
		\end{itemize}
	\end{footnotesize}
	\begin{description}
		\item[A.] \emph{Correction des algorithmes récursifs}
		\begin{small}
			\textblue{Commençons par l'étude de la correction des algorithmes récursifs.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.45\textwidth}
					\item \emph{Proposition}: Vérification par les prédicats
					\item \emph{Théorème}: Correction des algorithmes récursifs
				\end{minipage} \hfill
				\begin{minipage}{.45\textwidth}
					\item \emph{Exemple}: L'algorithme du pgcd est correct
					\item \emph{Exemple}: Correction du tri par insertion
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Correction des algorithmes itératifs}
		\begin{small}
			\textblue{Pour les algorithmes itératifs, nous pouvons utiliser la notion d'invariant de boucle. Cet invariant permet de prouver la correction des boucles: ce sont des étapes vers la correction de l'algorithme.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.45\textwidth}
					\item \emph{Définition}: Invariant de boucle
					\item \emph{Théorème}: Utilisation des invariants de boucles
				\end{minipage} \hfill
				\begin{minipage}{.35\textwidth}
					\item \textred{\emph{Exemple}: Correction de Dijkstra}
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Programmer c'est prouver}
		\begin{small}
			\textblue{La correspondance de Curry--Howard, notamment, nous dit que programmer c'est prouver et que cette preuve est en réalité la correction d'un énoncé logique (comme une spécification peut être vue comme une formule logique, on prouve le programme).}
		\end{small}
		
		\begin{footnotesize}
			\textbf{Preuve avant l'algorithme} \textblue{Dans certains cas la preuve de correction d'un algorithme vient par l'analyse de ce problème et de ces propriétés. Plus exactement, on en tire un algorithme. Dans d'autres cas, les structures de données que nous utilisons donne la correction de l'algorithme.}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Méthode}: Analyse du problème
					\item \emph{Exemple}: Alignement optimaux
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Application}: Programmation dynamique
					\item \emph{Méthode}: Structure de données
				\end{minipage}
				\item \emph{Exemple}: Tri par tas
			\end{itemize}
		\end{footnotesize}
		
		\begin{footnotesize}
			\textbf{La correspondance de Curry--Howard} \textblue{Curry et Howard ont prouvé grâce au $\lambda$-calcul simplement typé et la logique intuitionniste, ils prouvent que programmer c'est prouver. Ici, on ne évoque simplement quelques faits culturels autours de ce paradigme.}
			\begin{itemize}[label=$\rightsquigarrow$]
				\item \emph{Théorème}: Correspondance de Curry--Howard
				\item \emph{Application}: Requête SQL dans une base de données
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[III.] \textbf{Automatisation}
	\begin{small}
		\textblue{Même si l'automatisation de telles preuves est indécidable, nous souhaitons en automatiser une partie. Pour la correction la logique de Hoare s'avère être un outils puissant. Nous allons la définir pour un langage jouet IMP muni de sa sémantique naturelle (nous savons que la sémantique à petits pas et la sémantique dénotationnelles sont équivalentes à celle-ci pour ce langage). Nous pouvons donc choisir n'importe laquelle mais la naturelle est la plus simple pour faire ce que l'on souhaite.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Langage IMP \cite[p.7]{NielsonNielson}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Langage IMP
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Exemple}: Factorielle
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Sémantique naturelle \cite[p.20]{NielsonNielson}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Règle de la sémantique naturelle
					\item \emph{Définition}: Fonction déterministe
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Théorème}: Cette sémantique est déterministe
					\item \emph{Exemple}: Arbre de dérivation de factorielle
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Logique de Hoare \cite[p.213]{NielsonNielson}}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.44\textwidth}
					\item \emph{Définition}: Triplet de Hoare
					\item \emph{Définition}: Conséquence sémantique et preuve
					\item \emph{Définition}: wlp
				\end{minipage} \hfill
				\begin{minipage}{.38\textwidth}
					\item \emph{Définition}: Règle de la logique de Hoare
					\item \emph{Théorème}: Correction de la logique
					\item \textred{\emph{Théorème}: Complétude de la logique}
				\end{minipage}
				\item \emph{Exemple}:Factorielle par la logique de Hoare
				\item \emph{Remarque}: Rôle du wlp dans l'automatisation
			\end{itemize}
		\end{footnotesize}
	\end{description}
\end{description}