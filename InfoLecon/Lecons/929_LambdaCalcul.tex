\documentclass[a4paper,11pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais,english]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}
\usepackage{subcaption}

\usepackage{algorithm}
\usepackage{algpseudocode}%

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{listings}


\parskip=0\baselineskip %0.5 d'habitude
\usetikzlibrary{automata,positioning,shadows}
\usetikzlibrary{calc, shapes, backgrounds,arrows}

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{cex}{Contre-exemple}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	
	\title{Leçon 929: Le lambda-calcul comme modèle de calcul pur. Exemples.}
	
	\date{2018 - 2019}
	\author{Julie Parreaux}
	
	\maketitle
	
	\titlebox{blue}{Références pour la leçon}{
		\textblue{\cite{Hankin}} Hankin, \emph{Lambda Calculi: A guide for Computer Scientists.}
		
		\textblue{\cite{HindleySeldin}} Hindley et Seldin, \emph{Lambda Calculus and Combinators: an Introduction.}
	}
	
	\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
		\begin{minipage}{.5\textwidth}
			$\mu$-récursivité $\Rightarrow$ $\lambda$-définissable
		\end{minipage} \hfill
		\begin{minipage}{.45\textwidth}
			Théorèmes de Scott--Curry et Rice
		\end{minipage} 
	}
	
	\renewcommand{\contentsname}{Plan de la leçon}
	\tableofcontents
	
	\section*{Motivation}
	
	\subsection*{Défense}
	
	\subsection*{Ce qu'en dit le jury}

	Il s’agit de présenter un modèle de calcul : le lambda-calcul pur. Il est important de faire le lien avec au moins un autre modèle de calcul, par exemple les machines de Turing ou les fonctions récursives. Néanmoins, la leçon doit traiter des spécificités du lambda-calcul. Ainsi le candidat doit motiver l’intérêt du lambda-calcul pur sur les entiers et pourra aborder la façon dont il permet de définir et d’utiliser des types de données (booléens, couples, listes, arbres).

	\section*{Introduction}
	\addcontentsline{toc}{section}{Introduction}
	
	\begin{itemize}
		\item Introduction d'une logique alternative à la théorie des ensembles par Alonzo Church.
		\item Modéliser et formaliser les fonctions récursives via le calcul qui est omniprésent.
		\item Définir la sémantique des langages purement fonctionnels.
	\end{itemize}
	
	\section{Le $\lambda$-calcul: présentation du modèle}
	\textblue{Le $\lambda$-calcul est un modèle de calcul dont la syntaxe utilise les $\lambda$. On présentera une manière de calculer à l'aide de ce modèle: la $\beta$-réduction. Pour finir, on évoque leur lien avec les langages de programmation.}
	
	\subsection{La syntaxe du $\lambda$-calcul}
	\textblue{La syntaxe du $\lambda$-calcul nous permet de définir l'équivalence via une substitution.}
	\begin{itemize}
		\item \emph{Définition}: Langage du $\lambda$-calcul
		\item \emph{Exemple}: Un $\lambda$-terme
		\item \emph{Remarque}: Non-ambiguïté des $\lambda$-termes
		\item \emph{Définition}: Sous-terme sous une partie
		\item \emph{Définition}: Variable libre et lié
		\item \emph{Exemple}: Variable libre et lié dans ce terme
		\item \emph{Définition}: Terme clos
		\item \emph{Définition}: Longueur d'un terme \textblue{(nécessaire pour la définition suivante)}
		\item \emph{Définition}: Substitution
		\item \emph{Définition}: $\alpha$-conversion \textblue{(relation d'équivalence définie par induction permettant de rassembler les $\lambda$-termes représentant le même calcul)}
	\end{itemize}
	
	\subsection{La $\beta$-réduction}
	\textblue{La $\beta$-réduction nous permet de calculer grâce aux $\lambda$-terme (ce n'est pas la sémantique du $\lambda$-calcul en tant que logique mais sa sémantique calculatoire). Elle correspond à une exécution d'un programme.}
	\begin{itemize}
		\item \emph{Définition}: $\beta$-réduction
		\item \emph{Définition}: Forme normale
		\item \emph{Remarque}: On n'a pas toujours existence d'une telle forme
		\item \emph{Théorème}: Théorème de Church--Rosser
		\item \emph{Corollaire}: Unicité de la forme normale
		\item \emph{Définition}: $\beta$-équivalence
		\item \emph{Théorème}: Théorème de Church--Rosser
		\item \emph{Définition}: Combinateur de point fixe
	\end{itemize}
	
	\subsection{Stratégies de réduction et lien avec les langages de programmation}
	\textblue{Le $\lambda$-calcul est le cœurs de langages de programmation fonctionnel comme LISP ou Haskel. Cependant, une stratégie de réduction est nécessaire et donne le type de langage et ses spécificités.}
	\begin{itemize}
		\item \emph{Hypothèse}: La restriction de la $\beta$-réduction \textblue{(expressivité?)}
		\item \emph{Définition}: $\lambda$-abstraction
		\item \emph{Remarque}: Lien entre $\lambda$-abstraction et forme normale
		\item \emph{Définition}: Réduction par nom
		\item \emph{Application}: LISP
		\item \emph{Définition}: Réduction par valeurs
		\item \emph{Remarque}: Peu efficace sauf mémoïsation
		\item \emph{Application}: Haskel
	\end{itemize}
	
	\section{Un modèle de calcul puissant}
	\textblue{Le $\lambda$-calcul est un modèle de calcul puissant puisqu'il est aussi expressif que les machines de Turing. De plus, pour pouvoir implémenter des programmes dans les langages de programmation, il nous faut pouvoir implémenter des structures des données grâce à ce modèle de calcul.}
	
	\subsection{Encoder des données}
	\textblue{Le $\lambda$-calcul permet d'encoder différentes structures de données nécessaire à la programmation. Il est important de faire passer l'intuition sur ces modèles.}
	\begin{itemize}
		\item \emph{Structure de données}: Booléen
		\item \emph{Application}: Conditionnelle
		\item \emph{Structure de données}: Paire
		\item \emph{Structure de données}: Liste
		\item \emph{Application}: Fonction récursive
		\item \emph{Structure de données}: Entier de Church \textblue{(les entiers sont représenter par le travail)}
		\item \emph{Structure de données}: Entier de Barendregt \textblue{(les entiers sont implémentés pas les pairs)}
	\end{itemize}
	
	\subsection{Thèse de Church}
	\textblue{La thèse de Church est vérifiée par ce modèle de calcul. Nous avons donc une équivalence aux fonctions $\mu$-récursives et aux machines de Turing.}
	\begin{itemize}
		\item \emph{Définition}: Fonction $\lambda$-définissable
		\item \emph{Définition}: Fonction $\mu$-récursives
		\item \emph{Théorème}: Fonction $\lambda$-définissable $\Leftrightarrow$ fonction $\mu$-récursive \textred{(DEV $\Leftarrow$)}
		\item \emph{Théorème}: Fonction $\mu$-récursives $\Leftrightarrow$ fonction calculable par une machine de Turing
		\item \emph{Corollaire}: Fonction $\lambda$-définissable $\Leftrightarrow$ fonction calculable par une machine de Turing
		\item \emph{Application}: Thèse de Church
	\end{itemize}
	
	\subsection{Théorie de la décidabilité}
	\textblue{Grâce à la thèse de Church nous nous ouvrons les portes de la théorie de la décidabilité}
	\begin{itemize}
		\item \emph{Définition}: Séparabilité
		\item \emph{Théorème}: Théorème de Scott--Curry \textred{(DEV)}
		\item \emph{Définition}: Fonction non triviale
		\item \emph{Corollaire}: Théorème de Rice \textred{(DEV)}
		\item \emph{Application}: Indécidabilité de l'existence d'une forme normale
	\end{itemize}
	
	\section*{Ouverture}
	\addcontentsline{toc}{section}{Ouverture}
	La correspondance de Curry--Howard pour le $\lambda$-calcul simplement typé.
	
	\bibliographystyle{plain}
	\bibliography{./../../Livre}
	
\end{document}