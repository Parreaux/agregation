\documentclass[a4paper,11pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais,english]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}
\usepackage{subcaption}

\usepackage{algorithm}
\usepackage{algpseudocode}%

\usepackage{multirow}
\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{listings}


\parskip=0\baselineskip %0.5 d'habitude
\usetikzlibrary{automata,positioning,shadows}
\usetikzlibrary{calc, shapes, backgrounds,arrows}

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\newtheoremstyle{myRemark}% name of the style to be used
	{\topsep}% measure of space to leave above the theorem. E.g.: 3pt
	{\topsep}% measure of space to leave below the theorem. E.g.: 3pt
	{\footnotesize}% name of font to use in the body of the theorem
	{0pt}% measure of space to indent
	{\itshape}% name of head font
	{: }% punctuation between head and body
	{ }% space after theorem head; " " = normal interword space
	{\thmname{#1}\thmnumber{ #2}\thmnote{ (#3)}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{cex}{Contre-exemple}
	\theoremstyle{myRemark}
	\newtheorem*{req}{Remarque}
	
	\title{Leçon 915: Classes de complexité. Exemples}
	
	\date{2018 - 2019}
	\author{Julie Parreaux}
	
	\maketitle
	
	\titlebox{blue}{Références pour la leçon}{
		\textblue{\cite{Carton}} Carton, \emph{Langages formels, calculabilité et complexité.}
		
		\textblue{\cite{Cormen}} Cormen, \emph{Algorithmique.}
		
		\textblue{\cite{FloydBiegel}} Floyd et Biegel, \emph{Le langage des machines.}
		
		\textblue{\cite{KleinbergTardos}} Kleinberg et Tardos, \emph{Algorithm design.}
		
		\textblue{\cite{Papadimitriou}} Papadimitriou, \emph{Comptutational complexity.}
		
		\textblue{\cite{Sisper}} Sipser, \emph{Introduction to the Theory of Computation.}
	}
	
	\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
		\begin{minipage}{.5\textwidth}
			Le problème PSA est NP-complet
		\end{minipage} \hfill
		\begin{minipage}{.45\textwidth}
			Théorème de Savitch
		\end{minipage} 
	}
	
	\renewcommand{\contentsname}{Plan de la leçon}
	\tableofcontents
	
	\section*{Motivation}
	
	\subsection*{Défense}
	
	Une première classification des problèmes en informatique est sa décidabilité ou non. Cependant, cette classification est grossière et une question naturelle a été de classifier les problème décidable en fonction de leur dureté: l'accessibilité dans un graphe et Presburger ce n'est pas tout à fait le même problème. 
	
	Les machines de Turing sont un outils permettant de réaliser cette classification : elles permettent de définir la complexité d'un problème. Nous allons donc étudier les classes de complexité en temps et en espace (les familles de problèmes qui ont des complexités équivalentes) et leurs hiérarchies. Remarquons que nous nous intéressons à la complexité d'un problème et non d'un algorithme et même si les deux notions sont très liées, l'étude précise d'une implémentation ne nous intéresse pas (on souhaite uniquement mettre le problème dans une case). 
	
	\subsection*{Ce qu'en dit le jury}
	
	Le jury attend que le candidat aborde à la fois la complexité en temps et en espace. Il faut naturellement exhiber des exemples de problèmes appartenant aux classes de complexité introduites, et montrer les relations d’inclusion existantes entre ces classes, en abordant le caractère strict ou non de ces inclusions. Le jury s’attend à ce que les notions de réduction polynomiale, de problème complet pour une classe, de robustesse d’une classe vis à vis des modèles de calcul soient abordées.
	
	Se focaliser sur la décidabilité dans cette leçon serait hors sujet.
	
	\section*{Introduction}
	\addcontentsline{toc}{section}{Introduction}
	
	\noindent\emph{Motivation}: Classer les problème décidable en fonction de leur difficulté \textblue{(la difficulté est de définir la difficulté et la notion de problème plus dur qu'un autre)}.
	
	\noindent\emph{Cadre}: Problème décidable
	
	\noindent\emph{Pré-requis}: Machine de Turing (déterministe, non déterministe, à plusieurs rubans \textblue{(elles ne servent que pour $NL$ (hors programme), si on en parle pas, on peut tout faire avec un seul ruban)}), exécution, configurations
	
	\section{Classes de complexité}
	\textblue{Pour définir les classes de complexité, nous devons définir les problèmes de décision et surtout soulever le problème du codage des entiers. Ensuite, nous définissons la complexité pour notre modèle de calcul qui sert d'échelon: les machine de Turing (déterministe, non déterministe, à plusieurs ruban).}
	
	\subsection{Problèmes de décisions}
	\textblue{Un des points subtile dans la théorie de la complexité est le codage des entrées de notre problème. En effet, selon le codage choisi, la complexité de notre problème peut être différente (ou la preuve plus ou moins difficile).}
	
	\begin{itemize}
		\item \emph{Définition}: Problème de décision
		\item \emph{Problème}: Codage des entrées
		\item \emph{Exemple}: \textsf{Primes} et \textsf{UnaryPrimes}
	\end{itemize}
	
	\subsection{Complexité d'une machines de Turing}
	\textblue{La complexité d'une machine de Turing est sa consommation d'une ressource donnée lors de son exécution. Cette ressource est temporelle ou spatiale. Comme on ne se concentre que sur des problèmes décidables, on peut supposer que les exécutions de notre machine sont finies. On utilise la taille des configurations : il nous faut faire attention dans le cadre d'une machine à plusieurs rubans.}
	
	\begin{itemize}
		\item \emph{Définition}: Soient $\mathcal{M}$ une machine de Turing, $w$ une entrée et $C_w$ l'ensemble des suites de configurations des exécutions possibles sur $w$. La complexité d'une exécution $c$ en temps et en espace est $t(c) = n$ et $s(c) = \max_{0 \leq i \leq n} \left|c_i\right|$.
		\item \emph{Remarque}: Dans le cas d'une machine de Turing à plusieurs rubans, la taille des configurations ne prend pas en compte les rubans lecture ou en écriture seul dont la tête de lecture se déplace qu'à droite \textblue{(on ne compte que les rubans qui ont un rôle de mémoire)}.
		\item \emph{Définition}: Soient $\mathcal{M}$ une machine de Turing, $w$ une entrée et $C_w$ l'ensemble des suites de configurations des exécutions possibles sur $w$. La complexité pour une entrée est alors $t(w) = \max_{c \in C_w} t(c)$ et $s(w) = \max_{c \in C_w} s(c)$.
		\item \emph{Définition}: Soient $\mathcal{M}$ une machine de Turing, $w$ une entrée et $C_w$ l'ensemble des suites de configurations des exécutions possibles sur $w$. La complexité d'une machine est alors $t: n = \max_{\left|w\right| = n} t(w)$ et $s: n = \max_{\left|w\right| = n} s(w)$ \textblue{(fonction en fonction de la taille de l'entrée)}.
		\item \emph{Proposition} (Lien entre espace et temps). Soit $\mathcal{M}$ une machine de Turing, il existe $K \in \R^{+}$ tel que $s(n) \leq \max(t(n), n)$ et $t(n) \leq 2^{Ks(n)}$.
		\item \emph{Théorème}: Accélération temporelle et spatiale
	\end{itemize}
	
	\subsection{Classes de complexité}
	\textblue{Nous allons maintenant définir la complexité d'un problème de décision et les classes de complexités usuelles. On choisit des machines de Turing à une seule bande. Grâce au théorème d'accélération, on sait que les constance ne sont pas nécessaire dans l'étude de la complexité.}
	\begin{itemize}
		\item \emph{Définition}: Famille de problème en espace et en temps
		\item \emph{Définition}: Classes de complexité usuelles
		\item \emph{Remarque}: Implication de l'encodage (exemple \textsf{Primes} et \textsf{UnaryPrimes})
		\item \emph{Exemple}: Logique dans chacune des classes au programme \textblue{(on commence à poser l'idée que la logique est une bonne porte d'entrée pour résoudre des problèmes difficile).}
		\item \emph{Proposition}: Hiérarchie
		\item \emph{Définition}: Fonction constructible
		\item \emph{Proposition}: Rupture dans la hiérarchie
		\item \emph{Corollaire}: \textsf{P}$\neq$ \textsf{EXPTIME} et \textsf{PSPACE}$\neq$ \textsf{EXPSPACE}
	\end{itemize}
	
	\section{Complexité en temps: $P$ vs $NP$}
	\textblue{Nous allons nous intéresser à la complexité en temps et notamment aux classes \textsf{P} et \textsf{NP}. Nous allons étudier leur robustesse en fonction du modèle. Puis nous allons étudier la complétude et son impacte sur la question ouverte $P = NP$ ou non. Enfin, nous allons regarder au-delà avec la classe \textsf{EXPTIME}.}
	
	\subsection{Robustesse des classes temporelles}
	\textblue{Les classes temporelles sont robustes à l'ajout ou à la suppression de ruban. Cependant, elles ne le sont pas au passage au non-déterministe. Cela implique que nous ne savons toujours pas si \textsf{P} égale ou non \textsf{NP}.}
	\begin{itemize}
		\item \emph{Proposition}: Toute machine à $k \geq 2$ bandes $\mathcal{M}$ est équivalente à une machine à une bande $\mathcal{M}'$ telle que $t_{\mathcal{M}'} = O\left(\left(t_{\mathcal{M}}\right)^2\right)$.
		\item \emph{Interprétation}: L'utilisation d'une machine à plusieurs bandes n'influe pas sur la classe de complexité \textblue{(on peut donc utiliser plusieurs bandes sans soucis)}.
		\item \emph{Proposition}: Toute machine non-déterministe $\mathcal{M}$ est équivalente à une machine déterministe $\mathcal{M}'$ telle que $t_{\mathcal{M}'} = 2^{O\left(t_{\mathcal{M}}\right)}$.
		\item \emph{Interprétation}: Les classes \textsf{P} et \textsf{NP} ou \textsf{EXPTIME} et \textsf{NEXPTIME} ne sont à priori pas équivalent: on prend une exponentielle en passant de l'une à l'autre \textblue{(on ne peut donc pas déterminiser facilement sans impacter la complexité une machine non-déterministe)}.
	\end{itemize}
	
	\subsection{Notion de \textsf{NP}-complétude et conséquences théoriques}
	\textblue{Afin d'étudier les spécificités de la classe \textsf{NP}, nous allons définir une famille de problème qui sont plus durs que tous les autres de cette classe. On définit alors la notion de complétude. Dans le cas de ces classes, nous devons définir la notion de réduction polynomiale.}
	\begin{itemize}
		\item \emph{Définition}: Réduction polynomiale
		\item \emph{Définition}: Problème \textsf{NP}-dure et \textsf{NP}-complet
		\item \emph{Théorème}: Cook
		\item \emph{Application}: Montrer que d'autres problèmes sont \textsf{NP}-complet
		\item \emph{Exemple}: Le problème PSA est \textsf{NP}-complet \textred{DEV}
		\item \emph{Exemples}: $3$-coloration, set-cover, ...
		\item \emph{Corollaire}: Si il existe $p \in \text{\textsf{NP}-complet}$ et $p \in \textsf{P}$ alors $\textsf{P} = \textsf{NP}$.
		\item \emph{Corollaire}: Si $\textsf{P} \neq \textsf{NP}$ alors il existe $p \in \textsf{NP}$ tel que $p$ne soit pas complet.
	\end{itemize}
	
	\subsection{Conséquence de la \textsf{NP}-complétude en pratique}
	\textblue{Un problème \textsf{NP}-complet peut se traiter en pratique. On peut alors jouer: le temps, l'expressivité ou l'exactitude (si on a un problème d'optimisation).}
	\begin{itemize}
		\item \emph{Méthode}: Backtracking ou Branch and bound
		\item \emph{Exemple}: DPLL
		\item \emph{Méthode}: Approximation
		\item \emph{Exemple}: Approximation du problème du voyageur de commerce
		\item \emph{Méthode}: Restriction des entrées
		\item \emph{Remarque}: Baisse de l'expressivité du problème
		 \item \emph{Exemple}: Logique de Horn
	\end{itemize}
	
	\subsection{Au delà de la \textsf{NP}-complétude: la classe \textsf{EXPTIME}}
	\textblue{Les classes \textsf{P} et \textsf{NP} ne sont pas les seules classes temporelles (même si elles sont très intéressantes). Les classes \textsf{EXPTIME} et \textsf{NEXPTIME} contiennent des problèmes dont la résolution n'est pas aisée car ce sont des tours d'exponentielle. Notons qu'il existe des problèmes encore plus difficile: les problèmes non-élémentaires: qui sont \textsf{EXPTIME} pour tout $k$ de tour d'exponentielle. La généralisation de Presburer est un tel problème.}
	\begin{itemize}
		\item \emph{Exemple} \textred{(admis)}: Presburger
		\item \emph{Proposition}: Si \textsf{EXPTIME} $\neq$ \textsf{NEXPTIME} alors \textsf{P} $\neq$ \textsf{NP} \textblue{(argument de padding)}
		\item \emph{Définition}: Problèmes non élémentaires
		\item \emph{Exemple}: Presburger et généralité
	\end{itemize}
	
	\section{Complexité en espace: $PSPACE$}
	\textblue{Nous allons nous intéresser à la complexité en espace et notamment aux classes \textsf{PSPACE} et \textsf{NPSPACE}. Nous allons étudier leur robustesse en fonction du modèle. Puis nous allons étudier la complétude. Enfin, nous étudierons l'impacte de la complexité en espace dans l'étude de la classe \textsf{P}.}
	
	\subsection{Robustesse des classes spatiales}
	\textblue{Dans le cas de la complexité spatiale, les classes sont robustes si on déterminise la machine (théorème de Savitch). Cependant, elles ne le sont pas nécessairement face aux multibandes comme nous le verrons plus tard (même si ça ne change rien pour \textsf{PSPACE}).}
	\begin{itemize}
		\item \emph{Théorème}: Savitch \textred{DEV}
		\item \emph{Corollaire}: \textsf{PSPACE} = \textsf{NPSPACE}
		\item \emph{Corollaire}: \textsf{EXPSPACE} = \textsf{NEXPSPACE}
	\end{itemize}
	
	\subsection{Notion de \textsf{PSPACE}-complétude et conséquences pratiques}
	\textblue{La complétude dans \textsf{PSPACE} se défini à l'aide d'une réduction polynomiale. On peut alors définir des solveurs qui viennent résoudre l'ensemble des problèmes \textsf{PSPACE}.}
	\begin{itemize}
		\item \emph{Définition}: Problème \textsf{PSPACE}-dur et \textsf{PSPACE}-complet.
		\item \emph{Proposition}: Le problème QBF est \textsf{PSPACE}-complet.
		\item \emph{Application}: Montrer que les problèmes sont \textsf{PSPACE}-complet.
		\item \emph{Exemple}: Le problème d'universalité algébrique est \textsf{PSPACE}-complet.
		\item \emph{Remarque}: De manière analogue aux SAT-solver, on produit des QBF-solver.
	\end{itemize}
	
	\subsection{Exploration de la classe \textsf{P}}
	\textblue{La complexité spatiale permet d'explorer la classe $P$ est définissant une complexité log-space, la réduction qui va avec et les sous-classes qu'on en déduit. Nous définissons alors la classe \textsf{NL}, la \textsf{NL}-complétude et la \textsf{P}-complétude avec la réduction log-space.}
	\begin{itemize}
		\item \emph{Définition}: Classes \textsf{NL} et \textsf{L}
		\item \emph{Exemple}: Accessibilité dans un graphe est dans \textsf{NL} 
		\item \emph{Définition}: Log-space réduction
		\item \emph{Définition}: Problème \textsf{NL}-dur et \textsf{NL}-complet
		\item \emph{Proposition}: Le problème d'accessibilité dans un graphe est \textsf{NL} -complet
		\item \emph{Application}: Montrer que d'autre problème le sont.
		\item \emph{Exemple}: Le problème 2-\textsc{SAT} est \textsf{NL}-complet
		\item \emph{Remarque}: Théorème de Savitch
		\item \emph{Définition}: Problème \textsf{P}-dur et \textsf{P}-complet
		\item \emph{Proposition}: Le problème Horn est \textsf{P}-complet
	\end{itemize}
	
	
	\section{Hiérarchie}
	\textblue{Nous pouvons conclure avec le dessin de la hiérarchie telle que nous la connaissons aujourd'hui.}
	\begin{itemize}
		\item Conclusion sur la hiérarchie des classes
	\end{itemize}
	
	\section*{Ouverture}
	\addcontentsline{toc}{section}{Ouverture}
	On utilise les machines de Turing pour effectuer l'échelle de notre classification. Cependant, il existe d'autre modèle de calcul nous permettant d'étendre, de compléter, de raffiner ou de changer de points de vu sur cette classification.
	\begin{description}
		\item[Machines de Turing alternantes] En fonction de leur degrés d'alternance, elles définissent la hiérarchie polynomiale qui étend les classes $P$, $NP$ et $co-NP$.
		\item[Circuits] Les classes $AC_i$ (classe de problèmes résolus à l'aide d'un circuit de taille polynomiale (nombre de porte) et de profondeur $O\left(\log^i n\right)$) de la hiérarchie $AC$ comme $AC_0 \subset NL$ dont on sait l'inclusion stricte dans $P$.
		\item[Machines de Turing à oracle] Permet de classer les problèmes dans \textsf{EXPTIME} et même dans les problèmes indécidables.
		\item[Logique] La logique permet de définir des classes de complexité: c'est ce que nous appelons la complexité descriptive. Par exemple la classe $NP$ correspond à la classe des problèmes exprimables en la logique du second ordre existentielle, c'est-à-dire la logique du second ordre où on interdit de quantifier universellement sur les prédicats et fonctions.
	\end{description}
	
	\section*{Quelques notions importantes}
	
	\subsection*{Réductions}
	\input{./../Notions/Indecidabilite_reduction.tex}
	
	\subsection*{Le théorème de Cook}
	\input{./../Notions/NPC_Cook.tex}

	\bibliographystyle{plain}
	\bibliography{./../../Livre}
	
	
\end{document}