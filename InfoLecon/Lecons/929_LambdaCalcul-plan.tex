\chapter*{Leçon 929: Le lambda-calcul comme modèle de calcul pur. Exemples.}
\addcontentsline{toc}{chapter}{Leçon 929: Le lambda-calcul comme modèle de calcul pur. Exemples.}

\titlebox{blue}{\textblue{Références pour la leçon}}{
	\textblue{\cite{Hankin}} Hankin, \emph{Lambda Calculi: A Guide for Computer Scientists.}
	
	\textblue{\cite{HindleySeldin}} Hindley et Seldin, \emph{Lambda-Calculus and Combinators: An Introduction.}
}

\titlebox{PineGreen}{\textgreen{Développements de la leçon}}{
	\begin{minipage}{.5\textwidth}
		$\mu$-récursivité $\Rightarrow$ $\lambda$-définissable
	\end{minipage} \hfill
	\begin{minipage}{.45\textwidth}
		Théorèmes de Scott--Curry et Rice
	\end{minipage} 
}

\section*{Motivation}

\subsection*{Défense}

\subsection*{Ce qu'en dit le jury}

Il s’agit de présenter un modèle de calcul : le lambda-calcul pur. Il est important de faire le lien avec au moins un autre modèle de calcul, par exemple les machines de Turing ou les fonctions récursives. Néanmoins, la leçon doit traiter des spécificités du lambda-calcul. Ainsi le candidat doit motiver l’intérêt du lambda-calcul pur sur les entiers et pourra aborder la façon dont il permet de définir et d’utiliser des types de données (booléens, couples, listes, arbres).

\section*{Métaplan}

\begin{footnotesize}
	\begin{itemize}[label=$\rightsquigarrow$]
		\item Introduction d'une logique alternative à la théorie des ensembles par Alonzo Church.
		\item Modéliser et formaliser les fonctions récursives via le calcul qui est omniprésent.
		\item Définir la sémantique des langages purement fonctionnels.
	\end{itemize}
\end{footnotesize}

\begin{description}
	\item[I.] \textbf{Le $\lambda$-calcul: présentation du modèle}
	\begin{small}
		\textblue{Le $\lambda$-calcul est un modèle de calcul dont la syntaxe utilise les $\lambda$. On présentera une manière de calculer à l'aide de ce modèle: la $\beta$-réduction. Pour finir, on évoque leur lien avec les langages de programmation.}
	\end{small}
	\begin{description}
		\item[A.] \emph{La syntaxe du $\lambda$-calcul}
		\begin{small}
			\textblue{La syntaxe du $\lambda$-calcul nous permet de définir l'équivalence via une substitution: cette relation d'équivalence définie par induction permettant de rassembler les $\lambda$-termes représentant le même calcul. Avant d'introduire la substitution, nous sommes obligé d'introduire la notion de longueur d'un terme car c'est avec cette notion que nous montrons que tout est bien défini.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Langage du $\lambda$-calcul
					\item \emph{Remarque}: Non-ambiguïté des $\lambda$-termes
					\item \emph{Définition}: Variable libre et lié
					\item \emph{Définition}: Terme clos
					\item \emph{Définition}: Substitution
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Exemple}: Un $\lambda$-terme
					\item \emph{Définition}: Sous-terme sous une partie
					\item \emph{Exemple}: Variable libre et lié dans ce terme
					\item \emph{Définition}: Longueur d'un terme 
					\item \emph{Définition}: $\alpha$-conversion
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{La $\beta$-réduction}
		\begin{small}
			\textblue{La $\beta$-réduction nous permet de calculer grâce aux $\lambda$-terme (ce n'est pas la sémantique du $\lambda$-calcul en tant que logique mais sa sémantique calculatoire). Elle correspond à une exécution d'un programme.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.46\textwidth}
					\item \emph{Définition}: $\beta$-réduction
				\end{minipage} \hfill
				\begin{minipage}{.34\textwidth}
					\item \emph{Définition}: Forme normale
				\end{minipage}
				\item \emph{Remarque}: On n'a pas toujours existence d'une telle forme
				
				\begin{minipage}{.4\textwidth}
					\item \emph{Théorème}: Théorème de Church--Rosser
					\item \emph{Définition}: $\beta$-équivalence
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Corollaire}: Unicité de la forme normale
					\item \emph{Théorème}: Théorème de Church--Rosser
				\end{minipage}
				
				\item \emph{Définition}: Combinateur de point fixe
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Stratégies de réduction et lien avec les langages de programmation}
		\begin{small}
			\textblue{Le $\lambda$-calcul est le cœurs de langages de programmation fonctionnel comme LISP ou Haskel. Cependant, une stratégie de réduction est nécessaire et donne le type de langage et ses spécificités.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.48\textwidth}
					\item \emph{Hypothèse}: La restriction de la $\beta$-réduction 
					\item \emph{Remarque}: Lien entre $\lambda$-abstraction et forme normale
					\item \emph{Application}: LISP
					\item \emph{Remarque}: Peu efficace sauf mémoïsation
				\end{minipage} \hfill
				\begin{minipage}{.32\textwidth}
					\item \emph{Définition}: $\lambda$-abstraction
					\item \emph{Définition}: Réduction par nom
					\item \emph{Définition}: Réduction par valeurs
					\item \emph{Application}: Haskel
				\end{minipage}
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[II.] \textbf{Un modèle de calcul puissant}
	\begin{small}
		\textblue{Le $\lambda$-calcul est un modèle de calcul puissant puisqu'il est aussi expressif que les machines de Turing. De plus, pour pouvoir implémenter des programmes dans les langages de programmation, il nous faut pouvoir implémenter des structures des données grâce à ce modèle de calcul.}
	\end{small}
	\begin{description}
		\item[A.] \emph{Encoder des données}
		\begin{small}
			\textblue{Le $\lambda$-calcul permet d'encoder différentes structures de données nécessaire à la programmation. Il est important de faire passer l'intuition sur ces modèles.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Structure de données}: Booléen
					\item \emph{Structure de données}: Paire
					\item \emph{Application}: Fonction récursive
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Application}: Conditionnelle
					\item \emph{Structure de données}: Liste
					\item \emph{Structure de données}: Entier de Church
				\end{minipage}
				\item \emph{Structure de données}: Entier de Barendregt
			\end{itemize}
		\end{footnotesize}
		\item[B.] \emph{Thèse de Church}
		\begin{small}
			\textblue{La thèse de Church est vérifiée par ce modèle de calcul. Nous avons donc une équivalence aux fonctions $\mu$-récursives et aux machines de Turing.}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Fonction $\lambda$-définissable
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Fonction $\mu$-récursives
				\end{minipage}
				\item \textred{\emph{Théorème}: Fonction $\lambda$-définissable $\Leftrightarrow$ fonction $\mu$-récursive (DEV $\Leftarrow$)}
				\item \emph{Théorème}: Fonction $\mu$-récursives $\Leftrightarrow$ fonction calculable par une machine de Turing
				\item \emph{Corollaire}: Fonction $\lambda$-définissable $\Leftrightarrow$ fonction calculable par une machine de Turing
				\item \emph{Application}: Thèse de Church
			\end{itemize}
		\end{footnotesize}
		\item[C.] \emph{Théorie de la décidabilité}
		\begin{small}
			\textblue{Grâce à la thèse de Church nous nous ouvrons les portes de la théorie de la décidabilité}
		\end{small}
		\begin{footnotesize}
			\begin{itemize}[label=$\rightsquigarrow$]
				\begin{minipage}{.4\textwidth}
					\item \emph{Définition}: Séparabilité
					\item \emph{Définition}: Fonction non triviale
				\end{minipage} \hfill
				\begin{minipage}{.4\textwidth}
					\item \textred{\emph{Théorème}: Théorème de Scott--Curry}
					\item \textred{\emph{Corollaire}: Théorème de Rice}
				\end{minipage}
				\item \emph{Application}: Indécidabilité de l'existence d'une forme normale
			\end{itemize}
		\end{footnotesize}
	\end{description}
	\item[Ouverture] 
	\begin{footnotesize}
		La correspondance de Curry--Howard pour le $\lambda$-calcul simplement typé.
	\end{footnotesize}
\end{description}