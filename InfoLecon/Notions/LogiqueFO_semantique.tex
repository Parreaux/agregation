%DEVs: Validite FO
%LECONs: 
Nous allons maintenant étudier comment on donne un sens à nos formules à l'aide d'un modèle \cite[p.408]{Duparc}. Soit $\varphi$ une formule de la logique du premier ordre, $\mathcal{M}$ un modèle pour cette logique et $\mathcal{V}$ un ensemble de dénombrable de variables (que nous utilisons dans notre formule) sur notre domaine. 

\paragraph{Évaluation sous forme classique} L'évaluation permet de faire un pont entre la syntaxe et la sémantique. Elle se définit naturellement de manière inductive sur la hauteur de notre formule. Les variables ne sont pas comme les constantes, si elle ne sont pas liée par un quantificateur, elles sont libre comme l'air. \textblue{Dans la suite, on utilisera des formules closes (toutes les variables sont liées) mais nous devons définir l'évaluation en présence de variables libres car toutes sous formules d'une formule close ne sont pas nécessairement closes.}

\begin{definition}
	Une assignation des variable $v$ est une fonction de $\mathcal{V} \to M$.
\end{definition}

\begin{definition}
	Une assignation des termes est définie par induction comme suit:
	\begin{itemize}
		\item $v^{\mathcal{M}}(x) = v(x)$ si $x \in \mathcal{V}$;
		\item $v^{\mathcal{M}}(f(t_1, \dots, t_n)) = f^{\mathcal{M}}\left(v^{\mathcal{M}}(t_1), \dots, v^{\mathcal{M}}(t_n) \right)$ sinon.
	\end{itemize}
\end{definition}

\begin{definition}
	On définit (par induction sur la hauteur de la formule) $\mathcal{M}, v \models \varphi$ qui signifie que $\varphi$ est vraie dans le modèle $\mathcal{M}$ sous l'assignation des variables $v$ \textblue{(que nous appelons également condition de vérités)}.
	\begin{itemize}
		\item $\mathcal{M}, v \models p(t_1, \dots, t_n)$ si $p^{\mathcal{M}}\left(v^{\mathcal{M}}(t_1), \dots, v^{\mathcal{M}}(t_n)\right) = 1$
		\item $\mathcal{M}, v  \models \neg \varphi$ si $\mathcal{M}, v  \nvDash \varphi$ 
		\item $\mathcal{M}, v  \models (\varphi \vee \psi)$ si $\mathcal{M}, v  \models \varphi$ ou  $\mathcal{M}, v  \models \psi$
		\item $\mathcal{M}, v  \models (\varphi \wedge)$ si $\mathcal{M}, v  \models \varphi$ et $\mathcal{M}, v  \models \psi$
		\item $\mathcal{M}, v  \models \exists x\varphi$ s'il existe $m \in D$ tel que $\mathcal{M}, v[x:=m]  \models \varphi$ 
		\item $\mathcal{M}, v  \models \forall x \varphi$ pour tout $m \in D$ tel que $\mathcal{M}, v[x:=m]  \models \varphi$
	\end{itemize}
\end{definition}

\begin{req}
	Dans certain ouvrage, une $\Sigma$-structure est appelée modèle que si cette structure est telles que $\mathcal{M}, v \models \varphi$.
\end{req}

\begin{definition}
	Deux formules de la logique du premier ordre $\varphi$ et $\psi$ sont dites équivalentes si et seulement si pour tout modèle $\mathcal{M}$, ($\mathcal{M}, v \models \varphi$ si et seulement si $\mathcal{M}, v \models \psi$). On note alors $\varphi \equiv \psi$.
\end{definition}


\paragraph{Évaluation par le jeu} Évaluer une formule peut se présenter comme un jeu à deux joueurs: le premier cherchant à montrer que la formule est satisfiable et le second à prouver que ce n'est pas le cas. Pour définir ce jeu, on suppose que les seuls connecteurs logique que nous disposons sont $\neg, \vee, \wedge$.

\begin{definition}
	Soit $\varphi$ une formule du premier ordre et $\mathcal{M}$ un modèle. Le jeu d'évaluation $\mathbb{E}v(\mathcal{M}, \varphi)$ est défini comme suit:
	\begin{enumerate}
		\item Il comprend deux joueurs : Vérifieur (prouve que $\mathcal{M}, v \models \varphi$) et le Falsificateur (prouve que $\mathcal{M}, v \nvDash \varphi$). Les coûts des joueurs consiste à choisir des sous-formules via les règles suivantes:
		\begin{description}
			\item[$\varphi_0 \vee \varphi_1$] V choisit $j \in \{0, 1\}$ et le jeu continue avec $\varphi_j$.
			\item[$\varphi_0 \wedge \varphi_1$] F choisit $j \in \{0, 1\}$ et le jeu continue avec $\varphi_j$.
			\item[$\neg \varphi$] V et F échange leur rôle et le jeu continue avec $\varphi$.
			\item[$\exists x \varphi$] V choisit $a \in M$ et le jeu continue avec $v[x:=a]$ et $\varphi$.
			\item[$\forall x \varphi$] F choisit $a \in M$ et le jeu continue avec $v[x:=a]$ et $\varphi$.
			\item[$R(t_1, \dots, t_n)$] le jeu s'arrête et V gagne si et seulement si $p^{\mathcal{M}}\left(v^{\mathcal{M}}(t_1), \dots, v^{\mathcal{M}}(t_n)\right) = 1$
		\end{description}
		
		\item La condition de victoire n'apparaît que si le jeu s'arête. Il s'arête quand toutes les variables ont été assignées (si la formule est close) et que nous obtenons une formule atomique.
	\end{enumerate}
\end{definition}

On peut maintenant se demander si le Vérifieur a une stratégie gagnante dans notre jeu. En déroulant la partie sous la forme d'un arbre, on voit que cette stratégie (si elle existe) peut être calculant en remontant l'arbre à partir d'une solution gagnante pour celui-ci.

\begin{theo}
	$\mathcal{M}, v \models \varphi$ si et seulement si V a une stratégie gagnante dans $\mathbb{E}v(\mathcal{M}, \varphi)$.
\end{theo}
\begin{footnotesize}
	\begin{proof}[Idée de la preuve]
		Se fait par induction sur la hauteur de la formule pour montrer que si $\mathcal{M}, v \models \varphi$ alors V a une stratégie gagnante dans $\mathbb{E}v(\mathcal{M}, \varphi)$ et si si $\mathcal{M}, v \models \neg\varphi$ alors F a une stratégie gagnante dans $\mathbb{E}v(\mathcal{M}, \varphi)$.
	\end{proof}
\end{footnotesize}

\begin{req}
	Le jeu $\mathbb{E}v(\mathcal{M}, \varphi)$ est un jeu fini (la formule est de taille finie) mais l'arbre du jeu peut être infini (si le domaine est infini).
\end{req}