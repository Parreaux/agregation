%DEVs : Automate des occurrences
%LECONs : 909
Déterminer un automate déterministe contenant un nombre minimal d'états pour un langage rationnel donné est très intéressant en pratique. On définit ainsi un représentant canonique pour reconnaître un langage. Le résultat nous assurant de leur existence est un résultat très fort (qui reste vrai même si l'automate n'est pas déterministe). Il y a deux manières de définir un automate minimal (selon le point de vu que l'on considère suite au théorème de Kleene): intrinsèquement au langage avec la notion de quotient ou en utilisant les états inséparables qui est une méthode plus calculatoire.

\paragraph{Morphismes d'automates}
La relation d'ordre permettant de définir un automate minimal est formellement donnée par la notion de morphisme d'automate.

\begin{definition}[Morphisme d'automate \protect{\cite[p.120]{Sakarovitch}}]
	Soient $\mathcal{A}_1 = (Q_1, \delta_1, I_1, F_1)$ et $\mathcal{A}_2 = (Q_2, \delta_2, I_2, F_2)$ deux automates finis. Un morphisme d'automate $\phi : \mathcal{A}_1 \to \mathcal{A}_2$ est une application de $Q_1$ dans $Q_2$ telle que : $\phi(I_1) \subset I_2$; $\phi(F_1) \subset F_2$ et $\forall p, q \in Q_1$, $q \in \delta_1(p, a) \Rightarrow \phi(q) \in \delta_2(\phi(p), a)$.
\end{definition}

\begin{req}
	S'il existe $\phi : \mathcal{A}_1 \to \mathcal{A}_2$ un morphisme entre deux automates alors $\mathcal{L}(\mathcal{A}_1) \subseteq \mathcal{L}(\mathcal{A}_2) $. En effet, soit $w \in \mathcal{L}(\mathcal{A}_1)$. Par définition, il existe $i \in I_1$ tel que $\delta_1(i, w) \in F_1$. En appliquant le morphisme, on a $\phi(i) \in I_2$ (première condition sur le morphisme) et $\delta_2(\phi(i), w) \in F_2$ (deux dernières conditions sur le morphisme). Donc $w \in mathcal{L}(\mathcal{A}_2)$.
\end{req}

\begin{prop}
	Si $\phi : \mathcal{A}_1 \to \mathcal{A}_2$ est un morphisme d'automate surjectif entre deux automates déterministes complets, alors $\mathcal{L}(\mathcal{A}_1) = \mathcal{L}(\mathcal{A}_2) $.
\end{prop}
\begin{footnotesize}
	\begin{proof}[Idée de la démonstration]
	\begin{itemize}
		\item $\mathcal{L}(\mathcal{A}_1) \subseteq \mathcal{L}(\mathcal{A}_2)$ : remarque
		\item $\mathcal{L}(\mathcal{A}_1) \supseteq \mathcal{L}(\mathcal{A}_2)$ : surjectif qui assure que la fonction de transition sur la fonction inverse est bien définie et le complet assure qu'une transition inverse existe pour toute lettre.
	\end{itemize}
\end{proof}
\end{footnotesize}

\begin{definition}
	On définit la relation d'ordre $\preceq$ sur l'ensemble des automates déterministes complets par : $\mathcal{A}_1 \preceq \mathcal{A}_2$ s'il existe un morphisme $\phi$ surjectif.
\end{definition}

\begin{definition}
	Un automate est dit minimal s'il est minimal pour $\preceq$.
\end{definition}

\paragraph{Caractérisation de l'automate minimal} La notion de morphisme, même si elle donne un cadre formel à notre étude n'est pas facile à manipuler. Nous allons donc donner d'autres caractérisations de la minimalité d'un automate qui sont basées sur les points de vus que nous avons de cette minimalité.

\subparagraph{Automate des résiduels} Une première manière de caractériser l'automate minimal est d'utiliser le point de vu intrinsèque au langage : la notion de quotient qui est la notion de langage et d'automate résiduel.

\begin{minipage}{.46\textwidth}
	\begin{figure}[H]
		\begin{tikzpicture}[font=\sffamily]
		% Setup the style for the states
		\tikzset{node style/.style={state, 
				minimum width=1cm,
				line width=.25mm,
				fill=white,circle}}
		
		% Draw the states
		\node[node style,initial] at (0,0) (0) {$L$};
		\node[node style,accepting] at (3,-1) (1) {$\mathcal{L}(b^*)$};
		\node[node style] at (3,1) (2) {$\emptyset$};
		
		% Connect the states with arrows
		\draw[every loop,auto=right,line width=.25mm,
		>=latex]
		(0) edge [] node {$a$} (1)
		(0) edge [auto=left] node {$b$} (2)
		(1) edge [] node {$a$} (2)
		(1) edge [loop right] node {$b$} (1)
		(2) edge [loop right] node {$a,b$} (2);
		\end{tikzpicture}
		\caption{Un automate des résiduels pour $L = \mathcal{L}(ab^*)$.}
	\end{figure}
\end{minipage} \hfill
\begin{minipage}{.46\textwidth}
	\begin{definition}
		Soit $L \subseteq \Sigma^*$, $u \in \Sigma^*$. On définit le résiduel de $L$ par $u$ comme le langage $u^{-1}L = \{v \in \Sigma^* | uv \in L\}$.
	\end{definition}
	
	\begin{definition}
		Soit $L \subseteq \Sigma^*$. On définit l'automate des résiduels de $L$ par $\mathcal{R}(L) = \{Q_L, \delta_L, I_L, F_L\}$ avec $Q_L = \{u^{-1}L | u \in \Sigma^*\}$, $\delta_L(u^{-1}L,a) = a^{-1}u^{-1}L = (ua)^{-1}L$, $I_L = L = \epsilon^{-1}L$ et $F_L = \{u^{-1}L |u \in L\} = \{u^{-1}L | \epsilon \in u^{-1}L\}$.
	\end{definition}
\end{minipage}

\begin{prop}[Caractérisation de la minimalité par les résiduels]
	$L$ est reconnaissable si et seulement si $L$ possède un nombre fini de résiduels. De plus, dans ce cas, $\mathcal{R}(L)$ est minimal.
\end{prop}

\subparagraph{Équivalence de Nérode} Le deuxième point de vu qui est plus calculatoire est la congruence de Nérode. Cette congruence se fait sur une relation d'équivalence qui permet de déterminer si deux états sont inséparables ou non.

\begin{definition}
	Soit $\mathcal{A}$ un automate fini déterministe. Une relation d'équivalence $\sim$ sur $Q$ est une congruence si : 
	\begin{itemize}
		\item $\forall p, q$, $p \sim q \Rightarrow \forall a \in \Sigma, \delta(p, a) \sim \delta(q, a)$ \textblue{(comptabilité avec $\delta$)}
		\item $\forall p, q$, $p \sim q \Rightarrow (p \in F \Leftrightarrow q \in F)$ \textblue{(saturation de $F$)}
	\end{itemize}
\end{definition}

\begin{req}
	La congruence de Nérode est la plus grossière respectant la définition de la congruence.
\end{req}

\begin{definition}
	Si $\mathcal{A}$ un automate fini déterministe et $\sim$ est une relation d'équivalence sur $Q$, on définit le quotient de $\mathcal{A}$ par $\sim$ comme : $\mathcal{A}/\sim = (Q/\sim, \delta_{\sim}, \{[i]\}, \{[f] | f \in F\})$ où $\delta_{\sim}([p], a) = [\delta(p), a]$. 
\end{definition}

\begin{req}
	Cette définition est valide pour toute relation d'équivalence.
\end{req}

\begin{prop}
	On a $\mathcal{L}(\mathcal{A}/\sim) = \mathcal{L}(\mathcal{A})$.
\end{prop}

\begin{definition}
	Soit $\mathcal{A}$ un automate fini. L'équivalence sur $Q$ définie par $p \equiv q$ si et seulement si $\forall w \in \Sigma^* \delta(p, w) \in F \Leftrightarrow \delta(p, w) \in F$ est appelée congruence de Nérode.
\end{definition}

\begin{prop}
	L'automate quotient obtenu par la congruence de Nérode $\mathcal{A}/\equiv$ est isomorphe à l'automate des résiduels $\mathcal{R}(L)$.
\end{prop}


\paragraph{Calcul de l'automate minimal}
Nous avons énoncé quelques caractérisation de cet automate minimal. Il nous reste à étudier comment le calculer à partir d'un automate fini qui n'est pas nécessairement optimal. Cette construction peut se faire à partir de la construction de Moore que nous allons explicité. L'implémentation naïve de cette construction donne l'algorithme de Moore \cite[p.124]{Sakarovitch}. Puis nous étudierons l'algorithme de Hopcroft qui est une implémentation élaborée de cette méthode \cite[p.318]{Beauquier-Berstel-Chretienne}.

\subparagraph{Construction de Moore} Soit $\mathcal{A}$ un automate déterministe. Pour calculer son automate minimal, il suffit de calculer l'équivalence de Nérode. Pour cela, nous allons l'approcher successivement par l'équivalence $\sim_k$ dont la limite sera celle de Nérode.

\begin{definition}
	Soit $k \in \N$, on définit l'équivalence suivante sur $Q$ d'un automate déterministe $\mathcal{A}$:
	\begin{displaymath}
	p \sim_k q \Leftrightarrow L_p^{(k)} = L_q^{(k)}
	\end{displaymath}
	avec $L_p^{(k)} = \{w \in L_p~|~|w| \leq k\}$.
\end{definition}

\begin{prop}
	Pour tout entier $k \geq 1$, on a 
	\begin{displaymath}
	p \sim_k q \Leftrightarrow p \sim_{k-1} q \text{ et } \left(\forall a \in A, pa \sim_{k-1} qa\right)
	\end{displaymath}
\end{prop}
\begin{footnotesize}
	\begin{proof}
	On a
	\begin{displaymath}
	\begin{array}{ccll}
	L_p^{(k)} & = & \{p.w \in T~|~|w| \leq k\} & \textblue{\text{Définition}}\\
	& = & \{p.w \in T~|~|w| \leq k-1\} \cup \bigcup_{a \in A} a\{v ~|~ (pa)v \in T \text{ et } |v| \leq k - 1\} & \textblue{\text{Cas}}\\
	& = & L_p^{(k-1)} \cup \bigcup_{a \in A} aL_{pa}^{(k-1)} & \textblue{\text{Traduction}}\
	\end{array}
	\end{displaymath}
	On conclut en traduisant par leur définition ces égalités.
\end{proof}
\end{footnotesize}

\begin{cor}
	Si les équivalences $\sim_k$ et $\sim_{k+1}$ coïncident, alors les équivalence $\sim_{k+l}$ avec $l \geq 0$ sont toutes égales et égales à l'équivalence de Nérode.
\end{cor}
\begin{footnotesize}
	\begin{proof}
	Par récurrence (en appliquant  la proposition précédente), on a l'égalité des équivalence. La deuxième assertion provient de la congruence de Nérode: $p\sim q \Leftrightarrow p\sim_k q~\forall k \in \N$.
\end{proof}
\end{footnotesize}

\begin{prop}
	Si $\mathcal{A}$ est un automate à $n$ états, l'équivalence de Nérode de $\mathcal{A}$ est égale à $\sim_{n-2}$
\end{prop}
\begin{footnotesize}
	\begin{proof}
		Si pour $k \geq 0$, les équivalence $\sim_{k-1}$ et $\sim_k$ sont distinctes, le nombre de classe d'équivalence $\sim_k$ est inférieure où égale à $k + 2$.
	\end{proof}
\end{footnotesize}

\subparagraph{Algorithme de Hopcroft} L'algorithme de Hofcroft (algorithme~\ref{algo:Hopcroft}) permet d'implémenter de manière astucieuse cette équivalence inductive. 
 
\begin{minipage}{0.62\textwidth}
	\begin{algorithm}[H]
		Entrée : $\mathcal{A}$ est un automate déterministe complet et émondé
		\begin{algorithmic}[1]
			\Function{\textsc{Hopcroft}}{$\mathcal{A}$} 
			\State $\mathcal{P} \gets (F, Q\setminus F)$ 
			\State $S \gets \{(\min(F, Q \setminus F), a) ~|~ a \in A\}$
			\While{$S \neq \emptyset$}
			\State Choisir $(C, a) \in S$
			\State $S \gets S \setminus (C,a)$
			\For{$B \in \mathcal{P}$}
			\State Couper $B$ par $(C, a)$ en $B_1, B_2$
			\State Remplacer $B$ par $B_1$, $B_2$ dans $\mathcal{P}$
			\For{$b \in \Sigma$}
			\If{$(B, b) \in S$}
			\State Remplacer $(B, b)$ par $(B_1, b)$, $(B_2, b)$ dans $S$
			\Else
			\State Ajouter $(\min (B_1,B_2),b)$ à $S$
			\EndIf
			\EndFor
			\EndFor
			\EndWhile
			\EndFunction
		\end{algorithmic}
		\caption{Algorithme de Hopcroft permettant de calculer un automate minimal.}
		\label{algo:Hopcroft}
	\end{algorithm}
\end{minipage}\hfill
\begin{minipage}{.33\textwidth}
	L'algorithme naïf qui applique la méthode de Moore s'exécute en $O(mn^2)$ où $m$ est la taille de l'alphabet et $n$ le nombre d'étapes de calcul. L'algorithme de Hopcroft nous permet d'obtenir une exécution en temps en $O(mn\log n)$.
	
	\textcolor{white}{text}
	
	\emph{Principe}: Calcul de la congruence de Nérode par raffinement successifs utilisant le paradigme diviser pour régner.
	
	\begin{definition}[Partie stable \protect{\cite[p.320]{Beauquier-Berstel-Chretienne}}]
		Soit $\mathcal{P}$ une partition de $Q$, $A$, $B$ deux éléments de $\mathcal{P}$ et $a$ un élément de $\Sigma$.
		
		On dit que $A$ est stable pour $(B,a)$ si $Aa \subset B$ ou $Aa \cap B = \emptyset$ avec $Aa = \{qa~|~q \in A\}$. Sinon, la paire $(B, a)$ coupe $A$ en deux parties $A_1 = \{q \in A ~|~qa \in B\}$ et $A_2= \{q \in A ~|~qa \notin B\}$.
	\end{definition}
\end{minipage}

\begin{theo}
	Soit $\Sigma$ un alphabet à $m$ lettres et $\mathcal{A}$ un automate à $n$ états sur cet alphabet. L'algorithme \textsc{Hopcroft} (Algorithme~\ref{algo:Hopcroft}) calcule, dans le pire cas, en temps $O(mn\log n)$ l'automate minimal de $\mathcal{A}$.
\end{theo}
\begin{footnotesize}
	\begin{proof}[Arguments de la démonstration]
		\noindent\emph{Terminaison}: 
		\begin{itemize}
			\item On munit l'ensemble $\{\text{Partition de } Q\} \times \left(\mathcal{P}\left(\mathcal{P}(Q)\right)\right) \times \Sigma$ de l'ordre bien fondé $\leq$ définie telle que $(\mathcal{P}, S) \leq (\mathcal{P}', S')$ si $|\mathcal{P}| > |\mathcal{P}'|$ et $|S| \leq |S'|$.
			\item $\leq$ ordre bien fondé: si $\mathcal{P}$ reste stable, $S$ diminue et il ne peut pas augmenter.
		\end{itemize}
		
		\noindent\emph{Correction}: La partition donnée par Nérode est toujours plus fine que $\mathcal{P}$. On montre que, lorsque l'algorithme termine, $\mathcal{P}$ est stable pour $(P, a)$ avec $P \in \mathcal{P}$ et $a \in \Sigma$.
		
		\emph{Notations}: $\mathcal{P}_i$ la valeur de $\mathcal{P}$ avant la $i^{\text{ième}}$ itération et $\mathcal{P}_N$ la dernière valeur de $\mathcal{P}$ et
		\begin{displaymath}
			B \vartriangleleft_a C = \left\{ 
			\begin{array}{ll}
			\{B\} & \text{si } B \text{ est stable par} (C,a) \\
			\{B_1, B_2\}& \text{sinon} \\
			\end{array}
			\right.
		\end{displaymath}
	
		\begin{small}
			\begin{lemme}
			Soit $C_1 \sqcup C_2$, $a \in \Sigma$.
			\begin{enumerate}
				\item $B$ stable par $(C_1, a)$ et $(C_2, a)$ implique que $B$ est stable par $(C, a)$.
				\item $B$ stable par $(C_1, a)$ et $(C, a)$ implique que $B$ est stable par $(C_2, a)$.
				\item $(B \vartriangleleft_a C) \vartriangleleft_a T = (B \vartriangleleft_a T) \vartriangleleft_a C$.
			\end{enumerate}
		\end{lemme}
		\end{small}
		\begin{proof}[Arguments de la démonstration]
			\begin{enumerate}
				\item Distinction de cas
				\item Distinction de cas
				\item $(B \vartriangleleft_a C) \vartriangleleft_a T$ et $(B \vartriangleleft_a T) \vartriangleleft_a C$ sont composés d'éléments non vide de $B \cap a^{-1}C \cap b^{-1}T$, $B \cap a^{-1}\overline{C} \cap b^{-1}T$, $B \cap a^{-1}\overline{C} \cap b^{-1}\overline{T}$ et $B \cap a^{-1}C \cap b^{-1}\overline{T}$.
			\end{enumerate}
		\end{proof}
		
		\begin{small}
			\begin{lemme}
				Soit $P \in \mathcal{P}$, $a \in \Sigma$, $(P, a) \notin S$, alors $\mathcal{P}_N$ est stable pour $(P,a)$.
			\end{lemme}
		\end{small}
		\begin{proof}[Arguments de la démonstration]
			On raisonne par induction
			\begin{description}
				\item[Initialisation] Si $(P,a) \in S_1$, $(\overline{P}, a) \notin S_1$ et $\mathcal{P}_N$ sera stable pour $(\overline{P}, a)$ donc pour $(P,a)$ \textblue{(par 1 et 3 du lemme)}.
				
				\item[Hérédité] Soit $P \in \mathcal{P}_{k+1}$ tel que $(P,a) \notin S_{k+1}$.
				\begin{itemize}
					\item Si $P \in \mathcal{P}_k$, alors si $(P, a) \notin S_k$, ok. Sinon $(P, a) \in S_k$, on a alors retirer $(P, a)$ de $S_k$. Donc $\mathcal{P}_N$ est stable pour $(P,a)$.
					\item Si $P \notin \mathcal{P}_k$, alors il existe $C, b$ tels que $R \vartriangleleft_b C = (P, P')$. Si $(R, a) \notin S_k$, alors $(P', a) \in S_{k+1}$ et $\mathcal{P}_N$ est stable pour $(R, a)$ et $(P',a)$, donc pour $(P,a)$. Sinon, on a retiré $(R, a)$ et $S_{k+1}$ est stable pour $(R,a)$. Donc $(P,a) \in S_{k+1}$ et $\mathcal{P}_N$ est stable pour $(P,a)$.
				\end{itemize}
			\end{description}
		\end{proof}
		
		\noindent\emph{Complexité temporelle}: $O(mn\log n)$
		\begin{itemize}
			\item La boucle \textsf{tant que} s'effectue $O(nm)$ fois \textblue{(méthode de l'agrégat)}.
			\item Le nombre de fois que l'on supprime un élément de $S$ est au plus $\log n$.
		\end{itemize}
	\end{proof}
\end{footnotesize}

\subparagraph{Algorithme par renversement} Il existe un autre algorithme pour calculer un automate minimal qui n'utilise pas la congruence de Nérode \cite[p.125]{Sakarovitch}. On rappelle la définition d'un automate co-accessible : $\exists f \in F, \exists w \in \Sigma^*$ avec $f\in \delta(q, w)$ et co-déterministe est un automate déterministe si on inverse les transitions.

\begin{prop}
	Soit $L \in Rec(\Sigma^*)$. Le déterminisé d'un automate co-déterministe co-accessible qui reconnaît $L$ est minimal.
\end{prop}

\begin{definition}
	Soit $\mathcal{A} = (Q, \delta, I, F)$. Le miroir de $\mathcal{A}$ est $mir(\mathcal{A})  = (Q, \delta^t, F, I)$ où $\delta^t(p, a) = \{q ~|~ p \in \delta(q, a)\}$. 
\end{definition}

\begin{prop}[Algorithme de Brzozowski]
	\begin{displaymath}
	R\left(\mathcal{L}\left(\mathcal{A}\right)\right) = det(mir(det(mir(\mathcal{A}))))
	\end{displaymath}
\end{prop}


\begin{figure}
	\begin{minipage}{.46\textwidth}
		\begin{tikzpicture}[font=\sffamily]
		% Setup the style for the states
		\tikzset{node style/.style={state, 
				minimum width=1cm,
				line width=.25mm,
				fill=white,circle}}
		
		% Draw the states
		\node[node style,initial] at (0,0) (0) {$0$};
		\node[node style] at (3,0) (1) {$1$};
		\node[node style,accepting] at (0, -2) (2) {$2$};
		\node[node style] at (3, -2) (3) {$3$};
		\node[node style] at (0, -4) (4) {$4$};
		\node[node style,accepting] at (3, -4) (5) {$5$};
		\node[node style] at (1.5, -3) (6) {$6$};
		
		% Connect the states with arrows
		\draw[every loop,auto=right,line width=.25mm,
		>=latex]
		(0) edge [bend left] node {$a$} (1)
		(0) edge [] node {$b$} (2)
		(1) edge [bend left] node {$a$} (0)
		(1) edge [] node {$b$} (3)
		(2) edge [bend left] node {$b$} (4)
		(2) edge [auto=left] node {$a$} (6)
		(3) edge [bend left] node {$b$} (5)
		(3) edge [] node {$b$} (6)
		(4) edge [bend left] node {$b$} (2)
		(4) edge [] node {$b$} (6)
		(5) edge [bend left] node {$b$} (3)
		(5) edge [auto=left] node {$b$} (6)
		(6) edge [loop above] node {$a,b$} (6);
		\end{tikzpicture}
		\caption{Automate à minimiser}
\end{minipage}\hfill
\begin{minipage}{.46\textwidth}
	\begin{tabular}{cl}
		$r_0$ & $\{\underset{\textgreen{A}\textbrown{B}}{0}, \underset{\textgreen{A}\textbrown{A}}{1}, \underset{\textgreen{A}\textbrown{B}}{3}, \underset{\textgreen{A}\textbrown{B}}{4}, \underset{\textgreen{A}\textbrown{A}}{6}\}_A$ $\{\underset{\textgreen{A}\textbrown{A}}{2},  \underset{\textgreen{A}\textbrown{A}}{5}\}_B$ \\
		& \\
		$r_1$ & $\{\underset{\textgreen{B}\textbrown{A}}{1},  \underset{\textgreen{A}\textbrown{A}}{6}\}_A$ 
		$\{\underset{\textgreen{A}\textbrown{C}}{0},  \underset{\textgreen{A}\textbrown{C}}{3}, \underset{\textgreen{A}\textbrown{C}}{4}\}_B$ 
		$\{\underset{\textgreen{A}\textbrown{B}}{2},  \underset{\textgreen{A}\textbrown{B}}{5}\}_C$ \\
		& \\
		$r_2$ & $\{1\}_A$ $\{6\}_B$ 
		$\{\underset{\textgreen{A}\textbrown{D}}{0},  \underset{\textgreen{A}\textbrown{D}}{3}, \underset{\textgreen{B}\textbrown{D}}{4}\}_C$ 
		$\{\underset{\textgreen{B}\textbrown{C}}{2},  \underset{\textgreen{B}\textbrown{C}}{5}\}_D$\\
		& \\
		\textred{$r_3$} & $\{1\}_A$ $\{6\}_B$ $\{0\}_C$
		$\{\underset{\textgreen{B}\textbrown{E}}{3}, \underset{\textgreen{B}\textbrown{E}}{4}\}_D$ 
		$\{\underset{\textgreen{B}\textbrown{D}}{2},  \underset{\textgreen{B}\textbrown{D}}{5}\}_E$\\
		& \\
		$r_4$ & $\{1\}_A$ $\{6\}_B$ $\{0\}_C$
		$\{3, 4\}_D$ 
		$\{2, 5\}_E$\\
	\end{tabular}
	\caption{Exemple de calcul par l'algorithme de Hopcroft (Algorithme~\ref{algo:Hopcroft}). En vert, on donne la partition dans laquelle on arrive si on lit un $a$ et en marron celle si on lit un $b$.}	
\end{minipage}
	\label{fig:exHopcroft}
\end{figure}

\subparagraph{Applications}


\begin{itemize}
	\item Algorithmes de Hopcroft et de Moore
	\item \emph{Appli}: Équivalence de langage + Bi-simulation.
\end{itemize}