Les arbres splay sont une implémentation possible pour les arbres de recherche optimaux. Lorsqu'un élément est recherché, il est placé à la racine (recherche auto-adaptative sur un arbre). Ce sont donc des abres binaires de recherche qui permettent d'atteindre l'élément cherché le plus rapidement possible. Ils sont basés sur une opération principale \textsc{Splay} qui consiste à faire remonter un noeud à la racine par rotation successive. Ils ont été inventé par Daniel Sleator et Robert Tarjan en 1985.

\paragraph{Opérations}: Les arbres splays permettent de faire les opérations suivantes en $O(h)$:
\begin{itemize}
	\item \textsc{Recherche}($i$, $S$) détermine si l'élément $i$ est un élément de l'arbre $S$;
	\item \textsc{Insertion}($i$, $S$) ajoute l'élément $i$ dans $S$ s'il n'est pas déjà dedans;
	\item \textsc{Suppression}($i$, $S$) supprime l'élément $i$ de $S$ s'il est présent;
	\item \textsc{Union}($S$, $S'$) fusionne $S$ et $S'$ dans un unique arbre splay avec comme hypothèse $\forall x \in S, \forall y \in S', x < y$;
	\item \textsc{Pivot}($i$, $S$) sépare l'arbre splay en deux sous-arbre $S'$ et $S''$ tels que $x \leq i \leq y$ avec $x \in S'$ et $y \in S''$.
\end{itemize}

Ces opérations se base sur l'opération \textsc{Splay}($i$, $S$) qui réorganise l'arbre $S$ en mettant $i$ à la racine de l'arbre $S$ si $i \in S$ et $\max \{k \in S, k < i \}$ sinon.
\begin{itemize}
	\item \textsc{Recherche}($i$, $S$): \textsc{Splay}($i$, $S$) et on teste la racine;
	\item \textsc{Insertion}($i$, $S$): \textsc{Splay}($i$, $S$) et on insère $i$ à la racine;
	\item \textsc{Suppression}($i$, $S$): \textsc{Splay}($i$, $S$), on supprime $i$ et on applique \textsc{Union} aux deux fils;
	\item \textsc{Union}($S$, $S'$): \textsc{Splay}($+ \infty$, $S$) et on insère $S'$ comme fils droit;
	\item \textsc{Pivot}($i$, $S$): \textsc{Splay}($i$, $S$) et on récupère les deux fils.
\end{itemize}

L'opération \textsc{Splay} se fait à l'aide d'une opération élémentaire assez classique dans les ABR (rotation, Figure \ref{fig:ASplay-rotation}).

\begin{figure}
	%\centering
	\begin{minipage}[c]{.46\linewidth}
		\centering
		\begin{tikzpicture}[level/.style={sibling distance=5em, level distance=2.5em}]
		\node [circle,draw] (y){$y$}
		child {node [circle,draw] (x) {$x$}
			child {node {$\alpha$}}
			child {node {$\beta$}}
		}
		child {node {$\gamma$}};
		\end{tikzpicture}
	\end{minipage} \hfill
	\begin{minipage}[c]{.46\linewidth}
		\centering
		\begin{tikzpicture}[level/.style={sibling distance=5em, level distance=2.5em}]
		\node [circle,draw] (b){$x$}
		child {node {$\alpha$}}
		child {node [circle,draw] (a) {$y$}
			child {node {$\beta$}}
			child {node {$\gamma$}}
		};
		
		\end{tikzpicture}
	\end{minipage}
	\caption{Les rotations à la base de l'opération \textsc{Splay}: de droite à gauche, $rotation(x)$ et de gauche à droite, $rotation(y)$.}
	\label{fig:ASplay-rotation}	
\end{figure}

Cette opération fait bien remonter $x$ vers la racine mais il faut faire un peu plus attention pour conserver l'équilibre de l'arbre. On veut donc faire remonter le noeud $x$ à la racine selon les trois cas suivant:
\begin{enumerate}
	\item Si $x$ est fils de la racine: on a $zig(x) = rotation(x)$ (Figure \ref{fig:ASplay-rotation});
	\item Si $x$ et son père $y$ sont tous les deux fils gauches (ou droits): $zig-zig(x)$ est l'application successives de $rotation(y)$ ou $rotation(x)$ (Figure \ref{fig:ASplay-zigzig});
	\item Si $x$ et son père $y$ sont des fils de côtés différents (droite-gauche ou gauche-droite): $zig-zag(x)$ est l'application $rotation(x)$ deux fois (Figure \ref{fig:ASplay-zigzag}).
\end{enumerate}

\begin{figure}
	\centering
	\begin{tabular}{lcr}
		\begin{tikzpicture}[level/.style={sibling distance=5em, level distance=2.5em}]
		\node [circle,draw] (z) {$z$}
		child {node [circle,draw] (y) {$y$}
			child {node [circle,draw] (x) {$x$}
				child {node {$\alpha$}}
				child {node {$\beta$}}
			}
			child {node {$\gamma$}}
		}
		child {node {$\delta$}};
		\end{tikzpicture}
		&
		\begin{tikzpicture}[level/.style={sibling distance=8em/#1, level distance=2.5em}]
		\node [circle,draw] (y) {$y$}
		child {node [circle,draw] (x) {$x$}
			child {node {$\alpha$}}
			child {node {$\beta$}}
		}
		child {node [circle,draw] (z) {$z$}
			child {node {$\gamma$}}
			child {node {$\delta$}}
		};
		\end{tikzpicture}
		&
		\begin{tikzpicture}[level/.style={sibling distance=5em, level distance=2.5em}]
		\node [circle,draw] (x) {$x$}
		child {node {$\alpha$}}
		child {node [circle,draw] (y) {$y$}
			child {node {$\beta$}}
			child {node [circle,draw] (z) {$z$}
				child {node {$\gamma$}}
				child {node {$\delta$}}
			}	
		};
		\end{tikzpicture}
	\end{tabular}
	\caption{Les rotations à la base de l'opération \textsc{Splay}: l'opération $zig-zig(x)$.}
	\label{fig:ASplay-zigzig}
\end{figure}

\begin{figure}
	\centering
	\begin{tabular}{lcr}
		\begin{tikzpicture}[level/.style={sibling distance=5em, level distance=2.5em}]
		\node [circle,draw] (z) {$z$}
		child {node {$\alpha$}}
		child {node [circle,draw] (y) {$y$}
			child {node [circle,draw] (x) {$x$}
				child {node {$\beta$}}
				child {node {$\gamma$}}
			}
			child {node {$\delta$}}
		};
		\end{tikzpicture}
		&
		\begin{tikzpicture}[level/.style={sibling distance=5em, level distance=2.5em}]
		\node [circle,draw] (z) {$z$}
		child {node {$\alpha$}}
		child {node [circle,draw] (x) {$x$}
			child {node {$\beta$}}
			child {node [circle,draw] (y) {$y$}
				child {node {$\gamma$}}
				child {node {$\delta$}}
			}	
		};
		\end{tikzpicture}
		&
		\begin{tikzpicture}[level/.style={sibling distance=8em/#1, level distance=2.5em}]
		\node [circle,draw] (x) {$x$}
		child {node [circle,draw] (z) {$z$}
			child {node {$\alpha$}}
			child {node {$\beta$}}
		}
		child {node [circle,draw] (y) {$y$}
			child {node {$\gamma$}}
			child {node {$\delta$}}
		};
		\end{tikzpicture}
	\end{tabular}
	\caption{Les rotations à la base de l'opération \textsc{Splay}: l'opération $zig-zag(x)$.}
	\label{fig:ASplay-zigzag}
\end{figure}



\paragraph{Complexité}: La hauteur de l'arbre est dans le pire cas en $O(n)$ (très peu probable), en moyenne elle est de $O(n \log n)$)