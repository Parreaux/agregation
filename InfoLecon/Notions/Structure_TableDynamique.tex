%LECONs : 926
Lorsqu'on manipule un tableau \cite[p.428]{Cormen}, on ne connaît pas toujours la place dont on va avoir besoin. On veut alors pouvoir ré-allouer une table plus grande dans le cas où on ajoute une nouvelle valeur et que notre table est déjà pleine. Nous allons montre que la complexité amortie de l'opération \textsf{insérer} est en $O(1)$ lorsque \textsf{insérer} consiste à ajouter l'élément dans la table si elle est non pleine ou de créer une table de taille double, de recopier les valeurs de la première dans la nouvelle et d'ajouter la valeur dans cette table. 

Analysons la complexité d'une séquence de $n$ opérations \textsf{insérer} sur une table vide. On note $c_i$ le coût de la $i^{\text{ième}}$ opération. Si la table n'est pas pleine, on a $c_i = 1$ sinon $c_i = i$. Si on effectue $n$  opérations \textsf{insérer}, le coût défavorable d'une opération est $O(n)$, ce qui donne une complexité en $O(n^2)$ pour la séquence. Cette borne n'est pas optimale, on va calculer cette complexité amortie, à l'aide des trois méthodes exposées dans la leçon.

\paragraph{Calcul par la méthode de l'agrégat} On se donne une séquence de $n$ opérations \textsf{insérer} sur une table vide. On souhaite affiner la borne précédente. On remarque que la $i^{\text{ième}}$ opération déclenche une extension si et seulement si $i -1$ est une puissance de deux. Le coût de la $i^{\text{ième}}$ opération est alors:
\begin{displaymath}
c_i = \left \{\begin{array}{cl}
i & \text{si } i -1 \text{ est une puissance de } 2 \\
1 & \text{sinon}
\end{array}
\right.
\end{displaymath}
Le coût total de $n$ opération \textsf{insérer} sur une table vide est :
\begin{displaymath}
\sum_{i = 1}^{n} c_i \leq n + \sum_{j = 1}^{\lfloor\log n\rfloor} 2^j < n + 2n = 3n
\end{displaymath}
Le coût amorti d'une opération \textsf{insérer} est au plus trois.

\paragraph{Calcul par la méthode comptable} La méthode comptable permet de donner l'intuition de la valeur trois. En effet, lorsqu'on insère un élément, on lui donne un crédit correspondant à son insertion, son mouvement et la recopie d'un élément présent dans le tableau avant lui \textblue{(comme on double la taille du tableau le nombre d'éléments qui ne viennent pas d'être ajouté à déplacer lorsque le tableau est plein est la moitié. Donc les ajouts financent bien la recopie des autres éléments)}. Plus formellement, on fixe:
\begin{displaymath}
\begin{array}{ccc}
T \text{ est non plein} & cred(op) = 2 & dep(op) = 0 \\
T \text{ est plein} & cred(op) = 2 & dep(op) = |T| \\
\end{array}
\end{displaymath}
Ce qui nous donne 
\begin{displaymath}
c_{amo}(op) = \left\{ \begin{array}{c}
1 + 2 -0 \\
(|T| + 1) + 2 - |T| \\
\end{array}\right. = 3
\end{displaymath}
D'où la complexité amortie de trois.

\paragraph{Calcul par la méthode du potentiel} On va maintenant appliquer la méthode du potentiel pour le calcul de cette complexité amortie. On souhaite une fonction potentielle qui vaut $0$ lorsque la table vient d'être étendu et qui vaut la taille de la table lorsque celle-ci est pleine. La fonction $\varphi(T) = 2T.num - T.taille$ où $T.num$ est le nombre d'élément dans la table et $T.taille$ est sa taille, est une possibilité. On considère la $i^{\text{ième}}$ opération. On distingue deux cas.
\begin{itemize}
	\item Si elle ne déclenche pas d'extension de la table, alors $taille_i = taille_{i-1}$ et
	\begin{displaymath}
	\begin{array}{cl}
	c_{amo}(i) & = c_i + \varphi_i - \varphi_{i-1} = 1+ (2num_i - taille_i) -(2num_{i-1} - taille_{i-1}) \\
	& = 1+ (2num_i - taille_i) -(2(num_i - 1) - taille_i) = 3\\
	\end{array}
	\end{displaymath}
	\item Si elle déclenche l'extension de la table, alors $taille_i = 2taille_{i-1}$, $taille_{i-1} = 2(num_i) - 1$ et 
	\begin{displaymath}
	\begin{array}{cl}
	c_{amo}(i) & = c_i + \varphi_i - \varphi_{i-1} = 1+ (2num_i - taille_i) -(2num_{i-1} - taille_{i-1}) \\
	& = 1+ (2num_i - taille_i) -(2(num_i - 1) - (num_i - 1)) = 3
	\end{array}
	\end{displaymath}
\end{itemize}