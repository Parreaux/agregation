%DEVs : SPA est NP-complet
%LECONs : 928 (NP-completude)
Le théorème de Cook est un théorème historique car c'est le premier résultat de NP-complétude. Ce résultat n'applique donc pas une réduction polynomial à partir d'un problème NP-dur mais il explique quel est la réduction polynomiale à partir d'un problème NP quelconque.

\paragraph{Quelques rappels sur la NP-complétude [Papadimitriou?]}

\begin{definition}
	La classe $P$ est l'ensemble des problèmes de décision (vu comme un langage) décidé par une machine de Turing (ou un algorithme) déterministe en temps polynomial en la taille de l'entrée.
\end{definition}

\begin{small}
	\begin{req}
	Une définition alternative existe : elle fait apparaître la robustesse de cette classe. En effet, $P$ est la réunion (sur $\N$) de tous les problèmes (langages) qui sont décidés par une machine de Turing déterministe en temps $O(n^k)$.
\end{req}
\end{small}

\begin{definition}
	La classe $NP$ est l'ensemble des problèmes de décision (vu comme un langage) décidé par une machine de Turing (ou un algorithme) non-déterministe en temps polynomial en la taille de l'entrée.
\end{definition}

\begin{definition}
	Un vérifieur pour un problème $A$ est une machine de Turing $\mathcal{M}$ déterministe telle que $w$ est une instance positive de $A$ si et seulement s'il existe $c$ tel que $\mathcal{M}$ accepte $(w, c)$. Un tel $c$ est appelé certificat.
\end{definition}

\begin{prop}[Définition alternative de $NP$ \protect{\cite[p.294]{Sisper}}]
	$A \in NP$ si et seulement si il existe un vérifieur en temps polynomial de $A$.
\end{prop}
	\begin{proof}[Idée de la démonstration]
	L'idée est de montrer comment convertir un vérifieur en une machine de Turing non-déterministe et réciproquement.
	
	\begin{minipage}{.46\textwidth}
		\begin{algorithm}[H]
			\begin{algorithmic}[1]
				\Procedure{$\mathcal{M}$}{$w$}
				\State Choisir $c$ de longueur $f(|w|)$
				\If{$V(w, c)$ accepte}
				\State Accepter
				\Else
				\State Rejeter
				\EndIf
				\EndProcedure
			\end{algorithmic}
			\caption{Algorithme convertissant un vérifieur en une machine de Turing non-déterministe.}
		\end{algorithm}
	\end{minipage}\hfill
	\begin{minipage}{.46\textwidth}
		\begin{algorithm}[H]
			\begin{algorithmic}[1]
				\Procedure{$V$}{$w,c$}
				\State Simuler l'exécution $\mathcal{M}(w)$ en prenant les choix conseillés par $c$.
				\If{l'exécution accepte}
				\State Accepter
				\Else
				\State Rejeter
				\EndIf
				\EndProcedure
			\end{algorithmic}
			\caption{Algorithme convertissant un vérifieur en une machine de Turing non-déterministe.}
		\end{algorithm}
	\end{minipage}
	\textcolor{white}{text}
\end{proof}

\begin{small}
	\begin{req}
	On a $P \subseteq NP$ et si $NP-C \cap P \neq \emptyset$ alors $P = NP$ où $NP-C$ est l'ensemble des problèmes $NP$-complets.
	\end{req}
\end{small}

\begin{definition}
	Une réduction polynomiale (Figure~\ref{fig:principeRedPoly}) de $A$ vers $B$ est une fonction $tr$ telle que
	\begin{itemize}
		\item $tr$ est calculable en temps polynomial;
		\item $w$ est une instance positive de $A$ si et seulement si $tr(w)$ est une instance positive de $B$.
	\end{itemize}
	On note alors $A \preccurlyeq B$.
\end{definition}

\begin{figure}
	\begin{minipage}{0.46\textwidth}
		\begin{tikzpicture}[auto, thick, >=latex]
		\draw
		% Drawing the blocks of first filter :
		node at (-1,0) [rectangle,draw=none, name=input1] {} 
		node at (1.5,0) [rectangle,draw=black] (red) {Réduction $tr$}
		node at (4,0) [rectangle,draw=none] (tr) {$tr(w)$}
		node at (5.75,0) [rectangle,draw=black] (B) {$B$}
		node at (7,0) [rectangle,draw=none] (fin) {};
		% Joining blocks. 
		% Commands \draw with options like [->] must be written individually
		\draw[->](input1) -- node[near start]{$w$}(red);
		\draw[-](red) -- node {} (tr);
		\draw[->](tr) -- node {} (B);
		\draw[->](B) -- node {} (fin);
		% Boxing and labelling noise shapers
		\draw [color=gray,thick](-0.25,-1) rectangle (6.5,1.5);
		\node at (-0.25,1.25) [above=5mm, right=0mm] {$A$};
		\end{tikzpicture}
		\caption{Schéma du principe de réduction polynomiale du problème $A$ au problème $B$.}
		\label{fig:principeRedPoly}				
	\end{minipage}\hfill
	\begin{minipage}{.46\textwidth}		
		\centering
		\begin{tikzpicture}[auto, thick, >=latex]
		\draw
		% Drawing the blocks of first filter :
		node at (-1,0) [rectangle,draw=none, name=input1] {} 
		node at (1.5,0) [rectangle,draw=black] (red) {Réduction $tr$}
		node at (4,0) [rectangle,draw=none] (tr) {$\varphi$}
		node at (5.75,0) [rectangle,draw=black] (B) {\textsc{SAT}}
		node at (7,0) [rectangle,draw=none] (fin) {};
		% Joining blocks. 
		% Commands \draw with options like [->] must be written individually
		\draw[->](input1) -- node[near start]{$w$}(red);
		\draw[-](red) -- node {} (tr);
		\draw[->](tr) -- node {} (B);
		\draw[->](B) -- node {} (fin);
		% Boxing and labelling noise shapers
		\draw [color=gray,thick](-0.25,-1) rectangle (6.5,1.5);
		\node at (-0.25,1.25) [above=5mm, right=0mm] {$A$};
		\end{tikzpicture}
		\caption{Schéma du principe de réduction polynomiale dans le théorème de Cook d'un problème $A$ dans $NP$ au problème \textsc{SAT}.}
		\label{fig:principeRedCook}	
	\end{minipage}
\end{figure}

\begin{theo}
	Si le problème $A$ se réduit au problème $B$, alors:
	\begin{itemize}
		\item si $B \in P$ alors $A \in P$;
		\item si $A \in NP$ alors $B \in NP$.
	\end{itemize}
\end{theo}

\begin{definition}
	Un problème $A$ est dit $NP$-dur si pour tout problème $B \in NP$, il existe une réduction polynomiale de $B$ à $A$. Si, de plus, $A \in NP$, alors $A$ est dit $NP$-complet
\end{definition}

\paragraph{Le théorème en lui-même}
Nous allons maintenant énoncé et donner l'idée de la preuve du théorème de Cook (qui fut le premier à exhiber un problème $NP$-complet).

\begin{definition}
	On définit le problème \textsc{SAT} pour les formules de la logique propositionnelle. 
	
	\begin{tabular}{rl}
		\emph{Problème}: & \textsc{SAT} \\
		\textbf{entrée} &  une formule $\phi$ de la logique propositionnelle \\
		\textbf{sortie} & Oui si $\phi$ est satisfiable; non sinon
	\end{tabular}
\end{definition}

\begin{theo}
	Le problème \textsc{SAT} est $NP$-complet.
\end{theo}
\begin{proof}[Idée de la démonstration] Montrons que e problème \textsc{SAT} est $NP$-complet.
	
	\subparagraph{$\textsc{SAT} \in NP$} Choisir une valuation pour chacune des variables de la formule (choix non-déterministe) et vérifier si c'est une valuation. On accepte lorsqu'on a trouvé une valuation et sinon on rejette.
	
	\subparagraph{\textsc{SAT} est $NP$-dur} Comme c'est la première démonstration qu'un problème est $NP$-complet, il nous faut utiliser la définition. On prend donc un problème $NP$ $B$ et on va le réduire au problème \textsc{SAT} (Figure~\ref{fig:principeRedCook}). Par définition, il existe une machine de Turing non-déterministe $\mathcal{M}$ qui décide $B$ en temps polynomial. On va alors coder l'exécution de cette machine dans des formules de la logique propositionnelle. Nous énonçons les propriétés que nous souhaitons coder sans en donner la traduction en formule de la logique propositionnelle.
	\begin{enumerate}
		\item La machine de Turing $\mathcal{M}$ et dans un état à tout instant $t$.
		\item La machine de Turing $\mathcal{M}$ n'est jamais dans deux états à la fois.
		\item Le curseur est positionné quelque part à tout instant $t$.
		\item Le curseur n'est jamais à deux positions différentes.
		\item À tout instant, toute case du ruban contient une lettre.
		\item Une case du ruban ne contient pas plus d'une lettre.
		\item À tout instant, on tire une transition pour aller vers l'instant $t +1$.
		\item On ne tire jamais plus d'une transition.
		\item À l'instant $0$, le ruban contient l'instance donnée à la machine.
		\item À l'instant $0$, la machine est dans l'état initial $q_0$ et son curseur est à la position $1$.
		\item La machine atteint son état d'acceptation.
		\item On ne change pas le contenu du ruban, si le curseur n'y ait pas.
		\item On ne tire qu'une transition de son état courant lisant la lettre sur le ruban.
		\item On change s'état lorsqu'on applique une transition (le bon état).
		\item On écrit la nouvelle valeur du ruban sur la case $i$.
		\item Le curseur change de position lors d'une transition.
	\end{enumerate}
	La formule que l'on cherche est la conjonction de toutes ses formules, ce qui prouve le théorème de Cook.
\end{proof}

\paragraph{Application} La première application de ce théorème est une technique de preuve plus agréable pour montrer la $NP$-dureté. En effet, maintenant que nous avons un premier problème $NP$-complet, nous allons pourvoir réduire les autres à celui et par transitivité de la relation est réductible à, on prouvera la $NP$-dureté de ce problème. Depuis on en a trouvé des nombreux.

