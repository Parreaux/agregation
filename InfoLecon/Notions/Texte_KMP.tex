%LECONs: 907; 926
%DEVs : automate des occurrences
Les algorithmes Morris--Pratt et Knuth--Morris--Pratt sont des algorithmes qui utilisent la notion de bord. On utilise le bord des préfixes du motif pour être plus astucieux et rapide quand une comparaison échoue: il nous donne le décalage que l'on doit réaliser.

\emph{Hypothèse}: Le motif est fixe et connu à l'avance. 

\subparagraph{Quelques notions sur le bord \cite[p.340]{Beauquier-Berstel-Chretienne}} Le bord a un rôle essentiel dans ces algorithmes : il permet de calculer le décalage que l'on va effectuer lors d'un échec de comparaison. Bien définir cette notion est donc primordiale.

\begin{definition}
	Un bord de $x$ est un mot distinct de $x$ qui est à la fois préfixe et suffixe de $x$. On note $Bord(x)$ le bord maximal d'un mot non vide.
\end{definition}

\emph{Exemple} Le mot $ababa$ possède les trois bords $\epsilon, a$ et $aba$. De plus $Bord(ababa) = aba$.

\begin{prop}
	Soit $x$ un mot non vide et $k \in \N$, le plus petit, tel que $Bord^k(x) = \epsilon$.
	\begin{enumerate}
		\item Les bords de $x$ sont les mots $Bord^i(x)$ pour tout $i \in \{1, \dots, k\}$.
		\item Soit $a$ une lettre. Alors, $Bord(xa)$ est le plus long préfixe de $x$ qui est dans l'ensemble $\{Bord(x)a, \dots Bord(x)^ka, \epsilon\}$.
	\end{enumerate}
\end{prop}
\begin{footnotesize}
	\begin{proof}[Arguments de la preuve]
	\begin{enumerate}
		\item $z$ est un bord de $Bord(x)$ ssi $z$ est un bord de $x$ \textblue{(+ récurrence)}.
		\item $z$ est un bord de $za$ ssi $z = \epsilon$ ou $z =z'a$ avec $z'$ un bord de $x$.
	\end{enumerate}
\end{proof}
\end{footnotesize}

\begin{cor}
	Soit $x$ un mot non vide et soit $a$ une lettre. Alors,
	\begin{displaymath}
	Bord(xa) = \left \{ 
	\begin{array}{ll}
	Bord(x)a & \text{si } Bord(x)a \text{ est préfixe de } x \\
	Bord(Bord(x)a) & \text{sinon} \\
	\end{array} \right.
	\end{displaymath}
\end{cor}
\begin{footnotesize}
	\begin{proof}[Arguments de la preuve]
	\begin{itemize}
		\item Si $Bord(x)a$ est préfixe de $x$ alors $Bord(x)a = Bord(xa)$.
		\item Sinon, $Bord(xa)$ est préfixe de $Bord(x) = y$ et le plus long préfixe de $x$ dans $\{Bord(y)a, \dots Bord(y)^ka, \epsilon\}$.
	\end{itemize}
\end{proof}
\end{footnotesize}

\begin{definition}
	Soit $x$ un mot de longueur $m$. On pose $\beta : \{0, \dots, m\} \to \{-1, \dots, m-1\}$ la fonction qui est définie comme suit: $\beta(0) = -1$ et pour tout $i> 0$, $\beta(i) = |Bord(x_1, \dots, x_i)|$. On pose $s : \{1, \dots, m-1\} \to \{0, \dots, m\}$ la fonction suppléance de $x$ définie comme suit : $s(i) = 1 + \beta(i-1)$, $\forall i \in \{1, \dots, m\}$.
\end{definition}

\emph{Remarque}: on a bien entendu $\beta(i) \leq i - 1$.


\begin{cor}
	oit $x$ un mot non vide de longueur $m$. Pour $j \in \{0, \dots, m-1\}$, on a $\beta(1+j) = 1 + \beta^k(j)$ où $k \geq 1$ est le plus petit entier vérifiant l'une des deux conditions suivantes:
	\begin{enumerate}
		\item $1 + \beta^k(j) = 0$
		\item $1 + \beta^k(j) \neq 0$ et $x_{1 + \beta^k(j)} = x_{j+1}$
	\end{enumerate}
\end{cor}
\begin{footnotesize}
	\begin{proof}[Arguments de la preuve]
	Algorithme~\ref{algo:bordMax}
\end{proof}
\end{footnotesize}

\begin{minipage}{.46\textwidth}
	\begin{algorithm}[H]
		\begin{algorithmic}[1]
			\Function{\textsc{Bord-Maximaux}}{$x, \beta$} 
			\Comment{$x$ mot de taille $m$}
			\State $\beta[0] \gets -1$ 
			\For{$j = 1$ à $m$}
			\State $i \gets \beta[j-1]$
			\While{$i \geq 0$ et $x[j] \neq x[i+1]$}
			\State $i \gets \beta[i]$
			\EndWhile
			\State $\beta[j] \gets i+1$
			\EndFor
			\EndFunction
		\end{algorithmic}
		\caption{Calcul de la fonction $\beta$ donnant la taille des bords maximaux d'un mot $x$.}
		\label{algo:bordMax}
	\end{algorithm}
\end{minipage} \hfill
\begin{minipage}{.46\textwidth}
	\begin{algorithm}[H]
		\begin{algorithmic}[1]
			\Function{\textsc{Suppléance}}{$x, s$} 
			\Comment{$x$ mot de taille $m$}
			\State $s[1] \gets 0$ 
			\For{$j = 1$ à $m-1$}
			\State $i \gets s[j]$
			\While{$i \geq 0$ et $x[j] \neq x[i]$}
			\State $i \gets s[i]$
			\EndWhile
			\State $s[j+1] \gets i+1$
			\EndFor
			\EndFunction
		\end{algorithmic}
		\caption{Calcul de la fonction suppléance $s$ du mot $x$.}
		\label{algo:suppleance}
	\end{algorithm}
\end{minipage}


\subparagraph{L'algorithme de Morris--Pratt \cite[p.916]{Cormen}} L'algorithme de Morris--Pratt utilise un automate des occurrences pour calculer les bords du motif. On définit l'automate des occurrences associé à $P[1 \dots m]$ comme suit: $\mathcal{Q} = \{1, \dots m\}$; $q_0 = 0$; $F = \{m\}$; $\delta(q, a) = \sigma(P_q a)$ pour tout $a \in \Sigma$ et $q \in \mathcal{Q}$.

\begin{definition}
	La fonction suffixe associé au motif $P$ $\sigma : \Sigma^{*} \mapsto \{0, 1, \dots, m\}$ donne la longueur de $Bord(x)$ pour $x \in \Sigma^{*}$.
\end{definition}

\begin{minipage}{.46\textwidth}
	\begin{algorithm}[H]
		\begin{algorithmic}[1]
			\Function{\textsc{Calcul-$\delta$}}{$P, \Sigma$} 
			\Comment{$P$ motif; $\Sigma$ alphabet}
			\State $p \gets P.longueur$ 
			\For{$q = 0$ à $p$}
			\For{$a \in \Sigma$}
			\State $k \gets \min(p+1, q+2)$
			\Repeat
			\State $k \gets k+1$
			\Until{$P_k$ préfixe de $P_qa$} \Comment{$P_q$ est le préfixe de $P$ de longueur $q$.}
			\State $\delta(q, a) \gets k$
			\EndFor
			\EndFor
			\State Retourner $\delta$
			\EndFunction
		\end{algorithmic}
		\caption{Calcul de la fonction de transition $\delta$ de l'automate des occurrences.}
		\label{algo:fonctionAutomate}
	\end{algorithm}
\end{minipage} \hfill
\begin{minipage}{.46\textwidth}
	\emph{Complexité temporelle du prétraitement dans le pire cas}: $O(p|\Sigma|)$ 
	\textblue{Ce fait une fois par motif.}
	\begin{algorithm}[H]
		\begin{algorithmic}[1]
			\Function{\textsc{Morris--Pratt}}{$T, \delta, p$} 
			\Comment{$T$ texte; $\delta$ fonction transition; $p$ la longueur du motif}
			\State $t \gets T.longueur$ 
			\For{$i = 1$ à $t$}
			\State $q \gets \delta(q, T[i])$
			\If{$q = p$}
			\State Retourner $i-m$
			\EndIf
			\EndFor
			\State Retourner $0$
			\EndFunction
		\end{algorithmic}
		\caption{Calcul de la recherche dans l'automate des occurrences.}
		\label{algo:MP}
	\end{algorithm}
	\emph{Complexité temporelle dans le pire cas}: $\Theta(t)$
\end{minipage}

\emph{Remarque}: La validité de cet algorithme provient de la validité de la construction de l'automate des occurrences.

\subparagraph{L'algorithme de Knuth--Morris--Pratt \cite[p.343]{Beauquier-Berstel-Chretienne}} Cet algorithme utilise une méthode plus astucieuse afin de calculer les bords du préfixes. On s'épargne ainsi un calcul de l'automate des occurrences et on calcul la fonction $\delta$ puisse à la volée. On exploite les propriétés de la fonction de bord.

\begin{definition}
	La fonction préfixe associé au motif $P$ $\pi : \{1, 2, \dots, p\} \mapsto \{0, 1, \dots, p-1\}$ donne la longueur du plus long préfixe de $P$ qui est suffixe propre de $P_q$ où $q \in \{1, 2, \dots, p\}$.
\end{definition}

\textblue{Si on ne fait pas l'hypothèse d'un préfixe propre alors le plus long suffixe d'un mot qui est aussi son préfixe est le mot en question. De plus, sans cette hypothèse l'algorithme de Knuth-Morris-Pratt serait soit incorrect soit non terminal.}

\begin{minipage}{.46\textwidth}
	\begin{algorithm}[H]
		\begin{algorithmic}[1]
			\Function{\textsc{KMP}}{$T, P$} 
			\Comment{$T$ texte; $P$ motif}
			\State $t \gets T.longueur$ 
			\State $p \gets P.longueur$
			\State $i \gets 1$
			\State $j \gets 1$
			\State $s \gets \textsc{Suppléance}(x, P)$
			\While{$i \leq p$ et $j \leq t$}
			\If{$i \geq 1$ et $t[j] \neq x[i]$}
			\State $i \gets s[i]$
			\Else
			\State $i \gets i + 1$
			\State $j \gets j + 1$
			\EndIf
			\EndWhile
			\If $i> m$ 
			\State Renvoie $j-m$
			\Else
			\State Renvoie $0$
			\EndIf
			\EndFunction
		\end{algorithmic}
		\caption{Algorithme de Knuth--Morris--Pratt.}
		\label{algo:KMP}
	\end{algorithm}
\end{minipage} \hfill
\begin{minipage}{.46\textwidth}
	\emph{Remarque}: La validité de l'algorithme de Knuth--Morris--Pratt (Algorithme~\ref{algo:KMP}) provient des propriétés sur le bord.
	
	\emph{Complexité du prétraitement \textblue{Calcul de la fonction suppléance $s$}}: $\Theta(p)$ On applique une méthode de l'agrégat: dans la boucle \textsf{pour} la variable ne peut augmenter plus de $p$ fois et comme elle est toujours strictement positive, la boucle \textsf{tant que} ne peut pas s'exécuter plus de $p$ fois.
	
	\emph{Complexité}: $\Theta(t)$ On applique une méthode de l'agrégat (exactement le même raisonnement).
	
	\emph{Remarque}: Il existe encore une version améliorer de cet algorithme qui consiste à ne pas vouloir se retrouver dans la même situation qu'au début (on teste toujours une situation différente). Cette amélioration donne les même complexité asymptotique mais semblerait plus rapide en pratique.

\end{minipage}


