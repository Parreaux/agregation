%DEVs : Validite FO
%LECONs : 914 (decidabilite) ; 913 (MT)
Nous allons présenter un résumé sur les problèmes de décisions pour les machines de Turing. La majorité (sinon la totalité) de ces problèmes sont indécidables. Nous donnerons alors quelques éléments de leur preuve d'indécidabilité et de leurs applications.

\begin{definition}
	Le \emph{problème de l'\textsc{Arrêt}} sur une machine de Turing déterministe.
	
	\noindent\begin{tabular}{rl}
		Problème & \textsc{Arrêt} \\
		\textbf{entrée}: & Une machine de Turing déterministe $M$; un mot $w$ \\
		\textbf{sortie } & Oui si $M(w)$ s'arrête; non sinon \\
	\end{tabular}
\end{definition}

\begin{theo}[Un premier problème indécidable \protect{\cite{Wolper}}]
	Le problème de l'\textsc{Arrêt} est indécidable et dans RE.
\end{theo}	
\begin{footnotesize}
	\begin{proof}[Idée de la preuve]
		On donne une machine de Turing universelle $\mathcal{U}$ qui accepte l’entrée $\mathcal{M}$,$w$ si et seulement si $\mathcal{M}$ s’arrête sur $w$. Ceci montre que le problème de l'arrêt est récursivement énumérable.
		
		Pour montrer que le problème de l'\textsc{Arrêt} est indécidable, on raisonne par l'absurde. Il existe alors une machine de Turing $\mathcal{A}$ telle que elle s'arrête pour tout entrée $\mathcal{M}$, $w$ et accepte une telle entrée si et seulement si $\mathcal{M}(w)$ termine. On construit une machine \textsc{paradoxe} (algorithme~\ref{algo:arret}). On remarque que \textsc{paradoxe}(\textsc{paradoxe}) ne termine pas si et seulement si $\mathcal{A}$ accepte $\left(\textsc{paradoxe},\left<\textsc{paradoxe}\right>\right)$ où $\left<\textsc{paradoxe}\right>$ est le codage de la machine de Turing \textsc{paradoxe}. Soit  \textsc{paradoxe}(\textsc{paradoxe}) ne termine pas si et seulement si \textsc{paradoxe}(\textsc{paradoxe}) termine. Contradiction.
	\end{proof}
\end{footnotesize}

\begin{minipage}{0.46\textwidth}
	\begin{algorithm}[H]
		\begin{algorithmic}[1]
			\Procedure{\textsc{Paradoxe}}{$\mathcal{M}$} X
			\If{$\mathcal{A}$ accepte $\left(\mathcal{M},\left<\mathcal{M}\right>\right)$}
			\State Boucler
			\Else
			\State Accepter
			\EndIf
			\EndProcedure
		\end{algorithmic}
		\captionof{algorithm}{La procédure \textsc{Paradoxe} de la preuve de l'indécidabilité du problème de l'\textsc{Arrêt}.}
		\label{algo:arret}
	\end{algorithm}
\end{minipage} \hfill
\begin{minipage}{0.46\textwidth}
	\begin{algorithm}[H]
		\begin{algorithmic}[1]
			\Procedure{$\mathcal{N}_{\mathcal{M}, w}$}{$x$} 
			\State $\mathcal{M}(w)$.
			\If{$G$ accepte $x$}
			\State accepter
			\Else
			\State rejeter
			\EndIf
			\EndProcedure
		\end{algorithmic}
		\captionof{algorithm}{La procédure $\mathcal{N}_{\mathcal{M}, w}$ de la preuve du théorème de Rice.}
		\label{algo:Rice}
	\end{algorithm}
\end{minipage}

\noindent\emph{Applications} : 
\begin{itemize}
	\item Le problème de l'\textsc{Arrêt} est le premier problème que nous avons montrer qu'il était indécidable, nous allons donc l'utiliser pour des réductions afin de montrer qu'il n'est pas le seul problème qui est indécidable.
	\item Le théorème de Rice et ses applications.
	\item Si on identifie une machine de Turing à un langage de programmation (qui contient une boucle \textsf{while}, par exemple IMP), on vient de montrer que déterminer si un programme d'un tel langage est termine indécidable.
\end{itemize}

\begin{theo}[Rice \protect{\cite{Wolper}}]
	Pour toute propriété non triviale $\mathcal{P}$ sur les langages récursivement énumérables, le problème de savoir si le langage $L(\mathcal{M})$ d'une machine de Turing $\mathcal{M}$ vérifie $\mathcal{P}$ est indécidable.
\end{theo}	
\begin{footnotesize}
	\begin{proof}
		Sans perte de généralité, on suppose que $\emptyset \in \mathcal{P}$. On définit le problème $P_{\mathcal{P}}$.
		
		\noindent\begin{tabular}{rl}
			Problème & $P_{\mathcal{P}}$ \\
			\textbf{entrée}: & Une machine de Turing $\mathcal{M}$ \\
			\textbf{sortie}: & Oui si $L\left(\mathcal{M}\right) \in \mathcal{P}$; non sinon \\
		\end{tabular}
		
		Réduisons \textsc{Arrêt} à $P_{\mathcal{P}}$ (voir Figure~\ref{fig:riceRed}). Soit $G \in \mathcal{P}$. Comme $G \in RE$, il existe une machine $G$ qui accepte $G$. La réduction $tr$ est définie par $tr(\mathcal{M},w)=\mathcal{N}_{\mathcal{M},w}$ où $\mathcal{N}_{\mathcal{M},w}$ est la machine décrite dans l'algorithme~\ref{algo:Rice}.
		\begin{enumerate}
			\item $tr$ est une fonction calculable : on construit effectivement $\mathcal{N}_{\mathcal{M},w}$ à partir de $\mathcal{M}$ et $w$;
			\item $(\mathcal{M},w)$ instance positive de \textsc{Arrêt} si et seulement si $\mathcal{M}$ s’arrête sur $w$. Comme \begin{displaymath}
			L\left(\mathcal{N}_{\mathcal{M},w}\right) = \left \{ \begin{array}{cc}
			G & \text{si } \mathcal{M} \text{ s'arrête sur } w \\
			\empty & \text{sinon} \\
			\end{array}
			\right.
			\end{displaymath} $(\mathcal{M},w)$ instance positive de \textsc{Arrêt} si et seulement si $L\left(\mathcal{N}_{\mathcal{M},w}\right) \in \mathcal{P}$. Donc, $(\mathcal{M},w)$ instance positive de \textsc{Arrêt} si et seulement si $tr(\mathcal{M},w)=\mathcal{N}_{\mathcal{M},w}$ est instance positive de $\mathcal{P}$.
		\end{enumerate}	
	\end{proof}
\end{footnotesize}

\noindent\emph{Applications}:
\begin{itemize}
	\item Si on considère $\mathcal{P} = \emptyset$, le problème \textsc{LangageVide} est indécidable.
	\noindent\begin{tabular}{rl}
		Problème & \textsc{LangageVide} \\
		\textbf{entrée}: & Une machine de Turing $\mathcal{M}$ \\
		\textbf{sortie}: & Oui si $L\left(\mathcal{M}\right) = \emptyset$; non sinon \\
	\end{tabular}
	
	\item Si on identifie une machine de Turing à un langage de programmation, on vient de montrer que la correction d'un programme est un problème indécidable.
\end{itemize}



\begin{definition}
	Le \emph{problème de l'\textsc{Acceptation}} sur une machine de Turing déterministe.
	
	\noindent\begin{tabular}{rl}
		Problème & \textsc{Acceptation} \\
		\textbf{entrée}: & Une machine de Turing déterministe $M$; un mot $w$ \\
		\textbf{sortie } & Oui si $M$ accepte $w$; non sinon \\
	\end{tabular}
\end{definition}

\begin{theo}
	Le problème de l'\textsc{Acceptation} est indécidable.
\end{theo}	
\begin{footnotesize}
	\begin{proof}[Idée de la démonstration]
		On réduit le problème de l'\textsc{Arrêt} au problème de l'\textsc{Acceptation}: on construit une machine de Turing qui accepte $w$ si et seulement si $\mathcal{M}$ s'arrête sur $w$ où $\mathcal{M}, w$ est une instance de \textsc{Arrêt}.
	\end{proof}
\end{footnotesize}

\emph{Application}: Nous permet de montrer que le problème \textsc{Post} est indécidable.

\begin{definition}
	Le \emph{problème de \textsc{Post}} sur une famille de tuile.
	
	\noindent\begin{tabular}{rl}
		Problème & \textsc{Post} \\
		\textbf{entrée}: & Un alphabet fini, $\Sigma$ et une famille finie de couples de mots $((h_i, bi)_{i=1\dots n})$ sur $\Sigma$\\
		\textbf{sortie } & Oui s'il existe $i_1, \dots i_k \in \{1,\dots n\}$ tels que $h_{i_1}\dots h_{i_k} = b_{i_1}\dots b_{i_k}$; non sinon \\
	\end{tabular}
\end{definition}

\begin{theo}
	Le problème de \textsc{Post} est indécidable.
\end{theo}	
\begin{footnotesize}
	\begin{proof}[Idée de la démonstration]
		On réduit le problème de \textsc{Post marqué} au problème de \textsc{Post} où \textsc{Post marqué}  est un problème analogue à \textsc{Post} mais dont la première tuile est définie. On montre l'indécidabilité de \textsc{Post marqué} par réduction du problème de l'\textsc{Acceptation} dans \textsc{Post marqué}.
	\end{proof}
\end{footnotesize}

\emph{Application}: En utilisant le même principe de réduction, on montre par exemple que le problème de validité de la logique du premier ordre est indécidable.