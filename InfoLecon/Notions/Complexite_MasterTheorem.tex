%LECONs: 926
Le Master Theorem \cite[p.30]{Beauquier-Berstel-Chretienne} est un résultat qui nous permet d'étudier plus précisément certaines récurrences par partitions. Un grand nombre de situations d'analyse d'algorithmes (notamment d'algorithmes récursifs) sont couvertes par ce résultat.

\begin{theo}[Master Theorem]
	Soit $t : \N \to \R^+$ croissante à partir d'un certain rang. Soient $n_0 \geq 0$, $b \geq 2$ des entiers et $k \geq 0$, $a, c, d > 0$ des réels positifs tels que 
	\begin{displaymath}
	\left \{ 
	\begin{array}{ccll}
	t(n_0) & = & d & \\
	t(n) & = & a t\left(\frac{n}{d}\right)+cn^k & \text{pour } \frac{n}{n_0} \text{ une puissance de } b\\
	\end{array}
	\right.
	\end{displaymath}
	Alors, on a :
	\begin{displaymath}
	t(n) = \left \{ 
	\begin{array}{c@{\text{ si }}l}
	\Theta(n^k) & a < b^k\\
	\Theta(n^k\log_b(n)) & a = b^k\\
	\Theta(n^{\log_b(a)}) & a > b^k\\
	\end{array}
	\right.
	\end{displaymath}
\end{theo}
\begin{proof}
	Supposons d'abord que $n = n_0b^p$ pour un certain entier $p$. On écrit alors $T(n)$ sous la forme:
	\begin{displaymath}
	\begin{array}{ccl}
	T(n) & = & a(aT\left(\frac{n}{b^2}\right) + c\left(\frac{n}{b}\right)^k) + cn^k \\
	& = & \underbrace{a^pd}_{\gamma(n)} + cn^k\underbrace{\sum_{i=0}^{p} \frac{a^i}{b^ki}}_{\delta(n)}
	\end{array}
	\end{displaymath}
	
	On a, de plus,
	\begin{displaymath}
	\begin{array}{c}
	\gamma(n) = a^pd = a^{\log_b(n)-\log_b(n_0)}d = \Theta(n^{\log_b(a)}) \\
	a^{\log_b(n)} = b^{\log_b(a)\log_b(n)} = n^{\log_b(a)}
	\end{array}
	\end{displaymath}
	
	On distingue alors trois cas: 
	\begin{itemize}
		\item Si $a < b^k$, alors $\delta(n)$ converge et
		\begin{displaymath}
		T(n) = \Theta(n^{\log_b(a)}) + \Theta(n^k) = \Theta(n^k)
 		\end{displaymath}
		
 		\item Si $a < b^k$, $\delta(n) \sim cn^k(p+1) = \Theta(n^k\log_b(n))$ et 
 		\begin{displaymath}
 		T(n) = \Theta(n^{\log_b(a)}) + \Theta(n^k\log_b(n)) = \Theta(n^k\log_b(n))
 		\end{displaymath}
		
		\item Si $a < b^k$, $\delta(n) = \frac{(a/b^k)^{p+1} - 1}{(a/b^k) - 1} \sim \alpha\left(\frac{a}{b^k}\right)^{p+1}$ avec $\alpha = \frac{1}{(a/b^k) - 1}$ et 
		\begin{displaymath}
		\begin{array}{c}
		cn^k \alpha\left(\frac{a}{b^k}\right)^{p+1} = cp^{pk}n_0^k \frac{a^{p+1}}{b^{pk}b^k} \sim c \frac{n_0^k}{b^k}n^{\log_b(a)}
		T(n) = \Theta(n^{\log_b(a)}) + \Theta(n^{\log_b(a)}) = \Theta(n^{\log_b(a)})
		\end{array}
		\end{displaymath}
	\end{itemize}
	D'où le théorème dans le où $n = b^pn_0$ pour un certain $p \in \N$.
	
	Soit $n \in \N^*$, alors il existe $p$ tel que $b^pn_0 \leq n < b^{p+1}n_0$. Comme $T$ est croissante à partir d'un certain rang \textblue{(hypothèse)}, pour $n$ assez grand, $T(b^pn_0) \leq T(n) < T(b^{p+1}n_0)$. En appliquant ce qui précède aux deux membres extrêmes de la relation, $T(n)$ vérifie les conditions du théorème. D'où le résultat.
	
\end{proof}

\emph{Contre-exemple}: Le tri rapide (paradigme diviser pour régner) ne rentre pas dans le cadre du théorème car les sous-problème ne sont pas de taille connue.

\subparagraph{Exemple d'application : le tri fusion}
\input{./../Notions/Tri_fusion.tex}

\subparagraph{Exemple d'application : le produit matricielle de Strassen}