Référence : \cite[p.287]{Cormen}

Les arbres rouge-noir sont des arbres approximativement équilibrée car en rajoutant un unique bit (sa couleur) dont on contrôle sa valeur nous pouvons garantir qu'aucun des chemins n'est plus de deux fois plus long que n'importe quel autre chemin. 

\paragraph{Définition des arbres rouge-noir} On ajout alors l'attribue \textsc{Couleur} qui peut prendre les valeurs \textsc{rouge} et \textsc{noir}, à chacun des nœuds de l'arbre.

\begin{definition}
	Un arbre rouge-noir est un arbre vérifiant les propriétés rouge-noir suivantes:
	\begin{enumerate}
		\item chaque nœud est soit rouge, soit noir;
		\item la racine est noire;
		\item chaque feuille est noire;
		\item si un nœud est rouge, alors ses deux enfants sont noirs;
		\item pour chaque nœud, tous les chemins simples reliant le nœud à des feuilles situées plus bas contiennent le même nombre de nœuds noirs.
	\end{enumerate}
	On peut alors définir la notion de hauteur noire (notée $bh$) d'un nœud qui représente le nombre de nœuds noirs dans un chemin simple du nœud.
\end{definition}

\begin{req}
	Lors de l'implémentation, nous utilisons souvent une sentinelle (qui possède déjà la couleur noire) pour représenter toutes les feuilles. 
\end{req}

\begin{lemme}
	Un arbre rouge-noir ayant $n$ nœuds internes a une hauteur au plus égale à $2\log(n+1)$.
\end{lemme}
\begin{proof}[Idée de la preuve]
	On montre par récurrence sur la hauteur d'un nœud $x$ que le sous-arbre enraciné en $x$ contient au moins $2^{bh(x)} -1$ nœuds internes.
	
	On remarque ensuite que d'après la propriété 4 (dans la définition) que $bh(r) \geq \frac{h}{2}$ où $r$ est la racine de l'arbre et $h$ est sa hauteur.
	
	On obtient le résultat en passant au $\log$.
\end{proof}

\emph{Conséquences}: On peut effectuer les actions d'adjonctions, de suppressions et de recherche en $O(\log n)$.

\paragraph{Rotations} Comme pour les AVL, insérer ou supprimer un élément dans cet arbre peut modifier sa structure. Pour cela, on utilise la même rotation droite (Figure \ref{fig:Droit}). Cependant, on définit la rotation gauche comme la réciproque de la rotation droite (Figure \ref{fig:Droit}).

\paragraph{Insertion dans un arbre rouge-noir} Pour insérer un nouveau élément dans l'arbre, on le descend au feuille (ses fils pointent sur la sentinelle) et on met alors sa couleur à \textsc{rouge}. Cette action risque alors de violer les propriétés rouge-noir. Il nous faut donc les corriger.

On commence par remarqué que seules les propriétés 2 (quand $x$ devient la racine) et 5 (quand le parent de $x$ est également rouge) peuvent être violées. On va alors étudier ces différents cas.

\subparagraph{Cas 1: l'oncle de $x$, $y$, est \textsc{rouge}}
Le cas 1 (Figure \ref{fig:ARN_insert_Cas1}) se produit lorsque que $x$ et son parent $A$ sont tout deux \textsc{rouge}. Comme $A$ est \textsc{rouge}, on peut colorier son parent $C$ (qui est \textsc{noir}) en \textsc{rouge}. Dans ce cas, $A$ et $y$ sont à lors tour colorier en \textsc{noir}. On applique alors récursivement la procédure à $C$ qui devient le nouveau $x$, $x'$.

\begin{figure}
	%\centering
	\begin{minipage}[c]{.46\linewidth}
		\begin{tikzpicture}[level/.style={sibling distance=10em/#1, level distance=2.5em}]
		\node [circle,draw,fill=black, text=white] (c){$\textbf{C}$}
		child {node [circle,draw,fill=BrickRed, text=white] (a) {$\textbf{A}$}
			child {node {$\alpha$}}
			child {node [circle,draw,fill=BrickRed, text=white] (b) {$\textbf{x}$}
				child {node {$\beta$}}
				child {node {$\gamma$}}
			}
		}
		child {node [circle,draw,fill=BrickRed, text=white] (d) {$\textbf{y}$}
			child {node {$\delta$}}
			child {node {$\eta$}}
		};
		\end{tikzpicture}
	\end{minipage} \hfill
	\begin{minipage}[c]{.46\linewidth}
		\begin{tikzpicture}[level/.style={sibling distance=10em/#1, level distance=2.5em}]
		\node [circle,draw,fill=BrickRed,text=white] (c){$\textbf{C = x'}$}
		child {node [circle,draw,fill=black,text=white] (a) {$\textbf{A}$}
			child {node {$\alpha$}}
			child {node [circle,draw,fill=BrickRed,text=white] (b) {$\textbf{x}$}
				child {node {$\beta$}}
				child {node {$\gamma$}}
			}
		}
		child {node [circle,draw,fill=black,text=white] (d) {$\textbf{y}$}
			child {node {$\delta$}}
			child {node {$\eta$}}
		};
		\end{tikzpicture}
	\end{minipage}
	\caption{Résolution du cas 1 de l'insertion, lorsque $x$ est fils gauche (le cas à droite se traite de manière analogue)}
	\label{fig:ARN_insert_Cas1}	
\end{figure}

\subparagraph{Cas 2 : l'oncle de $x$, $y$ est \textsc{noir} et $x$ est un enfant de droite.}
Dans le cas 2 (comme dans le cas 3), $y$ est toujours noir. Les deux cas ne diffère que par le côté d'insertion de $x$. On va alors utiliser la rotation gauche sur le parent de $x$, $A$ pour se ramener au cas 3 (Figure \ref{fig:ARN_insert_Cas2}).

\begin{figure}
	%\centering
	\begin{minipage}[c]{.46\linewidth}
		\begin{tikzpicture}[level/.style={sibling distance=10em/#1, level distance=2.5em}]
		\node [circle,draw,fill=black, text=white] (c){$\textbf{C}$}
		child {node [circle,draw,fill=BrickRed, text=white] (a) {$\textbf{A}$}
			child {node {$\alpha$}}
			child {node [circle,draw,fill=BrickRed, text=white] (b) {$\textbf{x}$}
				child {node {$\beta$}}
				child {node {$\gamma$}}
			}
		}
		child {node {$\delta = y$}};
		\end{tikzpicture}
	\end{minipage} \hfill
	\begin{minipage}[c]{.46\linewidth}
		\begin{tikzpicture}[level/.style={sibling distance=10em/#1, level distance=2.5em}]
		\node [circle,draw,fill=black, text=white] (c){$\textbf{C}$}
		child {node [circle,draw,fill=BrickRed, text=white] (b) {$\textbf{x}$}
			child {node [circle,draw,fill=BrickRed, text=white] (a) {$\textbf{A = x'}$}
				child {node {$\alpha$}}
				child {node {$\beta$}}
			}
			child {node {$\gamma$}}
		}
		child {node {$\delta = y$}};
		\end{tikzpicture}
	\end{minipage}
	\caption{Passage du cas 2 de l'insertion au cas 3 de l'insertion.}
	\label{fig:ARN_insert_Cas2}	
\end{figure}

\subparagraph{Cas 3 : l'oncle de $x$, $y$ est \textsc{noir} et $x$ est un enfant de gauche.}
Le cas 3 arrive lorsque $y$ est \textsc{noir} et que $x$ a été insérer à gauche. On commence alors par effectuer une rotation droite sur le parent du parent de $x$ (qui est $B$), $C$. Ensuite, on met $B$ (qui est alors la nouvelle racine du sous-arbre) à \textsc{noir} et $C$ prend alors la couleur \textsc{rouge}. On ne crée aucune violation donc on n'a pas besoin de réitérer la correction.
 
\begin{figure}
	%\centering
	\begin{minipage}[c]{.46\linewidth}
		\begin{tikzpicture}[level/.style={sibling distance=10em/#1, level distance=2.5em}]
		\node [circle,draw,fill=black, text=white] (c){$\textbf{C}$}
		child {node [circle,draw,fill=BrickRed, text=white] (b) {$\textbf{B}$}
			child {node [circle,draw,fill=BrickRed, text=white] (a) {$\textbf{x}$}
				child {node {$\alpha$}}
				child {node {$\beta$}}
			}
			child {node {$\gamma$}}
		}
		child {node {$\delta = y$}};
		\end{tikzpicture}
	\end{minipage} \hfill
	\begin{minipage}[c]{.46\linewidth}
		\begin{tikzpicture}[level/.style={sibling distance=10em/#1, level distance=2.5em}]
		\node [circle,draw,fill=black,text=white] (b){$\textbf{B}$}
		child {node [circle,draw,fill=BrickRed,text=white] (a) {$\textbf{x}$}
			child {node {$\alpha$}}
			child {node {$\beta$}}
		}
		child {node [circle,draw,fill=BrickRed,text=white] (c) {$\textbf{C}$}
			child {node {$\gamma$}}
			child {node {$\delta$}}
		};
		\end{tikzpicture}
	\end{minipage}
	\caption{Résolution du cas 3 de l'insertion.}
	\label{fig:ARN_insert_Cas3}	
\end{figure}

\emph{Complexité}: On n'effectue jamais plus de deux rotations. On a donc une complexité en $O(\log n)$ comme annoncé.


\paragraph{Suppression dans un arbre rouge-noir}
La suppression d'un nœud $x$ dans un arbre rouge-noir est potentiellement plus complexe que l'insertion. Comme l'insertion d'un nœud, la suppression peut violé les propriétés de l'arbre qu'il faut alors rétablir. 

\emph{Les différences avec la suppression dans un arbre}:
\begin{itemize}
	\item Le nœud $y$ est supprimé ou déplacé dans l'arbre. Lorsque $y$ est déplacé, il prend la place de $x$ (sans condition).
	\item Comme la couleur de $y$ risque de changer (quand $y$ prend la place de $x$, il conserve également la couleur de $x$), nous conservons en mémoire sa couleur d'origine. En effet si la couleur de $y$ est \textsc{noir} nous pouvons avoir un problème.
	\item Le nœud $z$ prend alors la place de $y$ et le parent de $z$ reste toujours le parent originel de $y$. 
\end{itemize}

Comme lors de l'insertion, nous risquons de violer les propriétés rouge-noir. Nous devons alors les corriger. l'idée fondamentale de cette correction est de conserver le nombre de nœuds \textsc{noir}. Nous allons présenter les différents cas de corrections.

\subparagraph{Cas 1 : le frère de $z$, $w$ est \textsc{rouge}.}

Le cas 1 se produit lorsque le frère de $z$, $w$ est \textsc{rouge}. Comme $w$ doit avoir des enfants \textsc{noir} (et que son père $B$ est nécessairement \textsc{noir}), on peut permuter les couleurs de $B$ et de $w$. On finit alors par une rotation gauche (Figure \ref{fig:ARN_suppr_Cas1}).

\begin{figure}
	%\centering
	\begin{minipage}[c]{.46\linewidth}
		\begin{tikzpicture}[level/.style={sibling distance=10em/#1, level distance=2.5em}]
		\node [circle,draw,fill=black, text=white] (b){$\textbf{B}$}
		child {node [circle,draw,fill=black, text=white] (a) {$\textbf{z}$}
			child {node {$\alpha$}}
			child {node {$\beta$}}
		}
		child {node [circle,draw,fill=BrickRed, text=white] (d) {$\textbf{w}$}
			child {node [circle,draw,fill=black, text=white] (c) {$\textbf{C}$}
				child {node {$\gamma$}}
				child {node {$\delta$}}
			}
			child {node [circle,draw,fill=black, text=white] (e) {$\textbf{E}$}
				child {node {$\eta$}}
				child {node {$\zeta$}}
			}
		};
		\end{tikzpicture}
	\end{minipage} \hfill
	\begin{minipage}[c]{.46\linewidth}
		\begin{tikzpicture}[level/.style={sibling distance=10em/#1, level distance=2.5em}]
		\node [circle,draw,fill=black,text=white] (d){$\textbf{w}$}
		child {node [circle,draw,fill=BrickRed,text=white] (b) {$\textbf{B}$}
			child {node [circle,draw,fill=black,text=white] (a) {$\textbf{z}$}
				child {node {$\alpha$}}
				child {node {$\beta$}}
			}
			child {node [circle,draw,fill=black,text=white] (c) {$\textbf{C = w'}$}
				child {node {$\gamma$}}
				child {node {$\delta$}}
			}
		}
		child {node [circle,draw,fill=black,text=white] (e) {$\textbf{E}$}
			child {node {$\eta$}}
			child {node {$\zeta$}}
		};
		\end{tikzpicture}
	\end{minipage}
	\caption{Résolution du cas 1 de la suppression.}
	\label{fig:ARN_suppr_Cas1}	
\end{figure}


\subparagraph{Cas 2 : le frère de $z$, $w$ est \textsc{noir} et les deux enfants de $w$ sont \textsc{noir}s.}

Le cas 2 se produit lorsque le frère de $z$, $w$ est \textsc{noir} et les deux enfants de $w$ sont \textsc{noir}s. On enlève alors un \textsc{noir} à $x$ et à $w$ ce qui laisse $w$ à \textsc{rouge}. Pour compenser le passage de $w$ à rouge, son parent $B$ peut doit devenir \textsc{rouge} alors qu'il pouvait être des deux couleurs. Il faut donc rappliqué l'opération à $B$ qui devient le nouveau $x$, $x'$ (Figure \ref{fig:ARN_suppr_Cas2}).

\begin{figure}
	%\centering
	\begin{minipage}[c]{.46\linewidth}
		\begin{tikzpicture}[level/.style={sibling distance=10em/#1, level distance=2.5em}]
		\node [circle,draw,fill=Gray] (b){$\textbf{B}$}
		child {node [circle,draw,fill=black,text=white] (a) {$\textbf{z}$}
			child {node {$\alpha$}}
			child {node {$\beta$}}
		}
		child {node [circle,draw,fill=black, text=white] (d) {$\textbf{w}$}
			child {node [circle,draw,fill=black, text=white] (c) {$\textbf{C}$}
				child {node {$\gamma$}}
				child {node {$\delta$}}
			}
			child {node [circle,draw,fill=black, text=white] (e) {$\textbf{E}$}
				child {node {$\eta$}}
				child {node {$\zeta$}}
			}
		};
		\end{tikzpicture}
	\end{minipage} \hfill
	\begin{minipage}[c]{.46\linewidth}
		\begin{tikzpicture}[level/.style={sibling distance=10em/#1, level distance=2.5em}]
		\node[circle,draw,fill=Gray,text=black] (b) {$\textbf{B = x'}$}
		child {node [circle,draw,fill=black,text=white] (a) {$\textbf{z}$}
			child {node {$\alpha$}}
			child {node {$\beta$}}
		}
		child {node [circle,draw,fill=BrickRed, text=white] (d) {$\textbf{w}$}
			child {node [circle,draw,fill=black, text=white] (c) {$\textbf{C}$}
				child {node {$\gamma$}}
				child {node {$\delta$}}
			}
			child {node [circle,draw,fill=black, text=white] (e) {$\textbf{E}$}
				child {node {$\eta$}}
				child {node {$\zeta$}}
			}
		};
		\end{tikzpicture}
	\end{minipage}
	\caption{Résolution du cas 2 de la suppression (le nœud gris peut être \textsc{rouge} ou \textsc{noir} mais il conserve sa couleur durant la transformation).}
	\label{fig:ARN_suppr_Cas2}	
\end{figure}


\subparagraph{Cas 3 : le frère de $z$, $w$ est \textsc{noir} et l'enfant gauche de $w$ est \textsc{rouge} et l'enfant droit de $w$ est \textsc{noir}.}

Le cas 3 se produit quand le frère de $z$, $w$ est \textsc{noir} et le fils gauche de $w$, $C$, est \textsc{rouge} et le fils droit de $w$, $E$ est \textsc{noir}. On va chercher à se ramener au cas 4. Comme $E$ est \textsc{noir}, on peut permuter la couleur de $w$ (\textsc{noir}) avec celle de $C$, (\textsc{rouge}). Puis on effectuer une rotation droite sur $w$ (sans violer les propriétés de l'arbre) ce qui nous ramène au cas 4 (Figure \ref{fig:ARN_suppr_Cas3}).


\begin{figure}
	%\centering
	\begin{minipage}[c]{.46\linewidth}
		\begin{tikzpicture}[level/.style={sibling distance=10em/#1, level distance=2.5em}]
		\node [circle,draw,fill=Gray] (b){$\textbf{B}$}
		child {node [circle,draw,fill=black,text=white] (a) {$\textbf{z}$}
			child {node {$\alpha$}}
			child {node {$\beta$}}
		}
		child {node [circle,draw,fill=black, text=white] (d) {$\textbf{w}$}
			child {node [circle,draw,fill=BrickRed, text=white] (c) {$\textbf{C}$}
				child {node {$\gamma$}}
				child {node {$\delta$}}
			}
			child {node [circle,draw,fill=black, text=white] (e) {$\textbf{E}$}
				child {node {$\eta$}}
				child {node {$\zeta$}}
			}
		};
		\end{tikzpicture}
	\end{minipage} \hfill
	\begin{minipage}[c]{.46\linewidth}
		\begin{tikzpicture}[level/.style={sibling distance=10em/#1, level distance=2.5em}]
		\node [circle,draw,fill=Gray] (b){$\textbf{B}$}
		child {node [circle,draw,fill=black,text=white] (a) {$\textbf{z}$}
			child {node {$\alpha$}}
			child {node {$\beta$}}
		}
		child {node [circle,draw,fill=black, text=white] (c) {$\textbf{C = w'}$}
			child {node {$\gamma$}}
			child {node [circle,draw,fill=BrickRed, text=white] (d) {$\textbf{w}$}
				child {node {$\delta$}}
				child {node [circle,draw,fill=black, text=white] (e) {$\textbf{E}$}
					child {node {$\eta$}}
					child {node {$\zeta$}}
				}
			}
		};
		\end{tikzpicture}
	\end{minipage}
	\caption{Résolution du cas 3 de la suppression (le nœud gris peut être \textsc{rouge} ou \textsc{noir} mais il conserve sa couleur durant la transformation).}
	\label{fig:ARN_suppr_Cas3}	
\end{figure}

\subparagraph{Cas 4 : le frère de $z$, $w$ est \textsc{noir} et l'enfant droit de $w$ est \textsc{rouge}.}
Le cas 4 se produit lorsque le frère de $z$, $w$ est \textsc{noir} et le fils droit de $w$, $E$, est \textsc{rouge}. En modifiant les couleurs : $E$ et le parent de $w$, $B$, deviennent \textsc{noir} et $w$ prend la couleur de $B$; on ne violent pas les propriétés rouge-noir. En effectuant une rotation gauche sur $B$, on supprime le noir supplémentaire. On obtient alors un arbre correct (Figure \ref{fig:ARN_suppr_Cas4}).

\begin{figure}
	%\centering
	\begin{minipage}[c]{.46\linewidth}
		\begin{tikzpicture}[level/.style={sibling distance=10em/#1, level distance=2.5em}]
		\node [circle,draw,fill=Gray] (b){$\textbf{B}$}
		child {node [circle,draw,fill=black,text=white] (a) {$\textbf{z}$}
			child {node {$\alpha$}}
			child {node {$\beta$}}
		}
		child {node [circle,draw,fill=black, text=white] (d) {$\textbf{w}$}
			child {node [circle,draw,fill=darkgray, text=white] (c) {$\textbf{C}$}
				child {node {$\gamma$}}
				child {node {$\delta$}}
			}
			child {node [circle,draw,fill=BrickRed, text=white] (e) {$\textbf{E}$}
				child {node {$\eta$}}
				child {node {$\zeta$}}
			}
		};
		\end{tikzpicture}
	\end{minipage} \hfill
	\begin{minipage}[c]{.46\linewidth}
		\begin{tikzpicture}[level/.style={sibling distance=10em/#1, level distance=2.5em}]
		\node [circle,draw,fill=Gray] (d){$\textbf{w}$}
		child {node [circle,draw,fill=black,text=white] (b) {$\textbf{B}$}
			child {node [circle,draw,fill=black,text=white] (a) {$\textbf{z}$}
				child {node {$\alpha$}}
				child {node {$\beta$}}
			}
			child {node [circle,draw,fill=darkgray, text=white] (c) {$\textbf{C}$}
				child {node {$\gamma$}}
				child {node {$\delta$}}
			}
		}
		child {node [circle,draw,fill=black, text=white] (e) {$\textbf{E}$}
				child {node {$\eta$}}
				child {node {$\zeta$}}
		};
		\end{tikzpicture}
	\end{minipage}
	\caption{Résolution du cas 4 de la suppression (les nœuds gris peut être \textsc{rouge} ou \textsc{noir} mais ils conservent leur couleur durant la transformation). Dans ce cas, le nouveau $x$ est la racine de l'arbre.}
	\label{fig:ARN_suppr_Cas4}	
\end{figure}

\emph{Complexité}: Quelque soit le cas que l'on prend, on fait un nombre constant de changement de couleur et on effectue au plus trois rotations. La suppression reste donc en $O(\log n)$ comme annoncé.