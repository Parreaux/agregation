%DEVs : Validite FO; problème PSA
%LECONs : 914 (decidabilite) ; 913 (MT); 920 (NP-complet)
Pour montrer qu'un problème apparaît dans une certaine classe de complexité (et même sa dureté) ou qu'il est indécidable, nous utilisons une technique de preuve : la réduction. Pour appliquer le principe de réduction il nous faut connaître un premier problème possédant les propriétés que l'on souhaite montrer sur le deuxième. 

Nous présentons ici le principe de la réduction dans sa généralité puis nous verrons comment le spécialiser pour en faire ce que nous souhaitons.

\begin{definition}
	Une réduction d'un problème $A$ à un problème $B$ (Figure~\ref{fig:principeRed}) est une fonction $tr$ calculable telle que pour tout $w$ instance de $A$, $w$ est une instance positive de $A$ si et seulement si $tr(w)$ est une instance positive de $B$. On note $A \leq B$.
	
	On dit que $A$ se réduit à $B$ s'il existe une réduction de $A$ à $B$ \textblue{(intuitivement, $A$ est plus facile que $B$)}.
\end{definition}

\begin{req}
	En fonction des propriétés sur la fonction $tr$, on obtient différentes réductions qui vont nous permettre de spécialiser la réduction au résultat que nous souhaitons montrer.
\end{req}

\begin{minipage}{.46\textwidth}
	\begin{theo}[Principe de la réduction]
		Si $A$ se réduit à $B$, alors si $P$ est une propriété sur $B$ alors $P$ est une propriété sur $A$ \textblue{(dans notre cas, $P$ peut être l'appartenance à une classe de complexité ou être le caractère indécidable d'un problème, ...)}.
	\end{theo}
	\begin{footnotesize}
		\begin{proof}
			On raisonne par l'absurde et grâce à la fonction de traduction, on obtient une contradiction.
		\end{proof}
	\end{footnotesize}
\end{minipage} \hfill
\begin{minipage}{.46\textwidth}
	\begin{figure}[H]
		\centering
		\begin{tikzpicture}[auto, thick, >=latex]
		\draw
		% Drawing the blocks of first filter :
		node at (-1,0) [rectangle,draw=none, name=input1] {} 
		node at (1.5,0) [rectangle,draw=black] (red) {Réduction $tr$}
		node at (4,0) [rectangle,draw=none] (tr) {$tr(w)$}
		node at (5.75,0) [rectangle,draw=black] (B) {$B$}
		node at (7,0) [rectangle,draw=none] (fin) {};
		% Joining blocks. 
		% Commands \draw with options like [->] must be written individually
		\draw[->](input1) -- node[near start]{$w$}(red);
		\draw[-](red) -- node {} (tr);
		\draw[->](tr) -- node {} (B);
		\draw[->](B) -- node {} (fin);
		% Boxing and labelling noise shapers
		\draw [color=gray,thick](-0.25,-1) rectangle (6.5,1.5);
		\node at (-0.25,1.25) [above=5mm, right=0mm] {$A$};
		\end{tikzpicture}
		\caption{Schéma du principe de réduction du problème $A$ au problème $B$.}
		\label{fig:principeRed}	
	\end{figure}
\end{minipage}

\noindent Nous allons donner quelques réductions de $A$ à $B$ et leurs propriétés. On note $C$ une classe de complexité telle que  $P \subseteq C$.

\noindent\begin{tabular}{|c|c|c|}
	\hline
	Réduction & Propriétés & Conséquences \\
	\hline
	\multirow{2}{*}{Calculable} & $tr$ est calculable par une machine de Turing & $A$ indécidable $\Rightarrow B$ indécidable \\
	& \textblue{(variante de MT : équivalence)} & $B$ décidable $\Rightarrow A$ décidable \\
	\hline
	Temps &  $tr$ est calculable par une machine de Turing & $A$ est $C$-dur $\Rightarrow B$ est $C$-dur ($C \neq P$)  \\
	polynomial & déterministe en temps polynomial & $B \in C \Rightarrow A \in C$  \\
	\hline
	Espace &  $tr$ est calculable par une machine de Turing & $A$ est $D$-dur $\Rightarrow B$ est $D$-dur   \\
	logarithmique & déterministe en espace logarithmique & $B \in D \Rightarrow A \in D$ ($D \neq P$) \\
	& \textblue{(la MT a trois rubans)} & avec $D \in \{L, NL, co-NL, P\}$\\
	\hline
\end{tabular}

\begin{req}
	La réduction en espace logarithmique est une réduction très spéciale car une machine qui travail en espace logarithmique a au moins deux rubans (il ne faut pas que l'entrée rentre dans la calcul). Mais pour la réduction, la sortie n'est pas non plus dans la borne de la mémoire utilisé (notons qu'elle est polynomiale (car une réduction en espace logarithmique s'exécute en temps polynomial)), il nous faut donc un troisième ruban. La machine de Turing effectuant la réduction a donc trois rubans: un ruban contenant l'entrée en lecture seule et en une seule passe; un ruban de travail logarithmique et un ruban de sortie polynomial en écriture seule et en une seule passe.
\end{req}

\begin{prop}
	Une réduction en espace logarithmique est une réduction en temps polynomial.
\end{prop}