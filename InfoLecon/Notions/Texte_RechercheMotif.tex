%LECONs : 907
\emph{Objectif}: Recherche d'un élément lorsque $K$ est l'ensemble des mots de $|\Sigma|^{*}$ (où $\Sigma$ est un alphabet) et $E$ est l'ensemble des facteurs d'un texte.

\emph{Structure de données associée}: tableau de longueur la taille de la chaîne de caractère considérée. On notera $T = [1 \dots t]$ le tableau du texte et $P= [1 \dots p]$ le motif rechercher dans $T$ où $t$ est la taille du texte et $p$ est celle du motif.

\textblue{\emph{Remarques préliminaires}: Le motif ne peut être contenue dans le texte uniquement si sa taille est inférieure à celle du texte et s'ils sont définis sur un alphabet commun que l'on note $\Sigma$. Ce que l'on supposera dans la suite.}

\paragraph{Algorithme naïf \cite[p.905]{Cormen}}
Cet algorithme est loin d'être optimal en complexité temporelle. Par contre, il a une très bonne complexité spatiale et ne nécessite aucun pré-traitement. 

\begin{minipage}{.46\textwidth}
	\begin{algorithm}[H]
		\begin{algorithmic}[1]
			\Function{\textsc{Recherche-Naive}}{$T, P$} 
			\Comment{$T$ le texte et $P$ le motif}
			\State $t \gets T.longueur$ 
			\State $p \gets P.longueur$ 
			\For{$s = 0$ à $t-p$}
			\If{$P[1, \dots, m] = T[s+1, \dots, s+m]$}
			\State Retourner $s$
			\EndIf
			\EndFor
			\State Retourner $0$
			\EndFunction
		\end{algorithmic}
		\caption{Recherche naïve d'un motif dans un texte.}
		\label{algo:Texte_RechercheNaive}
	\end{algorithm}
\end{minipage}\hfill
\begin{minipage}{.46\textwidth}
	\emph{Principe}: L'algorithme naïf utilise le principe de la fenêtre glissante. Il va chercher le motif en décalant de un caractère en cas d'erreur. De plus la comparaison motif-texte se fait de gauche à droite. Il renvoie le décalage représentant le début du motif dans le texte ou $0$ si le motif n'apparaît pas dans le texte.
	
	\emph{Complexité dans le pire cas}: $O((\textcolor{NavyBlue}{t-p+1})\textcolor{Magenta}{p})$ ou \textcolor{NavyBlue}{l'ensemble des positions possibles pour le motif} et \textcolor{Magenta}{la taille du motif que nous devons tester à chaque fois}. 
	
	On peut attendre la borne atteinte de cet algorithme avec $T = a^{p-1}ba^p$ et $P= a^p$.
	
	\emph{Complexité spatiale}: $O(1)$.
\end{minipage}

\paragraph{Algorithme de Rabin-Karp \cite[p.905]{Cormen}}
Algorithme qui se comporte bien en moyenne pour la recherche de chaîne. De plus, il se généralise bien à des problèmes voisins tels que la recherche des motifs bidimensionnels.

\emph{Hypothèse 1}: Le motif est fixe et connu à l'avance. 

\emph{Hypothèse 2}: On considère que chacun des caractères est exprimé comme un chiffre en base $d = |\Sigma|$. Dans le cadre de cette leçon, on considère que $\Sigma = \{0,1, \dots, 9\}$. \textblue{(Cette hypothèse n'est pas très contraignante, il suffit de travailler en base $|\Sigma|$ puisque l'alphabet est fini)}

\begin{minipage}{.6\textwidth}
	\begin{algorithm}[H]
		\begin{algorithmic}[1]
			\Function{\textsc{Rabin-Karp}}{$T, P, d, q$} 
			\Comment{$T$ le texte, $P$ le motif, $d = |\Sigma|$, $q$ un nombre premier}
			\State $t \gets T.longueur$; $p \gets P.longueur$ 
			\State $h \gets d^{p+1} mod~q$
			\State $p \gets 0$; $t_0 \gets 0$
			\For{$i = 1$ à $p$} \Comment{Pré-traitement}
			\State $p \gets (dp +P[i]) mod~q$ \Comment{Valeur du motif}
			\State $t_0 \gets (dt_0 +T[i]) mod~q$ \Comment{Valeur du 1er bloc}
			\EndFor
			\For{$s = 0$ à $t - p$} \Comment{Recherche de la correspondance}
			\If{$p = t_s$}
			\If{$P[1, \dots, p] = T[s+1, \dots, s + p]$}
			\State Retourner $s$
			\EndIf
			\State $t_{s+1} \gets (d(t_s - T[s+1]h) + T[s+m+1]) mod~q$
			\EndIf
			\EndFor
			\State Retourner $0$
			\EndFunction
		\end{algorithmic}
		\caption{Algorithme de Rabin-Karp pour la recherche d'un motif dans un texte.}
		\label{algo:Recherche_RabinKarp}
	\end{algorithm}
\end{minipage} \hfill
\begin{minipage}{.36\textwidth}
	\emph{Principe}: Cet algorithme s'appuie sur la théorie élémentaire des nombres. En effet, tout bloc de la taille du motif dans le texte ainsi que le motif vont être soumis à un calcul donnant une valeur modulo un nombre premier. A la place de tester si tous les blocs sont égaux au motif, on testera si tous les blocs qui ont le même nombre modulo ce nombre premier sont égaux. On espère avoir moins de tests à effectuer. L'algorithme renvoie le décalage représentant le début du motif dans le texte ou $0$ si le motif n'apparaît pas dans le texte.
	
	\emph{Remarque}: On peut appliquer le même principe avec une fonction de hachage \cite[p.367]{CrochemoreRytter}. Dans ce cas, l'hypothèse 2 n'est plus nécessaire.
	
\end{minipage}
\emph{Complexité du prétraitement}: $\Theta(p)$ \textblue{Les valeurs pour le texte sont calculées à la volée lors de la recherche. Le prétraitement permet de calculer la valeur du motif et la première valeur pour le texte.}

\emph{Complexité dans le pire cas}: $O((t-p+1)p)$ \textblue{Comme dans le cas naïf.} On peut atteindre la borne avec $T = 002002011$ et $P= 011$.

\emph{Complexité spatiale}: $O(1)$ \textblue{(On stocke la valeur du motif et la valeur du bloc précédent)}

\textblue{\emph{Application}: Le problème de recherche multiples motifs se résout avec un lourd pré-traitement sur le texte $T$ ou les multiples motifs $P_i$ afin de comparer les multiples $P_i$ à une portion de texte une seule fois. On adapte l'algorithme de Rabin-Karp pour résoudre efficacement ce problème. (Autre algorithme pour ce problème: l'algorithme d'Aho-Corasick.)}

\textblue{\emph{Utilisation}: détection de plagiat; comparaison d'un fichier suspect à des fragments de virus; ...}

\paragraph{Algorithme de Knuth-Morris-Pratt \cite[p.905]{Cormen}}
\input{./../Notions/Texte_KMP.tex}

\paragraph{Algorithme de Boyer et Moore \cite[p.358]{Beauquier-Berstel-Chretienne}} 
\emph{Principe}: on fait la vérification du motif de la droite vers la gauche. En cas d'erreur et si le caractère d'erreur n'est pas dans le motif, on décale le motif de $p$ caractères dans le texte. En effet, si une erreur de ce type apparaît lors de la vérification, on sait que le motif n'est pas dans le texte vérifié.

\emph{Exemples}

\begin{tabular}{llll}
	Cas 1 & Cas 2 & Cas 3 & Cas 4 \\
	stupi\textred{d}$\_$spring$\_$string & stupid$\_$spri\textred{n}g$\_$string & stupid$\_$s\textred{p}\textgreen{ring}$\_$string & stupid$\_$spring$\_$\textgreen{string}\\
	
	strin\textred{g} &  . . . . . . strin\textred{g} & . . . . . . . s\textred{t}\textgreen{ring} & . . . . . . . . . . . . .  \textgreen{string}\\
\end{tabular}

Dans le cas 1 et 3: le caractère d'erreur ne se trouve pas dans le motif. On peut sauter directement dans le texte les 6 caractères du motif et recommencer par la fin du motif.

Dans le cas 2 et 4: le g de la clé ne correspond pas au n du texte. Cependant, le motif contient un n, un caractère avant, on peut donc décaler d’une position, et vérifier à nouveau en commençant par la fin du motif.

\emph{Complexité} : $O(pt)$
%TODO a finir

\paragraph{Arbre des suffixe}

%TODO a finir


