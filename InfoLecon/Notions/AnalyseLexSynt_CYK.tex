%LECONs: 907; 926
L'algorithme CYK (Cocke--Younger--Kasami) \cite[p.198]{Carton} est un algorithme qui décide en temps cubique si un mot est engendré par une grammaire en forme normale quadratique. Il donne alors un certificat que tout langage algébrique est dans la classe P: comme toute grammaire algébrique est équivalente à une grammaire en forme normale quadratique, tout langage algébrique peut être décidé en temps cubique. \textblue{Il faut faire attention au fait que la grammaire est fixée et qu'elle ne fait pas partie de l'entrée car la mise en forme normale d'une grammaire algébrique peut être exponentielle en sa taille.}

\begin{definition}
	Le problème du mot pour une grammaire algébrique:
	
	\begin{tabular}{cl}
		entrée : & une grammaire algébrique $G = (\Sigma, T, R)$, $w \in \Sigma^*$ un mot \\
		sortie : & oui si $w \in L_{G}$ le langage engendré par $G$; non sinon \\
	\end{tabular}
\end{definition}

\emph{Remarque}: En général répondre à ce problème est coûteux $O(|w|^3|G|^{|w|})$.

\begin{definition}
	Soit $G$ une grammaire algébrique. On dit que :
	\begin{itemize}
		\item $G$ est sous forme normale si les règles sont sous la forme $A \to BCD \dots Z$ où $A \in T$ \textblue{(alphabet de travail)} et $B, \dots, Z \in \Sigma \cup T$.
		\item $G$ est sous forme normale de Chomsky si les règles sont sous la forme : $A \to \alpha$ pour, $A \in V, \alpha \in \Sigma$ \textred{$V \cap \Sigma = \emptyset$}; $S \to \epsilon$ et $A \to BC$ pour $A \in V$ et $B, C \in V \setminus \{S\}$
	\end{itemize}
\end{definition}

\begin{theo}
	Le problème du mot pour une grammaire algébrique sous forme forme normale de Chomsky est décidable et un algorithme de programmation dynamique le décide en temps $O(|w|^3|G|)$ et en mémoire $O(|w|^3)$.
\end{theo}
\begin{proof}
	Cet algorithme est un algorithme de programmation dynamique. Soit $w = a_1 \dots a_n$ un mot.
	
\begin{minipage}{.35\textwidth}
	On note :
	\begin{itemize}
		\item $1 \leq i \leq j \leq n$, $w[i,j] = a_i \dots a_j$
		\item $1 \leq i \leq j \leq n$,$E_{i,j} = \{S \text{ des variables telles que} w[i, j] \in L_G(S)\}$. \textblue{L'algorithme calcul tous ces $E_{i,j}$}.
	\end{itemize}
	
	Nous allons maintenant énoncer quelques propriétés d'appartenance à ces ensembles $E_{i, j}$:
	\begin{itemize}
		\item $w \in L_G(S_0)$ si $S_0 \in E_{1,n}$
		\item $S$ appartient à $E_{i, i}$ si et seulement si $S \to a_i$ est une règle de $G$ \textblue{($w[i, i] = a_i$ et on a une grammaire en forme normale quadratique)}
		\item $S$ appartient à $E_{i, j}$ ($i < j$) si et seulement s'il existe une règle $S \to S_1S_2$ et $k \in \N$ tels que $w[i, k] \in L_G(S_1)$ et $w[k+1, j] \in L_G(S_2)$.
	\end{itemize}
	
	\textblue{Les ensembles $E_{i,j}$ peuvent être calculés à partir des ensembles $E_{i, k}$ et $E_{k+1, j}$ pour $i \leq k < j$. L'algorithme les calcul par récurrence sur la différence $j-i$.}

\end{minipage} \hfill
\begin{minipage}{.57\textwidth}
	\begin{algorithm}[H]
	\begin{algorithmic}[1] 
		\Function{\textsf{CYK}}{$w$} \Comment{$w = a_1 \dots a_n$: mot}
		\For{$1 \leq i \leq j \leq n$} \Comment{Initialisation; Complexité: $O(|w|^2)$ \textblue{(double boucle)}}
		\State $E_{i, j} \gets \emptyset$ 
		\EndFor
		\For{$i = 1$ à $n$} \Comment{Cas $i = j$; Complexité: $O(|w||G|)$ \textblue{(double boucle: une sur $w$, une sur $G$)}}
		\For{tout règle $S \to a$}
		\If{$a_i = a$}
		\State $E_{i, i} \gets E_{i, i} \cup \{a\}$
		\EndIf
		\EndFor 
		\EndFor
		\For{$d = 1$ à $n$} \Comment{Cas $i < j$; $d = j - i$; Complexité: $O(|w|^3|G|)$ \textblue{(triple boucle sur $w$; une sur $G$)}}
		\For{$i = 1$ à $n - d$}
		\For{$k = i$ à $i + d$}
		\For{tout règle $S \to S_1S_2$}
		\If{$S_1 \in E_{i, k}$ et $S_2 \in E_{k+1, i+d}$}
		\State $E_{i, i+d} \gets E_{i, i+d} \cup \{S\}$
		\EndIf
		\EndFor
		\EndFor
		\EndFor
		\EndFor
		\EndFunction
	\end{algorithmic}
	\caption{Algorithme CYk (Cocke--Younger--Kasami).}
	\label{algo:CYK}
\end{algorithm}
\end{minipage}

\emph{Complexité}: Comme la taille de la grammaire est fixé et ne fait pas partie de l'entrée (c'est une constante), on a bien la complexité annoncé en $O(|w|^3)$
\end{proof}