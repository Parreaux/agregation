%DEVs: Tseintin
%LECONs: 916 (logique prop)
Les formes normales sont une manière de représenté une formule par une autre formule dont les connecteurs logiques sont restreints et ordonnés selon un certain ordre \cite[p.166]{Duparc}. Ces deux formules peuvent être équivalentes mais sont généralement équisatisfiable. Cette condition suffit puisque la mise sous forme normale est très souvent effectuée pour répondre au problème de la satisfiabilité d'une formule.

\paragraph{Forme normale négative} La forme normale négative est à la base des deux formes suivante puisque les formes normales conjonctives et disjonctives sont négatives. Les négations ne peuvent porter que sur des variables propositionnelles et non sur des sous-formules.

\begin{definition}
	Une forme normale négative est une forme normale engendrée par le grammaire
	\begin{displaymath}
	\varphi :: = \bot ~|~ \top ~|~ p ~|~ \neg p ~|~ (\varphi \vee \varphi) ~|~ (\varphi \wedge \varphi)
	\end{displaymath}
	où $p$ est une variable propositionnelle.
\end{definition}

\begin{prop}
	Toute formule admet une forme normale équivalente.
\end{prop}
\begin{footnotesize}
	\begin{proof}[Idée de la démonstration]
	On effectue une traduction syntaxique inductive (sur la formule à traduire) où seul le cas de la négation est réécrit.
	\begin{displaymath}
	\begin{array}{clccl}
	\bullet & tr(\bot) = \bot & & \bullet & tr(\varphi \vee \psi) = tr(\varphi) \vee tr(\phi) \\
	\bullet & tr(\top) = \top & & \bullet & tr(\varphi \wedge \psi) = tr(\varphi) \wedge tr(\phi) \\
	\bullet & tr(p) = p & & \bullet & tr(\neg(\varphi \vee \psi)) = tr(\neg\varphi) \wedge tr(\neg\phi) \\
	\bullet & tr(\neg p) = \neg p & & \bullet & tr(\neg(\varphi \wedge \psi)) = tr(\neg\varphi) \vee tr(\neg\phi) \\
	\end{array}
	\end{displaymath}
\end{proof}
\end{footnotesize}

\paragraph{Forme normale conjonctive} Les formes normales conjonctives sont les formes normales les plus utilisée : leur satisfiabilité restent $NP$-complet mais la traduction peut se faire en temps linéaire. Nous avons donc une représentation plus simple pour les $SAT$-solveur qui n'explose pas à la transformation. C'est pour cette raison que nous l'utilisons souvent.

\begin{definition}
	Un littéral est une variable propositionnelle ou sa négation.
\end{definition}

\begin{definition}
	Une forme normale conjonctive est une formule de la forme $\bigwedge_{i=1}^{n} \left(\bigvee_{j=1}^{m} l_{i,j}\right)$ où les $l_{i,j}$ sont des littéraux.
\end{definition}

\begin{req}
	Une forme normale conjonctive est une forme normale négative (la réciproque est fausse).
\end{req}

\begin{definition}
	Une disjonction de littéraux est appelée une clause. \textblue{Cela explique le fait que nous parlons de forme clausale: on a une autre représentation.}
\end{definition}

\begin{prop}
	Pour toute formule $\psi$, il existe une formule $\varphi$ sous forme normale conjonctive telle que $\psi$ et $\varphi$ soient équivalentes.
\end{prop}
\begin{footnotesize}
	\begin{proof}[Idées de la démonstration]
		\begin{description}
			\item[Preuve existentielle] 
			\begin{itemize}
				\item $\psi$ n'admet pas de modèle où elle est fausse (c'est une tautologie): elle est équivalente à $\top$.
				\item $\psi$ admet un modèle où elle est fausse: on prend alors la négation de la formule que l'on transforme en DNF (c'est possible car sur ces modèles la négation est vraie et ce sont les seuls modèles). On remet la négation à la formule obtenue.
			\end{itemize}
			
			\item[Preuve constructive] On met la formule sous forme normale négative puis application de ces règles de distributivité. 
		\end{description}
	\end{proof}
\end{footnotesize}

\begin{req}
	Cette transformation peut exploser.
\end{req}

\begin{theo}
	Le problème de validité (\textsc{CNF-SAT}) est dans $NP$.
\end{theo}
\begin{footnotesize}
	\begin{proof}[Idée de la démonstration]
		On fait une réduction à \textsc{SAT} à l'aide de la transformation de Tseintin qui est linéaire.
	\end{proof}
\end{footnotesize}


\begin{theo}
	Le problème de validité (\textsc{CNF-SAT}) est dans $P$.
\end{theo}
\begin{footnotesize}
	\begin{proof}[Idée de la démonstration]
		Une forme normale conjonctive est valide si et seulement si pour toute clause, il existe une variable telle que cette variable et sa négation soient dans la clause.  Cette propriété implique un algorithme linéaire testant la validité.
		\begin{description}
			\item[$\Rightarrow$] Raisonnons par l'absurde et supposons qu'il existe une clause ne contenant pas la négation d'aucun de ces littéraux. Alors le modèle qui rend \textsf{faux} chacun de ces littéraux ne satisfait pas la formule \textblue{(la clause n'est pas satisfaite car elle ne contient aucune négation des littéraux que nous avons mis à \textsf{faux})}. Contradiction avec la validité de la formule
			
			\item[$\Leftarrow$] Soit une telle formule et soit un modèle pour $\varphi$. Comme dans chacune des clauses il existe une variable de cette clause telle que sa négation apparaissent alors la clause est \textsf{vraie} \textblue{(car la négation ou la variable est \textsf{vraie})}. 
		\end{description}
	\end{proof}
\end{footnotesize}

\paragraph{Forme normale disjonctive} Les formules conjonctives (et uniquement conjonctives) permettent de représenter un modèle. En effet, elle est vrai si et seulement si les valeurs de vérités du modèles sont transcrite dans les littéraux de la formule. Un forme normale disjonctive est donc satisfiable si et seulement si une de ces clauses décrit un modèle. 

\begin{definition}
	Une forme normale disjonctive est une formule de la forme $\bigvee_{i=1}^{n} \left(\bigwedge_{j=1}^{m} l_{i,j}\right)$ où les $l_{i,j}$ sont des littéraux.
\end{definition}

\begin{req}
	Une forme normale disjonctive est une forme normale négative (la réciproque est fausse).
\end{req}

\begin{prop}
	Pour toute formule $\psi$, il existe une formule $\varphi$ sous forme normale disjonctive telle que $\psi$ et $\varphi$ soient équivalentes.
\end{prop}
\begin{footnotesize}
	\begin{proof}[Idées de la démonstration]
	\begin{description}
		\item[Preuve existentielle] 
		\begin{itemize}
			\item $\psi$ n'admet pas de modèle: elle est équivalente à n'importe quelle contradiction : $x \wedge \neg x$
			\item $\psi$ admet un modèle: alors pour chacun de ces modèles on construit la formule conjonctive qui décrit sa valuation et on regroupe dans une disjonction la totalité de ces modèles.
		\end{itemize}
		
		\item[Preuve constructive] On procède de la même manière que pour les formes normales conjonctives en inversant les règles de distributivité (mise sous forme normale négative puis application de ces règles de distributivité). \textblue{On obtient les même effet que pour la forme normale conjonctive : on a une explosion de la taille de la formule.}
	\end{description}
\end{proof}
\end{footnotesize}

\begin{theo}
	Le problème de satsifiabilité (\textsc{DNF-SAT}) est dans $P$.
\end{theo}
\begin{footnotesize}
	\begin{proof}[Idée de la preuve]
	Pour chacune des clause (tant qu'on n'a pas réussi à en rendre une vraie): réinitialiser la valuation et affecter la valeur \textsf{vrai} à chacun des littéraux.
\end{proof}
\end{footnotesize}

\emph{Conséquence}: Par le théorème de Cook et si $P \neq NP$, on sait qu'il n'existe pas de transformation polynomiale d'une formule en une formule sous forme normale disjonctive équisatisfiable (ou équivalente).

\begin{cex}
	Soit $(\varphi_n)_{n \in \N}$ une famille de formule du calcul propositionnel telle que sa mise sous forme disjonctive est exponentielle (même pour l'équisatisfiabilité). On pose, pour tout $n \in \N$, $\varphi_n = (a_1 \vee b_1) \wedge \dots \wedge (a_n \vee b_n)$ qui a $2n$ variables et $2n - 1$ connecteurs. La forme normale disjonctive de $\varphi_n$ s’écrit $\bigvee_{x_i \in \{a_i,b_i\}}(x_1 \wedge \dots \wedge x_n)$ avec $2n$ disjonctions.
\end{cex}

\begin{theo}
	Le problème de validité (\textsc{DNF-SAT}) est $NP$-complet.
\end{theo}

\begin{req}
	La dualité entre les eux formes normales pour les problèmes de validité et de satisfiabilité provient de la dualité entre ces deux problèmes et du fait que la négation d'une forme normale conjonctive donne une forme normale disjonctive (sous forme normale négative) et réciproquement.
\end{req}