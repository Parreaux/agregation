%LECONs: 903
\paragraph{Algorithmes naïfs} On commence par traiter des algorithmes de tri naïf. Ce sont des algorithmes qui n'utilise aucun paradigmes ni aucune structures de données élaborées. 

\subparagraph{Tri par sélection} Le tri par sélection \cite[p.310]{Froidevaux-Gaudel-Soria} recherche le minimum parmi les éléments non triés pour le placer à la suite des éléments déjà triés. Lorsque l'on recherche séquentiellement le minimum et qu'on l'échange avec le premier élément non trié, nous réalisons une sélection ordinaire (Algorithme~\ref{algo:tri_selection-naif}).

Afin de simplifier les calculs de complexité, nous allons donner l'algorithme itératif équivalent (en terme de correction et de complexité) de cette sélection ordinaire (Algorithme~\ref{algo:tri_selection-naif-iter}).

\begin{minipage}{.56\textwidth}
	\begin{algorithm}[H]
		\begin{algorithmic}[1] 
			\Function{\textsf{Tri-Sélection}}{$A, i$}\Comment{$A$: tab à trier; $i \in \N$}
			\If{$i < n$}
			\State $j \gets i$
			\For{$k=i+1$ à $n$} \Comment{Recherche séquentielle du min}
			\If{$A[k] < A[j]$}
			\State $j \gets k$
			\EndIf
			\EndFor
			\State Échanger $A[i]$ et $A[j]$ \Comment{Placement du min}
			\State \textsf{Tri-Sélection}($A$, $i+1$) \Comment{Tri fin tableau}
			\EndIf
			\EndFunction
		\end{algorithmic}
		\caption{Algorithme récursif du tri par sélection classique.}
		\label{algo:tri_selection-naif}
	\end{algorithm}
\end{minipage}\hfill
\begin{minipage}{.38\textwidth}
	\begin{algorithm}[H]
		\begin{algorithmic}[1] 
			\Function{\textsf{Tri-Sélection-Iter}}{$A$}
			\State $i \gets 1$
			\While{$i < n$}
			\State $j \gets i$
			\For{$k=i+1$ à $n$} 
			\If{$A[k] < A[j]$}
			\State $j \gets k$
			\EndIf
			\EndFor
			\State Échanger $A[i]$ et $A[j]$ 
			\State $i \gets i + 1$
			\EndWhile
			\EndFunction
		\end{algorithmic}
		\caption{Algorithme itératif du tri par sélection classique.}
		\label{algo:tri_selection-naif-iter}
	\end{algorithm}
\end{minipage}

\begin{theo}[Complexité]
	Le tri par sélection ordinaire s'exécute en $\Theta(n^2)$ (en moyenne et dans le pire cas).
\end{theo}
\begin{proof}
	Pour analyser la complexité de cet algorithme, nous allons analyser le nombre de comparaisons effectué ainsi que le nombre d'échange lors du tri.
	
	Pour toute liste de taille $n$, on effectue $n-1$ comparaisons pour trouver le minimum. Cette recherche est ensuite suivie de la même recherche sur une liste à $n - 1$ éléments. En notant $Max_C(n)$ et $Moy_C(n)$ le nombre maximal (moyen) de comparaison pour une liste à $n$ éléments, on trouve: $Max_C(n) = n - 1 + Max_C(n-1)$ pour $n > 1$ et $Max_C(1) = 0$. Comme, le nombre de comparaison ne dépend pas de la liste : $Moy_C(n) = Max_C(n)$. L'équation de récurrence sur $Max$ se résout selon une méthode directe, on obtient : $Max_C = \frac{n(n-1)}{2}$. Le nombre de comparaisons que l'on effectue est donc $\Theta(n)$.
	
	Le nombre d'échange dans le pire cas est le même qu'en moyenne car on ne fait qu'un échange lors de l'appel du tri. On a donc $Max_E = Moy_E = n-1 = \Theta(n)$.
\end{proof}

Une autre implémentation d'un tri par sélection est un tri à bulle (Algorithme~\ref{algo:tri_selection-bulle}). Son principe, joliment présenté par son nom, consiste à faire remonter les plus petit éléments en tête du tableau (comme des bulles). Pour cela, on part de la fin du tableau et tant que l'élément est plus petit que les autres on effectue une permutation.

\begin{algorithm}
	\begin{algorithmic}[1] 
		\Function{\textsf{Tri-Bulle}}{$A$}\Comment{$A$: tableau à trier}
		\State $i \gets 1$
		\While{$i < n$}
		\For{$j=n$ à $i+1$} 
		\If{$A[1] < A[j-1]$}
		\State Échanger $A[j-1]$ et $A[j]$ 
		\EndIf
		\EndFor
		\State $i \gets i + 1$
		\EndWhile
		\EndFunction
	\end{algorithmic}
	\caption{Algorithme du tri par dénombrement.}
	\label{algo:tri_selection-bulle}
\end{algorithm}

\begin{theo}[Complexité]
	Le tri à bulle s'exécute en $\Theta(n^2)$ (en moyenne et dans le pire cas).
\end{theo}
\begin{proof}
	Pour analyser la complexité de cet algorithme, nous allons analyser le nombre de comparaisons effectué ainsi que le nombre d'échange lors du tri.
	
	Le nombre de comparaison est exactement le même que dans un tri par sélection ordinaire: $\Theta(n^2)$.
	
	Le nombre d'échange dans le pire cas n'est plus le même qu'en moyenne car on le nombre d'échange lors de l'appel du tri varie en fonction de la place de l'élément. On utilise deux méthodes pour calculer ce nombre d'échange: un calcul par dénombrement ou un calcul par séries génératrices. Dans les deux cas, on trouve une complexité moyenne en $O(n^2)$.
\end{proof}

\subparagraph{Tri par insertion} Le tri par insertion \cite[p.320]{Froidevaux-Gaudel-Soria} consiste à pré-trier une liste afin d'entrer les éléments à leur bon emplacement dans la liste triée. Par exemple à l'itération $i$, on insère le $i^{\text{ième}}$ élément à la bonne place dans la liste des $i-1$ éléments qui le précède \textblue{(cette liste est triée par construction de l'algorithme)}. Comme pour le tri par sélection, il existe plusieurs tri par insertion: le tri par insertion séquentiel ou le tri par insertion dichotomique.

Le tri par insertion séquentiel (Algorithme~\ref{algo:tri_insertion-sequentiel}) effectue la recherche de la place de l'élément à insérer séquentiellement: on parcours toute la liste au pire cas. La fonction récursive décrivant le tri par insertion que l'on donne n'est pas récursive terminal et peut difficilement l'être. Le tri ainsi défini n'est pas en place. Cependant, on peut donné un algorithme itératif qui conserve le nombre de comparaison ainsi que le nombre d'échange et qui rend le tri en place. On a alors bien un tri en place avec une complexité en $O(n^2)$.

\begin{minipage}{.56\textwidth}
	\begin{algorithm}[H]
		\begin{algorithmic}[1] 
			\Function{\textsf{Tri-Insertion}}{$A, i$}\Comment{$A$: tab à trier; $i \in \N$}
			\If{$i > 1$}
			\State \textsf{Tri-Insertion}($A$, $i-1$) \Comment{Trier début tab}
			\State $k \gets i -1$ \Comment{Recherche rang de $i$}
			\State $x \gets A[i]$
			\While{$A[k] > x$}
			\State $A[k+1] \gets A[k]$
			\State $k \gets k -1$
			\EndWhile
			\State $A[k+1] \gets x$ \Comment{On place $i$}
			\EndIf
			\EndFunction
		\end{algorithmic}
		\caption{Algorithme récursif du tri par insertion séquentiel.}
		\label{algo:tri_insertion-sequentiel}
	\end{algorithm}
\end{minipage}\hfill
\begin{minipage}{.38\textwidth}
	\begin{algorithm}[H]
		\begin{algorithmic}[1] 
			\Function{\textsf{Tri-Insertion-Iter}}{$A$}
			\For{$k=2$ à $n$} 
			\State $k \gets i - 1$
			\State $x \gets A[i]$
			\While{$A[k] > x$}
			\State $A[k+1] \gets A[k]$
			\State $k \gets k - 1$
			\EndWhile
			\State $A[k+1] \gets x$
			\EndFor
			\EndFunction
		\end{algorithmic}
		\caption{Algorithme itératif du tri par insertion séquentiel.}
		\label{algo:tri_insertion-sequentiel-iter}
	\end{algorithm}
\end{minipage}

\begin{prop}
	Le tri par insertion a une complexité en $O(n^2)$ dans le pire des cas et en moyenne.
\end{prop}
\begin{proof}
	Nous allons commencer par l'analyse du nombre de comparaisons dans le pire cas. Dans ce cas, on doit faire $i$ comparaisons après l'appel de \textsf{Tri-Insertion}($t$, $i-1$) \textblue{(on le fait pour tous les appels lorsque le tableau est trié dans l'ordre décroissant)}. On obtient alors la relation de récurrence suivante: $Max_C(n) = Max_C(n-1) + n$ pour $n > 1$ et $Max_C(1) = 0$. On a alors $Max_C(n) = \frac{n(n+1)}{2}-1$. Donc le nombre de comparaisons dans le pire cas est $O(n^2)$.
	
	Pour la complexité moyenne, on utilise les résultats établis pour l'analyse du tri à bulle...
	%TODO finir
\end{proof}

\paragraph{Tri sous le paradigme diviser pour régner} Le paradigme diviser pour régner est utile lors de la définition d'un tri. Comme l'ordre est total nous pouvons séparer en deux sous-ensemble les données à trier afin d'en simplifier le problème. Les deux algorithmes qui existent à ce sujet choisissent de mettre la difficulté à deux endroits différents : le tri rapide découpe l'ensemble de manière complexe afin de les rassembler très facilement (au pire c'est une concaténation de listes); le tri fusion découpe simplement l'ensemble mais demande une petite astuce lors de la fusion. Nous allons étudier ces deux méthodes, puis nous les comparerons.

\subparagraph{Tri rapide}
\input{./../Notions/Tri_rapide.tex}
%TODO : finir

\subparagraph{Tri fusion}
\input{./../Notions/Tri_fusion.tex} 

\paragraph{Optimalité du tri par comparaison}

