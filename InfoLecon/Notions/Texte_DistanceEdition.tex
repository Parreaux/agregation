%LECONs : 907
%DEVs : Alignement optimaux
Les distances d'éditions permettent de donner la similitude entre deux mots. On définit ici ce que sont ces distances d'éditions et quelques une de leur propriétés.

\paragraph{Distance d'édition} On peut définir plusieurs distances sur les mots (plus ou moins pratique) et plusieurs distances d'éditions. Nous allons en étudier quelques unes. Nous allons commencer par définir des distances théoriques \textblue{(elles n'ont aucune utilité en pratique mais peuvent servir pour des raisonnements)}.
\begin{itemize}
	\item La distance préfixe : $\forall u, v \in \Sigma^*$, $d_{pref} = |u| + |v| - 2|lcp(u,v)|$ où $lcp(u,v)$ est le plus long préfixe commun de $u$ et $v$.
	\item La distance suffixe : $\forall u, v \in \Sigma^*$, $d_{suff} = |u| + |v| - 2|lcs(u,v)|$ où $lcs(u,v)$ est le plus long suffixe commun de $u$ et $v$.
	\item La distance facteur : $\forall u, v \in \Sigma^*$, $d_{fact} = |u| + |v| - 2|lcp(u,v)|$ où $lcf(u,v)$ est le plus long facteur commun de $u$ et $v$.
\end{itemize}

Une distance plus importante et surtout utilisé en pratique est la distance de Hamming. Elle n'est pas très expressive : il y a une hypothèse forte sur la longueur des mots et elle nous donne juste un nombre et non quelles sont les modifications pour arriver à ce nombre. Cependant, elle reste la plus facile à calculer (linéaire en la taille du mot), c'est probablement pour cela qu'elle reste si utilisée.
\begin{definition}
	La distance de Hamming sur deux mots de la même longueur donne le nombre de différences de lettres entre les deux mots.
\end{definition}

\emph{Exemple}: Si $u = aab$ et $v = abb$, la distance de Hamming appliquée à $u$ et à $v$ est deux.

La distance d'édition est une distance définie sur toutes les paires de mots. De plus, contrairement à la distance de Hamming, on est capable de donner une explication du passage de $x$ à $y$. Pour cela, nous commençons par définir trois opérations élémentaires munies de leurs coûts.
\begin{itemize}
	\item La substitution qui positionne $a$ à la place de $b$ dans $x$ à une position donnée. On note sa fonction coût $Sub(a, b)$.
	\item L'insertion qui insère $a$ dans $x$ à une position donnée. On note sa fonction coût $Ins(a)$.
	\item La suppression qui supprime $a$ d'une position donnée dans $x$. On note sa fonction coût $Del(a)$.
\end{itemize}

\begin{definition}
	La distance d'édition de Leveinstein est une fonction $Dist$ définie telle que : 
	\begin{displaymath}
		Dist(x,y) = \min \{\text{coût de}~ \sigma : \sigma \in \Sigma_{x,y}\}
	\end{displaymath}
	où $\Sigma_{x,y}$ est une suite d'opérations élémentaires permettant de transformer $x$ en $y$ où son coût est la somme des coûts de ces opérations élémentaires.
\end{definition}

\begin{req}
	La distance de Hamming est une distance d'édition où $Del(a) = Ins(a) = + \infty$.
\end{req}

\begin{prop}
	$Dist$ est une distance définie sur $\Sigma^*$ \textblue{(au sens mathématiques)} si et seulement si $Sub$ est une distance sur $\Sigma$ et si $Del(a) = Ins(a) > 0$, $\forall a \in \Sigma$.
\end{prop}
\begin{proof}
	\begin{description}
		\item[$\Rightarrow$] Supposons que $Dist$ est une distance.
	\begin{itemize}
		\item $\forall a, b \in \Sigma, Dist(a, b) = Sub(a, b)$ : $Sub$ est une distance.
		\item $Del(a) = Dist(a, \epsilon) = Dist(\epsilon, a) = Ins(a)$ : $Del(a) = Ins(a) > 0$.
	\end{itemize}
	
	\item[$\Leftarrow$] Montrons que $Dist$ est une distance: on montre qu'elle possède les quatre propriétés.
	\begin{itemize}
		\item Positivité : $Sub \geq 0$ \textblue{($Sub$ distance)}, $Del, Ins > 0$ \textblue{(hypothèse)} : leur somme $\geq 0$.
		\item Séparation : si $u = v$, $Dist(u,v) = 0$ \textblue{($Sub$ distance)}. Réciproquement, si $Dist(u, v) = 0$, $u = v$ \textblue{($Sub$ est la seule fonction qui s'annule)}.
		\item Symétrie : la fonction $Sub$ est symétrique \textblue{($Sub$ distance)} et les fonctions $Ins$ et $Del$ sont équivalentes : $Dist$ est symétrique \textblue{(suite minimale est donnée par lecture inverse)}.
		\item Inégalité triangulaire : raisonnement par l'absurde, on suppose que $\exists w \in \Sigma^*$ tel que $Dist(u,w) + Dist(w, v) < Dist(u, v)$. On a donc une suite minimale de $u$ à $w$ puis une de $w$ à $v$ de coût inférieur à une suite minimale de $u$ à $v$.
	\end{itemize}
	\end{description}
\end{proof}

\emph{Problèmes pour le calcul}: Il n'existe pas d'unicité du calcul et cela reste un problème d'optimisation.

\paragraph{Alignement} L'alignement est une façon de visualiser leur similitudes. Lors du calcul de la distance d'édition ($Dist$), nous calculons un alignement optimal. Un alignement entre deux mots $x$ et $y$ est un mot $z \in (\Sigma \cup \{\epsilon\}) \times (\Sigma \cup \{\epsilon\}) \setminus \{(\epsilon, \epsilon)\}$ dont la projection sur la première composante est $x$ et la projection sur la seconde est $y$.

\emph{Exemple}: Voici un alignement (qui est même optimal).
\begin{displaymath}
\left(\begin{tabular}{cccccc}
	A & C & G & - & - & A \\
	A & C & G & C & T & A
\end{tabular} \right)
\end{displaymath}
Le nombre d'alignements entre deux mots est exponentiel. On donne maintenant une borne sur ce nombre.

\begin{prop}
	Soient $x, y \in \Sigma^*$ de longueurs respectives $m$ et $n$ avec $m \leq n$. Le nombre d'alignement entre $x$ et $y$ ne contenant pas de suppression consécutives de lettres de $x$ est $\binom{2n+1}{m}$.
\end{prop}
\begin{proof}
	Un alignement est caractérisé par les substitutions aux $n$ positions sur $y$ ainsi que les suppressions ($n+1$ emplacements possibles \textblue{(en comptant un emplacement avant $y[0]$ et un après $y[n-1]$)}) qui le compose. Un alignement est donc un choix de $m$ substitutions ou suppressions parmi $2n + 1$ emplacements possibles.
\end{proof}

\paragraph{Graphe d'édition} Le graphe d'édition traduit l'ensemble des choix qui forment l'alignement (Figure~\ref{fig:graphe_edition}). 

\begin{minipage}{.7\textwidth}
	\begin{figure}[H]
		\begin{tikzpicture}[font=\sffamily]
		% Setup the style for the states
		\tikzset{node style/.style={state, 
				minimum width=1cm,
				line width=.25mm,
				fill=white}}
		
		% Draw the states
		\node[node style] at (0,0) (0) {$(-1, -1)$};
		\node[node style] at (3,0) (2) {$(-1, 0)$};
		\node[node style,draw=none] at (5, 0) (1) {$\dots$};
		\node[node style] at (0, -2) (3) {$(0, -1)$};
		\node[node style,draw=none] at (0, -4)   (4) {$\vdots$};
		\node[node style,draw=none] at (6, -4)   (6) {$\ddots$};
		\node[node style,draw=none] at (5.5, -6)   (7) {$\dots$};
		\node[node style,draw=none] at (8, -3.5)   (8) {$\vdots$};
		\node[node style] at (8, -6)    (5) {$(m-1, n-1)$};
		\node[node style] at (3, -2) (9) {$(0, 0)$};
		\node[node style,draw=none] at (4.5, -3.5) (10) {$\ddots$};
		\node[node style,draw=none] at (3, -4) (11) {$\vdots$};
		\node[node style,draw=none] at (5, -2) (12) {$\dots$};
		
		% Connect the states with arrows
		\draw[every loop,auto=left,line width=.25mm,
		>=latex]
		(0) edge [] node {$Ins$} (2)
		(2) edge [] node {} (1)
		(0) edge [auto=right] node {$Del$} (3)
		(3) edge [] node {} (4)
		(0) edge [] node {$Sub$} (9)
		(9) edge [] node {} (10)
		(9) edge [] node {} (11)
		(9) edge [] node {} (12)
		(7) edge [] node {} (5)
		(8) edge [] node {} (5)
		(6) edge [] node {} (5);
		\end{tikzpicture}
		\caption{Un exemple partiel de graphe d'édition.}
		\label{fig:graphe_edition}
	\end{figure}
\end{minipage} \hfill
\begin{minipage}{.25\textwidth}
	Le graphe d'édition est composé des sommets correspondant à l'ensembles des paires de préfixes des deux mots \textblue{(un chemin de l'origine jusqu'à un nœud traduit la transformation d'un préfixe vers le second)}. On passe alors d'un état vers un autre à l'aide d'une opération. Tout chemin de $(-1, -1)$ vers $(m-1, n-1)$ dans ce graphe est un alignement entre $x$ et $y$. 
\end{minipage}

Le calcul d'un alignement optimal ou de $Dist$ se ramène au calcul d'un chemin de coût optimal sur le graphe d'édition $G$. Comme $G$ est acyclique, on a besoin d'un seul passage pour le calculer \textblue{(on met un ordre topologique)}. On utilise alors la programmation dynamique afin de calculer le graphe et d'en faire le parcours.