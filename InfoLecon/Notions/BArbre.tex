
\paragraph{Motivations}: Minimiser le nombre d'action sur un disque externe (c'est ce qui prend le plus de temps, en ms).
	
Un B-arbre est un arbre de la recherche avec une ramification importante et une hauteur plutôt faible. En pratique un noeud de notre arbre est une page de notre disque externe.
	
\emph{Hypothèses}: on se donne deux opérations atomiques: \textsc{Ecriture-Disque} et \textsc{Lecture-Disque} (\textblue{on ne peut pas les découper en sous fonctions, elles travaillent uniquement sur une page du disque dur}). On veut montrer l'optimalité de la structure de donnée face à l'utilisation de ces deux opérations.
	
\paragraph{Définition d'un B-arbre et exemple} On donne maintenant la définition d'un B-arbre. La figure \ref{fig:B-arbreexDef} nous donne un exemple d'un tel arbre.
	
\begin{definition}
	Un B-arbre est un arbre $T$ possédant les propriétés suivantes:
	\begin{enumerate}
		\item Chaque noeud $x$ contient les attributs ci-après:
		\begin{enumerate}
			\item $x.n$ le nombre de clés conservées dans le noeud (le noeud est alors d'arité $n+1$);
			\item les $x.n$ clés $x.cle_1, \dots x.cle_{n}$ stockées dans un ordre croissant (structure de donnée associée: liste);
			\item un booléen, $x.feuille$ marquant si le noeud est une feuille;
			\item les $n+1$ pointeurs des fils, $\{x.c_1, \dots x.c_{n+1}\}$;
		\end{enumerate}
		\item Les clés et les valeurs dans les arbres fils vérifient la propriété suivante: $k_0 \leq x.cle_1 \leq \dots \leq x.cle_n \leq k_n$;
		\item toutes les feuilles ont la même profondeur, qui est la hauteur $h$ de l'arbre;
		\item Le nombre de clé d'un noeud est contenu entre $t-1 \leq n \leq 2t - 1$. (\textblue{Pour la racine on n'a que la borne supérieure, la borne inférieure est 1: on demande qu'il y ait au moins une clé dans la racine}) où $t$ est le degré minimal du B-arbre.
	\end{enumerate}
\end{definition}
	
\begin{req}
	Dans la vrai vie, $t$ est choisi en fonction du nombre de tuples que peut contenir une page du disque.
\end{req}
	
\begin{theo}
	Si $n \geq 1$, alors pour tout B-arbre $T$ à $n$ clés de hauteur $h$ et de degré minimal $t \geq 2$, $h \leq \log_2 \frac{n+1}{2}$.
\end{theo}
\begin{proof}
	On montre par récurrence sur la hauteur de l'arbre que 
	\begin{displaymath}
	n \geq 1 + (t-1) \sum_{i=1}^{h}2t^{i-1} = 2t^h - 1
	\end{displaymath}
		
	D'où $t^h \leq \frac{(n+1)}{2}$ et on conclut en passant au $\log_t$.
\end{proof}
	
\begin{figure}
	\centering
	\begin{tikzpicture}[level/.style={sibling distance=8em/#1, level distance=3.5em}]
		\node [rectangle,draw] (a){$G~M~P~X$}
		child {node [rectangle,draw] (2){$A~C~D~E$}}
		child {node [rectangle,draw] (b) {$J~K$}}
		child {node [rectangle,draw] (c) {$N~O$}}
		child {node [rectangle,draw] (c) {$R~S~T~U~V$}}
		child {node [rectangle,draw] (c) {$Y~Z$}};
	\end{tikzpicture}
	\caption{Exemple d'un B-arbre de degré minimal $t=3$.}
	\label{fig:B-arbreexDef}
\end{figure}
	
\emph{Hypothèses}:
\begin{itemize}
	\item La racine du B-arbre se trouve toujours en mémoire principal: on n'a pas de \textsc{Lire-Disque}; cependant, il faudra effectuer un \textsc{Écriture-Disque} lors de la modification de la racine.
	\item Tout noeud passé en paramètre devra subir un \textsc{Lire-Disque}.
\end{itemize}
	
\paragraph{Recherche dans un B-arbre}
	
La recherche dans un B-arbre est analogue à la recherche dans un ABR mais dans ce cas on prend une décision sur les $n$ clés de l'arbre, on cherche donc à placer la clé cherchée dans un des intervalles définis par les $n$ clés.
	
L'algorithme prend en paramètre l'élément recherché $k$ et le noeud courant $x$. On retourne alors le noeud $x$ et la place de $k$ dans le noeud $x$, si $k$ est dans l'arbre. Sinon, on retourne \textsc{Nil}.
	
\begin{algorithm}
	\begin{algorithmic}[1]
		\Function{\textsc{Recherche-BArbre}}{$x, k$} \Comment{$x$ a subit une lecture dans le disque \textsc{Lire-Disque}}
		\State  $i \gets 1$
		\While {$i \leq x.n$ et $k > x.cle_i$} \Comment{Recherche de l'intervalle dans lequel se trouve $k$}
		\State $i \gets i+1$
		\If {$i \leq x.n$ et $ k = x.key_i$} \Comment{On a trouvé $k$}
		\State Retourne $(x, i)$ 
		\Else \Comment{On n'a pas trouvé $k$}
		\If {$x.feuille$} \Comment{On a fini l'arbre et $k$ n'y est pas}
		\State Retourne \textsc{Nil}
		\Else \Comment{On peut continuer à descendre dans l'arbre: récursivité sur le fils de $x$}
		\State \textsc{Lire-Disque}($x.c_i$)
		\State Retourne \textsc{Recherche-BArbre}($x.c_i, k$)
		\EndIf
		\EndIf
		\EndWhile
		\EndFunction
	\end{algorithmic}
	\caption{Recherche dans un B-arbre}
	\label{algo:B-arbre_recherche}
\end{algorithm}
	
\emph{Complexité}: $O(log_t n) = O(h)$ lecture-écriture sur le disque (on ne fait que des lectures) où $h$ est la hauteur et $n$ le nombre de nœuds. Comme $n < 2t$, la boucle \textbf{while} s'effectue en $O(t)$, le temps processeur total est $O(th) = O(t \log n)$.
	
\paragraph{Insertion dans un B-arbre}
	
L'insertion dans un B-arbre est plus délicate que dans une ABR. Comme dans un ABR, on commence par chercher la feuille sur laquelle on doit insérer cette nouvelle clé. Cependant, on ne peut pas juste créer une nouvelle feuille (on obtient un arbre illicite). On est alors obliger d'insérer l'élément dans un noeud déjà existant. Comme on ne peut pas insérer la clé dans un noeud plein, on introduit la fonction \textsc{Partage} qui sépare le noeud plein en deux sous noeud distincts autours de la valeur médiane qui remonte dans le noeud père (à qui il faut peut-être appliquer \textsc{Partage}). \textblue{Cette fonction \textsc{Partage}, nous permet, contrairement aux AVL ou arbre rouge-noir, de faire grandir l'arbre vers le haut à l'aide de la fonction \textsc{Partage}. Nous conservons alors un équilibre "parfait".} 

\emph{Principe de l'algorithme}: On descend alors dans l'arbre pour chercher l'emplacement de la nouvelle clé, de manière analogue à la fonction \textsc{Recherche-BArbre}. On souhaite alors insérer la clé dans la feuille que l'on trouve. Dans le cas où cette feuille est pleine, nous devons appliqué la fonction \textsc{Partage} qui conduit à insérer une clé dans le noeud père. Dans le pire des cas, on est alors amené à remonter entièrement l'arbre avec cette fonction \textsc{Partage}. En implémentant intelligemment la fonction \textsc{Insertion-BArbre}, nous pouvons faire qu'une descente dans l'arbre. \textblue{Lors de la descente de l'arbre nous allons appliquer \textsc{Partage} à tous les nœuds pleins que nous parcourons. On effectue la fonction \textsc{Partage} avant toutes opérations d'insertion dans l'arbre (la parité du nombre de clé nous permet de séparer le noeud plus facilement).}
	
La fonction \textsc{Partage} qui effectue un copié-collé des bon pointeurs, prend en paramètre un noeud $x$ \textblue{(en mémoire vive)} et un paramètre $i$ qui indique le noeud que l'on doit séparer ($x.c_i$). Elle modifie alors $x$ en lui ajoutant la valeur médiane de $x.c_i$, $x.c_i$ en lui retirant la moitié de ces valeurs et créer un nouveau noeud $z$ fils de $x$ contenant les valeurs restantes.
	
\begin{algorithm}
	\begin{algorithmic}[1]
		\Function{\textsc{Partage}}{$x, i$} \Comment{$x$ a une lecture dans le disque \textsc{Lire-Disque}}
			\State  $z \gets$ \textsc{Allouer-Noeud}() \Comment{$z$ va récupérer les valeurs les plus grandes et devenir un enfant de $x$}
			\State $y \gets x.c_i$ \Comment{Récupère les plus petites valeurs}
			\State $z.feuille \gets y.feuille$ ~;~ $z.n \gets t-1$
			\For {$j = 1$ à $t-1$} \Comment{Fabrication du noeud $z$: partage de ces clés}
				\State $z.cle_j \gets y.cle_{j+t}$
			\EndFor
			\If {non $y.feuille$} \Comment{Fabrication du noeud $z$: partage de ces fils}
				\For {$j = 1$ à $t$} 
					\State $z.c_j \gets y.c_{j+1}$
				\EndFor
			\EndIf
			\State $y.n \gets t-1$
			\For {$j = x.n+1$ décrois jusqu'à $i+1$}  \Comment{Modification de $x$ pour la nouvelle valeur: fils}
				\State $x.c_{j+1} \gets x.c_j$
			\EndFor
			\State $x.c_{i+1} \gets z$
			\For {$j = x.n$ décrois jusqu'à $i$} \Comment{Modification de $x$ pour la nouvelle valeur: clés}
				\State $x.cle_{j+1} \gets x.cle_j$
			\EndFor
			\State $x.cle_i \gets y.cle_t$
			\State $x.n \gets x.n+1$
			\State \textsc{Écrire-Disque($x$)}~;~ \textsc{Écrire-Disque($y$)}~;~ \textsc{Écrire-Disque($z$)} \Comment{Partage du résultat}
		\EndFunction
	\end{algorithmic}
	\caption{Partage un noeud plein dans un B-arbre}
\end{algorithm}
	
\emph{Complexité}: $O(1)$ en lecture-écriture sur le disque (on ne fait que trois écritures et deux lecture et une allocation qui utilise \textsc{Allouer-Noeud} : alloue une nouvelle page sur le disque en $O(1)$.). Par les deux boucles \textbf{for} en $\Theta(t)$, le temps processeur total est $\Theta(t)$.
	
On définit la fonction \textsc{Insertion-Incomplet-BArbre} \textblue{(une sous fonction de \textsc{Inserer-BArbre})} qui insère $k$ dans un noeud d'un B-arbre de racine $x$, supposé non plein. L'utilisation de cette fonction dans l'insertion globale garantie l'hypothèse.
	
\begin{algorithm}
	\begin{algorithmic}[1]
		\Function{\textsc{Insertion-Incomplet-BArbre}}{$x, k$} \Comment{$x$ a une lecture dans le disque \textsc{Lire-Disque}}
			\If {$x.feuille$} \Comment{Il faut qu'on insère $k$ dans $x$}
				\While{$i \geq n$ et $k < x.cle_i$} 
					\State $x.cle_{i+1} \gets x.cle_i$ ~;~ $ i \gets i -1$
				\EndWhile
				\State $x.cle_{i+1} \gets k$ ~;~ $x.n \gets x.n+1$
				\State \textsc{Écrire-Disque($x$)}
			\Else \Comment{$k$ doit être insérer dans un fils de $x$; récursivité}
				\While{$i \geq n$ et $k < x.cle_i$} 
					\State $ i \gets i -1$
				\EndWhile
				\State $i \gets i+1$
				\State \textsc{Écrire-Disque($x.c_i$)}
				\If{$x.c_i.n = 2t-1$}
					\State \textsc{Partage}($x, i$)
					\If{$k > x.cle_i$}
						\State $i \gets i+1$
					\EndIf
				\EndIf
				\State \textsc{Insertion-Incomplet-BArbre}($x.c_i, k$) 
			\EndIf
		\EndFunction
	\end{algorithmic}
	\caption{Insertion dans un noeud non-plein d'un B-arbre}
\end{algorithm}
	
\emph{Complexité}: $O(log_t n) = O(h)$ lecture-écriture sur le disque (on ne fait que $O(1)$ de lecture-écriture entre deux appels récursif) où $h$ est la hauteur et $n$ le nombre de nœuds. Comme $n < 2t$, les boucles \textbf{while} s'effectue en $O(t)$, le temps processeur total est $O(th) = O(t \log n)$.
	
On définit la fonction \textsc{Insertion-BArbre} qui insère $k$ dans B-arbre $T$. On utilise \textsc{Partage} et \textsc{Insertion-Incomplet-BArbre} pour assurer que la récursivité ne descendent jamais sur un noeud plein. \textblue{Cela nous assure qu'on a toujours une unique descente dans l'arbre.}
	
\begin{algorithm}
	\begin{algorithmic}[1]
		\Function{\textsc{Insertion-BArbre}}{$T, k$} \Comment{La racine de l'arbre est en mémoire vive}
		\State $r \gets T.racine$
		\If {$r.n =2t-1$} \Comment{La racine est pleine}
			\State $s \gets$ \textsc{Allouer-Noeud}()
			\State $T.racine \gets s$ ~;~ $ s.feuille \gets $ \textsc{Faux}
			\State $x.n \gets 0$ ~;~ $x.c_1 \gets r$
			\State \textsc{Partage}($s, 1$)
			\State \textsc{Insertion-Incomplet-BArbre}($s, k$)
		\Else 
			\State \textsc{Insertion-Incomplet-BArbre}($r, k$)
		\EndIf
		\EndFunction
	\end{algorithmic}
	\caption{Insertion d'un élément dans un B-arbre}
\end{algorithm}
	
\emph{Complexité}: $O(log_t n) = O(h)$ lecture-écriture sur le disque (on ne fait qu'une seule descente de l'arbre) où $h$ est la hauteur et $n$ le nombre de nœuds. Le temps processeur total est $O(th) = O(t \log n)$.
	
\begin{figure}
	\centering
	\begin{subfigure}{\textwidth}
		\begin{tikzpicture}[level/.style={sibling distance=8em/#1, level distance=3.5em}]
		\node [rectangle,draw] (a){$G~M~P~X$}
		child {node [rectangle,draw=Navy,fill=LightCyan] (2){$A~\textcolor{DarkBlue}{\textbf{B}}~C~D~E$}}
		child {node [rectangle,draw] (b) {$J~K$}}
		child {node [rectangle,draw] (c) {$N~O$}}
		child {node [rectangle,draw] (d) {$R~S~T~U~V$}}
		child {node [rectangle,draw] (e) {$Y~Z$}};
		\end{tikzpicture}
		\caption{Insertion de la lettre B dans le B-arbre (Figure \ref{fig:B-arbreexDef}). Le noeud en bleu contient cette nouvelle clé.}
	\end{subfigure}
	
	\hfill %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
	%(or a blank line to force the subfigure onto a new line)
	\begin{subfigure}{\textwidth}
		\begin{tikzpicture}[level/.style={sibling distance=7em/#1, level distance=3.5em}]
		\node [rectangle,draw=ForestGreen,fill=LightGreen] (a){$G~M~P~\textcolor{DarkGreen}{\textbf{T}}~X$}
		child {node [rectangle,draw] (2){$A~B~C~D~E$}}
		child {node [rectangle,draw] (b) {$J~K$}}
		child {node [rectangle,draw] (c) {$N~O$}}
		child {node [rectangle,draw=Navy,fill=LightCyan] (d) {$\textcolor{DarkBlue}{\textbf{Q}}~R~S$}}
		child {node [rectangle,draw=Red,fill=Coral] (d1) {$U~V$}}
		child {node [rectangle,draw] (e) {$Y~Z$}};
		\end{tikzpicture}
		\caption{Insertion de la lettre Q dans le B-arbre précédent. Le noeud bleu contient cette nouvelle clé, le noeud vert est le noeud qui a hérité de clé suite à l'action de \textsc{Partage} et le noeud rouge est le nouveau noeud crée suite à \textsc{Partage} pour lequel on n'a pas manipulé ces valeurs.}
	\end{subfigure}
	
	\hfill %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
	%(or a blank line to force the subfigure onto a new line)
	\begin{subfigure}{\textwidth}
		\begin{tikzpicture}[level 1/.style={sibling distance=20em, level distance=2em}, level 2/.style={sibling distance=8em, level distance=3em}]
		\node [rectangle,draw=ForestGreen,fill=LightGreen] (a){$\textcolor{DarkGreen}{\textbf{P}}$}
		child {node [rectangle,draw=Red,fill=Coral] (a1) {$G~M$}
			child {node [rectangle,draw] (2){$A~B~C~D~E$}}
			child {node [rectangle,draw=Navy,fill=LightCyan] (b) {$J~K~\textcolor{DarkBlue}{\textbf{L}}$}}
			child {node [rectangle,draw] (c) {$N~O$}}
		}
		child {node [rectangle,draw=Red,fill=Coral] (a1) {$T~X$}
			child {node [rectangle,draw] (d) {$Q~R~S$}}
			child {node [rectangle,draw] (d1) {$U~V$}}
			child {node [rectangle,draw] (e) {$Y~Z$}}
		};
		\end{tikzpicture}
		\caption{Insertion de la lettre L dans le B-arbre précédent. Le noeud bleu contient cette nouvelle clé. Comme la racine était pleine, nous lui avons appliqué l'opération \textsc{Partage}: le noeud vert est le noeud (la nouvelle racine) qui a hérité de clé suite médiane fait grandir l'arbre vers le haut. Les nœuds rouges sont les deux fils crées suite à \textsc{Partage} pour lequel on n'a pas manipulé leurs valeurs.}
	\end{subfigure}
	
	\hfill %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
	%(or a blank line to force the subfigure onto a new line)
	\begin{subfigure}{\textwidth}
		\begin{tikzpicture}[level 1/.style={sibling distance=20em, level distance=2em}, level 2/.style={sibling distance=6em, level distance=3em}]
		\node [rectangle,draw] (a){$P$}
		child {node [rectangle,draw=ForestGreen,fill=LightGreen] (a1) {$\textcolor{DarkGreen}{\textbf{C}}~G~M$}
			child {node [rectangle,draw=Red,fill=Coral] (b1){$A~B$}}
			child {node [rectangle,draw=Navy,fill=LightCyan] (a3){$D~E~\textcolor{DarkBlue}{\textbf{F}}$}}
			child {node [rectangle,draw] (b) {$J~K~L$}}
			child {node [rectangle,draw] (c) {$N~O$}}
		}
		child {node [rectangle,draw] (a2) {$T~X$}
			child {node [rectangle,draw] (d) {$Q~R~S$}}
			child {node [rectangle,draw] (d1) {$U~V$}}
			child {node [rectangle,draw] (e) {$Y~Z$}}
		};
		\end{tikzpicture}
		
		\caption{Insertion de la lettre L dans le B-arbre précédent. Le noeud bleu contient cette nouvelle clé. Comme la racine était pleine, nous lui avons appliqué l'opération \textsc{Partage}: le noeud vert est le noeud (la nouvelle racine) qui a hérité de clé suite médiane fait grandir l'arbre vers le haut. Les nœuds rouges sont les deux fils crées suite à \textsc{Partage} pour lequel on n'a pas manipulé leurs valeurs.}
	\end{subfigure}
	\caption{Insertions dans un B-arbre.}	
	\label{fig:B-arbreeInserer}
\end{figure}
	
\paragraph{Suppression dans un B-arbre} 
La suppression dans un B-arbre est plus délicate que l'insertion car nous sommes amenés à potentiellement modifier tous les nœuds internes (et plus seulement les feuilles). De manière analogue à l'insertion, la suppression peut construire un arbre avec un noeud illégal (ne effet, il peut devenir trop petit). Un algorithme naïf pourrait avoir tendance à rebrousser chemin quand un noeud devient trop petit (et à faire deux plusieurs aller-retour dans l'arbre). Nous allons présenter un algorithme qui se fait en une seule passe et ne remonte pas dans l'arbre (sauf lorsque nous devons rétrécir la hauteur de l'arbre). Dans le cas où un noeud se retrouverait sans clé alors il est supprimé et son unique fils devient la racine de l'arbre (la hauteur de l'arbre diminue de $1$). \textblue{L'arbre rétrécie par le haut (comme il grandit).}
	
\emph{Principe de la suppression}: On cherche la clé que l'on souhaite supprimer. Comme pour l'insertion, nous nous assurons que nous pouvons descendre dans l'arbre et supprimer la clé en toute légalité.
\begin{enumerate}
	\item Si la clé $k$ est dans le noeud $x$ qui est une feuille: on supprime $k$.
	\item Si la clé $k$ est dans le noeud $x$ qui n'est pas une feuille:
	\begin{enumerate}
		\item Si l'enfant $y$ qui précède $k$ a au moins $t$ clés, on cherche le prédécesseur de $k$, $k'$ (max dans la liste des clés de $y$). On supprime récursivement $k'$ dans $y$ et on remplace $k$ par $k'$. \textblue{Permet de conserver l'arité du noeud $x$.}
			
		\item Si l'enfant $y$ qui précède $k$ a $t - 1 $ clés, on examine symétriquement le fils suivant $z$. Si $z$ a au moins $t$ clés, on cherche le successeur de $k$, $k'$ (min dans la liste des clés de $z$). On supprime récursivement $k'$ dans $z$ et on remplace $k$ par $k'$. \textblue{Permet de conserver l'arité du noeud $x$.}
			
		\item Si $y$ et $z$ ont $t - 1$ clés, on fusionne $k$ dans le contenu de $z$ et on fait tout passer (en copiant) dans $y$ qui contient $2t-1$ clés ($x$ perd son pointeur vers $z$ et $k$). On libère $z$ et on supprime récursivement $k$ dans $y$. 
	\end{enumerate}
	\item Si la clé $k$ n'est pas dans le noeud $x$, on cherche le sous arbre $x.c_i$ qui devrait contenir $k$ (si il est dans l'arbre). Si $x.c_i.n = t-1$ on exécute un des deux sous-cas suivants (selon les besoin) pour garantir que l'on descend dans un noeud à au moins $t$ clés. On applique alors la récursivité sur l'enfant approprié de $x$.
	\begin{enumerate}
		\item Si $x.c_i$ n'a que $t-1$ clé mais qu'un de ces frères immédiats, $y$ à au moins $t$ clés, on bascule une des clés de $y$ (min ou max en fonction du côté) dans $x$ et on bascule la clé supplémentaire de $x$ dans $x.c_i$. Puis on reconnecte les pointeur la où il faut.
			
		\item Si $x.c_i$ et ses frères immédiats n'ont que $t-1$ clés, nous fusionnons les deux frères en descendant la bonne clé de $x$ (devient la clé médiane du noeud).
	\end{enumerate}
\end{enumerate}

\begin{figure}
	\centering
	\begin{subfigure}{\textwidth}
		\begin{tikzpicture}[level 1/.style={sibling distance=20em, level distance=2em}, level 2/.style={sibling distance=6em, level distance=3em}]
		\node [rectangle,draw] (a){$P$}
		child {node [rectangle,draw] (a1) {$C~G~M$}
			child {node [rectangle,draw] (b1){$A~B$}}
			child {node [rectangle,draw=Navy,fill=LightCyan] (a3){$D~E$}}
			child {node [rectangle,draw] (b) {$J~K~L$}}
			child {node [rectangle,draw] (c) {$N~O$}}
		}
		child {node [rectangle,draw] (a2) {$T~X$}
			child {node [rectangle,draw] (d) {$Q~R~S$}}
			child {node [rectangle,draw] (d1) {$U~V$}}
			child {node [rectangle,draw] (e) {$Y~Z$}}
		};
		\end{tikzpicture}
		\caption{Suppression de la lettre F (\textbf{cas 1}) dans le B-arbre (Figure \ref{fig:B-arbreeInserer}). Le noeud en bleu contenait la clé supprimée, c'est le noeud que nous avons manipulé.}
	\end{subfigure}
	
	\hfill %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
	%(or a blank line to force the subfigure onto a new line)
	\begin{subfigure}{\textwidth}
		\begin{tikzpicture}[level 1/.style={sibling distance=20em, level distance=2em}, level 2/.style={sibling distance=6em, level distance=3em}]
		\node [rectangle,draw] (a){$P$}
		child {node [rectangle,draw=Navy,fill=LightCyan] (a1) {$C~G~\textcolor{DarkBlue}{\textbf{L}}$}
			child {node [rectangle,draw] (b1){$A~B$}}
			child {node [rectangle,draw] (a3){$D~E$}}
			child {node [rectangle,draw=ForestGreen,fill=LightGreen] (b) {$J~K$}}
			child {node [rectangle,draw] (c) {$N~O$}}
		}
		child {node [rectangle,draw] (a2) {$T~X$}
			child {node [rectangle,draw] (d) {$Q~R~S$}}
			child {node [rectangle,draw] (d1) {$U~V$}}
			child {node [rectangle,draw] (e) {$Y~Z$}}
		};
		\end{tikzpicture}
		\caption{Suppression de la lettre M dans le B-arbre précédent (\textbf{cas 2a}). Le noeud bleu contenait la clé supprimé. On lui a ajouter une nouvelle clé issue du noeud vert afin qu'il reste légal.}
	\end{subfigure}
	
	\hfill %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
	%(or a blank line to force the subfigure onto a new line)
	\begin{subfigure}{\textwidth}
		\begin{tikzpicture}[level 1/.style={sibling distance=25em, level distance=2em}, level 2/.style={sibling distance=6em, level distance=3em}]
		\node [rectangle,draw] (a){$P$}
		child {node [rectangle,draw=Navy,fill=LightCyan] (a1) {$C~L$}
			child {node [rectangle,draw] (b1){$A~B$}}
			child {node [rectangle,draw=ForestGreen,fill=LightGreen] (b) {$D~E~J~K$}}
			child {node [rectangle,draw] (c) {$N~O$}}
		}
		child {node [rectangle,draw] (a2) {$T~X$}
			child {node [rectangle,draw] (d) {$Q~R~S$}}
			child {node [rectangle,draw] (d1) {$U~V$}}
			child {node [rectangle,draw] (e) {$Y~Z$}}
		};
		\end{tikzpicture}
		\caption{Suppression de la lettre G dans le B-arbre précédent (\textbf{cas 2c}). Le noeud bleu contenait la clé supprimé. Le noeud vert est le résultat (légal) de la fusion des deux fils de la clé enlevée: on ne pouvait pas appliquer une rotation comme précédemment.}
	\end{subfigure}
	
	\hfill %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
	%(or a blank line to force the subfigure onto a new line)
	\begin{subfigure}{\textwidth}
		\begin{tikzpicture}[level 1/.style={sibling distance=8em, level distance=1.5em}, level 2/.style={sibling distance=7.5em, level distance=3.5em}]
		\node [rectangle,draw=Red,fill=Coral] (a){$ $}
		child {node [rectangle,draw=ForestGreen,fill=LightGreen] (a1) {$C~L~\textcolor{DarkGreen}{\textbf{P}}~T~X$}
			child {node [rectangle,draw] (b1){$A~B$}}
			child {node [rectangle,draw=Navy,fill=LightCyan] (b) {$E~J~K$}}
			child {node [rectangle,draw] (c) {$N~O$}}
			child {node [rectangle,draw] (d) {$Q~R~S$}}
			child {node [rectangle,draw] (d1) {$U~V$}}
			child {node [rectangle,draw] (e) {$Y~Z$}}
		};
		\end{tikzpicture}
		\caption{Suppression de la lettre D dans le B-arbre précédent (\textbf{cas 3b}). Comme on ne peut pas descendre dans la récursivité (car le noeud $C~L$ ne contient que deux clés) nous avons fusionner les deux fils de la racines en rajoutant la clé $P$. Le noeud vert est le résultat de cette fusion. Le noeud bleu contenait la clé supprimé. Le noeud rouge quant à lui est le noeud de la racine (vide).}
	\end{subfigure}
	
	\hfill %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
	%(or a blank line to force the subfigure onto a new line)
	\begin{subfigure}{\textwidth}
		\begin{tikzpicture}[level 1/.style={sibling distance=8em, level distance=3em}]
		\node [rectangle,draw] (a1) {$C~L~P~T~X$}
			child {node [rectangle,draw] (b1){$A~B$}}
			child {node [rectangle,draw] (b) {$E~J~K$}}
			child {node [rectangle,draw] (c) {$N~O$}}
			child {node [rectangle,draw] (d) {$Q~R~S$}}
			child {node [rectangle,draw] (d1) {$U~V$}}
			child {node [rectangle,draw] (e) {$Y~Z$}}
		;
		\end{tikzpicture}
		\caption{Suppression de la racine vide. Dans ce cas la hauteur de l'arbre est diminue de $1$.}
	\end{subfigure}

	\hfill %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
	%(or a blank line to force the subfigure onto a new line)
	\begin{subfigure}{\textwidth}
		\begin{tikzpicture}[level 1/.style={sibling distance=8em, level distance=3em}]
		\node [rectangle,draw=ForestGreen,fill=LightGreen] (a1) {$\textcolor{DarkGreen}{\textbf{E}}~L~P~T~X$}
			child {node [rectangle,draw=Navy,fill=LightCyan] (b1){$A~\textcolor{DarkBlue}{\textbf{C}}$}}
			child {node [rectangle,draw=ForestGreen,fill=LightGreen] (b) {$J~K$}}
			child {node [rectangle,draw] (c) {$N~O$}}
			child {node [rectangle,draw] (d) {$Q~R~S$}}
			child {node [rectangle,draw] (d1) {$U~V$}}
			child {node [rectangle,draw] (e) {$Y~Z$}}
		;
		\end{tikzpicture}
		\caption{Suppression de la lettre D dans le B-arbre précédent (\textbf{cas 3a}). Comme on ne peut pas descendre dans la récursivité (car le noeud $A~B$ ne contient que deux clés) mais nous ne pouvons pas fusionner les deux fils de la racine car le fils gauche contient trois clés. Les nœuds verts est le résultat d'une rotation des clés. Le noeud bleu contenait la clé supprimé.}
	\end{subfigure}
	
	\caption{Suppressions dans un B-arbre.}	
	\label{fig:B-arbreSuppression}
\end{figure}
	
\emph{Complexité}: $O(log_t n) = O(h)$ lecture-écriture sur le disque (on ne fait que $O(1)$ de lecture-écriture entre deux appels récursif) où $h$ est la hauteur et $n$ le nombre de nœuds. Comme $n < 2t$, les boucles \textbf{while} s'effectue en $O(t)$, le temps processeur total est $O(th) = O(t \log n)$.