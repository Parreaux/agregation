%DEVs: tri topologique
%LECONs : 925
\emph{Stratégie du parcours} : Parcourir l'ensemble des nœuds du graphe en privilégiant la profondeur: on va le plus loin possible dans le dit graphe.

\emph{Astuces} : On utilise une coloration des nœuds du graphe afin de savoir quels sont les nœuds que nous avons déjà pris: pas étudier, en cours, fini. Cette coloration nous permet de nous assurer d'avoir parcouru tous les nœuds et de l'avoir fait une unique fois. On utilise aussi des dates de début et de fin de traitement pour chacun des nœuds. Ces dates jouent le même rôle que les couleurs et peuvent être exploité dans certaines applications comme le calcul de composantes fortement connexes. Ces deux astuces ne sont pas obligatoirement utiliser en même temps même si l'une ou l'autre peut ainsi simplifier les preuves de correction, de terminaison ou de complexité.

\noindent\emph{Applications du parcours en profondeur} : 
\begin{itemize}
	\item Tri topologique : à la sortie de \textsc{Visiter}, on ajoute le sommet que l'on vient de visiter dans une liste.
	\item Composantes fortement connexes :  on effectue deux passages de l'algorithme (un sur le graphe et le deuxième sur le graphe complémentaire dont l'ordre de l'étude des sommets est donné par les dates calculées lors du premier parcours.)
\end{itemize}

\paragraph{Présentation de l'algorithmique autours du parcours en profondeur de graphe} Nous présentons ici un algorithme récursif (Algorithme~\ref{algo:parcours_profondeur}) du parcours de graphe en profondeur \cite[p.558]{Cormen}. Cet algorithme fait appelle à une fonction auxiliaire (qui porte la récursivité) \textsc{Visite} (Algorithme~\ref{algo:parcours_visite}) qui traite chacun des sommets du graphe et décide de l'ordre dans lequel on les traites. Nous prouverons également la correction et la terminaison de cet algorithme. Nous finirons par étudier sa complexité. La figure~\ref{fig:parcours_ex} nous donne un exemple d'application de l'algorithme de parcours en profondeur.

\begin{minipage}[l]{.46\linewidth}
	\begin{algorithm}[H]
		\begin{algorithmic}[1] 
			\Function{\textsc{Parcours-Profondeur}}{$G$} 
			\For{tout $u \in G.V$}
			\State $u.couleur \gets \textsf{blanc}$
			\State $u.\pi \gets \textsc{Nil}$ \Comment{Prédécesseurs}
			\EndFor
			\State $date \gets 0$
			\For{tout $u \in G.V$} 
			\If{$u.couleur = \textsf{blanc}$}
			\State $\textsc{Visite}(G, u)$
			\EndIf
			\EndFor
			\EndFunction
		\end{algorithmic}
		\caption{Le parcours en profondeur d'un graphe.}
		\label{algo:parcours_profondeur}
	\end{algorithm}
\end{minipage} \hfill
\begin{minipage}[r]{.46\linewidth}
	\begin{algorithm}[H]
		\begin{algorithmic}[1] 
			\Function{\textsc{Visite}}{$G, u$}
			\State $date \gets date + 1$ \Comment{$u$ juste découvert}
			\State $u.d \gets date$
			\State $u.couleur = \textsf{gris}$
			\For{tout $v \in Adj(u)$} 
			\If{$v.couleur = \textsf{blanc}$}
			\State $v.\pi \gets u$
			\State $\textsc{Visite}(G, u)$
			\EndIf
			\EndFor
			\State $u \gets \textsf{noir}$
			\State $date \gets date + 1$
			\State $u.f \gets date$
			\EndFunction
		\end{algorithmic}
		\caption{Visiter un sommet: traiter ce sommet lors du parcours}
		\label{algo:parcours_visite}
	\end{algorithm}		
\end{minipage}

\begin{theo}[Terminaison]
	Soit $G = (V, E)$ un graphe. Le parcours en profondeur sur ce graphe termine.
\end{theo}
\begin{proof}
	La terminaison provient du fait qu'on appel la fonction \textsf{Visite} au plus $|V|$.
\end{proof}

\begin{req}
	L'ordre dans lequel les sommets sont examinés influe sur la sortie de l'algorithme. Mais en pratique, cet ordre nous importe peu car les sorties possibles sont équivalentes.
\end{req}

\begin{theo}[Complexité]
	Soit $G = (V, E)$ un graphe. Le parcours en profondeur sur ce graphe se fait en $O(|V| + |E|)$.
\end{theo}
\begin{proof}
	On applique la méthode de l'agrégat. On applique exactement $|V|$ fois la procédure \textsf{Visite}. De plus, pendant tout le parcours, la boucle dans \textsf{Visite} s'exécute $\sum_{v \in S} |Adj(v)| = \Theta(|E|)$. Le temps d'exécution du parcours en profondeur est $\Theta(|S| + |E|)$.
\end{proof}

\begin{figure}
	\begin{subfigure}{\textwidth}
	\begin{minipage}[c]{.46\linewidth}
		\begin{tikzpicture}[font=\sffamily]
		% Setup the style for the states
		\tikzset{node style/.style={state, 
			minimum width=1cm,
			line width=.25mm,
			fill=white}}
	
		% Draw the states
		\node[node style,fill=LightGray] at (0,0) (0) {$0$};
		\node[node style] at (1.5,1.5) (2) {$2$};
		\node[node style] at (1.5, -1.5) (1) {$1$};
		\node[node style] at (4, 1.5)    (3) {$3$};
		\node[node style] at (4, -1.5)   (4) {$4$};
		\node[node style] at (6, 0)    (5) {$5$};
	
		% Connect the states with arrows
		\draw[every loop,auto=right,line width=.25mm,
				>=latex]
		(0) edge [bend right] node {} (1)
		(1) edge []           node {} (0)
		    edge []           node {} (2)
		(2) edge []           node {} (3)
		    edge [bend right] node {} (0)
		    edge []           node {} (4)
		(3) edge []           node {} (5)
		    edge []           node {} (4)
		(4) edge []           node {} (5)
			edge []           node {} (1)
		(5) edge []           node {} (2);
		\end{tikzpicture}
		\caption{Graphe sur lequel on applique le parcours en profondeur}
	\end{minipage}\hfill
	\begin{minipage}[l]{.46\linewidth}
		\begin{tikzpicture}[font=\sffamily]
		% Setup the style for the states
		\tikzset{node style/.style={state, 
				minimum width=1cm,
				line width=.25mm,
				fill=white}}
		
		% Draw the states
		\node[node style,fill=LightGray] at (0,0) (0) {$0$};
		\node[node style] at (1.5,1.5) (2) {$2$};
		\node[node style,fill=LightGray] at (1.5,-1.5) (1) {$1$};
		\node[node style] at (4,1.5) (3) {$3$};
		\node[node style] at (4,-1.5) (4) {$4$};
		\node[node style] at (6,0) (5) {$5$};
		
		% Connect the states with arrows
		\draw[every loop,auto=right,line width=.25mm,
		>=latex]
		(0) edge [bend right] node {} (1)
		(1) edge []           node {} (0)
		edge []           node {} (2)
		(2) edge []           node {} (3)
		edge [bend right] node {} (0)
		edge []           node {} (4)
		(3) edge []           node {} (5)
		edge []           node {} (4)
		(4) edge []           node {} (5)
		edge []           node {} (1)
		(5) edge []           node {} (2);
		\end{tikzpicture}
		\caption{Visite du sommet $1$.}
	\end{minipage}
	
	\end{subfigure}
	\begin{subfigure}{\textwidth}
		\begin{minipage}[c]{.46\linewidth}
			\begin{tikzpicture}[font=\sffamily]
			% Setup the style for the states
			\tikzset{node style/.style={state, 
					minimum width=1cm,
					line width=.25mm,
					fill=white}}
			
			% Draw the states
			\node[node style,fill=LightGray] at (0,0) (0) {$0$};
			\node[node style,fill=LightGray] at (1.5,1.5) (2) {$2$};
			\node[node style,fill=LightGray] at (1.5, -1.5) (1) {$1$};
			\node[node style] at (4, 1.5) (3) {$3$};
			\node[node style] at (4, -1.5) (4) {$4$};
			\node[node style] at (6, 0) (5) {$5$};
			
			% Connect the states with arrows
			\draw[every loop,auto=right,line width=.25mm,
			>=latex]
			(0) edge [bend right] node {} (1)
			(1) edge []           node {} (0)
			edge []           node {} (2)
			(2) edge []           node {} (3)
			edge [bend right] node {} (0)
			edge []           node {} (4)
			(3) edge []           node {} (5)
			edge []           node {} (4)
			(4) edge []           node {} (5)
			edge []           node {} (1)
			(5) edge []           node {} (2);
			\end{tikzpicture}
			\caption{Visite du sommet $2$.}
		\end{minipage}\hfill
		\begin{minipage}[l]{.46\linewidth}
			\begin{tikzpicture}[font=\sffamily]
			% Setup the style for the states
			\tikzset{node style/.style={state, 
					minimum width=1cm,
					line width=.25mm,
					fill=white}}
			
			% Draw the states
			\node[node style,fill=LightGray] at (0,0) (0) {$0$};
			\node[node style,fill=LightGray] at (1.5,1.5) (2) {$2$};
			\node[node style,fill=LightGray] at (1.5,-1.5) (1) {$1$};
			\node[node style] at (4,1.5) (3) {$3$};
			\node[node style,fill=LightGray] at (4,-1.5) (4) {$4$};
			\node[node style] at (6,0) (5) {$5$};
			
			% Connect the states with arrows
			\draw[every loop,auto=right,line width=.25mm,
			>=latex]
			(0) edge [bend right] node {} (1)
			(1) edge []           node {} (0)
			edge []           node {} (2)
			(2) edge []           node {} (3)
			edge [bend right] node {} (0)
			edge []           node {} (4)
			(3) edge []           node {} (5)
			edge []           node {} (4)
			(4) edge []           node {} (5)
			edge []           node {} (1)
			(5) edge []           node {} (2);
			\end{tikzpicture}
			\caption{Visite du sommet $4$.}
		\end{minipage}
	\end{subfigure}
	\begin{subfigure}{\textwidth}
		\begin{minipage}[c]{.46\linewidth}
			\begin{tikzpicture}[font=\sffamily]
			% Setup the style for the states
			\tikzset{node style/.style={state, 
					minimum width=1cm,
					line width=.25mm,
					fill=white}}
			
			% Draw the states
			\node[node style,fill=LightGray] at (0,0) (0) {$0$};
			\node[node style,fill=LightGray] at (1.5,1.5) (2) {$2$};
			\node[node style,fill=LightGray] at (1.5, -1.5) (1) {$1$};
			\node[node style] at (4, 1.5) (3) {$3$};
			\node[node style,fill=LightGray] at (4, -1.5) (4) {$4$};
			\node[node style,fill=LightGray] at (6, 0) (5) {$5$};
			
			% Connect the states with arrows
			\draw[every loop,auto=right,line width=.25mm,
			>=latex]
			(0) edge [bend right] node {} (1)
			(1) edge []           node {} (0)
			edge []           node {} (2)
			(2) edge []           node {} (3)
			edge [bend right] node {} (0)
			edge []           node {} (4)
			(3) edge []           node {} (5)
			edge []           node {} (4)
			(4) edge []           node {} (5)
			edge []           node {} (1)
			(5) edge []           node {} (2);
			\end{tikzpicture}
			\caption{Visite du sommet $5$.}
		\end{minipage}\hfill
		\begin{minipage}[l]{.46\linewidth}
			\begin{tikzpicture}[font=\sffamily]
			% Setup the style for the states
			\tikzset{node style/.style={state, 
					minimum width=1cm,
					line width=.25mm,
					fill=white}}
			
			% Draw the states
			\node[node style,fill=LightGray] at (0,0) (0) {$0$};
			\node[node style,fill=LightGray] at (1.5,1.5) (2) {$2$};
			\node[node style,fill=LightGray] at (1.5,-1.5) (1) {$1$};
			\node[node style] at (4,1.5) (3) {$3$};
			\node[node style,fill=DarkGray] at (4,-1.5) (4) {$4$};
			\node[node style,fill=DarkGray] at (6,0) (5) {$5$};
			
			% Connect the states with arrows
			\draw[every loop,auto=right,line width=.25mm,
			>=latex]
			(0) edge [bend right] node {} (1)
			(1) edge []           node {} (0)
			edge []           node {} (2)
			(2) edge []           node {} (3)
			edge [bend right] node {} (0)
			edge []           node {} (4)
			(3) edge []           node {} (5)
			edge []           node {} (4)
			(4) edge []           node {} (5)
			edge []           node {} (1)
			(5) edge []           node {} (2);
			\end{tikzpicture}
			\caption{Fin des visites des sommets $4$ et $5$.}
		\end{minipage}
	\end{subfigure}
	\begin{subfigure}{\textwidth}
		\begin{minipage}[c]{.46\linewidth}
			\begin{tikzpicture}[font=\sffamily]
			% Setup the style for the states
			\tikzset{node style/.style={state, 
					minimum width=1cm,
					line width=.25mm,
					fill=white}}
			
			% Draw the states
			\node[node style,fill=LightGray] at (0,0) (0) {$0$};
			\node[node style,fill=LightGray] at (1.5,1.5) (2) {$2$};
			\node[node style,fill=LightGray] at (1.5, -1.5) (1) {$1$};
			\node[node style,fill=LightGray] at (4, 1.5) (3) {$3$};
			\node[node style,fill=DarkGray] at (4, -1.5) (4) {$4$};
			\node[node style,fill=DarkGray] at (6, 0) (5) {$5$};
			
			% Connect the states with arrows
			\draw[every loop,auto=right,line width=.25mm,
			>=latex]
			(0) edge [bend right] node {} (1)
			(1) edge []           node {} (0)
			edge []           node {} (2)
			(2) edge []           node {} (3)
			edge [bend right] node {} (0)
			edge []           node {} (4)
			(3) edge []           node {} (5)
			edge []           node {} (4)
			(4) edge []           node {} (5)
			edge []           node {} (1)
			(5) edge []           node {} (2);
			\end{tikzpicture}
			\caption{Visite du sommet $3$.}
		\end{minipage}\hfill
		\begin{minipage}[l]{.46\linewidth}
			\begin{tikzpicture}[font=\sffamily]
			% Setup the style for the states
			\tikzset{node style/.style={state, 
					minimum width=1cm,
					line width=.25mm,
					fill=white}}
			
			% Draw the states
			\node[node style,fill=DarkGray] at (0,0) (0) {$0$};
			\node[node style,fill=DarkGray] at (1.5,1.5) (2) {$2$};
			\node[node style,fill=DarkGray] at (1.5,-1.5) (1) {$1$};
			\node[node style,fill=DarkGray] at (4,1.5) (3) {$3$};
			\node[node style,fill=DarkGray] at (4,-1.5) (4) {$4$};
			\node[node style,fill=DarkGray] at (6,0) (5) {$5$};
			
			% Connect the states with arrows
			\draw[every loop,auto=right,line width=.25mm,
			>=latex]
			(0) edge [bend right] node {} (1)
			(1) edge []           node {} (0)
			edge []           node {} (2)
			(2) edge []           node {} (3)
			edge [bend right] node {} (0)
			edge []           node {} (4)
			(3) edge []           node {} (5)
			edge []           node {} (4)
			(4) edge []           node {} (5)
			edge []           node {} (1)
			(5) edge []           node {} (2);
			\end{tikzpicture}
			\caption{Fin des visites des sommets $3$, $2$, $1$ et $0$.}
		\end{minipage}
	\end{subfigure}
	\caption{Le parcours en profondeur d'un graphe. On obtient alors : $0$, $1$, $2$, $4$, $5$ et $3$.}
	\label{fig:parcours_ex}
\end{figure}

\paragraph{Quelques propriétés que l'on peut déduire du parcours} Le parcours en profondeur d'un graphe révèle quelques unes de ces structures. 

\begin{theo}[Théorème des parenthèses]
	Dans un parcours en profondeur d'un graphe $G = (V, E)$, pour deux sommets quelconques $u$ et $v$, une et une seule des trois conditions suivantes est vérifiée :
	\begin{itemize}
		\item les intervalles $[u.d, u.f]$ et $[v.d, v.f]$ sont disjoints, et ni $u$ ni $v$ n'est un descendant de l'autre dans la forêt de parcours en profondeur;
		\item l'intervalle $[u.d, u.f]$ est entièrement inclus dans l'intervalle $[v.d, v.f]$, et ni $u$ est un descendant de $v$ dans un arbre de parcours en profondeur;
		\item l'intervalle $[v.d, v.f]$ est entièrement inclus dans l'intervalle $[u.d, u.f]$, et ni $v$ est un descendant de $u$ dans un arbre de parcours en profondeur.
	\end{itemize}
\end{theo}
\begin{proof}
	Supposons que $u.d < u.f$. On distingue alors deux sous-cas. Si $v.d < u.f$, alors $v$ a été découvert pendant que $u$ était encore \textsf{gris} ($u$ est cours de traitement). Donc $v$ est un descendant de $u$. Puisque la découverte de $v$ est plus récente que $u$, le traitement de $v$ se termine avant le traitement de $u$. Donc, l'intervalle $[v.d, v.f]$ est entièrement inclus dans l'intervalle $[u.d, u.f]$.
	
	Dans l'autre sous-cas, $u.f < v.d$ ce qui implique que $u.d < u.f < v.d < v.f$ et donc les intervalles $[u.d, u.f]$ et $[v.d, v.f]$ sont disjoints. Comme les intervalles sont disjoints, aucun des deux sommets n'a été découvert pendant que l'autre était \textsf{gris}. Donc, aucun sommet n'est un descendant de l'autre.
	
	Dans le cas $v.d < u.d$, on raisonne de manière analogue en inversant les rôles de $u$ et $v$.
\end{proof}

\begin{cor}[Imbrication des intervalles descendants]
	Le sommet $v$ est un descendant propre du sommet $u$ dans la forêt de parcours en profondeur d'un graphe $G$ si et seulement si $u.d < v.d < v.f < u.f$.
\end{cor}
\begin{proof}
	Immédiat par le théorème précédent.
\end{proof}

\begin{theo}[Théorème du chemin blanc]
	Dans une forêt de parcours en profondeur d'un graphe $G = (V, E)$, un sommet $v$ est un descendant d'un sommet $u$ si et seulement si, au moment $u.d$ où le parcours découvre $u$, il existe un chemin de $u$ à $v$ composé uniquement de sommets blancs.
\end{theo}
\begin{proof}
	$\Rightarrow$: Si $v = u$, alors le chemin de $u$ à $v$ contient uniquement le sommet $u$, qui est encore \textsf{blanc} quand on définit la valeur de $u.f$. Supposons que $v$ soit un descendant propre de $u$ dans la forêt de parcours en profondeur. D'après ca qui précède, $u.d < v.d$ et donc $v$ est blanc à l'instant $u.d$. Comme $v$ peut être un descendant quelconque de $u$, tous les sommets situés sur le chemin simple unique entre $u$ et $v$ dans la forêt de parcours sont \textsf{blanc} à l'instant $u.d$.
	
	$\Leftarrow$: Supposons qu'à l'instant $u.d$ il existe un chemin constitué de sommets \textsf{blanc} entre $u$ et $v$, mais que $v$ ne devient pas un descendant de $u$ dans l'arbre de parcours en profondeur. Sans perte de généralité, on suppose que tous les autres sommets de ce chemin (sauf $v$) deviennent un descendant de $u$ dans l'arbre. \textblue{Si ce n'est pas le cas, on choisi le plus proche de $u$ qui vérifie cette propriété.} Soit $w$ le prédécesseur de $v$ dans ce chemine \textblue{($u$ et $w$ peuvent être le même chemin)}. D'après ce qui précède, $w.f \leq u.f$. Comme $v$ doit être découvert après $u$, mais avant que le traitement de $w$ soit terminé, on a $u.d < v.d < w.f \leq u.f$. Le théorème des intervalles implique alors que l'intervalle $[v.d, v.f]$ est entièrement inclus dans l'intervalle $[u.d, u.f]$. Donc, par le corollaire, $v$ est un descendant de $u$.
\end{proof}

\paragraph{Classification des arcs} Le parcours en profondeur peut permettre de classer les arcs du graphe $G= (V, E)$. Ce type peut fournir des informations sur le graphe.

\begin{definition}[Classification des arcs]
	Soit $G = (V, E)$ un graphe. Le parcours en profondeur donne une classification des arcs du graphe.
	\begin{enumerate}
		\item Les arcs de liaison sont les arcs de la forêt de parcours en profondeur. L'arc $(u, v)$ est un arc de liaison si $v$ a été découvert la première fois pendant le parcours de l'arc $(u, v)$.
		\item Les arcs arrière sont les arcs $(u, v)$ reliant un sommet $u$ à un ancêtre $v$ dans un arbre de parcours en profondeur. Les boucles (dans un graphes orienté) sont considérées comme des arcs arrière.
		\item Les arcs avant sont les arcs $(u, v)$ qui ne sont pas des arcs de liaison et qui relient un sommet $u$ à un descendant $v$ dans un arbre de parcours en profondeur.
		\item Les arcs transverse sont tous les autres arcs. Ils peuvent relier deux sommets d'un même arbre de parcours en profondeur, du moment que l'un des sommets n'est pas un ancêtre de l'autre. Ils peuvent aussi deux sommets appartenant à des arbres de parcours en profondeur différents.
	\end{enumerate}
\end{definition}

Lors du parcours en profondeur du graphe, on peut déterminer la nature de l'arc. Lorsque l'arc est exploré pour la première fois:
\begin{enumerate}
	\item \textsf{blanc} indique un arc de liaison;
	\item \textsf{gris} indique un arc arrière;
	\item \textsf{noir} indique un arc avant ou de transverse.
\end{enumerate}

\begin{prop}
	Dans un parcours en profondeur d'un graphe non orienté $G$, chaque arête de $G$ est soit un arc de liaison, soit un arc arrière.
\end{prop}
\begin{proof}
	Soit $(u, v)$ un arc de $G$ tel que $u.d < v.d$. Alors, $v$ doit être découvert et son traitement terminé avant le traitement de $u$ (pendant que $u$ est \textsf{gris}) \textblue{($v$ se trouve dans la liste d'adjacence de $u$)}. Si l'arête $(u, v)$ est d'abord exploré dans le sens $u$ vers $v$, alors $v$ n'a pas été découvert (\textsf{blanc}) jusqu'ici \textblue{(sinon on aurait déjà exploré cet arête dans la direction de $v$ vers $u$)}. Dans ce cas, $(u, v)$ est un arc de liaison.
	
	Si $(u, v)$ est exploré dans l'autre sens, on a un arc arrière car $u$ est encore \textsf{gris} lors de la première exploration. 
\end{proof}

\begin{prop}
	Un graphe $G$ est acyclique si et seulement si un parcours en profondeur de $G$ ne génère aucun arc arrière.
\end{prop}
\begin{proof}
	$\Rightarrow$: Supposons qu'un parcours en profondeur produise un arc arrière. Alors, le sommet $v$ est un ancêtre du sommet $u$ dans la forêt de parcours en profondeur. Donc, il existe un chemin $v$ à $u$ dans $G$ et l'arc arrière complète le cycle partant de $v$.
	
	$\Leftarrow$: Supposons que $G$ contienne un cycle $c$. On va montrer qu'un parcours en profondeur de $G$ génère un arc arrière. Soit $v$ le premier sommet découvert dans $c$ et soit $(u, v)$ l'arc précédent dans $c$. A l'instant $v.d$, les sommets de $c$ forment un chemin entre $v$ et $u$ composé de sommets \textsf{blanc}. D'après le théorème du chemin blanc, le sommet $u$ devient un descendant de $v$ dans la forêt de parcours en profondeur. Donc $(u, v)$ est donc un arc arrière.
\end{proof}

\paragraph{Structures de données et algorithmes affiliés} Le parcours en profondeur peut être écrit de manière impérative à l'aide d'une pile. \textblue{Dans le cas de l'algorithme récursif, la pile est caché dans le boucle \textsf{pour} et de la fonction \textsf{Visite}.}