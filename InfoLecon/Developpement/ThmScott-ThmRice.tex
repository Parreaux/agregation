\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{array}
\usepackage{tikz}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\newtheorem*{appli}{Application}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Théorème de Scott et application au théorème de Rice}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Hindley \cite[p.91]{HindleySeldin}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}}  914 (Décidabilité, Indécidabilité); 929 (Lambda-calcul).
		}}
	
	\section{Introduction}
	
	Le $\lambda$-calcul est un modèle de calcul qui vient toucher la frontière de l'indécidabilité. Le théorème de Scott nous confirme que séparer deux ensemble de $\lambda$-termes (deux ensembles de fonctions possédant leurs propres caractéristiques) est très indécidable. Ce résultat nous permet notamment de déduire le théorème de Rice. Par leur proximité avec les langages de programmation fonctionnels. Nous venons de prouver que la correction d'un programme est impossible à implémenter par un programme purement fonctionnel (le résultat est vrai pour tous les langages bien évident).
	
	\titlebox{red}{Remarques sur le développement}{
		\begin{enumerate}
			\item Preuve du lemme: $F \rrbracket gd(X) \llbracket = X$.
			\item Preuve du théorème pour les ensembles récursivement énumérables.
			\item Application au théorème de Rice.
		\end{enumerate}
	}
	
	\section{Preuve du théorème de Scott}
	
	On note $\Lambda$ un ensemble de $\lambda$-termes non vide clos par $\beta$-équivalence. On se donne un codage des $\lambda$-termes : $\textsf{gd}: \Lambda \to \N$ injectif.
	
	\begin{theo}[Théorème de Scott]
		Soient $A$ et $B$ deux sous-ensembles de $\Lambda$. Alors $A$ et $B$ ne sont pas récursivement énumérable.
	\end{theo}
	
	\begin{definition}
		 Soient $A$ et $B$ deux sous-ensembles de $\Lambda$. $A$ et $B$ sont récursivement séparable si et seulement s'il existe un ensemble récursif (son indicatrice est récursive) tel que $(A\subset C) \wedge (B \cap C = \emptyset)$.
	\end{definition}
	
	\begin{lemme}
		Pour tout $F \in \Lambda$, il existe $X$ tel que $F \llbracket \textsf{gd}(X) \rrbracket = X$.
	\end{lemme}
	\begin{proof}
		On définit
		\begin{displaymath}
		\begin{array}{cl}
		\textsf{Ap}\llbracket \textsf{gd}(M) \rrbracket\left(\llbracket \textsf{gd}(N) \rrbracket\right) = \llbracket \textsf{gd}(MN) \rrbracket & \textblue{\text{Interprété comme l'interprétation de $M$ appliqué en $N$}} \\
		\textsf{Num}\llbracket \textsf{gd}(n) = \llbracket \textsf{gd}\left(\llbracket \textsf{gd}(n) \rrbracket\right)\rrbracket & \textblue{\text{Interprété comme la traduction d'un entier}} \\
		\end{array}
		\end{displaymath}
		On pose $W = \lambda x. F(\textsf{Ap} x (Num x))$ et $X = W\llbracket \textsf{gd}(W) \rrbracket$. Alors,
		\begin{displaymath}
		\begin{array}{ccll}
		X & =_{\beta} & F(\textsf{Ap} \llbracket \textsf{gd}(W) \rrbracket (Num \llbracket \textsf{gd}(W) \rrbracket)) & \\
		& = & F\left(\textsf{Ap} \llbracket \textsf{gd}(W) \rrbracket \left(\llbracket \textsf{gd}\left(\llbracket \textsf{gd}(W) \rrbracket\right)\right)\right) & \textblue{\text{(En remplaçant \textsf{Num})}}\\
		& = & F\left(\llbracket \textsf{gd}(W \llbracket \textsf{gd}(W) \rrbracket)\rrbracket\right) & \textblue{\text{(En remplaçant \textsf{Ap})}}\\
		& = & F\llbracket X \rrbracket & \textblue{\text{(Définition de $X$)}}\\
		\end{array}
		\end{displaymath}
	\end{proof}
	
	\begin{proof}[Démonstration du théorème]
		Soit $M_0 \in A$ et $M_1 \in B$. Raisonnons par l'absurde et supposons que $C$ un ensemble récursif séparant $A$ et $B$. La fonction caractéristique de $\textsf{gd}(C)$ est récursive et définie par $F$ comme:
		\begin{displaymath}
			\begin{array}{ccl}
			M \in C & \Rightarrow & F \llbracket \textsf{gd}(M) \rrbracket = \llbracket 0 \rrbracket \\
			M \notin C & \Rightarrow & F \llbracket \textsf{gd}(M) \rrbracket = \llbracket 1 \rrbracket \\
			\end{array}
		\end{displaymath}
		
		On pose $G = \lambda x (\textsf{Zero}(Fx)) M_1M_0$ \textblue{(définie comme la conditionnelle sur la fonction caractéristique et être dans $C$ donne le résultat inverse)}. Alors, si $M \in C$, on obtient
		\begin{displaymath}
			G \llbracket \textsf{gd}(M) \rrbracket =_{\beta} \textsf{Zero}(F \llbracket \textsf{gd}(M) \rrbracket) M_1M_0  =_{\beta} \textsf{Zero}(\llbracket 0 \rrbracket) M_1M_0 =_{\beta} M_1
		\end{displaymath}
		En raisonnant de même, si $M \in C$, on obtient $G \llbracket \textsf{gd}(M) \rrbracket =_{\beta} M_0$.
			
		Par le lemme, il existe $X \in \Lambda$ tel que $G \llbracket \textsf{gd}(X) \rrbracket =_{\beta} X$. On en déduit que si 
		\begin{displaymath}
			\begin{array}{cclcll}
			X \in C & \Rightarrow & X = G \llbracket \textsf{gd}(X) \rrbracket = M_1 & \text{et} & X \notin C & \textblue{\text{car } B \cap C = \emptyset} \\
			X \notin C & \Rightarrow & X = G \llbracket \textsf{gd}(X) \rrbracket = M_0 & \text{et} & X \in C & \textblue{\text{car } A \subset C} \\
			\end{array}
		\end{displaymath}
		Contradiction.
	\end{proof}
	
	\begin{definition}
		$A$ un sous-ensemble de $\Lambda$ est non trivial si $A \neq \emptyset$ et $A \neq \Lambda$.
	\end{definition}
	
	\begin{cor}[Théorème de Rice]
		Soit $A$ un sous-ensemble de $\Lambda$ non trivial. Alors, $A$ n'est pas récursif.
	\end{cor}
	\begin{proof}
		Soit $A$ un sous-ensemble de $\Lambda$ non trivial. On applique le premier point du théorème à $A$ et à $A^c$; Donc pour tout ensemble récursif $C$ tel que $A \subseteq C$, $C \cap A^c \neq \emptyset$. En particulier $C \neq A$ et $A$ n'est pas récursif.
	\end{proof}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}