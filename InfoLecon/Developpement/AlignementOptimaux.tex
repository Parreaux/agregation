\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames,svgnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}
\usepackage{subcaption}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}


\parskip=0\baselineskip %0.5 d'habitude
\usetikzlibrary{automata,positioning,shadows}
\usetikzlibrary{calc, shapes, backgrounds,arrows}

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
} 

\begin{document}
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\sloppy
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\newtheorem*{appli}{Application}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Alignements optimaux}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Crochemore \cite[p.224]{CrochemoreHancartLecroq}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 907 (Algorithme du texte); 931 (Schémas algorithmiques).
			
			\textblue{\emph{Leçons où on peut l'évoquer:}} 921 (Recherche).
		}}

	\section{Introduction}
	
	Les distances d'éditions permettent de donner la similitude entre deux mots. Connaître cette distance permet aux correcteurs orthographiques de proposer des corrections en cas de mots mal orthographier (on va plus loin que juste savoir si le mot existe ou non) ou en bio-informatique elle permet de mettre en évidence des mutations probables dans un génome sur deux individus distincts.
	
	Calculer une distance d'édition revient souvent à calculer l'alignement optimal. En effet, une distance d'édition est le coût minimal d'une transformation d'un mot à l'autre. Le calcul de l'alignement optimal peut également chercher un motif avec à une distance d'au plus $k$.
	
	\titlebox{red}{\textred{\textbf{Remarques sur le développement}}}{
		Ce développement permet de présenter la construction d'un algorithme de programmation dynamique: il est donc important d'en faire ressortir les apports d'un tel paradigme ainsi que sa structure.
		\begin{enumerate}
			\item Présentation du problème et limites du graphe d'édition (à l'oral).
			\item Présentation de la structure du problème.
			\item Preuve de l'équation récursive solution.
			\item Écriture de l'algorithme final.
		\end{enumerate}
	}
	
	\section{Calcul des alignements optimaux}
	
	\emph{Cadre}: Soient $x, y \in \Sigma^*$ de longueurs respectives $m$ et $n$. 
	
	\emph{Objectif}: calculer un alignement optimal (qui n'est pas unique) afin de calculer $Dist(x, y)$. 
	
	\paragraph{Le graphe d'édition} Le graphe d'édition traduit l'ensemble des choix qui décrit l'alignement (Figure~\ref{fig:graphe_edition}, \textblue{faire le dessin}). 
	
	\begin{definition}[\textblue{(On la donne à l'oral)}]
		Le graphe d'édition est composé des sommets correspondant à l'ensembles des paires de préfixes des deux mots dont on calcul l'alignement optimal. On passe alors d'un état vers un autre à l'aide d'une opération d'édition et son coût pondère le graphe.
	\end{definition}  
	
	\textblue{(Un chemin de l'origine jusqu'à un nœud traduit l'alignement entre les deux préfixes.)} Tout chemin de $(-1, -1)$ vers $(m-1, n-1)$ dans ce graphe est un alignement entre $x$ et $y$. Le calcul d'un alignement optimal ou de $Dist$ se ramène au calcul d'un chemin de coût optimal sur le graphe d'édition $G$. Comme $G$ est acyclique, on a besoin d'un seul passage pour le calculer \textblue{(on met un ordre topologique)}. Cependant le calcul de ce graphe et son parcours peuvent être long, on utilise alors la programmation dynamique afin de réduire ces complexités \textblue{(limites)}.
	
	\paragraph{Utilisation de la programmation dynamique afin de calculer ce graphe} On va alors donner un algorithme sous le paradigme de la programmation afin de calculer le graphe et le plus court chemin à partir du sommet origine correspondant à la paire des mots vide.
	
	\noindent\emph{Notation}: On note la manipulation des chaînes de caractères comme suit
	\begin{itemize}
		\item $x_i$ est le préfixe de taille $i+1$ du mot $x$;
		\item $x_{-1}$ est le mot vide \textblue{(correspondant au préfixe de taille $0$)};
		\item $x[i]$ est le i$^{\text{ième}}$ caractère de $x$;
		\item $\Sigma_{u, v}$ est l'ensemble des suites d'opérations permettant de passer de $u$ à $v$ avec $u, v \in \Sigma^*$.
	\end{itemize}
	
	\subparagraph{Sous-structure optimale} Ce problème possède une sous-structure optimale : l'alignement pour les paires des préfixes \textblue{(ce sont les sommets du graphe)}. Si $\sigma$ est une suite optimale d'alignements pour les préfixes $x_n$, $y_m$. Alors, on peut écrire $\sigma = \sigma'.a$ où $a \{Ins, Del, Sub\}$. De plus, $\sigma'$ est optimale. On peut alors écrire l'équation récursive du problème.
	
	\subparagraph{Équation récursive} On pose $T$ un tableau représentant le coût d'un chemin minimal de $(-1, -1)$ à $(i, j)$ dans le graphe d'édition à l'aide d'un tableau de taille $(m+1) \times (n+1)$. On définie $T$ tel que pour tout $i \in \llbracket 0, m-1 \rrbracket$ et pour tout $j \in \llbracket 0, n-1 \rrbracket$, on a $T[i, j] = Dist(x_i, y_j)$. \textblue{\textbf{Remarque}: Les indices de $T$ commence à $-1$ pour encoder le mot vide.}
	
	\textblue{\textbf{Remarque}: On peut montrer que le calcul de $T[i,j]$ ne dépend que de trois valeurs: $T[i-1, j-1]$, $T[i-1, j]$ et $T[i, j-1]$.}
	
	\begin{prop}
		Pour tout $i \in \llbracket 0, m-1 \rrbracket$ et pour tout $j \in \llbracket 0, n-1 \rrbracket$, on a: 
		\begin{enumerate}
			\item $T[-1, -1] = 0$
			\item 
			\begin{displaymath}
			T[i,j] = \min \left \{
			\begin{array}{l}
			T[i-1, j-1] +Sub(x[i], y[j]) \\
			T[i-1, j] + Del(x[i]) \\
			T[i, j-1] + Ins(y[j]) \\
			\end{array}
			\right.
			\end{displaymath}
		\end{enumerate}
	\end{prop}	
	\begin{proof}
		Soit $i \in \llbracket 0, m-1 \rrbracket$ et pour tout $j \in \llbracket 0, n-1 \rrbracket$, on a: 
		\begin{enumerate}
			\item Par définition \textblue{(sur le mot vide, on ne fait aucune opération donc on a un coût nul)}.
			\item \textblue{(Dans le cas du développement, on n'a pas le temps de présenté cette égalité)} Par définition, $T[i, -1] = Dist(x_i, \epsilon)$. La suite minimale qui transforme le mot $x_ix[i]$ en mot vide se termine nécessairement par la suppression de $x[i]$. Le reste de la suite transforme $x_{i-1}$ en mot vide. On a:
			\begin{displaymath}
			\begin{array}{ccl}
			Dist(x_i, \epsilon) & = & \min \{\text{coût}~ \sigma~;~ \sigma \in \Sigma_{x_i, \epsilon}\} \\
			& = & \min \{\text{coût}~ \sigma'.(x[i], \epsilon)~;~ \sigma' \in \Sigma_{x_{i-1}, \epsilon}\} \\
			& = & \min \{\text{coût}~ \sigma'~;~ \sigma' \in \Sigma_{x_{i-1}, \epsilon}\} + Del(x[i]) \\
			& = & Dist(x_{i-1}, \epsilon) + Del(x[i]) \\
			\end{array}
			\end{displaymath}
			\item On raisonne de même.
			\item On distingue les cas selon la dernière opération de la suite minimale $\sigma$.
			\subparagraph{Cas 1} La dernière opération de la suite minimale $\sigma$ est une substitution entre $x[i]$ et $y[j]$. Dans ce cas, on peut écrire $\sigma = \sigma'.(x[i],y[j])$ avec $\sigma'$ qui reste minimale \textblue{(sinon on obtient une contradiction car $\sigma$ n'est pas minimale, \textred{argument de l'optimalité de la sous-structure})}. 
			\begin{displaymath}
			\begin{array}{ccl}
			Dist(x_i, y_j) & = & \min \{\text{coût}~ \sigma~;~ \sigma \in \Sigma_{x_i,  y_j}\} \\
			& = & \min \{\text{coût}~ \sigma'.(x[i], y[j])~;~ \sigma' \in \Sigma_{x_{i-1},  y_{j-1}}\} \\
			& = & \min \{\text{coût}~ \sigma'~;~ \sigma' \in \Sigma_{x_{i-1}, y_{j-1}}\} + Sub(x[i],y[j]) \\
			& = & Dist(x_{i-1}, y_{j-1}) + Sub(x[i],y[j]) \\
			\end{array}
			\end{displaymath}
			
			\subparagraph{Cas 2 et 3} On raisonne de manière analogue si la dernière opération est une insertion ou une suppression.
		\end{enumerate}
	\end{proof}
	
	\textblue{\textbf{Remarque}: Stratification des problèmes du plus petit au plus grand.}
	
	\begin{minipage}{.6\textwidth}
		\begin{algorithm}[H]
			\begin{algorithmic}[1] 
				\Function{\textsc{Calcul-Alignement}}{$x,y$} 
				\State $m \gets longueur(x)$; $n \gets longueur(m)$
				\State $T$ un tableau de taille $m \times n+1$ \Comment{Graphe}
				\State $T[-1, -1] \gets 0$
				\For{$i = 0$ à $m - 1$} \Comment{Calcul la passage de $x$ à $\epsilon$}
				\State $T[i, -1] \gets T[i-1, -1] + Del(x[i])$
				\EndFor
				\For{$j=0$ à $n-1$}
				\State $T[-1, j] \gets T[-1, j-1] + Ins(y[j])$ \Comment{Calcul de $\epsilon$ à $y$}
				\For{$i=0$ à $m-1$} \Comment{Traitement de $x$ à $y$}
				\State $T[i, j] \gets T[i-1, j-1] + Sub(x[i],y[j])$
				\If{$T[i, j] > T[i, j-1] + Ins(y[j])$}
				\State $T[i, j] \gets T[-1, j-1] + Ins(y[j])$
				\EndIf
				\If{$T[i, j] > T[i-1, j] + Del(x[i])$}
				\State $T[i, j] \gets T[i-1, j] + Del(x[i])$
				\EndIf
				\EndFor
				\EndFor
				\State Renvoie $T$
				\EndFunction
			\end{algorithmic}
			\caption{Algorithme calculant un alignement optimal.}
			\label{algo:alignementOptimal}
		\end{algorithm}
	\end{minipage} \hfill
	\begin{minipage}{.35\textwidth}
		\paragraph{Calcul d'une valeur de la solution optimale} L'algorithme~\ref{algo:alignementOptimal} suivant nous permet de calculer une valeur de la solution optimale par méthode ascendante.
		
		\textcolor{white}{a}
		
		L'algorithme est correct par la proposition précédente.
		
		\textcolor{white}{a}
		
		\emph{Complexité}: en temps et en espace $O(mn)$. On peut faire mieux en conservant juste les trois dernières valeurs pour $T$ \textblue{(les seules nécessaires à calculer)}.		
	\end{minipage}
	
	\subparagraph{Construire une solution optimale} On peut vouloir calculer l'alignement optimale en exhibant le chemin optimal dans le graphe. Pour cela, on peut utiliser un tableau $Direction$ qui contient le nœud précédent de notre nœud courant. En remontant ce tableau, on obtient le chemin souhaité. On n'est pas toujours obligé de stocker tout le tableau, on peut le faire en $O(m+n)$. 
	
	
	
	\section{Autours des principes de la programmation dynamique}
	\input{./../Notions/ProgrammationDynamique_Principe.tex}
	
	\section{Autours des distances d'éditions}
	\input{./../Notions/Texte_DistanceEdition.tex}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}