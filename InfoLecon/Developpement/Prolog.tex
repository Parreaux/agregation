\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames,svgnames]{xcolor}
\usepackage{graphicx}%
\usepackage{subcaption}
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}
\usepackage{tikz}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}


\parskip=0\baselineskip %0.5 d'habitude
\usetikzlibrary{automata,positioning}
\usetikzlibrary{calc, shapes, backgrounds}

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\newtheorem*{appli}{Application}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Étude du langage de programmation prologue}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Stern \cite{Stern}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}}  916 (Logique propositionnelle); 918 (Preuve en logique du premier ordre).
		}}
	
	\section{Introduction}
	
	Prolog est un langage logique qui nous permet de résoudre un but à l'aide d'une série de faits et d'une série d'implication. Il a été introduit pour l'intelligence artificielle: il permet de simuler une forme d'intelligence. Pour cela, on se restreint à des formules du type de Horn: le langage Prolog peut alors être vu comme un ensemble de clause de Horn. L'exécution est alors une réfutation (en logique du premier ordre ou de la logique propositionnelle) d'un clause de Horn.
	
	Une clause de Horn est de la forme $x_1 \wedge \dots \wedge x_n \Rightarrow y$ soit $\neg x_1 \vee \dots \vee \neg x_n \vee y$. En Prolog, une clause de Horn s'écrit sous la forme $L \to P_1 \dots P_k$ où $L$ est la tête et $P_1 \dots P_k$ la queue de la règle représente la connaissance à l'aide de $L \to \text{fait}$ et $> Q_1 \dots Q_r$ est le but. Le langage Prolog repose sur la réfutation de type LD de $\{Q_1, \dots, Q_r\}$ à l'aide de la connaissance. De plus, si la résolution est possible, on est capable de donner toutes les solutions. Donnons les étapes de la résolution de la réfutation:
	\begin{itemize}
		\item Effacement des buts: on cherche la règle $L \to P_1 \dots P_k$ telle que $L$ s'identifie à $Q_1$. On cherche donc un unificateur $\sigma$ ce qui nous donne $\{Q_2\sigma, \dots, Q_r\sigma\}$. La correction est assurer par le théorème que nous devons dénombrer, on parle alors de résolution SLR.
		\item Arbre d'effacement et bactracking .On énumère l'ensemble des unificateurs pour $Q_1$ et $L$ et on les explore les uns à la suite des autres (on a une infini à gérer).
		\item L'introduction du cut (qui permet de réduire le backtracking: les choix sont fixes) donne un langage à part entière.
	\end{itemize}
	
	\titlebox{red}{Remarques sur le développement}{
		Le développement donne le fonctionnement de Prolog selon la logique de base. Pour les deux logiques le développement s'articule de la même façon.
		\begin{enumerate}
			\item Définition du cadre.
			\item Étude de la résolution dans le cadre des clauses de Horn.
			\item Définition d'une preuve.
			\item Existence d'une preuve LD pour toute clause de départ.
		\end{enumerate}
		}
	
	\section{Prolog dans le cadre de la logique propositionnelle}
	
	\begin{definition}
		Soit $C$ un ensemble de clauses. On appelle preuve par résolution linéaire une suite $c_0, \dots, c_n$ de clauses telles que $c_0$ soit élément de $C$ et telle que $c_i$, $\forall 1 \leq i \leq n$, soit obtenue comme résolvante de $c_{i-1}$ et d'une clause de $c \cup \{c_0, \dots, c_{i-2}\}$ que l'on notera $l_{i-1}$.
	\end{definition}
	
	\begin{definition}
		On appelle clause de Horn une clause dont au plus un littéral est une variable propositionnelle. Une clause de Horn possédant une variable propositionnelle est appelée une clause définie. Sinon une clause de Horn sans aucune variable propositionnelle est appelée une clause négative ou but.
	\end{definition}
	
	\begin{prop}
		Soit $C$ un ensemble de clauses de Horn et soit $C_0$ le sous-ensemble formé des clauses définies, alors
		\begin{enumerate}
			\item $C_0$ est non contradictoire;
			\item Si $C$ est contradictoire, il existe une clause $g$ négative dans $C$ telle que $C_0 \cup \{g\}$ soit contradictoire.
		\end{enumerate}
	\end{prop}
	\begin{proof}
		\begin{enumerate}
			\item La résolvante de deux clauses définies est aussi définie \textblue{(car on supprime uniquement un littéral positif donc il en reste un littéral positif dans la résolvante)}.
			\item $C$ est contradictoire. Étudions les cas de la résolution:
			\begin{itemize}
				\item La résolution de deux clauses négatives est impossible.
				\item La résolution d'une clause négative et d'une clause positive donne une clause négative \textblue{(on supprime un littéraux positifs: il n'en reste plus)}.
				\item La résolvante de deux clauses définies est aussi définie \textblue{(car on supprime uniquement un littéral positif donc il en reste un littéral positif dans la résolvante)}.
			\end{itemize}
			Donc l'arbre de résolution pour $C$ ne contient que des clauses de Horn \textblue{(étude précédente: on ne pas obtenir plus d'un littéral positif)}.
			
			Si l'arbre de $C$ contient une racine négative:
			\begin{itemize}
				\item il existe une unique branche négative;
				\item tous les autres sommets sont des clauses définies.
			\end{itemize}
			En particulier, c'est vrai pour les arbres de réfutation. On pose alors $g$ l'unique feuille contenant une clause négative.
		\end{enumerate}
	\end{proof}
	
	\begin{definition}
		Une preuve par LD-résolution (L pour linéaire et D pour définie) est une preuve par réfutation dont la prémisse est négative et les entrées sont positives. Les arbres sont donc de la forme (Figure~\ref{fig:LDRefutation}).
	\end{definition}
	
	\begin{figure}
		\begin{minipage}[c]{.46\linewidth}
			\centering
			\begin{tikzpicture}[level/.style={sibling distance=3em, level distance=-3em}]
			\node [draw] (a){$N$}
			child {node [draw] (1){$N$}
				child {node [draw] (2){$N$}}
				child {node [draw=ForestGreen] (3){\textgreen{$D$}}}}
			child {node [draw=ForestGreen] (1){\textgreen{$D$}}};
			\end{tikzpicture}
			\caption{Arbre d'une LD-réfutation.}
			\label{fig:LDRefutation}
		\end{minipage} \hfill
		\begin{minipage}[c]{.46\linewidth}
			\centering
			\begin{tikzpicture}[level/.style={sibling distance=3em, level distance=-3em}]
			\node [draw] (a){$N$}
			child {node [draw] (1){$N$}}
			child {node [draw=ForestGreen] (1){\textgreen{$D$}}
				child {node [draw=ForestGreen] (2){\textgreen{$D$}}}
				child {node [draw=ForestGreen] (3){\textgreen{$D$}}}};
			\end{tikzpicture}
			\caption{Configuration élémentaire.}
			\label{fig:configElementaire}
		\end{minipage}
	\end{figure}
	
	\begin{theo}
		Soit $C$ un ensemble de clause de Horn définies, $g$ une clause négative telle que $C \cup \{g\}$ soit contradictoire, alors il existe une réfutation de $C \cup \{g\}$ par LD-résolution.
	\end{theo}
	\begin{proof}
		On se donne un arbre de réfutation de $C \cup \{g\}$ qui admet une unique branche négative \textblue{(existe par la proposition)}.
		\begin{description}
			\item[Si] cet arbre est un arbre de réfutation LD-résolution alors on a le résultat.
			\item[Sinon] il existe un sommet non terminal tel qu'il est étiqueté par des clauses définies ($D$) et que ces fils également. Prenons le sommet de profondeur minimal vérifiant ces propriétés, on obtient ainsi une configuration élémentaire (Figure~\ref{fig:configElementaire}). Par construction, on remarque qu'il est le fils d'un noeud étiqueté par une clause négative ($N$).
			
			On prend deux variables $p$ et $q$ autorisant les deux réfutations. On peut trouver des clauses négatives $g_1, g_2, g_3$ telles que $\neg q \notin g_1$ et $\neg p \notin g_3$ ce qui nous permet d'étiqueté cette configuration élémentaire (Figure~\ref{fig:ConfigElementairePreuve}).
			
			On remarque qu'on obtient également $g_1 \cup g_2 \cup g_3$ en résolvant $\{\neq q\} \cup g_1$ avec $\{q\} \cup \{\neq p\} \cup g_3$ et leur résolvante avec $\{p\} \cup g_2$ ce qui nous donne l'arbre (Figure~\ref{fig:LDRefutationPreuve}). On a ainsi éliminer un sommet non terminal étiqueté par une clause définie.
		\end{description}
		En itérant ce procédé sur l'arbre, comme le nombre des défaut de linéarité \textblue{(c'est le nombre de sommet non terminal étiqueté par des clauses définies)} diminue à chaque étape, on conclut la preuve.
	\end{proof}
	
	\begin{figure}
		\begin{minipage}[c]{.46\linewidth}
			\centering
			\begin{tikzpicture}[level/.style={sibling distance=10em, level distance=-3em}]
			\node [draw] (a){$g_1 \cup g_2 \cup g_3$}
			child {node [draw] (2){$\{\neg q\} \cup g_1$}}
			child {node [draw=ForestGreen] (1){\textgreen{$\{q\} \cup g_2 \cup g_3$}}
				child {node [draw=ForestGreen] (2){\textgreen{$\{q\} \cup \{\neg p\} \cup g_3$}}}
				child {node [draw=ForestGreen] (3){\textgreen{$\{p\} \cup g_2$}}}};
			\end{tikzpicture}
			\caption{La configuration élémentaire de notre preuve en logique propositionnelle.}
			\label{fig:ConfigElementairePreuve}
		\end{minipage} \hfill
		\begin{minipage}[c]{.46\linewidth}
			\centering
			\begin{tikzpicture}[level/.style={sibling distance=10em, level distance=-3em}]
			\node [draw] (a){$g_1 \cup g_2 \cup g_3$}
			child {node [draw] (1){$\{\neg p\} \cup g_1 \cup g_3$}
				child {node [draw] (2){$\{\neg q\} \cup g_1$}}
				child {node [draw=ForestGreen] (3){\textgreen{$\{q\} \cup \{\neg p\} \cup g_3$}}}}
			child {node [draw=ForestGreen] (2){\textgreen{$\{p\}\cup g_2$}}};
			\end{tikzpicture}
			\caption{La transformation en réfutation LD de notre configuration élémentaire en logique propositionnelle.}
			\label{fig:LDRefutationPreuve}
		\end{minipage}
	\end{figure}
	
	\section{Prolog dans le cadre de la logique du premier ordre}
	
	Dans le cadre de la logique du premier ordre la preuve repose sur les même argument. En effet, la règle de résolution au premier ordre demande une unification qui ne touche pas à la positivité ou à la négativité d'un littéral puisque c'est uniquement une substitution d'une variable (on ne touche pas aux différentes opérations autours de celle-ci). La preuve est rigoureusement la même en ajoutant cette notion d'unification.
	
	\begin{definition}
		Soit $C$ un ensemble de clauses. On appelle preuve par résolution linéaire une suite $c_0, \dots, c_n$ de clauses telles que $c_0$ soit élément de $C$ et telle que $c_i$, $\forall 1 \leq i \leq n$, soit obtenue comme résolvante de $c_{i-1}$ et d'une clause de $c \cup \{c_0, \dots, c_{i-2}\}$ que l'on notera $l_{i-1}$.
	\end{definition}
	
	\begin{definition}
		On appelle clause de Horn une clause dont au plus un littéral est une variable propositionnelle. Une clause de Horn possédant une variable propositionnelle est appelée une clause définie. Sinon une clause de Horn sans aucune variable propositionnelle est appelée une clause négative ou but.
	\end{definition}
	
	\begin{prop}
		Soit $C$ un ensemble de clauses de Horn et soit $C_0$ le sous-ensemble formé des clauses définies, alors
		\begin{enumerate}
			\item $C_0$ est non contradictoire;
			\item Si $C$ est contradictoire, il existe une clause $g$ négative dans $C$ telle que $C_0 \cup \{g\}$ soit contradictoire.
		\end{enumerate}
	\end{prop}
	\begin{proof}
		Comme l'unification est une substitution syntaxique des variables \textblue{(sans toucher à leur signe)}, on peut en déduire que la réfutation n'enlève qu'une seule variable positive dans l'union des deux clauses unifiée.
		\begin{enumerate}
			\item La résolvante de deux clauses définies est aussi définie \textblue{(car on supprime uniquement un littéral positif donc il en reste un littéral positif dans la résolvante)}.
			\item $C$ est contradictoire. Étudions les cas de la résolution:
			\begin{itemize}
				\item La résolution de deux clauses négatives est impossible.
				\item La résolution d'une clause négative et d'une clause positive donne une clause négative \textblue{(on supprime un littéraux positifs: il n'en reste plus)}.
				\item La résolvante de deux clauses définies est aussi définie \textblue{(car on supprime uniquement un littéral positif donc il en reste un littéral positif dans la résolvante)}.
			\end{itemize}
			Donc l'arbre de résolution pour $C$ ne contient que des clauses de Horn \textblue{(étude précédente: on ne pas obtenir plus d'un littéral positif)}.
			
			Si l'arbre de $C$ contient une racine négative:
			\begin{itemize}
				\item il existe une unique branche négative;
				\item tous les autres sommets sont des clauses définies.
			\end{itemize}
			En particulier, c'est vrai pour les arbres de réfutation. On pose alors $g$ l'unique feuille contenant une clause négative.
		\end{enumerate}
	\end{proof}
	
	\begin{definition}
		Une preuve par LD-résolution (L pour linéaire et D pour définie) est une preuve par réfutation dont la prémisse est négative et les entrées sont positives. Les arbres sont donc de la forme (Figure~\ref{fig:LDRefutation}).
	\end{definition}
	
	\begin{theo}
		Soit $C$ un ensemble de clause de Horn définies, $g$ une clause négative telle que $C \cup \{g\}$ soit contradictoire, alors il existe une réfutation de $C \cup \{g\}$ par LD-résolution.
	\end{theo}
	\begin{proof}
		On se donne un arbre de réfutation de $C \cup \{g\}$ qui admet une unique branche négative \textblue{(existe par la proposition)}.
		\begin{description}
			\item[Si] cet arbre est un arbre de réfutation LD-résolution alors on a le résultat.
			\item[Sinon] il existe un sommet non terminal tel qu'il est étiqueté par des clauses définies ($D$) et que ces fils également. Prenons le sommet de profondeur minimal vérifiant ces propriétés, on obtient ainsi une configuration élémentaire (Figure~\ref{fig:configElementaire}). Par construction, on remarque qu'il est le fils d'un noeud étiqueté par une clause négative ($N$).
			
			On prend deux variables $p$ et $q$ autorisant les deux réfutations via leur unifications respectives. On peut trouver des clauses négatives $g_1, g_2, g_3$ telles que $\neg q \notin g_1$ \textblue{(après unification des termes)} et $\neg p \notin g_3$ \textblue{(après unification des termes)} ce qui nous permet d'étiqueté cette configuration élémentaire (Figure~\ref{fig:ConfigElementairePreuveFO}).
			
			On remarque qu'on obtient également $g_1 \cup g_2 \cup g_3$ en résolvant $\{\neq q\} \cup g_1$ avec $\{q\} \cup \{\neq p\} \cup g_3$ et leur résolvante avec $\{p\} \cup g_2$ ce qui nous donne l'arbre (Figure~\ref{fig:LDRefutationPreuveFO}). On a ainsi éliminer un sommet non terminal étiqueté par une clause définie.
		\end{description}
		En itérant ce procédé sur l'arbre, comme le nombre des défaut de linéarité \textblue{(c'est le nombre de sommet non terminal étiqueté par des clauses définies)} diminue à chaque étape, on conclut la preuve.
	\end{proof}
	
	\begin{figure}
		\begin{minipage}[c]{.46\linewidth}
			\centering
			\begin{tikzpicture}[level/.style={sibling distance=10em, level distance=-3em}]
			\node [draw] (a){$\sigma'(g_1 \cup \sigma(g_2) \cup \sigma(g_3))$}
			child {node [draw] (2){$\{\neg q\} \cup g_1$}}
			child {node [draw=ForestGreen] (1){\textgreen{$\sigma(\{q\} \cup g_2 \cup g_3)$}}
				child {node [draw=ForestGreen] (2){\textgreen{$\{q\} \cup \{\neg p\} \cup g_3$}}}
				child {node [draw=ForestGreen] (3){\textgreen{$\{p\} \cup g_2$}}}};
			\end{tikzpicture}
			\caption{La configuration élémentaire de notre preuve en logique du premier ordre.}
			\label{fig:ConfigElementairePreuveFO}
		\end{minipage} \hfill
		\begin{minipage}[c]{.46\linewidth}
			\centering
			\begin{tikzpicture}[level/.style={sibling distance=10em, level distance=-3em}]
			\node [draw] (a){$\sigma'(\sigma(g_1) \cup g_2 \cup \sigma(g_3))$}
			child {node [draw] (1){$\sigma(\{\neg p\} \cup g_1 \cup g_3)$}
				child {node [draw] (2){$\{\neg q\} \cup g_1$}}
				child {node [draw=ForestGreen] (3){\textgreen{$\{q\} \cup \{\neg p\} \cup g_3$}}}}
			child {node [draw=ForestGreen] (2){\textgreen{$\{p\}\cup g_2$}}};
			\end{tikzpicture}
			\caption{La transformation en réfutation LD de notre configuration élémentaire en logique du premier ordre.}
			\label{fig:LDRefutationPreuveFO}
		\end{minipage}
	\end{figure}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}