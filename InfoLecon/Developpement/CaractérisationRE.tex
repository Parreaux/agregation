\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%
\usepackage[dvipsnames]{xcolor}

\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{array}
\usepackage{tikz}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{enumitem}
\usepackage{appendix}
\usepackage{stmaryrd}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\usepackage{pgf,tikz,pgfplots}
\usepackage{mathrsfs}
\usetikzlibrary{arrows}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
		};
		%\draw[color=blue] (box.north west)--(box.north east);
		\node[title] at (box.north) {#2};
		\end{tikzpicture}\bigskip%
} 

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
		
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\newtheorem*{appli}{Application}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Caractérisation des fonctions récursivement énumérables}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Cori--Lascar \cite[p.41]{CoriLascar2} \textblue{(cette référence est difficile: elle ne contient pas l'ensemble du développement au même endroit)}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 912 (Fonctions récursives); 914 (Décidabilité et indécidabilité).
		}}
	
	\section{Introduction}
	
	La classe de décidabilité RE est généralement définie à l'aide des machines de Turing. Cependant, par la thèse de Church et plus précisément l'équivalence entre les machines de Turing et les fonctions $\mu$-récursives, nous pouvons la définir à l'aide de ces dernières. Nous allons énoncer et montrer un résultat permettant de caractériser les fonctions dans la classe de décidabilité RE (et plus généralement la classe en elle-même) à l'aide des fonctions primitives récursives et des fonctions $\mu$-récursives.
	
	Nous énonçons ce résultat avec la définition via les machines de Turing d'un langage dans RE. Pour prouver certaines implications, nous utiliserons librement l'équivalence entre les machines de Turing et les fonctions $\mu$-récursives (que nous admettons). Pour plus de détail, voir le développement correspondant. Il faut donc faire attention de mentionner cette équivalence dans le plan avant le théorème afin de pouvoir utiliser librement sa preuve. 
	
	\titlebox{red}{\textred{\textbf{Remarques sur le développement}}}{
		Ce développement présente une difficulté autours de la référence mais la technicité de celui-ci reste abordable. Cependant, il faut faire attention car la première implication contient un développement (équivalence entre les fonctions $\mu$-récursives et les machines de Turing) qu'il nous faut donc admettre dans le cadre de celui-ci.
	}
	
	\section{Caractérisation des fonctions récursivement énumérables}
	
	\begin{theo}
		Soit $A \subseteq \N$. Les assertions suivantes sont équivalentes.
		\begin{enumerate}
			\item $A \in RE$;
			\item $A = \pi_{2}^{2}(B)$ où $B \subseteq \N^{2}$ est primitif récursif;
			\item $A = \emptyset$ ou $A$ est l'image d'une fonction primitive récursive;
			\item $A$ est l'image d'une fonction $\mu$-récursive.			
		\end{enumerate}
	\end{theo}
	\begin{proof}
		On montre la suite d'implication suivante: $1 \Rightarrow 2 \Rightarrow 3 \Rightarrow 4 \Rightarrow 1$.
		
		\paragraph{Montrons $1 \Rightarrow 2$} Soit $\mathcal{M}$ une machine de Turing qui accepte $A$ \textblue{($A \in RE$ donc par définition de $RE$, il existe bien une machine de Turing qui accepte $A$)}. Sans perte de généralité, on suppose que les états finaux de la machine de Turing boucle à l'infini sur eux-même \textblue{(par la thèse de Church on obtient le résultat de l'équivalence de l'expressivité des machines de Turing dont la frontière est la frontière de la classe $RE$)}.
		
		On pose le prédicat $P(t, m) = "\mathcal{M} \text{ s'arrête en } t \text{ étapes sur } n"$. Ce prédicat est primitif récursif car il s'écrit:
		\begin{displaymath}
		\mathbb{1}_{F} (\textsc{config} (\textsc{init}(n), t))
		\end{displaymath}
		\textbrown{(par l'équivalence entre les machines de Turing et les fonctions $\mu$-récursives, on sait que ces fonctions sont bien primitive récursives)}. \textblue{(Si on ne fait pas l'hypothèse supplémentaire sur la machine de Turing, le prédicat reste récursif car il peut s'écrire comme 
			\begin{displaymath}
			\mathbb{1}_{F} (\textsc{config} (\textsc{init}(n), \mu i \leq t, \mathbb{1}_{F} (\textsc{config} (\textsc{init}(n), t))))
			\end{displaymath}
		où on a juste ajouté une minimisation non-bornée.)}
		
		Notons $B = \{(t, n) \in \N^2 | P(t, n)\}$ \textblue{(le prédicat $P$ peut être vu comme l'indicatrice de $B$)}, donc $B$ primitif récursif \textblue{(car son indicatrice l'est)}. De plus,
		\begin{displaymath}
		\begin{array}{ccll}
		n \in A & ssi & \exists t \in \N, P(t,n) & \textblue{A \in RE \text{ et } \mathcal{M} \text{ accepte } A}\\
		& ssi & n \in \pi_{2}^2(B) & \textblue{\Leftrightarrow \exists t \in \N, (t,n) \in B}\\ 
		\end{array}
		\end{displaymath}
		Donc $A \in \pi_2^2(B)$ où $B$ est primitif récursif.
		
		\paragraph{Montrons $2 \Rightarrow 3$} Supposons $A \neq \emptyset$ \textblue{(sinon, on a terminé)}. Il existe donc $m \in A$. Soit $\begin{array}{cccl}
		\varphi : & \N & \to & \N \\
		& k & \mapsto & \left(t_k, n_k\right)
		\end{array}$ une énumération primitive récursive de $\N^2$. Une telle énumération existe: on peut prendre, par exemple, $\varphi : k \mapsto \left(\textsc{diag}(k) - \textsc{ecart}(k), \textsc{ecart}(k)\right)$ où 
		\begin{displaymath}
		\begin{array}{cc}
		\textsc{diag}: & k \mapsto \left(\mu i \leq (k+1). \left(\frac{i(i+1)}{2} > k\right)\right) -1 \\
		\textsc{ecart}: & k \mapsto k - \frac{\textsc{diag}(k)\left(\textsc{diag}(k)+1\right)}{2} \\
		\end{array}
		\end{displaymath}
		sont primitives récursives. Donc $\varphi$ est bien primitive récursive comme composition de fonctions primitives récursives. \textblue{(De plus, $\varphi$ est une bijection de réciproque $(t, n) \mapsto \frac{(t+n)(t+n+1)}{2} +n$.)}
		
		Alors en notant $P$ un prédicat récursif tel que $B = \{(t, n) | P(t, n)\}$ \textblue{(existe par hypothèse)}. On cherche $f$ tel que
		\begin{displaymath}
		f(n_k) = \left\{
		\begin{array}{cl}
		n_k & \text{si } (t_k, n_k) \in B \\
		m & \text{sinon}
		\end{array}
		\right.
		\end{displaymath}
		On pose alors $f = (P \circ \varphi) \times \left(\pi_2^2 \circ \varphi\right) + \left(1 - (P \circ \varphi) \right) \times m$ qui est bien une fonction primitive récursive et $A = Im ~f$ \textblue{(car si $n_k \in A$ alors il existe $(t_k, n_k) \in B$ et sinon pour tout $t_k$, $(t_k, n_k) \notin B$)}.
		
		
		\paragraph{Montrons $3 \Rightarrow 4$} Soit $A \subseteq \N$, on distingue deux cas.
		\begin{itemize}
			\item Si $A = \emptyset$, alors $A = im \left(\mu i\left(\mathbb{O}(i)\right)\right)$ où $\mathbb{O}(.)$ est le prédicat nul d'arité 1. Donc $A$ est l'image d'une fonction $\mu$-récursive.
			\item Sinon, $A$ est l'image d'une fonction primitive récursive, donc en particulier, elle  est l'image d'une fonction $\mu$-récursive.
		\end{itemize}
		
		\paragraph{Montrons $4 \Rightarrow 1$} Si $A$ est l'image d'une fonction primitive récursive, alors elle est calculable par une machine de Turing \textblue{(encore l'équivalence entre les machines de Turing et les fonctions $\mu$-récursives)} définie par l'algorithme \ref{algo:MTaccA} qui prend en paramètre un entier $n$ et renvoie \textsc{oui} si $n \in A$.
		\begin{algorithm}
			\begin{algorithmic}[1]
				\State $k \gets 0$ 
				\While{$f(k) \neq n$}
				\State $k \gets k +1$
				\EndWhile
				\State Accepte
			\end{algorithmic}
			\caption{Définition de la machine de Turing déterministe acceptant $A$}
			\label{algo:MTaccA}
		\end{algorithm}
		Donc $A \in RE$.
	\end{proof}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}

\end{document}