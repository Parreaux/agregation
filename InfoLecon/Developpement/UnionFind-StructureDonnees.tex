\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{array}
\usepackage{tikz}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}
\usepackage{multirow}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude
\usetikzlibrary{automata,positioning,shadows}
\usetikzlibrary{calc, shapes, backgrounds,arrows}
\usetikzlibrary{chains, decorations.pathmorphing}
\usetikzlibrary{positioning,decorations.pathreplacing}

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
		};
		%\draw[color=blue] (box.north west)--(box.north east);
		\node[title] at (box.north) {#2};
		\end{tikzpicture}\bigskip%
} 

\begin{document}
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\sloppy
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem{lemme}{Lemme}
	\newtheorem*{appli}{Application}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Union Find : des structures de données}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Cormen \cite[p.519]{Cormen}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 901 (Structures de données).
			
			\textblue{\emph{Leçons où on peut l'évoquer:}} 921 (Recherche); 925 (Graphes); 926 (Analyse en complexité).
		}}
	
	\section{Introduction}
	
	On souhaite une structure de donnée qui permet de faire une partition dans l'ensemble des données et de connaître rapidement à quelle partie de la partition se trouve un élément donnée. Les structures de données Union Find en sont capable. À partir du type abstrait contenant les opérations \textsf{create}, \textsf{union} et \textsf{find}, nous pouvons construire deux implémentations distinctes afin de faire deux structures de données Union Find. Une première consiste à représenter la partition de l'ensemble de départ à l'aide d'une liste chaînée. La seconde quant-à elle représente la partition à l'aide d'une forêt d'arbre.
	
	Dans ce développement nous allons étudier le type abstrait muni de ces deux implémentations possibles. Puis pour chacune d'entre elles nous donnerons quelques heuristiques permettant d'améliorer leur efficacité \textblue{(nous ne montrons aucun résultats sur la complexité)}. Enfin, nous donnerons une application de cette structure de données : l'algorithme de Kruskal permettant de calculer un arbre couvrant minimal.
	
	\titlebox{red}{\textred{\textbf{Remarques sur le développement}}}{
		Ce développement présente une structure de données: il faut donc appuyer sur son implémentation "réelle". Dans ce développement, il n'y a pas de preuves à proprement parler, cependant, il faut garder à l'esprits les preuves de complexités qui n'ont rien de triviales.
		\begin{enumerate}
			\item Présentation des enjeux de cette structure de données.
			\item Présentation d'un premier type concret: les listes chaînées.
			\item Présentation d'un deuxième type concret: la forêt d'arbres.
			\item Présentation de l'algorithme de Kruskal.
		\end{enumerate}
	}
	
	\section{Présentation de la structure de donnée Union Find}
	
	\noindent\emph{Objectif}: Regrouper $n$ éléments dans une collection de d'ensembles disjoints sur lesquels on souhaite trouver l'unique ensemble disjoint contenant un élément (\textsf{find}) ou réunir deux ensembles (\textsf{union}).
	
	\noindent\emph{Opérations}:
	\begin{itemize}
		\item \textsf{create}$(x)$ : créer une nouvelle partie dont $x$ est le seul membre \textblue{unique si $x$ déjà dans la structure : échec};
		\item \textsf{find}$(x)$ : donne un pointeur vers l'unique élément de $x$;
		\item \textsf{union}$(x, y)$ : associe les deux parties de $x$ et de $y$ \textblue{(avec un choix de nouveau représentant)}
	\end{itemize}
	
	\noindent\emph{Hypothèse}: Cette structure est construite en parallèle des données (elle ne contient que des copies). Les données et leurs copies sont relier par un pointeur dans les deux sens. \textblue{Donc, trouver un élément dans la structure de données a une complexité constante.}
	
	\paragraph{Type concret 1 : les listes chaînées} Le premier type concret qui va nous donner la première structure de données est un ensemble de listes chaînées. La liste possède deux pointeurs : un qui pointe sur le premier élément et un autre qui pointe sur le dernier. De plus, tous les éléments de la listes ont un pointeur qui va jusqu'à la structure \textblue{(faire un dessin au tableau, figure~\ref{fig:liste})}.
	
	\begin{figure}
	\begin{minipage}{.46\textwidth}
			\begin{tikzpicture}[auto, thick, >=latex]
	\draw
	% Drawing the blocks of first filter :
	node at (0,-0.5) [rectangle,draw=none, name=tete] {tête} 
	node at (0,-1.5) [rectangle,draw=none] (queue) {queue}
	node at (1,-0.5) [rectangle,draw=black, name=tete2] {} 
	node at (1,-1.5) [rectangle,draw=black] (queue2) {}
	node at (2.75,-.5) [rectangle,draw=none] (1val1) {}
	node at (4.1,-.5) [rectangle,draw=none] (2val1) {}
	node at (3.45,-.5) [rectangle,draw=none] (val1) {val}
	node at (5.35,-.5) [rectangle,draw=none] (1val2) {}
	node at (6.75,-.5) [rectangle,draw=none] (2val2) {}
	node at (6,-.5) [rectangle,draw=none] (val2) {val}
	node at (1,0.25) [rectangle,draw=none, name=tete3] {}
	node at (1,0.5) [rectangle,draw=none] (tete4) {} ;
	% Joining blocks. 
	% Commands \draw with options like [->] must be written individually
	\draw[->](tete2) -- node {} (1val1);
	\draw[->](queue2) -| node {} (2val2);
	\draw[->](2val1) -- node {} (1val2);
	\draw[->,color=blue](1val1) |- node {} (tete3);
	\draw[->,color=blue](1val2) |- node {} (tete4);
	% Boxing and labelling noise shapers
	\draw [color=gray,thick](-0.7,1) rectangle (1.5,-2);
	\draw [color=gray,thick](3,0) -- (3,-1);
	\draw [color=gray,thick](3.9,0) -- (3.9,-1);
	\draw [color=gray,thick](2.5,0) rectangle (4.5,-1);
	\draw [color=gray,thick](5.5,0) -- (5.5,-1);
	\draw [color=gray,thick](6.5,0) -- (6.5,-1);
	\draw [color=gray,thick](5,0) rectangle (7,-1);
	\end{tikzpicture}
	\caption{Liste chaînée représentant une partie de la partition dans union find.}
	\label{fig:liste}
	\end{minipage} \hfill
	\begin{minipage}{.46\textwidth}
		\centering
		\begin{tikzpicture}[level/.style={sibling distance=8em/#1, level distance=4em}]
		\node [rectangle,draw] (a){val}
		child {node [rectangle,draw] (2){val}
			child {node [rectangle,draw=black] (2b) {val}
				edge from parent [draw=red]}
			child {node [rectangle,draw=black] (2c) {val}
				edge from parent [draw=black]}
			edge from parent [draw=red]}
		child {node [rectangle,draw] (b) {val}
			child {node [rectangle,draw] (bb) {val}}};
		\end{tikzpicture}
		\caption{Forêt d'arbre (contenant un seul arbre) représentant la partition d'union find (contenant une seule partie).}
		\label{fig:arbre}
	\end{minipage}
	\end{figure}
	
	\noindent\emph{Complexités pour cette structure}:
	\begin{tabular}{ccl}
		\textsf{create} & $O(1)$ & \textblue{(créer un nombre fixe de pointeur)}\\
		\textsf{find} & $O(1)$ & \textblue{(suivre (montrer) les pointeurs bleus de la figure~\ref{fig:liste})} \\
		\textsf{union} & $O(l)$ & où $l$ est la longueur de la liste \textblue{(recopier toute la liste)}\\
	\end{tabular}
	
	\noindent\emph{Heuristique}: Union pondérée : on ajoute un attribut \textsf{longueur} dans la liste et on colle la plus petite liste à la plus grande liste \textblue{(moins de pointeurs à recopier)}.
	
	\paragraph{Type concret 2 : la forêt d'arbre} Une deuxième implémentation de ce type abstrait est une forêt d'arbre un peu particulière. En effet, dans cette forêt, les arc des arbres sont orientés vers la racine qui porte une boucle \textblue{(faire un dessin au tableau, figure~\ref{fig:arbre})}.
	
	\noindent\emph{Complexités pour cette structure}:
	
	\begin{tabular}{ccl}
		\textsf{create} & $O(1)$ & \textblue{(créer un nombre fixe de pointeur)}\\
		\textsf{find} & $O(l)$ &  où $l$ est la longueur de la liste \textblue{(remonter le chemin de découverte en rouge figure~\ref{fig:arbre})} \\
		\textsf{union} & $O(1)$ & \textblue{(modifier un pointeur)}\\
	\end{tabular}
	Cette implémentation n'est pas plus efficace que les listes chaînées \textblue{(on échange les difficultés de \textsf{union} et celle de \textsf{find})}.
	
	\noindent\emph{Heuristiques}: Les heuristiques améliorent grandement la complexité.
	\begin{itemize}
		\item Union par rang : on fait pointer la racine contenant le moins de nœuds vers celle en contenant le plus \textblue{(de profondeur la plus importante)}. On gère un rang qui majore la hauteur d'un nœud.
		\item Compression de chemin : on fait pointer les nœuds du chemin de découverte vers la racine \textblue{(dessin)}.
	\end{itemize}
	
	\paragraph{Application à l'algorithme de Kruskal} La structure Union Find est importante dans l'algorithme de Kruskal (elle garantie la correction et influe sur sa complexité). \textblue{L'algorithme de Kruskal permet de calculer un arbre couvrant minimal pour un graphe connexe pondéré}
	
	\section{Étude de la complexité de cette structure}
	\input{./../Notions/Structure_UnionFind.tex}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}

	
\end{document}