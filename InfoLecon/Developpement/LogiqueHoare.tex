\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{array}
\usepackage{tikz}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}


\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	
	\newtheorem*{definition}{Définition}
	\newtheorem*{notation}{Notation}
	\newtheorem*{theorem}{Théorème}
	
	\title{Preuve de la correction de la fonction \textsc{Factorielle} à l'aide de la logique d'Hoare}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Winskel \cite[p. =93, section 6.6]{Winskel}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçon où on présente le développement:}}  927 (Terminaison et correction).
			
			\textblue{\emph{Leçon où on peut l'évoquer:}}  930 (Sémantique).
		}}
	
	
	\section{Quelques rappels sur la logique de Hoare}
	
	Cette section permet d'introduire le développement et quelques remarques autours de la logique de Hoare. Elle n'est pas à vocation d'être évoquée dans le développement. 
	
	Le système de preuve de la logique de Hoare utilise les règles de déduction suivantes \cite[p. 89]{Winskel}:
	
	\begin{tabular}{lc}
		Règle du \textsc{skip} &  \\
		& $\{A\} \textsc{skip} \{A\}$\\
		Règle de l'affectation & \\
		& $\{B[a/X]\} X:=a \{B\}$\\
		Règle de la séquence & \\
		& $\frac{\{A\}c_0\{C\} ~ \{C\}c_1\{B\}}{\{A\}c_0; c_1 \{C\}}$\\
		Règle de la conditionnelle & \\
		& $\frac{\{A \wedge b \} c_0\{B\} ~ \{A \wedge \neg b \}c_1\{B\}}{\{A\} \textsc{if } b \textsc{ then } c_0 \textsc{ else } c_1 \{B\}}$ \\
		Règle de la boucle \textsc{While} & \\
		& $\frac{\{A \wedge b \}c\{A\}}{\{A\}\textsc{while }b\textsc{ do }c \{A\wedge \neg b \}}$\\
		Règle de la conséquence & \\
		& $\frac{\models (A \Rightarrow A') ~ \{A'\}c\{B'\} ~ \models (B' \Rightarrow B)}{\{A\}c\{B\}}$\\
	\end{tabular}
	
	Ce développement met en évidence deux méthodes généralement utilisée dans le cadre de preuves avec la logique de Hoare: on applique les règles de droite à gauche (on part de la conclusion et on évalue le programme dans le sens inverse) et on définit un invariant qui peut paraître un peut sur défini : il contient l'invariant (la variable X contient bien un résultat partiel) et la conjonction de l'entrée et de la sortie de la boucle \textsc{While} (X est positif (rentre dans la boucle) ou nul (sortie de la boucle)).
	
	\section{Preuve de la correction de la fonction \textsc{Factorielle}}

	On souhaite étudier la correction du programme \textsc{Factorielle} (Algorithme \ref{algo:fact}) implémenté selon les principe de la programmation impérative.
	\begin{algorithm}
		\begin{algorithmic}
			%\Require $n \geq 0$
			\Function{Factorielle}{$n$}
				\State  $X := n$; 
				\State $Y := 1$;
				\While {$X > 0$}
					\State $Y := Y \times X$;
					\State $X := X - 1$
				\EndWhile
			\EndFunction
		\end{algorithmic}
		\caption{La fonction \textsc{Factorielle} dans un langage impératif simple.}
		\label{algo:fact}
	\end{algorithm}
	
	 
	\begin{theorem}
		La fonction \textsc{Factorielle} est correct, i.e.
		\begin{displaymath}
		\{n \geq 0\} \textsc{Factorielle}(n) \{Y := n!\}
		\end{displaymath}
	\end{theorem}
	
	\begin{proof}
		Il est clair, par les deux affectations en $X$ et en $Y$, que  $\{n \geq 0\} X := n; Y := 1 \{n \geq 0 \wedge X = n; Y = 1\}$. 
		
		Il nous faut alors montrer que $\{n \geq 0 \wedge X = n \wedge Y = 1\} w \{n \geq 0\}$ où $w$ est l'instruction \textsc{While}.
		
		Commençons par montrer que $I = \{Y \times X! = n! \wedge X \geq 0\}$ est un invariant pour la boucle \textsc{While}. Il nous faut donc montrer que $\{I \wedge X > 0\} Y:= Y \times X; X:=X-1 \{I\}$. Pour cela, on applique un principe récurrent lorsqu'on souhaite prouver la correction d'un programme par la logique de Hoare: on part de la conclusion pour remonter jusqu'aux hypothèses. En effet, les règles de déductions sont plus naturelles dans cet ordre.
		\begin{enumerate}
			\item En appliquant la règle d'assignation à $X$, on obtient:
			\begin{displaymath}
			\{I[(X-1)/X]\} X := X - 1 \{I\}
			\end{displaymath}
			où $I[(X-1)/X] = \{Y \times (X - 1)! = n! \wedge (X-1) \geq 0 \}$.
			\item En appliquant la règle d'assignation à $Y$, on obtient:
			\begin{displaymath}
			\{Y \times X \times (X-1)! = n! \wedge (X-1)>0 \} Y := Y \times X \{I[(X-1)/X]\}
			\end{displaymath}
			où $\{Y \times X \times (X-1)! = n! \wedge (X-1) \geq 0 \} = \{Y \times X! = n! \wedge (X-1) \geq 0 \}$
			\item En appliquant la règle de séquence, on obtient:
			\begin{displaymath}
			\{Y \times X! = n! \wedge (X-1)\geq 0 \} Y := Y \times X; X := X - 1 \{I\}
			\end{displaymath}
			\item On souhaite appliquer la règle de la conséquence pour conclure que $I$ est bien un invariant. 
			\begin{enumerate}
				\item Montrons que $I \wedge X>0 \Rightarrow \{Y \times X! = n! \wedge (X-1)\geq 0 \}$.
				
				\begin{tabular}{lclr}
					$I \wedge X>0$ & $\Rightarrow$ & $Y \times X! = n! \wedge X \geq 0 \wedge X>0$ & (par réécriture de $I$) \\
					& $\Rightarrow$ & $Y \times X! = n! \wedge X \geq 1$ & (par $X >0$) \\
					& $\Rightarrow$ & $Y \times X! = n! \wedge (X-1) \geq 0$ & (par $X \geq 1$) \\
				\end{tabular}
				
				\item Par la règle de la conséquence, on a
				\begin{displaymath}
				\{I \wedge X > 0 \} Y := Y \times X; X := X - 1 \{I\}
				\end{displaymath}
			\end{enumerate}
		\end{enumerate}
		On en déduit que $I$ est un invariant de boucle.
		
		Maintenant nous pouvons montrer que $\{n \geq 0 \wedge X = n \wedge Y = 1\} w \{n \geq 0\}$.
		\begin{enumerate}
			\item Par la règle de la boucle $\textsc{While}$, on obtient que 
			\begin{displaymath}
			\{I\} w \{I \wedge X \ngtr 0 \}
			\end{displaymath}
			
			\item Pour conclure, nous devons vérifier que l'invariant de boucle $I$ est bien vérifié quand on rentre dans la boucle. De plus, la sortie de boucle doit impliqué notre post-condition. Nous allons appliquer une dernière fois la règle de la conséquence.
			\begin{enumerate}
				\item Montrons maintenant que $I$ est bien vérifié lorsque nous rentrons dans la boucle la première fois, i.e. $\{n \geq 0 \wedge X = n \wedge Y = 1\} \Rightarrow I$.
			
				En posant dans $I$, $X = n$ et $Y = 1$, on obtient le résultat souhaité, i.e. $\{n \geq 0 \wedge X = n \wedge Y = 1\}$.
			
				\item Montrons que l'invariant de boucle et de la sortie de boucle implique la postcondition de \textsc{Factorielle}, i.e. $I \wedge X \ngtr 0 \Rightarrow Y = n!$.
			
				\begin{tabular}{lclr}
					$I \wedge X \ngtr 0$ & $\Rightarrow$ & $Y \times X! = n! \wedge X \geq 0 \wedge X \ngtr 0$ & (par réécriture de $I$) \\
					& $\Rightarrow$ & $Y \times X! = n! \wedge X = 0$ & (par $X = 0$ et $X \ngtr 0$) \\
					& $\Rightarrow$ & $Y \times 0! = n!$ & (par $X = 0$) \\
					& $\Rightarrow$ & $Y = n!$ & (par $0! = 1$) \\
				\end{tabular}
				
				\item Par la règle de la conséquence, on obtient:
				\begin{displaymath}
				\{n \geq 0 \wedge X = n \wedge Y = 1\} w \{Y := n!\}
				\end{displaymath}
			\end{enumerate}
		\end{enumerate}
		 
		Par la règle de la séquence, on en déduit que 
		\begin{displaymath}
		\{n \geq 0\} \textsc{Factorielle}(n) \{Y := n!\}
		\end{displaymath}
	\end{proof}
	
	\begin{footnotesize}
		\bibliographystyle{alpha}
		\bibliography{./../../Livre}
	\end{footnotesize}
		%\printbibliography

	
\end{document}