\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}
\usepackage{multirow}

\usepackage{multicol}
\usepackage{caption}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude
\usetikzlibrary{automata,positioning,shadows}
\usetikzlibrary{calc, shapes, backgrounds,arrows}
\usetikzlibrary{chains, decorations.pathmorphing}
\usetikzlibrary{positioning,decorations.pathreplacing}

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem{lemme}{Lemme}
	\newtheorem*{appli}{Application}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Indécidabilité du problème de validité en logique du premier ordre}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Logic in computer science \cite[p.131]{HuthRyan}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 914 (Décidabilité et indécidabilité); 924 (Modèles et théorie FO).
		}}
		
	\section{Introduction}
	La logique du premier ordre est une logique expressive. En effet, savoir si une formule logique est valide (au sens où pour tout modèle elle est satisfiable) est indécidable. Cette indécidabilité donne de nombreuses applications: la complexité descriptive (à la place des machines de Turing, on considère des fragment de la logique du premier ordre), les théorème d'indécidabilité en base de données, ...
	
	Ce résultat nous permet d'utiliser la notion de réduction calculable (méthode pour prouver l'indécidabilité) à un problème indécidable bien connu \textsc{Post}. On pratique également le jeu de la syntaxe et de la sémantique dans la logique du premier ordre.
	
	\titlebox{red}{\textred{\textbf{Remarques sur le développement}}}{
		Ce développement met en œuvre une réduction pour prouver l'indécidabilité d'un problème: il faut la faire proprement.
		\begin{enumerate}
			\item Description de l'instance de Post.
			\item Description de la traduction.
			\item Preuve de l'indécidabilité (pour le sens direct de l'implication aller vite pour se laisser du temps pour la réciproque qui est plus intéressante).
		\end{enumerate}
	}
	
	\section{Le problème de la validité en logique du premier ordre}
	
	\begin{theo}
		Le problème \textsc{Valide} est indécidable.
	\end{theo}
	\begin{proof}
		On réduit le problème \textsc{Post} au problème \textsc{Valide}.
		
		\begin{minipage}{0.36\textwidth}
			Par indécidabilité du problème \textsc{Post}, la réduction donne l'indécidabilité du problème \textsc{Valide} \textblue{(se fait par un raisonnement par l'absurde)}.
		\end{minipage}\hfill
		\begin{minipage}{.56\textwidth}		
			\begin{figure}[H]
				\begin{tikzpicture}[auto, thick, >=latex]
				\draw
				% Drawing the blocks of first filter :
				node at (-1,0) [rectangle,draw=none, name=input1] {} 
				node at (1.5,0) [rectangle,draw=black] (red) {Réduction $tr$}
				node at (4,0) [rectangle,draw=none] (tr) {$(\varphi \to \psi)$}
				node at (5.75,0) [rectangle,draw=black] (B) {\textsc{Valide}}
				node at (7.25,0) [rectangle,draw=none] (fin) {};
				% Joining blocks.
				% Commands \draw with options like [->] must be written individually
				\draw[->](input1) -- node[near start]{$w$}(red);
				\draw[-](red) -- node {} (tr);
				\draw[->](tr) -- node {} (B);
				\draw[->](B) -- node {} (fin);
				% Boxing and labelling noise shapers
				\draw [color=gray,thick](-0.25,-1) rectangle (6.65,1.5);
				\node at (-0.25,1.25) [above=5mm, right=0mm] {\textsc{Post}};
				\end{tikzpicture}
				\caption{Schéma du principe de réduction du problème \textsc{Post} au problème \textsc{Valide}.}
				\label{fig:principeRed}	
			\end{figure}
		\end{minipage}
		
		\paragraph{Description de l'instance de \textsc{Post} que nous considérons}
		On se place sur l'alphabet $\Sigma = \{a, b\}$. Soit $w$ une instance de \textsc{Post}, autrement dit, un ensemble de $N$ tuiles de la forme 
		\begin{tabular}{|c|}
			\hline
			$u_i$ \\
			\hline
			$v_i$ \\
			\hline
		\end{tabular}
		pour $i \in \llbracket 1, N \rrbracket$ avec $u_i$ et $v_i$ des mots sur $\Sigma$ tel que $u_i$ et $v_i$ ne soit pas tous deux le mot vide ($\epsilon$) \textblue{(cette tuile ne sert à rien et cela nous permet de ne pas la confondre avec le cas où aucune tuile n'est encore posée)}. 
		
		On rappelle que le problème \textsc{Post} est de savoir s'il existe ou non une suite de tuiles telle qu'en les concaténant, les mots du haut et du bas (bornés par la concaténation des tuiles) sont les deux mêmes.
		
		\paragraph{Description de la traduction} On pose la traduction suivante : $tr(w) = (\varphi \rightarrow \psi)$ avec
		\begin{itemize}
			\item $\varphi = \underbrace{p(\epsilon, \epsilon)}_{\textblue{\text{constante}}} \wedge \bigwedge_{i=1}^N (\underbrace{\forall x \forall y}_{\textblue{\text{pour tout mot}}}, (\underbrace{p(x,y) \rightarrow p(u_i(x), v_i(y))}_{\textblue{\text{Si } x = y \text{ alors } u_ix = v_ix \text{ (après concaténation des tuiles)}}}))$
			\item $\psi = \exists x (p(a(x), a(x)) \vee p(b(x), b(x)))$
		\end{itemize}
		
		\noindent Posons quelques notations:
		\begin{itemize}
			\item $m = m_1 \dots m_n$ un mot sera noté $m(.) = m_1 \dots m_n(.) = m_1( \dots m_n(.) \dots)$ \textblue{(justification de $u_i(x)$}
			\item \begin{tabular}{|c|}
				\hline
				$x$ \\
				\hline
				$y$ \\
				\hline
			\end{tabular}
			une succession de tuiles qui donne le mot $x$ en haut et $y$ en bas.
		\end{itemize}
		
		
		\paragraph{Preuve de l'indécidabilité} Pour montrer l'indécidabilité, il nous faut montrer deux propriétés : $tr$ est une fonction calculable (lemme~\ref{lem:trCalculable}) et $tr$ est correcte (lemme~\ref{lem:trCorrecte}).
		
		\begin{lemme}
			La fonction $tr$ est calculable.
			\label{lem:trCalculable}
		\end{lemme}
		\begin{proof}
			La fonction $tr$ s'exécute en temps fini car pour toute instance $w$ du problème \textsc{Post}, il n'y a qu'un nombre fini de tuiles. La formule se calcul alors en temps fini.
		\end{proof}
		
		\begin{lemme}
			Pour toute instance $w$ de \textsc{Post}, $tr(w)$ est une formule valide si et seulement si $w$ est ne instance positive.
			\label{lem:trCorrecte}
		\end{lemme}
		\begin{proof}
			\begin{description}
				\item[$\Rightarrow$] La formule $\varphi \rightarrow \psi$ est valide, donc pour tout modèle $\mathcal{M}$, $\mathcal{M} \models (\varphi \rightarrow \psi)$.
				
				\subparagraph{Choisir le modèle} On choisit le modèle $\mathcal{M}$ suivant \textblue{(possible car la formule est conséquence sémantique de tous modèle)}.
				
				\begin{tabular}{clccl}
					$\bullet$ & $D_{\mathcal{M}} = \Sigma^*$ & & $\bullet$ & $e^{\mathcal{M}}$ correspond à $e_{\Sigma^*}$ \textblue{(constante : pas de tuiles)}\\
					
					$\bullet$ & $a^{\mathcal{M}}(.) :
					\begin{array}{ccl}
					\Sigma^* & \to & \Sigma^* \\
					x & \mapsto & xa \\
					\end{array}$
					& & $\bullet$ & $b^{\mathcal{M}}(.) :
					\begin{array}{ccl}
					\Sigma^* & \to & \Sigma^* \\
					x & \mapsto & xb \\
					\end{array}$ \\
					
					$\bullet$ & \multicolumn{4}{l}{$p^{\mathcal{M}} = \{(x, y) ~|~ \begin{array}{|c|}
						\hline
						x \\
						\hline
						y \\
						\hline
						\end{array} \} \cup \{\epsilon, \epsilon\}
						$ } \\
				\end{tabular}
				
				\subparagraph{Montrer que $\mathcal{M} \models \varphi$} On a $\mathcal{M} \models p(e, e)$ \textblue{(par hypothèse, correspond au cas où on n'a pas mis de tuiles sur la table)}. Montrons que $\mathcal{M} \models \forall x \forall y, (p(x,y) \rightarrow p(u_i(x), v_i(y)))$. Comme $\forall u, v \in \Sigma^*$, si $\mathcal{M}\left[\begin{array}{c}
				x := u \\y : = v
				\end{array}\right] \models p(x, y)$, alors en interprétant $p$, $\begin{array}{|c|}
				\hline
				u \\
				\hline
				v \\
				\hline
				\end{array}$ existe. Pour tout $i \in \llbracket 1, N \rrbracket$, par concaténation de 
				$\begin{array}{|c|}
				\hline
				u \\
				\hline
				v \\
				\hline
				\end{array}$ 
				et de 
				$\begin{array}{|c|}
				\hline
				u_i \\
				\hline
				v_i \\
				\hline
				\end{array}$,
				alors il existe
				$\begin{array}{|c|}
				\hline
				uu_i \\
				\hline
				vv_i \\
				\hline
				\end{array}$. Donc $\forall i \in \llbracket 1, N \rrbracket$, $\mathcal{M} \models \forall x \forall y, (p(x,y) \rightarrow p(u_i(x), v_i(y)))$ et $\mathcal{M} \models \varphi$.
				
				\subparagraph{Montrer que $w$ est une instance positive du problème \textsc{Post}} Comme $\mathcal{M} \models (\varphi \rightarrow \psi)$ \textblue{(la formule est valide)} et $\mathcal{M} \models \varphi$, $\mathcal{M} \models \psi$. Sans perte de généralité, on suppose que $\mathcal{M} \models \exists x, p(a(x), a(x))$ \textblue{(le cas $b$ est similaire)}. Donc, il existe $\alpha \in \Sigma^*$ tel que $\mathcal{M}[x:=\alpha] \models p(a(x), a(x))$. Par la définition de $p$, la tuile
				$\begin{array}{|c|}
				\hline
				a\alpha \\
				\hline
				a\alpha \\
				\hline
				\end{array}$ 
				existe. L'instance $w$ est donc positive \textblue{(car il existe une suite de tuiles telle que le mot de haut et le mot du bas sont le même)}.
				
				\item[$\Leftarrow$] On suppose que $w$ soit une instance positive du problème \textsc{Post}. Il existe donc $m \geq 0$ tel que $\exists i_1, \dots, i_m \in \llbracket 1, N \rrbracket$ avec $u_{i_1} \dots u_{i_m} = v_{i_1} \dots v_{i_m}$. Soit $\mathcal{M}$ un modèle tel que $\mathcal{M} \models \varphi$. Montrons que $\mathcal{M} \models \psi$.
				
				Montrons que $\mathcal{M}\left[\begin{array}{c}
				x:=u_{i_1} \dots u_{i_k}\\ y:= v_{i_1} \dots v_{i_k}\\
				\end{array}\right] p(x,y)$ en raisonnant par récurrence sur $k \in \N$.
				\begin{description}
					\item[Initialisation] Pour $k = 0$, on a $\mathcal{M} \models p(e,e)$ car $\mathcal{M} \models \varphi$.
					
					\item[Hérédité] Soit $k \in \N$ tel que $\mathcal{M}\left[\begin{array}{c}
					x:=u_{i_1} \dots u_{i_k}\\ y:= v_{i_1} \dots v_{i_k}\\
					\end{array}\right] \models p(x,y)$ et montrons le pour $k+1$.  
					
					On sait que $\forall i \in \llbracket 1, N \rrbracket$, $\mathcal{M} \models \forall x \forall y, (p(x,y) \rightarrow p(u_i(x), v_i(y)))$. 
					
					En particulier, pour $i = i_{k+1}$, on a $\mathcal{M} \models \forall x \forall y, (p(x,y) \rightarrow p(u_{i_{k+1}}(x), v_{i_{k+1}}(y)))$. 
					
					Donc, $\mathcal{M}\left[\begin{array}{c}
					x:=u_{i_1} \dots u_{i_k}\\ y:= v_{i_1} \dots v_{i_k}\\
					\end{array}\right] \models (p(x,y) \rightarrow p(u_{i_{k+1}}(x), v_{i_{k+1}}(y)))$. 
					
					Ainsi, $\mathcal{M}\left[\begin{array}{c}
					x:=u_{i_1} \dots u_{i_{k+1}}\\ y:= v_{i_1} \dots v_{i_{k+1}}\\
					\end{array}\right] \models p(x,y)$.
					
					\item[Conclusion] On a  $\forall k \in \llbracket 1, m \rrbracket$, $\mathcal{M}\left[\begin{array}{c}
					x:=u_{i_1} \dots u_{i_k}\\ y:= v_{i_1} \dots v_{i_k}\\
					\end{array}\right] \models p(x,y)$.
				\end{description}
				
				Sans perte de généralité, on suppose que $u_{i_1} \dots u_{i_m}$ finit par un $a$. On écrit $u_{i_1} \dots u_{i_m} = \tilde{u}a$ et $v_{i_1} \dots v_{i_m} = \tilde{v}a$. On veut prouver que $\mathcal{M} \models \psi$ et on a 
				$\mathcal{M}\left[\begin{array}{c}
				x:=u_{i_1} \dots u_{i_m}\\ y:= v_{i_1} \dots v_{i_m}\\
				\end{array}\right] \models p(x,y)$ 
				donc
				$\mathcal{M}\left[\begin{array}{c}
				x:=\tilde{u}a\\ y:= \tilde{v}a\\
				\end{array}\right] \models p(x,y)$
				d'où 
				$\mathcal{M}\left[\begin{array}{c}
					x:=\tilde{u}a\\ y:= \tilde{u}a\\
				\end{array}\right] \models p(x,y)$.
				De fait, il existe $\tilde{u}$ tel que $\mathcal{M}[x:=a] \models p(a(x), a(x))$. Donc $\mathcal{M} \models \psi$. Ainsi $\mathcal{M} \models (\varphi \rightarrow \psi)$. Ce qui prouve que $(\varphi \rightarrow \psi)$ est valide.
			\end{description}
		\end{proof}
		On a donc montre l'indécidabilité du problème \textsc{Valide}.
	\end{proof}
	
	\section{Validité et satisfiabilité}
	
	\subsection{Modèles en logique du premier ordre}
	\input{./../Notions/LogiqueFO_models.tex}
	
	\subsection{Sémantique de la logique du premier ordre}
	\input{./../Notions/LogiqueFO_semantique.tex}
	
	\subsection{Les problèmes de décision sur les logiques}
	\input{./../Notions/Logique_pbDecisions.tex}
	
	\section{Technique de la réduction}
	\input{./../Notions/Indecidabilite_reduction.tex}
	
	\section{Indécidabilité sur les machines de Turing}
	\input{./../Notions/MT_pbDecisions.tex}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}