\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{array}
\usepackage{tikz}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
		};
		%\draw[color=blue] (box.north west)--(box.north east);
		\node[title] at (box.north) {#2};
		\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\sloppy
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\newtheorem*{appli}{Application}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Tri par tas}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Cormen \cite[p.139]{Cormen}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçon où on présente le développement:}} 901 (Structure de données); 903 (Algorithmes de tri).
		}}
	
	\section{Introduction}
	
	Le tri par tas est un des tris optimaux par comparaison. Le tri par tas utilise une structure de données pour trié en place et de manière optimale. C'est un exemple de la conception d'un algorithme à l'aide (et autours) d'une structure de données. 
	
	\titlebox{red}{\textred{\textbf{Remarques sur le développement}}}{
		Ce développement présente un tri qui utilise une structure de donnée pour être optimal.
		\begin{enumerate}
			\item Définition de cette structure de données.
			\item Étude de la fonction \textsf{Entasser} (principe, algorithme, correction, complexité).
			\item Étude de la fonction \textsf{Construire} (principe, algorithme, correction, complexité).
			\item Étude de la fonction \textsf{Tri} (principe, algorithme, correction, complexité).
		\end{enumerate}
	}
	
	\section{Structure de tas binaire}
	
	\textblue{Nous allons étudier la structure de données tas binaire dont les opérations permettent de trier en place un tableau de manière optimale.}
	
	\begin{definition}
		Un tas (binaire, max) est un arbre binaire quasi-complet entassé à gauche dont l'étiquette de chaque nœud est supérieure à celle de ces fils.
	\end{definition}
	
	\noindent\emph{Représentation}: Un tas peut être vu comme un arbre binaire ou comme un tableau muni de sa taille.
	
	\noindent\textblue{\emph{Comment passer d'un arbre à un tableau?}
	\begin{tabular}{ccc}
		\textsf{Parent}($i$) $ = \lfloor \frac{i}{2} \rfloor$ &
		\textsf{Gauche}($i$) $ = 2i$ &
		\textsf{Droit}($i$) $ = 2i + 1$ \\
	\end{tabular}}
	
	La structure de donnée comporte trois opérations:
	
	\begin{tabular}{cc}
		\textsf{Entasser} & permet de conserver la structure de tas \\
		\textsf{Construire} & permet de construire un tas \\
		\textsf{Tri} & permet de faire un tri d'un tableau en place \\
	\end{tabular}
	
	\paragraph{La fonction \textsf{Entasser}} Son objectif est de conserver la propriété de tas (Algorithme~\ref{algo:tas_entasser}). Si cette propriété est violée, on fait alors descendre la clé violant la propriété dans le tas. Pour cela, on fait l'hypothèse que le tas est presque correct (seul notre nœud courant peut violer la propriété) et on rétabli la structure de données. Dans ce cas, la valeur maximale du tas enracinée en notre nœud courant est soit celui-ci soit dans un de ces deux fils. 
	
	\noindent\emph{Hypothèse}: \textsf{Gauche}($i$) et \textsf{Droit}($i$) sont des tas max, seul le nœud $i$ peut violer la propriété de tas.
	
	\begin{algorithm}
		\begin{algorithmic}[1] 
			\Function{\textsf{Entasser}}{$A, i$}\Comment{$A$ est un tas; $i$ est l'indice du nœud à entasser}
			\State $l \gets \textsf{Gauche}(i)$
			\State $r \gets \textsf{Droit}(i)$
			\State $max \gets i$
			\If{$l \leq A.taille$ et $A[l] > A[i]$} \Comment{On détermine si $max < l$}
			\State $max \gets l$
			\EndIf
			\If{$r \leq A.taille$ et $A[r] > A[i]$} \Comment{On détermine si $max < r$}
			\State $max \gets r$
			\EndIf
			\If{$max \neq i$} \Comment{Si $i$ n'est pas le max, on échange et on recommence}
			\State Échanger $A[i]$ et $A[max]$
			\State \textsf{Entasser}($A, max$)
			\EndIf
			\EndFunction
		\end{algorithmic}
		\caption{Fonction permettant d'entasser un tas : établir les propriétés du dit tas.}
		\label{algo:tas_entasser}
	\end{algorithm}
	
	\begin{theo}[Correction]
		L'algorithme d'entassement du tas (Algorithme~\ref{algo:tas_entasser}) est correcte.
	\end{theo}
	\begin{proof}
		A chaque itération, on a trois cas possibles:
		\begin{description}
			\item[Cas 1 : $max = i$] Renvoie un tas enraciné en $i$
			\item[Cas 2 : $max = l$] L'arbre binaire enraciné en $r$ est un tas \textblue{(par hypothèse comme on ne l'a pas touché)}. De plus, le nouveau fils gauche $l'$ est un tas \textblue{(par induction)}. Comme $max \geq r$ \textblue{(calcul du maximum)} et $max \geq l \geq l'$ \textblue{(calcul du maximum et propriété du tas enraciné en $l$ avant l'exécution de la fonction)}.
			\item[Cas 3 : $max = r$] Analogue
		\end{description}
		Donc l'arbre binaire enraciné en $max$ est un tas.
	\end{proof}
	
	\begin{theo}[Complexité]
		L'algorithme d'entassement du tas (Algorithme~\ref{algo:tas_entasser}) s'exécute en $O(\log n)$.
	\end{theo}
	\begin{proof}
		On effectue une unique descente dans l'arbre binaire du tas (cet arbre est équilibré). Donc, on a une complexité en $O(h)$ où $h$ est la hauteur, soit une complexité en $O(\log n)$.
		
		\textblue{Une autre manière de le voir : on applique le master theorem à la relation de récurrence suivante: 
		\begin{displaymath}
			T(n) \leq \underbrace{\frac{2n}{3}}_{\text{taille de l'arbre dans le pire cas car remplit à moitié}} + \underbrace{\Theta(1)}_{\text{complexité des modifs effectuées}}
		\end{displaymath}}
	\end{proof}
	
	\paragraph{La fonction \textsf{Construire}} Son objectif est de convertir un tableau en tas: de passer de la représentation d'un tableau simple en structure de données plus complexe: un tas. On va alors appliquer la fonction \textsf{Entasser} à tous les éléments du tableau dans l'ordre décroissant. Cette décroissance nous permet de garantir la correction de notre algorithme.
	
	\begin{algorithm}
		\begin{algorithmic}[1] 
			\Function{\textsf{Construire}}{$A$}\Comment{$A$ est un tableau que l'on veut transformer en tas}
			\For{$i = \lfloor\frac{A.longueur}{2}\rfloor$ à $1$} 
			\State $\textsf{Entasser}(A, i)$ \Comment{on commence par le bas les hypothèses sont ok)}
			\EndFor
			\EndFunction
		\end{algorithmic}
		\caption{Fonction permettant de construire un tas : on transforme un tableau en un tas.}
		\label{algo:tas_construire}
	\end{algorithm}
	
	\begin{theo}[Correction]
		L'algorithme de construction d'un tas (Algorithme~\ref{algo:tas_construire}) est correcte.
	\end{theo}
	\begin{proof}
		Invariant de la boucle \textsc{for}: "A chaque itération $i$, chaque nœuds $i+1, \dots, n$ est la racine d'un tas max." On le prouve par récurrence sur le nombre d'itération $i \in \{1, \dots, n \}$.
		\begin{description}
			\item[Initialisation] ok car $\lfloor\frac{n}{2}\rfloor + 1, \dots, n$ sont des feuilles d'un tas.
			\item[Hérédité] Les enfants du nœud $i$ ont des numéros supérieurs à $i$. Donc, tous les deux sont racines d'un tas max \textblue{(par l'invariant)}. L'hypothèse d'application de la fonction \textsf{Entasser} (nécessaire à sa correction) est vérifier, donc l'arbre binaire enraciné en $i$ se transforme en tas max \textblue{(sans altérer les propriétés sur les nœuds suivants)}.
		\end{description}
		Donc l'invariant de la boucle est vérifiée. \textblue{L'argument clé de cette démonstration est la décroissance de $i$ dans la boucle car elle permet d'établir l'invariant de boucle sur les nœuds suivants.}
	\end{proof}
	
	Lorsque l'on souhaite calculer la complexité de la fonction \textsf{Construire}, une majoration grossière nous donne $O(n \log n)$. En effet, par la boucle \textsc{For}, nous appliquons $n$ fois la fonction \textsf{Entasser} qui s'exécute en $O(\log n)$. Cette complexité est suffisante pour établir la complexité du tri. Cependant, en remarquant que la complexité de la fonction \textsf{Entasser} dépend de la hauteur du tas, on obtient une complexité en $O(n)$.
	
	\begin{theo}[Complexité]
		L'algorithme de construction d'un tas (Algorithme~\ref{algo:tas_construire}) s'exécute en $O(n)$.
	\end{theo}
	\begin{proof}
		On remarque que la complexité de la fonction \textsf{Entasser} dépend de la hauteur du tas. La complexité de la fonction \textsf{Construire} s'exprime donc comme suit:
		\begin{displaymath}
		\sum_{h = 0}^{\lfloor\log n rfloor} \lceil\frac{n}{2^{h+1}}\rceil O(h) = O(n\sum_{h = 0}^{\lfloor\log n rfloor} \frac{h}{2^h})
		\end{displaymath}
		Comme $\sum_{h=0}^{\infty} \frac{h}{2^h} = \frac{1/2}{(1-1/2)^2} = 2$,
		\begin{displaymath}
		O(n\sum_{h = 0}^{\lfloor\log n rfloor} \frac{h}{2^h}) = O(n \sum_{h=0}^{\infty} \frac{h}{2^h}) = O(n)
		\end{displaymath}
	\end{proof}
	
	\paragraph{La fonction \textsf{Tri}} Son objectif est de trier un tableau. Pour cela, on construit un tas max à partir du tableau, puis pour chaque racine on la sort du tas et on recommence.
	
	\begin{algorithm}
		\begin{algorithmic}[1] 
			\Function{\textsf{Tri-Par-Tas}}{$A$}\Comment{$A$ est un tableau que l'on souhaite trier}
			\State \textsf{Construire}($A$)
			\For{$i = A.longueur$ à $2$} 
			\State Échanger $A[1]$ et $A[i]$ \Comment{On sort la racine du tas}
			\State $A.taille \gets A.taille - 1$
			\State \textsf{Entasser}($A, i$) \Comment{On recommence}
			\EndFor
			\EndFunction
		\end{algorithmic}
		\caption{Fonction permettant de trier un tableau à l'aide d'un tas.}
		\label{algo:tas_tri}
	\end{algorithm}
	
	\begin{theo}[Correction]
		L'algorithme de tri par tas (Algorithme~\ref{algo:tas_tri}) est correcte.
	\end{theo}
	\begin{proof}
		Invariant de la boucle \textsc{for}: "A chaque itération $i$, chaque sous tableau $A[A.taille , :]$ est trié et $A[:, A.taille]$ est un tas." On le prouve en appliquant la correction des deux autres.
	\end{proof}
	
	\begin{theo}[Complexité]
		L'algorithme de tri par tas (Algorithme~\ref{algo:tas_tri}) s'exécute en $O(n\log n)$.
	\end{theo}
	\begin{proof}
		On exécute l'opération \textsf{Construire} un fois en $O(n)$. Puis on exécute $n$ fois (pour la boucle), l'opération \textsf{Entasser}. On a donc une complexité en $O(n \log n)$
	\end{proof}
	
	\section{Application à la file de priorité}
	\input{./../Notions/FilesPriorite.tex}
	
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}