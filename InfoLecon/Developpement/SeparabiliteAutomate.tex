\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%
%\usepackage[]{geometry} 
%\geometry{hmargin=1.5cm,vmargin=3cm}

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}
\usepackage{multirow}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}


\parskip=0\baselineskip %0.5 d'habitude
\usetikzlibrary{automata,positioning,shadows}
\usetikzlibrary{calc, shapes, backgrounds,arrows}
\usetikzlibrary{chains, decorations.pathmorphing}
\usetikzlibrary{positioning,decorations.pathreplacing}


\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\newtheorem*{appli}{Application}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Problème de séparabilité par un automate}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Le langage des machines \cite[p.555]{FloydBiegel}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 909 (Automates finis); 915 (Classes complexité); 928 (NP-complétude).
		}}

	\section{Introduction}
	
	Le problème de séparabilité par un automate intervient en théorie de l'apprentissage par exemple. Ce problème consiste à deviner un langage à partir d'informations partielles. On va alors chercher à construire un automate qui respecte les informations que nous avons. Lorsqu'on suppose que le langage que nous devons deviner est rationnel, nous pouvons travailler avec des automates finis. Le problème devient alors un problème de construction d'automate qui accepte une liste finie de mots et en rejette une autre. L'existence d'un tel automate connaissant l'ensemble des mots acceptant et rejetant en NP-complet. Nous allons prouver sa NP-dureté en le réduisant à SAT (qui est bien NP-complet par le théorème de Cook).
	
	Outre la théorie de l'apprentissage, ce problème permet de classifier deux langages rationnel à l'aide d'un automate fini déterministe.
	
	\titlebox{red}{\textred{\textbf{Remarques sur le développement}}}{
		Le développement présente une preuve du NP-dureté. Il est donc important de bien écrire la réduction et de dire qu'elle est polynomiale.
		\begin{itemize}
			\item Problème NP (attention au codage des entiers).
			\item Problème NP-dur.
			\begin{enumerate}
				\item Présentation des problèmes et de la réduction (faire le dessin et l'expliquer).
				\item Donner la traduction (en donner l'intuition sans la montrer).
				\item Preuve de la correction de la traduction
				\begin{description}
					\item[$\Leftarrow$] Construction de la valuation en fonction de l'automate.
					\item[$\Rightarrow$] Construction de l'automate à l'inverse.
				\end{description}
			\end{enumerate}
		\end{itemize}
	} 
	
	\section{Séparabilité par un automate}
	
	\emph{Contexte du problème} \textblue{(théorie de l'apprentissage)}: Un individu A essaye de deviner un langage $L$. Un individu B possède un ensemble fini de mots positifs $S$ (qui appartiennent à $L$) et un ensemble fini de mots négatifs $T$ (qui n'appartiennent pas à $L$). A doit trouver la règle la plus simple possible afin d'être compatible avec les exemples de B et en espérant qu'elle soit persistante. 
	
	On se place dans ce contexte et on suppose que $L$ est rationnel. L'individu A doit alors trouver un automate déterministe fini qui accepte tous les mots de $S$ et rejette ceux de $T$ \textblue{(pour les autres, on ne précise pas le comportement de l'automate)}.
	
	\begin{definition}
		On définit le problème \textsc{Séparation par un automate} sur les langages rationnels.
		
		\noindent\begin{tabular}{rl}
			Problème & \textsc{Séparation par un automate} (\textsc{SPA}) \\
			\textbf{entrée}: & $S$ et $T$ deux ensembles finis de mots et $k \in \N$ \textblue{($k$ est représenté en unaire)} \\
			\textbf{sortie}: & Oui s'il existe $\mathcal{A}$déterministe à $k$ états  tel que $S \subseteq \mathcal{L}(\mathcal{A})$ et $T \cap \mathcal{L}(\mathcal{A}) = \emptyset$; non sinon \\
		\end{tabular}
	\end{definition}
	
	\begin{theo}
		Le problème de \textsc{Séparation par un automate} est NP-complet.
	\end{theo}
	\begin{proof}
		Montrons que le problème de \textsc{Séparation par un automate} (\textsc{SPA}) est NP-complet.
		
		\paragraph{Le problème de \textsc{Séparation par un automate} est dans NP} On devine un automate déterministe à $k$ états et on test l'appartenance de $S$ à son langage et la non appartenance de $T$. Cette construction et ses tests se fond en temps polynomiale \textblue{(chaque état à un nombre borné de transition et le temps d'exécution des mots de $S$ et de $T$ sont de leur taille, $k$ est représenté en unaire sinon on a un facteur exponentielle)}.
		
		\paragraph{Le problème de \textsc{Séparation par un automate} est dans NP-dur} On effectue une réduction au problème \textsc{SAT} (Figure~\ref{fig:principeRed} \textblue{(à faire et à commenter)}). \textblue{(Ce problème est bien NP-complet par le théorème de Cook)}. Soit $\varphi$ une formule booléenne à $m$ clauses $C_0, \dots, C_{m-1}$ et $v$ variables $x_0, \dots, x_{v-1}$. On note $x_0, \dots, x_{v-1}, \neg x_0, \dots, \neg x_{v-1}$ les variables propositionnelles de la formule $\varphi$ et leur négation. On se base sur l'alphabet $\Sigma = \{a, b\}$.
		
		\begin{figure}
			\begin{minipage}{0.46\textwidth}
				\begin{tikzpicture}[auto, thick, >=latex]
				\draw
				% Drawing the blocks of first filter :
				node at (-1,0) [rectangle,draw=none, name=input1] {} 
				node at (1.5,0) [rectangle,draw=black] (red) {Réduction $tr$}
				node at (4,0) [rectangle,draw=none] (tr) {$S, T, k$}
				node at (5.75,0) [rectangle,draw=black] (B) {\textsc{SPA}}
				node at (7,0) [rectangle,draw=none] (fin) {};
				% Joining blocks. 
				% Commands \draw with options like [->] must be written individually
				\draw[->](input1) -- node[near start]{$\varphi$}(red);
				\draw[-](red) -- node {} (tr);
				\draw[->](tr) -- node {} (B);
				\draw[->](B) -- node {} (fin);
				% Boxing and labelling noise shapers
				\draw [color=gray,thick](-0.25,-1) rectangle (6.5,1.5);
				\node at (-0.25,1.25) [above=5mm, right=0mm] {\textsc{SAT}};
				\end{tikzpicture}
				\caption{Schéma du principe de réduction du problème \textsc{SPA} au problème \textsc{SAT}.}
				\label{fig:principeRed}				
			\end{minipage}\hfill
			\begin{minipage}{.46\textwidth}		
				\centering
				\begin{tikzpicture}[font=\sffamily]
				% Setup the style for the states
				\tikzset{node style/.style={state, 
						minimum width=1cm,
						line width=.25mm,
						fill=white,circle}}
				
				% Draw the states
				\node[node style,initial,accepting] at (0,0) (0) {$C_0$};
				\node[node style] at (2,0) (1) {$C_1$};
				\node[node style,draw=none] at (4,0) (1bis) {$\dots$};
				\node[node style] at (4,-2) (2) {$x_0$};
				\node[node style,draw=none] at (2,-2) (2bis) {$\dots$};
				\node[node style] at (0,-2) (3) {$\neg x_{v-1}$};
				
				% Connect the states with arrows
				\draw[every loop,auto=left,line width=.25mm,
				>=latex]
				(0) edge [] node {$a$} (1)
				(1) edge [dashed] node {$a$} (1bis)
				(1bis) edge [dashed] node {$a$} (2)
				(2) edge [dashed] node {$a$} (2bis)
				(2bis) edge [dashed] node {$a$} (3)
				(3) edge [] node {$a$} (0);
				\end{tikzpicture}
				\caption{Un automate $\mathcal{A}$ acceptant les mots de $S_1$ et rejetant ceux de $T_1$.}
				\label{fig_T1S1}
			\end{minipage}
		\end{figure}
		
		\noindent\emph{Réduction}: construire $k$, $S$ et $T$ de tel sorte que $\mathcal{A}$ soit contraint afin qu'il décrive une valuation pour $\varphi$.
		\begin{itemize}
			\item $k = m + 2v$ \textblue{(le nombre de clauses et de termes possibles)}
			\item $S = S_1 \cup S_5$
			\begin{itemize}[label=$\bullet$]
				\item $S_1 = \{\epsilon, a^k\}$ \textblue{(donne un unique état initial et un unique final dans un état à $k$ états)} 
				\item $S_5 = \{a^ibb ~|~ 0 \leq i < m\}$ \textblue{(pour toutes les clauses, il existe un littéral lié via $b$ à $C_0$)}
			\end{itemize}
			\item $T = T_1 \cup T_2 \cup T_3 \cup T_4$
			\begin{itemize}[label=$\bullet$]
				\item $T_1= \{a^i, 0 < i< k\}$ \textblue{(donne un unique état initial et un unique final dans un état à $k$ états)}
				\item $T_2 = \{a^iba^j ~|~ 0 \leq i < m; 0 \leq j < k\} \setminus \{a^iba^{2v-j} ~|~ l_j \text{ litéral de } C_i \}$ \textblue{(les clauses sont liés à un de ces littéral via $b$)}
				\item $T_3 = \{a^{m+j}ba^h ~|~ 0 \leq j < 2v; 0 < h < k; h \neq v\}$ \textblue{(les littéraux sont liés à $C_0$ ou à $\neg x_0$ via $b$)}
				\item $T_4 = \{a^{m+j}ba^{m+j+v}b ~|~ 0 \leq j < v\}$ \textblue{(les variables et leur négation ne sont pas liés en même temps à $C_0$ via $b$)}
			\end{itemize}
		\end{itemize}
		Cette réduction se calcul en temps polynomial puisque $k$ \textblue{(évident: addition)}, $S$ \textblue{(on construit un ensemble contenant $n + 2$ mots donc linéaire en la taille de la formule)} et $T$ \textblue{(on construit un ensemble contenant $(n +2v)k$ mots donc polynomial en la taille de la formule)} peuvent être construit en temps polynomial.
		
		\begin{figure}
			\begin{minipage}{.46\textwidth}
				\centering
				\begin{tikzpicture}[font=\sffamily]
				% Setup the style for the states
				\tikzset{node style/.style={state, 
						minimum width=1cm,
						line width=.25mm,
						fill=white,circle}}
				
				% Draw the states
				\node[node style,initial,accepting] at (0,0) (0) {$C_0$};
				\node[node style] at (2,0) (1) {$C_i$};
				\node[node style] at (4,0) (2) {$C_j$};
				
				% Connect the states with arrows
				\draw[every loop,auto=left,line width=.25mm,
				>=latex]
				(0) edge [] node {$a^i$} (1)
				(1) edge [] node {$b$} (2)
				(2) edge [bend left] node {$a^{k-j}$} (0);
				\end{tikzpicture}
				\caption{Un morceau de l'automate $\mathcal{A}$ sous l'hypothèse de l'absurde pour $T_2$.}
				\label{fig:T2_abs}
			\end{minipage} \hfill		
			\begin{minipage}{.46\textwidth}
				\centering
				\begin{tikzpicture}[font=\sffamily]
				% Setup the style for the states
				\tikzset{node style/.style={state, 
						minimum width=1cm,
						line width=.25mm,
						fill=white,circle}}
				
				% Draw the states
				\node[node style,initial,accepting] at (0,0) (0) {$C_0$};
				\node[node style] at (2,0) (1) {$C_i$};
				\node[node style] at (4, 0) (2) {$l_j$};
				
				% Connect the states with arrows
				\draw[every loop,auto=left,line width=.25mm,
				>=latex]
				(0) edge [] node {$a^i$} (1)
				(1) edge [] node {$b$} (2)
				(2) edge [bend left] node {$b$} (0);
				\end{tikzpicture}
				\caption{Un morceau de l'automate $\mathcal{A}$ acceptant $S_5$.}
				\label{fig:S5}
			\end{minipage}
		\end{figure}
		
		\begin{prop}
			$\varphi$ est satisfiable si et seulement s'il existe $\mathcal{A}$ déterministe à $k$ états tel que $\mathcal{A}$ accepte $S$ et rejette $T$.
		\end{prop}
		\begin{proof}
			\begin{description}
				\item[$\Leftarrow$] Supposons qu'il existe $\mathcal{A}$ déterministe à $k$ états tel que $\mathcal{A}$ accepte $S$ et rejette $T$. 
				\begin{itemize}[label=$\to$]
					\item Comme $\mathcal{A}$ accepte $S_1$ et rejette $T_1$, il possède donc $k$ états avec un unique état initial qui est aussi l'unique état final \textblue{(il possède bien $k$ états car tout états $q_i$, $q_j$ sont distingués par $a^{j-i}$, pour tout $1 \leq i <j \leq k$)}. Pour simplifier la notation des états, on les étiquette comme suit : $C_0, \dots, C_{m-1}, x_{0}, \dots, x_{v - 1}, \neg x_{0}, \dots, \neg x_{v - 1}$ (Figure~\ref{fig_T1S1}).
					
					\item  Comme $\mathcal{A}$ rejette $T_2$, les clauses sont liés à un littéral. Raisonnons par l'absurde et supposons qu'il existe une transition issue d'un état $C_i$ vers un état $C_j$ (Figure~\ref{fig:T2_abs}). Soit $u = a^iba^{k-j}$. Alors $u \in T_2 \subseteq T$ est accepté par $\mathcal{A}$, contradiction. Le mot $a^iba^{2v - j}$ est accepté par $\mathcal{A}$ si et seulement si $l_j$ est un littéral de $C_i$.
					
					\item Comme $\mathcal{A}$ rejette $T_3$, les littéraux sont liés à $C_0$ ou à $\neg x_0$. Raisonnons par l'absurde et supposons qu'il existe une transition issue d'un état $l_j$ vers un état $q$ autre que $C_0$ ou $\neq x_0$ (Figure~\ref{fig:T3_abs}). On distingue alors deux cas selon la nature de l'état $q$.
					\begin{itemize}
						\item Si $q = C_i$ pour un certain $i \neq 0$, alors la lecture du mot $u = a^{m+j}b$ atteint l'état $C_i$ \textblue{car la lecture du mot $a^{m+j}$, par la nature cyclique de $\mathcal{A}$ et les $m$ clauses, nous emmène en $l_j$}. Donc le mot $a^{m+j}ba^{m+2v-i} \in T_3 \subseteq T$ est accepté par $\mathcal{A}$\textblue{(car il y a $m+2v = k$ état et $C_i$ est à la position $i$)}. Absurde.
						\item Si $q = l_i$ pour un certain $i \neq v$, alors le mot  $a^{m+j}ba^{2v-i} \in T_3 \subseteq T$ est accepté par $\mathcal{A}$ \textblue{(car il y a $2v$ littéraux et $l_i$ est à la position $i$)}. Absurde
					\end{itemize}
					On obtient une contradiction dans les deux cas, d'où le résultat.
					
					\item Comme $\mathcal{A}$ rejette $T_4$, les variables et leurs négations ne sont pas liés à $C_0$ en même temps. Raisonnons par l'absurde et supposons qu'il existe une variable $x_j$ qui soit liée à $C_0$ en même temps que sa négation (Figure~\ref{fig:T4_abs}).  Supposons qu'il existe des transitions étiquetées entre $l_j$ et $C_0$ et entre $l_{j+v}$ et $C_0$. La lecture des mots $a^{m+j}b$ et $a^{m+v+j}b$ arrivent en $C_0$. La concaténation des deux mots $a^{m+j}ba^{m+v+j}b \in T_4 \subseteq T$ est alors acceptée. On obtient une contradiction.	
					
					\item Comme $\mathcal{A}$ accepte $S_5$ et rejette $T$, pour toute clause, il existe un littéral lié à $C_0$. Soit $u =a^ibb$ avec $0 \leq i < m$. Comme $u S_5 \subseteq S$, il est accepté par $\mathcal{A}$, la lecture du mot $a^ibb$ depuis $C_0$ nous emmène en $C_0$. Comme $i < m$, il existe un état $C_i$ tel que la lecture du mot $a^i$ depuis $C_0$ nous emmène en $C_i$, puis que la lecture du mot $bb$ de $C_i$ nous emmène en $C_0$. De l'état $C_i$, on va dans un état $l_j$ qui est soit véridique soit $\neg x_0$. Comme après la lecture du dernier $b$ on est en $C_0$ cet état $l_j$ est véridique. 
				\end{itemize}
				On construit une valuation $\nu$. On pose
				\begin{displaymath}
				\nu(x_i) = \left\{ 
				\begin{array}{ll}
				\textsf{vrai} & \text{si la transition } \delta(x_i, b) = C_0 \text{ est dans } \mathcal{A} \\
				\textsf{faux} & \text{sinon} \\ 
				\end{array}
				\right.
				\end{displaymath}
				On affecte la valeur \textsf{vrai} aux littéraux liés à $C_0$. Par la structure de $\mathcal{A}$, les variables et leur négation ne sont pas liés à $C_0$ en même temps donc leurs valuation $\nu$ est bien définie. De plus, dans $\mathcal{A}$ chacune des clauses contient un littéral lié à $C_0$. Toutes les clauses sont évaluées à \textsf{vrai} par $\nu$. Donc il existe une valuation pour $\varphi$.
				
				\item[$\Rightarrow$] Supposons que $\varphi$ est satisfiable par une valuation $\nu$. On note $t(C_i)$ le premier littéral tel que $\nu(t(C_i)) = \textsf{vrai}$.
				
				On construit $\mathcal{A}$.
				\begin{itemize}
					\item $Q = \{C_0, \dots, C_{m-1}, x_0, \dots, x_{v-1}, \neg x_0, \dots, \neg x_{v-1} \}$
					\item $i = C_0$ et $F = \{C_0\}$
					\item $\delta : \left\{ 
					\begin{array}{ll}
					(q_i, a, q_{(i+1) \mod k}) & \forall 0 \leq i < k \\
					(C_i, b,  t(C_i)) & \forall 0 \leq i < m \\
					(l_i, b, C_0) & \forall 0 \leq j < 2v \text{ si } \nu(l_j) = \textsf{vrai}\\
					(l_i, b, \neg x_0) & \forall 0 \leq j < 2v \text{ si } \nu(l_j) = \textsf{faux}\\
					\end{array}\right.$
				\end{itemize}
				
				Montrons que $\mathcal{A}$ accepte $S$.
				\begin{itemize}[label=$\to$]
					\item $\mathcal{A}$ accepte $S_1$: structure de l'automate.
					\item $\mathcal{A}$ accepte $S_5$: soit $u = a^ibb$ pour un certain $i$. Après la lecture de $a^i$, on est dans la clause $C_i$. Puis par lecture de $b$, on atteint l'état $l_j$ un littéral de valuation \textsf{vrai} de $C_i$. Donc par le lecture du second $b$, on atteint $C_0$ \textblue{($\nu(l_j) = \textsf{vrai}$)}.
				\end{itemize}
				
				Montrons que $\mathcal{A}$ rejette $T$.
				\begin{itemize}[label=$\to$]
					\item $\mathcal{A}$ rejette $T_1$: soit $u = a^i$ pour un certain $0 < i < k$. La lecture d'un tel mot nous amène dans l'état $q_i \neq C_0$ \textblue{(car pour atteindre $C_0$, $i$ doit être égal à $0$ ou à $k$)}.
					
					\item $\mathcal{A}$ rejette $T_2$: soit $u =a^iba^j$ pour $i$ et $j$ tels que $l_j$ ne soit pas une clause de $C_i$. La lecture de $a^i$ nous amène à l'état $C_i$. Puis la lecture de $b$ nous donne l'état $l_h \neq l_j$ \textblue{($l_j$ n'est pas un littéral de $C_i$)} pour un certain littéral de la clause $C_i$. La lecture de $a^j$ ne nous amène pas en $C_0$ car on n'est pas dans l'état $l_j$ à $j$ états de $C_0$.
					
					\item $\mathcal{A}$ rejette $T_3$: soit $u = a^{m+j}ba^h$ pour un certain $j$ et un certain $h$. La lecture de $a^{m+j}$ nous amène à l'état $l_j$. Si $\nu(l_j)= \textsf{vrai}$, la lecture de $b$ donne l'état $C_0$. Comme $0 < h < 2v$, la lecture de $a^h$ ne permet pas de revenir en $C_0$. Si $\nu(l_j)= \textsf{faux}$, la lecture de $b$ donne l'état $\neg x_0$. Comme $h \neq v$, la lecture de $a^h$ ne permet pas d'atteindre $C_0$.
					
					\item $\mathcal{A}$ rejette $T_4$: soit $u = a^{m+j}ba^{m+j+v}b$ pour un certain $j$. Supposons, sans perte de généralité \textblue{(l'autre cas se traite de la même façon)}, que $\nu(x_j) = \textsf{vrai}$. La lecture de $a^{m+j}b$ nous amène en $C_0$. La lecture de $a^{m+j+v}$ nous permet d'atteindre $\neg x_j$. Donc la lecture du $b$ nous amène et $\neg x_0 \neq C_0$.
				\end{itemize}
			\end{description}
		\end{proof}		
		
		
		\begin{figure}
			\begin{minipage}{.46\textwidth}
				\centering
				\begin{tikzpicture}[font=\sffamily]
				% Setup the style for the states
				\tikzset{node style/.style={state, 
						minimum width=1cm,
						line width=.25mm,
						fill=white,circle}}
				
				% Draw the states
				\node[node style,initial,accepting] at (0,0) (0) {$C_0$};
				\node[node style] at (2,0) (1) {$C_i$};
				\node[node style] at (4,0) (2) {$l_j$};
				
				% Connect the states with arrows
				\draw[every loop,auto=left,line width=.25mm,
				>=latex]
				(0) edge [] node {$a^i$} (1)
				(0) edge [bend left] node {$a^{m+j}$} (2)
				(2) edge [] node {$b$} (1)
				(1) edge [bend left] node {$a^{m+2v - i}$} (0);
				\end{tikzpicture}
				\begin{tikzpicture}[font=\sffamily]
				% Setup the style for the states
				\tikzset{node style/.style={state, 
						minimum width=1cm,
						line width=.25mm,
						fill=white,circle}}
				
				% Draw the states
				\node[node style,initial,accepting] at (0,0) (0) {$C_0$};
				\node[node style] at (2,0) (1) {$l_j$};
				\node[node style] at (4,0) (2) {$l_i$};
				
				% Connect the states with arrows
				\draw[every loop,auto=left,line width=.25mm,
				>=latex]
				(1) edge [] node {$b$} (2)
				(0) edge [] node {$a^{m+j}$} (1)
				(2) edge [bend left] node {$a^{2v - i}$} (0);
				\end{tikzpicture}
				\caption{Deux morceaux de l'automate $\mathcal{A}$ sous l'hypothèse de l'absurde pour $T_3$ (selon la distinction de cas).}
				\label{fig:T3_abs}
			\end{minipage} \hfill		
			\begin{minipage}{.46\textwidth}
				\centering
				\begin{tikzpicture}[font=\sffamily]
				% Setup the style for the states
				\tikzset{node style/.style={state, 
						minimum width=1cm,
						line width=.25mm,
						fill=white,circle}}
				
				% Draw the states
				\node[node style,initial,accepting] at (0,0) (0) {$C_0$};
				\node[node style] at (2,0) (1) {$l_j$};
				\node[node style] at (0,-2) (2) {$l_{j+v}$};
				
				% Connect the states with arrows
				\draw[every loop,auto=left,line width=.25mm,
				>=latex]
				(1) edge [bend left] node {$b$} (0)
				(0) edge [bend left] node {$a^{m+j}$} (1)
				(0) edge [bend right,auto=right] node {$a^{m+v+j}$} (2)
				(2) edge [bend right,auto=right] node {$b$} (0);
				\end{tikzpicture}
				\caption{Un morceau de l'automate $\mathcal{A}$ sous l'hypothèse de l'absurde pour $T_4$.}
				\label{fig:T4_abs}
			\end{minipage}
		\end{figure}
		
		
	
		Donc, le problème \textsc{SPA} est NP-dur, ce qui implique sa NP-complétude.
	\end{proof}
	
	
	\textblue{\textbf{Remarque}: On ne garantie pas que toutes les variables ou leur négation soient véridique si $\varphi$ est satisfiable. Ce n'est pas génant pour montrer la NP-complétude car pour montrer que le formule est satisfiable un seul littéral par clause de valuation \textsf{vrai} suffit.}
	
	\section{Théorème de Cook}
	\input{./../Notions/NPC_Cook.tex}
	
	\section{Technique de preuve : la réduction}
	\input{./../Notions/Indecidabilite_reduction.tex}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}