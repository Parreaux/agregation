\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{mathpartir}

\usepackage{array}
\usepackage{tikz}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
		};
		%\draw[color=blue] (box.north west)--(box.north east);
		\node[title] at (box.north) {#2};
		\end{tikzpicture}\bigskip%
} 

\begin{document}
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\newcommand\evalr{\ensuremath{\Downarrow}}
	\newcommand\annotPoint[2]{\tikz[baseline, remember picture] \node (#1) {\(#2\)};}
	\newcommand\trAnnot[4][left]{\draw[draw, thick, ->] (#2) edge[bend #1] node {\(#4\)}
		(#3)}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem{lemme}{Lemme}
	\newtheorem*{appli}{Application}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Équivalence des sémantiques opérationnelles à petits et grands pas pour le langage IMP}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Nielson $\&$ Nielson \cite[p.41]{NielsonNielson}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 930 (Sémantique).
		}}
	
	\section{Introduction}
	
	Nous avons définie deux sémantiques opérationnelles pour les instructions du langage IMP qui nous permettent de savoir comment on calcul. Ces deux sémantiques diffère par les pas de calcul qu'elle modélise: la sémantique à grand pas ne déroule pas les instructions contrairement à la sémantique à petit pas. Certaines propriétés sur les langages de programmation sont donc plus facilement exprimable par l'une ou l'autre de ces sémantiques, voir même inexprimable par l'une des sémantiques. Cependant, dans le cas particulier du langage IMP, nous allons voir qu'elles sont équivalentes : elles expriment donc les même propriété. Outre la motivation consistant à définir deux sémantiques équivalentes, ce résultat est intéressant par les deux inductions qu'il met en place dans sa preuve.
	
	\titlebox{red}{\textred{\textbf{Remarques sur le développement}}}{
		Ce développement demande une bonne compréhension des sémantiques opérationnelles à grands pas et à petits pas. Les propriétés sur les sémantiques sont à énoncer dans le plan (au tableau si on a le temps) mais pas à montrer.
		\begin{enumerate}
			\item Présentation des règles des sémantiques \textblue{(à écrire dans un coin du tableau)}.
			\item Preuve que la sémantique à grands pas implique celle à petit pas.
			\item Preuve que la sémantique à petits pas implique celle à grands pas.
		\end{enumerate}
	}
	
	\section{Les sémantiques opérationnelles à petits et grands pas}
	
	\paragraph{Sémantique opérationnelle à grands pas}
	\input{./../Notions/Semantique_GrandsPas.tex}
	
	\paragraph{Sémantique opérationnelle à grands pas}
	\input{./../Notions/Semantique_PetitsPas.tex}
	
	\section{Équivalence de ces sémantique pour le langage IMP}
	
	\begin{theo}
		Pour toute instruction $S$ du langage IMP, $\mathcal{S}_{SOS} \llbracket S \rrbracket = \mathcal{S}_{NS} \llbracket S \rrbracket$
	\end{theo}
	\begin{proof}
		Ce théorème exprime la propriété suivante : l'exécution d'une instruction $S$ à partir d'un état $s$ termine dans l'une des sémantique \textblue{(sémantique à grands pas)} si et seulement si elle termine dans l'autre \textblue{(sémantique à petits pas)}. Commençons par énoncer quelques propriétés sur la sémantique à petits pas.
		
		\paragraph{Propriétés sur la sémantique à petits pas} On énonce ici des résultats sur le comportement de la composition sous la sémantique à petits pas. En effet, dans une composition l'instruction suivante n'affecte pas son instruction précédente (lemme~\ref{lem:petitPas}). De plus, elle peut être décomposée en deux étapes : elle étudie la première instruction puis la seconde (lemme~\ref{lem:petitPasDecomposition})
		
		\begin{lemme}
			Soient $S_1, S_2 \in Stm$, $s, s' \in State$ et $k \in \N$ tels que $\left< S_1, s \right> \Rightarrow^k s'$, alors $\left< S_1;S_2, s \right> \Rightarrow^k \left< S_2, s' \right>$. \textblue{L'exécution de $S_1$ n'est pas affectée par l'exécution de l'instruction suivante.}
			\label{lem:petitPas}
		\end{lemme}
		\begin{proof}
			On raisonne par induction sur $k$.
			\begin{description}
				\item[Initialisation ($k = 0$)] Impossible \textblue{($\left< S_1, s \right> \neq s'$)}
				
				\item[Héridité] Soit $k$ tel que le lemme soit vrai. Soit $s \in State$ tel que $\left< S_1, s \right> \Rightarrow^{k+1} s'$. Soient $S_1, S_2$ deux instructions du langage IMP. On distingue les cas selon l'instruction $S_1$:
				\begin{description}
					\item[\protect{Cas $[ass_{SOS}]$}] ok (on est dans le cas $k = 0$) \textblue{Soit $s \in State$ tel que $\left< x:=a, s \right> \Rightarrow s[x \mapsto \mathcal{A}\llbracket a \rrbracket_s]$. Par la règle $[comp_{SOS}^2]$, on a $\left< x: =a; S_2, s \right> \Rightarrow \left< S_2, s[x \mapsto \mathcal{A}\llbracket a \rrbracket_s] \right>$.}
					
					\item[\protect{Cas $[skip_{SOS}]$}] ok (on est dans le cas $k = 0$) \textblue{Soit $s \in State$ tel que $\left< \textsf{skip}, s \right> \Rightarrow s$. Par la règle $[comp_{SOS}^2]$, on a $\left< \textsc{skip}; S_2, s \right> \Rightarrow s$.}
					
					\item[\protect{Cas $[comp_{SOS}^1]$}] Soient $S_1', S_2'$ deux instructions et $s, s'' \in State$ tels que $\left< S_1'; S_2', s \right> \Rightarrow^{k+1} s'$. En appliquant la règle $[comp_{SOS}^1]$, on a $\left< S_1'; S_2', s \right> \Rightarrow \left< S_1''; S_2', s \right> \Rightarrow^{k} s'$. En appliquant l'hypothèse d'induction à $\left< S_1''; S_2', s \right> \Rightarrow^{k} s'$, on obtient $\left< S_1''; S_2';S_2, s \right> \Rightarrow^{k} s'$ et donc $\left< S_1;S_2, s \right> \Rightarrow^{k+1} s'$.
					
					\item[\protect{Cas $[comp_{SOS}^2]$}] Soient $S_1', S_2'$ deux instructions et $s, s'' \in State$ tels que $\left< S_1'; S_2', s \right> \Rightarrow^{k+1} s'$. En appliquant la règle $[comp_{SOS}^1]$, on a $\left< S_1'; S_2', s \right> \Rightarrow \left< S_2', s \right> \Rightarrow^{k} s'$. En appliquant l'hypothèse d'induction à $\left< S_2', s \right> \Rightarrow^{k} s'$, on obtient $\left< S_2';S_2, s \right> \Rightarrow^{k} s'$ et donc $\left< S_1;S_2, s \right> \Rightarrow^{k+1} s'$. 
					
					
					\item[\protect{Cas $[if_{SOS}^{tt}]$}] Soient $S_1', S_2'$ deux instructions et $s, s'' \in State$ tels que $\left< \textsf{ if } b \textsf{ then } S_1' \textsf{ else } S_2', s \right> \Rightarrow^{k+1} s'$ et $b$ tel que $\mathcal{B} \llbracket b \rrbracket =tt$. On applique alors  $[if_{SOS}^{tt}]$, ce qui donne la séquence de dérivation suivante $\left< \textsf{ if } b \textsf{ then } S_1' \textsf{ else } S_2', s \right> \Rightarrow \left< S_1', s \right> \Rightarrow^{k} s'$. On applique l'hypothèse de récurrence à la dérivation $\left< S_1', s \right> \Rightarrow^{k} s'$, on obtient $\left< S_1'; S_2, s \right> \Rightarrow^{k} s'$. On obtient donc $\left< S_1;S_2, s \right> \Rightarrow^{k+1} s'$. 
					
					\item[\protect{Cas $[if_{SOS}^{ff}]$}] Analogue au cas $[if_{SOS}^{tt}]$. \textblue{Soient $S_1', S_2'$ deux instructions et $s, s'' \in State$ tels que $\left< \textsf{ if } b \textsf{ then } S_1' \textsf{ else } S_2', s \right> \Rightarrow^{k+1} s'$ et $b$ tel que $\mathcal{B} \llbracket b \rrbracket =ff$. On applique alors  $[if_{SOS}^{ff}]$, ce qui donne la séquence de dérivation suivante $\left< \textsf{ if } b \textsf{ then } S_1' \textsf{ else } S_2', s \right> \Rightarrow \left< S_2', s \right> \Rightarrow^{k} s'$. On applique l'hypothèse de récurrence à la dérivation $\left< S_2', s \right> \Rightarrow^{k} s'$, on obtient $\left< S_2'; S_2, s \right> \Rightarrow^{k} s'$. On obtient donc $\left< S_1;S_2, s \right> \Rightarrow^{k+1} s'$. }
					
					\item[\protect{Cas $[while_{SOS}]$}] Soient $S$ une instruction et $s, s'' \in State$ tels que $\left< \textsf{ while } b \textsf{ do } S, s \right> \Rightarrow^{k+1} s'$. On applique alors  $[while_{SOS}]$, ce qui donne la séquence de dérivation suivante $\left< \textsf{ while } b \textsf{ do } S, s \right> \Rightarrow \left< \textsf{ if } b \textsf{ then } (S; \textsf{ while } b \textsf{ do } S) \textsf{ else } \textsf{skip}, s \right> \Rightarrow^{k} s'$. On applique l'hypothèse de récurrence à la dérivation $\Rightarrow \left< \textsf{ if } b \textsf{ then } (S; \textsf{ while } b \textsf{ do } S) \textsf{ else } \textsf{skip}, s \right> \Rightarrow^{k} s'$, on obtient $Rightarrow \left< (\textsf{ if } b \textsf{ then } (S; \textsf{ while } b \textsf{ do } S) \textsf{ else } \textsf{skip});S_2, s \right> \Rightarrow^{k} s'$. On obtient donc $\left< S_1;S_2, s \right> \Rightarrow^{k+1} s'$. 
				\end{description}
			\end{description}
		\end{proof}
		
		\begin{lemme}
			Soient $S_1, S_2 \in Stm$, $s, s'' \in State$ et $k \in \N$ tels que $\left< S_1;S_2, s \right> \Rightarrow^k s''$, alors il existe $s' \in State$ et $k_1, k_2 \in \N$ tels que $k = k_1 + k_2$ et $\left< S_1, s \right> \Rightarrow^{k_1} s'$, $\left< S_2, s' \right> \Rightarrow^{k_2} s''$. \textblue{L'exécution de la séquence peut être décomposée en deux parties : la première puis la deuxième instruction.}
			\label{lem:petitPasDecomposition}
		\end{lemme}
		\begin{proof}
			On raisonne par induction sur $k$.
			\begin{description}
				\item[Initialisation ($k = 0$)] Impossible \textblue{($\left< S_1;S_2, s \right> \neq s''$)}
				
				\item[Héridité] Soit $k$ tel que le lemme soit vrai. Soit $s \in State$ tel que $\left< S_1;S_2, s \right> \Rightarrow^{k+1} s''$. On alors $\left< S_1;S_2, s \right> \Rightarrow \gamma \Rightarrow^{k} s''$. On distingue deux cas:
				\begin{description}
					\item[Cas $\left< S_1, s \right> \Rightarrow \left< S_1', s' \right>$] Dans ce cas, $\gamma = \left< S_1'; S_2, s' \right> \Rightarrow^{k} s''$. On applique l'hypothèse d'induction et on trouve $k_1'$ et $k_2$. On pose $k_1 = k_1 ' +1$. D'où le résultat.
					\item[Cas $\left< S_1, s \right> \Rightarrow s'$] Dans ce cas, $\gamma = \left<S_2, s' \right> \Rightarrow^{k} s''$. On a alors $k_1 = 1$ et $k_2 = k$.
				\end{description}
			\end{description}
		\end{proof}
		
		\paragraph{Équivalence sous la sémantique à grands pas} On énonce un résultat d'équivalence que nous utiliserons dans la preuve.
		
		\begin{lemme}
			Pour toute instruction $S$, les instructions $\textsf{ if } b \textsf{ then } (S; \textsf{ while } b \textsf{ do } S) \textsf{ else } \textsf{skip}$ et $\textsf{ while } b \textsf{ do } S$ sont sémantiquement équivalence sous la sémantique à grands pas.
		\end{lemme}
		\begin{proof}
			Supposons qu'il existe $s' \in State$ tel que $\left< \textsf{ while } b \textsf{ do } S, \right> \to s'$. On distingue deux cas:
			\begin{itemize}
				\item Si on est dans le cas d'application de la règle $[while_{NS}^{ff}]$. On a alors $\mathcal{B} \llbracket b \rrbracket_s = ff$ et $s'= s$. On obtient l'arbre de dérivation en appliquant les règles $[if_{NS}^{ff}]$ et $[skip_{NS}]$.
				
				\item Si on est dans le cas d'application de la règle $[while_{NS}^{tt}]$, on construit les deux arbres de dérivations qui vont bien : pour \textsf{while}, on applique la règle $[while_{NS}^{tt}]$ et dans le cas du \textsf{if}, on applique les règles $[if_{NS}^{tt}]$ et $[while_{NS}^{tt}]$.
			\end{itemize}
			
			Réciproquement, on raisonne de même. Si on est dans le cas de l'application de la règle $[if_{NS}^{ff}]$, alors on peut appliquer la règle $[while_{NS}^{ff}]$ ce qui nous donne la concordance. Sinon, on applique la règle $[while_{NS}^{tt}]$ pour le \textsf{while} et les règles $[if_{NS}^{tt}]$ et $[while_{NS}^{tt}]$ pour le \textsf{if}.
		\end{proof}

			
		
		\paragraph{Étape 2 : grands pas implique petit pas} Soient $S$ une instruction du lange IMP et $s, s' \in State$, montrons que $\left< S, s \right> \to s'$ implique $\left< S, s \right> \Rightarrow^* s'$. On raisonne par induction sur la structure de l'arbre de dérivation de $\left< S, s \right> \to s'$. On pose $\mathcal{H} = " \forall s, s' \in State, \left< S, s \right> \to s' \text{ implique } \exists k \in \N, \left< S, s \right> \Rightarrow^* s' "$ l'hypothèse d'induction.
		\begin{description}
			\item[\protect{Cas $\left[ass_{NS}\right]$}] Soit $s \in State$ tel que $\left< x:= a, s \right> \to s\left[x \mapsto \mathcal{A}\llbracket a \rrbracket_s\right]$ \textblue{(donnée par la règle $\left[ass_{NS}\right]$ de la sémantique à grands pas)}. Par la règle $[ass_{SOS}]$ \textblue{(sémantique à petit pas)}, on a $\left< x:= a, s \right> \Rightarrow s\left[x \mapsto \mathcal{A}\llbracket a \rrbracket_s\right]$. Donc $\mathcal{H}$ est vraie pour $\left[ass_{NS}\right]$.
			
			\item[\protect{Cas $\left[skip_{NS}\right]$}] Analogue au cas $\left[ass_{NS}\right]$. \textblue{(Soit $s \in State$ tel que $\left< \textsf{skip}, s \right> \to s$ (donnée par la règle $\left[ass_{NS}\right]$ de la sémantique à grands pas). Par la règle $[skip_{SOS}]$ (sémantique à petit pas), on a $\left< \textsf{skip}, s \right> \Rightarrow s$. Donc $\mathcal{H}$ est vraie pour $\left[skip_{NS}\right]$.)}
			
			\item[\protect{Cas $\left[comp_{NS}\right]$}] Soient $S_1$ et $S_2$ deux instructions du langage IMP et $s,s', s'' \in State$ tels qu'on ait
			\begin{displaymath}
			\left[comp_{NS}\right]\frac{\left< S_1, s \right> \to s' \textcolor{white}{aaa} \left< S_2, s' \right> \to s''}{\left< S_1; S_2, s \right> \to s''}
			\end{displaymath}
			En appliquant l'hypothèse d'induction aux deux prémisses $\left< S_1, s \right> \to s'$ et $\left< S_2, s' \right> \to s''$ nous donne $\left< S_1, s \right> \Rightarrow^* s'$ et $\left< S_2, s' \right> \Rightarrow^* s''$. Par le lemme~\ref{lem:petitPas}, on a $\left< S_1; S_2, s \right> \Rightarrow^* \left< S_2, s' \right>$. On a ainsi la séquence de dérivation $\left< S_1; S_2, s \right> \Rightarrow^* \left< S_2, s' \right> \Rightarrow^* s''$. Donc $\left< S_1; S_2, s \right> \Rightarrow^*
			 s''$ et $\mathcal{H}$ est vraie pour $\left[comp_{NS}\right]$.
			
			\item[\protect{Cas $\left[if_{NS}^{tt}\right]$}] Soient $S_1$ et $S_2$ deux instructions du langage IMP et $s,s', s'' \in State$ tels qu'on ait 
			\begin{displaymath}
			[if_{NS}^{tt}] \frac{\left< S_1, s \right> \to s'}{\left< \textsc{ if } b \textsc{ then } S_1 \textsc{ else } S_2, s \right> \to s'} \text{ si } \mathcal{B}\llbracket b \rrbracket_s = tt
			\end{displaymath}
			Comme $\mathcal{B}\llbracket b \rrbracket_s = tt$, par la règle $[if_{SOS}^{tt}]$, on a $\left< \textsc{ if } b \textsc{ then } S_1 \textsc{ else } S_2, s \right> \Rightarrow \left< S_1, s \right>$. En appliquant l'hypothèse d'induction à la prémisse $\left< S_1, s \right> \to s'$, on a $\left< S_1, s \right> \Rightarrow^* s'$. On a ainsi la séquence de dérivation $\left< \textsc{ if } b \textsc{ then } S_1 \textsc{ else } S_2, s \right> \Rightarrow \left< S_1, s \right> \Rightarrow^{*} s'$. Donc $\left< \textsc{ if } b \textsc{ then } S_1 \textsc{ else } S_2, s \right> \Rightarrow^* s'$ et $\mathcal{H}$ est vraie pour $\left[if_{NS}^{tt}\right]$.
			
			\item[\protect{Cas $\left[if_{NS}^{ff}\right]$}] Analogue au cas $\left[if_{NS}^{tt}\right]$. \textblue{Soient $S_1$ et $S_2$ deux instructions du langage IMP et $s,s', s'' \in State$ tels qu'on ait
				\begin{displaymath}
				[if_{NS}^{ff}] \frac{\left< S_2, s \right> \to s'}{\left< \textsc{ if } b \textsc{ then } S_1 \textsc{ else } S_2, s \right> \to s'} \text{ si } \mathcal{B}\llbracket b \rrbracket_s = ff
				\end{displaymath}
			Comme $\mathcal{B}\llbracket b \rrbracket_s = ff$, par la règle $[if_{SOS}^{ff}]$, on a $\left< \textsc{ if } b \textsc{ then } S_1 \textsc{ else } S_2, s \right> \Rightarrow \left< S_2, s \right>$. En appliquant l'hypothèse d'induction à la prémisse $\left< S_2, s \right> \to s'$, on a $\left< S_2, s \right> \Rightarrow^* s'$. On a ainsi la séquence de dérivation $\left< \textsc{ if } b \textsc{ then } S_1 \textsc{ else } S_2, s \right> \Rightarrow \left< S_2, s \right> \Rightarrow^{*} s'$. Donc $\left< \textsc{ if } b \textsc{ then } S_1 \textsc{ else } S_2, s \right> \Rightarrow^* s'$ et $\mathcal{H}$ est vraie pour $\left[if_{NS}^{ff}\right]$.}
			
			\item[\protect{Cas $\left[while_{NS}^{tt}\right]$}] Soient $S$ une instruction du langage IMP et $s,s', s'' \in State$ tels que 
			\begin{displaymath}
			[while_{NS}^{tt}] \frac{\left< S, s \right> \to s'~\textcolor{white}{aaa}~\left< \textsf{while } b \textsf{ do } S, s' \right> \to s''}{\left< \textsf{while } b \textsf{ do } S, s \right> \to s''} \text{ si } \mathcal{B}\llbracket b \rrbracket_s = tt
			\end{displaymath}
			En appliquant l'hypothèse d'induction aux prémisses $\left< S, s \right> \to s'$ et $\left< \textsf{while } b \textsf{ do } S, s' \right> \to s''$, on a $\left< S, s \right> \Rightarrow^* s'$ et $\left< \textsf{while } b \textsf{ do } S, s' \right> \Rightarrow^* s''$. Par le lemme~\ref{lem:petitPas} appliqué à $\left< S, s \right> \Rightarrow^* s'$, on a $\left< S; \textsf{while } b \textsf{ do } S, s \right> \Rightarrow^* s''$. Par les règles $[while_{NS}^{tt}]$ et $[if_{NS}^{tt}]$, on a la séquence de dérivation : $\left< \textsf{while } b \textsf{ do } S, s \right> \Rightarrow \left< \textsc{ if } b \textsc{ then } (S; \textsf{while } b \textsf{ do } S) \textsc{ else } \textsf{skip}, s \right> \Rightarrow \left< S; \textsf{while } b \textsf{ do } S, s \right>$. On a ainsi la séquence de dérivation $\left< \textsf{while } b \textsf{ do } S, s \right> \Rightarrow^* \left< S; \textsf{while } b \textsf{ do } S, s \right> \Rightarrow^{*} s''$. Donc $\left< \textsf{while } b \textsf{ do } S, s \right> \Rightarrow^* s''$ et $\mathcal{H}$ est vraie pour $\left[while_{NS}^{tt}\right]$.
			
			\item[\protect{Cas $\left[while_{NS}^{ff}\right]$}] Évident \textblue{ Soient $S$ une instruction du langage IMP et $s\in State$ tels que $[while_{NS}^{ff}] \left< \textsf{while } b \textsf{ do } S, s \right> \to s \text{ si } \mathcal{B}\llbracket b \rrbracket_s = ff$.	Par la règle $[while_{SOS}]$, on a $\left< \textsf{while } b \textsf{ do } S, s \right> \Rightarrow \left< \textsc{ if } b \textsc{ then } (S; \textsf{while } b \textsf{ do } S) \textsc{ else } \textsf{skip}, s \right>$. Comme $\mathcal{B}\llbracket b \rrbracket_s = ff$, la règle $[if_{SOS}^{ff}]$ donne la séquence de dérivation suivante : $\left< \textsf{while } b \textsf{ do } S, s \right> \Rightarrow \left< \textsc{ if } b \textsc{ then } (S; \textsf{while } b \textsf{ do } S) \textsc{ else } \textsf{skip}, s \right> \Rightarrow \left<\textsf{skip}, s \right>$. On applique enfin la règle $[skip_{SOS}]$, ce qui nous donne : $\left< \textsf{while } b \textsf{ do } S, s \right> \Rightarrow^* s$ et $\mathcal{H}$ est vraie pour $\left[while_{NS}^{ff}\right]$.}
		\end{description}
		
		\paragraph{Étape 3 : petits pas implique grands pas}  Réciproquement, soient $S$ une instruction du lange IMP, $s, s' \in State$ et $k \in \N$, montrons que $\left< S, s \right> \Rightarrow^k s'$ implique $\left< S, s \right> \to s'$. \textblue{Contrairement à l'implication précédente, on ne peut pas raisonner de manière structurelle sur la sémantique (dans le cas du \textsf{while} on n'a pas un sous terme strict).} On raisonne par induction sur $k$. On pose $\mathcal{H}_k = "\left< S, s \right> \Rightarrow^k s' \text{ implique } \left< S, s \right> \to s'"$ l'hypothèse d'induction.
		\begin{description}
			\item[Initialisation ($k = 0$)] Impossible car $\left< S, s \right>$ n'est pas égal à $s'$.
			
			\item[Hérédité] Soit $k_0 \in \N$ tel que $\forall k \leq k_0$,  $\left< S, s \right> \Rightarrow^k s'$ implique $\left< S, s \right> \to s'$. Montrons que si  $\left< S, s \right> \Rightarrow^{k_0 +1} s'$ alors $\left< S, s \right> \to s'$. On distingue les cas selon l'instruction $S$.
			\begin{description}
				\item[\protect{Cas $[ass_{SOS}]$}] ok (on est dans le cas $k_0 = 0$) \textblue{Soit $s \in State$ tel que $\left< x:=a, s \right> \Rightarrow s[x \mapsto \mathcal{A}\llbracket a \rrbracket_s]$. Par la règle $[ass_{NS}]$, on a $\left< S, s \right> \to s[x \mapsto \mathcal{A}\llbracket a \rrbracket_s]$. On a donc $\mathcal{H}_{k_0+1}$.}
				
				\item[\protect{Cas $[skip_{SOS}]$}] ok (on est dans le cas $k_0 = 0$) \textblue{Soit $s \in State$ tel que $\left< \textsf{skip}, s \right> \Rightarrow s$. Par la règle $[skip_{NS}]$, on a $\left< \textsc{skip}, s \right> \to s$. On a donc $\mathcal{H}_{k_0+1}$.}
				
				\item[\protect{Cas $[comp_{SOS}^1]$ et $[comp_{SOS}^2]$}] Soient $S_1, S_2$ deux instructions et $s, s'' \in State$ tels que $\left< S_1; S_2, s \right> \Rightarrow^{k_0+1} s'$. En appliquant le lemme~\ref{lem:petitPasDecomposition} à $\left< S_1; S_2, s \right> \Rightarrow^{k_0+1} s$, il existe $s' \in State$ et $k_1, k_2 \in \N$ tels que $k_0 + 1 = k_1 + k_2$ et $\left< S_1, s \right> \Rightarrow^{k_1} s'$, $\left< S_2, s' \right> \Rightarrow^{k_2} s''$. On applique l'hypothèse d'induction à $\left< S_1, s \right> \Rightarrow^{k_1} s'$ et à $\left< S_2, s' \right> \Rightarrow^{k_2} s''$, on obtient $\left< S_1, s \right> \to s'$ et $\left< S_2, s' \right> \to s''$. Par la règle $[comp_{NS}]$, on a $\left< S_1; S_2, s \right> \to s$ et $\mathcal{H}_{k_0+1}$ est vraie.
				
				
				\item[\protect{Cas $[if_{SOS}^{tt}]$}] Soient $S_1, S_2$ deux instructions et $s, s'' \in State$ tels que $\left< \textsf{ if } b \textsf{ then } S_1 \textsf{ else } S_2, s \right> \Rightarrow^{k_0+1} s'$ et $b$ tel que $\mathcal{B} \llbracket b \rrbracket =tt$. On applique alors  $[if_{SOS}^{tt}]$, ce qui donne la séquence de dérivation suivante $\left< \textsf{ if } b \textsf{ then } S_1 \textsf{ else } S_2, s \right> \Rightarrow \left< S_1, s \right> \Rightarrow^{k_0} s'$. On applique l'hypothèse de récurrence à la dérivation $\left< S_1, s \right> \Rightarrow^{k_0} s'$, on obtient $\left< S_1, s \right> \to s'$. On applique la règle $[if_{NS}^{tt}]$, $\left< \textsf{ if } b \textsf{ then } S_1 \textsf{ else } S_2, s \right> \to s'$. On a donc $\mathcal{H}_{k_0+1}$.
				
				\item[\protect{Cas $[if_{SOS}^{ff}]$}] Analogue au cas $[if_{SOS}^{tt}]$. \textblue{Soient $S_1, S_2$ deux instructions et $s, s'' \in State$ tels que $\left< \textsf{ if } b \textsf{ then } S_1 \textsf{ else } S_2, s \right> \Rightarrow^{k_0+1} s'$ et $b$ tel que $\mathcal{B} \llbracket b \rrbracket = ff$. On applique alors  $[if_{SOS}^{ff}]$, ce qui donne la séquence de dérivation suivante $\left< \textsf{ if } b \textsf{ then } S_1 \textsf{ else } S_2, s \right> \Rightarrow \left< S_2, s \right> \Rightarrow^{k_0} s'$. On applique l'hypothèse de récurrence à la dérivation $\left< S_2, s \right> \Rightarrow^{k_0} s'$, on obtient $\left< S_2, s \right> \to s'$. On applique la règle $[if_{NS}^{ff}]$, $\left< \textsf{ if } b \textsf{ then } S_1 \textsf{ else } S_2, s \right> \to s'$. On a donc $\mathcal{H}_{k_0+1}$.}
				
				\item[\protect{Cas $[while_{SOS}]$}] Soient $S$ une instruction et $s, s'' \in State$ tels que $\left< \textsf{ while } b \textsf{ do } S, s \right> \Rightarrow^{k_0+1} s'$. On applique alors  $[while_{SOS}]$, ce qui donne la séquence de dérivation suivante $\left< \textsf{ while } b \textsf{ do } S, s \right> \Rightarrow \left< \textsf{ if } b \textsf{ then } (S; \textsf{ while } b \textsf{ do } S) \textsf{ else } \textsf{skip}, s \right> \Rightarrow^{k_0} s'$. On applique l'hypothèse de récurrence à la dérivation $\Rightarrow \left< \textsf{ if } b \textsf{ then } (S; \textsf{ while } b \textsf{ do } S) \textsf{ else } \textsf{skip}, s \right> \Rightarrow^{k_0} s'$, on obtient $Rightarrow \left< \textsf{ if } b \textsf{ then } (S; \textsf{ while } b \textsf{ do } S) \textsf{ else } \textsf{skip}, s \right> \to s'$. Par équivalence sémantique de $\textsf{ if } b \textsf{ then } (S; \textsf{ while } b \textsf{ do } S) \textsf{ else } \textsf{skip}$ et de $\textsf{ while } b \textsf{ do } S$ dans la sémantique à grands pas, $\left< \textsf{ while } b \textsf{ do } S, s \right> \to s'$. On a donc $\mathcal{H}_{k_0+1}$.
			\end{description}
		\end{description}
		D'où l'équivalence
	\end{proof}
	
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}