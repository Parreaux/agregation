\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais,english]{babel}%

\usepackage[dvipsnames,svgnames]{xcolor}
\usepackage{graphicx}%
\usepackage{subcaption}
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{array}
\usepackage{tikz}
\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}%

\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude
\usetikzlibrary{automata,positioning}
\usetikzlibrary{calc, shapes, backgrounds}

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
} 

\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}{\textcolor{blue}}
	\newcommand{\textgreen}{\textcolor{Green}}
	\newcommand{\textred}{\textcolor{red}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Les B-arbres}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Cormen \cite[p.447]{Cormen}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 901 (Structures de données); 921 (Algorithmes de recherche); 932 (Base de données).
		}}
	
	\section{Introduction}
	
	On se place dans le cadre d'une recherche de clé dans une base de données stockée sur un disque dur externe. On cherche à minimiser le nombre d'action sur un disque externe (c'est ce qui prend le plus de temps, facteur d'un million entre un accès au disque et d'une opération sur processeur). La structure de données que l'on privilégie est un arbre de recherche. On cherche alors un arbre avec une ramification importante ce qui nous permet de minimiser le nombre d'appel au disque (hauteur plutôt faible ce qui minimise les quantités recherchées). En pratique on stocke une page (unité de transfert avec le disque dur) sur un noeud. Dans ce cas, on maximise l'impact de l'accès au disque.
	
	Lors de ce développement, on se donne deux opérations atomiques: \textsc{Ecriture-Disque} et \textsc{Lecture-Disque}. On veut montrer l'optimalité de la structure de donnée face à l'utilisation de ces deux opérations.
	
	Mettre un exemple dans le cas où on a un arbre de degré minimal $t = 2$ (Figure~\ref{fig:exDef}).	
	
	\begin{figure}
		\begin{minipage}[c]{.46\linewidth}
			\centering
			\begin{tikzpicture}[level/.style={sibling distance=5em, level distance=3em}]
				\node [rectangle,draw] (a){$J~M$}
				child {node [rectangle,draw] (2){$A~B~D$}}
				child {node [rectangle,draw] (b) {$K$}}
				child {node [rectangle,draw] (c) {$S~T~W$}};
			\end{tikzpicture}
			\caption{Exemple d'un B-arbre de degré minimal $2$.}
			\label{fig:exDef}
		\end{minipage} \hfill
		\begin{minipage}[c]{.46\linewidth}
			\centering
			\begin{tikzpicture}[level/.style={sibling distance=5em, level distance=3em}]
			\node [rectangle,draw] (a){$B~J~M$}
			child {node [rectangle,draw] (2){$A$}}
			child {node [rectangle,draw] (b) {$D~E$}}
			child {node [rectangle,draw] (3) {$K$}}
			child {node [rectangle,draw] (c) {$S~T~W$}};
			\end{tikzpicture}
			\caption{Exemple de l'insertion de $E$ dans le B-arbre précédent.}
			\label{fig:Insertion1}
		\end{minipage}
	\end{figure}
	
	\titlebox{red}{\textred{\textbf{Remarques sur le développement}}}{
		Ce développement présente une structure de données: il faut donc appuyer sur son implémentation "réelle".
		\begin{enumerate}
			\item Présentation des enjeux de cette structure de données.
			\item Présentation de la recherche (algorithme et complexité).
			\item Présentation de l'insertion (idées naïve et plus poussée).
		\end{enumerate}
	}
	
	\section{Recherche dans un B-arbre}
	
	\textblue{La recherche dans un B-arbre est analogue à la recherche dans un ABR en rajoutant des intervalles dans les nœuds. On commence alors à chercher dans quel intervalle dans lequel on se trouve, puis on descend.}
	
	L'algorithme prend en paramètre l'élément recherché $k$ et le noeud courant $x$. On retourne alors le noeud $x$ et la place de $k$ dans le noeud $x$, si $k$ est dans l'arbre. Sinon, on retourne \textsc{Nil}.
	
	\begin{algorithm}
		\begin{algorithmic}[1]
			\Function{\textsc{Recherche-BArbre}}{$x, k$} \Comment{$x$ a subit une lecture dans le disque \textsc{Lire-Disque}}
			\State  $i \gets 1$
			\While {$i \leq x.n$ et $k > x.cle_i$} \Comment{Recherche de l'intervalle dans lequel se trouve $k$}
			\State $i \gets i+1$
			\If {$i \leq x.n$ et $ k = x.key_i$} \Comment{On a trouvé $k$}
			\State Retourne $(x, i)$ 
			\Else \Comment{On n'a pas trouvé $k$}
			\If {$x.feuille$} \Comment{On a fini l'arbre et $k$ n'y est pas}
			\State Retourne \textsc{Nil}
			\Else \Comment{On peut continuer à descendre dans l'arbre: récursivité sur le fils de $x$}
			\State \textsc{Lire-Disque}($x.c_i$)
			\State Retourne \textsc{Recherche-BArbre}($x.c_i, k$)
			\EndIf
			\EndIf
			\EndWhile
			\EndFunction
		\end{algorithmic}
		\caption{Recherche dans un B-arbre}
		\label{algo:Recherche}
	\end{algorithm}
	
	\noindent\emph{Hypothèses pour le calcul de la complexité}:
	\begin{itemize}
		\item La racine du B-arbre se trouve toujours en mémoire principal: on n'a pas de \textsc{Lire-Disque}; cependant, il faudra effectuer un \textsc{Écriture-Disque} lors de la modification de la racine.
		\item Tout noeud passé en paramètre devra subir un \textsc{Lire-Disque}.
	\end{itemize}
	
	\begin{theo}
		Soit $T$ un B-arbre à $n$ élément de degré minimal $t \geq 2$. Alors la hauteur $h$ vérifie $h \leq \log_2 \frac{n+1}{2}$.
	\end{theo}
	\begin{proof}
		\begin{itemize}
			\item La racine a au moins une clé et les autres nœuds $t-1$.
			\item $T$ possède $2$ nœuds à la profondeur $1$, $2t$ à la profondeur $2$,... Par récurrence, on montre que $T$ possède $2t^{h-1}$ nœuds à la profondeur $h$.
			\item On en déduit $n \geq 1 + (t-1) \sum_{i=1}^{h}2t^{i-1} = 1 + 2(t-1) (\frac{t^h -1}{t-1}) = 2t^h - 1$.
			\item $t^h \leq \frac{(n+1)}{2}$ et on conclut en passant au $\log_t$.
		\end{itemize}
	\end{proof}
	
	\textblue{Si on garde l'indice $t$ dans le $\log$, il faut savoir le justifier : ce $t$ est très grand et donc $\log_t$ est relativement faible. Dans la vrai vie, $t$ vaut le nombre de tuple que l'on peut mettre sur une page (unité de transfert entre le disque dur et l'ordinateur).}
	
	\emph{Complexité en accès au disque}: $O(h)$ où $h$ est la hauteur de l'arbre. Donc en $O(log_t n)$ lecture-écriture sur le disque (on ne fait que des lectures). \textblue{Comme $n < 2t$, la boucle \textbf{while} s'effectue en $O(t)$, le temps processeur total est $O(th) = O(t \log n)$.}
	
	\section{Insertion dans un B-arbre}
	
	\textblue{L'insertion dans un B-arbre est plus délicate que dans une ABR. Comme dans un ABR, on commence par chercher la feuille sur laquelle on doit insérer cette nouvelle clé. Cependant, on ne peut pas juste créer une nouvelle feuille (on obtient un arbre illicite). On est alors obliger d'insérer l'élément dans un noeud déjà existant.} 
	
	L'opération de l'ajout consiste en la recherche de la place de la clé dans les feuilles de l'arbre puis on insère dans la feuille de l'arbre correspondant.
	\begin{description}
		\item[Problème] on peut violer la capacité du nœud.
		\item[Solution naïve] on joute le nœud et on fait remonter la clé correspondante tant qu'on n'a pas trouvé de nœud capable de garder la clé. Cependant, il nous faut deux passages dans l'arbre: un pour trouver l'emplacement de la nouvelle clé et un pour remonter le superflu. Ce n'est pas optimal.
		\item[Idée] si le nœud est plein lors de la descente de l'arbre (recherche de l'emplacement),  on sépare le nœud (on garanti qu'il n'y a aucun nœud plein sur notre parcours de l'arbre). On alors un unique passage.
	\end{description}
	
	Montrer un exemple : Figures~\ref{fig:Insertion1} et~\ref{fig:Insertion2}
	
	\textblue{L'arbre grandi vers le haut à l'aide de la fonction \textsc{partage} quand la racine est pleine.} 
	
	\begin{figure}
		\centering
		\begin{tikzpicture}[level/.style={sibling distance=5em, level distance=3em}]
		\node [rectangle,draw] (a){$J$}
		child {node [rectangle,draw] (4) {$B$}
			child {node [rectangle,draw] (2){$A$}}
			child {node [rectangle,draw] (b) {$D~E$}}}
		child {node [rectangle,draw] (5) {$M~T$}
			child {node [rectangle,draw] (3) {$K$}}
			child {node [rectangle,draw] (c) {$S$}}
			child {node [rectangle,draw] (c) {$U~W$}}};
		\end{tikzpicture}
		\caption{Exemple de l'insertion de $U$ dans le B-arbre précédent.}
		\label{fig:Insertion2}
	\end{figure}
	
	\emph{Complexité en accès au disque}: $O(h) = O(\log_t n)$ où $h$ est la hauteur de l'arbre
	
	\section{Compléments sur les $B$-arbres}
	\input{./../Notions/BArbre.tex}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
	
\end{document}