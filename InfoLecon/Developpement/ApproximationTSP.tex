\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{array}
\usepackage{tikz}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\newtheorem*{appli}{Application}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Approximation et non-approximation pour les problèmes de voyageur de commerce}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Cormen \cite[p.1022]{Cormen}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 925 (Graphe); 928 (NP-complet).
		}}
	
	\section{Introduction}
	
	Lorsqu'un problème est NP-complet, il n'existe à priori aucun algorithme efficace pour résoudre ce problème. Une méthode nous permettant d'obtenir un algorithme efficace consiste (dans le cas de problème d'optimisation) à chercher un résultat approché (à un facteur près). Le problème du voyager de commerce (\textsc{TSP}) et ses variantes est un problème NP-complet existant sous une version de problème d'optimisation qui n'admet pas d'algorithme d'approximation dans le cas général. Cependant, si on le restreint (on le considère dans un plan) alors il admet une 2-approximation par un algorithme glouton.
	
	On rappelle les deux versions du problème que l'on va considéré.
	
	\titlebox{red}{\textred{\textbf{Remarques sur le développement}}}{
		\begin{enumerate}
			\item Présentation du problème.
			\item Présentation de la 2-approximation de TSPE.
			\item Preuve de la non-approximation de TSP.
			\begin{enumerate}
				\item Construction d'une instance du cycle Hamiltonien à partir de TSP en temps polynomial.
				\item Construction de l'algorithme polynomial qui résout cycle Hamiltonien en temps polynomial (pas une approximation).
			\end{enumerate}
		\end{enumerate}
	} 
	
	\begin{definition}
		Le problème du voyageur de commerce (\textsc{TSP}) sur un graphe complet.
		
		\noindent\begin{tabular}{rl}
			Problème & \textsc{TSP} \\
			\textbf{entrée}: & $G$ un graphe complet de poids $w$ positif \\
			\textbf{sortie } & Oui s'il existe un cycle hamiltonien de poids minimal; non sinon \\
		\end{tabular}
	\end{definition}
	
	\begin{definition}
		Le problème du voyageur de commerce euclidien (\textsc{TSPE}) sur un graphe complet.
		
		\noindent\begin{tabular}{rl}
			Problème & \textsc{TSPE} \\
			\textbf{entrée}: & $G$ un graphe complet de poids $w$ positif vérifiant l'inégalité triangulaire \\
			\textbf{sortie } & Oui s'il existe un cycle hamiltonien de poids minimal; non sinon \\
		\end{tabular}
	\end{definition}
	
	\section{Une 2-approximation du problème TSPE}
	
	On voit que dans le cas euclidien (la fonction de poids vérifie l'inégalité triangulaire), il existe une 2-approximation pour le problème. Cette 2-approximation est basée sur un algorithme glouton.
	
	\begin{prop}
		Il existe une 2-approximation en temps polynomiale du problème TSPE.
	\end{prop}
	\begin{proof}
		\emph{Idée}: On va utiliser l'arbre couvrant minimal dont le poids minore nécessairement le coût du cycle \textblue{(dans le meilleur des cas, le cycle est constitué de l'arbre plus une arête)}.
		
		\begin{minipage}{0.46\textwidth}
			\begin{algorithm}[H]
				\begin{algorithmic}[1]
					\Procedure{\textsc{TSPE-2Approx}}{$G, w$}
					\State Choisir $r \in S$ comme "racine"
					\State $T \gets \textsc{PRIM}(G, w, r)$ \Comment{Calcul de ACM}
					\State $H \gets$ parcours préfixe de $T$ \Comment{Obtenir une liste d'arête}
					\State Renvoie $H$
					\EndProcedure
				\end{algorithmic}
				\caption{Une $2$-approximation pour le problème TSPE.}
				\label{algo:arret}
			\end{algorithm}
		\end{minipage} \hfill
		\begin{minipage}{0.46\textwidth}
			Faire un dessin au tableau représentant les différents parcours de l'arbre. On montre ainsi comment l'hypothèse euclidienne intervient.
		\end{minipage}
		
		Montrons que \textsc{TSPE-2Approx} est bien une $2$-approximation pour le problème TSPE.
		\subparagraph{L'algorithme est polynomial} L'algorithme \textsc{TSPE-2Approx} est polynomial : $O(S^2)$. En effet, \textsc{PRIM} s'effectue en $O(S^2)$ et le parcours de l'arbre se fait en $O(S)$.
		
		\subparagraph{L'algorithme est une $2$-approximation} Soit $H^*$ une tournée optimale. Soit $T$ un arbre couvrant minimal de $G$.
		\begin{itemize}
			\item Comme $H^*$ est un arbre couvrant auquel on a rajouté une arête (de poids positif) et que $T$ est un arbre couvrant minimal: $w(T) \leq w(H^*)$.
			\item Le parcours complet liste les sommets au début et à la fin de la visite $(H_1)$ donne $w(H_1) = 2w(T)$ \textblue{(on traverse deux fois chaque arêtes)}. Donc, $w(H_1) \leq 2w(H^*)$.
			\item $H_1$ n'est pas une tournée \textblue{(visite certains sommets deux fois)}. L'inégalité triangulaire va nous permettre de supprimer la visite d'un sommet sans augmenter le coût d'un cycle. On supprime alors de $H_1$ toutes les deuxièmes visites (sauf à la racine) : $H$ est alors le parcours préfixe de $T$.
			\item $H$ est un cycle hamiltonien et $w(H) \leq w(H_1)$ \textblue{(inégalité triangulaire)}. Donc par ce qui précède, $w(H) \leq 2w(H^*)$.
		\end{itemize} 
		Donc \textsc{TSPE-2APPROX} est une 2-approximation du TSPE.
	\end{proof}
	
	\begin{req}
		La meilleure approximation connue est une $\frac{2}{3}$-approximation. De plus, on sait que dans le cas euclidien, il existe un schéma d'approximation pour ce problème.
	\end{req}
	
	\section{La non approximation du problème TSP}
	
	\begin{prop}
		Si $P \neq NP$, alors il existe aucune $\rho$-approximation de TSP $(\rho \geq 1)$.
	\end{prop}
	\begin{proof}
		On raisonne par contraposée. Soit $\rho > 1$ tel qu'il existe un algorithme $B$ $\rho$-approximant en temps polynomial le problème TSP. \textblue{(On peut supposer que $\rho \in \N$, mais le résultat est moins fort)}.
		\begin{itemize}[label=$\to$]
			\item Montrons que nous pouvons résoudre les instances du problème Hamiltonien en temps polynomial.
			\begin{itemize}[label=$\bullet$]
				\item $G = (S, A)$ une instance du problème Hamiltonien. On souhaite utiliser $B$ pour savoir si il y a ou non un cycle hamiltonien.
				\item On transforme $G$ en une instance de TSP \textblue{(On effectue une réduction du problème Hamiltonien vers le problème TSP.)}
				\begin{itemize}[label=$*$]
					\item $G' = (S, A')$ le graphe complet sur $S$: $A' = \{(u, v) ~|~ u, v \in S, u \neq v \}$.
					\item $w(u, v) = \left\{ 
					\begin{array}{ll}
					1 & \text{si } (u, v) \in A \\
					\rho|S| + 1 & \text{sinon} \\
					\end{array} \right. $
				\end{itemize}
				Construction en temps polynomiale d'une instance.
				\item On exhibe une caractérisation d'un chemin hamiltonien sur $G$ : $G$ possède un chemin Hamiltonien si et seulement si le parcours de TSP sur $G'$ est de poids $|S|$.
				\begin{description}
					\item[$\Rightarrow$] Soit $H$ le cycle Hamiltonien de $G$. Alors, $\forall(u,v) \in H$, $w(u, v) = 1$ \textblue{(car $(u, v) \in A$)}. Comme on fait un cycle de taille $S$ \textblue{(correspondant aux $S$ sommets)} de $G'$, on a une tournée de poids $S$.
					\item[$\Leftarrow$] Supposons que $G$ ne possède pas de cycle Hamiltonien, alors $G'$ utilise une arête qui n'est pas dans $A$. Sa tournée a un coût d'au moins
					\begin{displaymath}
					(\rho|S|+1) + (|S| - 1) = \rho|S| + |S| = |S|(\rho + 1) >  \rho|S|
					\end{displaymath}
				\end{description}
			\end{itemize}
			Remarque: On a un écart $\rho|S|$ entre la présence ou non d'une tournée Hamiltonienne.
			
			\begin{req}
				On prouve la NP-dureté du problème lorsque $\rho$ vaut $1$ avec cette construction.
			\end{req}
			
			\item Caractérisation de la présence d'un cycle Hamiltonien avec $B$ : $G$ a un cycle Hamiltonien si et seulement si $B(G', w)$ renvoie un cycle de poids $\leq \rho|S|$.
			\begin{description}
				\item[$\Rightarrow$] Supposons que $G$ possède un cycle Hamiltonien. Alors $G'$ possède une tournée pour le problème TSP de poids $|S|$. Comme $B$ est une $\rho$-approximation, $B(G', w)$ revoie une tournée de poids $\leq \rho|S|$.
				\item[$\Leftarrow$] Supposons que $B$ retourne un cycle dont le poids est $\rho$ supérieure à la tournée optimale. Donc $B$ renvoie un cycle Hamiltonien.
			\end{description}
			
			\item Le problème Hamiltonien est un problème NP-complet et on a un moyen de le résoudre en temps polynomial. Donc $P = NP$. 
		\end{itemize}
	\end{proof}
	

	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}