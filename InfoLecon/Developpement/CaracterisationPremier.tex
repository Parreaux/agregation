\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{array}
\usepackage{tikz}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}
\usepackage{wasysym}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}


\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
} 

\begin{document}
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\newtheorem*{appli}{Application}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Caractérisation du premier}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Schwarzentruber \cite[p.63]{LegendreSchwarzentruber}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçon où on présente le développement:}}  923 (Analyse lexicale et syntaxique).
		}}
	
	\section{Introduction}
	
	Lors d'une analyse syntaxique par analyse descendante (ou même ascendante) on utilise la notion de premier. Pour en avoir une caractérisation que l'on peut facilement calculé: on utilise la caractérisation suivante. Pour cela, comme dans le cas de grammaires $LL(1)$, on utilise uniquement une stratégie de dérivation gauche.
	
	\titlebox{red}{\textred{\textbf{Remarques sur le développement}}}{
		Ce développement permet de donner une caractérisation de l'ensemble premier.
		\begin{enumerate}
			\item Si on vérifie la caractérisation alors on est premier (à évoquer mais pas à s'étendre dessus le développement est déjà assez long).
			\item Si on est premier alors on vérifie la caractérisation (par récurrence).
		\end{enumerate}
	}
	
	\section{Caractérisation des premiers}
	
	On note $T$ l'ensemble des terminaux et $\mathcal{N}$ l'ensemble des non-terminaux.
	
	\begin{theo}
		$\textsf{Premier}$ est la plus petite partie $P$ de $(T \cup \mathcal{N})^* \times (T \cup \{\Bowtie\})$ telle que
		\begin{enumerate}
			\item $P(a) = \{a\}$ pour tout terminal $a$.
			\item $P(\epsilon) = \{\Bowtie\}$.
			\item Pour toute règle $N \to \epsilon$, $P(\epsilon) \subseteq P(N)$.
			\item Pour toute règle $N \to \alpha_1 \dots \alpha_n$, $P(\alpha_1 \dots \alpha_n) \subseteq P(N)$.
			\item Si $\Bowtie \notin P(\alpha_1)$, alors $P(\alpha_1) \subseteq P(\alpha_1 \dots \alpha_n)$.
			\item Si $\Bowtie \in P(\alpha_1)$, alors $P(\alpha_1) \setminus \{\Bowtie\} \cup P(\alpha_2 \dots \alpha_n) \subseteq P(\alpha_1 \dots \alpha_n)$.
		\end{enumerate}
	\end{theo}
	
	\begin{req}
		Le symbole $\Bowtie$ est un symbole supplémentaire pour désigner la première lettre du mot vide (qui, stricto sensu, n'existe pas). Il permet de distinguer le langage engendré qui contient le mot vide à travers premier. En effet, le langage engendré par $S$ est vide pour la grammaire $S \to S$ et $\textsf{Premier}(S) = \emptyset$, alors le langage engendré par $S$ est $\{\epsilon\}$. Pour la grammaire $S \to \epsilon$ et $\textsf{Premier}(S) = \{\Bowtie\}$.
	\end{req}
	
	\begin{proof}
		$P$ est bien définie. En effet, l'ensemble $(T \cup \mathcal{N})^* \times (T \cup \{\Bowtie\})$ complet vérifie bien l'ensemble des propriétés par définition de l'ensemble $(T \cup \mathcal{N})^* \times (T \cup \{\Bowtie\})$ et notamment quand on prend le point de vue fonctionnel \textblue{(on est obligé de prendre l'ensemble des parties pour la seconde partie de l'ensemble)}.
		\begin{definition}
			On rappelle que la définition de $\textsf{Premier}$ par 
			\begin{displaymath}
			\textsf{Premier} : 
			\begin{array}{ccl}
			(T \cup \mathcal{N})^* & \to & \mathcal{P}(T \cup \{\Bowtie\}) \\
			\alpha & \mapsto & \{a \in T ~|~ \alpha^* a\beta\} \cup \{\Bowtie \text{ si } \alpha \to^* \epsilon\} \\
			\end{array}
			\end{displaymath}
		\end{definition}
		Montrons que $\textsf{Premier} = P$ par double inclusion.
		
		\begin{description}
			\item[$P \subseteq \textsf{Premier}$] $\textsf{Premier}$ vérifie les six conditions.
			\begin{enumerate}
				\item $\textsf{Premier}(a) = \{a\}$ car $a \in T$ et $a \to^* a$.
				\item $\textsf{Premier}(\epsilon) = \{\Bowtie\}$ car $\epsilon \to^* \epsilon$.
				\item Soit $N \to \epsilon$. Montrons que $\textsf{Premier}(\epsilon) \subseteq \textsf{Premier}(N)$. Comme $\textsf{Premier}(\epsilon) = \{\Bowtie\}$ \textblue{(par la propriété 2)}, il nous faut donc montrer que $\{\Bowtie\} \subseteq \textsf{Premier}(N)$ soit que $\Bowtie \in \textsf{Premier}(N)$. Or $N \to \epsilon$, donc $N \to^* \epsilon$ et $\Bowtie \in \textsf{Premier}(N)$ \textblue{(par définition de $\textsf{Premier}$)}.
				\item Soit $N \to \alpha_1 \dots \alpha_n$. Montrons que $\textsf{Premier}(\alpha_1 \dots \alpha_n) \subseteq \textsf{Premier}(N)$. Soit $e \in \textsf{Premier}(\alpha_1 \dots \alpha_n)$. 
				\begin{itemize}
					\item Si $e = \Bowtie$, alors $\alpha_1 \dots \alpha_n \to^* \epsilon$. Donc $N \to \alpha_1 \dots \alpha_n \to^* \epsilon$, soit $N \to^* \epsilon$.
					\item Si $e = a$, alors $\alpha_1 \dots \alpha_n \to^* a\beta$. Donc $N \to \alpha_1 \dots \alpha_n \to^* a\beta$, soit $N \to^* a\beta$.
				\end{itemize}
				\item Supposons que $\Bowtie \notin P(\alpha_1)$. Montrons que $P(\alpha_1) \subseteq P(\alpha_1 \dots \alpha_n)$. Soit $a \in \textsf{Premier}(\alpha_1)$ \textblue{($a \neq \Bowtie$ par hypothèse)}. Par définition de $\textsf{Premier}$, $\alpha_1 \to^* a\beta$. Comme notre stratégie est en dérivation gauche, $\alpha_1\dots\alpha_n \to^* a\underbrace{\beta\alpha_2\dots\alpha_n}_{\beta'}$. D'où $\alpha_1\dots\alpha_n \to^* a\beta'$. Donc $a \in \textsf{Premier}(\alpha_1\dots\alpha_n)$.
				
				\item Supposons que $\Bowtie \in P(\alpha_1)$. Montrons que $P(\alpha_1) \setminus \{\Bowtie\} \cup P(\alpha_2 \dots \alpha_n) \subseteq P(\alpha_1 \dots \alpha_n)$. Soit $a \in P(\alpha_1) \setminus \{\Bowtie\} \cup P(\alpha_2 \dots \alpha_n)$, on distingue deux cas.
				\begin{description}
					\item[Cas $a \in P(\alpha_1) \setminus \{\Bowtie\}$] On se ramène à la propriété 5.
					\item[Cas $a \in P(\alpha_2 \dots \alpha_n)$] On distingue deux cas:
					\begin{itemize}
						\item $a = \Bowtie$ car $\alpha_2 \dots \alpha_n \to^* \epsilon$. Dans cas, $\alpha_1 \dots \alpha_n \to^* \epsilon$ car $\alpha_1 \to^* \epsilon$. Donc $\Bowtie \in \textsf{Premier}(\alpha_2 \dots \alpha_n)$.
						\item $a \in T$ d'où $\alpha_2 \dots \alpha_n \to^* a\beta$. Dans cas, $\alpha_1 \dots \alpha_n \to^* a\beta$ car $\alpha_1 \to^* \epsilon$. Donc $a \in \textsf{Premier}(\alpha_1 \dots \alpha_n)$.
					\end{itemize}
				\end{description}
			\end{enumerate}
			Donc $\textsf{Premier}$ vérifie donc bien les six propriétés et par la minimalité de $P$, on a $P \subseteq \textsf{Premier}$.
			
			\item[$\textsf{Premier} \subseteq P$] On raisonne par récurrence forte sur la longueur de la dérivation: $\forall n \in \N$, $HR_n$: "S'il existe une dérivation gauche $\alpha \to^n a\beta$, alors $a \in P(\alpha)$. S'il existe une dérivation gauche $\alpha \to^n \epsilon$, alors $\Bowtie \in P(\alpha)$.".
			\begin{description}
				\item[Initialisation] Montrons $HR_0$.
				\begin{itemize}
					\item Pour une dérivation gauche $\alpha \to^0 a\beta$ alors $\alpha = a\beta$. Montrons que $a \in P(\alpha)$.
					\begin{displaymath}
					a \in \{a\} \underset{(1)}{=} P(a) \underset{(5) \text{ car } (1) \text{ et } \Bowtie \neq \alpha}{\subseteq} P(a\beta) \underset{(4)}{\subset} P(\alpha)
					\end{displaymath}
					\item Pour une dérivation gauche $\alpha \to^0 \epsilon$ alors $\alpha = \epsilon$. Montrons que $\Bowtie \in P(\alpha)$.
					\begin{displaymath}
					\Bowtie \in \{\Bowtie\} \underset{(6)}{=} P(\epsilon) \underset{(4)}{\subset} P(\alpha)
					\end{displaymath}
				\end{itemize}
				\item[Hérédité] Soit $n \in \N$ tel que $\forall i \leq n$, $HR_i$ est vrai. Montrons que $HR_{n+1}$.
				\begin{itemize}[label=$*$]
					\item Considérons la dérivation gauche $\alpha \to^{n+1} a\beta$. Montrons que $a \in P(\alpha)$. La dérivation peut d'écrire $\alpha \to \gamma \to^{n} a \beta$ soit $N\alpha' \to \gamma'\alpha' \to^{n} a\beta$.
					\begin{itemize}[label=$+$]
						\item Si $N \to a\gamma''$ alors
						\begin{displaymath}
						a \in \{a\} \underset{(1)}{=} P(a) \underset{(5) \text{ car } \Bowtie \neq a}{\subseteq} P(a\gamma') \underset{(4)}{\subset} P(\alpha)
						\end{displaymath}
						\item Si $N \to x_1 \dots x_k$ alors
						\begin{displaymath}
						\underbrace{N\alpha'}_{\alpha} \to \underbrace{x_1 \dots x_k \alpha'}_{\gamma} \to^n a\beta
						\end{displaymath}
						Par $HR_n$, $a \in P(\gamma)$. Montrons maintenant que $a \in P(N)$.
						\begin{itemize}
							\item Si $N \to^{j} \epsilon$ avec $j \leq n$, alors par $HR_j$ \textblue{(récurrence forte)} $\Bowtie \in P(N)$ D'où $\alpha' \to^{n - j} a \beta$ donc par $HR_{n-j}$, $a \in P(\alpha')$. Par la propriété 6, on a $a \in P(\alpha)$.
							\item Si $N \to x_1 \dots x_k$ et $x_1 \dots x_j \to^{\leq n} a \delta$ car $x_i \to a$ et $\forall j < i$ $x_j \to \epsilon$. Donc $\Bowtie \in P(x_j)$, $\forall j < i$ et $x_i \dots x_k \alpha' \to^{n} a \beta$.
							\begin{displaymath}
							a \in \{a\} \underset{(1)}{=} P(x_i) \underset{(5)^*}{\subseteq} P(x_i \dots x_k) \underset{(6)}{\subset} P(x_{i-1} \dots x_k) \underset{(6)^*}{\subset} P(x_1 \dots x_k) \underset{(4)}{\subset} P(N)
							\end{displaymath}
							Puis par la propriété 4 ou 6, $a \in P(\alpha)$.
						\end{itemize}
					\end{itemize}
					\item Considérons la dérivation gauche $\alpha \to^{n+1} \epsilon$. La dérivation peut d'écrire  $\underbrace{N\alpha'}_{\alpha} \to \underbrace{x_1\dots x_k\alpha'}_{\gamma} \to^{n} \epsilon$.
					\begin{itemize}[label=$+$]
						\item $x_1\dots x_k \to^{\leq n} \epsilon$ alors, par hypothèse de récurrence
						\begin{displaymath}
						\Bowtie \in P(x_1\dots x_k) \underset{(4)}{\subseteq} P(N)
						\end{displaymath}
						\item $\alpha' \to^{\leq n} \epsilon$ alors, par hypothèse de récurrence
						\begin{displaymath}
						\Bowtie \in P(\alpha') \underset{(6)}{\subseteq} P(\alpha)
						\end{displaymath}
					\end{itemize}
				\end{itemize}
			\end{description}
		\end{description}
	\end{proof}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}