\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{array}
\usepackage{tikz}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\newtheorem*{appli}{Application}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	\newtheorem*{cex}{Contre-exemple}
	\newtheorem*{ex}{Exemple}
	
	\title{Transformation de Tseintin}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Aucune référence connue
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 916 (logique propositionnelle).
		}}
	
	\section{Introduction}
	Les problèmes \textsc{SAT} et \textsc{CNF-SAT} sont des problèmes $NP$-complet. La mise sous forme normale d'une formule conjonctive (CNF) est donc une action non coûteuse (polynomiale via la réduction entre ces deux problèmes) et donne accès à des méthodes de résolution pour \textsc{SAT}. En effet, les \textsc{SAT}-solver qui tentent de résoudre le problème \textsc{SAT} avec une complexité temporelle en temps polynomiale (dans le cas moyen car dans le pire cas on sera en exponentielle) utilisent des algorithmes (comme DPLL) qui prennent en entrée des formules sous forme CNF voir même sous forme 3CNF. Avoir un algorithme efficace pour effectuer cette transformation est donc essentiel en pratique. 
	
	Il existe un algorithme qui effectue une transformation linéaire pour mettre une formule sous forme normale CNF. La transformation de Tseintin que nous étudions ici met sous forme normale une formule de taille linéaire avec la contrainte de l'équisatisfiabilité (et non l'équivalence). Mais comme la principale application est la résolution  du problème \textsc{SAT} cela ne pose pas de problème.
	
	\titlebox{red}{\textred{\textbf{Remarques sur le développement}}}{
		\begin{enumerate}
			\item Description de la transformation de Tseintin.
			\item Preuve de la linéarité de la transformation.
			\item Preuve de l'équisatisfiabilité de la transformation.
		\end{enumerate}
	} 
	
	\section{Mise sous forme normale conjonctive via Tseintin}
	
	\begin{theo}
		Pour toute formule $\varphi$, il existe une formule sous forme CNF $T(\varphi)$ de taille linéaire en la taille de la formule $\varphi$ telle que $\varphi$ et $T(\varphi)$ sont équisatisfiables \textred{(\textbf{Attention}: pas d'équivalence entre les formules)}.
	\end{theo}
	\noindent\textblue{\emph{Remarque}: On fait même mieux car on est capable de construire une valuation pour $\varphi$.}
	\begin{proof}
		Soit $\varphi$ une formule.
		\begin{itemize}
			\item On note $|\varphi|$ la taille de la formule $\varphi$ correspondant aux nombres de connecteurs logique de $\varphi$.
			\item On note $SF(\varphi)$ l'ensemble des sous-formule de $\varphi$ \textblue{($\varphi$ comprise)} et $V(\varphi)$ l'ensemble des variables propositionnelles \textblue{(proposition atomique)} de $\varphi$.
		\end{itemize}
		Pour tout sous-formule $\psi \in SF(\varphi)$, on définie une nouvelle variable propositionnelle $a_{\psi}$ \textblue{(à qui on donne le sens $a_{\psi}$ est \textsf{vrai}, lorsqu'on définit une valuation pour la formule contenant cette nouvelle variable)}.
		\begin{itemize}
			\item Comme $\{\neg, \vee\}$ est un système de connecteur complet, on peut supposer que $\varphi$ ne contient que ces connecteurs. Si ce n'est pas le cas, on utilise les règles de transformation \textblue{(ce sont des règles de réécriture)} $a \wedge b \equiv \neg(\neg a \vee \neg b)$, $a \Rightarrow b \equiv \neg a \vee b$ et $a \Leftrightarrow b \equiv \neg( \neg(\neg a \vee b) \vee \neg(\neg b \vee a))$ pour remplacer les autres connecteurs. La formule ainsi obtenue est linéaire en la taille de $\varphi$ la formule de départ \textblue{(par exemple, on ajoute trois au nombre de $\vee$)}.
			\item On pose maintenant, $T(\varphi) = a_{\varphi} \wedge \underset{\psi \in SF(\varphi) \setminus V}{\bigwedge} \underbrace{t(\psi)}_{\text{ne contient que des }\neg \text{ et des } \vee}$ où
			\begin{displaymath}
			\begin{array}{l}
			t(\neg \psi) =  a_{\neg \psi} \Leftrightarrow \neg a_{\psi} = (a_{\psi} \vee a_{\neg \psi}) \wedge (\neg a_{\neg\psi} \vee \neg a_{\psi})\\
			t(\psi_1 \vee \psi_2) =  a_{\psi_1 \vee \psi_2} \Leftrightarrow a_{\psi_1} \vee a_{\psi_2} = (a_{\psi_1 \vee \psi_2} \vee a_{\neg \psi_1}) \wedge (a_{\psi_1 \vee \psi_2} \vee \neg a_{\psi_2}) \wedge (\neg a_{\psi_1 \vee \psi_2} \vee \neg a_{\psi_1} \vee \neg a_{\psi_1})\\
			\end{array}
			\end{displaymath}
			On remarque que $|t(\neg \psi)| = 5$ et $|t(\psi_1 \vee \psi_2)| = 10$ \textblue{(elles sont donc bornées)}.
		\end{itemize}
		
		\begin{lemme}
			$|T(\varphi)|$ est linéaire en $|\varphi|$.
		\end{lemme}
		\begin{proof}
			Soit $\varphi$ une formule ne contenant que des connecteurs de $\{\vee, \neg\}$.
			
			\paragraph{Montrons que $\#SP(\varphi) \leq 2|\varphi| +1$} Montrons cette propriété par induction sur $\varphi$ construit avec le système de connecteurs complet $\{\vee, \neg\}$. On pose $\mathcal{H}_{\varphi}$ l'hypothèse d'induction définie par $\mathcal{H}_{\varphi} = "\#SP(\varphi) \leq 2|\varphi| +1"$.
			\begin{description}
				\item[Cas de base: $\varphi \in V$] Comme $\varphi \in V$, le nombre de connecteurs logique de $\varphi$ est $0$. Donc $|\varphi| =0$. De plus, $\#SF(\varphi) = \#\{a_{\varphi}\} = 1$. On en déduit $\mathcal{H}_{\varphi}$.
				
				\item[Cas inductif: $\varphi = \neg \varphi_1$] Supposons que $\mathcal{H}_{\varphi_1}$ est vraie et montrons $\mathcal{H}_{\varphi}$. On a 
				\begin{displaymath}
				\begin{array}{ccll}
				\#SF(\varphi) & = & 1 + \#SF(\varphi_1) & \textblue{(SF(\varphi) = \varphi \cup SF(\varphi_1))} \\
				 & \leq & 1 + 2|\varphi_1| + 1 & \textblue{(\text{hypothèse d'induction})} \\
				 & = & 2(|\varphi_1| + 1) = 2|\varphi| & \textblue{(|\varphi| = |\varphi_1| + 1)} \\
				 & \leq & 2|\varphi| + 1 & \\
				\end{array}
				\end{displaymath}
				
				\item[Cas inductif: $\varphi = \varphi_1 \vee \varphi_2$] Supposons que $\mathcal{H}_{\varphi_1}$ et $\mathcal{H}_{\varphi_2}$ sont vraies et montrons $\mathcal{H}_{\varphi}$. On a 
				\begin{displaymath}
				\begin{array}{ccll}
				\#SF(\varphi) & = & 1 + \#SF(\varphi_1)  + \#SF(\varphi_2) & \textblue{(SF(\varphi) = \varphi \cup SF(\varphi_1) \cup SF(\varphi_2))} \\
				& \leq & 1 + 2|\varphi_1| + 1 + 2|\varphi_2| + 1 & \textblue{(\text{hypothèses d'inductions})} \\
				& = & 2(|\varphi_1| +|\varphi_2| + 1) + 1 = 2|\varphi| +1 & \textblue{(|\varphi| = |\varphi_1| + |\varphi_2| + 1)} \\
				\end{array}
				\end{displaymath}
			\end{description}
			D'ou $\mathcal{H}_{\varphi}$ pour toute formule $\varphi$ du calcul propositionnel.
			
			\textblue{L'idée est que pour tout connecteur, on sépare le sous-formule en au plus deux sous-formules (on peut les voir comme des arbres).}
			
			\paragraph{Montrons que $|T(\varphi)|$ est linéaire en $|\varphi|$} On a 
			\begin{displaymath}
			\begin{array}{ccll}
			|T(\varphi)| & = & 1 + |\underset{\psi \in SF(\varphi) \setminus V}{\bigwedge} t(\psi)| & \textblue{(\text{définition de } T(\varphi))} \\
			& \leq & 1 + k \#SF(\varphi) & \textblue{\text{(majoration de la conjonction)}} \\
			& \leq & 1 + k(2|\varphi|+1) & \textblue{\text{(propriété précédente)}} \\
			& = & 1 + 2k|\varphi|+1+k & \\
			\end{array}
			\end{displaymath}
			Comme $k$ est bornée \textblue{($k \leq 10$)}, on est bien linéaire en la taille de $\varphi$.
		\end{proof}
		
		\begin{lemme}
			$T(\varphi)$ et $\varphi$ sont équisatisfiable.
		\end{lemme}
		\begin{proof}
			\begin{description}
				\item[$\Rightarrow$] Supposons que $\varphi$ est satisfiable, alors il existe une valuation $\nu$ telle que $\nu(\varphi) = \textsf{vrai}$. On pose $\nu'$ telle que pour tout $\psi \in SF(\varphi)$, $\nu'(a_{\varphi}) \nu(\varphi)$. Montrons par induction sur $\varphi$ que $\nu'(T(\varphi)) = \textsf{vrai}$. On pose l'hypothèse d'induction $\mathcal{H}_{\varphi} : "\nu(\varphi) = \textsf{vrai} \Rightarrow \nu'(\varphi) = \textsf{vrai}"$.
				\begin{description}
					\item[Cas de base: $\varphi \in V$] Supposons que $\nu(\varphi) = \textsf{vrai}$ et montrons que $\nu'(\varphi) = \textsf{vrai}$. Par définition de $T(\varphi)$, $T(\varphi) = a_{\varphi} \wedge \underset{\psi \in SF(\varphi) \setminus V}{\bigwedge} t(\psi) = a_{\varphi} \wedge \underset{\emptyset}{\bigwedge} t(\psi) = a_{\varphi}$ \textblue{(car $SF(\varphi) = a_{\varphi}$ et $SF(\varphi) \setminus V = \emptyset$)}. On en déduit que $\nu'(T(\varphi)) = \nu'(a_{\varphi}) = \textsf{vrai}$
					
					\item[Cas inductif: $\varphi = \neg \varphi_1$] Supposons que $\mathcal{H}_{\varphi_1}$ est vraie et montrons $\mathcal{H}_{\varphi}$. Par définition de $T(\varphi)$, $T(\varphi) = a_{\varphi} \wedge \underset{\psi \in SF(\varphi_1) \setminus V}{\bigwedge} t(\psi) = a_{\varphi} \wedge T(\varphi_1)$ \textblue{(car $\underset{\psi \in SF(\varphi_1) \setminus V}{\bigwedge} t(\psi) = T(\varphi_1)$)}. En l'évaluant avec $\nu'$, on en déduit que $\nu'(T(\varphi)) = \nu'(a_{\varphi}) \wedge \nu'(T(\varphi_1))$. Par hypothèse sur $a_{\varphi}$ et en appliquant les hypothèses d'induction, $\nu'(a_{\varphi}) = \nu'(T(\varphi_1)) = \textsf{vrai}$. Donc $\nu'(T(\varphi)) = \textsf{vrai}$.
					
					\item[Cas inductif: $\varphi = \varphi_1 \vee \varphi_2$] Supposons que $\mathcal{H}_{\varphi_1}$ et $\mathcal{H}_{\varphi_2}$ sont vraies et montrons $\mathcal{H}_{\varphi}$. Par définition de $T(\varphi)$, $T(\varphi) = a_{\varphi} \wedge \underset{\psi \in SF(\varphi_1) \setminus V}{\bigwedge} t(\psi) \wedge \underset{\psi \in SF(\varphi_2) \setminus V}{\bigwedge} t(\psi)= a_{\varphi} \wedge T(\varphi_1) \wedge T(\varphi_2)$ \textblue{(car $\underset{\psi \in SF(\varphi_i) \setminus V}{\bigwedge} t(\psi) = T(\varphi_i)$)}. En l'évaluant avec $\nu'$, on en déduit que $\nu'(T(\varphi)) = \nu'(a_{\varphi}) \wedge \nu'(T(\varphi_1)) \wedge \nu'(T(\varphi_2))$. Par hypothèse sur $a_{\varphi}$ et en appliquant les hypothèses d'induction, $\nu'(a_{\varphi}) = \nu'(T(\varphi_1)) = \nu'(T(\varphi_2)) = \textsf{vrai}$. Donc $\nu'(T(\varphi)) = \textsf{vrai}$.
				\end{description}
				D'ou $\mathcal{H}_{\varphi}$ pour toute formule $\varphi$ du calcul propositionnel.
				
				
				\item[$\Leftarrow$] Supposons que $T(\varphi)$ soit satisfiable : il existe une valuation $\nu$ tel que $\nu(T(\varphi)) = \textsf{vrai}$. On pose $\nu'$ telle que pour tout $p \in V$, $\nu'(p) = \nu(a_{\varphi})$. Alors, $\nu'(\varphi) = \textsf{vrai}$ car la variable associée à une sous-formule est équivalente à cette sous formule.
			\end{description}
		\end{proof}
	\end{proof}
	
	\begin{ex}
		On applique cette transformation à la formule $\varphi = (p \vee \neg q) \vee (\neg p)$. On obtient $T(\varphi) = a_{\varphi} \wedge t(\neg q) \wedge t(p \vee \neg q) \wedge t(\neg p) \wedge t((p \vee \neg q) \vee (\neg p))$. On conclut avec
		\begin{displaymath}
			\begin{array}{cl}
				\varphi = & a_{\varphi} \\
				& \wedge (a_{p} \vee a_{\neg q}) \wedge (\neg a_{p} \vee \neg a_{\neg q}) \\
				& \wedge (a_{q} \vee a_{\neg p}) \wedge (\neg a_{q} \vee \neg a_{\neg p}) \\
				& \wedge (a_{p \vee \neg q} \vee \neg a_p) \wedge (a_{p \vee \neg q} \vee \neg a_{\neg q}) \wedge (\neg a_{p \vee \neg q} \vee a_p \vee a_{\neg p}) \\
				& \wedge (a_{((p \vee \neg q) \vee (\neg p))} \vee \neg a_{(p \vee \neg q)}) \wedge (a_{((p \vee \neg q) \vee (\neg p))} \vee \neg a_{\neg q}) \wedge (\neg a_{((p \vee \neg q) \vee (\neg p))} \vee a_{(p \vee \neg q)} \vee a_{\neg p}) \\
			\end{array}
		\end{displaymath}
	\end{ex}

	\section{Équivalence sémantique et équisatisfiabilité}
	\input{./../Notions/LogiqueProp_equivalence.tex}
	
	\section{Formes normales}
	\input{./../Notions/LgiqueProp_formesNormales.tex}
	
	\section{Systèmes de connecteurs}
	\input{./../Notions/LogiqueProp_systConnecteur.tex}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}	
\end{document}