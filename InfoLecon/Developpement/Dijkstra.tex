\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{array}
\usepackage{tikz}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\newtheorem*{appli}{Application}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Algorithme de Dijkstra : terminaison, correction et complexité}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Cormen \cite[p.609]{Cormen}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 901 (Structure de données); 925 (Graphe); 926 (Analyse : complexité); 927 (Analyse : Correction); 931 (Schéma algorithmique).
		}}
	
	\section{Introduction}
	
	L'algorithme de Dijkstra est un grand classique pour calculer le plus court chemin dans un graphe à partir d'une origine unique. Pour la correction de cet algorithme, l'ensemble des poids doivent être positifs ou nuls. De plus, celle-ci se prouve à l'aide d'un invariant de boucle qui n'a rien de trivial. L'algorithme de Dijkstra demande l'implémentation d'une file de priorité (qui n'est pas une structure de données simple). On voit alors apparaître la jeu des structures de données dans le calcul de la complexité d'un algorithme.
	
	\titlebox{red}{\textred{\textbf{Remarques sur le développement}}}{
		\begin{enumerate}
			\item Présentation de l'algorithme.
			\item Preuve de sa terminaison.
			\item Preuve de sa correction.
			\item Étude de sa complexité.
		\end{enumerate}
	} 
	
	\section{Étude de l'algorithme de Dijkstra}
	
	\paragraph{Présentation de l'algorithme} 
	
	\textcolor{white}{a}
	
	\noindent\emph{Objectif}: chemin le plus court à origine unique. Pour $a \in V$, on veut calculer $d : V \to \{+ \infty\}$ tel que $\forall v \in V$, $d(v) = dist(s, v)$, où $s$ est la source. \textblue{(Bien implémenté, il y a de meilleurs performances de Bellman-Ford.)}
	
	\textcolor{white}{a}
	
	\noindent\emph{Hypothèse}: Soit $G = (S, A)$ un graphe orienté pondéré avec un poids positif ou nul $\left(\forall(u,v)\in E, w(u, v)\geq 0\right)$.
	
	\textcolor{white}{a}
	
	\noindent\emph{Principe}:  On choisi parmi tous les sommets celui dont la distance $dist$ est minimal \textblue{(le plus court chemin)} et on relâche les autres. C'est un algorithme glouton dont le résultat est optimal.
	
	\textcolor{white}{a}
	
	\noindent\emph{Notation}:  On note la fonction $dist : V \times V \to \N \cup \{+ \infty\}$ la longueur du plus court chemin entre $u$ et $v$.
	
	\noindent\begin{minipage}{.3\textwidth} 
		
		\begin{req}
			La file de priorité peut être remplacée par un tri topologique sur le graphe.
		\end{req}
		
		\paragraph{Terminaison}
		Si $F = \emptyset$, alors $E = S$. D'où la terminaison. On pose le variant suivant : $F = S \setminus E$.
		\begin{description}
			\item[Initialisation] $F = S$ donc $E = \emptyset$.
			\item[Hérédité] Extraire un sommet de $S \setminus E$ pour l'ajouter à $E$ : ok
		\end{description}
		
		
	\end{minipage} \hfill
	\begin{minipage}{.65\textwidth}
		\begin{algorithm}[H]
			Entrée : $G$ le graphe; $w$ le poids; $s$ la source
			\begin{algorithmic}[1] 
				\Function{\textsc{Dijkstra}}{$G, w, s$} 
				\For{tout $v \in G.S$} \Comment{Source initiale}
				\State $v.d \gets \infty$ \Comment{Majore le poids jusqu'à l'origine}
				\State $v.\pi \gets NIL$ \Comment{Prédécesseur}
				\EndFor
				\State $s.d = 0$ \Comment{$s$ est la source}
				\State $E \gets \emptyset$
				\State $F = G.S$ \Comment{$F$ est une file de priorité}
				\While{$F \neq \emptyset$} 
				\State $u \gets \textsf{Extraire-Min}(F)$
				\State $E \gets E \cup \{u\}$
				\For{tout $v \in Adj[u]$}
				\If{$v.d > u.d + w(u,v)$}
				\State $v.d = u.d + w(u,v)$
				\State $v.\pi = u$
				\EndIf
				\EndFor
				\EndWhile
				\EndFunction
			\end{algorithmic}
			\caption{Algorithme de Dijkstra calculant le plus court chemin à partir d'un sommet initial.}
			\label{algo:Dijkstra}
		\end{algorithm}
	\end{minipage} 
	
	
	
	\paragraph{Correction}
	On pose l'invariant de boucle : "à chaque itération de la boucle \textsf{while}, $\forall v \in E$, $v.d = dist(s, v)$. On montre par induction cet invariant.
	\begin{description}
		\item[Initialisation] Comme $E = \emptyset$, l'invariant est vrai.
			
		\item[Conservation] Montrons qu'à chaque itération "$u.d \neq dist(s, u)$ pour le sommet ajouté à $E$. Par l'absurde, supposons par l'absurde que $u$ est le premier sommet pour lequel $u.d \neq dist(s, u)$ quand on ajoute à $E$.
		\begin{itemize}[label=$\to$]
			\item $u \neq s$ car $u.d =0$ et $dist(s, s) = 0$. Donc $E \neq \emptyset$.
			
			\item Il existe un chemin de $s$ à $u$ sinon $u.d = +\emptyset = dist(u,v)$ \textblue{(contradiction avec l'hypothèse)}.
			
			\item Il existe un plus court chemin $p$ de $s$ à $u$ où $s \in E$ et $u \in S \setminus E$ \textblue{(car existence d'un chemin)}. On peut alors décomposé $p$ de la manière suivante
			\begin{displaymath}
				s \overset{p_1}{\rightsquigarrow} x \to y \overset{p_2}{\rightsquigarrow} u
			\end{displaymath}
			\textblue{($p_1$ ou $p_2$ peut ne pas avoir d'arc)} où $y$ est le premier sommet appartenant à $S \setminus E$ et $x$ sont prédécesseur.
				
			$y.d = dist(s, y)$ quand $u$ est ajouté à $E$. Comme $x \in E$, alors $x.d = dist(s, x)$ si $s$ lors de son ajout à $E$. Dans ce cas, l'arc $(x,y)$ est relâché à cet instant. Donc $y.d = dist(s,y)$ \textblue{(convergence)}.
				
			\item On souhaite faire apparaître la contradiction: $dist(s, y) \leq dist(s, u)$.
				
			$y.d = dist(s, y) \leq dist(s, u) \leq u.d$ \textblue{(car $y$ arrive avant $u$ sur le chemin le plus court et $dist$ est minimal)}.
				
			Mais comme $u,y \in S \setminus E$ lors du choix de $u$, $u.d \leq y.d$ \textblue{(car choisi par $F$)}. Donc, on a l'égalité (contradiction).
		\end{itemize}
	\end{description}
	D'où la correction.
		
	\paragraph{Complexité} Supposons que le graphe est représenter par une liste d'adjacence. Sa complexité est alors
	\begin{displaymath}
	cout(\textsf{Dikjstra}) \leq |S|(cout(\textsf{Inserer}) + cout(\textsf{Extraire}) + \underbrace{|A|(\textsf{Diminuer-Cle}}_{\text{analyse par agrégat}})
	\end{displaymath}
	
	Sa complexité dépend de la structure de données implémentant $E$.
	\begin{itemize}
		\item tableau où on stocke $v.d$ pour chaque sommet (non trié).
		\begin{displaymath}
		\begin{tabular}{l}
		cout(\textsf{Inserer}) = 1 = cout(\textsf{Diminuer-Cle}) \\
		cout(\textsf{Extraire-Min}) = O(S) \textblue{\text{ (parcourir tout le tableau)}}\\
		\end{tabular}
		\end{displaymath}
		Dans le cas d'un graphe peu dense $A = O(S^2/ \log S)$.
		
		\item tas binaire
		\begin{description}
			\item[cout(\textsf{Inserer})] $O(S)$ pour construire le tas entier \textblue{($O(1)$ est amortie)}
			\item[cout(\textsf{Diminuer-Cle})] $O(\log S)$
			\item[cout(\textsf{Extraire-Min})] $O(\log S)$
		\end{description}
		$O((S+A)\log S) = O(A\log S)$ si tous les sommets accessibles depuis l'origine.
		
		\item \textblue{Tas Fibonacci dans le cas optimal (motive l'existence de ce tas)
			\begin{description}
				\item[cout(\textsf{Diminuer-Cle})] $O(1)$ 
				\item[cout(\textsf{Extraire-Min})] $O(\log S)$ pour $S$
			\end{description}
			$O(S \log S + A)$}
	\end{itemize}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}