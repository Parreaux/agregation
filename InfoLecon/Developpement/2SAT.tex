\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{array}
\usepackage{tikz}
\usepackage{multirow}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}


\parskip=0\baselineskip %0.5 d'habitude
\usetikzlibrary{automata,positioning,shadows}
\usetikzlibrary{calc, shapes, backgrounds,arrows}
\usetikzlibrary{chains, decorations.pathmorphing}
\usetikzlibrary{positioning,decorations.pathreplacing}

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
} 

\begin{document}
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\newtheorem*{appli}{Application}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Le problème 2SAT}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Papadimitriou \cite[p.184; 398]{Papadimitriou}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}}  915 (Classes de complexité); 916 (Logique propositionnelle).
		}}
	
	\section{Introduction}
	
	Le problème SAT est dans NP, il est même NP-complet (par le théorème de Cook). Une manière de calculer la satisfiabilité d'une formule du calcul propositionnelle est d'en réduire son expressivité en considérant 2SAT (on restreint les instances du problème).Ce nouveau problème portant sur les formule 2CNF est dans P. On a même mieux que cela, 2SAT est NL-complet.
	
	\begin{definition}
		On définit le problème \textsc{2SAT} sur les formules propositionnelles.
		
		\noindent\begin{tabular}{rl}
			Problème & \textsc{2SAT} \\
			\textbf{entrée}: & $\varphi$ une formule sous forme normale conjonctive avec deux littéraux par clause \\
			\textbf{sortie}: & Oui si $\varphi$ est satisfiable; non sinon \\
		\end{tabular}
	\end{definition}
	
	\titlebox{red}{\textred{\textbf{Remarques sur le développement}}}{
		Le développement établit que 2SAT est dans P. Si le temps (et le niveau) le permet on peut aller jusqu'à montrer que ce problème est NL-complet.
		\begin{itemize}
			\item Problème 2SAT est dans P.
			\begin{enumerate}
				\item Écriture de l'algorithme.
				\item Preuve de sa correction.
				\item Preuve de sa complexité.
			\end{enumerate}
			\item Problème 2SAT est NL-complet.
			\begin{enumerate}
				\item 2SAT est NL: accessibilité dans un graphe.
				\item 2SAT est NL-dur: réduction via la preuve dans P du problème d'accessibilité dans un graphe.
			\end{enumerate}
		\end{itemize}
	}
	
	\section{Le problème \textsc{2SAT} est dans $P$}
	On commence par montrer que le problème \textsc{2SAT} est dans $P$. Le graphe d'implication est une structure de données nous permettant de calculer une valuation pour la formule. C'est grâce à ce graphe que nous avons cette complexité pour le problème \textsf{2SAT}.
	
	\begin{definition}
		Soit $\varphi$ une formule sous forme 2CNF. Le graphe d'implication $G_{\varphi}$ est le graphe où
		\begin{itemize}
			\item les sommets sont les variables propositionnelles de $\varphi$ et leurs négations \textblue{(les littéraux de $\varphi$)};
			\item $(\alpha, \beta) \in A$ si $\alpha \to \beta$ est une clause de $\varphi$ \textblue{(donc $\neg \alpha \vee \beta$ est la clause que nous considérons)}.
		\end{itemize}
	\end{definition}
	
	\begin{theo}
		Soit $\varphi$ une formule du calcul propositionnelle. $\varphi$ est insatisfiable si et seulement s'il existe une variable $x$ telle qu'il existe un chemin de $x$ à $\neg x$ et un chemine de $\neg x$ à $x$ dans le graphe d'implication $G_{\varphi}$.
	\end{theo}
	\begin{proof}
		\begin{description}
			\item[$\Leftarrow$] On suppose que ces deux chemins existent pour $x$. Raisonnons par l'absurde : supposons que $\varphi$ est satisfiable pour une valuation $\nu$. On suppose que $\nu(x) = \textsf{vrai}$ \textblue{(analogue dans le cas $\nu(x) = \textblue{faux}$)}.
			\begin{itemize}[label=$\to$]
				\item $\nu$ est une valuation et $\nu(x) = \textsf{vrai}$ : $\nu(\neg x) = \textsf{faux}$.
				\item Existence d'un chemin de $x$ à $\neg x$ : $\exists (\alpha, \beta)$ un arc tel que $\nu(\alpha) = \textsf{vrai}$ et $\nu(\beta) = \textsf{faux}$.
				\item $(\alpha, \beta) \in A$ : $(\alpha, \beta)$ est une clause $\neg \alpha \vee \beta$.
				\item $\nu(\neg \alpha \vee \beta) = \textsf{faux}$ : contradiction.
			\end{itemize}
			\item[$\Rightarrow$] On suppose qu'il n'existe pas de tels chemin pour toute variable $x$. Construisons une valuation pour la formule $\varphi$ tel que pour tout nœuds $G(\varphi)$ il n'existe pas d'arc allant d'une variable mise à \textsf{vrai} à \textsf{faux}. L'algorithme~\ref{algo:DconstruireValuation} nous permet de construire cette valuation.
			
			\noindent\begin{minipage}{.46\textwidth}
				\begin{algorithm}[H]
					Entrée : $G$ le graphe d'implication d'une formule 2CNF
					\begin{algorithmic}[1] 
						\Function{\textsf{ConstruireValuation}}{$G$} 
						\While{il existe une variable non-assignée $x$ \textblue{telle que le chemin de $x$ à $\neg x$ n'existe pas}}
						\State Choisir $x$ vérifiant ces conditions
						\State $\nu(x) \gets \textsf{vrai}$
						\For{tout $\alpha \in \mathrm{Adj}(x)$}
						\State $\nu(\alpha) \gets \textsf{vrai}$
						\State $\nu(\neg\alpha) \gets \textsf{faux}$
						\EndFor
						\EndWhile
						\EndFunction
					\end{algorithmic}
					\textblue{(non nécessaire par hypothèse)}
					\caption{Algorithme permettant de construire une valuation pour une formule 2CNF via son graphe d'implication.}
					\label{algo:DconstruireValuation}
				\end{algorithm}
			\end{minipage} \hfill
			\begin{minipage}{.46\textwidth}
				\begin{itemize}[label=$\to$]
					\item Le corps de la boucle \textsf{while} est bien défini car s'il existe un chemin de $\alpha$ à $\beta$ et un chemin de $\alpha$ à $\neg\beta$, il existe un chemin de $\alpha$ à $\neg\alpha$. \textblue{(Par symétrie : $(\alpha, \beta) \in A \Leftrightarrow (\neg\alpha, \neg\beta) \in A$, puis par récurrence sur la longueur du chemin.)}
					\item S'il existe un chemin de $\alpha$ à $\beta$ tel que $\nu(\beta)= \textsf{faux}$ à une étape précédente alors $\nu(\alpha)= \textsf{faux}$ à cette étape. Par symétrie $(\alpha, \beta) \in A \Leftrightarrow (\neg\alpha, \neg\beta) \in A$. Si $\nu(\beta) = \textsf{faux}$, $\nu(\neg\beta) = \textsf{vrai}$ à  l'étape précédente. Comme, on a traité $\neg\beta$, $\nu(\neg\alpha) = \textsf{vrai}$ et $\nu(\alpha)= \textsf{faux}$.
					\item Lorsque tous les nœuds ont une valuation, il n'existe pas d'arcs tels que $\textsf{vrai} \to \textsf{faux}$.
					\begin{itemize}
						\item assignation : $\textsf{vrai} \to \textsf{vrai}$
						\item $\beta$ soit mis à $\textsf{vrai}$ car $x$ 
					\end{itemize}
				\end{itemize}
			\end{minipage} 
		\end{description}
	\end{proof}
	
	\begin{prop}
		Le problème \textsf{2SAT} est dans P.
	\end{prop}
	\begin{proof}
		L'algorithme permettant de calculer la valuation d'une telle formule utilise un graphe : le graphe d'implication de la formule.
		
		
		
		
		
		On considère l'algorithme \textsf{Sat2SAT} qui en temps polynomial \textblue{(on est même en temps linéaire)} donne la satisfiabilité d'une formule 2CNF \textblue{(on résout 2SAT)}.
		
		\begin{lemme}
			L'algorithme \textsf{Sat2SAT} appliqué à la formule $\varphi$ sous forme 2CNF (Algorithme~\ref{algo:sat2sat}) retourne \textsf{satisfiable} si et seulement si $\varphi$ est satisfiable.
		\end{lemme}
		\begin{proof}
			\begin{description}
				\item[$\Leftarrow$] Soit $V$ une valuation telle que $V \models \varphi$. Par l'absurde, on suppose qu'il existe $p$ tel que $p$ et $\neg p$ soient dans la même composante fortement connexe. Sans perte de généralité, supposons que $V \models p$. 
				\begin{itemize}[label=$\to$]
					\item Il existe un chemine de $p$ à $\neg p$, noté $(l_0, \dots, l_n)$ dans $G_{\varphi}$ \textblue{($p$ et $\neg p$ sont dans la même composante fortement connexe)}.
					
					\item $V \models l_i \to l_{i+1}$ \textblue{(par définition des arcs dans le graphe des implications)}.
				\end{itemize}
				On montre par récurrence sur $i \in \N$ tel que $V \models l_i \forall i$. Donc $V \models \neg p$. Contradiction.
				
				\item[$\Rightarrow$] Supposons que l'algorithme \textsf{Sat2SAT} renvoie \textsf{satisfiable}. On considère l'algorithme \textsf{ConstruireValuation} (Algorithme~\ref{algo:DconstruireValuation}). On pose $V = \textsf{ConstruireValuation}(G)$.
				\begin{itemize}[label=$\to$]
					\item L'opération "$V$ de tous les  littéraux de $C$: \textsf{vrai}" est bien définie. Pour toute variable $p$, $p$ et $\neg p$ ne sont pas dans la même composante fortement connexe \textblue{(l'algorithme \textsf{Sat2SAT} renvoie \textsf{satisfiable})} donc on ne donne une unique valeur à $p$ \textblue{($\neg p$ n'apparaît pas dans la clause et sa valuation n'est pas mise à vrai)}.
					\item $V$ est une valuation totale sur les variables propositionnelles apparaissant dans $\varphi$.
					\begin{itemize}[label=$\bullet$]
						\item On considère tous les sommets de $G_{\varphi}$
						\item $V \models \varphi$ On considère une clause $l_1 \to l_2$ de $\varphi$. Montrons que $V \models l_1 \to l_2$. Supposons que $V \models l_1$. Ainsi, $l_1$ a été mis à vrai dans \textsf{ConstruireValuation}: on avait $l_1 \in C_1$ où $C_1$ est une composante fortement connexe dans un certain graphe $G_1$:
						\begin{itemize}
							\item si $l_1$ et $l_2$ sont dans une composante fortement connexe de $G_{\varphi}$, alors $l_2$ est mis à \textsf{vrai} comme $l_1$ \textblue{(en même temps puisqu'il sont dans la même composante fortement connexe)}.
							
							\item sinon, dans $G_{\varphi}$, $l_1$ et $l_2$ sont dans deux composantes fortement connexes distinctes et $l_2$ est dans une composante fortement connexe qui sera final avant $l_1$ \textblue{(puisque celle de $l_1$ admet une transition allant à celle de $l_2$, l'arc ($l_1$, $l_2$))}. Donc au moment où on affecte la valeur \textsf{vrai} à $l_1$, $l_2$ a déjà été supprimé \textblue{(car la composante fortement connexe de $l_1$ ne peut pas être finale tant que celle de $l_2$ n'a pas été supprimée)}.
							
							\textblue{(Fin de la preuve à revoir : il faut également prouvé que nous avons mis $l_2$ à vrai lors de sa suppression (qu'il a bien été supprimé car il était dans une composante finale et non car sa négation a été affecté avec une valeur...))}
						\end{itemize}
					\end{itemize}
				\end{itemize}
			\end{description}
		\end{proof}
		Donc le problème \textsc{2SAT} est dans $P$.
	\end{proof}
	
	\section{2SAT est NL-complet}
	On peut même montrer mieux que cela: on peut montrer que le problème \textsc{2SAT} est un problème $NL$-complet. 
	
	\begin{theo}
		Le problème \textsc{2SAT} est un problème $NL$-complet.
	\end{theo}
	\begin{proof}
		\begin{description}
			\item[\textsc{2SAT} est dans $NL$] On simule le problème d'accessibilité dans le graphe d'implication à l'aide des clauses \textblue{(on n'a pas besoin de la construire)} pour vérifié si une variable et sa négation sont dans la même composante fortement connexe.
			
			\item[\textsc{2SAT} est $NL$-dur] On réduit \textsc{Non-accessibilité} à \textsc{2SAT} \textblue{($NL = co-NL$ et \textsc{Accessible} est $NL$-complet)}. On suppose que $G$ est un graphe acyclique \textblue{(ne change rien à se complexité)}.
			\begin{minipage}{.3\textwidth}
				$\forall (x, y) \in A$, on construit une clause $(\neg x \vee y)$ dans $\varphi$:
				\begin{itemize}
					\item une variable par noeud du graphe;
					\item deux clauses supplémentaires: $s$ et $\neq t$ pour le noeud source et cible.
				\end{itemize}
			\end{minipage} \hfill
			\begin{minipage}{.5\textwidth}
				\begin{figure}[H]
					\centering
					\begin{tikzpicture}[auto, thick, >=latex]
					\draw
					% Drawing the blocks of first filter :
					node at (-1.2,0) [rectangle,draw=none, name=input1] {} 
					node at (1.5,0) [rectangle,draw=black] (red) {Réduction $tr$}
					node at (4,0) [rectangle,draw=none] (tr) {$\varphi$}
					node at (5.75,0) [rectangle,draw=black] (B) {\textsc{2SAT}}
					node at (7,0) [rectangle,draw=none] (fin) {};
					% Joining blocks. 
					% Commands \draw with options like [->] must be written individually
					\draw[->](input1) -- node[near start]{$G, s, t$}(red);
					\draw[-](red) -- node {} (tr);
					\draw[->](tr) -- node {} (B);
					\draw[->](B) -- node {} (fin);
					% Boxing and labelling noise shapers
					\draw [color=gray,thick](-0.25,-1) rectangle (6.5,1.5);
					\node at (-0.25,1.25) [above=5mm, right=0mm] {\textsc{Accessibilité}};
					\end{tikzpicture}
					\caption{Schéma du principe de réduction du problème \textsc{Non-accessibilité} au problème \textsc{2SAT}.}
					\label{fig:Red}	
				\end{figure}
			\end{minipage}
		\end{description}
		$\varphi$ est satisfiable si et seulement s'il n'existe pas de chemin de $s$ à $t$.
		\begin{description}
			\item[$\Rightarrow$] Supposons que $\varphi$ est satisfiable. Raisonnons par l'absurde et supposons qu'il existe un chemin de $s$ à $t$: $s, x_1, \dots, x_n, t$. Alors $\nu(s) = \nu(x_i) = \nu(t)$ pour tout $i \in \{1, \dots, n\}$. Contradiction.
			
			\begin{minipage}{.46\textwidth}
				\begin{figure}[H]
					\begin{tikzpicture}[font=\sffamily]
					% Setup the style for the states
					\tikzset{node style/.style={state, 
							minimum width=0.5cm,
							line width=.25mm,
							fill=white,circle}}
					
					% Draw the states
					\node[node style] at (-1.5,0) (-1) {$s$};
					\node[node style] at (0,0) (0) {$x$};
					\node[node style] at (1.5,0) (1) {$y$};
					\node[node style] at (3,0) (2) {$z$};
					\node[node style] at (4.5,0) (3) {$t$};
					
					% Connect the states with arrows
					\draw[every loop,auto=right,line width=.25mm,
					>=latex]
					(-1) edge [] node {} (0)
					(0) edge [] node {} (1)
					(1) edge [] node {} (2)
					(2) edge [] node {} (3);
					\end{tikzpicture}
					\caption{Exemple d'un chemin de taille $3$ dans un graphe.}
				\end{figure}
			\end{minipage} \hfill
			\begin{minipage}{.4\textwidth}
				Exemple de la formule correspondant à ce chemin : $s \wedge (\neg s \vee x) \wedge (\neg x \vee y) \wedge (\neg y \vee z) \wedge (\neg z \vee t) \wedge \neg t$
			\end{minipage}
			
			\item[$\Leftarrow$] Raisonnons par contraposée: supposons qu'il existe un chemin de $s$ à $t$. Alors comme précédemment, $\nu(s) = \nu(x_i) = \nu(t)$ pour tout $i \in \{1, \dots, n\}$. On en déduit que $\varphi$ est insatisfiable.
		\end{description}
		De plus cette réduction s'effectue en place constante puisque le graphe encode la formule (d'où en log space). D'où le résultat de dureté.
	\end{proof}
	
	
	\section{Technique de preuve : la réduction}
	\input{./../Notions/Indecidabilite_reduction.tex}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
		%\printbibliography

	
\end{document}