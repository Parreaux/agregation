\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{array}
\usepackage{tikz}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
		};
		%\draw[color=blue] (box.north west)--(box.north east);
		\node[title] at (box.north) {#2};
		\end{tikzpicture}\bigskip%
}

\begin{document}
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\newtheorem*{appli}{Application}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Les fonctions $\mu$-récursives sont équivalentes aux fonctions $\lambda$-définissables}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Hanking \cite[p.88]{Hankin}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçon où on présente le développement:}}  929 (Lambda-calcul).
			
			\textblue{\emph{Leçons où on peut l'évoquer}}: 912 (Fonctions récursives); 913 (Machines de Turing); 914 (Décidabilité, indécidabilité).
		}}
	
	\section{Introduction}
	
	Le $\lambda$-calcul est un modèle aussi expressif que les machines de Turing ou encore que les fonctions $\mu$-récursives. Il fait alors partis des modèles vérifiant la thèse de Church et permettant de délimiter la frontière du calculable. Dans ce document, on ne s'intéresse à l'équivalence entre les fonctions $\lambda$-définissable et les fonctions $\mu$-récursives (pour le $\lambda$-calcul c'est l'équivalence la plus facile).
	
	\titlebox{red}{Remarques sur le développement}{
		Le développement se fait par induction structurelle des fonctions $\mu$-récursives. Il faut faire attention car il faut bien connaître les différentes représentations de ces dites fonctions.
	}
	
	\section{Les fonctions $\mu$-récursives sont $\lambda$-définissables}
	
	On utilise la représentation des entiers de Barendregt pour coder nos entiers.
	
	\begin{theo}
		Les fonctions $\mu$-récursives sont $\lambda$-définissables
	\end{theo}
	\begin{proof}
		Soit $\varphi$ une fonction $\mu$-récursive. On raisonne par induction sur $\varphi$.
		\begin{description}
			\item[Cas $\varphi = \mathbb{O}$] $\left[\varphi\right] = \lambda x. \left[0\right]_B$.
			\item[Cas $\varphi = \pi_i^p$] $\left[\varphi\right] = \lambda x_0 \dots x_p. x_i$.
			\item[Cas $\varphi = \textsf{succ}$] $\left[\varphi\right] = \lambda x. \left< F, x \right>$.
			\item[Cas de la composition] Dans ce cas, on a $\varphi = \chi(\psi_1, \dots, \psi_k)$. Soit $G$ le représentant de $\chi$ et $F_i$ ceux des $\psi_i$ \textblue{(existent par induction)}. Alors, $\left[\varphi\right] = \lambda x. G\left(F_1 x, \dots, F_k x\right)$.
			\item[Cas de la récursivité] Dans ce cas, on a $\varphi(0, n) = \psi(n)$ et $\varphi(k + 1, n) = \chi(\varphi(k, n), k, n)$. Soit $G$ et $H$ les représentants de $\psi$ et $\chi$ \textblue{(existent par induction)}. On note $C =$ if \textsc{Zero} $x$ then $Gy$  else $H \left(b (\textsf{pred } x) y\right) (\textsf{pred } x) y$. Alors $\left[\varphi\right] = \Theta\left(\lambda fxy. C\right)$. Par récurrence sur $k$, montrons que $H_k$: "$\left[\varphi\right]\left[k\right]\left[n\right] =_{\beta} \left[\varphi(k, n)\right]$".
			\begin{description}
				\item[Initialisation] Si $k = 0$, alors $\left[\varphi\right]\left[k\right]\left[n\right] \to_{\beta}^{*} G\left[n\right] =_{\beta} \left[\chi(n)\right] =_{\beta} \left[\varphi(0, n)\right]$.
				\item[Hérédité] Soit $k \in \N$ tel qu'on ait $H_k$. Montrons que nous avons $H_{k + 1}$.
				\begin{displaymath}
				\begin{array}{ccll}
				\left[\varphi\right]\left[k + 1\right]\left[n\right] & \to_{\beta}^{*} &  H\left(\left[\varphi\right]\left(\textsf{pred} \left[k+1\right]\left[n\right]\right)\right) \left(\textsf{pred} \left[k+1\right]\right)\left[n\right] & \textblue{\text{(Application de la $\beta$-rédution)}}\\
				& \to_{\beta} & H\left(\left[\varphi\right]\left[k\right]\left[n\right]\right) \left[k\right]\left[n\right] & \textblue{\text{(Définition de \textsf{pred})}}\\
				& =_{\beta} & H\left[\varphi(k,n)\right) \left[k\right]\left[n\right] & \textblue{\text{(Par $H_k$)}}\\
				& =_{\beta} & \left[\psi\left( \varphi(k,n), k, n\right)\right] & \textblue{\text{(Par induction)}}\\
				& =_{\beta} & \varphi(k+1,n) & \textblue{\text{(Par définition)}}\\
				\end{array}
				\end{displaymath}
			\end{description}
			\item[Cas de la minimisation] Dans ce cas, $\varphi(n) = \mu_k (\chi(k, n) = 0)$. Soit $P$ un $\lambda$-terme et notons
			\begin{displaymath}
			\begin{array}{l}
			H_P = \Theta\left(\lambda hz. \text{if } Px \text{ then } z \text{ else } h\left(\textsf{succ } z\right)\right) \\
			\mu_P = H_P\left[0\right] \\
			\end{array}
			\end{displaymath}
			Supposons que pour tout $n \in \N$, $P\left[n\right]_B =_{\beta} T$ ou $P\left[n\right]_B = F$ et qu'il existe $n \in \N$ tel que $P\left[n\right]_B = T$. Montrons que si $\mu_n(P\left[n\right]_B = T)$ alors $\mu P =_{\beta} \left[n\right]$. En effet, si $k < n$,
			\begin{displaymath}
			H_P\left[k\right] \to_{\beta}^{*} \text{ if } P\left[k\right] \text{ then } \left[k\right] \text{ else } H_P\left(\textsf{succ } \left[k\right]\right) \to_{\beta}^{*} H_P\left[k + 1\right]
			\end{displaymath}
			Donc par récurrence, $\mu_P =_{\beta} \left[n\right]$ donc si $G$ est un représentant de $\chi$ \textblue{(existe par induction)}, on pose $\left[\varphi\right] = \lambda y. \mu\left(\lambda x. Gxy\right)$.
		\end{description}
	\end{proof}
	
	
	\section{Les fonctions $\lambda$-définissables sont $\mu$-récursives}
	
	La réciproque qui nous permet de montrer que le $\lambda$-calcul est aussi expressif que les fonctions $\mu$-récursives est plus délicat. Il demande à montrer que le graphe de la fonction $\lambda$-définissable que nous considérons soit récursivement énumérable. Puis grâce à la caractérisation de RE via les fonctions $\mu$-récursives on obtient que ce graphe est l'image d'une fonction $\mu$-récursive \textblue{(cette preuve n'est pas constructive)}.
	
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
	
\end{document}