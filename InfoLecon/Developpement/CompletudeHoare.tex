\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%
\usepackage{tikz}

\usepackage{array}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
		};
		%\draw[color=blue] (box.north west)--(box.north east);
		\node[title] at (box.north) {#2};
		\end{tikzpicture}\bigskip%
} 

\begin{document}
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem{lemme}{Lemme}
	\newtheorem*{appli}{Application}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Complétude partielle de la logique de Hoare}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Nielson $\&$ Nielson \cite[p.223]{NielsonNielson}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 927 (Analyse: terminaison et correction); 930 (Sémantique).
		}}
	
	\section{Introduction}
	La logique de Hoare est une sémantique axiomatique. Elle nous permet d'automatiser les preuves de programmes et notamment leur correction. En effet, deux versions de cette logique existe : la logique partielle et la logique complète. La logique partielle que nous utilisons ici nous permet de garantir les propriétés sur un programme sous l'hypothèse que celui-ci termine. Autrement dit, elle ne nous permet pas de prouver la terminaison des algorithmes. La logique complète, elle prouve égalament la correction de notre programme (nous ne l'évoquons pas ici).
	
	La logique partielle de Hoare est complète et correcte. Ici, on montre que la complétude de la logique de Hoare. Nous rappelons le système d'inférence de la logique partielle de Hoare. Ensuite, on montrera la complétude partielle, puis la correction.
	
	\titlebox{red}{\textred{\textbf{Remarques sur le développement}}}{
		Ce développement est long, il faut alors faire des choix dans les propriétés démontrées. De plus, il demande une bonne compréhension de la logique de Hoare ainsi que de la sémantique à grands pas.
		\begin{enumerate}
			\item Présentation des règles de la logique et de la sémantique \textblue{(à écrire dans un coin du tableau)}.
			\item Preuve du théorème par induction \textblue{(on ne rédige pas les cas de l'affectation et de la conditionnelle)}.
		\end{enumerate}
	}
	
	
	\section{Logique de Hoare partielle}

	\subsection{Présentation de la logique}
	\input{./../Notions/Semantique_Hoare.tex}
	
	\subsection{Correction de la logique de Hoare partielle}
	\input{./../Notions/Semantique_HoareCorrection.tex}
	
	\section{Sémantique opérationnelle à grands pas}
	\input{./../Notions/Semantique_GrandsPas.tex}
	
	\section{La complétude de la logique de Hoare partielle}
	Avant de définir la complétude partielle (on ne garantie les propriétés que si le programme termine (c'est une des hypothèses)), on n'a besoin d'un prédicat particulier : le weakest liberal precondition \textsf{wlp}.
	
	\begin{definition}
		Soit $S$ une instruction du langage IMP et $Q$ un prédicat:
		\begin{displaymath}
		\textsf{wlp}(S, Q) = tt \text{ si et seulement si } \forall s' \in State \text{, si } \underset{\textblue{\text{sémantique à grands pas}}}{\left< S, s\right> \to s'} \text{ alors } Qs' = tt
		\end{displaymath}
	\end{definition}
	
	\begin{lemme}
		Pour toute instruction du langage IMP et prédicat $Q$, on a
		\begin{enumerate}
			\item $\models_p \{\textsf{wlp}(S, Q)\}~ S~ \{Q\}$.
			\item Si $\models_p \{P\}~ S~ \{Q\}$ alors $P \Rightarrow \textsf{wlp}(S, Q)$.
		\end{enumerate}
		\textblue{$\textsf{wlp}(S, Q)$ est alors la plus faible précondition pour $S$ et $Q$.}
		\label{lem:wlp}
	\end{lemme}
	\begin{proof}
		\begin{enumerate}
			\item Soient $s, s' \in State$ tels que $\left< S, s\right> \to s'$ et $\textsf{wlp}(S, Q)s = tt$. Par définition de \textsf{wlp}, $Qs = tt$. Donc, $\models_p \{\textsf{wlp}(S, Q)\}~ S~ \{Q\}$.
			
			\item Supposons que $\{P\} ~S~ \{Q\}$ et $s \in State$ tel que $Ps = tt$. Si $\left< S, s\right> \to s'$ alors $Qs' = tt$ \textblue{(par $\models_p \{P\}~ S~ \{Q\}$)}. Donc, $\textsf{wlp}(S, Q)s = tt$ \textblue{(par définition)}.
		\end{enumerate}
	\end{proof}
	
	\textblue{\begin{req}
			Il existe la strongest possible postcondition: $\textsf{sp}(P, S)s' = tt$ si et seulement si $\exists s$ tel que $\left< S, s\right> \to s'$ et $Ps = tt$. On peut également prouver la complétude partielle avec cette postcondition.
		\end{req}}
	
	\begin{theo}
		La logique de Hoare partielle telle que l'on a définie est complète. Pour tout formule partiellement correcte $\{P\}~S~\{Q\}$, on a: $\models_p \{P\}~S~\{Q\} \text{ implique } \vdash_p \{P\}~S~\{Q\}$.
	\end{theo}
	\begin{proof} 
	Il suffit de montrer que nous pouvons inférer pour toute instruction $S$ et toute postcondition $Q$, $\vdash_p \{\textsf{wlp}(S, Q)\}~S~\{Q\}$. En effet, supposons que $\vdash_p \{\textsf{wlp}(S, Q)\}~S~\{Q\}$ et $\models_p \{P\}~S~\{Q\}$. Par le lemme~\ref{lem:wlp}, on a $P \Rightarrow \textsf{wlp}(S, Q)$. En appliquant la règle $[\textsc{cons}_p]$ avec $P \Rightarrow \textsf{wlp}(S, Q)$, on a $\vdash_p \{P\}~S~\{Q\}$.
	
	Montrons alors que pour toute instruction $S$ et tout prédicat $Q$, $\vdash_p \{\textsf{wlp}(S, Q)\}~S~\{Q\}$. On raisonne par induction structurelle sur les instructions $S$. 
	\begin{description}
		\item[Cas du $skip$] Montrons que $\textsf{wlp}(S, Q) \Rightarrow Q$ \textblue{(on peut également montrer que $\textsf{wlp}(S, Q) = Q$ mais on a besoin que de l'implication dans ce développement)}. Soit $s \in State$ tel que $\textsf{wlp}(S, Q)_s = tt$. Par définition, pour tout $s'$ tel que $\left< S, s\right> \to s'$ alors $Qs' = tt$. Par application de la règle $[skip_{NS}]$ de la sémantique à grands pas, on a $s = s'$. Donc $Qs = tt$. Comme $\textsf{wlp}(S, Q) = Q$, en appliquant la règle $[skip_p]$, on a $\vdash_p \{\textsf{wlp}(S, Q)\}~ skip~ \{Q\}$. 
		
		\item[Cas $x := a$] Analogue car $wlp(S, Q) \Rightarrow Q[x := a]$.
		
		
		\item[Cas de la composition] Soit $Q$ un prédicat.
		
		En appliquant les hypothèses d'induction à $S_1$ et $S_2$, on obtient: $\vdash_p \{\textsf{wlp}(S_2, Q)\}~S_2~\{Q\}$ et $\vdash_p \{\textsf{wlp}(S_1,\textsf{wlp}(S_2, Q))\}~S_1~\{\textsf{wlp}(S_2, Q)\}$ \textblue{($\textsf{wlp}(S_2, Q)$ est légal car $Q$ est un prédicat quelconque dans l'hypothèse d'induction)}. En appliquant la règle de la composition $[comp_p]$, on obtient $\vdash_p \{\textsf{wlp}(S_1,\textsf{wlp}(S_2, Q))\}~S_1;S_2~\{Q\}$.
		
		Montrons que $\textsf{wlp}(S_1; S_2, Q) \Rightarrow \textsf{wlp}(S_1,\textsf{wlp}(S_2, Q))$ \textblue{(on veut appliquer $[comp_p]$)}. On suppose qu'il existe $s \in State$ tel que $\textsf{wlp}(S_1; S_2, Q)s = tt$. Montrons que $\textsf{wlp}(S_1,\textsf{wlp}(S_2, Q))s = tt$. Soit $s' \in State$ tel que $\left< S_1, s \right> \to s'$ et montrons que $\textsf{wlp}(S_2, Q)s' =tt$. Soit $s'' \in State$ tel que $\left< S_2, s' \right> \to s''$ et montrons que $Qs'' =tt$. Par la règle $[comp_{NS}]$ de la sémantique à grands pas, on a $\left< S_1; S_2, s \right> \to s''$. Comme $\textsf{wlp}(S_1; S_2, Q)s = tt$ \textblue{(par hypothèse)}, $Qs'' = tt$ \textblue{(on peut peut-être condenser ce passage)}.
		
		On applique alors $[cons_p]$ avec $\textsf{wlp}(S_1; S_2, Q) \Rightarrow \textsf{wlp}(S_1,\textsf{wlp}(S_2, Q))$, d'où $\vdash_p \{\textsf{wlp}(S_1;S_2, Q)\}~S_1;S_2~\{Q\}$.
		
		\item[Cas de la conditionnelle] Soit $Q$ un prédicat.
		
		On pose $P = \left(\mathcal{B}\llbracket b \rrbracket \wedge \textsf{wlp}(S_1, Q)\right) \vee \left(\neg\mathcal{B}\llbracket b \rrbracket \wedge \textsf{wlp}(S_2, Q)\right)$ \textblue{(si j'ai $b$ alors je dois pouvoir appliquer $S_1$, sinon je dois pouvoir appliquer $S_2$)}. On a alors $\mathcal{B}\llbracket b \rrbracket \wedge P = \left(\mathcal{B}\llbracket b \rrbracket \wedge \textsf{wlp}(S_1, Q)\right) \vee \left(\mathcal{B}\llbracket b \rrbracket \wedge \neg\mathcal{B}\llbracket b \rrbracket \wedge \textsf{wlp}(S_2, Q)\right) = \mathcal{B}\llbracket b \rrbracket \wedge \textsf{wlp}(S_1, Q) \Rightarrow  \textsf{wlp}(S_1, Q)$. De même, on montre que $\neg \mathcal{B}\llbracket b \rrbracket \wedge P \Rightarrow \textsf{wlp}(S_2, Q)$.
		
		Montrons que $\vdash_p \{P\}~\textsf{if } b \textsf{ then } S_1 \textsf{ else } S_2~\{Q\}$. En appliquant les hypothèses d'induction à $S_1$ et $S_2$, on obtient: $\vdash_p \{\textsf{wlp}(S_1, Q)\}~S_1~\{Q\}$ et $\vdash_p \{\textsf{wlp}(S_2,Q)\}~S_2~\{Q\}$. En appliquant la règle $[cons_p]$ aux deux preuves avec $\mathcal{B}\llbracket b \rrbracket \wedge P \Rightarrow \textsf{wlp}(S_1, Q)$ et $\neg \mathcal{B}\llbracket b \rrbracket \wedge P \Rightarrow \textsf{wlp}(S_2, Q)$, on obtient $\vdash_p \{\mathcal{B}\llbracket b \rrbracket \wedge P\}~S_1~\{Q\}$ et $\vdash_p \{\neg\mathcal{B}\llbracket b \rrbracket \wedge P\}~S_2~\{Q\}$. On applique la règle $[if_p]$, donc $\vdash_p \{P\}~\textsf{if } b \textsf{ then } S_1 \textsf{ else } S_2~\{Q\}$.
		
		Montrons maintenant que $\textsf{wlp}(\textsf{if } b \textsf{ then } S_1 \textsf{ else } S_2, Q) \Rightarrow P$. Supposons qu'il existe $s \in State$ tel que $\textsf{wlp}(\textsf{if } b \textsf{ then } S_1 \textsf{ else } S_2, Q)s =tt$ et montrons que $Ps = tt$. On distingue deux cas:
		\begin{itemize}
			\item Cas $\mathcal{B}\llbracket b \rrbracket_s = tt$. Soit $s' \in State$ tel que $\left< \textsf{if } b \textsf{ then } S_1 \textsf{ else } S_2\right> \to s'$. Montrons que $Qs' = tt$. Comme $\mathcal{B}\llbracket b \rrbracket_s = tt$, en appliquant la règle $[if^{tt}_{NS}]$, $\left< \textsf{if } b \textsf{ then } S_1 \textsf{ else } S_2\right> \to s'$ si $\left< S_1, s \right> \to s'$. Or, par hypothèse d'induction \textblue{($\vdash_p \{\textsf{wlp}(S_1, Q)\}~S_1~\{Q\}$)} $Ps' =tt \Rightarrow \textsf{wlp}(S_1, Q)s = tt \Rightarrow Qs' = tt$. 
			\item Cas $\mathcal{B}\llbracket b \rrbracket_s = ff$ : analogue
		\end{itemize}
		En appliquant la règle $[cons_p]$ avec $\textsf{wlp}(\textsf{if } b \textsf{ then } S_1 \textsf{ else } S_2, Q) \Rightarrow P$ permet de conclure.
		
		\item[Cas de la boucle \textsf{while}] On pose $P = \textsf{wlp}(\textsf{while } b \textsf{ do }S, Q)$.
		
		Montrons que $(\neg \mathcal{B}\llbracket b \rrbracket \wedge P) \Rightarrow Q$. Soit $s \in State$ tel que $(\neg \mathcal{B}\llbracket b \rrbracket \wedge P)s =tt$. Dans ce cas, \textblue{(application de la conjonction)} $(\mathcal{B}\llbracket b \rrbracket)s = ff$ et $Ps = tt$. Comme $(\mathcal{B}\llbracket b \rrbracket)s = ff$, en appliquant la règle $[while^{ff}_{NS}]$, on a $\left< \textsf{while } b \textsf{ do }S, s\right> \to s$. Donc, $Qs = tt$ \textblue{(par $Ps = tt$, sa définition et la définition de \textsf{wlp})}.
		
		Montrons que $(\mathcal{B}\llbracket b \rrbracket \wedge P) \Rightarrow \textsf{wlp}(S,P)$. Soit $s \in State$ tel que $(\mathcal{B}\llbracket b \rrbracket \wedge P)s = tt$, donc \textblue{(application de la conjonction)} $(\mathcal{B}\llbracket b \rrbracket)s = tt$ et $Ps = tt$. Montrons que $\textsf{wlp}(S,P)s = tt$. Soit $s' \in State$ tel que $\left< S, s\right> \to s'$ et montrons que $Ps' = tt$. On distingue deux cas.
		\begin{itemize}
			\item Supposons qu'il existe $s'' \in State$ tel que $\left< \textsf{while } b \textsf{ do }S, s' \right> \to s''$. En appliquant la règle $[while^{tt}_{NS}]$, on a $\left< \textsf{while } b \textsf{ do }S, s \right> \to s''$. Comme $Ps = tt$ \textblue{(hypothèse)}, on a alors $Qs'' = tt$ par le lemme~\ref{lem:wlp}.
			
			\item Supposons maintenant que pour tout $s'' \in State$ on n'ait pas $\left< \textsf{while } b \textsf{ do }S, s' \right> \to s''$. On en déduit que pour tout $s'' \in State$, $Ps' = ff$. 
		\end{itemize}
		
		L'hypothèse d'induction sur $S$ donne $\vdash_p \{\textsf{wlp}(S, P)\}~S~\{P\}$. En appliquant la règle $[cons_p]$ avec $(\mathcal{B}\llbracket b \rrbracket \wedge P) \Rightarrow \textsf{wlp}(S,P)$, on obtient $\vdash_p \{\mathcal{B}\llbracket b \rrbracket \wedge P\}~S~\{P\}$. En appliquant la règle $[while_p]$, on a $\vdash_p \{P\}~\textsf{while } b \textsf{ do }S~\{\neg\mathcal{B}\llbracket b \rrbracket \wedge P\}$. En appliquant une seconde fois la règle $[cons_p]$, on a $\vdash_p \{P\}~\textsf{while } b \textsf{ do }S~\{Q\}$. 
		\end{description}
		D'où la complétude.
	\end{proof}
	
	
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}