\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[svgnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{array}
\usepackage{tikz}
\usepackage{subcaption}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude
\usetikzlibrary{automata,positioning,shadows}
\usetikzlibrary{calc, shapes, backgrounds,arrows}

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\newtheorem*{appli}{Application}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Automate des occurrences}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Cormen \cite[p.915]{Cormen}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 907 (Texte); 909 (Langages rationnels); 921 (Recherche).
		}}
	
	\section{Introduction}
	
	L'automate des occurrences, aussi appelé l'automate des suffixes permet d'accepter les mots dont le suffixe est un certain motif: il reconnaît des mots du type $\Sigma^*P$ où $P$ est fixé à l'avance. De plus, sa construction nous donne un automate minimal ce qui est toujours intéressant.
	
	Cet automate peut être utilisé pour la recherche d'un motif dans un texte puisque rechercher un motif c'est vérifier si un des préfixes de ce texte n'a pas comme suffixe le motif. Autrement dit, on cherche si un préfixe du texte n'est pas accepté par l'automate des occurrences. Cette remarque, sans le prétraitement, nous donne une recherche linéaire en la taille du texte puisqu'une seule lecture du texte dans l'automate nous donne la présence ou non du motif dans le texte. 
	
	\titlebox{red}{\textred{\textbf{Remarques sur le développement}}}{
		Ce développement peut se spécialiser en fonction de la leçon.
		\begin{enumerate}
			\item Définition et propriétés de la fonction suffixe.
			\item Définition de l'automate des occurrences \textblue{(leçons 907 et 921: présentation d'un exemple)}.
			\item Correction de cet automate.
			\item Minimalité de cet automate \textblue{(leçon 909)}.
			\item Application à la recherche de motif \textblue{(leçons 907 et 921)}.
		\end{enumerate}
	}
	
	\section{Propriétés de l'automate des occurrences et applications}
	
	Soit $\Sigma$ un alphabet fini, $T \in \Sigma^*$ un texte et $P \in \Sigma^*$ un mot que l'on cherche dans $T$. On suppose $P$ fixé.
	
	\paragraph{Définition et propriétés de la fonction suffixe} Nous commençons par définir la fonction suffixe qui est une manière de calculer le décalage en cas d'échec de la lecture.
	
	\begin{definition}
		On définie la fonction suffixe associée à $P$, $\sigma$, telle que $\forall u \in \Sigma^*$
		\begin{displaymath}
		\sigma(u) = \underset{k \in \llbracket 0, p \rrbracket}{\max}\{P_k \text{ est suffixe de } u\}
		\end{displaymath}
		où $p$ est la taille de $P$ et $P_k$ est le préfixe de $P$ de longueur $k$. \textblue{(Cette fonction calcule la taille du plus long suffixe de $u$ qui est aussi préfixe de $P$.)}
	\end{definition}
	
	\begin{prop}
		La fonction suffixe vérifie les propriétés suivantes.
		\begin{enumerate}
			\item Si $u$ est un suffixe de $v$ alors $\sigma(u) \leq \sigma(v)$ \textbrown{(remarque après la définition)} 
			\item Si $u \in \Sigma^*$ et $a \in \Sigma$, alors $\sigma(ua) \leq \sigma(u) + 1$ \textbrown{(lemme 32.2)}
			\item Si $w \in \Sigma^*$ et $a \in \Sigma$, alors $\sigma(wa) = \sigma(P_{\sigma(w)}a)$ \textbrown{(lemme 32.3)}
		\end{enumerate}
		\label{prop:fonctionSuffixe}
	\end{prop}
	\begin{proof}
		\begin{enumerate}
			\item Si $u$ est suffixe de $v$ alors tout suffixe $w$ de $u$ est aussi suffixe de $v$. En particulier si $w$ est le plus long suffixe de $u$ qui est aussi préfixe de $P$, alors $w$ est suffixe de $v$. Par définition de la fonction suffixe, on $\sigma(u) \leq \sigma(v)$ \textblue{($w$ n'est pas nécessairement le plus long pour $v$)}.
			
			\item On pose $r = \sigma(ua)$. Si $r = 0$, alors $r = 0 \leq \sigma(u) + 1$ par positivité de la fonction $\sigma$. Sinon, $r > 0$ et $P_r$ est un suffixe de $ua$. En particulier $P_{r-1}$ est un suffixe de $u$ \textblue{(on supprime le $a$ à la fin de $P_r$ et de $ua$)}. D'où $r - 1 \leq \sigma(u)$ \textblue{(par définition de $\sigma(u)$ qui est la longueur maximale d'un suffixe de $u$ qui est préfixe de $P$)}. Donc, $r \leq \sigma(u) + 1$.
			
			\item \begin{description}
				\item[$\geq$] Par définition de $\sigma$, $P_{\sigma(w)}$ est un suffixe de $w$. En particulier $P_{\sigma(w)}a$ est un suffixe de $wa$. Donc, \textblue{(par le même raisonnement que précédemment)} $\sigma(wa) \geq \sigma(P_{\sigma(w)}a)$.
				
				\item[$\leq$] Réciproquement, $P_{\sigma(w)}a$ et $P_{\sigma(wa)}$ sont suffixes de $wa$. Comme, $|P_{\sigma(wa)}| = \sigma(wa) \leq \sigma(w) + 1 = |P_{\sigma(w)}|$ \textblue{(item précédent)}, $P_{\sigma(wa)}$ est un suffixe de $P_{\sigma(w)}a$. D'où, $\sigma(wa) \leq \sigma(P_{\sigma(w)}a)$.
			\end{description}
		\end{enumerate}
	\end{proof}
	
	\paragraph{Définition de l'automate des occurrences} On continue en énonçant la définition de l'automate des occurrences ainsi que ces propriétés.
	
	\begin{minipage}{.53\textwidth}
		\begin{figure}[H]
			\begin{tikzpicture}[font=\sffamily]
			% Setup the style for the states
			\tikzset{node style/.style={state, 
					minimum width=1cm,
					line width=.25mm,
					fill=white,circle}}
			
			% Draw the states
			\node[node style,initial] at (0,0) (0) {$0$};
			\node[node style] at (2,0) (1) {$1$};
			\node[node style] at (4,0) (2) {$2$};
			\node[node style,accepting] at (6,0) (3) {$3$};
			
			% Connect the states with arrows
			\draw[every loop,auto=right,line width=.25mm,
			>=latex]
			(0) edge [] node {$a$} (1)
			(0) edge [loop above] node {$b$} (1)
			(1) edge [] node {$b$} (2)
			(1) edge [loop above] node {$a$} (1)
			(2) edge [bend left,auto=left] node {$b$} (0)
			(2) edge [] node {$a$} (3);
			\end{tikzpicture}
			\caption{Un automate des occurrences pour le motif $aba$. \textblue{(On ne fait un automate qui ne reconnaît que $\Sigma^*w$, cela simplifie la preuve de la minimalité (sinon on contredit la co-déterminisation).)}}
		\end{figure}
	\end{minipage} \hfill
	\begin{minipage}{.4\textwidth}
		Dans le cadre de la recherche, on l'applique à $T = aabbabab$.
		
		\begin{definition}
			On construit l'automate des occurrences comme suit :
			\begin{itemize}
				\item $Q = \llbracket 0, m \rrbracket$ si $m = |P|$
				\item $I = 0$
				\item $F = \{m\}$
				\item $\delta(q, a) = \sigma(P_qa)$
			\end{itemize}
		\end{definition}
	\end{minipage}
	
	\begin{definition}
		On définit $\phi : \Sigma^* \to Q$ la fonction calculant l'état dans lequel on arrive dans l'automate en lisant un mot donné (en entrée). On la définie par induction:
		\begin{itemize}
			\item $\phi(\epsilon) = 0$;
			\item $\forall u \in \Sigma^*$ $a \in \Sigma$, $\phi(ua) = \delta(\phi(u), a)$.
		\end{itemize}
	\end{definition}
	
	\begin{theo}
		Pour tout mot $w \in \Sigma^*$, on a $\phi(w) = \sigma(w)$.
	\end{theo}
	\begin{proof}
		On raisonne par récurrence sur la taille de $w$. On note, pour tout $i \in \N$, $\mathcal{P}_i : "\forall w \in \Sigma^i, \phi(w) = \sigma(w)"$ l'hypothèse de récurrence pour tous les mots de longueur $i$.
		
		\begin{description}
			\item[Initialisation] Pour $i = 0$, $|w| = 0$ implique que $w = \epsilon$. On a par définition, $\phi(\epsilon) = 0$ et $\sigma(\epsilon) = 0$.
			
			\item[Hérédité] Soit $i \in \N$ tel que $\mathcal{P}_i$ soit vraie. Soit $w \in \Sigma^{i+1}$. Montrons que $\phi(w) = \sigma(w)$. On note $w = w'a$ avec $|w'|  = 1$. On a :
			\begin{displaymath}
			\begin{array}{ccll}
			\phi(w) & = & \phi(w'a) & \textblue{\text{(définition de} w)} \\
			 & = & \delta(\phi(w'), a) & \textblue{\text{(définition de } \phi)} \\
			 & = & \delta(\sigma(w'), a) & \textblue{\text{(hypothèse de récurrence)}} \\
			 & = & \sigma(P_{\sigma(w')}a)& \textblue{\text{(définition de } \delta)} \\
			 & = & \sigma(w'a)& \textblue{\text{(item 3 de la proposition~\ref{prop:fonctionSuffixe})}} \\
			 & = & \sigma(w)& \textblue{\text{(définition de } w)} \\
			\end{array}
			\end{displaymath}
		\end{description}
	\end{proof}
	
	\begin{cor}
		Soit $P$ un motif de taille $m$ et $\mathcal{A}$ son automate des occurrences. Alors, on a $\mathcal{L}(\mathcal{A}) = \Sigma^*P$.
	\end{cor}
	\begin{proof}
		On raisonne par double inclusion.
		\begin{description}
			\item[$\subseteq$] Si $w \in \mathcal{L}(\mathcal{A})$, alors $\delta(0, w) = m = \sigma(m)$. Donc $P$ est suffixe de $w$ et $w \in \Sigma^*P$.
			
			\item[$\supseteq$] Si $w \in \Sigma^*P$, alors $\sigma(w) = m = \delta(0, w)$. Donc $w$ est accepté par l'automate $\mathcal{A}$, soit $w \in \mathcal{L}(\mathcal{A})$.
		\end{description}
	\end{proof}
	
	\begin{theo}
		Soit $\mathcal{A}$ l'automate des occurrences d'un motif $P$. $\mathcal{A}$ est l'automate minimal pour $\Sigma^*P$.
	\end{theo}
	\begin{proof}
		Montrons que l'automate des occurrences est minimal par l'algorithme des renversements. Pour cela, on va montrer qu'il est déterministe, co-accessible et co-déterministe.
		
		Montrons que l'automate des occurrences $\mathcal{A}$ est déterministe. Comme $\delta$ est définie par la fonction suffixe $\sigma$, $\mathcal{A}$ est déterministe \textblue{(sa fonction transition est donnée par une fonction)}.
		
		Montrons que l'automate des occurrences $\mathcal{A}$ est co-accessible \textblue{($\forall q \in Q, \exists f \in F$ tel que $\exists w \in \Sigma^+, \delta(q, w) = f$)}. Soit $q \in Q$. Comme $F = \{m\}$, $f$ est nécessairement $m$. On pose $w$ qui est le suffixe de taille $m-q$ de $P$. Par définition de $\delta$, on a $\delta(q, w) = \sigma(P_qw) = \sigma(P) = m$. Donc $\mathcal{A}$ est co-accessible.
		
		Montrons que l'automate des occurrences $\mathcal{A}$ est co-déterministe \textblue{(le miroir de celui-ci est déterministe)}. Par définition $F = \{m\}$, donc $\mathcal{A}$ ne possède qu'un état acceptant? Montrons que pour tout état $i \in Q$ et toute lettre $a \in \Sigma$, il n'y a au plus qu'une transition de $a$ qui arrive dans $i$. Raisonnons par l'absurde et supposons qu'il existe $i \in Q$ et $a \in \Sigma$ tels que $\delta(q, a) = \delta(p,a) = i$ avec $p \neq q$. Par définition de $\delta$, $\sigma(P_qa) = \sigma(P_pa) = i$. Par la construction de $\mathcal{A}$, il existe $v \in \Sigma^*$ tel que $P_qav = P = P_qav$ D'ou $|P_qav| = |P_pav|$. On en déduit que $|P_q| = |P_q|$. On obtient une contradiction car $p \neq q$.		
		
		Par la propriété justifiant la correction de l'algorithme par renversement, $\mathcal{A}$ est minimal.
	\end{proof}
	\begin{proof}[Autre preuve à privilégier]
		Soient $i \neq j$ deux états distincts $q_i$ et $q_j$ de l'automate des occurrences. Montrons que ces deux états sont séparables par la congruence de Nérode. Soit $w$ un mot tel $q_i \overrightarrow{w} q_f$. Alors $q_j \overrightarrow{w} q_k$ avec $k \neq f$ car sinon $w$ est suffixe des préfixes de taille $i$ et de taille $j$.
	\end{proof}
	
	\begin{req}
		Un argument de cardinalité sur les états peut être également évoquer.
	\end{req}
	
	\paragraph{Application à la recherche de motif \cite[p.916]{Cormen}} Une application de cet automate est son utilisation pour la recherche d'un motif. En effet, si $P$ est dans un texte $T$ alors $T = \Sigma^*P\Sigma^*$. L'automate que nous venons de construire est donc tout adapté à cette recherche. Les algorithmes~\ref{algo:fonctionAutomate} et~\ref{algo:MP} permettent d'effectuer cette recherche. Ils sont correct par le théorème précédent \textblue{(pas celui sur la minimalité)}. On obtient une recherche en $O(|T|)$ avec un prétraitement de $O(m|\Sigma|)$. \textblue{L'algorithme KMP améliore la complexité du prétraitement.}
	
	\section{Algorithme de Knuth--Morris--Pratt}
	Une amélioration de l'application à la recherche de motif est l'algorithme de Knuth--Morris--Pratt qui va se passer de l'automate pour calculer le saut à effectuer.
	\input{./../Notions/Texte_KMP.tex}
	
	\section{Automate minimal}
	Dans ce développement, on présente une preuve de minimalité d'un automate fini car l'automate des occurrences est un automate fini. On va donc présenter ici quelques caractéristiques de ces automates minimaux.
	\input{./../Notions/Automate_Minimal.tex}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}