\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames,svgnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{array}
\usepackage{tikz}

\usepackage{algorithm}
\usepackage{algpseudocode}%

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newtheorem*{definition}{Définition}
	\newtheorem*{notation}{Notation}
	\newtheorem*{theorem}{Théorème}
	\newtheorem*{lemme}{Lemme}
	
	\newcommand{\textblue}{\textcolor{blue}}
	\newcommand{\textgreen}{\textcolor{Green}}
	\newcommand{\textred}{\textcolor{red}}
	\newcommand{\textbrown}{\textcolor{Brown}}
	\newcommand{\textmagenta}{\textcolor{DarkOrchid}}
	
	\title{Preuve du théorème de Savitch}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Sipser \cite[p.335]{Sisper}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 913 (Machines de Turing); 915 (Classes de complexité).
			
			\textblue{\emph{Leçons dans lesquelles on peut l'évoquer:}} 926 (Analyse complexité); 931 (Schéma algorithmique) \textblue{(application du diviser pour régner)}.
		}}
	
	\section{Introduction}
	
	Le théorème de Savitch permet de montrer l'égalité entre deux classe de complexité NPSPACE et PSPACE, ce qui est un des rares résultats de ce type. Généralement, lorsque nous nous donnons deux classes de complexité nous ne pouvons pas dire si elles sont distinctes ou non (P égal ou non NP).
	
	Dans le cas très particulier de la complexité en espace polynomiale, le théorème de Savitch, nous assure que l'équivalence entre une machine déterministe et non déterministe est solvable en espace polynomial (ce qui induit PSPACE = NPSAPCE).
	
	\titlebox{red}{\textred{\textbf{Remarques sur le développement}}}{
		Le développement présente une preuve du théorème de Savitch : il en existe des théorèmes qui sont plus généraux (voir le Carton \cite{Carton}?).
		\begin{enumerate}
			\item Hypothèse sur $M$.
			\item Construction de $M'$.
			\item Complexité spatiale de $M'$.
			\begin{enumerate}
				\item Majoration du nombre de configurations accessibles depuis la configuration initiale.
				\item Caractérisation de la présence d'un chemin.
			\end{enumerate}
		\end{enumerate}
	} 
	
	\section{Théorème de Savitch}
	
	\begin{theorem}
		Soit $f : \N \rightarrow \R^{+}$ telle que $f(n) \geq n$, pour $n$ assez grand. Toute machine de Turing non-déterministe qui décide en espace $f(n)$ est équivalente à une machine de Turing déterministe en espace $O(f^{2}(n))$.
	\end{theorem}
	
	\subsection{Idée de la preuve}
	
	On souhaite modéliser le comportement d'une machine non-déterministe à l'aide d'une machine déterministe en bornant l'espace que cette dernière peut utiliser (en fonction de l'espace occupé par la première). 
	
	L'idée est alors d'utiliser le graphe des configurations de la machine non-déterministe (qui est borné par hypothèse), et de lui appliqué le problème d'accessibilité dans un graphe orienté. Comme celui-ci est dans NL, on va pouvoir exploiter sa complexité spatiale afin d'obtenir le résultat que l'on recherche. On va donc "ramener" le problème au d'accessibilité dans un graphe orienté.
	
	\subsection{Preuve du théorème}
	
	Nous allons montrer une version plus faible du théorème de Savitch que nous énonçons ci-dessous.	
	
	\begin{theorem}
		Soit $f : \N \rightarrow \R^{+}$ telle que $f(n) \geq n$ et $f(n)$ est calculable en espace $O(f)$. Alors, $NSPACE(f) \subseteq SPACE(f^{2})$.
	\end{theorem}
	
	\begin{proof}
		Soit $M$ une machine non-déterministe qui décide un langage $L$ en espace $f(n)$ ($L \in NSPACE(f)$). Montrons qu'il existe une machine $M'$ qui simule $M$ pour décider $L$ en espace $O(f^{2}(n))$.
	
		\paragraph{Hypothèses sur $M$} Nous supposons sans perte de généralité que $M$ possède un unique état initial $q_{init}$ et un unique état final $q_{final}$ qui est atteint une fois que $M$ a effacé son ruban et que la tête de lecture est revenue se placer à gauche. On notera $c_{final}$ la configuration finale (liée à l'état final). Soit $w$ l'entrée sur $M$, on note $c_{init}$ la configuration initial correspondant à $w$.
		
		\textblue{On s'intéresse à la mémoire utilisée par la machine de Turing durant son exécution (pas à son temps de calcule ou à sa taille), c'est à dire la quantité de ruban que la machine utilise durant son exécution. Les changements que nous effectuons ici, ne touche en aucun cas à la quantité de ruban utilisée puisque la seule opération effectuée sur le ruban par cette nouvelle machine consiste à l'effacer ce qui ne change pas la quantité de ruban utilisé.}
		
		
		\paragraph{Définition de $M'$} On définit $M'$ par l'algorithme (\ref{algo:M'}). Elle prend en entrée $w$ l'entrée de $M$ et elle accepte l'exécution si $w \in L(M)$ (elle la rejette dans le cas contraire). On remarque que cet algorithme est bien déterministe. 
		
		\begin{algorithm}
			\begin{algorithmic}[1]
				\Procedure{\textsc{M'}}{$w$} 
				\If {$(c_init) \vdash^{*}_{M} c_{accept}$ dans le graphe des configuration de $M$}
				\State Accepter
				\Else
				\State Rejeter
				\EndIf
				\EndProcedure
			\end{algorithmic}
			\caption{Définition de la machine de Turing déterministe $M'$}
			\label{algo:M'}
		\end{algorithm}
		
		
		\paragraph{Complexité de $M'$} Nous allons évaluer la complexité de $M'$ en étudiant la complexité pour le calcul de ce prédicat: $(c_init) \vdash^{*}_{M} c_{accept}$. On remarque dans un premier temps que si on a $(c_init) \vdash^{*}_{M} c_{accept}$ alors il existe un chemin de $c_{init}$ à $c_{final}$ dans le graphe des configurations de $M$ (\textblue{Le graphe des configuration est un graphe dont les sommets sont les configurations de la machine de Turing et les arrêtes sont définies telles que la fonction de transition permet de passer d'une configuration à une autre en une étape.})  que l'on note $(c_init) \rightarrow^{*}_{M} c_{accept}$. 
		
		Pour ce calcul de complexité, nous allons majorer le nombre de configuration accessible à partir $c_{initial}$ et définir une fonction qui calcul le chemin de $c_{initial}$ à $c_{final}$ en majorant le nombre d'étapes. Pour cela, nous allons utiliser deux lemmes.
		
		\begin{lemme}
			Il existe un $d \in \N$ tel que le nombre de configuration accessible depuis $c_{initial}$ soit majoré par $2^{df(|w|)}$.
		\end{lemme}
		\begin{proof}
			Le nombre de configuration accessible depuis  $c_{initial}$ est majoré par $\textblue{|\mathcal{Q}|} \times \textred{|\Gamma|^{f(|w|)}} \times \textbrown{f(|w|)}$ car \textblue{le nombre d'état que l'on peut atteindre}, \textred{le nombre de mot que l'on peut écrire sur le ruban sans violer la propriété sur $M$} et \textbrown{le nombre de position possibles pour la tête de lecture}.
		\end{proof}
		
		\begin{lemme} $c_{initial} \rightarrow^{*} c_{accept}$ si et seulement si $c_{initial} \rightarrow^{2df(|w|)} c_{accept}$.
		\end{lemme}
		
		Nous allons alors nous appuyer sur la fonction \textsc{Chemin?} dont voici la spécification. Pour deux configurations $c_1$ et $c_2$ de $M$, ainsi qu'un entier $t$, $\textsc{Chemin?}(c_1, c_2, t)$ retourne \emph{vrai} s'il existe un chemin (\textblue{une suite de configuration}) de $c_1$ vers $c_2$ dans le graphe des configurations de $M$ en au plus $t$ étapes; et \emph{faux} sinon. Dans la suite, on suppose que $t$ est une puissance de $2$ (\textblue{dans le cas contraire, comme on a toujours l'existence d'une puissance de $2$ qui est plus grande que $t$, on prend la plus petit de ces puissance de $2$ comme nouvelle valeur de $t$ (ce n'est pas géant car on cherche à majorer la complexité)}).
		
		\begin{proof}
			Le sens indirect est immédiat. Nous allons alors montrer la réciproque.
			
			Nous implémentons le test $c_{init} \rightarrow^{*} c_{final}$ à l'aide de la fonction $\textsc{Chemin?}(c_{initial}, c_{accept}, 2^{df(|w|)})$ où \textsc{Chemin?} est définit comme précédemment et par l'algorithme \ref{algo:chemin}.
			\begin{algorithm}
			\begin{algorithmic}[1]
				\Function{\textsc{Chemin?}}{$c_1, c_2, t$} 
				\If {$t = 1$}
				\State Renvoie $c_1 \rightarrow^{\leq 1} c_2$ 
				\Else
				\For{toute configuration $c$ de taille de ruban d'au plus $f(|w|)$}
				\If{\textsc{Chemin?}($c_1, c, \frac{t}{2}$) et \textsc{Chemin?}($c, c_2, \frac{t}{2}$)}
				\State Renvoie \emph{vraie}
				\EndIf
				\EndFor
				\State Renvoie \emph{faux}
				\EndIf
				\EndFunction
			\end{algorithmic}
			\caption{Fonction $\textsc{Chemin?}$}
			\label{algo:chemin}
		\end{algorithm}
		
		La fonction \textsc{Chemin?} est suit le principe diviser pour régner. Sa complexité spatiale est alors de $Cout(t) = O(f) + Cout(\frac{t}{2})$ où $t$ est le troisième argument de \textsc{Chemin?} (\textmagenta{Hypothèse 2: on peut calculer $f(|w|)$ en $O(f)$}). On obtient donc $Cout(t) = O(f\log_2 t)$ (\textblue{par le master théorème}). La complexité spatiale de $M'$, \textmagenta{par l'hypothèse 1} est de $\textbrown{O(f)} + \textblue{Cout(2^{df(|w|)})} = O(f) + O(f\log_2 (2^{df(|w|)}) = O(f) + O(fdf) = O(f) + O(f^{2}) = O(f^{2})$ car \textbrown{on calcul une fois $f(|w|)$ pour la fonction \textsc{Chemin?}} \textmagenta{et par l'hypothèse 2 se fait en $O(f)$} et \textblue{on exécute la fonction \textsc{Chemin?} avec $t = 2^{df(|w|)}$}.
		
		\end{proof}
		
		D'où le résultat.
		
	\end{proof}
	
	
	\subsection{Remarques sur la preuve}
	
	\begin{itemize}
		\item On peut le faire dans le cadre du plan mais il faut faire attention à la complexité du calcul de $f(|w|)$ Pour contrer cela, on appel la fonction \textsc{Chemin?} pour chaque valeur de $f(i)$ et on incrémente $i$ pas à pas tant qu'on a pas trouvé de chemin acceptante. (Cela nous permet de calculer expérimentalement la borne que l'on a ajouté dans les hypothèses).
		\item L'hypothèse $f(n) \geq n$ nous permet de lire l'entrée sur le ruban; cependant avec une machine à plusieurs rubans, $f(n) \geq \log n$ suffit.
		\item Cette preuve est basée sur la NL-complétude du problème d'accessibilité dans un graphe. Avec cet argument (non trivial), la preuve devient très facile.
	\end{itemize}
	
	Pendant le calcul de la complexité, nous utilisons de manière centrale ce théorème \textblue{A savoir énoncer et montrer!}
	
	
	\begin{theorem}[Master theorem]
		Soient $t : \N \mapsto \R_{+}$ une fonction croissante à partir d'un certain rang $n_0$, $b \geq 2$ entiers, $k \geq 0$ entier et $a, b, c, d > 0$ réels positifs tels que pour tout $n$ tel que $\frac{n}{n_0}$ puissance de $b$: 
		\[
		\left \{
		\begin{array}{c @{ = } l}
		t(n_0) & b \\
		t(n) & at(\frac{n}{b}) + cn^{b} \\
		\end{array}
		\right.
		\]
		
		Alors, on a:
		 \[ t(n) =
			\left \{
			\begin{array}{l @{~si~} r}
			\Theta(n^{k}) & a< b^{k} \\
			\Theta(n^{k}\log_{b}n) & a = b^{k}\\
			\Theta(n^{\log_{b}a}) & a > b^{k}\\
			\end{array}
			\right.
			\]  
	\end{theorem}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}