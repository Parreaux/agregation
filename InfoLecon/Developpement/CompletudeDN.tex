\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{array}
\usepackage{tikz}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{bussproofs}
\usepackage{amssymb}
\usepackage{latexsym}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}


\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
} 

\begin{document}
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\newtheorem*{appli}{Application}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\def\fCenter{{\mbox{\Large$\rightarrow$}}}
	
	% Optional to turn on the short abbreviations
	\EnableBpAbbreviations
	
	\title{Complétude de la déduction naturelle en logique du premier ordre}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} David Nour Raffali \cite[p.83]{DavidNourRaffali}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 918 (Systèmes de preuve); 924 (Théories et modèles).
		}}
	
	\section{Introduction}
	
	Le système de preuve de la déduction naturelle est correcte et complet: une preuve d'une formule existe que si celle-ci est satisfiable. Ce système de preuve dont on rappelle les règles dans la Table~\ref{tab:DNregles}, ne fait pas n'importe quoi.
	
	\begin{table}
		\centering
		\begin{tabular}{|lcr|lc|}
			\hline
			& & & & \\
			Axiome &  
			\AxiomC{}
			\LeftLabel{}\RightLabel{{\footnotesize \textsf{Ax}}}
			\UnaryInfC{$A \vdash A$}
			\DisplayProof
			& & Affaiblissement & 
			\AxiomC{$\vdash A$}
			\LeftLabel{}\RightLabel{{\footnotesize \textsf{Aff}}}
			\UnaryInfC{$B \vdash A$}
			\DisplayProof \\
			& & & & \\
			\hline
			& & & & \\
			Intro implication &  
			\AxiomC{$B \vdash A$}
			\LeftLabel{}\RightLabel{{\footnotesize \textsf{$\rightarrow_i$}}}
			\UnaryInfC{$\vdash A \rightarrow B$}
			\DisplayProof
			& & Élim implication & 
			\AxiomC{$\vdash A \rightarrow B$}
			\AxiomC{$\vdash A$}
			\LeftLabel{}\RightLabel{{\footnotesize \textsf{$\rightarrow_e$}}}
			\BinaryInfC{$\vdash B$}
			\DisplayProof \\
			& & & & \\
			\hline
			& & & & \\
			Intro conjonction &  
			\AxiomC{$\vdash A$}
			\AxiomC{$\vdash B$}
			\LeftLabel{}\RightLabel{{\footnotesize \textsf{$\wedge_i$}}}
			\BinaryInfC{$\vdash A \wedge B$}
			\DisplayProof
			& & Élim disjonction & 
			\AxiomC{$\vdash A \vee B$}
			\AxiomC{$\vdash A \to C$}
			\AxiomC{$\vdash B \to C$}
			\RightLabel{{\footnotesize \textsf{$\vee_e$}}}
			\TrinaryInfC{$\vdash C$}
			\DisplayProof \\
			& & & & \\
			\hline
			& & & & \\
			Élim conjonction &  
			\AxiomC{$\vdash A \wedge B$}
			\LeftLabel{}\RightLabel{{\footnotesize \textsf{$\wedge_e^g$}}}
			\UnaryInfC{$\vdash A$}
			\DisplayProof
			& & Élim conjonction & 
			\AxiomC{$\vdash A \wedge B$}
			\LeftLabel{}\RightLabel{{\footnotesize \textsf{$\wedge_e^d$}}}
			\UnaryInfC{$\vdash B$}
			\DisplayProof \\
			& & & & \\
			\hline
			& & & & \\
			Intro disjonction &  
			\AxiomC{$\vdash A$}
			\LeftLabel{}\RightLabel{{\footnotesize \textsf{$\vee_i^g$}}}
			\UnaryInfC{$\vdash A \vee B$}
			\DisplayProof
			& & Intro disjonction & 
			\AxiomC{$\vdash B$}
			\LeftLabel{}\RightLabel{{\footnotesize \textsf{$\vee_i^d$}}}
			\UnaryInfC{$\vdash A \vee B$}
			\DisplayProof \\
			& & & & \\
			\hline
			& & & & \\
			Intro négation &  
			\AxiomC{$A \vdash \bot$}
			\LeftLabel{}\RightLabel{{\footnotesize \textsf{$\neg_i$}}}
			\UnaryInfC{$\vdash \neg A$}
			\DisplayProof
			& & Élim négation & 
			\AxiomC{$\vdash \neg A$}
			\AxiomC{$\vdash A$}
			\LeftLabel{}\RightLabel{{\footnotesize \textsf{$\neg_e$}}}
			\BinaryInfC{$\vdash \bot$}
			\DisplayProof \\
			& & & & \\
			\hline
			& & & & \\
			Élim pour tout &  
			\AxiomC{$\vdash \forall x~ A$}
			\LeftLabel{}\RightLabel{{\footnotesize \textsf{$\forall_e$}}}
			\UnaryInfC{$\vdash A[x := t]$}
			\DisplayProof
			& & Intro pour tout & 
			\AxiomC{$\Gamma \vdash A$}
			\AxiomC{$x \notin \textsf{Free}(\Gamma)$}
			\LeftLabel{}\RightLabel{{\footnotesize \textsf{$\forall_i$}}}
			\BinaryInfC{$\Gamma \vdash \forall x~ A$}
			\DisplayProof \\
			& & & & \\
			\hline
			& & & & \\
			Intro existence &  
			\AxiomC{$\vdash A$}
			\LeftLabel{}\RightLabel{{\footnotesize \textsf{$\exists_i$}}}
			\UnaryInfC{$\vdash \exists~x A$}
			\DisplayProof
			& & Élim existence & 
			\AxiomC{$\Gamma \vdash \exists x~ A$}
			\AxiomC{$x \notin \textsf{Free}(\Gamma)$}
			\LeftLabel{}\RightLabel{{\footnotesize \textsf{$\exists_e$}}}
			\BinaryInfC{$\Gamma \vdash A$}
			\DisplayProof \\
			& & & & \\
			\hline
		\end{tabular}
		\caption{Règle de la déduction naturelle pour la logique du premier ordre.}
		\label{tab:DNregles}
	\end{table}
	
	La preuve de la complétude du système de preuve ne se fait pas directement. On utilise les notions de théorie contradictoire et de théorie consistante. On va prouver que ces deux notions sont équivalentes bien que la première porte sur la sémantique alors que la seconde sur la preuve. C'est cette équivalence qui va nous permettre de montrer la correction et la complétude dans la logique du premier ordre. Cependant, cette équivalence n'est pas plus forte que la correction et la complétude car de ce résultat, on peut également la prouver.
	
	\titlebox{red}{\textred{\textbf{Remarques sur le développement}}}{
		Ce développement permet de montrer la complétude du système de déduction naturel (section 3). Il est alors important de bien maîtriser les règles.
		\begin{enumerate}
			\item Lemme à écrire ou à évoquer: on ne le montre pas.
			\item Définir le modèle qui va vérifier notre théorie.
			\item Ce modèle est correct: on le montre par induction.
		\end{enumerate}
	}
	
	\section{Correction et complétude de la déduction naturelle}
	
	Commençons par montrer la correction et la complétude de la logique du premier ordre via la déduction naturelle. Pour cela, on va énoncer un résultat sur les théories consistantes et contradictoires que nous montrerons plus tard (une des équivalences est l'équivalence que l'on montre en développement). De ce résultat, on en déduira la correction et la complétude. On remarquera ensuite que du théorème de complétude et de correction, on peut en déduire le résultat portant sur les théories.
	
	\begin{definition}
		Une théorie $T$ est contradictoire s'il n'existe pas de modèle de $T$.
	\end{definition}
	
	\begin{definition}
		Une théorie $T$ est consistante si et seulement si $T \nvdash \bot$.
	\end{definition}
	
	\begin{theo}
		Soit $T$ une théorie. $T$ est consistante si et seulement si elle est non contradictoire.
		\label{theo:theories} 
	\end{theo}
	\begin{proof}[Démonstration via le corollaire]
		\begin{displaymath}
		\begin{array}{ccll}
		T \text{ est consistante} & \Leftrightarrow & T \vdash \bot & \textblue{\text{(Définition de consistante)}} \\
		& \Leftrightarrow & T \models \bot  & \textblue{\text{(Par le corollaire~\ref{cor:CorCom})}} \\
		& \Leftrightarrow & \text{il n'existe pas de modèle de } T  & \textblue{\text{(Il en existe pas de $\bot$)}} \\
		& \Leftrightarrow & T \text{ est contradictoire}  & \textblue{\text{(Par définition d'une théorie contradictoire)}} \\
		\end{array}
		\end{displaymath}
	\end{proof}
		
	\begin{cor}[Théorème de correction et de complétude]
		Soient $T$ une théorie et $F$ une formule close. Alors $T \vdash F$ si et seulement si $T \models F$.
		\label{cor:CorCom}
	\end{cor}
	\begin{proof}
		\begin{displaymath}
			\begin{array}{ccll}
				T \vdash F & \Leftrightarrow & T, \neg F \vdash F & \textblue{\text{(Par la règle de l'affaiblissement)}} \\
				& \Leftrightarrow & T \cup \{\neg F\} \text{ est inconsistante} & \textblue{\text{(Par définition d'une théorie inconsistante)}} \\
				& \Leftrightarrow & T \cup \{\neg F\} \text{ est contradictoire} & \textblue{\text{(Par le théorème~\ref{theo:theories})}} \\
				& \Leftrightarrow & \text{aucun modèle de } T \text{ ne satisfait } \neg F & \textblue{\text{(Par définition d'une théorie contradictoire)}} \\
				& \Leftrightarrow & \text{tout modèle de } T \text{ satisfait } F & \textblue{\text{(Par définition d'un modèle)}} \\
				& \Leftrightarrow & T \models F & \textblue{\text{(Par définition d'être un modèle)}} \\
			\end{array}
		\end{displaymath}
	\end{proof}
	
	\section{Une théorie consistante est non contradictoire}
	
	Soit $T$ une théorie consistante, montrons qu'elle est contradictoire. Pour cela, on va se baser sur un lemme que nous admettrons.
	
	\begin{lemme}
		Il existe $\mathcal{L}' \supseteq \mathcal{L}$ et $Th$ une théorie sur $\mathcal{L}'$ telle que
		\begin{enumerate}
			\item $T \subseteq Th$;
			\item Pour toute formule $F$ ayant au plus une variable libre $x$, il existe $c_F$ tel que $Th \vdash \exists x ~F \to F\left[x := c_F\right]$;
			\item $Th$ est complète.
		\end{enumerate}
	\end{lemme}
	\begin{proof}[Idées de la preuve]
		On construit $\mathcal{L}'$ et $Th$ par récurrence.
		\begin{itemize}
			\item $\mathcal{L}' = \bigcup_{n \in \N} \mathcal{L}_n$ où $\mathcal{L}_{n + 1} = \mathcal{L}_n \cup \{c_F ~|~ F \text{ a une variable libre sur } \mathcal{L}_n\}$ et $c_F$ est une nouvelle constante.
			\item $Th_1 = \bigcup_{n \in \N} T_n$ où $T_{n + 1} = T_n \cup \{\exists x~ F\left[x\right] \to F\left[c_F\right] ~|~ F \text{ a une variable libre sur } \mathcal{L}_n\}$ et $c_F$ est une nouvelle constante.
		\end{itemize}
	\end{proof}
	
	Si $Th$ est non contradictoire alors $T$ est non contradictoire \textblue{(car on peut restreindre la modèle de $Th$ sur le langage de $T$ (via l'inclusion) et toutes les formules $T$ vont être vérifiées car elles sont dans $Th$)}. On définie le modèle $\mathcal{M}$ suivant:
	\begin{itemize}
		\item $\left|\mathcal{M}\right| = \mathcal{E}$ l'ensemble des formules closes de $\mathcal{L}'$. Par le point 2 du lemme, $\mathcal{E} \neq \emptyset$.
		\item Interprétation des symboles de $\mathcal{L}'$:
		\begin{itemize}
			\item Si $c$ est une constante, $c_{\mathcal{M}} = c$.
			\item Si $f$ est un symbole de fonction $n$-aire, $f_{\mathcal{M}}(t_1, \dots, t_n) = f(t_1, \dots, t_n)$.
			\item Si $R$ est une relation $n$-aire, $(t_1, \dots, t_n) \in R_{\mathcal{M}} \Leftrightarrow Th \vdash R(t_1, \dots, t_n)$.
		\end{itemize}
	\end{itemize}
	Montrons alors que $\mathcal{M} \models Th$ \textblue{(ceci repose sur la proposition suivante)}.
	
	\begin{req}
		Dans le cadre de la théorie de l'égalité (si on utilise les règles d'introduction et d'élimination de l'égalité), $\left|\mathcal{M}\right|$ ne peut pas être simplement $\mathcal{E}$ car $Th$ égaliserait des termes syntaxiquement distincts. Il nous faut donc quotienter $\mathcal{E}$ par la relation d'équivalence $\sim$ définie telle que $t \sim t'$ si et seulement si $Th \vdash t = t'$. Ensuite, il nous faut vérifier que les interprétations soient correctes.
	\end{req}
	
	\begin{prop}
		Soit $F$ une formule, pour toute substitution $\sigma$ des variables libres de $F$ en des termes clos: $\mathcal{M} \models F\left[\sigma\right]$ si et seulement si $\mathcal{M} \vdash F\left[\sigma\right]$.
	\end{prop}
	\begin{proof}
		On raisonne par induction sur la formule $F$. Sans perte de généralité, on peut supposer que $F$ n'utilise que les connecteurs $\neg$, $\vee$, $\exists$ \textblue{(ils sont complets sémantiquement)}.
		\begin{description}
			\item[Cas $F = \bot$] $Th$  est consistante donc $Th \nvdash \bot$. De plus, $\mathcal{M} \not\models \bot$ \textblue{(par définition des modèles)}.
			\item[Cas $F = R\left(t_1, \dots, t_n\right)$] Montrons que $\mathcal{M} \models R\left(u_1 := t_1, \dots, u_n := t_n\right) \Leftrightarrow Th \vdash R\left(u_1 := t_1, \dots, u_n := t_n\right)$, pour tout $t_i$. Par définition de $R_{\mathcal{M}}$, $\left(t_1, \dots, t_n\right) \in R \Leftrightarrow Th \vdash R\left(t_1, \dots, t_n\right)$.
			\item[Cas $F = F_1 \vee F_2$] Soit $\sigma$ une substitution.
			\begin{displaymath}
			\begin{array}{ccll}
			\mathcal{M} \models F\left[\sigma\right] & \Leftrightarrow & \mathcal{M} \models F_1\left[\sigma\right] \text{ ou } \mathcal{M} \models F_2\left[\sigma\right] & \textblue{\text{(Définition de $\models$)}} \\
			& \Leftrightarrow & Th \vdash F_1\left[\sigma\right] \text{ ou } Th \vdash F_2\left[\sigma\right] & \textblue{\text{(Hypothèse d'induction)}} \\
			& \Leftrightarrow & Th \vdash F_1\left[\sigma\right] \vee F_2\left[\sigma\right] & \textblue{
				\text{(
				\AxiomC{}
				\UnaryInfC{$Th \vdash F_1\left[\sigma\right]$}
				\RightLabel{{\footnotesize $\vee_i^g$}}
				\UnaryInfC{$Th \vdash F_1\left[\sigma\right] \vee F_2\left[\sigma\right]$}
				\DisplayProof  ou
				\AxiomC{}
				\UnaryInfC{$Th \vdash F_2\left[\sigma\right]$}
				\RightLabel{{\footnotesize $\vee_i^d$}}
				\UnaryInfC{$Th \vdash F_1\left[\sigma\right] \vee F_2\left[\sigma\right]$}
				\DisplayProof)}} \\
			\end{array}
			\end{displaymath}
			\item[Cas $F = \neg G$] Soit $\sigma$ une substitution.
			\begin{displaymath}
			\begin{array}{ccll}
			\mathcal{M} \models F\left[\sigma\right] & \Leftrightarrow & \mathcal{M} \not\models G\left[\sigma\right] & \textblue{\text{(Définition de $\models$)}} \\
			& \Leftrightarrow & Th \nvdash G\left[\sigma\right] & \textblue{\text{(Hypothèse d'induction)}} \\
			& \Leftrightarrow & Th \vdash F\left[\sigma\right] & \textblue{\text{($Th$ est complète, lemme item 3)}} \\
			\end{array}
			\end{displaymath}
			\item[Cas $F = \exists x~G$] Soit $\sigma$ une substitution.
			\begin{description}
				\item[$\Rightarrow$] Supposons que $\mathcal{M} \models F\left[\sigma\right]$. Par définition de $\models$, il existe $t \in \mathcal{E}$ tel que $M \models G\left[\sigma\left[x := t\right]\right]$. Par induction, $Th \vdash G\left[\sigma\left[x := t\right]\right]$. Par la règle d'introduction du quantificateur existentiel,
				\begin{prooftree}
					\AxiomC{$Th\vdash G\left[\sigma\left[x := t\right]\right]$}
					\LeftLabel{}\RightLabel{{\footnotesize \textsf{$\exists_i$}}}
					\UnaryInfC{$Th \vdash \exists~x G\left[\sigma\right]$}
				\end{prooftree}
				Donc, $Th \vdash F\left[\sigma\right]$
				\item[$\Leftarrow$] Supposons que $\mathcal{M} \vdash F\left[\sigma\right]$. Par hypothèse sur $Th$, il esite une constante $c_G$ telle que $Th \vdash \exists x ~G\left[\sigma\right] \to G\left[\sigma\left[x := c_G\right]\right]$. Par la règle du modus pones \textblue{(élimination de l'introduction)}, on a 
				\begin{prooftree}
					\AxiomC{$Th \vdash \exists x~G[\sigma] \to G\left[\sigma\left[x := c_G\right]\right]$}
					\AxiomC{$Th \vdash \exists x~ G\left[\sigma\right]$}
					\LeftLabel{}\RightLabel{{\footnotesize \textsf{$\rightarrow_e$}}}
					\BinaryInfC{$Th \vdash G\left[\sigma\left[x := c_G\right]\right]$}
				\end{prooftree}
				Donc $Th \vdash G\left[\sigma\left[x := c_G\right]\right]$ et par induction, on a $\mathcal{M} \models G\left[\sigma\left[x := c_G\right]\right]$. Par définition de $\models$, $\mathcal{M} \models \exists x~G\left[\sigma\right]$. Donc $\mathcal{M} \models F\left[\sigma\right]$.
			\end{description}
		\end{description}
		Ce qui achève l'induction
	\end{proof}
	
	On a $\forall F \in Th$, $Th \vdash F$, et comme $\mathcal{M} \models F$, on en déduit que $\mathcal{M} \models Th$
	
	\section{Une théorie non contradictoire est consistante}
	
	La réciproque est basée sur une proposition qui se montre par induction sur l'arbre de preuve. Pour cela, on a besoin de deux lemmes intermédiaires.
	
	\begin{lemme}
		Soient $t$, $u$ des termes et $e$ un environnement. Soient $v = t\left[x := u\right]$ et $e' = e\left[x := \textsf{Val}(u, e)\right]$. Alors $\textsf{Val}(v, e) = \textsf{Val}(t', e)$.
		\label{lem:terme}
	\end{lemme} 
	\begin{proof}
		Immédiate par induction sur $t$.
	\end{proof}
	
	\begin{lemme}
		Soient $A$ une formule, $t$ un terme et $e$ un environnement. Si $e' = e\left[x := \textsf{Val}(u, e)\right]$, alors $\mathcal{M}, e \models A\left[x := t\right]$ si et seulement si $\mathcal{M}, e' \models A$.
		\label{lem:formule}
	\end{lemme} 
	\begin{proof}
		Immédiate par induction sur $A$ et en utilisant le lemme précédent.
	\end{proof}
	
	\begin{prop}
		Soient $\Gamma$, $A$ des formules, $\mathcal{M}$ une interprétation et $e$ un environnement. Si $\Gamma \vdash A$ et $\mathcal{M}, e \models \Gamma$ alors $\mathcal{M}, e \models A$.
	\end{prop}
	\begin{proof}
		On raisonne par induction sur l'arbre de preuve \textblue{(on traite donc la dernière règle appliquée)}. On traitera que les cas de l'implication et des quantificateurs \textblue{(les autres cas sont analogues)}.
		\begin{description}
			\item[Cas $\to_i$] La règle d'introduction de l'implication est 
			\AxiomC{$\Gamma, ~B \vdash A$}
			\LeftLabel{}\RightLabel{{\footnotesize \textsf{$\rightarrow_i$}}}
			\UnaryInfC{$\Gamma \vdash A \rightarrow B$}
			\DisplayProof.
			Montrons que $\mathcal{M}, e \models A \to B$. Il faut donc montre que si $\mathcal{M}, e \models A$ , alors $\mathcal{M}, e \models B$. Comme $\mathcal{M}, e \models \Gamma \cup \{A\}$ et $\Gamma, ~A \vdash B$, on conclut par induction.
			
			\item[Cas $\to_e$] La règle d'élimination de l'implication est 
			\AxiomC{$\Gamma \vdash A \rightarrow B$}
			\AxiomC{$\Gamma \vdash A$}
			\LeftLabel{}\RightLabel{{\footnotesize \textsf{$\rightarrow_e$}}}
			\BinaryInfC{$\Gamma \vdash B$}
			\DisplayProof.
			Soient $\mathcal{M}, e$ tels que $\mathcal{M}, e \models \Gamma$. Par induction, on a $\mathcal{M}, e \models A \to B$ et  $\mathcal{M}, e \models A$. D'où le résultat.
			
			\item[Cas $\forall_i$] La règle d'introduction du quantificateur universel est 
			\AxiomC{$\Gamma \vdash A$}
			\AxiomC{$x \notin \textsf{Free}(\Gamma)$}
			\LeftLabel{}\RightLabel{{\footnotesize \textsf{$\forall_i$}}}
			\BinaryInfC{$\Gamma \vdash \forall x~ A$}
			\DisplayProof.
			Soient $\mathcal{M}, e$ tels que $\mathcal{M}, e \models \Gamma$ et $a \in \left|\mathcal{M}\right|$. Puisque $x$ n'est pas libre dans $\Gamma$, $\mathcal{M}, e\left[x := a \right] \models \Gamma$ et donc par induction $\mathcal{M}, e\left[x := a \right] \models A$. On en déduit que $\mathcal{M}, e \models \forall x~ A$.
			
			\item[Cas $\forall_e$] La règle d'élimination du quantificateur universelle est 
			\AxiomC{$\vdash \forall x~ A$}
			\LeftLabel{}\RightLabel{{\footnotesize \textsf{$\forall_e$}}}
			\UnaryInfC{$\vdash A[x := t]$}
			\DisplayProof.
			Soient $\mathcal{M}, e$ tels que $\mathcal{M}, e \models \Gamma$ et $a = \textsf{Val}(t, e)$. Par induction $\mathcal{M}, e \models\forall x~A$ et donc $\mathcal{M}, e\left[x := a\right] \models A$. Le lemme~\ref{lem:formule} permet de conclure.
			
			\item[Cas $\exists_i$] La règle d'introduction du quantificateur existentiel est 
			\AxiomC{$\vdash A$}
			\LeftLabel{}\RightLabel{{\footnotesize \textsf{$\exists_i$}}}
			\UnaryInfC{$\vdash \exists~x A$}
			\DisplayProof.
			Soient $\mathcal{M}, e$ tels que $\mathcal{M}, e \models \Gamma$. Par induction et le lemme~\ref{lem:formule}, on a $\mathcal{M}, e\left[x := a\right] \models A$ et donc $\mathcal{M}, e \models \exists x~ A$.
			
			\item[Cas $\exists_e$] La règle d'élimination du quantificateur existentiel est 
			\AxiomC{$\Gamma \vdash \exists x~ A$}
			\AxiomC{$x \notin \textsf{Free}(\Gamma)$}
			\LeftLabel{}\RightLabel{{\footnotesize \textsf{$\exists_e$}}}
			\BinaryInfC{$\Gamma \vdash A$}
			\DisplayProof.
			Soient $\mathcal{M}, e$ tels que $\mathcal{M}, e \models \Gamma$. Par induction, soit $a \in \left|\mathcal{M}\right|$ tel que $\mathcal{M}, e\left[x := a\right] \models A$. Comme $x$ n'est pas libre dans $\Gamma$, on a $\mathcal{M}, e\left[x := a\right] \models \Gamma$. On en déduit que $\Gamma \vdash A$.
		\end{description}
	\end{proof}
	
	\begin{cor}
		Une théorie non contradictoire $T$ est consistante.
	\end{cor}
	\begin{proof}
		Soit $\mathcal{M}$ un modèle de $T$. Si $T \vdash \bot$, alors par la proposition $\mathcal{M} \models \bot$. Contradiction.
	\end{proof}
	
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}	
\end{document}