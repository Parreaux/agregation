\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[svgnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{array}
\usepackage{tikz}
\usepackage{subcaption}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 
\usetikzlibrary{automata,positioning,shadows}
\usetikzlibrary{calc, shapes, backgrounds,arrows}

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\newtheorem*{appli}{Application}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Tri topologique}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Cormen \cite[p.566]{Cormen}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 903 (Algorithmes de tri); 925 (Algorithmes de graphe).
			
			\textblue{\emph{Leçon où on peut l'évoquer:}} 927 (Correction et terminaison)
		}}
	
	\section{Introduction}
	
	Le tri topologique est un tri avec un ordre partiel donnée par un graphe orienté acyclique. En effet, un graphe orienté acyclique définit une relation d'ordre partielle. Cependant, avec une relation d'ordre partielle ne permet pas d'appliquer les algorithmes de tri classique. Le tri topologique tri en fonction des contraintes sur les données (introduites par le graphe) grâce au parcours en profondeur. Nous donnons ici l'algorithme du tri topologique, sa terminaison, sa correction et sa complexité.
	
	\titlebox{red}{\textred{\textbf{Remarques sur le développement}}}{
		\begin{enumerate}
			\item Algorithme du tri topologique.
			\item Terminaison de cet algorithme.
			\item Complexité de cet algorithme.
			\item Correction de cet algorithme.
			\item Ajout à la détection de cycle si le temps le permet.
		\end{enumerate}
	}
	
	\section{Présentation du tri topologique}
	
	\emph{Hypothèse}: on considère un graphe orienté acyclique \textblue{(un graphe orienté acyclique définit alors une relation d'ordre partielle sur les sommets du graphe)}.
	
	Soit $G = (V, E)$ un graphe acyclique. On cherche $L$ une liste de tous les éléments de $V$ tels que $\forall (u, v) \in A$, $u$ apparaît avant $v$ dans $L$ \textblue{(on dit une liste topologique : on voit apparaître l'ordre partiel)}. Les algorithmes~\ref{algo:tri-topologique} et~\ref{algo:visite} réalisent un tri topologique. \textblue{On n'utilise pas tout à fait l'algorithme donné dans \cite{Cormen}: on le déroule. On prend la partie du parcours en profondeur \cite[p.558]{Cormen} qui nous intéresse (on enlève les notions de dates) et on ajoute le traitement spécifique du tri topologique.}
	
	\begin{minipage}[l]{.46\linewidth}
		\begin{algorithm}[H]
			\begin{algorithmic}[1] 
				\Function{\textsc{Tri-Topologique}}{$G$} 
				\State $L \gets []$ \Comment{$G$ est un graphe}
				\State Colorier les sommets de $G$ en \textsf{blanc}
				\For{tout $u \in G$} 
				\If{$u$ est en \textsf{blanc}}
				\State $\textsc{Visite}(G, u)$
				\EndIf
				\EndFor
				\State \textsf{Renvoie} $L$
				\EndFunction
			\end{algorithmic}
			\caption{Le tri topologique d'un graphe orienté acyclique.}
			\label{algo:tri-topologique}
		\end{algorithm}
	\end{minipage} \hfill
	\begin{minipage}[r]{.46\linewidth}
		\begin{algorithm}[H]
			\begin{algorithmic}[1] 
				\Function{\textsc{Visite}}($G, u$)
				\State Colorier $u$ en \textsf{gris}
				\For{tout $v \in Adj(u)$} 
				\If{$v$ est de couleur \textsf{blanc}}
				\State $\textsc{Visite}(G, u)$
				\EndIf
				\EndFor
				\State Colorier $u$ en \textsf{noir} \Comment{\textblue{correction}}
				\State $L \gets u :: L$
				\EndFunction
			\end{algorithmic}
			\caption{Visiter un sommet: traiter ce sommet lors du parcours}
			\label{algo:visite}
		\end{algorithm}		
	\end{minipage}
	
	\begin{figure}
		\centering
		\begin{tikzpicture}[font=\sffamily]
		% Setup the style for the states
		\tikzset{node style/.style={state, 
				minimum width=1cm,
				line width=.25mm,
				fill=white,rectangle}}
		
		% Draw the states
		\node[node style] at (0,0) (0) {calçon};
		\node[node style] at (0,-2) (2) {pantalon};
		\node[node style] at (4,0) (1) {chaussettes};
		\node[node style] at (4,-2) (3) {chaussures};
		\node[node style] at (0,-4) (4) {ceinture};
		\node[node style] at (8,-2) (5) {chemise};
		\node[node style] at (8,-4) (7) {cravate};
		\node[node style] at (4,-4) (8) {veste};
		\node[node style] at (8,0) (9) {montre};
		
		% Connect the states with arrows
		\draw[every loop,auto=right,line width=.25mm,
		>=latex]
		(0) edge [] node {} (2)
		(0) edge [] node {} (3)
		(2) edge [] node {} (3)
		(2) edge [] node {} (4)
		(1) edge [] node {} (3)
		(4) edge [] node {} (8)
		(5) edge [] node {} (4)
		(5) edge [] node {} (7)
		(7) edge [] node {} (8);
		\end{tikzpicture}
		\caption{Le tri topologique sur ce graphe nous donne: chaussettes, caleçon, pantalon, chaussures, montre, chemise, ceinture, cravate, veste.}
	\end{figure}
	
	\begin{theo}[Terminaison]
		L'algorithme du tri topologique (algorithme~\ref{algo:tri-topologique}) termine.
	\end{theo}
	\begin{proof}
		On effectue au plus $|V|$ appels à \textsc{Visite} (algorithme~\ref{algo:visite}). \textblue{Comme le nombre d'appel à \textsc{Visite} est borné, le nombre d'appel récursif l'est également d'où la terminaison.}
	\end{proof}
	
	\begin{req}
		Le nombre de sommets blancs décroît strictement. Donc le tri topologique termine.
	\end{req}
	
	\begin{theo}[Correction]
		L'algorithme du tri topologique (algorithme~\ref{algo:tri-topologique}) est correct.
	\end{theo}
	\begin{proof}
		\textblue{Notons que l'on fait un parcours en profondeur. On va alors prouver trois propriétés permettant d'assurer la correction.}
		\paragraph{Propriété 1}: chaque sommet est ajouter une et une seule fois à la liste. \textblue{En effet, on ajoute le sommet à la liste à la fin de \textsc{Visiter}. On appelle une seule fois \textsc{Visiter} pour chacun des sommets \textsf{blanc} qui sont directement colorier en \textsf{gris} au début de la fonction (ils ne sont plus jamais \textsf{blanc} ensuite).}
		
		\paragraph{Propriété 2}: à chaque début et fin de \textsc{Visiter}, $L$ contient exactement les sommets \textsf{noir}. \textblue{En effet, on les ajoute après les avoir mis en \textsf{noir} et avant de sortir de \textsc{Visiter}.}
		
		\paragraph{Propriété 3}: pour toute arête $(u, v) \in E$, $v$ se retrouve avant $u$ dans $L$ \textblue{(considération temporelle)}. Soit $(u, v) \in E$ explorée par le tri. Lors de l'appel à \textsc{Visiter}, trois cas sont possibles.
		\begin{itemize}
			\item Si $v$ est \textsf{noir}, alors $v \in L$ donc $v$ est dans $L$ avant $u$.
			\item Si $v$ est \textsf{blanc}, alors $\textsc{Visite}(G, v)$ se termine avant l'ajout de $u$ dans $L$ donc $v$ est dans $L$ avant $u$.
			\item Si $v$ est \textsf{gris}, alors il existe un chemin de $v$ à $u$ \textblue{(se montre par récurrence)} et un arc de $u$ à $v$. On a une contradiction avec l'hypothèse d'acyclicité.
		\end{itemize}
		
		Par ces trois propriété, on a montré la correction de l'algorithme du tri topologique.
	\end{proof}
	
	\noindent\emph{Complexité}: on effectue essentiellement un parcours en profondeur (on n'ajoute aucune opération coûteuse), sa complexité est alors la même que le parcours en profondeur: $O\left(|V| + |E|\right)$ si $G$ est en liste d'adjacence.
	
	\begin{req}
		On peut alors ajouter la détection de cycle: si le sommet visiter tombe sur un sommet \textsf{gris} on a un cycle.
	\end{req}
	
	\noindent\emph{Quelques applications du tri topologique}: 
	\begin{itemize}
		\item définition non circulaire de mots dans un dictionnaire s'utilisant mutuellement pour se définir \textblue{(contre-exemple:  algorithme - modèle de calcul - procédure effective ;) )};
		\item unité d'enseignement (on essaye que les élèves on les prérequis nécessaire lorsqu'ils assistent à un cours) : plus généralement, la notion de prérequis;
		\item l'outil \textsf{make} et plus généralement l'ordonnancement de tâches non circulaire;
		\item implémenter comme pré-traitement d'un plus court chemin (remplace la file de priorité).
	\end{itemize} 
	
	\begin{req}
		\begin{itemize}
			\item Si le graphe est cyclique, la correction n'est plus assurée.
			\item Si le graphe n'est pas connexe alors l'algorithme de tri ne pose aucun problème.
		\end{itemize}
	\end{req}
	
	\section{Autours du parcours de profondeur}
	
	\input{./../Notions/Graphe_parcoursProfondeur.tex}

	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}
\end{document}