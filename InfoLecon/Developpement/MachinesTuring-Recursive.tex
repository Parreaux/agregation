\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{array}
\usepackage{tikz}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}

\parskip=0\baselineskip %0.5 d'habitude

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
} 


\begin{document}
	
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}{\textcolor{blue}}
	\newcommand{\textred}{\textcolor{red}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{lemme}{Lemme}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{Les machines de Turing sont équivalentes que les fonctions $\mu$-récursives}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Wolper \cite[p.135]{Wolper}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçons où on présente le développement:}} 913 (Machines de Turing); 912 (Fonctions récursives).
			
			\textblue{{Leçons pour lesquelles on peut l'évoquer:}} 914 (Décidabilité et indécidabilité) si on évoque la thèse de Church.
		}}

	\section{Introduction}
	
	La thèse de Church qui dit que les fonctions calculables sont exactement celles calculables par une machine de Turing ne peut pas être prouvé. Cependant nous pouvons conforter ce résultat en comparant la puissance de calcul des machines de Turing à d'autres modèles de calculs qui permettent de définir des fonctions calculables (les fonctions récursives par exemple). Si les ensembles définit par ces deux modèles sont égaux cela nous conforte dans la thèse de Church. 
	
	L'objectif de ce développement est de montrer que les machines de Turing est un modèle de calcul équivalent aux fonctions $\mu$-récursives. Pendant ce développement nous nous concentrons sur la preuve du sens direct du théorème \ref{theo} énoncé ci-dessus. Nous évoquerons rapidement le sens réciproque à la fin de ce document.
	
	\begin{theo}
		Les fonctions calculables par une machines de Turing sont exactement les fonctions $\mu$-récursives.
		\label{theo}
	\end{theo}
	
	En particulier, dans la leçon sur les fonctions primitives récursives, ils est important de noter que nous prouvons le théorème dans le cadre de fonctions totales (le résultat analogue se fait trivialement). On joue alors sur la définition de calculabilité des machines de Turing.
	
	\begin{theo}
		Les fonctions totales calculables par une machines de Turing sont exactement les fonctions $\mu$-récursives totales.
	\end{theo}
	
	\titlebox{red}{\textred{\textbf{Remarques sur le développement}}}{
		Ce développement présente la construction d'une fonction $\mu$-récursive à partir du fonctionnement d'une machine de Turing (étude de cas). Cette version n'est pas la plus formelle mais permet de faire apparaître la Gödelisation. Pour avoir une meilleure version, aller voir le Carton \cite{Carton}.
	}
	
	\section{Les machines de Turing sont moins expressives que les fonctions $\mu$-récursives.}
	
	\begin{theo}
		Toute fonction (totale) calculable par une machine de Turing est aussi $\mu$-récursive (totale).
	\end{theo}
	
	
\begin{proof}
	Soit $M$ une machine de Turing sur un alphabet $\Sigma$ calculant une fonction $f_{\mathcal{M}} : \Sigma^{*} \rightarrow \Sigma^{*}$. Quitte à déterminiser $\mathcal{M}$, on peut supposer que $\mathcal{M}$ est déterministe. On pose une fonction $gd : \Sigma^{*} \rightarrow \N$ codant les mots de $\Sigma^{*}$ par des entiers. Nous allons montrer qu'il existe $f$ une fonction $\mu$-récursive tel que $f_{\mathcal{M}}(w) = gd^{-1}(f(gd(w)))$.
	
	\subparagraph{Hypothèses sur la machine $\mathbb{\mathcal{M}}$} On peut supposer sans perte de généralité qu'il existe $m \in \N^{*}$ tel que $\mathcal{Q} = \{0; \dots; m-1\}$ où $\mathcal{Q}$ est l'ensemble des états de $\mathcal{M}$. De plus, on suppose que $\mathcal{M}$ possède un unique état initial $q_{init} = 0$ et un unique état final $ q_{final} = m-1$ qui est atteint lorsque $\mathcal{M}$ inscrit le dernier caractère de la solution (celui-ci est alors avant la tête de lecture sur le ruban). De plus, on suppose $\mathcal{M}$ complète de telle sorte que la fonction de transition $\delta: \mathcal{Q} \times \Sigma \rightarrow \mathcal{Q} \times \Gamma \times \{\leftarrow; \rightarrow\}$ soit total. 
	
	\subparagraph{Coder des configurations} Pour traduire une exécution d'une machine de Turing déterministe à l'aide de fonctions $\mu$-récursives, il nous faut les définir sur des configurations. Celles-ci interpréterons la suite de configurations représentant cette exécution. Nous devons alors coder les configurations : on code les mots sur les rubans par la méthode de Godël et $\{\leftarrow; \rightarrow\}$ par deux entiers distincts. On peut codé les configurations de la machine de Turing à l'aide de la fonction $\mathsf{code\_conf} : \Sigma^{*} \times \N \times \Sigma^{*} \rightarrow \N^{3}$ définie telle que $(\alpha, n, \gamma) \mapsto (gd(\alpha), n, gd(\gamma))$. Comme $gd$ est une fonction primitive récursive (elle est une somme finie de multiplications et de puissances), $\mathsf{code\_conf}$ est également primitive récursive.
	
	
	\subparagraph{Définitions des fonctions primitives récursives utilisées lors du fonctionnement d'une machine de Turing déterministe} Le comportement d'une machine de Turing déterministe peut être modélisé par une composition de fonctions primitives récursives sous une minimisation non bornée. Ces fonctions opèrent sur le codage des configurations à l'aide de la fonction $\mathsf{code\_conf}$ qui code les configurations de $\mathcal{M}$.
	\begin{description}
		\item[\textsc{init}(x)] donne la configuration initiale de $\mathcal{M}$ pour le mot d'entrée $w$ mis sous forme de Gödel $v$.
		
		\begin{tabular}{cccl}
			\textsc{init}: & $\N$ & $\rightarrow$ & $\N^{3}$ \\
			& $v$ & $\mapsto$ & $\mathsf{code\_conf}(\epsilon, 0, gd^{-1}(v))$ \\
		\end{tabular}
		
		La fonction \textsc{init} est bien primitive récursive car $\textsc{init} = \circ (\mathsf{code\_conf}, \epsilon, \mathbb{O}, \circ(gd^{-1}, \pi_{1}^{1}))$.
		
		\item[\textsc{config$\_$suivante}(x)] donne la configuration suivante d'une configuration $c =(\alpha a, c_2, b\beta)$ sous forme de Gödel que l'on note $x=(x_1, x_2, x_3)$ par $\mathcal{M}$. On définit la fonction \textsc{config$\_$suivante}: $\N^{3} \rightarrow \N^{3}$ par:
		\[ \textsc{config\_suivante}(x_1, x_2, x_3) \mapsto
		\left \{
		\begin{array}{cc}
		\mathsf{code\_conf}(\alpha ab', q', \gamma) & si ~ \delta(c_2, b) = (q', b', \rightarrow) \\
		\mathsf{code\_conf}(\alpha, q', ab'\gamma) & si ~ \delta(c_2, b) = (q', b', \leftarrow) \\
		\end{array}
		\right.
		\]
		
		La fonction \textsc{config$\_$suivante} est une fonction primitive récursive. La fonction de transitions, $\delta$ est primitive récursive car elle est à support fini. En effet, une fonction à support fini peut toujours être écrite comme somme finie de fonctions indicatrices. Ces dernières étant, elles-même, primitives récursives par le prédicat \textsc{égalité}. On en conclut, que les fonctions à support fini peuvent être réécrite à l'aide d'une conditionnelle et de fonctions primitives récursives. 
		
		Soient $(x_1, x_2, x_3)$ la configuration encodée de la configuration $c = (c_1, c_2, c_3)$. On note $\delta(c_2, b) = (q', b', \delta_3)$ où $\delta_3 \in \{\leftarrow;\rightarrow\}$. On récupère ces valeurs à l'aide d'une projection sur le résultat de cette fonction (ceci est primitif récursif). On a alors
		
		\[\textsc{config$\_$suivante}(x_1, x_2, x_3) = 
		\left \{
		\begin{array}{cc}
		(\circ (\mathsf{code\_conf}, \alpha ab', q', \gamma)) & si ~ \delta_3 = \rightarrow \\
		(\circ (\mathsf{code\_conf}, \alpha, q', ab'\gamma)) & si ~ \delta_3 = \leftarrow \\
		\end{array}
		\right.
		\]
		qui est alors primitive récursive.
		
		
		\item[\textsc{config}(x, n)] donne la configuration après $n$ étapes de calculs d'une configuration $c$ sous forme de Gödel que l'on note $x$ dans $\mathcal{M}$. On définit \textsc{config}: $\N^{3} \times \N \rightarrow \N^{3}$ tel que:
		\[ \textsc{config}(x, n) \mapsto
		\left  \{
		\begin{array}{ll}
		x & si ~ n = 0 \\
		\textsc{config\_suivante}(\textsc{config}(x, n-1)) & sinon \\
		\end{array}
		\right.
		\]
		qui est primitive récursive car $\textsc{config} = rec(\pi_{1}^{3}, \circ (\textsc{config\_suivante}, \pi_{3}^{3}))$;
		
		\item[\textsc{stop}(x)] est un prédicat qui teste si la configuration donnée $c = (c_1, c_2, c_3)$ dont la représentation de Gödel est $x= (x_1, x_2, x_3)$ est finale.
		
		\begin{tabular}{cccl}
			\textsc{stop}: & $\N^{3}$ & $\rightarrow$ & $\{0; 1\}$ \\
			& $(x_1, x_2, x_3)$ & $\mapsto$ & $x_2 = m-1$ \\
		\end{tabular}
		
		La fonction \textsc{stop} est bien récursive primitive car $\textsc{stop} = \circ (egal, \pi_{2}^{3}, m-1)$. (Dans le cadre de la leçon 912: notons que le prédicat est primitif récursif car la machine de Turing calcul la fonction $f$ et donc elle s'arrête nécessairement car $f$ est totale. Dans le cadre d'une fonction partielle le prédicat n'est plus sûr et on tombe sur une fonction primitive récursive partielle.)
		
		\item[\textsc{sortie}(x)] donne la valeur du ruban de $\mathcal{M}$ lorsqu'elle est dans une configuration finale $c_{\text{finale}} = (c_1, c_2, c_3)$ dont la représentation de Gödel est $x= (x_1, x_2, x_3)$.
		
		\begin{tabular}{cccl}
			\textsc{sortie}: & $\N^{3}$ & $\rightarrow$ & $\N$ \\
			& $(x_1, x_2, x_3)$ & $\mapsto$ & $x_1$ \\
		\end{tabular}
		
		La fonction \textsc{sortie} est bien récursive primitive car $\textsc{sortie} = \pi_{1}^{3}$.
	\end{description}
	
	\subparagraph{Représentation de $\mathcal{M}$ et conclusion} La fonction $f$ recherché est alors $f(x) = \textsc{sortie}(\textsc{config}(\textsc{init}(x), \textsc{nb\_pas}(x)))$ où $\textsc{nb\_pas}(x) = \mu i. \textsc{stop}(\textsc{config}(\textsc{init}(x),i))$. Comme \textsc{nb$\_$pas} est une minimisation non bornée d'une fonction primitive récursive $f(x,i) = \circ (\textsc{stop}, \circ (\textsc{config}, \circ (\textsc{init}, \pi_{1}^{2}), \pi_{2}^{2})) $, elle est $\mu-recursive$. Donc $f$ est $\mu$-récursive par composition de fonctions primitives récursives et $\mu$-récursive.
\end{proof}
	
	\section{Les machines de Turing sont plus expressives que les fonctions $\mu$-récursives}
	
	
	\begin{theo}
		Toute fonction $\mu$-récursive est aussi calculable par une machine de Turing.
	\end{theo}
	
	\begin{proof}[Idée de la preuve]
		Se fait par induction sur la structure des expressions primitives récursives.
		\begin{description}
			\item[Fonction nulle]: Machine qui remplace les arguments par $0$.
			\item[Fonction sucesseur] Machine qui remplace incrémente d'un le dernier nombre d'écrit.
			\item[Projection] Recopier le $i$ème argument en première place et effacer le reste.
			\item[Composition] Remplacer sur la deuxième machine ses arguments par le résultat de ceux-ci par la première.
			\item[Récursivité] Boucle pour.
			\item[Minimisation non bornée] Boucle tant que.
		\end{description}
	\end{proof}
	
	\section{Gödelisation}
	
	Pour représenter des chaînes de caractères par des entiers ou vise-versa, on peut raisonner comme suit: les chaînes de caractères est un ensemble dénombrable donc il existe une bijection entre les entiers et les chaînes de caractères. On peut alors coder les entiers (ou les chaînes de caractères) à l'aide de cette bijection (ou de sa réciproque). \textred{Attention}: ce raisonnement est correct mais pas suffisant. En effet, dans le contexte de la calculabilité, il faut de plus que cette bijection (et sa réciproque) soit calculable \textblue{(par une procédure effective)}. Nous nommerons cette représentation, le représentation effective des entiers ou des chaînes de caractères.
	
	\begin{lemme}
		Il existe une représentation effective des chaînes de caractères par les entiers.
	\end{lemme}
	
	\begin{proof}
		Les représentations binaire \textblue{(sur l'alphabet $\Sigma = \{0, 1\}$)} ou décimale sont des exemples de représentations effectives. Nous allons montrer que la représentation binaire est bien une représentation effective en montrant qu'il existe des fonctions $\mu$-récursives \textblue{(qui sont en réalité des fonctions primitives récursives)} qui permettent de calculer la représentation binaire.
		\begin{description}
			\item[\textsc{binlong}($n$)] Calcul la longueur binaire de $n$ \textblue{(primitive récursive car c'est une minimisation bornée (par $n$) de division par deux qui donne le résultat)};
			\item[\textsc{chiffre}($n,m$)] Donne la $m$ième chiffre de l'entier $n$ si $m \leq \textsc{binlong}(n)$ et $0$ sinon \textblue{(primitive récursive car c'est une minimisation bornée (par $m$) de division par deux qui donne le résultat)}.
		\end{description}
		Ces deux fonctions sont bien primitives récursives.
	\end{proof}
	
	\textblue{\emph{Remarque}: notons que cette représentation bien que calculable par une machine de Turing n'est pas nécessairement la plus agréable à manipuler. Une représentation unaire, vue comme une composition de fonction successeur, peut être beaucoup plus intéressante pour écrire sur le ruban de notre machine de Turing.}
	
	La représentation inverse est la plus délicate, c'est elle qui porte le nom de Gödélisation car introduite par le logicien Kurt Gödel.
	
	\begin{lemme}
		Il existe une représentation effective des entiers par les chaînes de caractères.
	\end{lemme}
	
	\begin{proof}
		Soit $\Sigma$ un alphabet contenant $k$ symboles. Une représentation simple consiste à attribué à chacune des lettres de l'alphabet un nombre entre $0$ et $k-1$ (si $a \in \Sigma$, on note $gd(a)$ son codage par un entier). Puis on code la chaîne de caractère à partir de ce codage: soit $w = w_1 \dots w_n$ une chaîne de caractères, alors $gd(w) = \sum_{i=0}^{l}k^(l-i)gd(w_i)$ qui correspond au naturel dont la représentation en base $k$ est $gd(w_1)\dots gd(w_n)$.
		
		\textred{Attention}: ce codage n'est pas univoque puisque $gd(aa) = 00 = 0 = gd(a)$. Pour corriger cette représentation, il faut que le codage ne doit pas utilisé $0$. Pour cela on code les lettres de l'alphabet entre $1$ et $k$ et on a $gd(w) = \sum_{i=0}^{l}(k+1)^(l-i)gd(w_i)$
	\end{proof}
	
	\textblue{\emph{Remarque}: Il est bon de remarqué que la représentation donnée par $gd$ n'est pas injective. En effet, par définition $0$ ne sera jamais atteint. Cependant, comme nous cherchons une représentation des chaînes de caractères et non des entiers, cette non injectivité n'est pas gênante.}
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}

	
\end{document}