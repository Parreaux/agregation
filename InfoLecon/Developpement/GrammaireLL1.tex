\documentclass[a4paper,9pt]{article}

\usepackage{fullpage}%
\usepackage[utf8]{inputenc}%
\usepackage[T1]{fontenc}%
\usepackage[main=francais]{babel}%

\usepackage{graphicx}%
\usepackage{pdfpages}
\usepackage{url}%

\usepackage{array}
\usepackage{tikz}

\usepackage{mathpazo}%
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{appendix}
\usepackage{wasysym}
\usepackage{textcomp}
\usepackage{tikz}

\usepackage{algorithm}
\usepackage{algpseudocode}
%\usepackage{algorithmicx}
\usepackage{listings}


\parskip=0\baselineskip %0.5 d'habitude
\usetikzlibrary{automata,positioning}
\usetikzlibrary{calc, shapes, backgrounds}

\newcommand{\titlebox}[3]{%
	\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw=#1]%
	\tikzstyle{title}=[fill=white]%
	%
	\bigskip\noindent\begin{tikzpicture}
	\node[titlebox] (box){%
		\begin{minipage}{0.94\textwidth}
		#3
		\end{minipage}
	};
	%\draw[color=blue] (box.north west)--(box.north east);
	\node[title] at (box.north) {#2};
	\end{tikzpicture}\bigskip%
}

\begin{document}
	\newcommand{\R}{\mathbb{R}}
	\newcommand{\Z}{\mathbb{Z}}
	\newcommand{\N}{\mathbb{N}}
	\newcommand{\Q}{\mathbb{Q}}
	\newcommand{\C}{\mathbb{C}}
	
	\sloppy
	
	\newcommand{\textblue}[1]{\textcolor{blue}{#1}}
	\newcommand{\textgreen}[1]{\textcolor{Green}{#1}}
	\newcommand{\textred}[1]{\textcolor{red}{#1}}
	\newcommand{\textbrown}[1]{\textcolor{Brown}{#1}}
	
	\theoremstyle{plain}
	\newtheorem*{theo}{Théorème}
	\newtheorem*{prop}{Proposition}
	\newtheorem*{cor}{Corollaire}
	\newtheorem*{lemme}{Lemme}
	\newtheorem*{appli}{Application}
	\theoremstyle{definition}
	\newtheorem*{definition}{Définition}
	\theoremstyle{remark}
	\newtheorem*{req}{Remarque}
	
	\title{$\mathcal{L}(G_{\text{post}})$ est $LL(1)$}
	
	\author{Julie Parreaux}
	\date{2018-2019}
	
	\maketitle
	
	\noindent\emph{Référence du développement:} Schwarzentruber \cite[p.151]{LegendreSchwarzentruber}
	
	\noindent\fcolorbox{blue}{white}{\parbox{\textwidth}{
			\textblue{\emph{Leçon où on présente le développement:}}  923 (Analyse lexicale et syntaxique).
		}}
	
	\section{Introduction}
	
	L'approche $LL(1)$ est une approche gloutonne de l'analyse syntaxique qui permet de la réaliser en temps linéaire. Cependant pour obtenir un tel résultat, il nous faut utiliser des grammaires bien particulières : les grammaires $LL(1)$. Comme toutes les grammaires ne sont pas $LL(1)$, il nous faut montrer qu'un langage est LL(1) en lui trouvant une grammaire $LL(1)$. C'est ce que nous faisons dans ce développement pour le langage engendré par $G_{post}$. 
	
	\titlebox{red}{\textred{\textbf{Remarques sur le développement}}}{
		Ce développement permet de montrer qu'une grammaire est $LL(1)$ et qu'elle engendre le bon langage.
		\begin{enumerate}
			\item Exhiber une grammaire candidate.
			\item Montrer que cette grammaire est bien $LL(1)$ \textblue{(caractérisation via les premier et les suivant)}.
			\item Montrer que $\mathcal{L}(G_{\text{post}}) \subseteq \mathcal{L}(G_{\text{post}}^{LL(1)})$ \textblue{(par récurrence forte)}.
			\item Montrer que $\mathcal{L}(G_{\text{post}}) \supseteq \mathcal{L}(G_{\text{post}}^{LL(1)})$ \textblue{(par récurrence forte)}.
		\end{enumerate}
	}
	
	\section{La grammaire $G_{\text{post}}$ est $LL(1)$}
	
	\begin{definition}
		La grammaire $G_{\text{post}}$ est définie par
		\begin{displaymath}
		\begin{array}{ccl}
		S & \to & SS+ \\
		S & \to & c \\
		\end{array}
		\end{displaymath}
	\end{definition}
	
	\begin{prop}
		Le langage engendré par la grammaire $G_{\text{post}}$ est $LL(1)$, c'est-à-dire qu'il existe une grammaire $LL(1)$ qui reconnaît exactement le même langage.
	\end{prop}
	\begin{proof}
		\begin{description}
			\item[Étape 1] Exhibons une grammaire candidate.
			\begin{displaymath}
			G_{\text{post}}^{LL(1)} : 
			\begin{array}{ccl}
			S & \to & ST \\
			T & \to & S + T \\
			T & \to \epsilon
			\end{array}
			\end{displaymath}
			
			\item[Étape 2] Montrons que $G_{\text{post}}^{LL(1)}$ est $LL(1)$.
			\begin{itemize}
				\item $S$ donne une seule règle donc la grammaire est $LL(1)$ en $S$.
				\item $T$: $\textsf{Premier}(\epsilon) \cap \textsf{Premier}(S + T) = \emptyset$ et $\textsf{Suivant}(T) \cap \textsf{Premier}(S + T) = \emptyset$ donc la grammaire est $LL(1)$ en $T$.
				\begin{displaymath}
				\begin{array}{l}
				\textsf{Suivant}(S) = \{+, \wasypropto\} \\
				\textsf{Suivant}(T) = \textsf{Suivant}(S) \cup \textsf{Suivant}(T) = \{+, \wasypropto\} \\
				\textsf{Premier}(cT) = \{c\} \\
				\textsf{Premier}(S + T) = \textsf{Premier}(S) = \{c\} \\ 
				\textsf{Premier}(\epsilon) = \{\Bowtie\} \\
				\end{array}
				\end{displaymath}
			\end{itemize}
			
			\item[Étape 3] Montrons que $\mathcal{L}(G_{\text{post}}) \subseteq \mathcal{L}(G_{\text{post}}^{LL(1)})$. Raisonnons par récurrence forte sur $n \in \N$ via la propriété $P_n$: "un mot contenant $n$ occurrences de $+$ engendré par $G_{\text{post}}$ est engendré par $G_{\text{post}}^{LL(1)}$".
			\begin{description}
				\item[Initialisation] Montrons que $P_0$. Le seul mot sans occurrence de $+$ par  $G_{\text{post}}$ est $c$. Il est également engendré par $G_{\text{post}}^{LL(1)}$: $S \to CT \to c\epsilon \to c$.
				\item[Hérédité] Soit $n \in \N$ tel que $\forall i \leq n$ $P_i$ est vrai. Montrons que $P_{n+1}$. Soit $m_1m_2+$ un mot engendré par $G_{\text{post}}$ ayant $(n + 1)$ terminaux $+$.
				\begin{itemize}
					\item Par définition de $G_{\text{post}}$, $m_1$ et $m_2$ sont également engendrés par $G_{\text{post}}$.
					\item Les mots $m_1$ et $m_2$ ont au plus $n$ terminaux $+$ chacun. Par hypothèse de récurrence, il existe deux arbres de dérivation $T_1$ et $T_2$ de $G_{\text{post}}^{LL(1)}$ tel que $\textsf{mot}(T_1) = m_1$ et $\textsf{mot}(T_2) = m_2$.
					\begin{itemize}[label=$*$]
						\item Dans $T_1$ il existe une branche $T ~-~ \epsilon$ \textblue{(sinon, il resterait des non terminaux $T$ dans le mot)}.
						\item On peut donc prendre celle qui est la plus à droite pour la remplacer par 
					\begin{tikzpicture}[level/.style={sibling distance=5em, level distance=3em}]
					\node [draw=none] (a){$T$}
					child {node [draw=none] (2){$T_2$}}
					child {node [draw=none] (b) {$+$}}
					child {node [draw=none] (3) {$T$}
					child {node [draw=none] (c) {$\epsilon$}}};
					\end{tikzpicture}
						\item Cet arbre est bien engendré par la grammaire \textblue{(correspond à $T \to T_2 + T \to T_2 + \epsilon \to T_2$)}.
						\item Le mot des feuilles de cet arbre est $m_1m_2 +$.
						\begin{itemize}
							\item On a pris la branche $T~-~\epsilon$ la plus à droite dans $T_1$ donc il n'y a pas de lettre après.
							\item À gauche on génère $m_1$.
							\item L'arbre inséré génère donc $m_2$.
						\end{itemize}
					\end{itemize}
				\end{itemize}
				D'où la récurrence.
			\end{description}
			
			\item[Étape 3] Montrons que $\mathcal{L}(G_{\text{post}}^{LL(1)}) \subseteq \mathcal{L}(G_{\text{post}})$. Raisonnons par récurrence forte sur $n \in \N$ via la propriété $P_n$: "un mot contenant $n$ occurrences de $+$ engendré par $G_{\text{post}}^{LL(1)}$ est engendré par $G_{\text{post}}$".
			\begin{description}
				\item[Initialisation] Montrons que $P_0$. Le seul mot sans occurrence de $+$ par  $G_{\text{post}}^{LL(1)}$ obtenu par $S \to CT \to c\epsilon \to c$ est $c$. Il est également engendré par $G_{\text{post}}$ par $S \to c$.
				\item[Hérédité] Soit $n \in \N$ tel que $\forall i \leq n$ $P_i$ est vrai. Montrons que $P_{n+1}$. Soit un mot $m$ avec $(n + 1)$ occurrence du non-terminal $+$. Cherchons le $+$ de la racine de l'arbre de dérivation de la grammaire de $G_{\text{post}}$. On parcours en profondeur la branche droite de l'arbre de dérivation de $m$ par $G_{\text{post}}^{LL(1)}$.
				\begin{itemize}
					\item Cette branche se termine $T ~-~ \epsilon$.
					\item Le nœud père de $T ~-~ \epsilon$ ne peut pas être $S$ \textblue{(sinon pas de $+$)} donc ce nœud est nécessairement $T$ et on a l'arbre suivant. Si on remplace la branche de racine $T$ par $T ~-~ \epsilon$, on a avec $T_3$ qui possède un $+$ en mot donc par l'hypothèse de récurrence, il existe $m_3$ un mot de $G_{\text{post}}$ correspondant à $T_3$. De même pour $T_4$ qui donne $m_4$ \textblue{(par hypothèse de récurrence)}. Donc l'arbre génère $m_3m_4 + \in \mathcal{L}(G_{\text{post}})$.
					
					\begin{minipage}{.46\textwidth}
						\begin{tikzpicture}[level/.style={sibling distance=5em, level distance=3em}]
						\node [draw=none] (a){$U$}
						child {node [draw=none] (1){$T_3$}}
						child {node [draw=none] (2){$T$}
							child {node [draw=none] (3) {$T_4$}}
							child {node [draw=none] (4) {$+$}}
							child {node [draw=none] (5) {$T$}
								child {node [draw=none] (c) {$\epsilon$}}}};
						\end{tikzpicture}
					\end{minipage}
					\begin{minipage}{.46\textwidth}
						\begin{tikzpicture}[level/.style={sibling distance=5em, level distance=3em}]
						\node [draw=none] (a){$U$}
						child {node [draw=none] (1){$T_3$}}
						child {node [draw=none] (2){$T$}
						child {node [draw=none] (c) {$\epsilon$}}};
						\end{tikzpicture}
					\end{minipage}
				\end{itemize}
			\end{description}
		\end{description}
	\end{proof}
	
	
	\begin{footnotesize}
		\bibliographystyle{plain}
		\bibliography{./../../Livre}
	\end{footnotesize}

	
\end{document}